import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarCampanaComponent } from './actualizar-campana.component';

describe('ActualizarCampanaComponent', () => {
  let component: ActualizarCampanaComponent;
  let fixture: ComponentFixture<ActualizarCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
