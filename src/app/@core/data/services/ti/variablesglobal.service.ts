import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VariablesglobalService {
  total: number;
  constructor() { }
  savevariable(value) {
    this.total = value;
  }
  getvariable() {
    return this.total;
  }
}
