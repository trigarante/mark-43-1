import {Observable} from 'rxjs';

export interface GrupoUsuarios {
  id?: number;
  idSubarea: number;
  idPuesto: number;
  idPermisosVisualizacion: number;
  nombre: string;
  descripcion: string;
  permisos: string;
  activo: number;
  puestoNombre: string;
  subarea: string;
  idGrupo: number;
}

export abstract class GrupoUsuariosData {
  abstract get(): Observable<GrupoUsuarios[]>;
  abstract post(jsonGrupoUsuario):  Observable<GrupoUsuarios>;
  abstract postSocket(json): Observable<JSON>; //
  abstract getByID(idGrupo): Observable<GrupoUsuarios>;
  abstract put(idgrupo, jsonGrupoUsuario): Observable<GrupoUsuarios>;
}
