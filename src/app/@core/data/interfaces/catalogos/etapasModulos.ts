import {Observable} from 'rxjs';

export interface EtapasModulos {
  id: number;
  idArea: number;
  etapa: string;
  nivel: number;
  visible: number;
  escritura: number;
}

export abstract class EtapasModulosData {
  abstract get(): Observable<EtapasModulos[]>;
  abstract createEtapasModulos(etapaModulo): Observable<EtapasModulos>;
  abstract getEtapasModulosById(idEtapaModulo): Observable<EtapasModulos>;
  abstract updateEtapasModulos(idEtapaModulo, etapaModulo): Observable<EtapasModulos>;
}
