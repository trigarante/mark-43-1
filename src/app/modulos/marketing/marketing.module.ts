import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClipboardModule} from 'ngx-clipboard';
import {MarketingRoutingModule} from './marketing-routing.module';
import {CampanaComponent} from './campana/campana.component';
import {MarketingComponent} from './marketing.component';
import {MediosDifusionComponent} from './medios-difusion/medios-difusion.component';
import {CampanaCategoriaComponent} from './campana-categoria/campana-categoria.component';
import {PaginaComponent} from './pagina/pagina.component';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {
  NbAlertModule,
  NbDatepickerModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbTooltipModule,
} from '@nebular/theme';
import {DialogModule, DropdownModule, FileUploadModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule, MatPaginatorIntl,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
} from '@angular/material';
import { CampanaMarketingCreateComponent } from './modals/campana-marketing-create/campana-marketing-create.component';
import { AsignarCategoriaCamapanaComponent } from './modals/asignar-categoria-camapana/asignar-categoria-camapana.component';
import { AsignarMediosDifusionComponent } from './modals/asignar-medios-difusion/asignar-medios-difusion.component';
import { AseguradorasComponent } from './aseguradoras/aseguradoras.component';
import { AgregarCamposComponent } from './modals/agregar-campos/agregar-campos.component';
import { AgregarNodoComponent } from './modals/agregar-nodo/agregar-nodo.component';
import { GuardadoComponent } from './modals/guardado/guardado.component';
import { ModificarDatosComponent } from './modals/modificar-datos/modificar-datos.component';
import { ActualizarPaginaComponent } from './modals/actualizar-pagina/actualizar-pagina.component';
import { VerConfiguracionComponent } from './modals/ver-configuracion/ver-configuracion.component';
import {ProveedoresLoadCreateComponent} from './modals/proveedores-load-create/proveedores-load-create.component';
import { AgregarAseguradoraComponent } from './modals/agregar-aseguradora/agregar-aseguradora.component';
import { DatosNodosComponent } from './modals/datos-nodos/datos-nodos.component';
import { AgregarCamposNodoComponent } from './modals/agregar-campos-nodo/agregar-campos-nodo.component';
import { ActualizarCamposNodoComponent } from './modals/actualizar-campos-nodo/actualizar-campos-nodo.component';
import { ActualizarCampanaComponent } from './modals/actualizar-campana/actualizar-campana.component';
import { ActualizarMediosDifusionComponent } from './modals/actualizar-medios-difusion/actualizar-medios-difusion.component';
import { ActualizarCampanaCategoriaComponent } from './modals/actualizar-campana-categoria/actualizar-campana-categoria.component';
import { AgregarTipoComponent } from './modals/agregar-tipo/agregar-tipo.component';



@NgModule({
  declarations: [
    CampanaComponent,
    MarketingComponent,
    MediosDifusionComponent,
    CampanaCategoriaComponent,
    PaginaComponent,
    CampanaMarketingCreateComponent,
    AsignarCategoriaCamapanaComponent,
    AsignarMediosDifusionComponent,
    AseguradorasComponent,
    AgregarCamposComponent,
    AgregarNodoComponent,
    GuardadoComponent,
    ModificarDatosComponent,
    ActualizarPaginaComponent,
    VerConfiguracionComponent,
    ProveedoresLoadCreateComponent,
    AgregarAseguradoraComponent,
    DatosNodosComponent,
    AgregarCamposNodoComponent,
    ActualizarCamposNodoComponent,
    ActualizarCampanaComponent,
    ActualizarMediosDifusionComponent,
    ActualizarCampanaCategoriaComponent,
    AgregarTipoComponent,
  ],
  imports: [
    CommonModule,
    MarketingRoutingModule,
    ThemeModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    ClipboardModule,
  ],
  entryComponents: [
    CampanaMarketingCreateComponent,
    AsignarCategoriaCamapanaComponent,
    AsignarMediosDifusionComponent,
    AgregarCamposComponent,
    AgregarNodoComponent,
    GuardadoComponent,
    ModificarDatosComponent,
    ActualizarPaginaComponent,
    VerConfiguracionComponent,
    ProveedoresLoadCreateComponent,
    AgregarAseguradoraComponent,
    DatosNodosComponent,
    AgregarCamposNodoComponent,
    ActualizarCamposNodoComponent,
    ActualizarCampanaComponent,
    ActualizarMediosDifusionComponent,
    ActualizarCampanaCategoriaComponent,
    AgregarTipoComponent,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: CampanaComponent,
    },
  ],
})
export class MarketingModule {
}
