import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Global} from '../global';
import {AreaService} from './area.service';

describe('Area servicios', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: AreaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = {
    id: 158,
    nombre: 'AREA TES cambio',
    idMarcaEmpresarial: 1,
    activo: 1,
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [AreaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(AreaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('la conexion es correcta de los servicios ', () => {
    const url1 = baseURL.split('//');
    const url2 = url1[0] + '//' + url1[1].split('/')[0];
    const request = new Request(url2);
    fetch(request, {mode: 'no-cors'}).then(function(response) {
      // Convert to JSON
      console.log('Response->', response.status);
    }).then(function(j) {
      // Yay, `j` is a JavaScript object
      console.log('String-->', JSON.stringify(j));
    }).catch(function(error) {
      console.log('Request failed', error);
      fail('No hay conexion a ' + url2);
    });
  });
  describe('funsiona servicio correctamente', () => {
    describe('get', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.get.toString());
        chai.request(url)
          .get(go)
          .end((err, res) => {
            console.log('AreaService GET 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion get debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.get);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('getAreaById', () => {
      it('status 200', (done) => {
        const url = validarFuncion();
        const go = validarFuncion(service.getAreaById.toString());
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        chai.use(chaiHttp);
        chai.request(url)
          .get(go + 1)
          .end((err, res) => {
            console.log('AreaService getAreaById 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getAreaById debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getAreaById);
              }
            done();
          });
        done();
        setTimeout(done, 500);
      });
    });
    describe('post', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.post.toString());
        chai.request(url)
          .post(go)
          .send(body)
          .end( (err, res) => {
            console.log('AreaService POST 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion post debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.post.toString());
              }
            if (res.status === 500) {
              fail(`La funcion post debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que alguno de los paramteros que se envian
               falta o es incorrecto '${body}'\n\n` + JSON.stringify(body));
            }
            /*expect(res.status).toBe(200);*/
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('put', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.put.toString());
        chai.request(url)
          .put(go + 158 + '/' + 74)
          .send(body)
          .end( (err, res) => { console.log('AreaService PUT 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion put debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.put.toString());
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
});
