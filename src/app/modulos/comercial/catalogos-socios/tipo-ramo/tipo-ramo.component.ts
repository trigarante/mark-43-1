import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {TipoRamo, TipoRamoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-ramo';
import {TipoRamoCreateComponent} from '../modals/tipo-ramo-create/tipo-ramo-create.component';
import {environment} from '../../../../../environments/environment';
import {Router} from "@angular/router";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-ramo',
  templateUrl: './tipo-ramo.component.html',
  styleUrls: ['./tipo-ramo.component.scss'],
})
export class TipoRamoComponent implements OnInit {
  cols: any[];
  tipoRamo: TipoRamo[];
  tipoRamos: TipoRamo;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // socket
  private stompClient = null;
  constructor(private tipoRamoServices: TipoRamoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tiporamo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoRamo', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoRamo(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
    // this.router.navigate(['/modulos/comercial/socios']);
  }
  getTipoRamo(mensajeAct) {
    this.tipoRamoServices.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.tipoRamo = data;
      this.dataSource = new MatTableDataSource(this.tipoRamo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Ramo`);
  }

  crearTipoRamo() {
    this.dialogService.open(TipoRamoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoRamo(0));
  }

  bajaTipoRamo(idTipoRamo) {
    this.getTipoRamoById(idTipoRamo);
    swal({
      title: '¿Deseas dar de baja este Tipo Ramo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoRamos.activo = 0;
        this.tipoRamoServices.put(idTipoRamo, this.tipoRamos).subscribe( result => {
          this.tipoRamoServices.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El Tipo Ramo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoRamo(0);
        }));
      }
    });
  }

  getTipoRamoById(idTipoRamo) {
    this.tipoRamoServices.getTipoRamoById(idTipoRamo).subscribe(data => {
      this.tipoRamos = data;
    });
  }

  updateTipoRamo(idTipoRamo) {
    this.dialogService.open(TipoRamoCreateComponent, {
      context: {
        idTipo: 2,
        idTipoRamo: idTipoRamo,
      },
    }).onClose.subscribe( x => this.getTipoRamo(1));
  }

  ngOnInit() {
    this.connect();
    this.getTipoRamo(0);
    this.cols = ['tipo', 'acciones'];
    //   {field: 'tipo', header: 'Tipo'},
    //   {field: 'acciones', header: 'Acciones'},
    // ];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoGiro.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoGiro.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
