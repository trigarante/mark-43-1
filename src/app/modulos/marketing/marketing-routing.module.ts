import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {MarketingComponent} from './marketing.component';
import {CampanaComponent} from './campana/campana.component';
import {PaginaComponent} from './pagina/pagina.component';
import {MediosDifusionComponent} from './medios-difusion/medios-difusion.component';
import {CampanaCategoriaComponent} from './campana-categoria/campana-categoria.component';
import {AseguradorasComponent} from './aseguradoras/aseguradoras.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: MarketingComponent,
  children: [
    {
      path: 'campana',
      component: CampanaComponent,
      canActivate: [AuthGuard],
      // data: { permiso: permisos.modulos.campana.activo},
    },
    {
      path: 'pagina',
      component: PaginaComponent,
      canActivate: [AuthGuard],
      // data: { permiso: permisos.modulos.pagina.activo},
    },
    {
      path: 'medios-difusion',
      component: MediosDifusionComponent,
      canActivate: [AuthGuard],
      // data: { permiso: permisos.modulos.medioDifusion.activo},
    },
    {
      path: 'camapana-categoria',
      component: CampanaCategoriaComponent,
      canActivate: [AuthGuard],
      // data: { permiso: permisos.modulos.campañaCategoria.activo},
    },
    {
      path: 'aseguradoras/:id',
      component: AseguradorasComponent,
    },
    {
      path: 'catalogos-marketing',
      loadChildren: './catalogos-marketing/catalogos-marketing.module#CatalogosMarketingModule',
    },
  ],
}];

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarketingRoutingModule {
}
