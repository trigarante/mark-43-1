import {Observable} from 'rxjs';

export interface ConductorHabitual {
  id: number;
  idPais: number;
  idRegistro: number;
  nombre: string;
  paterno: string;
  materno: string;
  correo: string;
  cp: number;
  idColonia: string;
  calle: string;
  numInt: string;
  numExt: string;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  fechaNacimiento: Date;
  fechaRegistro: Date;
}

export abstract class ConductorHabitualData {

  abstract get(): Observable<ConductorHabitual[]>;

  abstract post(Conductor): Observable<ConductorHabitual>;

  abstract getConductorById(idConductor): Observable<ConductorHabitual>;

  abstract put(idConductor, Conductor): Observable<ConductorHabitual>;

}
