import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Banco, BancoData} from '../../interfaces/catalogos/banco';
import {Observable} from 'rxjs';


@Injectable()
export class BancosService extends BancoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Banco[]> {
    return this.http.get<Banco[]>(this.baseURL + 'v1/bancos');
  }

  post(banco: Banco): Observable<Banco> {
    return this.http.post<Banco>(this.baseURL + 'v1/bancos', banco);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'bancos/saveBanco', json);
  }

  getBancoById(idBanco): Observable<Banco> {
    return this.http.get<Banco>(this.baseURL + 'v1/bancos/' + idBanco);
  }

  put(idBanco, banco: Banco): Observable<Banco> {
    return this.http.put<Banco>(this.baseURL + 'v1/bancos/' + idBanco + '/'
      + sessionStorage.getItem('Empleado'), banco);
  }
}
