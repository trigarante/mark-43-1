import { Component, OnInit } from '@angular/core';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Area, AreaData} from '../../../../@core/data/interfaces/catalogos/area';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {map} from 'rxjs/operators';
import {ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {ExtensionSaveComponent} from '../extension-save/extension-save.component';
import {Router} from '@angular/router';


@Component({
  selector: 'ngx-crear-extension',
  templateUrl: './crear-extension.component.html',
  styleUrls: ['./crear-extension.component.scss'],
})
export class CrearExtensionComponent implements OnInit {
  ExtensionesForm: FormGroup;
  /**arreglos de las area*/
  areas: any[];
  subareas: Area[];
  /** arrelos de las subareas*/
  area: any[];
  subarea: Subarea[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** variable que define true o false*/
  estadoExtensiones: boolean;
  menor: number ;
  menoruno: number;
  rango: boolean = false;
  rangoactivo: string;
  activarRango: boolean = false;
  constructor(protected ref: NbDialogRef<CrearExtensionComponent>, private fb: FormBuilder,
              private areaService: AreaData, private subareaService: SubareaData,  private extensionesService: ExtensionData,
              private toastrService: NbToastrService, public dialogService: NbDialogService,
              private router: Router) {
    this.ExtensionesForm = this.fb.group({
      'subareacontrol': new FormControl('', Validators.required),
      'areacontrol': new FormControl('', Validators.required),
      'extensionmenor': new FormControl('', Validators.compose([Validators.required,
        Validators.max(50000), Validators.min(0), Validators.maxLength(5)])),
      'extensionmayor': new FormControl('', Validators.compose([
        Validators.max(50000)])),
      'comentarios': new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.getArea();
  }

  /** consigue todas las areas */
  getArea() {
    this.areaService.get().pipe(map(data => this.subareas = data)).subscribe(data => {
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  /** consigue todas las subareas o campañas de un area de ejecutivo*/
  getsubareas(event) {
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.area = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.area.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});

        // this.empleado = null;
      }
    });
    // this.tipoarea = event;
  }
  conseguirRango() {

    this.menoruno = this.menor + 1;
    this.rango = true;
    this.activarRango = !this.activarRango;
    if ( this.activarRango === false) {
      this.ExtensionesForm.controls['extensionmenor'].setValue(0 );
      this.ExtensionesForm.controls['extensionmayor'].setValue(this.ExtensionesForm.controls['extensionmenor'].value );
      this.ExtensionesForm.controls['extensionmayor'].setValidators([Validators.min(this.menor)]);

    }
    if (this.activarRango === true) {
      this.ExtensionesForm.controls['extensionmayor'].setValue(this.ExtensionesForm.controls['extensionmenor'].value + 1);
      this.ExtensionesForm.controls['extensionmayor'].setValidators(Validators.compose([Validators.min(this.menoruno ),
        Validators.required, Validators.max(50000) ] ));

    }
    Validators.max(50000);
  }
  getmenor(event) {
    this.menor = Number.parseInt(event);
    this.menoruno = this.menor + 1;
    if ( this.activarRango === false) {
      this.ExtensionesForm.controls['extensionmayor'].setValue(this.ExtensionesForm.controls['extensionmenor'].value );
      this.ExtensionesForm.controls['extensionmayor'].setValidators([Validators.min(this.menor)]);

    }

    if (this.activarRango === true) {
      this.ExtensionesForm.controls['extensionmayor'].setValue(this.ExtensionesForm.controls['extensionmenor'].value + 1);
      this.ExtensionesForm.controls['extensionmayor'].setValidators(Validators.compose([Validators.min(this.menoruno ),
        Validators.required, Validators.max(50000) ] ));
    }
  }
  /** metodo para guardar las extensiones por rango, si ya existe ese rango no guardara y mandara mensaje de extensiones
   * ocupadas */
  guardar() {
    this.submitted = true;

    if (this.ExtensionesForm.invalid) {
      return;
    }
    const objetoExtension = {
      'idSubarea': this.ExtensionesForm.value.subareacontrol,
      'idEstado' : 0,
      'descrip' : this.ExtensionesForm.value.comentarios,
    };

    this.extensionesService.post(objetoExtension, this.ExtensionesForm.value.extensionmenor,
      this.ExtensionesForm.value.extensionmayor).subscribe(response => {
        this.estadoExtensiones = response;
      if (this.estadoExtensiones === false) {
        this.showToast('danger');
      }else if (this.estadoExtensiones === true) {

        this.dialogService.open(ExtensionSaveComponent, {});
        this.extensionesService.postSocket(this.ExtensionesForm.value).subscribe(x => {});
        this.router.navigate(['modulos/ti/extensiones']);
        this.ref.close();
      }
    });
  }
  showToast(status) {
    this.toastrService.danger('Atencion', `Extensiones ya ocupadas en otra area`);
  }
}
