import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CatalogosSociosComponent} from './catalogos-socios.component';
import {CommonModule} from '@angular/common';
import {TipoCompetenciaComponent} from './tipo-competencia/tipo-competencia.component';
import {TipoProductoComponent} from './tipo-producto/tipo-producto.component';
import {TipoCoberturaComponent} from './tipo-cobertura/tipo-cobertura.component';
import {BajaSociosComponent} from './baja-socios/baja-socios.component';
import {SubCategoriaComponent} from './sub-categoria/sub-categoria.component';
import {DivisasComponent} from './divisas/divisas.component';
import {TipoRamoComponent} from './tipo-ramo/tipo-ramo.component';
import {TipoSubramoComponent} from './tipo-subramo/tipo-subramo.component';
import {CategoriaComponent} from './categoria/categoria.component';
import {AuthGuard} from '../../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: CatalogosSociosComponent,
  children: [
    {
      path: 'tipo-ramo',
      component: TipoRamoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoGiro.activo},
    },
    {
      path: 'tipo-sub-ramo',
      component: TipoSubramoComponent,
    },
    {
      path: 'tipo-competencia',
      component: TipoCompetenciaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoCompetencia.activo},
    },
    {
      path: 'tipo-producto',
      component: TipoProductoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoProducto.activo},
    },
    {
      path: 'divisas',
      component: DivisasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.divisas.activo},
    },
    {
      path: 'categoria',
      component: CategoriaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.categoria.activo},
    },
    {
      path: 'sub-categoria',
      component: SubCategoriaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.subCategoria.activo},
    },
    {
      path: 'tipo-cobertura',
      component: TipoCoberturaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoCobertura.activo},
    },
    {
      path: 'bajas-socios',
      component: BajaSociosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajasSocios.activo},
    },
  ],
}];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogosSociosRoutingModule {
}
