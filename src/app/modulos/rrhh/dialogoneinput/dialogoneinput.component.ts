import {Component, OnInit, Inject, Output} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {DialogOneInputData} from '../../../@core/data/interfaces/drive/dialog-one-input-data';



@Component({
    selector: 'ngx-dialogoneinput',
    templateUrl: './dialogoneinput.component.html',
    styleUrls: ['./dialogoneinput.component.css'],

})
export class DialogOneInputComponent implements OnInit {
  @Output()
    name: string;
    constructor(
        public dialogRef: MatDialogRef<DialogOneInputComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogOneInputData,
    ) {
        this.name = data.DefaultInputText;
    }

    ngOnInit(): void {
    }

    cancel(): void {
        this.dialogRef.close();
    }
}
