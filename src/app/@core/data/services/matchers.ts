// @ts-ignore
import CustomMatcherFactories = jasmine.CustomMatcherFactories;
// @ts-ignore
import CustomMatcher = jasmine.CustomMatcher;
// @ts-ignore
import CustomMatcherResult = jasmine.CustomMatcherResult;
import {environment} from '../../../../environments/environment';
export const CustomMatchers: CustomMatcherFactories = {
  toBeGreaterThan: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        if (actual > expected) {
          return {
            pass: true,
            message: `Se esperaba que ${actual} sea menor que ${expected}`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que ${actual} sea mayor que ${expected}`,
          };
        }
      },
    };
  },
  toBeGreaterThanOrEqual: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        if (actual >= expected) {
          return {
            pass: true,
            message: `test is passed`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que ${actual} sea mayor que ${expected}`,
          };
        }
      },
    };
  },
  toEqual: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        if (actual === expected) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba el valor ${expected} pero se recibio ${actual}`,
          };
        }
      },
    };
  },
  toBe: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        if (actual === expected) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba el valor ${expected} pero se recibio ${actual}`,
          };
        }
      },
    };
  },
  toBePositiveInfinity: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (actual === Number.POSITIVE_INFINITY) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} sea un numero real infinito`,
          };
        }
      },
    };
  },
  toBeTruthy: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (!!actual) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} no puede ser undefined o false, y debe se un valor inicializado o existente`,
          };
        }
      },
    };
  },
  toBeUndefined: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (void 0 === actual) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba qu eel valor actual ${actual}, debe ser ${void 0}`,
          };
        }
      },
    };
  },
  toContain: (util, customEqualityTesters): CustomMatcher => {
    const customEqualityTester = customEqualityTesters || [];
    return {
      compare: (actual, expected) => {
        if (util.contains(actual, expected, customEqualityTester)) {
          return {
            pass: true,
            message: 'test correct',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba qu el valor actual ${expected}, de ser parte de ${actual}`,
          };
        }
      },
    };
  },
  toMatch: (): CustomMatcher => {
    return {
      compare: (actual, expected) => {
        if (String(actual) && (expected instanceof RegExp)) {
          const regexp = new RegExp(expected);
          return {
            pass: regexp.test(actual),
            message: `No existe ninguna conicdencia entre el valor actual ${actual} y el valor esperado ${expected}`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${expected} sea un String o una expresion RegExp`,
          };
        }
      },
    };
  },
  toBeDefined: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (void 0 !== actual) {
          return {
            pass: true,
            message: `test passed`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} sea un valor definido`,
          };
        }
      },
    };
  },
  toBeNull: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (actual === null) {
          return {
            pass: true,
            message: `test passed`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} sea null`,
          };
        }
      },
    };
  },
  toBeNaN: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (actual !== actual) {
          return {
            pass: true,
            message: `test passed`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} sea NaN`,
          };
        }
      },
    };
  },
  toBeFalsy: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (!!!actual) {
          return {
            pass: true,
            message: `test passed`,
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor ${actual} sea false`,
          };
        }
      },
    };
  }, /*
  toHaveBeenCalled: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if (actual.isSpy()) {
          return {
            pass: actual.calls.any(),
            message: actual.calls.any() ?
              'Expected spy ' + actual.and.identity + ' no ha sido llamado.' :
              'Expected spy ' + actual.and.identity + ' ha sido llamado.'
          };
        } else if (!(actual as any).isSpy(actual)) {
          return {
            pass: false,
            message: `Se esperaba un spy, pero se recibio un ${actual}`
          };
        } else if (this.arguments.length > 1) {
          return {
            pass: false,
            message: `No se puede agregar argumentos, mejor usa toHaveBeenCalledWith`
          };
        }
      }
    };
  },*/
  toBeLessThan: (): CustomMatcher => {
    return {
      compare: (actual, expected) => {
        if (actual < expected) {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que ${actual} sea menor que ${expected}`,
          };
        }
      },
    };
  },
  toBeNumber: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if ((typeof actual) === 'number') {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor actual ${actual} sea de tipo number`,
          };
        }
      },
    };
  },
  toBeString: (): CustomMatcher => {
    return {
      compare: (actual) => {
        if ((typeof actual) === 'string') {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que el valor actual ${actual} sea de tipo string`,
          };
        }
      },
    };
  },
  toBeLessThanOrEqual: (): CustomMatcher => {
    return {
      compare: (actual, expected) => {
        if (actual <= expected) {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que ${actual} sea menor que ${expected}`,
          };
        }
      },
    };
  },
  toBeValidArgument: (): CustomMatcher => {
    return {
      compare: (actual, expected) => {
        if (actual === expected) {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que la funcion reciba un de parametro tipo ${expected}`,
          };
        }
      },
    };
  },
  toBeConection: (): CustomMatcher => {
    return {
      compare: (actual, url) => {
        if (actual.toString() === 'OnLine') {
          return {
            pass: true,
            message: `Sin Conexion a ${url}`,
          };
        }
        if (actual.toString() === 'Offline') {
          return {
            pass: false,
            message: `Sin Conexion a \n\n ${url}\n\n\nVerifica que:\n\n-El nombre de dominio sea correcto.\n-El puerto sea correcto\n-La ruta sea correcta\n\n`,
          };
        }
        if (actual.toString() === '0') {
          return {
            pass: false,
            message: `La ruta es incorrecta\n\nRuta : /${url.split('//')[1].split('/')[1]}/\n\n${url}\n\n`,
          };
        }
        if (actual.toString() === 'OfflineInternet') {
          return {
            pass: false,
            message: `Sin Conexion a Internet\n\n\n\n\n\n`,
          };
        }
      },
    };
  },
  toBeStatusGet: (): CustomMatcher => {
    return {
      // @ts-ignore
      compare: (body, url, ruta) => {
        const id = body[1].id;
        // @ts-ignore
        const $ = require('jquery');
        $.ajax({
          type: 'GET',
          url: url.toString() + ruta,
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          cache: true,
          timeout: 5000,
          async: false,
          success: (data) => {
          },
          complete: (xhr, textStatus) => {
            localStorage.setItem('getCode' + id, xhr.status);
          },
          error: (response) => {
          },
        });
        if (localStorage.getItem('getCode' + id) === '200') {
          return {
            pass: true,
            message: 'test passed',
          };
        }
        if (url !== environment.GLOBAL_SERVICIOS) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${url}\nVerifica los siguientes errores\n
\t1.- La ruta debe ser ${environment.GLOBAL_SERVICIOS}
            \nStatus: ${localStorage.getItem('getCode' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (ruta.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- Faltan parametros en la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (ruta.split('/').length > 2) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('getCode' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '400') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n
\t2.- Es posible que tenga un parametro que no es necesario\n\nStatus: ${localStorage.getItem('getCode' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '401') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene un parametro incorrecto
            \nStatus: ${localStorage.getItem('getCode' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '405') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- Estas enviando parametros que no son necesarios\n\nStatus: ${localStorage.getItem('getCode' + id)}
            \n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '404') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n
\t2.- Es posible que tenga un parametro que no es necesario\n\nStatus: ${localStorage.getItem('getCode' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '415') {
          return {
            pass: false,
            message: `Formato de datos no soportado\nStatus: ${localStorage.getItem('getCode' + id)}\nBody: \n${JSON.stringify(body[0])}`,
          };
        }
        if (localStorage.getItem('getCode' + id) === '500') {
          return {
            pass: false,
            message: `El idEmpelado es incorrecto\n\nStatus: ${localStorage.getItem('getCode' + id)}
            \n\nRuta: ${url + ruta}\n\nBody: `,
          };
        }
        if (localStorage.getItem('getCode' + id) === '0') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta contiene parameros incorrectos\n\nStatus: ${localStorage.getItem('getCode' + id)}
            \n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
          localStorage.clear();
        }
        return {
          pass: true,
          message: 'test passed',
        };
      },
    };
  },
  toBeStatusGetById: (): CustomMatcher => {
    return {
      // @ts-ignore
      compare: (body, url, ruta, options = '') => {
        const id = body[1].id;
        // @ts-ignore
        const $ = require('jquery');
        $.ajax({
          type: 'GET',
          url: url.toString() + ruta + options,
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          cache: true,
          timeout: 5000,
          async: false,
          success: (data) => {
          },
          complete: (xhr, textStatus) => {
            localStorage.setItem('getCodeById' + id, xhr.status);
          },
          error: (response) => {
          },
        });
        if (localStorage.getItem('getCodeById' + id) === '200') {
          return {
            pass: true,
            message: 'test passed',
          };
          localStorage.clear();
        }
        if (url !== environment.GLOBAL_SERVICIOS) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${url}\nVerifica los siguientes errores\n
\t1.- La ruta debe ser ${environment.GLOBAL_SERVICIOS}
            \nStatus: ${localStorage.getItem('getCodeById' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (ruta.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Faltan parametros en la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (options.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Faltan parametros al final de la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (ruta.split('/').length > 2) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('getCodeById' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (options.split('/').length > 2) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('getCodeById' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCodeById' + id) === '400') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n
\t2.- Es posible que tenga un parametro que no es necesario\n\nStatus: ${localStorage.getItem('getCodeById' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCodeById' + id) === '401') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene un parametro incorrecto
            \nStatus: ${localStorage.getItem('getCodeById' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCodeById' + id) === '404') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n
\t2.- Es posible que tenga un parametro que no es necesario\n\nStatus: ${localStorage.getItem('getCodeById' + id)}\n
            \nRuta: ${url + ruta + options}\n\nBody: \n\n\n`,
          };
          localStorage.clear();
        }
        if (localStorage.getItem('getCodeById' + id) === '405') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Estas enviando parametros que no son necesarios\n\nStatus: ${localStorage.getItem('getCodeById' + id)}
            \n\nRuta: ${url + ruta + options}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('getCodeById' + id) === '415') {
          return {
            pass: false,
            message: `Formato de datos no soportado\n\nStatus: ${localStorage.getItem('getCodeById' + id)}\n
            \nRuta: ${url + ruta + options}\n\nBody: \n\n\n\n\n`,
          };
          localStorage.clear();
        }
        if (localStorage.getItem('getCodeById' + id) === '500') {
          return {
            pass: false,
            message: `Atención, faltan datos por enviar\n\nStatus: ${localStorage.getItem('getCodeById' + id)}\n
            \nBody: \n${JSON.stringify(body[0])}\n\n\n\n\n\n`,
          };
          localStorage.clear();
        }
        if (localStorage.getItem('getCodeById' + id) === '0') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta contiene parameros incorrectos\n\nStatus: ${localStorage.getItem('getCodeById' + id)}
            \n\nRuta: ${url + ruta + options}\n\nBody: \n\n\n`,
          };
          localStorage.clear();
        }
        return {
          pass: true,
          message: 'test passed',
        };
      },
    };
  },
  toBeStatusPost: (): CustomMatcher => {
    return {
      // @ts-ignore
      compare: (body, url, ruta) => {
        const id = body[1].id;
        // @ts-ignore
        const $ = require('jquery');
        $.ajax({
          type: 'POST',
          url: url.toString() + ruta,
          data: JSON.stringify(body[0]),
          dataType: 'json',
          context: document.body,
          contentType: 'application/json; charset=utf-8',
          cache: true,
          async: false,
          success: (data) => {
          },
          complete: (xhr, textStatus) => {
            localStorage.setItem('statusPost' + id, xhr.status);
          },
          error: (response) => {
          },
        });
        if (localStorage.getItem('statusPost' + id) === '200') {
          return {
            pass: true,
            message: 'test passed',
          };
        }
        if (url !== environment.GLOBAL_SERVICIOS) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${url}\nVerifica los siguientes errores\n
\t1.- La ruta debe ser ${environment.GLOBAL_SERVICIOS}
            \nStatus: ${localStorage.getItem('statusPost' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (ruta.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- Faltan parametros en la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (ruta.split('/').length > 2) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('statusPost' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n\n`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '400') {
          return {
            pass: false,
            message: `Este mensaje se debe a alguno de los siguientes errores:\nVerifica\n
\t1.- Es probable que el nombre de algun parametro no sea válido en esta parte de la ruta: ${ruta}\n
\t3.- Es probable que en el body que se esta enviando tenga valores incorrectos\n\nStatus: ${localStorage.getItem('statusPost' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '401') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene un parametro incorrecto
            \nStatus: ${localStorage.getItem('statusPost' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '404') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n
\t2.- Es posible que tenga un parametro que no es necesario\n\nStatus: ${localStorage.getItem('statusPost' + id)}\n
            \nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
          localStorage.clear();
        }
        if (localStorage.getItem('statusPost' + id) === '405') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- Estas enviando parametros que no son necesarios\n\nStatus: ${localStorage.getItem('statusPost' + id)}
            \n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '415') {
          return {
            pass: false,
            message: `Formato de datos no soportado\n\nStatus: ${localStorage.getItem('statusPost' + id)}Ruta: ${url + ruta}\n
            \nBody: \n${JSON.stringify(body[0])}`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '500') {
          return {
            pass: false,
            message: `Verifica los siguientes errores\n
\t1.- El body que se esta enviando no tiene todos los datos necesarios\n
            \nStatus: ${localStorage.getItem('statusPost' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPost' + id) === '0') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta contiene parameros incorrectos\n\nStatus: ${localStorage.getItem('statusPost' + id)}
            \n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
          localStorage.clear();
        }
        return {
          pass: true,
          message: 'test passed',
        };
      },
    };
  },
  toBeStatusPut: (): CustomMatcher => {
    return {
      // @ts-ignore
      compare: (body, url, ruta, options) => {
        const id = body[1].id;
        // @ts-ignore
        const $ = require('jquery');
        $.ajax({
          type: 'PUT',
          url: url.toString() + ruta + options,
          data: JSON.stringify(body[0]),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          cache: true,
          timeout: 5000,
          async: false,
          success: (data) => {
          },
          complete: (xhr, textStatus) => {
            localStorage.setItem('statusPut' + id, xhr.status);
          },
          error: (response) => {
          },
        });
        if (localStorage.getItem('statusPut' + id) === '200') {
          return {
            pass: true,
            message: 'test passed',
          };
        }
        if (url !== environment.GLOBAL_SERVICIOS) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${url}\nVerifica los siguientes errores\n
\t1.- La ruta debe ser ${environment.GLOBAL_SERVICIOS}
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (ruta.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Faltan parametros en la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (options.length === 0) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Faltan parametros al final de la ruta
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (ruta.split('/').length > 2) {
          console.log(options.split('/'));
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (options.split('/').length > 3) {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta tiene parametros que no son necesarios
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPut' + id) === '400') {
          return {
            pass: false,
            message: `Este mensaje se debe a alguno de los siguientes errores:\nVerifica\n
\t1.- Es probable que el nombre de algun parametro no sea válido en esta parte de la ruta: ${ruta + options}\n
\t3.- Es probable que en el body que se esta enviando tenga valores incorrectos\n\n\nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPut' + id) === '401') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta ${ruta}\nVerifica los siguientes errores\n
\t1.- La ruta tiene un parametro incorrecto
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPut' + id) === '404') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Es probable que el nombre de algun parametro no sea válido.\n\nStatus: ${localStorage.getItem('statusPut' + id)}\n
            \nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
          localStorage.clear();
        }
        if (localStorage.getItem('statusPut' + id) === '405') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- Estas enviando parametros que no son necesarios\n\nStatus: ${localStorage.getItem('statusPut' + id)}
            \n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPut' + id) === '500') {
          return {
            pass: false,
            message: `Este mensaje pude deberse a alguno de los siguientes errores\nVerifica lo siguiente\n
\t1.- El body que se esta enviando no tiene todos los datos necesarios\n
\t2.- Se esta enviando un parametro incorrecto en : ${ruta + options}
            \nStatus: ${localStorage.getItem('statusPut' + id)}\n\nRuta: ${url + ruta + options}\n\nBody: \n${JSON.stringify(body[0])}\n\n\n\n`,
          };
        }
        if (localStorage.getItem('statusPut' + id) === '0') {
          return {
            pass: false,
            message: `Esta parte de la ruta es incorrecta: ${ruta + options}\nVerifica los siguientes errores\n
\t1.- La ruta contiene parameros incorrectos\n\nStatus: ${localStorage.getItem('statusPut' + id)}
            \n\nRuta: ${url + ruta + options}\n\nBody: \n\n${JSON.stringify(body[0])}\n\n\n`,
          };
          localStorage.clear();
        }
        return {
          pass: true,
          message: 'test passed',
        };
      },
    };
  },
  toBeCloseTo: (): CustomMatcher => {
    return {
      // @ts-ignore
      compare: (actual: any, expected: any, precision) => {
        if (precision !== 0) {
          precision = precision || 2;
        }
        if (expected === null || actual === null) {
          throw new Error('No puede hacer uso de toBeCloseTo con null. Verifique sus argumetos: \n' +
            'expect(' + actual + ').toBeCloseTo(' + expected + ') :(',
          );
        }
        const pow = Math.pow(10, precision + 1);
        const delta = Math.abs(expected - actual);
        const maxDelta = Math.pow(10, -precision) / 2;
        if (Math.round(delta * pow) / pow <= maxDelta) {
          return {
            pass: true,
            message: 'test passed',
          };
        } else {
          return {
            pass: false,
            message: `Se esperaba que ${actual} conicida con ${expected} hasta el ${precision}° decimal`,
          };
        }
      },
    };
  }, /*
  toThrow: (util): CustomMatcher => {
    return {
      compare: (actual, expected) => {
        // tslint:disable-next-line:one-variable-per-declaration prefer-const
        let result = { pass: false , message: ''}, threw = false, thrown;
        if (typeof actual !== 'function') {
          throw new Error('Actual no es una funcion :(');
        }

        try {
          actual();
        } catch (e) {
          threw = true;
          thrown = e;
        }
        if (!threw) {
          return {
            pass: false,
            message: 'Se espera que la función lance una excepción.'
          };
        }
        if (this.arguments.length === 1) {
          return {
            pass: true,
            message: 'Se esperaba que la funcion lanzara un excepcion, pero lanzo ....'
          };
        }
        if (util.equals(thrown, expected)) {
          return {
            pass: true,
            message: 'La funcion no lanzo lo esperado.'
          };
        } else {
          return {
            pass: true,
            message: 'La funcion lanzo una excepcion, pero .....'
          };
        }
      }
      };
  },*/
  toBeFirstNameEqualTo: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        const firstName = actual.split(' ')[0];
        if (firstName === expected) {
          return {
            pass: true,
            message: `test is passed`,
          };
        } else {
          return {
            pass: false,
            message: `test fails`,
          };
        }
      },
    };
  }, /*
  toHaveClass: (): CustomMatcher => {
    function isElement(maybeEl) {
      return maybeEl &&
        maybeEl.classList && typeof maybeEl.classList.contains === 'function';
    }
    return {
      compare: (actual, expected): CustomMatcherResult => {
        if (!isElement(actual)) {
          throw new Error(' is not a DOM element');
        }
        if (actual.classList.contains(expected)) {
          return {
            pass: actual.classList.contains(expected),
            message: `test is passed`
          };
        }
      }
    };
  },*/
  toBeLastNameEqualTo: (): CustomMatcher => {
    return {
      compare: (actual: any, expected: any): CustomMatcherResult => {
        const lastName = actual.split(' ')[1];
        if (lastName === expected) {
          return {
            pass: true,
            message: `test is passed`,
          };
        } else {
          return {
            pass: false,
            message: `test fails`,
          };
        }
      },
    };
  },
};

declare global {
  namespace jasmine {
    interface Matchers<T> {
      toBe(expected: any, expectationFailOutput?: any): boolean;

      toEqual(expected: any, expectationFailOutput?: any): boolean;

      toBeDefined(expectationFailOutput?: any): boolean;

      toBeUndefined(expectationFailOutput?: any): boolean;

      toBeNull(expectationFailOutput?: any): boolean;

      toBeNaN(): boolean;

      toBeNumber(): boolean;

      toBeValidArgument(expected: string): boolean;

      toBeConection(expected: string): boolean;

      toBeStatusGet(url: any, ruta: any): boolean;

      toBeStatusGetById(url: any, ruta: any, options?: any): boolean;

      toBeStatusPost(url: any, ruta: any): boolean;

      toBeStatusPut(url: any, ruta: any, options: any): boolean;

      toBeString(): boolean;

      toBeTruthy(expectationFailOutput?: any): boolean;

      toBeFalsy(expectationFailOutput?: any): boolean;

      /*toHaveBeenCalled(): boolean;*/
      toContain(expected: any, expectationFailOutput?: any): boolean;

      toBeLessThan(expected: number, expectationFailOutput?: any): boolean;

      toBeLessThanOrEqual(expected: number, expectationFailOutput?: any): boolean;

      toBeGreaterThan(expected: any, expectationFailOutput?: any): boolean;

      toBeGreaterThanOrEqual(expected: number, expectationFailOutput?: any): boolean;

      toBeCloseTo(expected: number, precision?: any, expectationFailOutput?: any): boolean;

      /*toThrow(expected?: any): boolean;*/
      toBePositiveInfinity(expected: any, expectationFailOutput?: any): boolean;

      toMatch(expected: any, expectationFailOutput?: any): boolean;

      toBeFirstNameEqualTo(expected: any, expectationFailOutput?: any): boolean;

      toBeLastNameEqualTo(expected: any, expectationFailOutput?: any): boolean;

      /*toHaveClass(expected: any, expectationFailOutput?: any): boolean;*/
    }
  }
}

export function Conexion(url, identificador, consola = false) {
  if (navigator.onLine) {
    // @ts-ignore
    const $ = require('jquery');
    $.ajax({
      url: url.toString() + 'v1/vacantes/1',
      context: document.body,
      cache: true,
      timeout: 900,
      async: false,
      error: (jqXHR, exception) => {
        localStorage.setItem('status' + identificador, 'Offline');
        if (jqXHR.status === 0) {
          localStorage.setItem('status' + identificador, jqXHR.status);
        }
        if (consola === true) {
          console.log(localStorage.getItem('status' + identificador));
        }
      },
      success: (data) => {
        localStorage.setItem('status' + identificador, 'OnLine');
        if (consola === true) {
          console.log(localStorage.getItem('status' + identificador));
        }
      },
    });
  } else {
    localStorage.setItem('status' + identificador, 'OfflineInternet');
    if (consola === true) {
      console.log(localStorage.getItem('status' + identificador));
    }
  }
}
export const link = 'http://localhost:8090/mark43-service/';


