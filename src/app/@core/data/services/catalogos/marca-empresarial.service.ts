import {Injectable} from '@angular/core';
import {MarcaEmpresarial, MarcaEmpresarialData} from '../../interfaces/catalogos/marca-empresarial';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class MarcaEmpresarialService extends MarcaEmpresarialData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<MarcaEmpresarial[]> {
    return this.http.get<MarcaEmpresarial[]>(this.baseURL + 'v1/marca-empresarial');
  }

  post(marcaEmpresarial: MarcaEmpresarial): Observable<MarcaEmpresarial> {
    return this.http.post<MarcaEmpresarial>(this.baseURL + 'v1/marca-empresarial', marcaEmpresarial);
  }

  getMarcaEmpresarialById(idMarcaEmpresarial): Observable<MarcaEmpresarial> {
    return this.http.get<MarcaEmpresarial>(this.baseURL + 'v1/marca-empresarial/' + idMarcaEmpresarial);
  }

  put(idMarcaEmpresarial, marcaEmpresarial: MarcaEmpresarial): Observable<MarcaEmpresarial> {
    return this.http.put<MarcaEmpresarial>(this.baseURL + 'v1/marca-empresarial/' + idMarcaEmpresarial, marcaEmpresarial);
  }
}
