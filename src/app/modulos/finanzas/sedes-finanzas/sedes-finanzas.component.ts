import {Component, OnInit, ViewChild} from '@angular/core';
import {Sede, SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SedeCreateComponent} from '../../rrhh/catalogos/modals/sede-create/sede-create.component';
import {CreateAreaComponent} from '../modals/create-area/create-area.component';

@Component({
  selector: 'ngx-sedes-finanzas',
  templateUrl: './sedes-finanzas.component.html',
  styleUrls: ['./sedes-finanzas.component.scss'],
})
export class SedesFinanzasComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  sede: Sede[];
  dataSource: any;
  permisos: any;
  escritura: boolean = false;

  /**
   * En este caso: Servicio de Puesto.
   * @param {SedeData} sedeService Servicio del componente sede para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes (diálogos). */
  constructor(
    private sedeService: SedeData, private dialogoService: NbDialogService,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene las sedes y las asigna al arreglo sede. */
  getSede() {
    this.sedeService.get().subscribe(data => {
      this.sede = [];
      this.sede = data;
      this.dataSource = new MatTableDataSource(this.sede); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  /** Función que despliega un modal para crear una Sede. */
  crearSede() {
    this.dialogoService.open(SedeCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getSede();
    });
  }

  /** Función que despliega un modal para actualizar la información de una Sede. */
  updaSede(idSede) {
    this.dialogoService.open(SedeCreateComponent, {
      context: {
        idTipo: 2,
        idSede: idSede,
      },
    }).onClose.subscribe(x => {
      this.getSede();
    });
  }

  crearArea(idSede) {
    this.dialogoService.open(CreateAreaComponent, {
      context: {
        idTipo: 1,
        idSede: idSede,
      },
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.catalogos.escritura;
  }

  ngOnInit() {
    this.getSede();
    this.cols = ['detalle', 'idPais', 'nombre', 'cp', 'acciones'];
    this.getPermisos();
  }

}
