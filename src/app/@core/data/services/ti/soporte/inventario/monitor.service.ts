import { Injectable } from '@angular/core';
import {Monitor, MonitorDate} from '../../../../interfaces/ti/soporte/inventario/monitor';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class MonitorService extends MonitorDate {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Monitor[]> {
    return this.http.get<Monitor[]>( this.baseURL + 'v1/monitor');
  }
  post(monitor: Monitor): Observable<Monitor> {
    return this.http.post<Monitor>(this.baseURL + 'v1/monitor', monitor);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'monitor/saveMonitor', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajamonitor/saveBajaMonitor', json);
  }
  getMonitorById(idMonitor): Observable<Monitor> {
    return this.http.get<Monitor>(this.baseURL + 'v1/monitor/' + idMonitor);
  }
  put(idMonitor, monitor: Monitor): Observable<Monitor> {
    return this.http.put<Monitor>(this.baseURL + 'v1/monitor/' + idMonitor + '/'
      + sessionStorage.getItem('Empleado'), monitor);
  }
}
