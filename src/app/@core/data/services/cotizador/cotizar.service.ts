import {Injectable} from '@angular/core';
import {CotizarData, CotizarRequest, CotizarResponse} from '../../interfaces/cotizador/cotizar';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Catalogo} from '../../interfaces/cotizador/catalogo';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class CotizarService extends CotizarData {

  constructor(private http: HttpClient) {
    super();
  }

  cotizar(c: CotizarRequest): Observable<Object> {
    return this.http.post<CotizarResponse>(
      `${environment.CATALOGO_AUTOS}/cotizar`, c);
  }
}
