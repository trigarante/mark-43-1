import {getTestBed, TestBed} from '@angular/core/testing';
import {PreempleadosService} from './preempleados.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Global} from '../global';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Preempleados Service', () => {
  const baseURL = Global.url;
  let service: PreempleadosService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
  },
    {
      id: 'preempleado',
    }];

  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [PreempleadosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(PreempleadosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/empleados/pre-empleado');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/empleados/pre-empleado');
      }); /*
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/empleados/pre-empleado/209/6/1564159920811');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/empleados/pre-empleado', '/89/1008136800000/74');
      });*/
    });
  }
});
