export interface IconInterface {
  repositorio: string;
  name: string;
  checked: boolean;
}
