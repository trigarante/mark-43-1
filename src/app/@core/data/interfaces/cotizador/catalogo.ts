import {Observable} from 'rxjs';


export class Catalogo {
  id: string;
  text: string;
}

export class CatalogoPorAseguradora {
  id: string;
  text: string;
  aseguradora: string;
}

export class Auto {
  aseguradora: string = '';
  marca: Catalogo;
  modelo: Catalogo;
  descripcion: Catalogo;
  subdescripcion: Catalogo;
  detalle: Catalogo;
  edad: string;
  genero: string;
  descuento: string;



}

export abstract class CatalogoData {

  abstract getMarcas(aseguradora?: string): Observable<Catalogo[]>;

  abstract getModelos(aseguradora: string, marca: string): Observable<Catalogo[]>;

  abstract getDescripcion(aseguradora: string, marca: string, modelo: string): Observable<Catalogo[]>;

  abstract getSubdescripcion(aseguradora: string, marca: string, modelo: string, descripcion: string): Observable<Catalogo[]>;

  abstract getDetalles(aseguradora: string, marca: string, modelo: string, descripcion: string, subdescripcion: string)
    : Observable<Catalogo[]>;

  abstract getDetallesHomologado(marca: string, modelo: string, descripcion: string, subdescripcion: string)
    : Observable<CatalogoPorAseguradora[]>;


}
