import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VentaNuevaRoutingModule} from './venta-nueva-routing.module';
import {VentaNuevaComponent} from './venta-nueva.component';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {
  NbAlertModule,
  NbDatepickerModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbTooltipModule,
} from '@nebular/theme';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule, MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule, MatOptionModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import {SolicitidesVnComponent} from './solicitides-vn/solicitides-vn.component';
import {ProspectoCreateComponent} from './modals/prospecto-create/prospecto-create.component';
import { ProductoSolicitudCreateComponent } from './modals/producto-solicitud-create/producto-solicitud-create.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { LlamadasComponent } from './modals/llamadas/llamadas.component';
import { LlamadasMyccComponent } from './modals/llamadas-mycc/llamadas-mycc.component';
import { LlamdasAgendadasComponent } from './llamdas-agendadas/llamdas-agendadas.component';
import { HistorialLlamadasComponent } from './historial-llamadas/historial-llamadas.component';
import { EmisionesComponent } from './emisiones/emisiones.component';
import { EstadoSolicitudComponent } from './modals/estado-solicitud/estado-solicitud.component';
import { CreateEmisionComponent } from './modals/create-emision/create-emision.component';
import { DatosProductoComponent } from './modals/datos-producto/datos-producto.component';
import { RegistroPolizaComponent } from './modals/registro-poliza/registro-poliza.component';
import { AlertComponent } from './modals/alert/alert.component';
import { StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import { PagosComponent } from './pagos/pagos.component';
import { RecibosComponent } from './recibos/recibos.component';
import { InspeccionesComponent } from './inspecciones/inspecciones.component';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ClienteComponent } from './cliente/cliente.component';

@NgModule({
  declarations: [
    VentaNuevaComponent,
    SolicitidesVnComponent,
    ProspectoCreateComponent,
    ProductoSolicitudCreateComponent,
    CotizadorComponent,
    DatosSolicitudComponent,
    LlamadasComponent,
    LlamadasMyccComponent,
    LlamdasAgendadasComponent,
    HistorialLlamadasComponent,
    EmisionesComponent,
    EstadoSolicitudComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    RegistroPolizaComponent,
    // FilesUploadComponent,
    AlertComponent,
    StepCotizadorComponent,
    PagosComponent,
    RecibosComponent,
    InspeccionesComponent,
    ClienteComponent,
  ],
  imports: [
    CommonModule,
    VentaNuevaRoutingModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    ThemeModule,
    MatTabsModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    NbTooltipModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatStepperModule,
    MatSlideToggleModule,
    KeyFilterModule,
  ],
  entryComponents: [
    ProspectoCreateComponent,
    ProductoSolicitudCreateComponent,
    DatosSolicitudComponent,
    LlamadasMyccComponent,
    // CreateEmisionComponent,
    EstadoSolicitudComponent,
    // DatosProductoComponent,
    // RegistroPolizaComponent,
    AlertComponent,
    // StepCotizadorComponent,
  ],
})
export class VentaNuevaModule {
}
