import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as d3Select from 'd3-selection';
import * as d3Shape from 'd3-shape';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';

// CARGAR SERVICIOS Y SUSTITUIR "Datas" EN SUBATRESVG

@Component({
  selector: 'reporte-solicitudes',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reporte-solicitudes.component.html',
  styleUrls: ['./reporte-solicitudes.component.scss']
})
export class ReporteSolicitudesComponent implements OnInit {
  private sedeSvg: any;
  private campanaSvg: any;
  private colors: any;
  private arc: any;
  private x: any;
  private y: any;
  private barr: any;
  private dataSource: any;
  private cols: string[];

  constructor() {
    this.colors = ['#15aa96', '#c83660', '#714288', '#ff5500'];
    this.cols = ['poliza', 'nombre', 'tiempo', 'PNE', 'PNA', 'PNC'];
  }


  ngOnInit() {
    this.sedeSVG();
    this.subareaSVG();
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sedeSVG() {
    const sedeSize = [180, 510];
    this.sedeSvg = d3Select.select('#sede').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox',  `${0} ${0} ${sedeSize[0]} ${sedeSize[1]}`);

    const tacuba = this.sedeSvg.append('rect')
      .attr('x', '10%')
      .attr('y', '10%')
      .attr('height', '15%')
      .attr('width', '80%')
      .attr('rx', 10)
      .style('fill',  this.colors[0])
      .attr('cursor', 'pointer');
     // .on('click', () => this.admin());
    const text1 = this.sedeSvg.append('text').attr('x', '26%').attr('y', '19%').attr('font-size', 20).text('TACUBA')
      .attr('fill', 'white').attr('cursor', 'pointer');

    const Gcircle2 = this.sedeSvg.append('rect')
      .attr('x', '10%')
      .attr('y', '45%')
      .attr('height', '15%')
      .attr('width', '80%')
      .attr('rx', 10)
      .style('fill', this.colors[1])
      .attr('cursor', 'pointer');
      //.on('click', () => this.ejec());
     const text2 = this.sedeSvg.append('text').attr('x', '26%').attr('y', '54%').attr('font-size', 20).text('VALLEJO')
       .attr('fill', 'white').attr('cursor', 'pointer');

    const Gcircle3 = this.sedeSvg.append('rect')
      .attr('x', '10%')
      .attr('y', '80%')
      .attr('height', '15%')
      .attr('width', '80%')
      .attr('rx', 10)
      .style('fill', this.colors[2])
      .attr('cursor', 'pointer');
      //.on('click', () => this.asist());
     const text3 = this.sedeSvg.append('text').attr('x', '21%').attr('y', '89%').attr('font-size', 20).text('QUERETARO')
       .attr('fill', 'white').attr('cursor', 'pointer');
  }

  subareaSVG() {
    const subareaSize = [410, 510];
    const label = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];
    const Datas = [['ABA', 'ANA', 'AXA', 'BANORTE', 'AGUILA', 'GNP', 'HDI', 'INBURSA', 'MAPFRE', 'QUALITAS', 'ZURICH', 'TAXIS', 'UBER', 'MOTOS', 'MULTIMARCAS', 'AHORRA', 'DIRIGIDA', 'SURA', 'MITTUO', 'AIG', 'ATLAS'],
      [10, 21, 5, 8, 11, 3, 9, 19, 7, 2, 18, 4, 17, 14, 10, 20, 1, 3, 11, 20, 2]];
    const innerRadius = 35;
    const outerRadius = 150;
    // definicion de SVG para dibujar grafico
    this.campanaSvg = d3Select.select('#campanas').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox',  `${0} ${0} ${subareaSize[0]} ${subareaSize[1]}`);
    // escalas de grafico
    this.x = d3Scale.scaleBand()
      .domain(label)
      .range([0 - 0.15, 2 * Math.PI - 0.15]);

    this.y = d3Scale.scaleSqrt()
      .domain([0, d3Array.max(Datas[1])])
      .range([innerRadius, outerRadius]);

    // Barras en circunferencia
    this.arc =  d3Shape.arc().innerRadius(innerRadius);

    this.barr = this.campanaSvg.append('g')
      .selectAll('path')
      .data(Datas[1])
      .enter()
      .append('path')
      .attr('fill', this.colors[3])
      .attr('transform', 'translate(220,250)')
      .attr('d', this.arc
          .outerRadius((d, i) => this.y(d))
          .startAngle((d, i) => this.x(i + 1))
          .endAngle((d, i) => this.x(i + 1) + 0.25)
        );




    // Agregar etiquetas .... a grafico barras
      this.campanaSvg.append('g')
        .selectAll('g')
        .data(Datas[1])
        .enter()
        .append('g')
        .append('text')
          .text((d, i) => Datas[0][i])
          .attr('transform', (d, i) => 'rotate(' + ( i * 17 - 90) +' '+ (220 + (this.y(d) + 5) * Math.cos(i * 0.3 - (Math.PI / 2))) +' '+ (250 + (this.y(d) + 5) * Math.sin(i * 0.3 - (Math.PI / 2))) +')'  )
          .attr('x', (d, i) => 220 + (this.y(d) + 5) * Math.cos(i * 0.3 - (Math.PI / 2) ))
          .attr('y', (d, i) => 250 + (this.y(d) + 5) * Math.sin(i * 0.3 - (Math.PI / 2) ))
          .attr('font-size', 16);

  }


}
