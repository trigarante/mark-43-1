// <<<<<<< HEAD
// import {Component, OnInit, ViewChild} from '@angular/core';
// import {MediosDifusion, MediosDifusionData} from '../../../@core/data/interfaces/marketing/medios-difusion';
// import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {map} from 'rxjs/operators';
// import {NbDialogService} from '@nebular/theme';
// import {ActualizarMediosDifusionComponent} from '../modals/actualizar-medios-difusion/actualizar-medios-difusion.component';
// import {environment} from '../../../../environments/environment';
// const SockJS = require('sockjs-client');
// const Stomp = require('stompjs');
// @Component({
//   selector: 'ngx-medios-difusion',
//   templateUrl: './medios-difusion.component.html',
//   styleUrls: ['./medios-difusion.component.scss'],
// })
// export class MediosDifusionComponent implements OnInit {
//   mediosDifusion: MediosDifusion[];
//   medioDifusion: MediosDifusion;
//   dataSource: any;
//   cols: any[];
//   // sockets
//   private stompClient = null;
//   constructor(protected mediosDifusionService: MediosDifusionData, private dialogoService: NbDialogService) {
//   }
//   connect() {
//     const socket = new SockJS(environment.GLOBAL_SOCKET + 'mediodifusion');
//     this.stompClient = Stomp.over(socket);
//     const _this = this;
//     this.stompClient.connect({}, function(frame) {
//       console.log(frame);
//       _this.stompClient.subscribe('/task/panelMedioDifusion', (content) => {
//         _this.showMessage(JSON.parse(content.body));
//         setTimeout(() => {
//           _this.getMediosDifusion();
//         }, 500);
//       });
//     });
//   }
//   showMessage(message) {
//     this.dataSource.data.unshift(message);
//     this.dataSource.data = this.dataSource.data.slice();
//   }
//   @ViewChild(MatPaginator) paginator: MatPaginator; //
//   @ViewChild(MatSort) sort: MatSort; //
//   verDatos() {
//   }
//
//   getMediosDifusion() {
//     this.mediosDifusionService.get().pipe(map(result => {
//       return result.filter(data => data.activo === 1);
//     })).subscribe(data => {
//       this.mediosDifusion = [];
//       this.mediosDifusion = data;
//       this.dataSource = new MatTableDataSource(this.mediosDifusion); //
//       this.dataSource.sort = this.sort; //
//       this.dataSource.paginator = this.paginator; //
//     });
//   }
//
//   applyFilter(filterValue: string) { //
//     this.dataSource.filter = filterValue.trim().toLowerCase();
//   }
//   actualizarMediosDifusion(id) {
//     this.dialogoService.open(ActualizarMediosDifusionComponent, {
//       context: {
//         id: id,
//
//       },
//     }).onClose.subscribe(x => {
//        this.getMediosDifusion();
//     });
//   }
//
//   getMedioById(idMedio) {
//     this.mediosDifusionService.getMediosDifusionById(idMedio).subscribe(res => {
//       this.medioDifusion = res;
//     });
//   }
//
//   bajaMedios(idMedio) {
//     this.getMedioById(idMedio);
//     swal({
//       title: '¿Deseas dar de baja este medio?',
//       // text: 'Una vez activado lo podrás visualizar de color ROJO',
//       icon: 'warning',
//       dangerMode: true,
//       buttons: {
//         cancel: {
//           text: 'Cancelar',
//           value: false,
//           visible: true,
//           className: '',
//           closeModal: true,
//         },
//         confirm: {
//           text: 'Dar de baja',
//           value: true,
//           visible: true,
//           className: '',
//           closeModal: true,
//         },
//       },
//     }).then((valor) => {
//       if (valor) {
//         this.mediosDifusion['activo'] = 0;
//         this.mediosDifusionService.put(idMedio, this.mediosDifusion).subscribe( result => {
//         });
//         swal('¡El medio se ha dado de baja exitosamente!', {
//           icon: 'success',
//         }).then((resultado => {
//           this.getMediosDifusion();
//         }));
//       }
//     });
//   }
//
//   ngOnInit() {
//     this.connect();
//     this.getMediosDifusion();
//     this.cols = ['idCampana', 'idProvedor', 'descripcion', 'acciones'];
//   }
//
// }

import {Component, OnInit, ViewChild} from '@angular/core';
import {MediosDifusion, MediosDifusionData} from '../../../@core/data/interfaces/marketing/medios-difusion';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {NbDialogService} from '@nebular/theme';
import {ActualizarMediosDifusionComponent} from '../modals/actualizar-medios-difusion/actualizar-medios-difusion.component';
import {environment} from '../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-medios-difusion',
  templateUrl: './medios-difusion.component.html',
  styleUrls: ['./medios-difusion.component.scss'],
})
export class MediosDifusionComponent implements OnInit {
  mediosDifusion: MediosDifusion[];
  medioDifusion: MediosDifusion;
  dataSource: any;
  cols: any[];
  // sockets
  private stompClient = null;
  constructor(protected mediosDifusionService: MediosDifusionData, private dialogoService: NbDialogService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'mediodifusion');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelMedioDifusion', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getMediosDifusion();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  verDatos() {
  }

  getMediosDifusion() {
    this.mediosDifusionService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.mediosDifusion = [];
      this.mediosDifusion = data;
      this.dataSource = new MatTableDataSource(this.mediosDifusion); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  actualizarMediosDifusion(id) {
    this.dialogoService.open(ActualizarMediosDifusionComponent, {
      context: {
        id: id,

      },
    }).onClose.subscribe(x => {
       this.getMediosDifusion();
    });
  }

  getMedioById(idMedio) {
    this.mediosDifusionService.getMediosDifusionById(idMedio).subscribe(res => {
      this.medioDifusion = res;
    });
  }

  bajaMedios(idMedio) {
    this.getMedioById(idMedio);
    swal({
      title: '¿Deseas dar de baja este medio?',
      // text: 'Una vez activado lo podrás visualizar de color ROJO',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.mediosDifusion['activo'] = 0;
        this.mediosDifusionService.put(idMedio, this.mediosDifusion).subscribe( result => {
        });
        swal('¡El medio se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getMediosDifusion();
        }));
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.getMediosDifusion();
    this.cols = ['idCampana', 'idProvedor', 'descripcion', 'acciones'];
  }

}
