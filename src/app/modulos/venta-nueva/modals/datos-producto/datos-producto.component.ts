import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductoSolicitudData} from '../../../../@core/data/interfaces/ventaNueva/producto-solicitud';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductoClienteAutoData} from '../../../../@core/data/interfaces/ventaNueva/producto-cliente-auto';
import {map} from 'rxjs/operators';
import {ProductoClienteData} from '../../../../@core/data/interfaces/ventaNueva/producto-cliente';
import {NbDialogService} from '@nebular/theme';
import {SolicitudesVNData} from '../../../../@core/data/interfaces/ventaNueva/solicitudes';

@Component({
  selector: 'ngx-datos-producto',
  templateUrl: './datos-producto.component.html',
  styleUrls: ['./datos-producto.component.scss'],
})
export class DatosProductoComponent implements OnInit {
  // @Input () idCliente: number;
  @Input() idProductoSolicitud: number;
  @Input() idSolicitud: number;
  @Input() idSubRamo: number;

  @Output() _idProductoCliente = new EventEmitter<number>();
  @Output() _registroPoliza = new EventEmitter<boolean>();

  /**Variable Aux del microservicio*/
  productoSolicitud: any;
  /**Variable que almacena el Form*/
  datosProductoSolicitudForm: FormGroup;
  /**Variable que almacena los datos del JSON*/
  datos: any;
  idCliente: number;

  constructor(protected productoSolicitudService: ProductoSolicitudData,
              private fb: FormBuilder,
              private productoClienteService: ProductoClienteData,
              private dialogoService: NbDialogService,
              private solicitudesService: SolicitudesVNData) {

    this.solicitudesService.returnIdCliente()
      .subscribe(res => {
        this.idCliente = res;
      });
  }

  ngOnInit() {
    this.getProductoSolicitud();

    this.datosProductoSolicitudForm = this.fb.group({
      'numeroPlacas': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('[A-Za-z]{3}[0-9]{4}')])),
      'numeroMotor': new FormControl('', Validators.required),
    });


  }

  getProductoSolicitud() {
    this.productoSolicitudService.getProductoSolicitud(this.idProductoSolicitud).subscribe(result => {
      this.productoSolicitud = result;
      this.datos = JSON.parse(this.productoSolicitud.datos);
    });
  }

  crearProducto(aseguradora) {
    this.productoSolicitudService.getDatos().pipe(map(result => {
      return result.filter(data => data.numeroMotor === this.datosProductoSolicitudForm.controls['numeroMotor'].value ||
        data.numeroPlacas === this.datosProductoSolicitudForm.controls['numeroPlacas'].value);
    })).subscribe(result => {
      if (result.length === 0) {
        // this.productoSolicitud.datos = JSON.parse(this.productoSolicitud.datos);
        this.datos['numeroPlacas'] = this.datosProductoSolicitudForm.controls['numeroPlacas'].value;
        this.datos['numeroMotor'] = this.datosProductoSolicitudForm.controls['numeroMotor'].value;
        // console.log(this.productoSolicitud);
        const productoCliente: any = {
          'idSolicitud': this.idSolicitud,
          'idCliente': 3, /*this.idCliente,*/
          'datos': JSON.stringify(this.datos),
          'idSubRamo': this.idSubRamo,
        };
        this.productoClienteService.post(productoCliente).subscribe(
          data => {
            console.log('Se ha registrado');
            if (aseguradora === 'QUALITAS') {
              this.productoClienteService.entryAseguradora(1);
            }else {
              this.productoClienteService.entryAseguradora(0);
            }
            this._registroPoliza.emit(true);
            // this.dialogoService.open(RegistroPolizaComponent, {
            //   context: {
            //     idProductoCliente: data.id,
            //   },
            // });
          },
        );
      } else {
        console.log('Ya existe');
      }
    });
  }
}
