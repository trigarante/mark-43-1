import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'Dashboard',
  //   icon: 'nb-home',
  //   link: '/pages/dashboard',
  // },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
  {
    title: '',
  },
  {
    title: 'Recursos Humanos',
    icon: 'nb-person',
    children: [
      {
        title: 'Vacantes',
        link: '/modulos/rrhh/vacantes',
      },
      {
        title: 'Solicitudes',
        link: '/modulos/rrhh/solicitudes',
      },
      {
        title: 'Precandidatos',
        link: '/modulos/rrhh/precandidatos',
      },
      {
        title: 'Candidatos',
        link: '/modulos/rrhh/candidatos',
      },
      {
        title: 'Capacitacion',
        link: '/modulos/rrhh/capacitacion',
      },
      {
        title: 'Seguimiento',
        link: '/modulos/rrhh/seguimiento',
      },
      {
        title: 'Bajas',
        link: '/modulos/rrhh/bajas',
      },
      {
        title: 'Pre empleados',
        link: '/modulos/rrhh/pre-empleados',
      },
      {
        title: 'Empleados',
        link: '/modulos/rrhh/empleados',
      },
      {
        title: 'Catalogos',
        children: [
          {
            title: 'Area',
            link: '/modulos/rrhh/catalogos/area',
          },
          {
            title: 'Banco',
            link: '/modulos/rrhh/catalogos/banco',
          },
          {
            title: 'Bolsa de trabajo',
            link: '/modulos/rrhh/catalogos/bolsaTrabajo',
          },
          {
            title: 'Competencias',
            link: '/modulos/rrhh/catalogos/competencias',
          },
          {
            title: 'Competencias de candidato',
            link: '/modulos/rrhh/catalogos/competenciasCandidato',
          },
          {
            title: 'Empresa',
            link: '/modulos/rrhh/catalogos/empresa',
          },
          {
            title: 'Escolaridad',
            link: '/modulos/rrhh/catalogos/escolaridad',
          },
          {
            title: 'Estado de escolaridad',
            link: '/modulos/rrhh/catalogos/estadoEscolaridad',
          },
          {
            title: 'Estado civil',
            link: '/modulos/rrhh/catalogos/estadoCivil',
          },
          {
            title: 'Medio de transporte',
            link: '/modulos/rrhh/catalogos/medioTransporte',
          },
          {
            title: 'Paises',
            link: '/modulos/rrhh/catalogos/paises',
          },
          {
            title: 'Puesto',
            link: '/modulos/rrhh/catalogos/puesto',
          },
          {
            title: 'Tipo de puesto',
            link: '/modulos/rrhh/catalogos/tipoPuesto',
          },
        ],
      },
    ],
  },
  {
    title: 'TI',
    icon: 'nb-person',
    children: [
      {
        title: 'Administrativo',
        link: '/modulos/ti/administrativo',
      },
      {
        title: 'Ejecutivo',
        link: '/modulos/ti/ejecutivo',
      },
      ],
  },
  {
    title: 'Reportes',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Pre-candidato',
        link: '/modulos/reportes/reporte-precandidato',
      },
      {
        title: 'Candidato',
        link: '/modulos/reportes/reporte-candidato',
      },
      // {
      //   title: 'Capacitación',
      //   link: '/modulos/reportes/reporte-capacitacion',
      // },
      // {
      //   title: 'Pre-empleado',
      //   link: '/modulos/reportes/reporte-preempleado',
      // },
      // {
      //   title: 'Empleado',
      //   link: '/modulos/reportes/reporte-empleado',
      // },
    ],
  },

];
