import {Component, OnInit, ViewChild} from '@angular/core';
import {Capacitacion, CapacitacionData} from '../../../@core/data/interfaces/rrhh/capacitacion';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarSeguimientoComponent} from '../modals/asignar-seguimiento/asignar-seguimiento.component';
import {map} from 'rxjs/operators';
import {AsistenciaComponent} from '../modals/asistencia/asistencia.component';
import Swal from 'sweetalert2';
import {BajaPrecandidatoComponent} from '../modals/baja-precandidato/baja-precandidato.component';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {AsignarCapacitacionComponent} from '../modals/asignar-capacitacion/asignar-capacitacion.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosCapacitacionComponent} from '../modals/datos-capacitacion/datos-capacitacion.component';
import {environment} from '../../../../environments/environment';
import swal from 'sweetalert2';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componenten de capacitación.
 */
@Component({
  selector: 'ngx-capacitacion',
  templateUrl: './capacitacion.component.html',
  styleUrls: ['./capacitacion.component.scss'],
})
export class CapacitacionComponent implements OnInit {
  cols: any[];
  capacitacion: Capacitacion[];
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  c: any;
  date: Date = new Date();
  fechaCompleta: any;
  horaActual: any;
  diaCapacitacion: any;
  activoAsistencia: boolean;
  jsonFecha: any;
  // capcitacion: any;
  actAsignar: boolean;
  dataSource: any;
  escritura: boolean;
  permisos: any;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   *
   * En este caso: Servicio de Capacitación y Servicio de Diálogos.
   * @param {CapacitacionData} capacitacionService Servicio del componente capacitacion para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes). */
  constructor(protected capacitacionService: CapacitacionData,
              private dialogService: NbDialogService,
              protected motivosService: EstadoRrhhData,
              private toastrService: NbToastrService,
              ) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'capacitacion');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelCapacitacion', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCapacitacion(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /** Función que obtiene todos los candidatos que se encuentran en capacitación. */
  getCapacitacion(mensajeAct) {
    this.capacitacionService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 4 && data.idEstado === 1 ||
        data.idEtapa === 4 && data.idEstado === 16);
    })).subscribe(data => {
      this.capacitacion = data;
      this.dataSource = new MatTableDataSource(this.capacitacion);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

      if (this.capacitacion.length === 0) {
        swal.fire({
          type: 'error',
          title: 'Oops',
          text: 'No tienes candidatos por capacitar',
        });
      } else {
        for (let i = 0; i < this.capacitacion.length; i++) {
          this.jsonFecha = this.capacitacion[i].asistencias;
          if (this.jsonFecha !== null) {
            this.jsonFecha = JSON.parse(this.jsonFecha);
            if (typeof this.jsonFecha === 'string') {
              this.jsonFecha = JSON.parse(this.jsonFecha);
            }
            if (Object.keys(this.jsonFecha).length === null) {
              this.actAsignar = true;
            } else {
              this.actAsignar = false;
            }
          }
        }
      }
    }, () => {
        swal.fire({
          type: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      },
      () => {
        if (mensajeAct === 1 && this.capacitacion.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Couching`);
        }
    });
  }

  verDatos(idCapacitacion) {
    this.dialogService.open(DatosCapacitacionComponent, {
      context: {
        idCapacitacion: idCapacitacion,
      },
    });
  }

  /** Esta función permite mostrar un modal en el cual se establece el seguimiento al candidato en capacitación. */
  asignarSeguimiento(idCapacitacion, idPrecandidato, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarSeguimientoComponent, {
      context: {
        idCapacitacion: idCapacitacion,
        idPrecandidato: idPrecandidato,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
      },
    });
  }

  registroAsistencia(idCapacitacion) {
    this.dialogService.open(AsistenciaComponent, {
      context: {
        idCapacitacion: idCapacitacion,
      },
    }).onClose.subscribe(x => {
      this.getCapacitacion(0);
      });
  }

  updateAsistencia(idCapacitacion) {
    this.dialogService.open(AsistenciaComponent, {
      context: {
        idCapacitacion: idCapacitacion,
        idTipo: 2,
      },
    }).onClose.subscribe(xx => {
      this.getCapacitacion(0);
    });
  }

  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }

  /** Función que despliega un modal para dar de baja un precandidato. */
  async bajaCapacitacion(idPrecandidato, idCapacitacion) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }

    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await Swal.fire({
      title: '¿Deseas dar de baja este couching?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada un motivo de baja
      this.dialogService.open(BajaPrecandidatoComponent, {
        context: {
          idPrecandidato: idPrecandidato,
          idEstadoRh: motivo,
          idCapacitacion: idCapacitacion,
        },
      }).onClose.subscribe(x => {
        this.getCapacitacion(0);
      });
    }
  }

  updateCpacitacion(idCapacitacion, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarCapacitacionComponent, {
      context: {
        idCapacitacion: idCapacitacion,
        idTipo: 2,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
      },
    }).onClose.subscribe(xx => {
      this.getCapacitacion(1);
    });
  }


  /** Función que inicializa el componente, mandando a llamar a getCapacitacion para llenar la tanla.*/
  ngOnInit() {
    this.connect();
    this.getMotivosBaja();
    this.getPermisos();
    this.cols = ['detalle', 'nombre', 'calificacion', 'sede', 'subarea', 'acciones'];
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/capacitacion') {
        this.getCapacitacion(0);
    //   }
    // }, 1000);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.teorico.escritura;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
