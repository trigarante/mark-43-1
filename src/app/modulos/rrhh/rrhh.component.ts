import {Component} from '@angular/core';

@Component({
  selector: 'ngx-rrhh',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class RrhhComponent {

  public loading: boolean;

  constructor() {
    this.loading = true;
  }


}

