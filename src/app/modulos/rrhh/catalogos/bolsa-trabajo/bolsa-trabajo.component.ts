import {Component, OnInit, ViewChild} from '@angular/core';
import {BolsaTrabajo, BolsaTrabajoData} from '../../../../@core/data/interfaces/catalogos/bolsa-trabajo';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {BolsaTrabajoCreateComponent} from '../modals/bolsa-trabajo-create/bolsa-trabajo-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-bolsa-trabajo',
  templateUrl: './bolsa-trabajo.component.html',
  styleUrls: ['./bolsa-trabajo.component.scss'],
})
export class BolsaTrabajoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  bolsasTrabajoo: BolsaTrabajo[];
  bolsaTrabajo: BolsaTrabajo;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
/**
   * En este caso: Servicio de Bolsa de Trabajo y Servicio de Diálogos.
   * @param {BolsaTrabajoData} bolsaTrabajoService Servicio del componente bolsa-trabajo para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio para diálogos (mensajes). */
  constructor(private bolsaTrabajoService: BolsaTrabajoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bolsatrabajo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelBolsaTrabajo', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getBOlsaTrabajo(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Esta función obtiene la bolsa de trabajo existente y los datos recogidos, se asignan al arreglo bolsasTrabajo. */
  getBOlsaTrabajo(mensajeAct) {
    this.bolsaTrabajoService.getBolsaTrabajo().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.bolsasTrabajoo = [];
      this.bolsasTrabajoo = data;
      this.dataSource = new MatTableDataSource(this.bolsasTrabajoo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Bolsa-Trabajo`);
  }
  /** Función que obtiene la información de la bolsa de trabajo dependiendo de su ID. */
  getBolsaTrabajoById(idBolsaTrabajo) {
    this.bolsaTrabajoService.getBolsaTrabajoById(idBolsaTrabajo).subscribe( data => {
      this.bolsaTrabajo = data;
    });
  }
  /** Esta función despliega un modal en el cual se crea, establece el nombre y el tipo de la bolsa de trabajo. */
  crearBolsaTrabajo() {
    this.dialogoService.open(BolsaTrabajoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getBOlsaTrabajo(0);
    });
  }
  /** Función que despliega un modal en el cual se puede actualizar la información de una bolsa de trabajo. */
  updateBolsaTrabajo(idBolsaTrabajo) {
    this.dialogoService.open(BolsaTrabajoCreateComponent, {
      context: {
        idTipo: 2,
        idBolsaTrabajo: idBolsaTrabajo,
      },
    }).onClose.subscribe(x => {
      this.getBOlsaTrabajo(1);
    });
  }

  bajaBolsaTrabajo(idBolsaTrabajo) {
    /*this.dialogoService.open(BolsaTrabajoCreateComponent, {
      context: {
        idTipo: 3,
        idBolsaTrabajo: idBolsaTrabajo,
      },
    });*/
    this.getBolsaTrabajoById(idBolsaTrabajo);
    swal({
      title: '¿Deseas dar de baja esta bolsa de trabajo?',
      text: 'Una vez dada de baja, no podrás verla de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.bolsaTrabajo['activo'] = 0;
        this.bolsaTrabajoService.put(idBolsaTrabajo, this.bolsaTrabajo).subscribe( result => {
          this.bolsaTrabajoService.postSocket({idBolsaTrabajo: idBolsaTrabajo, bolsaTrabajo: this.bolsaTrabajo}).subscribe(x => {});
        });
        swal('¡La bolsa de trabajo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getBOlsaTrabajo(0);
        }));
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bolsaDeTrabajo.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bolsaDeTrabajo.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.connect();
    this.cols = [ 'nombre', 'tipo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getBOlsaTrabajo(0);
  }
}
