import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {PruebasComponent} from './pruebas.component';

const routes: Routes = [{
  path: '',
  component: PruebasComponent,
  children: [
    {
      path: 'pruebas',
      component: PruebasComponent,
    },
  ],
}];

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PruebasRoutingModule {
}