import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';
fdescribe('Extensiones service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 101,
    idEstado: 0,
    idSubarea: 61,
    area: 'TECNOLOGIAS DE LA INFORMACION',
    subarea: 'DESARROLLO WEB',
    descrip: 'OKI',
    estado: 'LIBRE',
    idArea: 19,
  },
    {
      id: 'extensiones',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/extensiones');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/extensiones', '/101');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/extensiones');
      }); /*
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/extensiones', '/3/74');
      });*/
    });
  }
})
