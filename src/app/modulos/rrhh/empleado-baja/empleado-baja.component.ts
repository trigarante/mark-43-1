import {Component, OnInit, ViewChild} from '@angular/core';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {BajaEmpleadoComponent} from '../modals/baja-empleado/baja-empleado.component';
import swal from 'sweetalert';
import {Precandidatos, PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import * as moment from 'moment';
import {ReactivarEmpleadoComponent} from '../modals/reactivar-empleado/reactivar-empleado.component';
import {AsignarFiniquitoComponent} from '../modals/asignar-finiquito/asignar-finiquito.component';
import {RecontratableComponent} from '../modals/recontratable/recontratable.component';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'ngx-empleado-baja',
  templateUrl: './empleado-baja.component.html',
  styleUrls: ['./empleado-baja.component.scss'],
})
export class EmpleadoBajaComponent implements OnInit {
  empleado: Empleados[];
  cols: any[];
  precandidato: any;
  dataSource: any;
  fechaIngreso: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {EmpleadosData} empleadoService Servicio del componente Empleados para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes). */
  constructor(protected empleadoService: EmpleadosData,
              private dialogService: NbDialogService,
              private precandidatosService: PrecandidatosData,
              private router: Router, private toastrService: NbToastrService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'empleadosbaja');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelEmpleadosBaja', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEmpleado(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
   /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getEmpleado(mensajeAct) {
    this.empleadoService.get().pipe(map(result => {
      return result.filter(value => value.idEtapa === 3 && value.idEstadoRH !== 1 &&
        value.idEstadoRH !== 16);
    })).subscribe( data => {
      this.empleado = data;
      for (let i = 0; i < this.empleado.length; i++) {
        this.empleado[i].fechaBaja = moment(this.empleado[i].fechaBaja).format('DD-MM-YYYY');
        if (this.empleado[i].recontratable === '0') {
          this.empleado[i].recontratable = 'NO';
        } else {
          this.empleado[i].recontratable = 'SI';
        }
      }
      this.dataSource = new MatTableDataSource(this.empleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
     if (mensajeAct === 1) {
       this.showToast();
     }
  }
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Baja Empleados`);
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Función que despliega un modal mandando llamar a BajaEmpleadoComponent para el formulario. */
  bajaEmpleado(idPrecandidato: number) {
    this.dialogService.open(BajaEmpleadoComponent, {
      context: {
        idPrecandidato: idPrecandidato,
      },
    });
  }

  activarEmpleado(idEmpleado, idPrecandidato) {
    this.empleadoService.getEmpleadoById(idEmpleado).subscribe(result => {
      this.precandidato = result;
      swal({
        title: '¿Deseas Activar este Empleado?',
        text: 'El empleado regresará a la vista de Empleados.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          succes: {
            text: 'Activar',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.dialogService.open(ReactivarEmpleadoComponent, {
            context: {
              idEmpleado: idEmpleado,
              idPrecandidato: idPrecandidato,
            },
          }).onClose.subscribe(x => {
            this.getEmpleado(0);
          });
        }
      });
    });
  }

  asignarFiniquito(idEmpleado, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarFiniquitoComponent, {
            context: {
              idTipo: 1,
              idEmpleado: idEmpleado,
              nombre: nombre + ' ' + apellidoPaterno + ' ' + apellidoMaterno,
            },
          }).onClose.subscribe(x => {
            this.getEmpleado(1);
    });
  }

  editarFiniquito(idEmpleado, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarFiniquitoComponent, {
            context: {
              idTipo: 2,
              idEmpleado: idEmpleado,
              nombre: nombre + ' ' + apellidoPaterno + ' ' + apellidoMaterno,
            },
          }).onClose.subscribe(x => {
            this.getEmpleado(1);
    });
  }

  recontratable(idPrecandidato, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(RecontratableComponent, {
      context: {
        idPrecandidato: idPrecandidato,
        idTipo: 2,
        nombre: nombre + ' ' + apellidoPaterno + ' ' + apellidoMaterno,
      },
    }).onClose.subscribe(x => {
      this.getEmpleado(1);
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.empleados.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bajaEmpleados.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
    console.log(this.activo);
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nombre', 'subarea', 'fechaBaja', 'nombreEtapa', 'estadoRh', 'detalleBaja', 'recontratable', 'acciones'];
    this.getPermisos();
    this.getEmpleado(1);
    this.estaActivo();
    this.getEmpleado(0);
  }

}
