import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Cpu, CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {map} from 'rxjs/operators';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {Procesador, ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-cpu-create',
  templateUrl: './cpu-create.component.html',
  styleUrls: ['./cpu-create.component.scss'],
})
export class CpuCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idCpu: number;
  cpu: Cpu;
  cpuCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  /**Areglo que inicia los valores del Microservicio*/
  procesadores: Procesador[];
  /**Arreglo auxiliar para  marcas*/
  procesador: any;

  origenEquipos: SelectItem[];

  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<CpuCreateComponent>, private cpuService: CpuData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData,
              private procesadorService: ProcesadorSoporteData, private router: Router, private sedesService: SedeData,
              private tipoSedeService: TipoSedeData) { }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  getProcesador() {
    this.procesadorService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.procesador = [];
      this.procesadores = data;
      for (let i = 0; i < this.procesadores.length; i++) {
        this.procesador.push({'label': this.procesadores[i].nombreProcesador, 'value': this.procesadores[i].id});
      }
    });
  }

  guardarCreateCpu() {
    if (this.cpuCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.cpuService.post(this.cpuCreateForm.value).subscribe((result) => {
      this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
      this.cpuCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario/', 'cpu']);
    });
  }

  updateCpu() {
    this.cpuService.put(this.idCpu, this.cpuCreateForm.value).subscribe(result => {
      this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
      this.cpuCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario/', 'cpu']);
    });
  }
  getCpuById() {
    this.cpuService.getCpuById(this.idCpu).subscribe(data => {
      this.cpu = data;
      console.log(data);
      this.cpuCreateForm.controls['numeroSerie'].setValue(this.cpu.numeroSerie);
      this.cpuCreateForm.controls['idProcesador'].setValue(this.cpu.idProcesador);
      this.cpuCreateForm.controls['hdd'].setValue(this.cpu.hdd);
      this.cpuCreateForm.controls['ram'].setValue(this.cpu.ram);
      this.cpuCreateForm.controls['idMarca'].setValue(this.cpu.idMarca);
      this.cpuCreateForm.controls['modelo'].setValue(this.cpu.modelo);
      this.cpuCreateForm.controls['idEstadoInventario'].setValue(this.cpu.idEstadoInventario);
      this.cpuCreateForm.controls['idObservacionesInventario'].setValue(this.cpu.idObservacionesInventario);
      this.cpuCreateForm.controls['comentarios'].setValue(this.cpu.comentarios);
      this.cpuCreateForm.controls['idEmpleado'].setValue(this.cpu.idEmpleado);
      this.cpuCreateForm.controls['idTipoSede'].setValue(this.cpu.idTipoSede);
      this.cpuCreateForm.controls['origenRecurso'].setValue(this.cpu.origenRecurso);

    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCpuById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
     this.getProcesador();
     this.getMarca();
     this.getOrigen();
     this.getSedes();
    this.cpuCreateForm = this.fb.group({
      'numeroSerie': new FormControl('', Validators.compose([Validators.required])),
      'idProcesador': new FormControl('', Validators.compose([Validators.required])),
      'hdd': new FormControl('', Validators.compose([Validators.required])),
      'ram': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'modelo': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
