import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaInventarioComponent } from './baja-inventario.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {CpuData} from '../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {MonitorDate} from '../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {LaptopData} from '../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {DiademaData} from '../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {TabletData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {TecladoData} from '../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {MouseData} from '../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';

describe('BajaInventarioComponent', () => {
  let component: BajaInventarioComponent;
  let fixture: ComponentFixture<BajaInventarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaInventarioComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        CpuData,
        MonitorDate,
        LaptopData,
        DiademaData,
        TabletData,
        TecladoData,
        MouseData,
        EstadoInventarioData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(BajaInventarioComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
   /*expect(component).toBeTruthy();*/
  });
});
