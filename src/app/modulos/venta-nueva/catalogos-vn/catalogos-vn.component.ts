import { Component } from '@angular/core';


@Component({
  selector: 'ngx-catalogos-vn',
  template: '<router-outlet></router-outlet>',
})
export class CatalogosVnComponent {

}
