import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Solicitudes, SolicitudesVNData} from '../../interfaces/ventaNueva/solicitudes';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable()

export class SolicitudesvnService extends SolicitudesVNData {

  private baseURL;

  public idCliente = new BehaviorSubject<number>(0);
  public idRegistroPoliza = new BehaviorSubject<number>(0);
  public  urlPostventa = new BehaviorSubject<number>(0);
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Solicitudes[]> {
    return this.http.get<Solicitudes[]>(this.baseURL + 'v1/solicitudes-vn/' + sessionStorage.getItem('Usuario'));
  }

  post(solicitud: Solicitudes): Observable<Solicitudes> {
    return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes-vn' , solicitud);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vnsolicitudes/saveVnSolicitudes', json);
  }

  getSolicitudesById(idSolicitud): Observable<Solicitudes> {
    return this.http.get<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/get-by-id/' + idSolicitud);
  }

  put(idSolicitud, solicitud): Observable<Solicitudes> {
    return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes-vn/' + idSolicitud, solicitud);
  }
  entryIdCliente(value) {
    this.idCliente.next(value);
  }

  returnIdCliente(): Observable<number> {
    return this.idCliente.asObservable();
  }

  entryIdRegistroPoliza(value) {
    this.idRegistroPoliza.next(value);
  }

  returnIdRegistroPoliza(): Observable<number> {
    return this.idRegistroPoliza.asObservable();
  }

  entryURLType(value) {
    this.urlPostventa.next(value);
  }

  returnentryURLType(): Observable<number> {
    return this.urlPostventa.asObservable();
  }
}
