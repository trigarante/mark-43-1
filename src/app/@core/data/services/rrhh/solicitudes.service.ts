import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Solicitudes, SolicitudesData} from '../../interfaces/rrhh/solicitudes';
import {environment} from '../../../../../environments/environment';
import {Observable } from 'rxjs';


@Injectable()
export class SolicitudesService extends SolicitudesData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
    get(): Observable<Solicitudes[]> {
      return this.http.get<Solicitudes[]>(this.baseURL + 'v1/solicitudes/' + sessionStorage.getItem('Usuario'));
    }

    post(solicitud: Solicitudes): Observable<Solicitudes> {
        return this.http.post<Solicitudes>(this.baseURL + 'v1/solicitudes', solicitud);
    }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'solicitudes/saveSolicitud/1', json);
  }

    bajaSolicitud(idSolicitud, solicitud: Solicitudes): Observable<Solicitudes> {
      return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes/baja-solicitud/' + idSolicitud + '/' +
        sessionStorage.getItem('Empleado'), solicitud);
    }

    updateSolicitud(idSolicitud, solicitud: Solicitudes): Observable<Solicitudes> {
      return this.http.put<Solicitudes>(this.baseURL + 'v1/solicitudes/' + idSolicitud + '/' +
        sessionStorage.getItem('Empleado'), solicitud);
    }

    agendarCita(id, fechaCita) {
        return this.http.put(this.baseURL + 'v1/solicitudes/' + id + '/' + fechaCita + '/' +
          sessionStorage.getItem('Empleado'), id);
    }

    getSolicitudById(idSolicitud): Observable<Solicitudes> {
        return this.http.get<Solicitudes>(this.baseURL + 'v1/solicitudes/get-by-id/' + idSolicitud);
    }

    checkDocumentos(idSolicitud, documentos) {
        return this.http.put(this.baseURL + 'v1/solicitudes/documentos/' + idSolicitud + '/' +
          sessionStorage.getItem('Empleado'), documentos);
    }
}


