import {Component, Injectable, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SubCategoriaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/sub-categoria';
import {Divisas, DivisasData} from '../../../../../@core/data/interfaces/comerciales/catalogo/divisas';
import {map} from 'rxjs/operators';
import {Categoria, CategoriaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/categoria';

@Component({
  selector: 'ngx-create',
  templateUrl: './subcategoria-create.component.html',
  styleUrls: ['./subcategoria-create.component.scss'],
})
export class SubcategoriaCreateComponent implements OnInit {
@Input() idTipo: number;
@Input() idSubCategoria: number;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  subcategoriaCreateForm: FormGroup;
  submitted: Boolean;
  divisa: Divisas[];
  divisas:  any[];
  categoria: Categoria[];
  categorias: any[];
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private tipoConvenioService: CategoriaData,
              private divisasService: DivisasData, private subCategoriaServive: SubCategoriaData) { }
  crearSubCategoria() {
    this.submitted = true;
    if (this.subcategoriaCreateForm.invalid) {
      return;
    }
    this.subCategoriaServive.post(this.subcategoriaCreateForm.value).subscribe((result) => {
      this.subCategoriaServive.postSocket({status: 'ok'}).subscribe(x => {});
      this.subcategoriaCreateForm.reset();
      this.ref.close();
    });
  }
  getSubCategoriaById() {
    this.subCategoriaServive.getSubCategoriaById(this.idSubCategoria).subscribe(data => {
      this.subcategoriaCreateForm.controls['detalle'].setValue(data.detalle);
      this.subcategoriaCreateForm.controls['regla'].setValue(data.regla);
      this.subcategoriaCreateForm.controls['idDivisas'].setValue(data.idDivisas);
      this.subcategoriaCreateForm.controls['idCategoria'].setValue(data.idCategoria);
      this.getCategoriasById(data.idCategoria);
    });
  }
  getCategorias() {
    this.tipoConvenioService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.categoria = data;
      this.categorias = [];
      for (let i = 0; i < this.categoria.length; i++) {
        this.categorias.push({'label': this.categoria[i].tipo, 'value': this.categoria[i].id});
      }
    });
  }
  getCategoriasById(idSubCategoria) {
    this.tipoConvenioService.getCategoriaById(idSubCategoria).subscribe(data => {
      this.subcategoriaCreateForm.controls['idCategoria'].setValue(data.id);
    });
  }
  updateSubCategoria() {
    this.subCategoriaServive.put(this.idSubCategoria, this.subcategoriaCreateForm.value).subscribe(result => {
      this.subCategoriaServive.postSocket({status: 'ok'}).subscribe(x => {});
      this.subcategoriaCreateForm.reset();
      this.ref.close();
    });
  }
  getDivisa() {
    this.divisasService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.divisa = data;
      this.divisas = [];
      for (let i = 0; i < this.divisa.length; i++) {
        this.divisas.push({'label': this.divisa[i].divisa, 'value': this.divisa[i].id});
      }
    });
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSubCategoriaById();
    }
    this.getCategorias();
    this.getDivisa();
    this.subcategoriaCreateForm = this.fb.group({
      'detalle': new FormControl('', [Validators.required, Validators.minLength(4)]),
      'regla': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'idDivisas': new FormControl('', Validators.required),
      'activo': 1,
      'idCategoria': new FormControl('', Validators.required),
    });
  }

}
