import {Observable} from 'rxjs';

export interface SociosBaja {
  id: number;
  idEstado: number;
  estado: string;
  activo: number;
}

export abstract class SociosBajaData {
  abstract get(): Observable<SociosBaja[]>;

  abstract post(tipoCategoria): Observable<SociosBaja>;
  abstract postSocket(json): Observable<JSON>;

  abstract getSocioBajaById(idSocio): Observable<SociosBaja>;

  abstract put(idSocio, socioBaja): Observable<SociosBaja>;
}
