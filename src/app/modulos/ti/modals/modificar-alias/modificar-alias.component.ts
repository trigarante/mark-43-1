import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map} from 'rxjs/operators';
import {CorreoData, Domains} from '../../../../@core/data/interfaces/ti/correo';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {SaveModificadoComponent} from '../save-modificado/save-modificado.component';

@Component({
  selector: 'ngx-modificar-alias',
  templateUrl: './modificar-alias.component.html',
  styleUrls: ['./modificar-alias.component.scss'],
})
export class ModificarAliasComponent implements OnInit {
  @Input() idUsuario: number;
  @Input() usuario: string;
  @Input() idCorreo: string;
  /** Variable que permite la inicialización del formulario para crear modificar el alias. */
  aliasModificadoform: FormGroup;
  tipodominio: string;
  tipodominioCorreo: string;
  /** inicializa arrelo de dominios*/
  domainss: any[];
  domains: Domains[];
  correo: string;
  correofinal: string;
  estadoCorreo2: boolean = false;
  estadoCorreo3: boolean = true;
  estadofinal: string;
  estadofinalias: boolean;
  /** arreglos para ver el usuario*/
  usuarioput: Usuario;
  /** arreglos para modificar el usuario*/
  usuarioput1: Usuario;
  usuario3: Usuario;

  constructor(private fb: FormBuilder, private correoService: CorreoData,  protected ref: NbDialogRef<ModificarAliasComponent>,
              private usuarioService: UsuarioData, public dialogService: NbDialogService ) { }
  /** consigue el tipo de dominio*/
  getTipo() {
    if (this.usuario.match(/trigarante.com/) || this.usuario.match(/nexosmedia.com/)
      || this.usuario.match(/grupopg.com.mx/) || this.usuario.match(/migoseguros.com/)
      || this.usuario.match(/migoseguros.com.mx/) || this.usuario.match(/grupoahorra.com/)
      || this.usuario.match(/serviciostrigarante.com/)) {
      this.tipodominio = 'administrativo';
      this.tipodominioCorreo = 'administrativo';
    } else if (this.usuario.match(/ahorraseguros.mx/) || this.usuario.match(/ahorracredit.com/)
      || this.usuario.match(/mejorsegurodeauto.mx/) || this.usuario.match(/mejorsegurodevida.mx/)
      || this.usuario.match(/qualitassegurosautos.com/) || this.usuario.match(/comparaya.mx/)
      || this.usuario.match(/gnpsegurosauto.com/) || this.usuario.match(/lalatinoseguros.mx/)
      || this.usuario.match(/multivaseguros.com/) || this.usuario.match(/segurosdetaxi.com.mx/)
      || this.usuario.match(/seguroselpotosi.mx/) || this.usuario.match(/segurosmotos.com.mx/)
      || this.usuario.match(/ahorraseguros.com/) || this.usuario.match(/inprove.mx/) ) {
      this.tipodominio = 'ejecutivo';
      this.tipodominioCorreo = 'ejecutivo';
    } else if (this.usuario.match(/ahorraasistencia.com/)) {
      this.tipodominio = 'asistencia';
      this.tipodominioCorreo = 'asistencia';
    }
    this.dominio();
  }
  dominio() {
  // console.log(this.tipodominio);
    this.correoService.getDominios(this.tipodominio).pipe(map(data => this.domains = data)).subscribe(data => {
      this.domainss = [];
      for (let i = 0; i < this.domains.length; i++) {
        this.domainss.push({'label': this.domains[i].domainName, 'value': this.domains[i].domainName});
      }
    });
  }
  /** Da posible nombre al correo al seleccionar el dominio*/
  getdomainss(event) {
    this.correo = 'alias' + '@' + event;
    this.correo = this.correo.toLowerCase();

    this.getnuevocorreo(this.correo);
  }
 /** Toma el correo y valida si esta disponible o no*/
  getnuevocorreo( email) {

    this.correoService.validarCorreo(this.tipodominio, email).subscribe(response => {
      this.estadoCorreo3 = response;
      this.aliasModificadoform.controls['emailmodificado'].setValue(email);
      if (this.estadoCorreo3 === true) {
        this.estadofinal = 'Correo ya en uso';
      } else if (this.estadoCorreo3 === false) {
        this.estadofinal = 'Correo disponible';
        this.correo = email;

      }
    });
  }
 /** Toma el correo escrito en el input y valida si esta disponible o no*/
  getcorreofinal(event) {
    if (event) {
      this.correoService.validarCorreo(this.tipodominio, event).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.estadofinal = 'Correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.correo = event;
          this.estadofinal = 'Correo disponible';
        }
      });
    }
  }
  /** metodo que llama el boton guardar- cambia en nombre del correo en el api, y modifica el nombre del usuario*/
  guardar() {
    const objeto = {
      'id': this.idCorreo,
      'email': this.correo,
    };
    this.correoService.cambiarAlias(this.tipodominioCorreo, objeto).subscribe(response => {
      this.correoService.postSocketDatosTiUsuarios({status: 'ok'}).subscribe(x => {});
      this.estadofinalias = response;
      this.dialogService.open(SaveModificadoComponent, {});
      this.ref.close();
    });
  }

  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {

    this.getTipo();
    this.aliasModificadoform = this.fb.group({
      'dominio': new FormControl(),
      'emailmodificado': new FormControl(),
    });
  }

}
