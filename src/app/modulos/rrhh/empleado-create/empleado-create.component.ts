import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Puesto, PuestoData} from '../../../@core/data/interfaces/catalogos/puesto';
import {PuestoTipo, PuestoTipoData} from '../../../@core/data/interfaces/catalogos/puesto-tipo';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {Empresa, EmpresaData} from '../../../@core/data/interfaces/catalogos/empresa';
import {Banco, BancoData} from '../../../@core/data/interfaces/catalogos/banco';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {Sede, SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {Preempleados, PreempleadosData} from '../../../@core/data/interfaces/rrhh/preempleados';
import {Grupo, GrupoData} from '../../../@core/data/interfaces/catalogos/grupo';
import {RadioButton} from 'primeng/primeng';
import {MarcaEmpresarial, MarcaEmpresarialData} from '../../../@core/data/interfaces/catalogos/marca-empresarial';
import {Turno, TurnoData} from '../../../@core/data/interfaces/catalogos/turno';
import {TurnoEmpleado, TurnoEmpleadoData} from '../../../@core/data/interfaces/catalogos/turno-empleado';

/**
 * Componente para crear un nuevo empleado.
 */

@Component({
  selector: 'ngx-empleado-create',
  templateUrl: './empleado-create.component.html',
  styleUrls: ['./empleado-create.component.scss'],
})
export class EmpleadoCreateComponent implements OnInit {
  marcaEmpresarial: MarcaEmpresarial[];
  marcasEmpresariales: any[];
  /** Expresión regular para sólo letras */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Variable que permite la inicialización del formulario para crear un empleado. */
  empleadoForm: FormGroup;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  puestos: Puesto[];
  /** Arreglo auxiliar para puesto. */
  puesto: any[];
  /** Arreglo que se inicializa con los valores del campo Tipo de Puesto desde el Microservicio. */
  puestoTipo: PuestoTipo[];
  /** Arreglo auxiliar para puestoTipo. */
  puestotipo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para puesto. */
  empresa: any[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del campo Banco desde el Microservicio. */
  bancos: Banco[];
  /** Arreglo auxiliar para bancos. */
  banco: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  turnos: TurnoEmpleado[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /** Arreglo auxiliar para turno. */
  turnoEmpleado: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Variable para conocer el resultado de la foto tomada al empleado. */
  resultado: any;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Turnos: Turno[];
  /** Arreglo auxiliar para Turnos. */
  turno: any[];
  /**Variables que almacenan el valor de los radiobutton al dar check*/
  @ViewChild('radioCuenta')
  radioCuenta: RadioButton;
  @ViewChild('radioClave')
  radioClave: RadioButton;
  /**variable que almacena le nombre delprecandidato*/
  nombre: any;
  /**variables para establecer maximo y minimo de caracteres en cuenta clave*/
  maxNum: any;
  minNum: any;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**Variable para saber si ya existe en numero de empleado**/
  existeNumeroEmpleado: boolean = false;
  /**Nombre del epmleado**/

  tamano: boolean;

  uploadedFiles: any[] = [];

  fileToUpload: File = null;
  preEmpleados: Preempleados;
  idEtapa: string;
  idTipoPuesto: string;
  idCandidato: number = this.rutaActiva.snapshot.params.idCandidato;

  idPrecandidato: number = this.rutaActiva.snapshot.params.idPrecandidato;
  guardarEmpleado: any;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarTurno: boolean;
  mostrarSubarea: boolean;
  mostrarPuesto: boolean;
  error: any;
  mostrarMarcasEmpresariales: boolean;
  /**
   * El método constructor manda a llamar Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {ActivatedRoute} rutaActiva Establece la ruta que se encuentra activa actualmente.
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {PuestoData} puestoService Servicio del componente puesto para poder invocar los métodos.
   * @param {PuestoTipoData} puestoTipoService Servicio del componente puesto-tipo para poder invocar los métodos.
   * @param {SubareaData} subareaService Servicio del componente subarea para poder invocar los métodos.
   * @param {EmpresaData} empresaService Servicio del componente empresa para poder invocar los métodos.
   * @param {BancoData} bancoService Servicio del componente banco para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes).
   * @param {EmpleadosData} empleadoService Servicio del componente Empleados para poder invocar los métodos.
   * @param {Router} router Variable para navegar entre componentes.
   */
  constructor(private rutaActiva: ActivatedRoute,
              private fb: FormBuilder,
              protected  puestoService: PuestoData,
              protected  puestoTipoService: PuestoTipoData,
              protected subareaService: SubareaData,
              protected empresaService: EmpresaData,
              protected bancoService: BancoData,
              private dialogService: NbDialogService,
              protected empleadoService: EmpleadosData,
              protected areaService: AreaData,
              protected sedeService: SedeData,
              protected turnoService: TurnoEmpleadoData,
              private router: Router,
              protected preEmpleadoService: PreempleadosData,
              private grupoService: GrupoData,
              protected empleadosService: EmpleadosData,
              private toastrService: NbToastrService,
              protected servicePreempleado: PreempleadosData,
  ) {
  }
  /** Función que permite obtener los valores del campo Puesto desde el Microservicio
   * y los datos obtenidos los almacena en el arreglo puesto. */
  getPuesto() {
    this.puestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.puestos = data;
      if (this.puestos.length === 0) {
        this.mostrarPuesto = true;
      }else {
        this.mostrarPuesto = false;
      }
      this.puesto = [];
      for (let i = 0; i < this.puestos.length; i++) {
        this.puesto.push({'label': this.puestos[i].nombre, 'value': this.puestos[i].id});
      }
    });
  }

  /** Función que permite obtener los valores del campo Tipo de Puesto desde el Microservicio
   * y los datos obtenidos los almancena en el arreglo puestoTipo. */
  getPuestoTipo() {
    this.puestoTipoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.puestoTipo = result;
      this.puestotipo = [];
      for (let i = 0; i < this.puestoTipo.length; i++) {
        this.puestotipo.push({'label': this.puestoTipo[i].nombre, 'value': this.puestoTipo[i].id});
      }
    });
  }

  /** Función que permite obtener los valores del campo Subarea desde el Microservicio
   *  y los datos obtenidos los almacena en el arreglo subArea. */
  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }


  /** Función que permite obtener los valores del campo Empresa desde el Microservicio
   *  y los datos obtenidos los almacena en el arreglo empresa. */
  // idsubarea
  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  /***/
  getTurnos() {
    this.turnoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Turnos = data;
    });
  }

  getPreEmpleado() {
    this.preEmpleadoService.getPreEmpleadoById(this.rutaActiva.snapshot.params.idCandidato).pipe(map(data => this.preEmpleados = data))
      .subscribe(data => {
        this.empleadoForm.controls['idPuesto'].setValue(this.preEmpleados.idPuesto);
        this.empleadoForm.controls['idTipoPuesto'].setValue(this.preEmpleados.idTipoPuesto);
        this.empleadoForm.controls['idArea'].setValue(this.preEmpleados.idArea);
        this.empleadoForm.controls['idSubarea'].setValue(this.preEmpleados.idSubarea);
        this.empleadoForm.controls['idSede'].setValue(this.preEmpleados.idSede);
        this.empleadoForm.controls['idEmpresa'].setValue(this.preEmpleados.idEmpresa);
        this.empleadoForm.controls['idGrupo'].setValue(this.preEmpleados.idGrupo);
        // this.empleadoForm.controls['idMarcaEmpresarial'].setValue(this.preEmpleados.idMarcaEmpresarial);
        this.getEmpresa(this.preEmpleados.idGrupo);
        this.getSedes(this.preEmpleados.idEmpresa);
        this.getArea(this.preEmpleados.idSede);
        this.getSubarea(this.preEmpleados.idArea);
        this.getPuestoTipo();
        this.nombre = this.preEmpleados.preEmpleado;
      });
  }
/**Funcion para detectar que el numero de empleado ya se encuentra registrado**/
  buscarNumeroEmpleado(numeroEmpleado) {
    if (numeroEmpleado !== '' && numeroEmpleado !== '.' && numeroEmpleado !== '´') {
      // this.existeNumeroEmpleado = false;
      this.empleadosService.getEmpleadoById(numeroEmpleado)
        .subscribe(data => {
          if (typeof data.id === 'number') {
            this.showToast('danger');
            this.existeNumeroEmpleado = false;
          }else {
            this.existeNumeroEmpleado = true;
          }
        },
          error1 => {
          this.error = error1;
          if (error1) {
            this.existeNumeroEmpleado = true;
          }
          });
    }else {
      this.existeNumeroEmpleado = false;
    }

  }

  /** Función que permite obtener los valores del campo Banco desde el Microservicio
   *  y los datos obtenidos los almacena en el arreglo bancos. */
  getBanco() {
    this.bancoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.bancos = result;
      this.banco = [];
      for (let i = 0; i < this.bancos.length; i++) {
        this.banco.push({'label': this.bancos[i].nombre, 'value': this.bancos[i].id});
      }
    });
  }

  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }
  getSedes(idEmpresa) {
    this.sedeService.get().pipe(map(result => {
      return result.filter(valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarSede = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }

  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      } else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  /** Función que despliega un modal. */
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  /** Esta función permite guardar al empleado validando primero si la información contenida en el formulario de empleado está completa.*/
  saveEmpleados() {
    this.inactivo = true;
    this.guardarEmpleado = {
      'id': Number(this.empleadoForm.value.id),
      'idBanco': this.empleadoForm.value.idBanco,
      'idCandidato': this.empleadoForm.value.idCandidato,
      'idSubarea': this.empleadoForm.value.idSubarea,
      'idPuesto': this.empleadoForm.value.idPuesto,
      'idTipoPuesto': this.empleadoForm.value.idTipoPuesto,
      'puestoDetalle': this.empleadoForm.value.puestoDetalle,
      'sueldoDiario': Number(this.empleadoForm.value.sueldoDiario),
      'sueldoMensual': Number(this.empleadoForm.value.sueldoMensual),
      'kpi': 0,
      'ctaClabe': this.empleadoForm.value.ctaClabe,
      'tarjetaSecundaria': this.empleadoForm.value.tarjetaSecundaria,
      'kpiMensual': Number(this.empleadoForm.value.kpiMensual),
      'kpiTrimestral': Number(this.empleadoForm.value.kpiTrimestral),
      'kpiSemestral': Number(this.empleadoForm.value.kpiSemestral),
      'comentarios': this.empleadoForm.value.comentarios,
      'fechaIngreso': this.empleadoForm.value.fechaIngreso,
      'idTurnoEmpleado': this.empleadoForm.value.idTurnoEmpleado,
    };
    this.submitted = true;
    if (this.empleadoForm.invalid) {
      return;
    }
    if (this.guardarEmpleado.kpiMensual !== 0 &&
      this.guardarEmpleado.kpiTrimestral !== 0 &&
      this.guardarEmpleado.kpiSemestral !== 0) {
      this.guardarEmpleado.kpi = 1;
    }
    this.empleadoService.saveEmpleado(this.guardarEmpleado, this.rutaActiva.snapshot.params.idPrecandidato, 3).subscribe(
      result => {
    },
      error => { },
      () => {
        this.empleadoService.postSocket(this.guardarEmpleado).subscribe(x => {});
        this.servicePreempleado.postSocket(this.guardarEmpleado).subscribe(x => {});
        this.empleadosService.postSocketImss(this.guardarEmpleado).subscribe(x => {});
        this.router.navigate(['modulos/rrhh/empleados']);
      },
    );
  }

  habilita() {
    this.empleadoForm.controls['ctaClabe'].setValue('');
    if (this.radioCuenta.checked) {
      this.maxNum = 10;
      this.minNum = 10;
    }else if (this.radioClave.checked) {
      this.maxNum = 16;
      this.minNum = 16;
    }
  }

  // onUpload(event) {
  //   for (let file of event.files) {
  //     this.uploadedFiles.push(file);
  //   }
  // }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);

    const newObject = {
      'lastModified': this.fileToUpload.lastModified,
      // 'lastModifiedDate' : this.fileToUpload.lastModifiedDate,
      'name': this.fileToUpload.name,
      'size': this.fileToUpload.size,
      'type': this.fileToUpload.type,
    };

    const fileJSON = JSON.stringify(newObject);
    //
    // console.log(this.fileToUpload);
    // console.log(fileJSON);
  }
  /** Método para mostrar un mensaje en pantalla, en este caso, un mensaje de advertencia. */
  showToast(status) {
    this.toastrService.danger('Atencion', `Número de Empleado ya se encuentra registrado`);
  }

  validarEmpleado(idEmpleado) {
  /**Aqui se validara que el numero de empleado ingresado no exista*/

  }
  /** Función que inicializa el componente, mandando a llamar a todos los getters e inicializando el formulario "empleadoForm"  */
  ngOnInit() {
  this.existeNumeroEmpleado = false;
    // this.getPuestoTipo();
    this.getBanco();
    this.getPreEmpleado();
    this.getGrupo();
    this.getPuesto();
    this.getTurnos();
    this.empleadoForm = this.fb.group({
      'id': new FormControl('', Validators.required),
      'fechaIngreso': new FormControl('', Validators.required),
      'idPuesto': new FormControl('', Validators.required),
      'idTipoPuesto': new FormControl('', Validators.required),
      'puestoDetalle': new FormControl(''),
      'idSubarea': new FormControl('', Validators.required),
      'idArea': new FormControl('', Validators.required),
      'idSede': new FormControl('', Validators.required),
      'idGrupo': new FormControl('', Validators.required),
      'sueldoDiario': new FormControl('', Validators.compose([Validators.required, Validators.max(5000), Validators.min(88)])),
      'sueldoMensual': new FormControl('', Validators.compose([Validators.required, Validators.max(50000), Validators.min(2600)])),
      /**'rfc': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][1-9][1-9][A-Z-0-9][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(13), Validators.minLength(13)])),*/
      /**'imss': new FormControl('', Validators.compose([Validators.maxLength(11), Validators.minLength(11)])),*/
      'ctaClabe': new FormControl('', Validators.required),
      // 'ctaClabe': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(this.max),
      //   Validators.minLength(this.min)])),
      'idBanco': new FormControl('', Validators.required),
      'idEmpresa': new FormControl('', Validators.required),
      'kpiMensual': new FormControl('', Validators.compose([
        Validators.max(10000), Validators.min(0), Validators.maxLength(5)])),
      'kpiTrimestral': new FormControl('', Validators.compose([Validators.max(10000), Validators.min(0)])),
      'kpiSemestral': new FormControl('', Validators.compose([Validators.max(10000), Validators.min(0)])),
      'idImagen': 1,
      'idEstado': 1,
      'idCandidato': +this.rutaActiva.snapshot.params.idCandidato,
      'foto': 1,
      'recontratable': 1,
      'idHuella': 1,
      'documentos': null,
      'idTipoUsuario': 1,
      'kpi': 0,
      'idTurnoEmpleado': new FormControl('', Validators.required),
      'tarjetaSecundaria': new FormControl(''),
    });
    /*this.empleadoService.getFoto(3041).subscribe( result => {
      this.foto = new Blob([result], {type: 'image/jpeg'});
    });*/
  }
}
