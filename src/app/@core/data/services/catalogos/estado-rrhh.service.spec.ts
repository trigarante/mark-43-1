import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {EstadoRrhhService} from './estado-rrhh.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('EstadoRrhh service', () => {
  let injector: TestBed;
  let service: EstadoRrhhService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 16,
    estado: 'TEST UNICO',
    activo: 0,
  },
    {
      id: 'razonsocial',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EstadoRrhhService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EstadoRrhhService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/estadosRrhh');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/estadosRrhh', '/16');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/estadosRrhh');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/estadosRrhh', '/16/74');
      });
    });
  }
})
