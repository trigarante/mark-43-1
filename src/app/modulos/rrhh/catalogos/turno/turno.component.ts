import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TurnoCreateComponent} from '../modals/turno-create/turno-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {Turno, TurnoData} from '../../../../@core/data/interfaces/catalogos/turno';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-turno',
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.scss'],
})
export class TurnoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  turnoss: Turno[];
  turno: Turno;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {TurnoData} turnoService Servicio del componente turno para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio para diálogos (mensajes). */
  constructor(private turnoService: TurnoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'turnos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTurnos', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTurnos(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que despliega un modal para agregar un nuevo turno. */
  crearTurno() {
    this.dialogoService.open(TurnoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getTurnos(1);
    });
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Turnos`);
  }
  /** Función que obtiene el campo Turno. */
  getTurnos(mensajeAct) {
    this.turnoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.turnoss = [];
      this.turnoss = data;
      this.dataSource = new MatTableDataSource(this.turnoss); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcón que obtiene la información de un turno según su ID. */
  getTurnoById(idTurno) {
    this.turnoService.getTurnoById(idTurno).subscribe(data => {
      this.turno = data;
    });
  }
  /** Función que despliega un modal en el cual se puede actualizar la información de un turno. */
  updateTurno(idTurno) {
    this.dialogoService.open(TurnoCreateComponent, {
      context: {
        idTipo: 2,
        idTurno: idTurno,
      },
    }).onClose.subscribe(x => {
      this.getTurnos(1);
    });
  }
  /** Función que despliega un modal para seleccionar si se da de baja un turno. */
  bajaTurno(idTurno) {
    this.getTurnoById(idTurno);
    swal({
      title: '¿Deseas dar de baja este turno?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.turno['activo'] = 0;
        this.turnoService.getTurnoById(idTurno).subscribe(data => {
          if (data.activo === 1) {
            this.turnoService.put(idTurno, this.turno).subscribe( result => {
              this.turnoService.postSocket({idTurno: idTurno, turno: this.turno}).subscribe(x => {});
            });
            swal('¡El turno se ha dado de baja exitosamente!', {
              icon: 'success',
            }).then((resultado => {
              this.getTurnos(0);
            }));
          }else {
            swal('¡Ateción el turno ya ha sido dado de baja!', {
              icon: 'warning',
            }).then((resultado => {
              this.getTurnos(0);
            }));
          }
        });
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.cols = ['turno', 'horario', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/catalogos/turno') {
    this.getTurnos(0);
    //   }
    // }, 1000);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.turno.activo;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.empleados.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
