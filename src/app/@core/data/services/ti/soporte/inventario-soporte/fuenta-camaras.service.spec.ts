import { TestBed } from '@angular/core/testing';

import { FuentaCamarasService } from './fuenta-camaras.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Corriente Camaras Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    folio: '1111',
    idMarca: 3,
    fechaRecepcion: '2019-09-12T16:45:26.000+0000',
    fechaSalida: '2019-09-12T16:45:26.000+0000',
    activo: 1,
    usuario: null,
    nombreMarca: 'SAMSUMG',
    origenRecurso: 'renta',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'corrienteCamara',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [FuentaCamarasService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/corrienteCamara');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/corrienteCamara', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/corrienteCamara');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/corrienteCamara', '/1/74');
      });
    });
  }
});
