import {Observable} from 'rxjs';

export interface Empresa {
  id: number;
  nombre: string;
  descripcion: string;
  idGrupo: number;
  activo: number;
  codigo: string;
}

export abstract class EmpresaData {
  abstract get(): Observable<Empresa[]>;

  abstract post(empresa: Empresa): Observable<Empresa>;

  abstract getEmpresasById(idEmpresa): Observable<Empresa>;

  abstract put(idEmpresa, empresa): Observable<Empresa>;

}
