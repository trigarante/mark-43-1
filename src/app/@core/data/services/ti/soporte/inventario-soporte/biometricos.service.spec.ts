import { TestBed } from '@angular/core/testing';

import { BiometricosService } from './biometricos.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

describe('BiometricosService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'FALLA ELECTRICA',
    idEstadoInventario: 4,
    idObservacionesInventario: 1,
    folio: '6565',
    idMarca: 2,
    fechaRecepcion: '2019-07-26T20:26:44.000+0000',
    fechaSalida: null,
    activo: 0,
    usuario: null,
    nombreMarca: 'HP',
    origenRecurso: 'renta',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'biometricos',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [BiometricosService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/biometricos');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/biometricos', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/biometricos');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/biometricos', '/1/74');
      });
    });
  }
});
