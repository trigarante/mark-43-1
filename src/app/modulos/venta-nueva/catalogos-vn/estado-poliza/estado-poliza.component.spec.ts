import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoPolizaComponent } from './estado-poliza.component';

describe('EstadoPolizaComponent', () => {
  let component: EstadoPolizaComponent;
  let fixture: ComponentFixture<EstadoPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
