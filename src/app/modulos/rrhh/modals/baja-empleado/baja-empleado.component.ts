import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EstadoRrhh, EstadoRrhhData} from '../../../../@core/data/interfaces/catalogos/estado-rrhh';
import {NbDialogRef} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/** Componente del modal "baja empleado". */
@Component({
  selector: 'ngx-baja-empleado',
  templateUrl: './baja-empleado.component.html',
  styleUrls: ['./baja-empleado.component.scss'],
})

export class BajaEmpleadoComponent implements OnInit {
  /** Variable que guarda el idPrecandidato obtenido. */
  @Input() idPrecandidato: number;
  /** Variable que guarda el idEmpleado obtenido. */
  @Input() idEmpleado: number;
  /** Variable que permite la inicialización del formulario para realizar la baja de un empleado. */
  bajaEmpleadoForm: FormGroup;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los valores del campo campana (campaña) desde el Microservicio. */
  estadosRrhh: EstadoRrhh[];
  /** Arreglo auxiliar para estadosRrhh */
  estados = [{'label': '', 'value': 0}];
  /** Lista de elementos para los valores de recontratable. */
  recontratable: SelectItem[];
  empleadoEliminado: boolean;
  // Web Sockets
  /**
   * El método constructor manda llamar a Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EstadoRrhhData} estadoRrhhService Servicio del componente estado-rrhh para poder invocar los métodos.
   * @param  NbDialogRef<BajaEmpleadoComponent>} ref Referencia al diálogo (mensaje).
   * @param {EmpleadosData} empleadoService Servicio del componente empleado para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected estadoRrhhService: EstadoRrhhData, private ref: NbDialogRef<BajaEmpleadoComponent>,
              protected empleadosService: EmpleadosData,
              protected empleadoService: EmpleadosData, private router: Router) {
  }
  // Web socket connection
  // connect() {
  //   const socket = new SockJS('https://www.mark-43.net:445/empleadosbaja');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({}, frame =>  {
  //     console.log(frame);
  //   });
  //   // const socket2 = new SockJS('https://www.mark-43.net:445/empleados');
  //   // this.stompClientE = Stomp.over(socket2);
  //   // this.stompClientE.connect({}, frame =>  {
  //   //   console.log(frame);
  //   // });
  // }

  /** Función que obtiene los valores del campo estados-rrhh del Microservicio. */
  getEstadosRrhh() {
    this.estadoRrhhService.get().subscribe(data => this.estadosRrhh = data);
    this.estadoRrhhService.get().pipe(map(data => {
      /**return data.filter( value => value.idEstadoRh === 0);*/
      return data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
    })).subscribe(data => {
      this.estados = [];
      for (let i = 0; i < data.length; i++) {
        this.estados.push({'label': data[i].estado, 'value': data[i].id});
      }
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que realiza la baja de un empleado, resetea el formulario y redirecciona a la página. */
  bajaEmpleado() {
    let fechaBaja: any = this.bajaEmpleadoForm.value.fechaBaja;
    fechaBaja = Date.parse(fechaBaja);
    this.empleadosService.getEmpleadoById(this.idEmpleado).subscribe(data => {
      if (data.idEtapa === 3 && data.idEstadoRH === 1 || data.idEstadoRH === 16) {
        this.empleadoService.bajaEmpleado(this.idPrecandidato, this.idEmpleado, this.bajaEmpleadoForm.value, fechaBaja).
        subscribe(result => {
          // this.stompClient.send('/tasks/saveEmpleadoBaja', {}, JSON.stringify(this.bajaEmpleadoForm.value));
          this.empleadosService.postSocketBaja(this.bajaEmpleadoForm.value).subscribe(x => {});
          this.empleadoService.postSocket(this.bajaEmpleadoForm.value).subscribe(x => {});
          // this.stompClientE.send('/tasks/saveEmpleado', {}, JSON.stringify(this.bajaEmpleadoForm.value));
          this.router.navigate(['modulos/rrhh/empleados']);
          this.bajaEmpleadoForm.reset();
          this.ref.close();
          /**location.reload();*/
        });
      }else {
        this.empleadoEliminado = true;
      }
    });
  }

  /** Función que inicializa la lista "recontratable", agregándole los valores de "Si" y "No". */
  getOpcionesRecontratable() {
    this.recontratable = [];
    this.recontratable.push({'label': 'Si', 'value': true});
    this.recontratable.push({'label': 'No', 'value': false});
  }

  blockearDate() {
    const date = new Date().toISOString().split('T')[0];
    document.getElementsByName('somedate')[0].setAttribute('min', date);
  }

  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    // this.connect();
    this.blockearDate();
    this.getEstadosRrhh();
    this.bajaEmpleadoForm = this.fb.group({
      'idEstadoRH': new FormControl('', Validators.required),
      'detalleBaja': new FormControl('', Validators.required),
      'fechaBaja': new FormControl('', Validators.required),
      'recontratable': new  FormControl('', Validators.required),
    });
    this.getOpcionesRecontratable();
  }

}
