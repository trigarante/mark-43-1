import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource, TooltipPosition} from '@angular/material';
import {HistorialLlamadas, HistorialLlamadasData} from '../../../@core/data/interfaces/ventaNueva/historial-llamadas';
import {ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'ngx-historial-llamadas',
  templateUrl: './historial-llamadas.component.html',
  styleUrls: ['./historial-llamadas.component.scss'],
})
export class HistorialLlamadasComponent extends MatPaginatorIntl implements OnInit {

  /**Variable que alamcena parametro por URL*/
  @Input() idSolicitud: number = this.rutaActiva.snapshot.params.idSolicitud;
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  dataSource: any;
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  historial: HistorialLlamadas[];

  loading: boolean;


  constructor(private historialLlamadaService: HistorialLlamadasData, private rutaActiva: ActivatedRoute) {

    super();

    this.loading = true;

    this.getAndInitTranslations();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getAndInitTranslations() {
    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

  ngOnInit() {
    this.cols = ['detalle', 'nombre', 'numero', 'fechaRegistro', 'titulo', 'comentario'];
    this.getHistorial(this.idSolicitud);
  }

  getHistorial(idSolicitud) {
    this.historialLlamadaService.get(idSolicitud).subscribe(valor => {
      this.historial = [];
      this.historial = valor;
      this.dataSource = new MatTableDataSource(this.historial); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    // if (mensajeAct === 1) {
    //   this.showToast();
    // }
  }

}
