import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlLlamadasComponent } from './control-llamadas.component';

describe('ControlLlamadasComponent', () => {
  let component: ControlLlamadasComponent;
  let fixture: ComponentFixture<ControlLlamadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlLlamadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlLlamadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
