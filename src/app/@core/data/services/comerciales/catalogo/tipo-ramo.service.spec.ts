import { TestBed } from '@angular/core/testing';
import { TipoRamoService } from './tipo-ramo.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../matchers';

fdescribe('TipoRamoService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    tipo: 'SEGURO',
    activo: 0,
  },
    {
      id: 'tiporamo',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoRamoService, HttpClient],
  }));
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tipo-ramo');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tipo-ramo', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tipo-ramo');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tipo-ramo', '/1/74');
      });
    });
  }
});
