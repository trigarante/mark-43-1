import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCompetenciaCreateComponent } from './tipo-competencia-create.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {TipoCompetenciaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-cometencia';

describe('TipoCompetenciaCreateComponent', () => {
  let component: TipoCompetenciaCreateComponent;
  let fixture: ComponentFixture<TipoCompetenciaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCompetenciaCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
      ],
      providers: [
        TipoCompetenciaData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCompetenciaCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
