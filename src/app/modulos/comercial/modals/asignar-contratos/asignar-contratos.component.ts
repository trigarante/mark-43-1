import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Contratos, ContratosData} from '../../../../@core/data/interfaces/comerciales/contratos';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-asignar-contratos',
  templateUrl: './asignar-contratos.component.html',
  styleUrls: ['./asignar-contratos.component.scss'],
})
export class AsignarContratosComponent implements OnInit {
  @Input() idSocio: number;
  @Input() idTipo: number;
  submitted: Boolean;
  contratosCreateForm: FormGroup;
  contratos: Contratos;
  min: Date;
  inactivo: boolean;
  dataSource: any;
  // socket
  disabled = true;
  datos: any[] = [];
  newDatos: any;
  minDate = new Date();

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected contratosService: ContratosData) {
  }

  guardarContrato() {
    this.submitted = true;
    if (this.contratosCreateForm.invalid) {
      return;
    }
    let fechaInicio: any;
    let fechaFin: any;
    fechaInicio = Date.parse(this.contratosCreateForm.controls['fechaInicio'].value);
    fechaFin = Date.parse(this.contratosCreateForm.controls['fechaFin'].value);
    this.contratosCreateForm.controls['idSocio'].setValue(this.idSocio);
    this.contratosService.post(this.contratosCreateForm.value, fechaInicio, fechaFin).subscribe((result) => {
      this.contratosService.postSocket(this.contratosCreateForm.value).subscribe(x => {});
      this.contratosCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/contratos']);
    });
  }

  getConveniosById() {
    this.contratosService.getContratoById(this.idSocio).subscribe(data => {
      this.contratosCreateForm.controls['fechaInicio'].setValue(new Date(data.fechaInicio));
      this.contratosCreateForm.controls['fechaFin'].setValue(new Date(data.fechaFin));
      this.contratosCreateForm.controls['referencia'].setValue(data.referencia);
      this.contratosCreateForm.controls['idSocio'].setValue(data.idSocio);
    });
  }

  updateConveio() {
    this.inactivo = true;
    this.contratosService.put(this.idSocio, this.contratosCreateForm.value)
    .subscribe(x => {
      this.contratosService.postSocket(this.contratosCreateForm.value).subscribe(x => {});
      this.ref.close();
    });
  }

  dismiss() {
    this.ref.close();
  }
  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getConveniosById();
    }
    this.contratosCreateForm = this.fb.group({
      'idSocio': new FormControl(),
      'fechaInicio': new FormControl('', Validators.required),
      'fechaFin': new FormControl('', Validators.required),
      'referencia': new FormControl('', [Validators.required, Validators.minLength(4)]),
      'activo': 1,
    });
  }

}
