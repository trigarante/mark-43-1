import {Observable} from 'rxjs';

export interface ProductoCliente {
  id: number;
  idCliente: number;
  idSolicitud: number;
  idCategoriaSocio: number;
  datos: string;
}

export abstract class ProductoClienteData {
  abstract get(): Observable<ProductoCliente[]>;

  abstract post(productoCliente): Observable<ProductoCliente>;

  abstract getProductoClienteById(idProductoCliente): Observable<ProductoCliente>;

  abstract put(idProductoCliente, productoCliente): Observable<ProductoCliente>;

  abstract entryAseguradora(aseguradora);

  abstract returnAseguradora(): Observable<number>;
}
