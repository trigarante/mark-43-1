import { Component } from '@angular/core';

@Component({
  selector: 'ngx-catalogos-soporte',
  template: '<router-outlet></router-outlet>',
})
export class CatalogosSoporteComponent  {

}
