import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {PaisesComponent} from '../../paises/paises.component';
import {Paises, PaisesData} from '../../../../../@core/data/interfaces/catalogos/paises';
import {Router} from '@angular/router';
/** Componente del modal del catálogo para "crear países". */
@Component({
  selector: 'ngx-paises-create',
  templateUrl: './paises-create.component.html',
  styleUrls: ['./paises-create.component.scss'],
})
export class PaisesCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idPais obtenido. */
  @Input() idPais: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un país. */
  paisesCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  paises: Paises;
  paisEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {PaisesData} paisesService Servicio del componente paises para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<PaisesComponent>, private paisesService: PaisesData,
              private router: Router) {
  }
  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarPaises() {
  if (this.paisesCreateForm.invalid) {
    return;
  }
  this.submitted = true;
  this.paisesService.post(this.paisesCreateForm.value).subscribe( (result) => {
    this.paisesService.postSocket({idPais: this.idPais}).subscribe(x => {});
    this.paisesCreateForm.reset();
    this.router.navigate( ['/modulos/rrhh/catalogos/paises']);
    this.ref.close();
    //window.location.reload();
  });
  }

  /** Función que actualiza la información del campo país. */
  updatePaises() {
    this.paisesService.getPaisesById(this.idPais).subscribe(data => {
      if(data.activo === 1){
        this.paisesService.put(this.idPais, this.paisesCreateForm.value).subscribe( result => {
          this.paisesService.postSocket({idPais: this.idPais}).subscribe(x => {});
          this.paisesCreateForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.paisEliminado =true;
      }
    });

  }

  /** Función que da de baja un país, cambiando su atributo activo a 0. */
  /*bajaPaises() {
    this.paisesCreateForm.controls['activo'].setValue(0);
    this.paisesService.put(this.idPais, this.paisesCreateForm.value).subscribe( result => {
      this.paisesCreateForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que obtiene toda la información del campo país dependiendo de su ID. */
  getPaisesById() {
  this.paisesService.getPaisesById(this.idPais).subscribe(data => {
    this.paises = data;
    this.paisesCreateForm.controls['nombre'].setValue(this.paises.nombre);
  });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getPaisesById();
    }
    this.paisesCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }
}
