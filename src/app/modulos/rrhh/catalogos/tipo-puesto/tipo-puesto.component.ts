import {Component, OnInit, ViewChild} from '@angular/core';
import {PuestoTipo, PuestoTipoData} from '../../../../@core/data/interfaces/catalogos/puesto-tipo';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {TipoPuestoCreateComponent} from '../modals/tipo-puesto-create/tipo-puesto-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-puesto',
  templateUrl: './tipo-puesto.component.html',
  styleUrls: ['./tipo-puesto.component.scss'],
})
export class TipoPuestoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  puestoTipo: PuestoTipo[];
  /** Arreglo para el tipo de puesto que se dará de baja. */
  tipoPuesto: PuestoTipo;
  dataSource: any;
  permisos: any;
  escritura: boolean = false;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {PuestoTipoData} puestoTipoService Servicio del componente puesto-tipo para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes (diálogos). */
  constructor(private puestoTipoService: PuestoTipoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tipopuesto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoPuesto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoPuesto(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene los puestos y los asigna al arreglo puestoTipo. */
  getTipoPuesto(mensajeAct) {
    this.puestoTipoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.puestoTipo = [];
      this.puestoTipo = data;
      this.dataSource = new MatTableDataSource(this.puestoTipo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Función que obtiene la información del tipo de puesto dependiendo de su ID. */
  getTipoPuestoById(idPuestoTipo) {
    this.puestoTipoService.getPuestoTipoById(idPuestoTipo).subscribe(data => {
      this.tipoPuesto = data;
    });
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo-Puesto`);
  }

  /** Función que despliega un modal del componente "TipoPuestoCreateComponent" para poder "crear un tipo de puesto". */
  crearTipoPuesto() {
    this.dialogoService.open(TipoPuestoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getTipoPuesto(0);
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de un tipo de puesto. */
  updateTipoPuesto(idPuestoTipo) {
    this.dialogoService.open(TipoPuestoCreateComponent, {
      context: {
        idTipo: 2,
        idPuestoTipo: idPuestoTipo,
      },
    }).onClose.subscribe(x => {
      this.getTipoPuesto(1);
    });
  }

  bajaTipoPuesto(idPuestoTipo) {
    this.getTipoPuestoById(idPuestoTipo);
    swal({
      title: '¿Deseas dar de baja este tipo de puesto?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoPuesto['activo'] = 0;
        this.puestoTipoService.put(idPuestoTipo, this.tipoPuesto).subscribe( result => {
          this.puestoTipoService.postSocket({idPuestoTipo: idPuestoTipo, tipoPuesto: this.tipoPuesto}).subscribe(x => {});
        });
        swal('¡El tipo de puesto se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoPuesto(0);
        }));
      }
    });
  }

  ngOnInit() {
    this.connect();
   this.cols = ['nombre', 'acciones'];
   this.getPermisos();
   this.estaActivo();
    this.getTipoPuesto(0);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoDePuesto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoDePuesto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
