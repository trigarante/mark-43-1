import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Divisas, DivisasData} from '../../../../../@core/data/interfaces/comerciales/catalogo/divisas';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';

@Component({
  selector: 'ngx-divisa-create',
  templateUrl: './divisa-create.component.html',
  styleUrls: ['./divisa-create.component.scss'],
})
export class DivisaCreateComponent implements OnInit {
  @Input() idDivisa: number;
  @Input() idTipo: number;
  divisaCreateForm: FormGroup;
  submitted: Boolean;
  divisa: Divisas;
  constructor(private fb: FormBuilder, private divisasService: DivisasData, protected ref: NbDialogRef<SociosComponent>,
              ) { }
  guardarDivisa() {
    this.submitted = true;
    if (this.divisaCreateForm.invalid) {
      return;
    }
    this.divisasService.post(this.divisaCreateForm.value).subscribe((result) => {
      this.divisasService.postSocket({status: 'ok'}).subscribe(x => {});
      this.divisaCreateForm.reset();
      this.ref.close();
    });
  }
  getDivisaById() {
    this.divisasService.getDivisaById(this.idDivisa).subscribe(data => {
      this.divisa = data;
      this.divisaCreateForm.controls['divisa'].setValue(data.divisa);
      this.divisaCreateForm.controls['descripcion'].setValue(data.descripcion);
    });
  }
  updateDivisa() {
    this.divisasService.put(this.idDivisa, this.divisaCreateForm.value).subscribe(result => {
      this.divisasService.postSocket({status: 'ok'}).subscribe(x => {});
      this.divisaCreateForm.reset();
      this.ref.close();
    });
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getDivisaById();
    }
    this.divisaCreateForm = this.fb.group({
      'divisa': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }
  dismiss() {
    this.ref.close();
  }
}
