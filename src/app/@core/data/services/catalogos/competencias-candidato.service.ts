import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CompetenciaCandidatoData, CompetenciasCandidato} from '../../interfaces/catalogos/competencias-candidato';

@Injectable()
export class CompetenciasCandidatoService extends CompetenciaCandidatoData {
  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  get(): Observable<CompetenciasCandidato[]> {
    return this.http.get<CompetenciasCandidato[]>(this.baseURL + 'v1/competencias-candidatos');
  }
  post(competenciasCandidato: CompetenciasCandidato): Observable<CompetenciasCandidato> {
    return this.http.post<CompetenciasCandidato>(this.baseURL + 'v1/competencias-candidatos', competenciasCandidato);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'competenciascandidato/saveCompetenciaCandidato', json);
  }
  getCompetenciaCandidatoById(idCompetenciasCandidato): Observable <CompetenciasCandidato> {
    return this.http.get<CompetenciasCandidato>(this.baseURL + 'v1/competencias-candidatos/' + idCompetenciasCandidato);
  }
  put(idCompetenciasCandidato, competenciaCandidato: CompetenciasCandidato): Observable<CompetenciasCandidato> {
    return this.http.put<CompetenciasCandidato>(this.baseURL + 'v1/competencias-candidatos/' + idCompetenciasCandidato
      + '/' + sessionStorage.getItem('Empleado'), competenciaCandidato);
  }
}
