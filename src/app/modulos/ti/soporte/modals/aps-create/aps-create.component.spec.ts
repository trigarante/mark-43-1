import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApsCreateComponent } from './aps-create.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {ObservacionesInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {ApsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';

describe('ApsCreateComponent', () => {
  let component: ApsCreateComponent;
  let fixture: ComponentFixture<ApsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApsCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        ApsData,
        EstadoInventarioData,
        ObservacionesInventarioData,
        MarcaSoporteData,
        SedeData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApsCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
