import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPolizaCreateComponent } from './tipo-poliza-create.component';

describe('TipoPolizaCreateComponent', () => {
  let component: TipoPolizaCreateComponent;
  let fixture: ComponentFixture<TipoPolizaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPolizaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPolizaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
