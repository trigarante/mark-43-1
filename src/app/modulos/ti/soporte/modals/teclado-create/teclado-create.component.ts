import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ObservacionesInventario,
  ObservacionesInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';

import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';

import {Teclado, TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
// import {SedeData, Sede} from '../../../../../@core/data/interfaces/catalogos/sede';
import {SelectItem} from 'primeng/api';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-teclado-create',
  templateUrl: './teclado-create.component.html',
  styleUrls: ['./teclado-create.component.scss'],
})
export class TecladoCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTeclado: number;
  teclado: Teclado;
  tecladoCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;

  constructor(private tecladoService: TecladoData, private fb: FormBuilder, protected ref: NbDialogRef<TecladoCreateComponent>,
              private router: Router, private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private sedesService: TipoSedeData) { }
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map( data => this.observaciones = data)).subscribe( data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({'label': this.observaciones[i].observaciones, 'value': this.observaciones[i].id});
      }
    });
  }
  guardarTeclado() {
    if (this.tecladoCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.tecladoService.post(this.tecladoCreateForm.value).subscribe((result) => {
      this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tecladoCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'teclado' ]);
    });
    // window.location.reload();

  }
  updateTeclado() {
    this.tecladoService.put(this.idTeclado, this.tecladoCreateForm.value).subscribe( result => {
      this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tecladoCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'teclado' ]);
    });
  }
  getTecladoByid() {
    this.tecladoService.getTecladoById(this.idTeclado).subscribe(data => {
      this.teclado = data;
      this.tecladoCreateForm.controls['idMarca'].setValue(this.teclado.idMarca);
      this.tecladoCreateForm.controls['idEstadoInventario'].setValue(this.teclado.idEstadoInventario);
      this.tecladoCreateForm.controls['idObservacionesInventario'].setValue(this.teclado.idObservacionesInventario);
      this.tecladoCreateForm.controls['comentarios'].setValue(this.teclado.comentarios);
      this.tecladoCreateForm.controls['idEmpleado'].setValue(this.teclado.idEmpleado);
      this.tecladoCreateForm.controls['idTipoSede'].setValue(this.teclado.idTipoSede);
      this.tecladoCreateForm.controls['origenRecursos'].setValue(this.teclado.origenRecursos);
    });
  }
  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTecladoByid();
    }
    this.getMarca();
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getSedes();
    this.getOrigen();
    this.tecladoCreateForm = this.fb.group({
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecursos': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });

  }

}
