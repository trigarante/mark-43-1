import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TarjetasVn, TarjetasVnData} from '../../../interfaces/ventaNueva/catalogos/tarjetas-vn';

@Injectable()
export class TarjetasVnService extends TarjetasVnData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<TarjetasVn[]> {
    return this.http.get<TarjetasVn[]>(this.baseURL + 'v1/tarjetas');
  }

  getTarjetaById(id): Observable<TarjetasVn> {
    return this.http.get<TarjetasVn>(this.baseURL + 'v1/tarjetas' + id);
  }

  post(tarjeta): Observable<TarjetasVn> {
    return this.http.post<TarjetasVn>(this.baseURL + 'v1/tarjetas', tarjeta);
  }

  put(id, tarjeta): Observable<TarjetasVn> {
    return this.http.put<TarjetasVn>(this.baseURL + 'v1/tarjetas' + id, tarjeta);
  }
}
