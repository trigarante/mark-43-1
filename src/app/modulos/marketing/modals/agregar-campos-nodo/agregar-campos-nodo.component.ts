import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {GuardadoComponent} from '../guardado/guardado.component';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';

@Component({
  selector: 'ngx-agregar-campos-nodo',
  templateUrl: './agregar-campos-nodo.component.html',
  styleUrls: ['./agregar-campos-nodo.component.scss'],
})
export class AgregarCamposNodoComponent implements OnInit {
  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  /** variable que se puede obtener el nombre de la aseguradora*/
  @Input() nombrenodo: string;
  /** posicion del array de la aseguradora*/
  @Input() posicionnodo: number;
  @Input() tiponombre: string;
  agregarForm: FormGroup;
  pagina: Pagina;
  aseguradoras: any;
  /** propiedades del radio button*/
  favoriteSeason: string;
  seasons: string[] = ['TEXTO', 'NUMERO', 'TRUE/FALSE'];
  seleccion2: any[];

  constructor(private paginaService: PaginasData, private fb: FormBuilder, protected ref: NbDialogRef<AgregarCamposNodoComponent>,
              public dialogService: NbDialogService, private router: Router) {
    this.agregarForm = this.fb.group({
      'nombreCampo': new FormControl('', Validators.compose([Validators.required])),
      'valor': new FormControl('', Validators.compose([Validators.required])),
      'tipo': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  seleccion() {
    this.seleccion2 = [];

    this.seleccion2.push({'label': 'true', 'value': true});
    this.seleccion2.push({'label': 'false', 'value': false});
  }
  getIdpagina(event) {
        this.agregarForm.controls['nombreCampo'].setValue('');
        this.agregarForm.controls['valor'].setValue('');
  }
  guardarCampos() {

    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);


      this.aseguradoras[this.tiponombre].aseguradoras[this.posicionnodo][this.nombrenodo][0]
        [this.agregarForm.value.nombreCampo] = this.agregarForm.value.valor;

      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);
      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));
      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocketDatosNodo({status: 'ok'}).subscribe(x => {});
        this.ref.close();
      });

      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      });

    });
  }

  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.seleccion();
  }

}
