import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {Procesador, ProcesadorSoporteData} from '../../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {ProcesadorComponent} from '../../procesador/procesador.component';

@Component({
  selector: 'ngx-crear-procesador',
  templateUrl: './crear-procesador.component.html',
  styleUrls: ['./crear-procesador.component.scss'],
})
export class CrearProcesadorComponent implements OnInit {
  /**Varible que almacena el ID de la marca para actualizar datos*/
  @Input() idProcesador: number;
  /**Variable que almacena el tipo de datos que se motraran*/
  @Input() idTipo: number;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  procesadorCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Variable que se inicializa con los valores del Microservicio. */
  procesador: Procesador;

  constructor(private fb: FormBuilder,
              protected ref: NbDialogRef<ProcesadorComponent>,
              private router: Router, protected procesadorSoporteService: ProcesadorSoporteData) { }

  ngOnInit() {
    this.procesadorCreateForm = this.fb.group({
      'nombreProcesador': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getProcesadorById();
    }
  }
  guardarProcesador() {
    if (this.procesadorCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.procesadorSoporteService.post(this.procesadorCreateForm.value).subscribe((result) => {
      this.procesadorSoporteService.postSocket({status: 'ok'}).subscribe(x => {});
      this.procesadorCreateForm.reset();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/procesadores']);
      this.ref.close();
    });
  }

  /** Función que obtiene la información de un procesador dependiendo de su ID. */
  getProcesadorById() {
    this.procesadorSoporteService.getProcesadorSoporte(this.idProcesador).subscribe(data => {
      this.procesador = data;
      this.procesadorCreateForm.controls['nombreProcesador'].setValue(this.procesador.nombreProcesador);
    });
  }
  updateProcesador() {
    this.procesadorSoporteService.put(this.idProcesador, this.procesadorCreateForm.value).subscribe( result => {
      this.procesadorSoporteService.postSocket({status: 'ok'}).subscribe(x => {});
      this.procesadorCreateForm.reset();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/procesadores']);
      this.ref.close();
    });
  }

  dismiss() {
    this.ref.close();
  }


}
