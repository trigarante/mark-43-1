import {Component, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {Extension, ExtensionData} from '../../../@core/data/interfaces/ti/extension';
import {CrearExtensionComponent} from '../modals/crear-extension/crear-extension.component';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { environment } from '../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-extensiones',
  templateUrl: './extensiones.component.html',
  styleUrls: ['./extensiones.component.scss'],
})

export class ExtensionesComponent implements OnInit {
  ExtensionesForm: FormGroup;
  /**arreglos de las area*/
  areas: any[];
  subareas: Area[];
  /** arrelos de las subareas*/
  area: any[];
  subarea: Subarea[];
  extensiones: Extension[];
  cols: any[];
  tipoarea: string;
  subareanuevo: Subarea;
  nombre: string;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private areaService: AreaData, private fb: FormBuilder, private subareaService: SubareaData,
              private extensionesService: ExtensionData, public dialogService: NbDialogService, private router: Router) {
    this.ExtensionesForm = this.fb.group({
      'subareacontrol': new FormControl(),
      'areacontrol': new FormControl(),
    });
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getArea();
    this.getExtensiones();
    this.cols = [
      'id',
      'subarea',
      'area',
      'descripcion',
      'estado',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getArea() {
    this.areaService.get().pipe(map(data => this.subareas = data)).subscribe(data => {
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }
  /** consigue todas las subareas o campañas de un area de ejecutivo*/
  getsubareas(event) {
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.area = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.area.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});
      }
    });
  }

  getExtensiones() {
    this.extensionesService.getExtension().subscribe(data => {
      this.extensiones = data;
      this.dataSource = new MatTableDataSource(this.extensiones);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  getExtension() {
    this.dialogService.open(CrearExtensionComponent, {
    }).onClose.subscribe(x => {
      this.getExtensiones();
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'extensiones');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelExtensiones', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.getExtensiones();
        }, 500);
      });
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.extensiones.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.extensiones.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
