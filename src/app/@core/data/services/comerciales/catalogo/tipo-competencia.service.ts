import {Injectable} from '@angular/core';
import {TipoCometencia, TipoCompetenciaData} from '../../../interfaces/comerciales/catalogo/tipo-cometencia';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class TipoCompetenciaService extends TipoCompetenciaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoCometencia[]> {
    return this.http.get<TipoCometencia[]>(this.baseURL + 'v1/tipo-competencia');
  }

  post(tipoCompetencia: TipoCometencia): Observable<TipoCometencia> {
    return this.http.post<TipoCometencia>(this.baseURL + 'v1/tipo-competencia', tipoCompetencia);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tipocompetencia/saveTipoCompetencia', json);
  }

  getTipoCompetenciaById(idTipoCompetencia): Observable<TipoCometencia> {
    return this.http.get<TipoCometencia>(this.baseURL + 'v1/tipo-competencia/' + idTipoCompetencia);
  }

  put(idTipoCompetencia, tipoCompetencia: TipoCometencia): Observable<TipoCometencia> {
    return this.http.put<TipoCometencia>(this.baseURL + 'v1/tipo-competencia/' + idTipoCompetencia + '/'
      + sessionStorage.getItem('Empleado'), tipoCompetencia);
  }
}
