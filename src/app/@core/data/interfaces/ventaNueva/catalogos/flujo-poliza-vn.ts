import {Observable} from 'rxjs';

export interface FlujoPolizaVn {
  id: number;
  estado: string;
  activo: number;
}

export abstract class FlujoPolizaData {
  abstract get(): Observable<FlujoPolizaVn[]>;

  abstract post(flujoPoliza): Observable<FlujoPolizaVn>;

  abstract getFlujoPolizaById(idFlujoPoliza): Observable<FlujoPolizaVn>;

  abstract put(idFlujoPoliza, flujoPoliza): Observable<FlujoPolizaVn>;

  abstract postSocket(json): Observable<JSON>;
}
