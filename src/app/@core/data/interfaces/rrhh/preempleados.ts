import {Observable} from 'rxjs';
import * as moment from 'moment';
import _date = moment.unitOfTime._date;

export interface Preempleados {
  idPrecandidato: number;
  idCandidato: number;
  idEtapa: number;
  preEmpleado: string;
  idMarcaEmpresarial: number;
  idPuesto: string;
  idSubarea: string;
  vacante: string;
  empresa: string;
  idSede: string;
  idTipoPuesto: string;
  idArea: string;
  idEstadoRh: number;
  idEmpresa: string;
  idGrupo: string;
  calificacionExamen?: number;
  calificacionPsicometrico?: number;
  comentarios?: string;
  fechaIngreso?: _date;
  fechaNacimiento?: _date;
  email?: string;
  genero?: string;
  estadoCivil?: string;
  escolaridad?: string;
  estadoEscolar?: string;
  cp?: string;
  colonia?: string;
  calle?: string;
  numeroExterior?: number;
  numeroInterior?: number;
  telefonoFijo?: string;
  telefonoMovil?: string;
  fechaCreacion?: _date;
  fechaBaja?: _date;
  tiempoTraslado?: number;
  medio?: string;
  estacion?: string;
  lineas?: string;
  estadoRH?: string;
  curp?: string;
  detalleBaja?: string;
  fechaRegistroBaja?: _date;
  nombreEtapa?: string;
  puesto?: string;
  puestoTipo?: string;
  tipoUsuario?: string;
  sede?: string;
  subarea?: string;
  grupo?: string;
  recontratable?: number;
}

export abstract class PreempleadosData {
  abstract get(): Observable<Preempleados[]>;
  abstract postSocket(json): Observable<JSON>;
  abstract getPreEmpleadoById(idPreEmpleado): Observable<Preempleados>;
}
