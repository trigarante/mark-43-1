import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EtapasModulos, EtapasModulosData} from '../../interfaces/catalogos/etapasModulos';

@Injectable()
export class EtapasModulosService extends EtapasModulosData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<EtapasModulos[]> {
    return this.http.get<EtapasModulos[]>(this.baseURL + 'v1/etapas');
  }

  createEtapasModulos(etapasModulos: EtapasModulos): Observable<EtapasModulos> {
    return this.http.post<EtapasModulos>(this.baseURL + 'v1/etapas', etapasModulos);
  }

  updateEtapasModulos(idEtapaModulo, etapasModulos: EtapasModulos): Observable<EtapasModulos> {
    return this.http.put<EtapasModulos>(this.baseURL + 'v1/etapas/' + idEtapaModulo, etapasModulos);
  }

  getEtapasModulosById(idEtapaModulo): Observable<EtapasModulos> {
    return this.http.get<EtapasModulos>(this.baseURL + 'v1/etapas/' + idEtapaModulo);
  }
}
