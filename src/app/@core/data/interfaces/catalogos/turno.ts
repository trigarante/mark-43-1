import {Observable} from 'rxjs';

export interface Turno {
  id: number;
  turno: string;
  horario: string;
  activo: number;
}

export abstract class TurnoData {
  abstract get(): Observable<Turno[]>;

  abstract post(turno): Observable<Turno>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTurnoById(idTurno): Observable<Turno>;

  abstract put(idTurno, turno): Observable<Turno>;

  // abstract putBaja(idTurno): Observable<Turno>;
}
