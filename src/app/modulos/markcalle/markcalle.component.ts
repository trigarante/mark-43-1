import { Component} from '@angular/core';

@Component({
  selector: 'ngx-markcalle',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MarkcalleComponent {}
