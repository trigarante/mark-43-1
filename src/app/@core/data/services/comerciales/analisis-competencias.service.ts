import {Injectable} from '@angular/core';
import {AnalisisCompetencia, AnalisisCompetenciaData} from '../../interfaces/comerciales/analisis-competencia';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable()
export class AnalisisCompetenciasService extends AnalisisCompetenciaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<AnalisisCompetencia[]> {
    return this.http.get<AnalisisCompetencia[]>(this.baseURL + 'v1/analisis-competencias');
  }

  post(analisisCompetencia: AnalisisCompetencia): Observable<AnalisisCompetencia> {
    return this.http.post<AnalisisCompetencia>(this.baseURL + 'v1/analisis-competencias', analisisCompetencia);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'analisis-competencia/saveACompetencia/1', json);
  }

  getAnalisisCompetenciaById(idAnalisisCompetencia): Observable<AnalisisCompetencia> {
    return this.http.get<AnalisisCompetencia>(this.baseURL + 'v1/analisis-competencias/' + idAnalisisCompetencia);
  }

  put(idAnalisisCompetencia, analisisCompetencia: AnalisisCompetencia): Observable<AnalisisCompetencia> {
    return this.http.put<AnalisisCompetencia>(this.baseURL + 'v1/analisis-competencias/' + idAnalisisCompetencia + '/'
      + sessionStorage.getItem('Empleado') , analisisCompetencia);
  }
}
