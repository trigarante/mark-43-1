import {Component, Input, NgZone, OnInit} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {FileInfo, MIME_TYPE_FOLDER} from '../../../@core/data/interfaces/drive/fileinfo';
import {BreadCrumbItem} from '../../../@core/data/interfaces/drive/bread-crumb-item';
import {AppContext} from '../../../@core/data/services/app.context';
import {FilesUploadComponent} from '../filesupload/filesupload.component';
import {MatBottomSheet, MatDialog} from '@angular/material';
import {Userd} from '../../../@core/data/interfaces/drive/userd';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-log-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss'],
})
export class DocumentosComponent implements OnInit {
  breadCrumbItems: BreadCrumbItem[] = [];
  dataSource: any;
  displayedColumns: string[] = ['icon', 'name', 'tipo', 'modifiedTime', 'size', 'delete'];
  files: FileInfo[] = [];
  users: Userd[] = [];
  selectedUserId: string;
  documento: string = this.rutaActiva.snapshot.params.documento;
  id: string = this.rutaActiva.snapshot.params.id;
  $state: any;
  constructor(
    private appContext: AppContext,
    private bottomSheet: MatBottomSheet,
    private zone: NgZone,
    public dialog: MatDialog,
    private router: Router,
    private rutaActiva: ActivatedRoute,
  ) {
    this.dataSource = new MatTableDataSource(this.files);
    this.appContext.Session.File.uploadFinished.subscribe(() => {
      this.refresh(this.appContext.Session.BreadCrumb.currentItem.Id);
    });
    this.$state = this.router.getCurrentNavigation().extras.state.title;
  }

  signOut() {
    this.appContext.Session.Gapi.signOut();
    this.router.navigate(['/modulos/rrhh/empleados']);
  }

  browse(file: FileInfo) {
    if (file.IsFolder) {
      this.appContext.Repository.File.getFiles(file.Id)
        .then((res) => {
          this.zone.run(() => {
            this.files = res;
            this.dataSource.data = this.files;
            this.appContext.Session.BreadCrumb.navigateTo(file.Id, file.Name);
            this.breadCrumbItems = this.appContext.Session.BreadCrumb.items;
          });
        });
    }
  }
  //
  // createNewFolder() {
  //   const data: DialogOneInputData = new DialogOneInputData();
  //   data.DefaultInputText = 'Untitled folder';
  //   data.Title = 'New folder'
  //   const dialogRef = this.dialog.open(DialogOneInputComponent, {
  //     width: '250px',
  //     data: data,
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.appContext.Repository.File.create(
  //         '1ojzbD1HpKPZ2GQp-NzaVkBEo-qpHyL_6',
  //         result)
  //         .then((res) => {
  //           this.refresh('1ojzbD1HpKPZ2GQp-NzaVkBEo-qpHyL_6');
  //         });
  //     }
  //
  //   });
  // }

  delete(file: FileInfo) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
      this.appContext.Repository.File.delete(file.Id)
        .then(() => {
          this.zone.run(() => {
            this.dataSource.data = this.files;
          });
        });
    }
  }

  ngOnInit() {
    this.users = this.appContext.Repository.User.getAll();
    this.selectedUserId = this.users[0].Id;
    this.appContext.Session.BreadCrumb.init();
    this.breadCrumbItems = this.appContext.Session.BreadCrumb.items;
    this.refresh(this.documento);
   this.appContext.Session.BreadCrumb.currentItem.Id = this.documento;
   // this.$state = this.router.events.pipe(filter(e => e instanceof NavigationStart),
   //   map( () => this.router.getCurrentNavigation().extras.state));
   //  this.$state = this.rutaActiva.snapshot.queryParams.get('title');
    // console.log(this.$state);
  }

  // onSelectedItemChanged($event: BreadCrumbItem) {
  //   const fileInfo: FileInfo = new FileInfo();
  //   fileInfo.Id = $event.Id;
  //   fileInfo.Name = $event.Name;
  //   fileInfo.MimeType = MIME_TYPE_FOLDER;
  //   this.browse(fileInfo);
  // }

  createNewFile($event) {
    // this.appContext.Session.BreadCrumb.currentItem.Name = 'CURP EMPLEADO ' + this.id;
      this.bottomSheet.open(FilesUploadComponent, { data: $event.target.files});
  }

  refresh(fileId: string) {
    this.appContext.Repository.File.getFiles(fileId)
      .then((res) => {
        this.zone.run(() => {
          this.files = res;
          this.dataSource.data = this.files;
        });
      });
  }

}
