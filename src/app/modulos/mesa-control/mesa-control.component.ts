import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-mesa-control',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MesaControlComponent {
}
