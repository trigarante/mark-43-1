import {Observable} from 'rxjs';

export interface Drive {
  tipo: String;
  fecha: Date;
  numero: string;
  downloadLink: String;
}

export abstract class DriveData {
  abstract post(files: FileList, registro?: string , registros?: Array<string>): Observable<string>;
  abstract get(fechaInicio: Date, fechaFin: Date, ACD: boolean, manual: boolean, numero: string): Observable<Drive[]>;
}
