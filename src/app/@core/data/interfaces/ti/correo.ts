import {Observable} from 'rxjs';

export interface Correo {
  id: number;
  name: string;
  lastname: string;
  email: any;
  emails?: any;
  orgUnitPath: string;
  primaryEmail: string;
}
export interface Domains {
  domainName: string;
}

export interface Alias {
  alias: string;
}

export interface UnidadOrganizativa {
  name: string;
  orgUnitPath: string;
}

export abstract class CorreoData {
  abstract get(tipo): Observable<Correo[]>;
  abstract getById(tipo: string, id: string): Observable<Correo>;
  abstract validarCorreo(tipo, alias);
  abstract cambiarAlias(tipo, jsonCorreo);

  abstract enviarByAlias(tipo, jsonCorreo);
  abstract postSocket(json): Observable<JSON>;
  abstract postSocketDatosTiUsuarios(json): Observable<JSON>;
  abstract post(tipo, jsonCorreo): Observable<string>;
  abstract getDominios(tipo): Observable<Domains[]>;
  abstract getAlias(tipo, idCorreo): Observable<Alias>;
  abstract deleteAlias(tipo, id, alias);
  abstract getUnidadesOrganizativas(tipo): Observable<UnidadOrganizativa[]>;
}
