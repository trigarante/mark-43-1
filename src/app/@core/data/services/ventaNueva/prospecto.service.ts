import {Injectable} from '@angular/core';
import {Prospecto, ProspectoData} from '../../interfaces/ventaNueva/prospecto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProspectoService extends ProspectoData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Prospecto[]> {
    return this.http.get<Prospecto[]>(this.baseURL + 'v1/prospectovn');
  }

  post(prospecto: Prospecto): Observable<Prospecto> {
    return this.http.post<Prospecto>(this.baseURL + 'v1/prospectovn/', prospecto);
  }

  getProspectoById(idProspecto): Observable<Prospecto> {
    return this.http.get<Prospecto>(this.baseURL + 'v1/prospectovn/' + idProspecto);
  }

  put(idProspecto, prospecto: Prospecto): Observable<Prospecto> {
    return this.http.put<Prospecto>(this.baseURL + 'v1/prospectovn/' + idProspecto, prospecto);
  }

  validaCorreo(correo): Observable<Prospecto> {
    return this.http.get<Prospecto>(this.baseURL + 'v1/prospectovn/correo/' + correo);
  }

}
