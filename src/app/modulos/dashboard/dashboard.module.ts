import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { MatCardModule} from '@angular/material';

@NgModule({
  imports: [
    ThemeModule,
    MatCardModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
