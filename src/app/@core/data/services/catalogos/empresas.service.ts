import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Empresa, EmpresaData} from '../../interfaces/catalogos/empresa';

@Injectable()
export class EmpresasService extends EmpresaData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(this.baseURL + 'v1/empresa');
  }

  post(empresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(this.baseURL + 'v1/empresa', empresa);
  }

  getEmpresasById(idEmpresa): Observable<Empresa> {
    return this.http.get<Empresa>(this.baseURL + 'v1/empresa/' + idEmpresa);
  }

  put(idEmpresa, empresa: Empresa) {
    return this.http.put<Empresa>(this.baseURL + 'v1/empresa/' + idEmpresa, empresa);
  }
}
