import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {Switchs, SwitchsData} from '../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {Servidores, ServidoresData} from '../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {TarjetaRed, TarjetaRedData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIp, TelefonoIpData} from '../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {Racks, RacksData} from '../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanel, PatchPanelData} from '../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRack, CharolasRackData} from '../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {NbDialogService} from '@nebular/theme';
import {SwitchsCreateComponent} from '../modals/switchs-create/switchs-create.component';
import {ServidoresCreateComponent} from '../modals/servidores-create/servidores-create.component';
import {TarjetaRedCreateComponent} from '../modals/tarjeta-red-create/tarjeta-red-create.component';
import {TelefonoIpCreateComponent} from '../modals/telefono-ip-create/telefono-ip-create.component';
import {RacksCreateComponent} from '../modals/racks-create/racks-create.component';
import {PatchPanelCreateComponent} from '../modals/patch-panel-create/patch-panel-create.component';
import {CharolasRackCreateComponent} from '../modals/charolas-rack-create/charolas-rack-create.component';
import {AsignarEquipoComponent} from '../modals/asignar-equipo/asignar-equipo.component';
import swal from 'sweetalert';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import swal2 from 'sweetalert2';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-inventario-redes',
  templateUrl: './inventario-redes.component.html',
  styleUrls: ['./inventario-redes.component.scss'],
})
export class InventarioRedesComponent implements OnInit {
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;
  /** Variable que inicializa las columnas*/
  cols: any[];
  dataSource: any;
  switch: Switchs[];
  servidores: Servidores[];
  tarjetaRed: TarjetaRed[];
  telefonoIp: TelefonoIp[];
  racks: Racks[];
  patchPanel: PatchPanel[];
  charolaRack: CharolasRack[];
  switchdesactivar: Switchs;
  servidoresdesactivar: Servidores;
  tarjetaReddesactivar: TarjetaRed;
  telefonoIpdesactivar: TelefonoIp;
  racksdesactivar: Racks;
  patchPaneldesactivar: PatchPanel;
  charolaRackdesactivar: CharolasRack;
  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  tipoEquipos: string;
  // sockets
  actualizar: string;
  private stompClient1 = null;
  private stompClient2 = null;
  private stompClient3 = null;
  private stompClient4 = null;
  private stompClient5 = null;
  private stompClient6 = null;
  private stompClient7 = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    private rutaActiva: ActivatedRoute,
    private switchsService: SwitchsData,
    private servidoresService: ServidoresData,
    private tarjetaRedService: TarjetaRedData,
    private telefonoIdService: TelefonoIpData,
    private racksService: RacksData,
    private patchPanelService: PatchPanelData,
    private charolasRackService: CharolasRackData,
    public dialogService: NbDialogService,
    private estadoInventarioServive: EstadoInventarioData,
    private router: Router,
  ) {}
  connect() {
    const _this = this;
    /**----------------------switch-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'switch');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, (frame) => {
      _this.stompClient1.subscribe('/task/panelSwitch', (content) => {
        if (this.actualizar === 'switch' || this.tipoEquipo === 'switch') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('switch');
            _this.getEquipo();
          }, 500);
        }
      });
    });
    /**----------------------servidores-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'servidores');
    this.stompClient2 = Stomp.over(socket1);
    this.stompClient2.connect({}, (frame) => {
      _this.stompClient2.subscribe('/task/panelServidores', (content) => {
        if (this.actualizar === 'servidores') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('servidores');
          }, 500);
        }
      });
    });
    /**----------------------tarjeta de red-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'targetared');
    this.stompClient3 = Stomp.over(socket2);
    this.stompClient3.connect({}, (frame) => {
      _this.stompClient3.subscribe('/task/panelTargetaRed', (content) => {
        if (this.actualizar === 'tarjeta de red') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('tarjeta de red');
          }, 500);
        }
      });
    });
    /**----------------------telefonos ip-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'telefonoip');
    this.stompClient4 = Stomp.over(socket3);
    this.stompClient4.connect({}, (frame) => {
      _this.stompClient4.subscribe('/task/panelTelefonoIP', (content) => {
        if (this.actualizar === 'telefonos ip') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('telefonos ip');
          }, 500);
        }
      });
    });
    /**----------------------rack-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'rack');
    this.stompClient5 = Stomp.over(socket4);
    this.stompClient5.connect({}, (frame) => {
      _this.stompClient5.subscribe('/task/panelRack', (content) => {
        if (this.actualizar === 'rack') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('rack');
          }, 500);
        }
      });
    });
    /**----------------patch panel-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'patchpanel');
    this.stompClient6 = Stomp.over(socket5);
    this.stompClient6.connect({}, (frame) => {
      _this.stompClient6.subscribe('/task/panelPatchPanel', (content) => {
        if (this.actualizar === 'patch panel') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('patch panel');
          }, 500);
        }
      });
    });
    /**----------------charolas rack-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'charolasrack');
    this.stompClient7 = Stomp.over(socket6);
    this.stompClient7.connect({}, (frame) => {
      _this.stompClient7.subscribe('/task/panelCharolasRack', (content) => {
        if (this.actualizar === 'charolas rack') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('charolas rack');
          }, 500);
        }
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  ngOnInit() {
    this.connect();
    this.getEstado();
    this.cols = ['detalle', 'id', 'nombre', 'usuario', 'folio', 'marca', 'activo', 'acciones'];
    this.getEquipo();
    this.getPermisos();
    this.estaActivo();
  }

  getEquipo() {
    switch (this.tipoEquipo) {
      case 'switch':
        this.switchsService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.switch = data;
          this.dataSource = new MatTableDataSource(this.switch);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'servidores':
        this.servidoresService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.servidores = data;
          this.dataSource = new MatTableDataSource(this.servidores);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'tarjetadered':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'tarjeta de red':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'telefonosip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'telefonos ip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'racks':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.racks = data;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'rack':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.racks = data;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'patchpanel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.patchPanel = data;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'patch panel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.patchPanel = data;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'charolasrack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.charolaRack = data;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'charolas rack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.charolaRack = data;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      default: break;
    }
    this.tipoEquipos = this.tipoEquipo;
  }
  /** Se muestra un menu con el componente TAB, con el selectedTabChange se selecciona que datos mostrara en la tabla*/
  getEquipos(tipoEquipo) {
    tipoEquipo = tipoEquipo.toLowerCase();
    this.actualizar = tipoEquipo;
    this.tipoEquipos = tipoEquipo;
    switch (tipoEquipo) {
      case 'switch':
        this.switchsService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.switch = data;
          this.dataSource = new MatTableDataSource(this.switch);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        });
        this.tipoEquipo = 'switch';
      break;
      case 'servidores':
        this.servidoresService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.servidores = data;
          this.dataSource = new MatTableDataSource(this.servidores);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'servidores';
      break;
      case 'tarjeta de red':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'tarjeta de red';
      break;
      case 'telefonos ip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'telefonos ip';
      break;
      case 'rack':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.racks = data;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'rack';
      break;
      case 'patch panel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.patchPanel = data;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'patch panel';
      break;
      case 'charolas rack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.charolaRack = data;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'charolas rack';
      break;
      default: break;
    }
  }

  /** funcion que envia a un modal para crear un switch*/
  crearSwitch() {
    this.dialogService.open(SwitchsCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }
  /** funcion que envia a un modal para modificar un switch*/
  updateSwitch(idSwitch) {
    this.dialogService.open(SwitchsCreateComponent, {
      context: {
        idTipo: 2,
        idSwitch: idSwitch,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear un servidor*/
  crearServidores() {
    this.dialogService.open(ServidoresCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un servidor*/
  updateServidores(idServidores) {
    this.dialogService.open(ServidoresCreateComponent, {
      context: {
        idTipo: 2,
        idServidores: idServidores,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear una tarjeta de red*/
  crearTarjetaRed() {
    this.dialogService.open(TarjetaRedCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un tarjeta de red*/
  updateTarjetaRed(idTarjetaRed) {
    this.dialogService.open(TarjetaRedCreateComponent, {
      context: {
        idTipo: 2,
        idTarjetaRed: idTarjetaRed,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear un telefono*/
  crearTelefonoIp() {
    this.dialogService.open(TelefonoIpCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un telefono*/
  updateTelefonoIp(idTelefonoIp) {
    this.dialogService.open(TelefonoIpCreateComponent, {
      context: {
        idTipo: 2,
        idTelefonoIp: idTelefonoIp,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  crearRacks() {
    this.dialogService.open(RacksCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }
  updateRacks(idRacks) {
    this.dialogService.open(RacksCreateComponent, {
      context: {
        idTipo: 2,
        idRacks: idRacks,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  crearPatchPanel() {
    this.dialogService.open(PatchPanelCreateComponent , {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  updatePatchPanel(idPatchPanel) {
    this.dialogService.open(PatchPanelCreateComponent, {
      context: {
        idTipo: 2,
        idPatchPanel: idPatchPanel,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  crearCharolaRack() {
    this.dialogService.open(CharolasRackCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  updateCharolasRack(idCharolasRack ) {
    this.dialogService.open(CharolasRackCreateComponent, {
      context: {
        idTipo: 2,
        idCharolasRack: idCharolasRack,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** Funcion que manda un modal para asignar un equipo a un empleado*/
  asignarEquipo(id, tipoEquipos) {
    this.dialogService.open(AsignarEquipoComponent, {
      context: {
        id: id,
        tipoEquipos: tipoEquipos,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  getSwitchById(id) {
    this.switchsService.getSwitchsById(id).subscribe(data => {
      this.switchdesactivar = data;
    });
  }
  getServidoresById(id) {
    this.servidoresService.getServidoresById(id).subscribe(data => {
      this.servidoresdesactivar = data;
    });
  }
  getTarjetaRedById(id) {
    this.tarjetaRedService.getTarjetaRedById(id).subscribe(data => {
      this.tarjetaReddesactivar = data;
    });
  }
  getTelefonosIpById(id) {
    this.telefonoIdService.getTelefonoIpById(id).subscribe(data => {
      this.telefonoIpdesactivar = data;
    });
  }
  getRackById(id) {
    this.racksService.getRacksById(id).subscribe(data => {
      this.racksdesactivar = data;
    });
  }
  getPatchPanelById(id) {
    this.patchPanelService.getPatchPanelById(id).subscribe(data => {
      this.patchPaneldesactivar = data;
    });
  }
  getCharolasRackById(id) {
    this.charolasRackService.getCharolasRackById(id).subscribe(data => {
      this.charolaRackdesactivar  = data;
    });
  }
  /**funcion para desasingnar el equipo de un empleado*/
  desasignarEquipo(idEquipo) {

    switch (this.tipoEquipos) {
      case 'switch':
        this.getSwitchById(idEquipo);
        break;
      case 'servidores':
        this.getServidoresById(idEquipo);
        break;
      case 'tarjeta de red':
        this.getTarjetaRedById(idEquipo);
        break;
      case 'telefonos ip':
        this.getTelefonosIpById(idEquipo);
        break;
      case 'rack':
        this.getRackById(idEquipo);
        break;
      case 'patch panel':
        this.getPatchPanelById(idEquipo);
        break;
      case 'charolas rack':
        this.getCharolasRackById(idEquipo);
        break;
      default:
        break;
    }
    swal({
      title: '¿Deseas desvincular este equipo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Desvincular',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        switch (this.tipoEquipos) {
          case 'switch':
            this.switchdesactivar['idEmpleado'] = null;
            this.switchsService.put(idEquipo, this.switchdesactivar).subscribe(result => {
              this.switchsService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'servidores':
            this.servidoresdesactivar['idEmpleado'] = null;
            this.servidoresService.put(idEquipo, this.servidoresdesactivar).subscribe(result => {
              this.servidoresService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'tarjeta de red':
            this.tarjetaReddesactivar['idEmpleado'] = null;
            this.tarjetaRedService.put(idEquipo, this.tarjetaReddesactivar).subscribe(result => {
              this.tarjetaRedService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'telefonos ip':
            this.telefonoIpdesactivar['idEmpleado'] = null;
            this.telefonoIdService.put(idEquipo, this.telefonoIpdesactivar).subscribe(result => {
              this.telefonoIdService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'rack':
            this.racksdesactivar['idEmpleado'] = null;
            this.racksService.put(idEquipo, this.racksdesactivar).subscribe(result => {
              this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'patch panel':
            this.patchPaneldesactivar['idEmpleado'] = null;
            this.patchPanelService.put(idEquipo, this.patchPaneldesactivar).subscribe(result => {
              this.patchPanelService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'charolas rack':
            this.charolaRackdesactivar['idEmpleado'] = null;
            this.charolasRackService.put(idEquipo, this.charolaRackdesactivar).subscribe(result => {
              this.charolasRackService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          default:
            break;
        }
        swal('¡El equipo se ha desvinculado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          // window.location.reload();
          this.getEquipo();
        }));
      }
    });
  }
  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 0);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({
          'id': this.estadoInventariodata[i].id,
          'value': this.estadoInventariodata[i].estado,
        });

      }
    });
  }
  /** Función que desprende un modal para dar de baja un Equipo. */
  async eliminarEquipo(idEquipo, tipoEquipo) {

    /** switch de seleccion dependiendo el tipo de Equipo que de el usuario hara la busqueda por idEquipo*/
    switch (this.tipoEquipos) {
      case 'switch':
        this.getSwitchById(idEquipo);
        break;
      case 'servidores':
        this.getServidoresById(idEquipo);
        break;
      case 'tarjeta de red':
        this.getTarjetaRedById(idEquipo);
        break;
      case 'telefonos ip':
        this.getTelefonosIpById(idEquipo);
        break;
      case 'rack':
        this.getRackById(idEquipo);
        break;
      case 'patch panel':
        this.getPatchPanelById(idEquipo);
        break;
      case 'charolas rack':
        this.getCharolasRackById(idEquipo);
        break;
      default:
        break;
    }

    /** Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,*/
    /** correspondiendo con los obtenidos desde el microservicio*/
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de baja este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { /** Si ya ha sido seleccionada una opción, debe de desactivar*/

      /**buscara por el tipo de Equipo y modificara el equipo*/
      switch (this.tipoEquipos) {
        case 'switch':
          this.switchdesactivar['idEstadoInventario'] = Number(motivo);
          this.switchdesactivar['activo'] = 0;
          this.switchsService.put(idEquipo, this.switchdesactivar).subscribe(result => {
            this.switchsService.postSocket({status: 'ok'}).subscribe(x => {});
            this.switchsService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'servidores':
          this.servidoresdesactivar['idEstadoInventario'] = Number(motivo);
          this.servidoresdesactivar['activo'] = 0;
          this.servidoresService.put(idEquipo, this.servidoresdesactivar).subscribe(result => {
            this.servidoresService.postSocket({status: 'ok'}).subscribe(x => {});
            this.servidoresService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'tarjeta de red':
          this.tarjetaReddesactivar['idEstadoInventario'] = Number(motivo);
          this.tarjetaReddesactivar['activo'] = 0;
          this.tarjetaRedService.put(idEquipo, this.tarjetaReddesactivar).subscribe(result => {
            this.tarjetaRedService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tarjetaRedService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'telefonos ip':
          this.telefonoIpdesactivar['idEstadoInventario'] = Number(motivo);
          this.telefonoIpdesactivar['activo'] = 0;
          this.telefonoIdService.put(idEquipo, this.telefonoIpdesactivar).subscribe(result => {
            this.telefonoIdService.postSocket({status: 'ok'}).subscribe(x => {});
            this.telefonoIdService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'rack':
          this.racksdesactivar['idEstadoInventario'] = Number(motivo);
          this.racksdesactivar['activo'] = 0;
          this.racksService.put(idEquipo, this.racksdesactivar).subscribe(result => {
            this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
            this.racksService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'patch panel':
          this.patchPaneldesactivar['idEstadoInventario'] = Number(motivo);
          this.patchPaneldesactivar['activo'] = 0;
          this.patchPanelService.put(idEquipo, this.patchPaneldesactivar).subscribe(result => {
            this.patchPanelService.postSocket({status: 'ok'}).subscribe(x => {});
            this.patchPanelService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'charolas rack':
          this.charolaRackdesactivar['idEstadoInventario'] = Number(motivo);
          this.charolaRackdesactivar['activo'] = 0;
          this.charolasRackService.put(idEquipo, this.charolaRackdesactivar).subscribe(result => {
            this.charolasRackService.postSocket({status: 'ok'}).subscribe(x => {});
            this.charolasRackService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de baja exitosamente!',
      }).then((resultado => {

        this.getEquipo();
      }));

    }
  }
  /** funcion que muestra los detalles del equipo*/
  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.inventarioRedes.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.inventarioRedes.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
