import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoCategoria, TipoCategoriaData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-categoria';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {TipoCategoriaCreateComponent} from '../modals/tipo-categoria-create/tipo-categoria-create.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'ngx-tipo-categoria',
  templateUrl: './tipo-categoria.component.html',
  styleUrls: ['./tipo-categoria.component.scss'],
})
export class TipoCategoriaComponent implements OnInit {
  cols: any[];
  tipoCategoria: TipoCategoria[];
  tipoCategorias: TipoCategoria;
  dataSource: any;

  constructor(private tipoCategoriaService: TipoCategoriaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              ) {
  }


  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getTipoCategoria(mensajeAct) {
    this.tipoCategoriaService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.tipoCategoria = data;
      this.dataSource = new MatTableDataSource(this.tipoCategoria); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

/** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Sub Ramo`);
  }

  crearTipoCategoria() {
    this.dialogService.open(TipoCategoriaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoCategoria(0));
  }

  updateTipoCategoria(idTipoCategoria) {
    this.dialogService.open(TipoCategoriaCreateComponent, {
      context: {
        idTipo: 2,
        idTipoCategoria: idTipoCategoria,
      },
    }).onClose.subscribe( x => this.getTipoCategoria(1));
  }

  getTipoCategorias(idTipoCategoria) {
    this.tipoCategoriaService.getTipoCategoriaById(idTipoCategoria).subscribe(data => {
      this.tipoCategorias = data;
    });
  }

  bajaTipoCategoria(idTipoCategoria) {
    this.getTipoCategorias(idTipoCategoria); swal({
      title: '¿Deseas dar de baja este Tipo Sub Ramo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoCategorias.activo = 0;
        this.tipoCategoriaService.put(idTipoCategoria, this.tipoCategorias).subscribe( result => {
        });
        swal('¡La Categoria se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoCategoria(0);
        }));
      }
    });
  }

  ngOnInit() {
    this.getTipoCategoria(0);
    this.cols = ['tipo', 'acciones'];
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
