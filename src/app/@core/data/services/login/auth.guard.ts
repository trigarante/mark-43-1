import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import * as moment from 'moment';
import {NbDateService, NbToastrService} from '@nebular/theme';
import {TokenStorageService} from './token-storage.service';



@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    name?: any;
    log: string;
    constructor(private auth: TokenStorageService,
                private myRoute: Router,
                protected dateService: NbDateService<Date>,
                ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const helper = new JwtHelperService();
      try {
      try {
        if (next.data.permiso === false) {
          this.myRoute.navigate(['modulos', this.log]);
          return false;
        }
        return true
      }catch (e) {
        this.log = '0';
        this.myRoute.navigate(['modulos', this.log]);
        return false;
      }
        const decodedToken = helper.decodeToken(window.localStorage.getItem('token'));
        const fechaToken = moment(decodedToken.exp * 1000).toString();
        const fechaHoy = this.dateService.today().toString();
        if (fechaToken < fechaHoy) {
          this.log = 'warn';
          this.myRoute.navigate(['login', this.log]);
          return false;
        }
        return true;
      }catch (e) {
        this.log = '0';
        this.myRoute.navigate(['login', this.log]);
        return false;
      }
    }
}
