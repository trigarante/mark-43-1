import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {NbDialogRef} from '@nebular/theme';
import {CandidatoData, Candidatos} from '../../../../@core/data/interfaces/rrhh/candidatos';
import {Router} from '@angular/router';
import {RadioButton} from 'primeng/primeng';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';

/** Componente del modal "asignar candidato". */
@Component({
  selector: 'ngx-asignar-candidato',
  templateUrl: './asignar-candidato.component.html',
  styleUrls: ['./asignar-candidato.component.scss'],
})
export class AsignarCandidatoComponent implements OnInit {

  /** Variable que almacena el idPrecandidato obtenido. */
  @Input() idPrecandidato: number;
  @Input() idEtapa: number;
  /** Variable que almacena la Fecha de ingreso*/
  @Input() fechaIngreso: string;
  /** Variable que almacena el idCandidato obtenido. */
  @Input() idCandidato: number;
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que se inicializa con los valores del Microservicio.*/
  candidatos: Candidatos;
  /** Variable que permite la inicialización del formulario para asignar el seguimiento a un precandidato. */
  candidatoForm: FormGroup;
  /** Expresión regular para números y punto. */
  numeroPunto: RegExp = /[0-9.]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Lista de elementos de psicometrico. */
  psicometricoSelect: SelectItem[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: Boolean;
  /** Bandera para habilitar o deshabilitar el campo de calificación de examen. */
  examenTecnico: boolean;
  /** Variable que hace referencia al radiobutton de SI. */
  @ViewChild('radioSi')
  radioSi: RadioButton;
  /** Variable que hace referencia al radiobutton de NO. */
  @ViewChild('radioNo')
  radioNo: RadioButton;
  /** Variable que hace referencia al input de calificación de examen. */
  @ViewChild('calificacionExamen')
  calificacionExamen: Input;
  //
  activo: boolean;
  activoNo: boolean;
  /**Variable que muestra nombre completo*/
  nombreCompleto: string;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  candidatoEliminado: boolean;
  precandidatoEliminado: boolean;
  /**
   * El método constructor invoca Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<AsignarCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {CandidatoData} candidatoService Servicio del componente candidato para poder invocar los métodos.
   * @param {Router} _router Inicializa variable para poder navegar entre componentes. */
  constructor(
    private fb: FormBuilder, protected ref: NbDialogRef<AsignarCandidatoComponent>,
    protected candidatoService: CandidatoData, protected precandidatoService: PrecandidatosData,
    private _router: Router) {
  }
  /** Función que inicializa los elementos de la lista "psicometricoSelect" con los valores "Aprobado" y "No aprobado". */
  calificacionPsicometrico() {
    this.psicometricoSelect = [];
    this.psicometricoSelect.push({'label': 'Aprobado', value: true});
    this.psicometricoSelect.push({'label': 'No aprobado', value: false});
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario y redirecciona a la página. */
  verificarFormulario() {
    if (this.radioSi.checked !== undefined || this.radioNo.checked !== undefined) {
      if (this.radioSi.checked) {
        this.examenTecnico = true;  // Campo de calificación visible
        if (this.candidatoForm.value.calificacionExamen === '' || this.candidatoForm.value.calificacionExamen === null ||
          !this.candidatoForm.valid) {
          return true;  // Botón deshabilitado
        } else {
          return false; // Botón habilitado
        }
      } else {
        if (this.calificacionExamen === undefined) {  // Reseteando campo de calificación de examen
          this.candidatoForm.controls['calificacionExamen'].setValue('');
        }
        this.examenTecnico = false; // Campo de calificación no visible
        this.candidatoForm.controls['calificacionExamen'].setValue(null);
        if (this.candidatoForm.valid) {
          return false; // Botón habilitado
        } else {
          return true;  // Botón deshabilitado
        }
      }
    } else {
      return true;  // Botón deshabilitado
    }
  }

  /** Función que permite la asignación de un candidato. */
  asignarCandidato() {
    this.inactivo = true;
    // console.log(this.candidatoForm.value);
    this.submitted = true;
    if (this.candidatoForm.invalid) {
      return;
    }
    this.idEtapa = 2;
    this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      if (data.idEtapa === 1 && data.idEstadoRH === 1 || data.idEtapa === 1 && data.idEstadoRH === 16) {
        this.candidatoService.post(this.idEtapa, this.candidatoForm.value).subscribe(
          (result) => {
          },
          error => { },
          () => {
            this.candidatoService.postSocket(this.candidatoForm.value).subscribe(d => {});
            this.precandidatoService.postSocket(this.candidatoForm.value).subscribe(d => {});
            this.ref.close();
            this.candidatoForm.reset();
            this._router.navigate(['/modulos/rrhh/candidatos']);
          },
        );
      } else {
        this.precandidatoEliminado = true;
      } });

    this.getCandidato(0);
  }

  getCandidato(mensajeAct) {
    this.candidatoService.get();
  }

  /** Función que obtiene la información del candidato dependiendo de su ID. */
  getCandidatoById(idCandidato) {
    this.candidatoService.getCandidatoById(idCandidato).subscribe(result => {
      this.candidatos = result;
      this.nombreCompleto = this.candidatos.nombre + ' ' + this.candidatos.apellidoPaterno + ' ' + this.candidatos.apellidoMaterno;
      if (this.candidatos.calificacionPsicometrico) {
        this.candidatoForm.controls['calificacionPsicometrico'].setValue(true);
      } else {
        this.candidatoForm.controls['calificacionPsicometrico'].setValue(false);
      }
      if (this.candidatos.calificacionExamen !== null && this.candidatos.calificacionExamen !== 0) {
        this.activo = true;
        this.candidatoForm.controls['calificacionExamen'].setValue(this.candidatos.calificacionExamen);
        this.examenTecnico = true;
      }else {
        this.activoNo = true;
      }
      this.candidatoForm.controls['comentarios'].setValue(this.candidatos.comentarios);
      this.fechaIngreso = this.candidatos.fechaCreacion;
    });
  }

  /** Función que actualiza la información de un candidato. */
  // updateCandidato() {
  //   alert('ACTUALIZADO el ID del candidato: ' + this.idCandidato + '\n\n Con el ID de precandidato: ' + this.idPrecandidato);
  // }

  /** Función que actualiza la información de un candidato. */
  updateCandidato() {
    this.inactivo = true;
    this.candidatoService.getCandidatoById(this.idCandidato).subscribe(data => {
      if (data.idEtapa === 2 && data.idEstadoRH === 1 || data.idEtapa === 2 && data.idEstadoRH === 16) {
        if (this.candidatoForm.controls['calificacionPsicometrico'].value) {
          this.candidatoForm.controls['calificacionPsicometrico'].setValue(1);
        }
        this.candidatoService.put(this.idCandidato, this.candidatoForm.value).subscribe(
          result => {
          },
          error => { },
          () => {
            this.candidatoService.postSocket(this.candidatoForm.value).subscribe(d => {});
            this.candidatoForm.reset();
            this.ref.close();
          },
        );
      }else {
        this.candidatoEliminado = true;
      }
    });
  }

  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    this.candidatoForm = this.fb.group({
      'calificacionPsicometrico': new FormControl('', Validators.required),
      'calificacionExamen': new FormControl('', Validators.compose([Validators.max(10)])),
      'comentarios': new FormControl('', Validators.required),
      'idPrecandidato': this.idPrecandidato,
      'fechaIngreso': this.fechaIngreso,
    });
    this.calificacionPsicometrico();
    if (this.idTipo === 2) {
      this.getCandidatoById(this.idCandidato);
    }
    this.examenTecnico = false;
  }
}
