import { Component} from '@angular/core';

@Component({
  selector: 'ngx-endosos-cancelaciones',
 template:`
    <router-outlet></router-outlet>
  `,
})
export class EndososCancelacionesComponent{
}
