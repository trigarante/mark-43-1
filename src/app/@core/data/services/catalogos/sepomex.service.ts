import {Injectable} from '@angular/core';
import {Sepomex, SepomexData} from '../../interfaces/catalogos/sepomex';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class SepomexService extends SepomexData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getColoniaByCp(cp): Observable<Sepomex[]> {
    return this.http.get<Sepomex[]>(this.baseURL + 'v1/sepomex/' + cp);
  }
}
