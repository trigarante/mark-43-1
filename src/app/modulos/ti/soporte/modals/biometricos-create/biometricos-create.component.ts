import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {
  Biometricos,
  BiometricosData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-biometricos-create',
  templateUrl: './biometricos-create.component.html',
  styleUrls: ['./biometricos-create.component.scss'],
})
export class BiometricosCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idBiometricos: number;
  biometricos: Biometricos;
  biometricosCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;


  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BiometricosCreateComponent>,
              private biometricosService: BiometricosData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private router: Router, private sedesService: TipoSedeData) { }
  /**selec que muestra las opciones de estado de inventario*/
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  /**selec que muestra las opciones de observaciones del inventario*/
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }
  /**selec que muestra las opciones de marcas*/
  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  /**Funcion que permite crear un nuevo biometrico*/
  guardarBiometricos() {
    if (this.biometricosCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.biometricosService.post(this.biometricosCreateForm.value).subscribe((result) => {
      this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
      this.biometricosCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'biometricos']);
    });
  }
  /**Funcion que permite modificar un biometricos*/
  updateBiometricos() {
    this.biometricosService.put(this.idBiometricos, this.biometricosCreateForm.value).subscribe(result => {
      this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
      this.biometricosCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'biometricos']);
    });
  }
  /**Funcion que permite obtener un biometrico de red por su id*/
  getBiometricosById() {
    this.biometricosService.getBiometricosById(this.idBiometricos).subscribe(data => {
      this.biometricos = data;
      this.biometricosCreateForm.controls['folio'].setValue(this.biometricos.folio);
      this.biometricosCreateForm.controls['idMarca'].setValue(this.biometricos.idMarca);
      this.biometricosCreateForm.controls['idEstadoInventario'].setValue(this.biometricos.idEstadoInventario);
      this.biometricosCreateForm.controls['idObservacionesInventario'].setValue(this.biometricos.idObservacionesInventario);
      this.biometricosCreateForm.controls['idEmpleado'].setValue(this.biometricos.idEmpleado);
      this.biometricosCreateForm.controls['idTipoSede'].setValue(this.biometricos.idTipoSede);
      this.biometricosCreateForm.controls['origenRecurso'].setValue(this.biometricos.origenRecurso);

    });
  }
  /**selec que muestra si el equipo es propio o rentado*/
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  /**selec que muestra de donde es el equipo*/
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getBiometricosById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.biometricosCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
