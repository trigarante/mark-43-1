import {Observable} from 'rxjs';
export interface Divisas {
  id: number;
  divisa: string;
  descripcion: string;
  activo: number;
}
export abstract class DivisasData {
abstract get(): Observable<Divisas[]>;

abstract post(divisa): Observable<Divisas>;
abstract postSocket(json): Observable<JSON>;

abstract getDivisaById(idDivisa): Observable<Divisas>;

abstract put(idDivisa, divisa): Observable<Divisas>;
}
