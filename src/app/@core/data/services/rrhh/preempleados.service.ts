import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Preempleados, PreempleadosData} from '../../interfaces/rrhh/preempleados';

@Injectable()
export class PreempleadosService extends PreempleadosData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Preempleados[]> {
    return this.http.get<Preempleados[]>(this.baseURL + 'v1/empleados/pre-empleado');
  }
  postSocket(json): Observable<JSON> {
    return  this.http.post<JSON>(this.socketURL + 'preempleados/savePreempleado', json);
  }

  getPreEmpleadoById(idPreEmpleado): Observable<Preempleados> {
    return this.http.get<Preempleados>(this.baseURL + 'v1/empleados/pre-empleado/' + idPreEmpleado);
  }
}
