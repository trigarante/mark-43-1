import { Injectable } from '@angular/core';
import {HistorialLlamadas, HistorialLlamadasData} from '../../interfaces/ventaNueva/historial-llamadas';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Global3} from '../global3';

@Injectable()

export class HistorialLlamadasService extends HistorialLlamadasData {

  private baseURL;

  constructor( private http: HttpClient) {
    super();
    this.baseURL = Global3.url;
  }

  get(idSolicitud): Observable<HistorialLlamadas[]> {
    return this.http.get<HistorialLlamadas[]>(this.baseURL + 'callback/solicitud/' + idSolicitud);
  }

  getById(idSolicitud): Observable<HistorialLlamadas> {
    return this.http.get<HistorialLlamadas>(this.baseURL + 'callback/get/');
  }

  post(solicitud): Observable<HistorialLlamadas> {
    return this.http.post<HistorialLlamadas>(this.baseURL + 'callback/post/', solicitud);
  }

  put(idSolicitud, solicitud): Observable<HistorialLlamadas> {
    return this.http.put<HistorialLlamadas>(this.baseURL + 'callback/put/' + idSolicitud, solicitud);
  }
}
