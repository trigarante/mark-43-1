import {Injectable} from '@angular/core';

const TOKEN_KEY = 'token';
const USERNAME_KEY = 'AuthUsername';
const AUTHORITIES_KEY = 'AuthAuthorities';

@Injectable({
    providedIn: 'root',
})

export class TokenStorageService {
    private roles: Array<string> = [];

    constructor() {
    }

    signOut() {
      window.localStorage.clear();
      window.sessionStorage.clear();
    }

    public saveToken(token: string) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }

    public saveTokenGoogle(token: string) {
      window.localStorage.removeItem(TOKEN_KEY);
      window.localStorage.setItem(TOKEN_KEY, token);
    }

    public saveCodeGoogle(code: string) {
      window.localStorage.removeItem('code');
      window.localStorage.setItem('code', code);
    }

    public getToken(): string {
        return sessionStorage.getItem(TOKEN_KEY);
    }

    isLoggednIn() {
        return this.getToken() !== null;
    }

    public saveUsername(username: string) {
        window.sessionStorage.removeItem(USERNAME_KEY);
        window.sessionStorage.setItem(USERNAME_KEY, username);
    }

    public getUsername(): string {
        return sessionStorage.getItem(USERNAME_KEY);
    }

    public saveCurrentUserPermissions(authorities: string) {
        window.localStorage.removeItem('User');
        window.localStorage.setItem('User', authorities);
    }

    public saveCurrentEmployee(idEmpleado: string, idUsuario: string) {
      window.sessionStorage.removeItem('Empleado');
      window.sessionStorage.removeItem('Usuario');
      window.sessionStorage.setItem('Empleado', idEmpleado);
      window.sessionStorage.setItem('Usuario', idUsuario);
    }

    public getAuthorities(): string[] {
        this.roles = [];

        if (sessionStorage.getItem(TOKEN_KEY)) {
            JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY)).forEach(authority => {
                this.roles.push(authority.auauthority);
            });
        }

        return this.roles;
    }
}
