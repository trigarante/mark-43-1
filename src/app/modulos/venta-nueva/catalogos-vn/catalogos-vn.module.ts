import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CatalogosVnComponent} from './catalogos-vn.component';
import {TipoContactoVnComponent} from './tipo-contacto-vn/tipo-contacto-vn.component';
import {CatalogosVnRoutingModule} from './catalogos-vn-routing.module';
import {TipoContactoCreateComponent} from './modals/tipo-contacto-create/tipo-contacto-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {KeyFilterModule} from 'primeng/primeng';
import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatIconModule, MatButtonModule, MatSlideToggleModule, MatSidenavModule, MatListModule,
} from '@angular/material';
import {NbDialogModule} from '@nebular/theme';
import { EstadoSolicitudComponent } from './estado-solicitud/estado-solicitud.component';
import { CreateEstadoSolicitudComponent } from './modals/create-estado-solicitud/create-estado-solicitud.component';
import { EstadoPolizaComponent } from './estado-poliza/estado-poliza.component';
import { FlujoPolizaComponent } from './flujo-poliza/flujo-poliza.component';
import { TipoPagoComponent } from './tipo-pago/tipo-pago.component';
import { TipoPolizaComponent } from './tipo-poliza/tipo-poliza.component';
import { TipoPolizaCreateComponent } from './modals/tipo-poliza-create/tipo-poliza-create.component';
import { TipoPagoCreateComponent } from './modals/tipo-pago-create/tipo-pago-create.component';
import { FlujoPolizaCreateComponent } from './modals/flujo-poliza-create/flujo-poliza-create.component';
import { EstadoPolizaCreateComponent } from './modals/estado-poliza-create/estado-poliza-create.component';
import { EstadoPagoComponent } from './estado-pago/estado-pago.component';
import { EstadoPagoCreateComponent } from './modals/estado-pago-create/estado-pago-create.component';


@NgModule({
  declarations: [
    CatalogosVnComponent,
    TipoContactoVnComponent,
    TipoContactoCreateComponent,
    EstadoSolicitudComponent,
    CreateEstadoSolicitudComponent,
    EstadoPolizaComponent,
    FlujoPolizaComponent,
    TipoPagoComponent,
    TipoPolizaComponent,
    TipoPolizaCreateComponent,
    TipoPagoCreateComponent,
    FlujoPolizaCreateComponent,
    EstadoPolizaCreateComponent,
    EstadoPagoComponent,
    EstadoPagoCreateComponent,
  ],
  imports: [
    CommonModule,
    CatalogosVnRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    KeyFilterModule,
    NbDialogModule.forChild(),
    MatSlideToggleModule,
    MatSidenavModule,
    FormsModule,
    MatListModule,
  ],
  entryComponents: [
    TipoContactoCreateComponent,
    TipoPagoCreateComponent,
    TipoPolizaCreateComponent,
    EstadoPolizaCreateComponent,
    FlujoPolizaCreateComponent,
    EstadoPagoCreateComponent,
  ],
})
export class CatalogosVnModule {
}
