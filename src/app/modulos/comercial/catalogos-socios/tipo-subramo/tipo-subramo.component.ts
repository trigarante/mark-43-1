import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {TipoSubramo, TipoSubramoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {TipoSubramoCreateComponent} from '../modals/tipo-subramo-create/tipo-subramo-create.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-tipo-subramo',
  templateUrl: './tipo-subramo.component.html',
  styleUrls: ['./tipo-subramo.component.scss'],
})
export class TipoSubramoComponent implements OnInit {
  cols: any[];
  tipoSubramo: TipoSubramo[];
  tipoSubramos: TipoSubramo;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private tipoSubramoService: TipoSubramoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
  ) {
  }


  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getTipoSubramo(mensajeAct) {
    this.tipoSubramoService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.tipoSubramo = data;
      this.dataSource = new MatTableDataSource(this.tipoSubramo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Sub Ramo`);
  }

  crearTipoSubramo() {
    this.dialogService.open(TipoSubramoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoSubramo(0));
  }

  updateTipoSubramo(idTipoSubramo) {
    this.dialogService.open(TipoSubramoCreateComponent, {
      context: {
        idTipo: 2,
        idTipoSubramo: idTipoSubramo,
      },
    }).onClose.subscribe( x => this.getTipoSubramo(1));
  }

  getTipoSubramos(idTipoSubramo) {
    this.tipoSubramoService.getTipoSubramoById(idTipoSubramo).subscribe(data => {
      this.tipoSubramos = data;
    });
  }

  bajaTipoSubramo(idTipoSubramo) {
    this.getTipoSubramos(idTipoSubramo); swal({
      title: '¿Deseas dar de baja este Tipo Sub Ramo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoSubramos.activo = 0;
        this.tipoSubramoService.put(idTipoSubramo, this.tipoSubramos).subscribe( result => {
        });
        swal('¡La Subramo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoSubramo(0);
        }));
      }
    });
  }

  ngOnInit() {
    this.getTipoSubramo(0);
    this.cols = ['tipo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoGiro.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoGiro.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
