import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment.prod';
import {PreguntasInterfaces} from '../../interfaces/rrhh/preguntas-interfaces';

@Injectable()
class PreguntasService {

  constructor(private httpClient: HttpClient) {

  }

  public getPreguntasByArea(idP: number) {
    return this.httpClient
      .get<PreguntasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/preguntas-by-area/' + idP);
  }

  public getPreguntasByAreabyFecha(idP: number, fi: string, ff: string) {
    return this.httpClient
      .get<PreguntasInterfaces[]>(
        environment.apiUrl + 'v1/cultura-bienestar/preguntas-by-fecha/' + idP + '/' + fi + ' 00:00:00/' + ff + ' 23:59:59');
  }

  public getPreguntasById(idPr: number) {
    return this.httpClient
      .get<PreguntasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/preguntas-by-id/' + idPr);
  }
}
