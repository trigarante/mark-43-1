import {Injectable} from '@angular/core';
import {CampanaCategoria, CampanaCategoriaData} from '../../interfaces/marketing/campana-categoria';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CampanaCategoriaService extends CampanaCategoriaData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<CampanaCategoria[]> {
    return this.http.get<CampanaCategoria[]>(this.baseURL + 'v1/campana-categorias');
  }

  post(campanaCategoria: CampanaCategoria): Observable<CampanaCategoria> {
    return this.http.post<CampanaCategoria>(this.baseURL + 'v1/campana-categorias', campanaCategoria);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'campaniacategoria/saveCampaniaCategoria', json);
  }

  getCampanasCategoriaById(idCampanaCategoria): Observable<CampanaCategoria> {
    return this.http.get<CampanaCategoria>(this.baseURL + 'v1/campana-categorias/' + idCampanaCategoria);
  }

  put(idCampanaCategoria, campanaCategoria: CampanaCategoria) {
    return this.http.put<CampanaCategoria>(this.baseURL + 'v1/campana-categorias/' + idCampanaCategoria + '/'
      + sessionStorage.getItem('Empleado') , campanaCategoria);
  }
}
