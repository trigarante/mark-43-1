import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {AnalisisCompetenciaData} from '../../../../@core/data/interfaces/comerciales/analisis-competencia';

@Component({
  selector: 'ngx-asignar-analisis-competencia',
  templateUrl: './asignar-analisis-competencia.component.html',
  styleUrls: ['./asignar-analisis-competencia.component.scss'],
})
export class AsignarAnalisisCompetenciaComponent implements OnInit {
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  @Input() idCompetencia: number;
  @Input() idAnalisisCompetencia: number;
  @Input() idTipo: number;
  asignarAnalisisForm: FormGroup;
  submitted: Boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected analisisCompetenciaService: AnalisisCompetenciaData) {
  }

  guardarAnalisisSeguimiento() {
    this.submitted = true;
    if (this.asignarAnalisisForm.invalid) {
      return;
    }
    let precios: any;
    precios = this.asignarAnalisisForm.controls['precios'].value;
    precios = precios.replace(/,/g, '');
    this.asignarAnalisisForm.controls['precios'].setValue(Number(precios));
    let participacion: any;
    participacion = this.asignarAnalisisForm.controls['participacion'].value;
    participacion = participacion.replace(/,/g, '');
    this.asignarAnalisisForm.controls['participacion'].setValue(Number(participacion));
    this.analisisCompetenciaService.post(this.asignarAnalisisForm.value).subscribe((result) => {
      this.analisisCompetenciaService.postSocket(this.asignarAnalisisForm.value).subscribe(x => {});
      this.asignarAnalisisForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/analisis-competencia']);    });
  }

  formatoNumerosConComas(objetivo, control) {
    /**La asignacion de comas llega hasta 100,000,000,000 para mayor cantidad, agregar maas validaciones o hacerlo dinamico*/
    let formatNumber: any = '';
    objetivo = objetivo.replace(/,/g, '');
    if (objetivo.length > 3 && objetivo.length < 7) {
      formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
      this.asignarAnalisisForm.controls[control].setValue(formatNumber);
    } else if (objetivo.length > 6 && objetivo.length < 10) {
      formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
      this.asignarAnalisisForm.controls[control].setValue(formatNumber);
    } else if (objetivo.length > 9 && objetivo.length < 13) {
      formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
        ',' + objetivo.slice(-3);
      this.asignarAnalisisForm.controls[control].setValue(formatNumber);
    }else {
      this.asignarAnalisisForm.controls[control].setValue(objetivo);
    }
  }
getAnalisisCVompetenciaById() {
  this.analisisCompetenciaService.getAnalisisCompetenciaById(this.idAnalisisCompetencia).subscribe(data => {
    this.formatoNumerosConComas(data.precios.toString(), 'precios');
    this.formatoNumerosConComas(data.participacion.toString(), 'participacion');
    this.asignarAnalisisForm.controls['idCompetencia'].setValue(data.idCompetencia);
  });
}
  updateAnalisisCompetencia() {
    let precios: any;
    precios = this.asignarAnalisisForm.controls['precios'].value;
    precios = precios.replace(/,/g, '');
    this.asignarAnalisisForm.controls['precios'].setValue(Number(precios));
    let participacion: any;
    participacion = this.asignarAnalisisForm.controls['participacion'].value;
    participacion = participacion.replace(/,/g, '');
    this.asignarAnalisisForm.controls['participacion'].setValue(Number(participacion));
  this.analisisCompetenciaService.put(this.idAnalisisCompetencia, this.asignarAnalisisForm.value).subscribe(data => {
    this.analisisCompetenciaService.postSocket(this.asignarAnalisisForm.value).subscribe(x => {});
    this.asignarAnalisisForm.reset();
    this.ref.close();
  });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getAnalisisCVompetenciaById();
    }
    this.asignarAnalisisForm = this.fb.group({
      'idCompetencia': this.idCompetencia,
      'precios': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'participacion': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'activo': 1,
    });
  }

}
