import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {EstadoEscolaridadService} from './estado-escolaridad.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('EstadoEscolaridad service', () => {
  let injector: TestBed;
  let service: EstadoEscolaridadService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 16,
    estado: 'TEST UNICO',
    activo: 0,
  },
    {
      id: 'estadoescolar',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EstadoEscolaridadService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EstadoEscolaridadService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/estado-escolar');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/estado-escolar', '/16');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/estado-escolar');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/estado-escolar', '/16/74');
      });
    });
  }
})
