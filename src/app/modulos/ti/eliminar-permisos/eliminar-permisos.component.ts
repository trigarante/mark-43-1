import { Component, OnInit, ViewChild } from '@angular/core';
import {GrupoUsuariosData, GrupoUsuarios} from '../../../@core/data/interfaces/ti/grupoUsuarios';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
} from '@angular/material';
import { map } from 'rxjs/operators';
import swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-eliminar-permisos',
  templateUrl: './eliminar-permisos.component.html',
  styleUrls: ['./eliminar-permisos.component.scss'],
})

export class EliminarPermisosComponent implements OnInit {
  cols: any[];
  dataSource: any;
  grupoUsuario: GrupoUsuarios[];
  grupUsuario: GrupoUsuarios;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  constructor(private grupoUsuariosService: GrupoUsuariosData, private router: Router) {}
  ngOnInit() {
    this.setGrupoUsuarios();
    this.cols = [
      'subarea',
      'puesto',
      'nombre',
      'descripcion',
      'modificar',
    ];
    this.getPermisos();
    this.estaActivo();
    this.connect();
  }
  setGrupoUsuarios() {
    this.grupoUsuariosService
      .get()
      .pipe(
        map(result => {
          return result.filter(data => data.activo === 0);
        }),
      )
      .subscribe(data => {
        this.grupoUsuario = data;
        this.dataSource = new MatTableDataSource(this.grupoUsuario);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  } // setGrupoUsuarios
  activaGrupoUsuario(idGrupoUsuario) {
    this.grupoUsuariosService.getByID(idGrupoUsuario).subscribe(data => {
      this.grupUsuario = data;
      swal
        .fire({
          title: 'Deseas activar este GrupoUsuario ',
          type: 'warning',
          showCancelButton: true,
          showConfirmButton: true,
        })
        .then(e => {
          if (e.value) {
            this.grupUsuario['activo'] = 1;
            this.grupoUsuariosService
              .put(idGrupoUsuario, this.grupUsuario)
              .subscribe(res => {  });
            swal
              .fire({
                title: '¡El GrupoUsuario se ha dado de alta exitosamente!',
                type: 'success',
              }).then(res => this.setGrupoUsuarios() );
          } else {  }
        });
    });
  } // activaGrupoUsuario

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  connect() {
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'reactivarpermisos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelReactivarPermisos', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.setGrupoUsuarios();
        }, 500);
      });
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.reactivarPermisos.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.reactivarPermisos.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
