import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Tablet Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 10,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'nuevo',
    estadoInventario: 'A ESTADO',
    idEstadoInventario: 12,
    idObservacionesInventario: 1,
    numFolio: '0001TT',
    idProcesador: 1,
    capacidad: '500',
    ram: '4',
    idMarca: 1,
    telefono: 0,
    fechaRecepcion: '2019-09-24T16:23:23.000+0000',
    fechaSalida: null,
    comentarios: 'EXCELENTE',
    activo: 1,
    usuario: null,
    nombreProcesador: 'NVIDIA',
    nombreMarca: 'ALLENWARE',
    idSede: 1,
    origenRecurso: 'propio',
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'tablet',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tablet');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tablet', '/10');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tablet');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tablet', '/10/74');
      });
    });
  }
});
