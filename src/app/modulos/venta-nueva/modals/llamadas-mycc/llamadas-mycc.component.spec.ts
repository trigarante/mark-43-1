import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamadasMyccComponent } from './llamadas-mycc.component';

describe('LlamadasMyccComponent', () => {
  let component: LlamadasMyccComponent;
  let fixture: ComponentFixture<LlamadasMyccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlamadasMyccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlamadasMyccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
