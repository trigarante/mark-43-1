import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../@core/data/services/login/token-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../@core/data/interfaces/ti/user';
import {UserService} from '../@core/data/users.service';

@Component({
  selector: 'ngx-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  name?: any;
  code?: any;
  constructor(private tokenStorage: TokenStorageService, private route: ActivatedRoute, private myRoute: Router,
              private userService: UserService) { }

  ngOnInit() {
    let idEmpleado: string;
    let idUsuario: string;
    let permisos: string;

    this.route.snapshot.queryParamMap.get('token');
    this.route.queryParamMap.subscribe(queryParams => {
      this.name = queryParams.get('token');
      this.code = queryParams.get('code');
      this.userService.getCurrentUser(this.name).subscribe(
         (users: User) => {
          if ( users.idEmpleado === null) {
            this.myRoute.navigate(['login', 'error']);
          } else {
            idEmpleado = users.idEmpleado;
          }
          idUsuario = users.id.toString();
          permisos = users.permisos;
        }, () => {
          this.myRoute.navigate(['login', 'error']);
        }, () => {
          this.tokenStorage.saveCurrentUserPermissions(permisos);
          this.tokenStorage.saveCurrentEmployee(idEmpleado, idUsuario);
          this.tokenStorage.saveTokenGoogle(this.name);
          this.tokenStorage.saveCodeGoogle(this.code);
          this.myRoute.navigate(['modulos']);
        });
    });
}
}
