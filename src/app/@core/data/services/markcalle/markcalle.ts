import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Empleados, EmpleadosData} from '../../interfaces/rrhh/empleados';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root',
})
export class MarkcalleService  {
  private baseURL;

  constructor(private http: HttpClient) {

    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'empleados');
  }

//   saveEmpleado(empleados: Empleados, idPrecandidato, idEtapa): Observable<Empleados> {
//     return this.http.post<Empleados>(this.baseURL + 'empleados/' + idPrecandidato + '/' + idEtapa, empleados);
//   }

//   saveFoto(foto, id) {
//     return this.http.post(this.baseURL + 'empleados/guardar-foto/' + id, foto);
//   }

//   getEmpleadoById(idEmpleados): Observable<Empleados> {
//     return this.http.get<Empleados>(this.baseURL + 'empleados/' + idEmpleados);
//   }

//   put(id, empleado): Observable<Empleados> {
//     return this.http.put<Empleados>(this.baseURL + 'empleados/' + id, empleado);
//   }

//   getBySubarea(idSubarea): Observable<Empleados[]> {
//     return this.http.get<Empleados[]>(this.baseURL + 'empleados/get-by-id-subarea/' + idSubarea);
//   }

//   bajaEmpleado(idPrecandidato, datosBaja): Observable<Empleados> {
//     return this.http.put<Empleados>(this.baseURL + 'empleados/baja-empleado/' + idPrecandidato, datosBaja);
//   }

//   altaImss(idEmpleado, empleado: Empleados): Observable<Empleados> {
//     return this.http.put<Empleados>(this.baseURL + 'empleados/alta-imss/' + idEmpleado, empleado);
//   }
//   getEmpleadobySubareabyidUsuario(idSubarea, idUsuario): Observable<Empleados> {
//     return this.http.get<Empleados>(this.baseURL + 'empleados/idSubarea/' + idSubarea + '/idUsuario/' + idUsuario);
//   }
}





