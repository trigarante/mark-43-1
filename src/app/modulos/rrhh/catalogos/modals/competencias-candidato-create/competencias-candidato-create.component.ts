import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CompetenciaCandidatoData, CompetenciasCandidato} from '../../../../../@core/data/interfaces/catalogos/competencias-candidato';
import {NbDialogRef} from '@nebular/theme';
import {CompetenciasCandidatoComponent} from '../../competencias-candidato/competencias-candidato.component';
import {Router} from '@angular/router';
/** Componente del modal del catálogo para "crear competencias de un candidato". */
@Component({
  selector: 'ngx-competencias-candidato-create',
  templateUrl: './competencias-candidato-create.component.html',
  styleUrls: ['./competencias-candidato-create.component.scss'],
})
export class CompetenciasCandidatoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idCompetenciasCandidato obtenido. */
  @Input() idCompetenciasCandidato: number;
  /** Arreglo para inicializar las cabeceras de la tabla. */
  cols: any[];
  /** Variable que permite la inicialización del formulario para realizar la adición de una competencia de un candidato. */
  competenciaCandidatoForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enivado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  competenciaCandidato: CompetenciasCandidato;
  competenciacandidatoEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {CompetenciaCandidatoData} competenciaCandidatoService Servicio del componente competencias-candidato
   * para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected competenciaCandidatoService: CompetenciaCandidatoData,
              protected ref: NbDialogRef<CompetenciasCandidatoComponent>, private router: Router) {
  }
  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  gurdarCrearCompetenciaCandidato() {
    if (this.competenciaCandidatoForm.invalid) {
      return;
    }
    this.submitted = true;
    this.competenciaCandidatoService.post(this.competenciaCandidatoForm.value).subscribe((result) => {
      this.competenciaCandidatoService.postSocket(this.competenciaCandidatoForm.value).subscribe(x => {});
      this.competenciaCandidatoForm.reset();
      this.router.navigate( ['/modulos/rrhh/catalogos/competenciasCandidato']);
      this.competenciaCandidatoForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que obtiene la información de la competencia del candidato dependiendo de su ID. */
  getCompetenciaCandidatoById() {
    this.competenciaCandidatoService.getCompetenciaCandidatoById(this.idCompetenciasCandidato).subscribe( data => {
      this.competenciaCandidato = data;
      this.competenciaCandidatoForm.controls['descripcion'].setValue(this.competenciaCandidato.descripcion);
      this.competenciaCandidatoForm.controls['activo'].setValue(this.competenciaCandidato.activo);
    });
  }

  /** Función que da de baja una competencia de un candidato, cambiando el atributo activo a 0. */
  /*bajaCompetencia() {
    this.competenciaCandidatoForm.controls['activo'].setValue(0);
    this.competenciaCandidatoService.put(this.idCompetenciasCandidato, this.competenciaCandidatoForm.value).subscribe( result => {
      this.competenciaCandidatoForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Funciona que actualiza las competencias del candidato. */
  updateCompetenciaCandidato() {
    this.competenciaCandidatoService.getCompetenciaCandidatoById(this.idCompetenciasCandidato).subscribe( data => {
      if(data.activo === 1){
        this.competenciaCandidatoService.put(this.idCompetenciasCandidato, this.competenciaCandidatoForm.value).subscribe( result => {
          this.competenciaCandidatoService.postSocket(this.competenciaCandidatoForm.value).subscribe(x => {});
          this.competenciaCandidatoForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else {
        this.competenciacandidatoEliminado = true;
      }
    });

  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCompetenciaCandidatoById();
    } else if (this.idTipo === 3) {
      this.getCompetenciaCandidatoById();
    }
    this.competenciaCandidatoForm = this.fb.group( {
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
