import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {MedioTransporteComponent} from '../../medio-transporte/medio-transporte.component';
import {MedioTransporte, MedioTransporteData} from '../../../../../@core/data/interfaces/catalogos/medio-transporte';
import {Router} from '@angular/router';
/** Componente del modal del catálogo para "crear medio de transporte". */
@Component({
  selector: 'ngx-medio-transporte-create',
  templateUrl: './medio-transporte-create.component.html',
  styleUrls: ['./medio-transporte-create.component.scss'],
})
export class MedioTransporteCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idMedioTransporte obtenido. */
  @Input() idMedioTransporte: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un medio de transporte. */
  medioTransporteForm: FormGroup;
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z ]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  medioTransporte: MedioTransporte;
  mediotransporteElimindo: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {MedioTransporteData} medioTransporteService Servicio del componente medio-transporte para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<MedioTransporteComponent>,
              private medioTransporteService: MedioTransporteData, private router: Router) { }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarMedioTransporte() {
    if (this.medioTransporteForm.invalid) {
      return;
    }
    this.submitted = true;
    this.medioTransporteService.post(this.medioTransporteForm.value).subscribe((result) => {
      this.medioTransporteService.postSocket(this.medioTransporteForm.value).subscribe(x => {});
      this.medioTransporteForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/medioTransporte']);
      this.medioTransporteForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Función que actualiza la información de un medio de transporte. */
  updateMedioTransporte() {
    this.medioTransporteService.getMedioTransporteById(this.idMedioTransporte).subscribe(data => {
      if(data.activo === 1){
        this.medioTransporteService.put(this.idMedioTransporte, this.medioTransporteForm.value).subscribe(result => {
          this.medioTransporteService.postSocket(this.medioTransporteForm.value).subscribe(x => {});
          this.medioTransporteForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.mediotransporteElimindo = true;
      }
    });

  }

  /** Función que da de baja un medio de transporte, cambiando el atributo activo a 0. */
  /*bajaMedioTransporte() {
    this.medioTransporteForm.controls['activo'].setValue(0);
    this.medioTransporteService.put(this.idMedioTransporte, this.medioTransporteForm.value).subscribe( result => {
      this.medioTransporteForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que obtiene toda la información del medio de transporte según su ID. */
  getMedioTRansporteById() {
    this.medioTransporteService.getMedioTransporteById(this.idMedioTransporte).subscribe(data => {
      this.medioTransporte = data;
      this.medioTransporteForm.controls['medio'].setValue(this.medioTransporte.medio);
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getMedioTRansporteById();
    }
    this.medioTransporteForm = this.fb.group({
      'medio': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }
}
