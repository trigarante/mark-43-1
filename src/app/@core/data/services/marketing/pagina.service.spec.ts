import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';
import {PaginaService} from './pagina.service';

fdescribe('Pagina service', () => {
  let injector: TestBed;
  let service: PaginaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
  },
    {
      id: 'pagina',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [PaginaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(PaginaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/pagina');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/pagina', '/1');
      }); /*En paginas no hay post
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/pagina');
      });*/
      /*
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/pagina', '/1/1564159920811/1564159920811/74');
      });*/
    });
  }
})
