import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {SociosService} from '../../../../../@core/data/services/comerciales/socios.service';
import {SociosBaja, SociosBajaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/socios-baja';
import {SociosBajaService} from '../../../../../@core/data/services/comerciales/catalogo/socios-baja.service';


@Component({
  selector: 'ngx-tipo-bajasocios-create',
  templateUrl: './tipo-bajaSocios-create.html',
  styleUrls: ['./tipo-bajaSocios-create.scss'],
})
export class TipoBajaSociosCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idBajaSocios: number;
  bajaSociosCreateForm: FormGroup;
  submitted: Boolean;
  bajaSocios: SociosBaja;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private BajaSociosService: SociosBajaData) {
  }

  guardarBajaSocios() {
    this.submitted = true;
    if (this.bajaSociosCreateForm.invalid) {
      return;
    }
    this.BajaSociosService.post(this.bajaSociosCreateForm.value).subscribe((result) => {
      this.BajaSociosService.postSocket({status: 'ok'}).subscribe(X => {});
      console.log(result);
      this.bajaSociosCreateForm.reset();
      this.ref.close();
    });
  }

  getBajaSociosById() {
    this.BajaSociosService.getSocioBajaById(this.idBajaSocios).subscribe(data => {
      this.bajaSocios = data;
      this.bajaSociosCreateForm.controls['estado'].setValue(this.bajaSocios.estado);
    });
  }


  updateBajaSocios() {
    this.BajaSociosService.put(this.idBajaSocios, this.bajaSociosCreateForm.value).subscribe(result => {
      this.BajaSociosService.postSocket({status: 'ok'}).subscribe(X => {});
      this.bajaSociosCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getBajaSociosById();
    }

    this.bajaSociosCreateForm = this.fb.group({
      'estado': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
      'idEstado': 1,
    });
  }

}
