import {Component} from '@angular/core';

@Component({
  selector: 'ngx-venta-nueva',
  template:  `
    <router-outlet></router-outlet>
  `,
})
export class VentaNuevaComponent {

}
