import {Observable} from 'rxjs';

export interface Solicitudes {
  id: number;
  idCotizacionAli: number;
  cotizo: string;
  idEmpleado: number;
  idEstadoSolicitud: number;
  estado: string;
  fechaSolicitud: string;
  comentarios: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
}

export abstract class SolicitudesVNData {

  abstract get(): Observable<Solicitudes[]>;

  abstract post(solicitud): Observable<Solicitudes>;
  abstract postSocket(json): Observable<JSON>;

  abstract put(idSolicitud, solicitud): Observable<Solicitudes>;

  abstract getSolicitudesById(idSolicitud): Observable<Solicitudes>;

  abstract entryIdCliente(value);

  abstract returnIdCliente(): Observable<number>;

  abstract entryIdRegistroPoliza(value);

  abstract returnIdRegistroPoliza(): Observable<number>;

  abstract entryURLType(value);

  abstract returnentryURLType(): Observable<number>;

}
