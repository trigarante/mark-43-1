import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {Router} from '@angular/router';
import swal from 'sweetalert';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import {CandidatoData} from "../../../../@core/data/interfaces/rrhh/candidatos";
import {CapacitacionData} from "../../../../@core/data/interfaces/rrhh/capacitacion";
import {SeguimientoData} from "../../../../@core/data/interfaces/rrhh/seguimiento";
@Component({
  selector: 'ngx-reactivar-empleado',
  templateUrl: './reactivar-empleado.component.html',
  styleUrls: ['./reactivar-empleado.component.scss'],
})
export class ReactivarEmpleadoComponent implements OnInit {
/**Variable que recibe el idEmpleado de empleado-baja*/
  @Input () idEmpleado: number;
  /**Variable que recibe el idPrecandidato de empleado-baja**/
  @Input () idPrecandidato: number;
  /**Variable para establecer el tipo de actualizacion*/
  idTipo: number;
  activaEmpleadoForm: FormGroup;
  fechaIngreso: any;
  precandidatoReactivado: boolean;
  /**Variable para desctivar boton*/
  inactivo: boolean;
  // Web Sockets
  // private stompClientTeorico = null;
  // private stompClientRolePlay = null;
  // private stompClientAsignaciones = null;
  // private stompClientEmpleado = null;
  private stompClientEmpleadoBaja = null;
  // private stompClientBajasRH = null;
  constructor(
     protected ref: NbDialogRef<ReactivarEmpleadoComponent>,
     private fb: FormBuilder,
     protected empleadoService: EmpleadosData,
     private router: Router,
     private precandidatosService: PrecandidatosData,
     protected candidatoService: CandidatoData,
     protected capacitacionService: CapacitacionData,
     protected seguimientoService: SeguimientoData,
  ) { }
  // Web socket connection
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  activarEmpleado() {
        this.inactivo = true;
        this.fechaIngreso = this.activaEmpleadoForm.controls['fechaBaja'].value;
        this.fechaIngreso = Date.parse(this.fechaIngreso);
        this.empleadoService.reactivarEmpleado(this.idEmpleado, this.idPrecandidato, this.fechaIngreso).subscribe(() => {
          const object = {
            'idEmpleado': this.idEmpleado,
            'idPrecandidato': this.idPrecandidato,
            'fechaIngreso': this.fechaIngreso,
          };
          // this.stompClientEmpleado.send('/tasks/saveEmpleado', {}, JSON.stringify(object));
          this.empleadoService.postSocket(object).subscribe(x => {});
          this.empleadoService.postSocketBaja(object).subscribe(x => {});
          // this.stompClientEmpleadoBaja.send('/tasks/saveEmpleadoBaja', {}, JSON.stringify(object));
          swal('¡Se ha activado exitosamente!', {
            icon: 'success',
          }).then((resultado => {
            this.activaEmpleadoForm.reset();
            this.ref.close();
          }));
        });
  }

  activarPrecandidato() {
    this.inactivo = true;
        this.fechaIngreso = this.activaEmpleadoForm.controls['fechaBaja'].value;
        this.fechaIngreso = Date.parse(this.fechaIngreso);
        this.precandidatosService.reactivarPrecandidato(this.idPrecandidato, this.fechaIngreso).subscribe(
          () => {},
          error => { },
          () => {
            const object = {
              'idPrecandidato': this.idPrecandidato,
              'fechaIngreso': this.fechaIngreso,
            };
            // this.stompClient.send('/tasks/saveCandidato', {}, JSON.stringify(object));
            // this.stompClientTeorico.send('/tasks/saveCapacitacion', {}, JSON.stringify(object));
            // this.stompClientRolePlay.send('/tasks/saveRollplay', {}, JSON.stringify(object));
            // this.stompClientAsignaciones.send('/tasks/saveAsignaciones', {}, JSON.stringify(object));
            // this.stompClientBajasRH.send('/tasks/saveBajasRH', {}, JSON.stringify(object));
            // this.stompClientPrecandidatos.send('/tasks/savePrecandidato/1', {}, JSON.stringify(object));
            this.precandidatosService.postSocket(object).subscribe(d => {});
            this.candidatoService.postSocket(object).subscribe(d => {});
            this.capacitacionService.postSocket(object).subscribe(d => {});
            this.seguimientoService.postSocketRolplay(object).subscribe(x => {});
            this.seguimientoService.postSocketAignaciones(object).subscribe(x => {});
            this.precandidatosService.postSocketBajaRH(object).subscribe(x => {});
            swal('¡Se ha activado exitosamente!', {
              icon: 'success',
            }).then((resultado => {
              this.ref.close();
            }));
          },
        );
  }

  verificarFormulario() {
    if (this.activaEmpleadoForm.valid && this.activaEmpleadoForm.dirty && this.activaEmpleadoForm !== null) {
      return false;
    }else {
      return true;
    }
}

  ngOnInit() {
    this.inactivo = false;
    this.activaEmpleadoForm = this.fb.group({
      'fechaBaja': new FormControl('', Validators.compose( [Validators.required])),
    });
    this.verificarFormulario();
  }

}
