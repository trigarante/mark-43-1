import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {FuenteCamaras, FuenteCamarasData} from '../../../../interfaces/ti/soporte/inventario-soporte/fuenta-camaras';

@Injectable()
export class FuentaCamarasService extends FuenteCamarasData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<FuenteCamaras[]> {
    return this.http.get<FuenteCamaras[]>(this.baseURL + 'v1/corrienteCamara');
  }

  post(fuenteCamaras: FuenteCamaras): Observable<FuenteCamaras> {
    return this.http.post<FuenteCamaras>(this.baseURL + 'v1/corrienteCamara', fuenteCamaras);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'corrientecamara/saveCorrienteCamara', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajacorrientecamara/saveBajaCorrienteCamara', json);
  }

  getFuenteCamaraById(idFuenteCamara): Observable<FuenteCamaras> {
    return this.http.get<FuenteCamaras>(this.baseURL + 'v1/corrienteCamara/' + idFuenteCamara);
  }

  put(idFuenteCamara, fuenteCamaras: FuenteCamaras): Observable<FuenteCamaras> {
    return this.http.put<FuenteCamaras>(this.baseURL + 'v1/corrienteCamara/' + idFuenteCamara + '/'
      + sessionStorage.getItem('Empleado') , fuenteCamaras);
  }
}
