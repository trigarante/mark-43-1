import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {SubRamoComponent} from '../../sub-ramo/sub-ramo.component';
import {AsignarSubramoComponent} from '../asignar-subramo/asignar-subramo.component';
import {SubRamo, SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';
import swal from 'sweetalert';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-datos-subramo',
  templateUrl: './datos-subramo.component.html',
  styleUrls: ['./datos-subramo.component.scss'],
})
export class DatosSubramoComponent implements OnInit {
  @Input() idSubramo: number;
  @Input() idRamo: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  tipoSubramo: any;
  tipoGiro: any;
  categoria: SubRamo;
  dataSource: any;
  // socket
  disabled = true;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;

  constructor(private subramoService: SubramoData, protected ref: NbDialogRef<SubRamoComponent>,
              private dialogService: NbDialogService, private route: Router) { }

  getcategoria() {
    this.subramoService.getSubramoById(this.idSubramo).subscribe(
      result => {
        this.alias = result.alias;
        this.descripcion = result.descripcion;
        this.estado = result.estado;
        this.nombreComercial = result.nombreComercial;
        this.prioridades = result.prioridades;
        this.tipoSubramo = result.tipoSubRamo;
        this.tipoGiro = result.tipoRamo;
      },
    );
  }
  updateSubRamo() {
    this.dialogService.open(AsignarSubramoComponent, {
      context: {
        idTipo: 2,
        idSubramo: this.idSubramo,
        idRamo: this.idRamo,
      },
    });
  }

  bajaSubramo() {
    this.subramoService.getSubramoById(this.idSubramo).subscribe(data => {
      this.categoria = data; });
    swal({
      title: '¿Deseas dar de baja este Sub Ramo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.categoria.activo = 0;
        let categoriaForm: any;
        categoriaForm = {
          'activo': this.categoria.activo,
          'descripcion': this.categoria.descripcion,
          // 'id': this.categoria.id,
          'idRamo': this.categoria.idRamo,
          'idTipoSubRamo': this.categoria.idTipoSubRamo,
          'prioridad': this.categoria.prioridad,
        };
        this.subramoService.put(this.idSubramo, categoriaForm).subscribe(result => {
          this.subramoService.postSocket({subramo: categoriaForm}).subscribe(x => {});
        });
        swal('¡El Sub Ramo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          /**window.location.reload();*/
          this.dismiss();
        }));
        this.route.navigate(['modulos/comercial/sub-ramo']);
      }
    });
  }


  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  ngOnInit() {
    this.getcategoria();
    this.getPermisos();
  }

  dismiss() {
    this.ref.close();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verSubRamo.escritura;
  }


}
