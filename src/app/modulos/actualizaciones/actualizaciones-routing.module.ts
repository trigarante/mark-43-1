import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ActualizacionestiComponent} from './actualizacionesti/actualizacionesti.component';

const routes: Routes = [
  {
    path: 'actualizaciones',
    component: ActualizacionestiComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ActualizacionesRoutingModule {
}
