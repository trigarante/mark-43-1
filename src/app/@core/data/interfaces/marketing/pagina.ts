import {Observable} from 'rxjs';

export interface Pagina {
  id?: number;
  idCampana: number;
  idTipoPagina: number;
  url: string;
  descripcion: string;
  status: number;
  fechaRegistro: string;
  fechaActualizacion: string;
  configuracion: string;
}

export abstract class PaginasData {
  abstract get(): Observable<Pagina[]>;

  abstract post(pagina): Observable<Pagina>;
  abstract postSocket(json): Observable<JSON>;
  abstract postSocketAseguradoras(json): Observable<JSON>;
  abstract postSocketDatosNodo(json): Observable<JSON>;

  abstract getPaginaById(idPagina): Observable<Pagina>;

  abstract put(idPagina, fechaRegistro, fechaActualizacion, pagina);
}
