import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {CompetenciasComponent} from '../../competencias/competencias.component';
import {Competencia, CompetenciaData} from '../../../../../@core/data/interfaces/catalogos/competencia';
import {Router} from '@angular/router';
/** Componente del modal del catálogo para "crear competencias". */
@Component({
  selector: 'ngx-competencias-create',
  templateUrl: './competencias-create.component.html',
  styleUrls: ['./competencias-create.component.scss'],
})
export class CompetenciasCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idCompetencias obtenido. */
  @Input() idCompetencias: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una competencia. */
  competenciaForm: FormGroup;
  /** Bandera  para verificar que el formulario ha sido enivado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  competencia: Competencia;
  competenciaEliminado: boolean;

  numeros: RegExp = /[0-9]+/;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {CompetenciaData} competenciaService Servicio del componente competencia para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<CompetenciasComponent>, private competenciaService: CompetenciaData,
              private router: Router) {
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarCompetencia() {
    if ( this.competenciaForm.invalid) {
      return;
    }
    this.submitted = true;
    this.competenciaService.post(this.competenciaForm.value).subscribe( (result) => {
      this.competenciaService.postSocket(this.competenciaForm.value).subscribe(x => {});
      this.competenciaForm.reset();
      this.router.navigate( ['/modulos/rrhh/catalogos/competencias']);
      this.competenciaForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Función que actualiza una competencia. */
  updateCompetencias() {
    this.competenciaService.getCompetenciasById(this.idCompetencias).subscribe( data => {
      if(data.activo === 1) {
        this.competenciaService.put(this.idCompetencias, this.competenciaForm.value).subscribe( result => {
          this.competenciaService.postSocket(this.competenciaForm.value).subscribe(x => {});
          this.competenciaForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else {
        this.competenciaEliminado = true;
      }
    });

  }

  /** Función que obtiene los datos de la competencia dependiendo de su ID. */
  getCompetenciaById() {
    this.competenciaService.getCompetenciasById(this.idCompetencias).subscribe( data => {
      this.competencia = data ;
      this.competenciaForm.controls['escala'].setValue(this.competencia.escala);
      this.competenciaForm.controls['estado'].setValue(this.competencia.estado);
      this.competenciaForm.controls['descripcion'].setValue(this.competencia.descripcion);
      this.competenciaForm.controls['activo'].setValue(this.competencia.activo);

    });
  }

  /** Función que da de baja una competencia, cambiando el atributo activo a 0. */
  /*bajaCompetencia() {
    this.competenciaForm.controls['activo'].setValue(0);
    this.competenciaService.put(this.idCompetencias, this.competenciaForm.value).subscribe( result => {
      this.competenciaForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCompetenciaById();
    }else if (this.idTipo === 3) {
      this.getCompetenciaById();
    }
    this.competenciaForm = this.fb.group({
      'escala': new FormControl('', Validators.compose([Validators.required, Validators.max(10)])),
      'estado': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
