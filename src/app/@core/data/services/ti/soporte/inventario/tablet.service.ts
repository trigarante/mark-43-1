import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Tablet, TabletData} from '../../../../interfaces/ti/soporte/inventario/tablet';

@Injectable()
export class TabletService extends TabletData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Tablet[]> {
    return this.http.get<Tablet[]>(this.baseURL + 'v1/tablet');
  }
  post(tablet: Tablet): Observable<Tablet> {
    return this.http.post<Tablet>(this.baseURL + 'v1/tablet', tablet);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'tablet/saveTablet', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajatablet/saveBajaTablet', json);
  }
  getTabletById(idTablet): Observable<Tablet> {
    return this.http.get<Tablet>(this.baseURL + 'v1/tablet/' + idTablet);
  }
  put(idTablet, tablet: Tablet): Observable<Tablet> {
    return this.http.put<Tablet>(this.baseURL + 'v1/tablet/' + idTablet + '/'
      + sessionStorage.getItem('Empleado'), tablet);
  }
}
