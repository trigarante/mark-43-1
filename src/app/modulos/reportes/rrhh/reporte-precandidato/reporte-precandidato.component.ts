import {Component, ViewEncapsulation, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from 'd3-format';
import * as d3Inter from 'd3-interpolate';
import 'd3-transition';
import {PrecandidatosService} from '../../../../@core/data/services/rrhh/precandidatos.service';
import {Precandidatos} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import * as moment from 'moment';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'ngx-app-reporte-precandidato',
  encapsulation: ViewEncapsulation.None,    // Necesaria para que se vea chidito. :v
  templateUrl: './reporte-precandidato.component.html',
  styleUrls: ['./reporte-precandidato.component.scss'],
  providers: [PrecandidatosService],
})

export class ReportePrecandidatoComponent implements OnInit {
  title = 'Prueba';
  precandidatos = [];
  flares: any;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;
  private maxRadius: number;
  x;
  y;
  partition;
  formatNumber;
  root;
  private arc: any;
  private color: any;
  private svg: any;
  datas: any;
  cols: any[];
  etapas: any[];
  dates: any;
  label = 'TOTAL DE PRE-CANDIDATOS:';
  rangeDates: Date[];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(private precandidatosService: PrecandidatosService) {
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    // this.radius = Math.min(this.width, this.height) / 2;
    this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
    this.cols = ['nombre', 'telefonoMovil', 'nombreEtapa'];
    this.etapas = ['Precandidato', 'Candidato', 'Empleado', 'Capacitación', 'Seguimiento 1', 'Seguimiento 2'];
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
  }

  ngOnInit() {
    this.initSvg();
    this.getPrecandidatos(null);
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPrecandidatos(rangeData: Date[]) {
    this.precandidatosService.get()
      .subscribe(data => {
        if (rangeData == null) {
          this.precandidatos = data;
          this.dates = this.precandidatos.length;
          this.drawPie(this.precandidatos);
        } else {
          this.precandidatos = [];

          console.log(' FORMATO rangeData: ' + moment(rangeData[1]));
          console.log('Fecha de servicio: ' + moment(data[0].fechaCreacion).valueOf());

          for (let i = 0; i < data.length; i++) {
            if (moment(data[i].fechaCreacion).valueOf() >= moment(rangeData[0]).valueOf()
              && moment(data[i].fechaCreacion).valueOf() <= moment(rangeData[1]).valueOf()) {
              this.precandidatos.push(data[i]);
            }
          }

          this.removeSvg();
          this.drawPie(this.precandidatos);
        }
      });
  }

  private initSvg() {

    const colors = ['#004da4', '#ff1456', '#006ebb'];
    this.formatNumber = d3Format.format(',d');
    this.color = d3Scale.scaleOrdinal(colors);
    // d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition = d3Partition.partition();


    this.arc = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));

  }

  private drawPie(data) {
    this.root = d3Partition.hierarchy(this.dataTransform(data));
    this.root.sum(d => d.size);
    this.root.each(d => d.current = d);
    this.svg = d3.select('#graph')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      // .append('g')
      // .attr('style','padding-bottom: ' + Math.ceil(this.height * 100 / this.width) + '%')
      // .attr('transform', 'translate(' + this.width / 2 + ',' + this.height / 2 + ')')
      .on('click', () => this.focusOn());
    const slice = this.svg.selectAll('g.slice')
      .data(this.partition(this.root).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn(d);
      });
    this.datas = this.dataFilter({data: this.flares});
    this.dataSource = new MatTableDataSource(this.dataFilter({data: this.flares}));
    this.dataSource.sort = this.sort; //
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.6 : 0.4)
      .attr('d', this.arc);

    newSlice.append('path')
      .attr('class', 'hidden-arc')
      .attr('id', (_, i) => 'hiddenArc' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', '#5b5b5b');

    return this.dataSource;
  }

  private removeSvg() {
    this.svg.remove();
  }

  private focusOn(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg.transition()
      .duration(750)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc')
      .attrTween('d', d => () => this.arc(d));

    transition.selectAll('path.hidden-arc')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter(d));
    this.dataSource.sort = this.sort; //
    this.dataSource.paginator = this.paginator;
  }

  private textFits(d) {
    const CHAR_SPACE = 8;
    const deltaAngle = this.x(d.x1) - this.x(d.x0);
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const perimeter = r * deltaAngle;

    return d.data.name.length * CHAR_SPACE < perimeter;
  }

  private middleArcLine(d) {
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);

    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw
    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path.path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);
    return path.toString();
  }

  private dataTransform(data: Precandidatos[]) {
    this.flares = {
      name: 'Total de Precandidatos: ' + data.length,
      id: 'precandidatos',
      children: [],
    };
    const precandidatosActivos = [];
    const precandidatosBaja = [];
    const paCandidato = [];
    const paCapacitacion = [];
    const paSeguimiento1 = [];
    const paSeguimiento2 = [];
    const paEmpleado = [];
    const pbCandidato = [];
    const pbCapacitacion = [];
    const pbSeguimiento1 = [];
    const pbSeguimiento2 = [];
    const pbEmpleado = [];

    // const candidatos
    for (let i = 0; i < data.length; i++) {
      if (data[i].idEstadoRH === 1) {        // Precancidatos Activos
        precandidatosActivos.push(data[i]);
        if (data[i].idEtapa === 1) {  // Precandidato
          // paPrecandidato.push(data[i]);
        } else if (data[i].idEtapa === 2) {  // Candidato
          paCandidato.push(data[i]);
        } else if (data[i].idEtapa === 3) {  // Empleado
          paEmpleado.push(data[i]);
        } else if (data[i].idEtapa === 4) {  // Capacitación
          paCapacitacion.push(data[i]);
        } else if (data[i].idEtapa === 5) {  // Seguimiento 1
          paSeguimiento1.push(data[i]);
        } else if (data[i].idEtapa === 6) {  // Seguimiento 2
          paSeguimiento2.push(data[i]);
        }
      } else {        // Precancidatos Inactivos
        precandidatosBaja.push(data[i]);
        if (data[i].idEtapa === 1) {  // Precandidato
          // paPrecandidato.push(data[i]);
        } else if (data[i].idEtapa === 2) {  // Candidato
          pbCandidato.push(data[i]);
        } else if (data[i].idEtapa === 3) {  // Empleado
          pbEmpleado.push(data[i]);
        } else if (data[i].idEtapa === 4) {  // Capacitación
          pbCapacitacion.push(data[i]);
        } else if (data[i].idEtapa === 5) {  // Seguimiento 1
          pbSeguimiento1.push(data[i]);
        } else if (data[i].idEtapa === 6) {  // Seguimiento 2
          pbSeguimiento2.push(data[i]);
        }
      }
    }

    const activos = {
      name: 'Total de Activos: ' + precandidatosActivos.length,
      id: 'activos',
      children: [],
    };

    const bajas = {
      name: 'Total de Bajas: ' + precandidatosBaja.length,
      id: 'bajas',
      children: [],
    };
    const paCandi = {
      name: 'Candidato: ' + paCandidato.length,
      id: 'pacandidato',
      size: paCandidato.length,
    };
    const paCapa = {
      name: 'Teórico: ' + paCapacitacion.length,
      id: 'pacapacitacion',
      size: paCapacitacion.length,
    };
    const paSegui1 = {
      name: 'Roll Play: ' + paSeguimiento1.length,
      id: 'paseguimiento1',
      size: paSeguimiento1.length,
    };
    const paSegui2 = {
      name: 'Asignaciones: ' + paSeguimiento2.length,
      id: 'paseguimiento2',
      size: paSeguimiento2.length,
    };
    const paEmple = {
      name: 'Empleado: ' + paEmpleado.length,
      id: 'paempleado',
      size: paEmpleado.length,
    };
    const pbCandi = {
      name: 'Candidato: ' + pbCandidato.length,
      id: 'pbcandidato',
      size: pbCandidato.length,
    };
    const pbCapa = {
      name: 'Teórico: ' + pbCapacitacion.length,
      id: 'pbcapacitacion',
      size: pbCapacitacion.length,
    };
    const pbSegui1 = {
      name: 'Roll Play: ' + pbSeguimiento1.length,
      id: 'pbseguimiento1',
      size: pbSeguimiento1.length,
    };
    const pbSegui2 = {
      name: 'Asiganciones: ' + pbSeguimiento2.length,
      id: 'pbseguimiento2',
      size: pbSeguimiento2.length,
    };
    const pbEmple = {
      name: 'Empleado: ' + pbEmpleado.length,
      id: 'pbempleado',
      size: pbEmpleado.length,
    };

    activos.children.push(paCandi);
    activos.children.push(paCapa);
    activos.children.push(paSegui1);
    activos.children.push(paSegui2);
    activos.children.push(paEmple);
    bajas.children.push(pbCandi);
    bajas.children.push(pbCapa);
    bajas.children.push(pbSegui1);
    bajas.children.push(pbSegui2);
    bajas.children.push(pbEmple);

    this.flares.children.push(bajas);
    this.flares.children.push(activos);
    return this.flares;
  }

  private dataFilter(d: any) {
    const data = this.precandidatos;
    const precandidatosActivos = [];
    const precandidatosBaja = [];
    const paCandidato = [];
    const paCapacitacion = [];
    const paSeguimiento1 = [];
    const paSeguimiento2 = [];
    const paEmpleado = [];
    const pbCandidato = [];
    const pbCapacitacion = [];
    const pbSeguimiento1 = [];
    const pbSeguimiento2 = [];
    const pbEmpleado = [];

    // const candidatos
    for (let i = 0; i < data.length; i++) {
      if (data[i].idEstadoRH === 1) {        // Precancidatos Activos
        precandidatosActivos.push(data[i]);
        if (data[i].idEtapa === 1) {  // Precandidato
          // paPrecandidato.push(data[i]);
        } else if (data[i].idEtapa === 2) {  // Candidato
          paCandidato.push(data[i]);
        } else if (data[i].idEtapa === 3) {  // Empleado
          paEmpleado.push(data[i]);
        } else if (data[i].idEtapa === 4) {  // Capacitación
          paCapacitacion.push(data[i]);
        } else if (data[i].idEtapa === 5) {  // Seguimiento 1
          paSeguimiento1.push(data[i]);
        } else if (data[i].idEtapa === 6) {  // Seguimiento 2
          paSeguimiento2.push(data[i]);
        }
      } else {        // Precancidatos Inactivos
        precandidatosBaja.push(data[i]);
        if (data[i].idEtapa === 1) {  // Precandidato
          // paPrecandidato.push(data[i]);
        } else if (data[i].idEtapa === 2) {  // Candidato
          pbCandidato.push(data[i]);
        } else if (data[i].idEtapa === 3) {  // Empleado
          pbEmpleado.push(data[i]);
        } else if (data[i].idEtapa === 4) {  // Capacitación
          pbCapacitacion.push(data[i]);
        } else if (data[i].idEtapa === 5) {  // Seguimiento 1
          pbSeguimiento1.push(data[i]);
        } else if (data[i].idEtapa === 6) {  // Seguimiento 2
          pbSeguimiento2.push(data[i]);
        }
      }
    }

    if (d.data.id === 'precandidatos') {
      this.label = 'TOTAL DE PRE-CANDIDATOS:';
      this.dates = data.length;
      return data;
    } else if (d.data.id === 'activos') {
      this.label =  'TOTAL DE ACTIVOS:';
      this.dates = precandidatosActivos.length;
      return precandidatosActivos;
    } else if (d.data.id === 'bajas') {
      this.label =  'TOTAL DE BAJAS:';
      this.dates = precandidatosBaja.length;
      return precandidatosBaja;
    } else if (d.data.id === 'pacandidato') {
      this.label =  'TOTAL EN CANDIDATOS:';
      this.dates = paCandidato.length;
      return paCandidato;
    } else if (d.data.id === 'pacapacitacion') {
      this.label =  'TOTAL EN TEORICO:';
      this.dates = paCapacitacion.length;
      return paCapacitacion;
    } else if (d.data.id === 'paseguimiento1') {
      this.label =  'TOTAL EN ROL PLAY:';
      this.dates = paSeguimiento1.length;
      return paSeguimiento1;
    } else if (d.data.id === 'paseguimiento2') {
      this.label =  'TOTAL EN ASIGNACION:';
      this.dates = paSeguimiento2.length;
      return paSeguimiento2;
    } else if (d.data.id === 'paempleado') {
      this.label =  'TOTAL EN EMPLEADO :';
      this.dates = paEmpleado.length;
      return paEmpleado;
    } else if (d.data.id === 'pbcandidato') {
      this.label =  'TOTAL EN CANDIDATO:';
      this.dates = pbCandidato.length;
      return pbCandidato;
    } else if (d.data.id === 'pbcapacitacion') {
      this.label =  'TOTAL EN TEORICO:';
      this.dates = pbCapacitacion.length;
      return pbCapacitacion;
    } else if (d.data.id === 'pbseguimiento1') {
      this.label =  'TOTAL EN ROL PLAY:';
      this.dates = pbSeguimiento1.length;
      return pbSeguimiento1;
    } else if (d.data.id === 'pbseguimiento2') {
      this.label =  'TOTAL EN ASIGNACION:';
      this.dates = pbSeguimiento2.length;
      return pbSeguimiento2;
    } else if (d.data.id === 'pbempleado') {
      this.label =  'TOTAL EN EMPLEADOS:';
      this.dates = pbEmpleado.length;
      return pbEmpleado;
    }
  }
}
