import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Registro, RegistrosData} from '../../interfaces/ventaNueva/registro';
import {Observable} from 'rxjs';

@Injectable()
export class RegistroService extends RegistrosData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Registro[]> {
    return this.http.get<Registro[]>(this.baseURL + 'v1/registro-vn');
  }

  post(registro: Registro): Observable<Registro> {
    return this.http.post<Registro>(this.baseURL + 'v1/registro-vn/', registro);
  }

  getRegistroById(idRegistro): Observable<Registro> {
    return this.http.get<Registro>(this.baseURL + 'v1/registro-vn/' + idRegistro);
  }

  put(idRegistro, registro: Registro): Observable<Registro> {
    return this.http.put<Registro>(this.baseURL + 'v1/registro-vn/' + idRegistro, registro);
  }

}
