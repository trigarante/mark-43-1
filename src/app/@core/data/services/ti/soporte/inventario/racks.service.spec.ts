import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Racks Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 8,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    folio: '0001R',
    idMarca: 1,
    fechaRecepcion: '2019-09-24T17:03:58.000+0000',
    fechaSalida: null,
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    origenRecurso: 'propio',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'racks',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/racks');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/racks', '/8');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/racks');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/racks', '/8/74');
      });
    });
  }
});
