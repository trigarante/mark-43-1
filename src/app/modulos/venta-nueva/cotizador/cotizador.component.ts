import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Auto, Catalogo, CatalogoData} from '../../../@core/data/interfaces/cotizador/catalogo';
import {CotizarData, CotizarRequest, CotizarResponse} from '../../../@core/data/interfaces/cotizador/cotizar';
import {Pagina, PaginasData} from '../../../@core/data/interfaces/marketing/pagina';
import {ActivatedRoute, Router} from '@angular/router';
import {TipoProducto, TipoProductoData} from '../../../@core/data/interfaces/comerciales/catalogo/tipo-producto';
import {filter, map} from 'rxjs/operators';
import {ProductoSolicitudData} from '../../../@core/data/interfaces/ventaNueva/producto-solicitud';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CotizacionesData} from '../../../@core/data/interfaces/ventaNueva/cotizaciones';
import {CotizacionesAliData} from '../../../@core/data/interfaces/ventaNueva/cotizaciones-ali';
import {SolicitudesVNData} from '../../../@core/data/interfaces/ventaNueva/solicitudes';
import {NbDialogService} from '@nebular/theme';
import {SociosData} from '../../../@core/data/interfaces/comerciales/socios-comercial';
import {trigger, state, animate, transition, style} from '@angular/animations' ;
import {SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';



@Component({
  selector: 'ngx-cotizador',
  templateUrl: './cotizador.component.html',
  styleUrls: ['./cotizador.component.scss'],
  animations: [
    trigger('car', [
      state('true', style({
        opacity: 1,
      })),
      state('void', style({
        opacity: 0,
      })),
      transition(':enter', animate('1000ms ease-in-out')),
      transition(':leave', animate('1000ms ease-in-out')),
    ]),
    trigger('size', [
      state('true', style({opacity: 1})),
      state('void', style({opacity: 0})),
      transition(':enter', animate('700ms ease-in-out')),
    ]),
  ],
})

export class CotizadorComponent implements OnInit {


  // @ViewChild('videocar') matVideo: MatVideoComponent;
  // video: HTMLVideoElement;
  disabledBtn: boolean = false;
  formCotizacion: FormGroup;
  @ViewChild('aseguradoras') aseguradoraContent: ElementRef;
  @Input() idProspecto: number = this.rutaActiva.snapshot.params.idProspectoExis;
  tipoProductos: TipoProducto[];
  idTipoCategoria: any;
  idSolicitud: any;
  autoSelected: Auto = new Auto();
  marcas: Catalogo[];
  modelos: Catalogo[];
  descripciones: Catalogo[];
  subdescripciones: Catalogo[];
  detalles: Catalogo[];
  cotizacionRequest = null;
  cotizacionResponse: any;
  pagina: Pagina;
  cpValid: boolean = false;
  config: Object[];
  configMotos: Object[];
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  descuentos = Array.from(Array(20).keys(), n => n + 1);
  aseguradoras: Object[] = [{value: '', aseguradora: 'Todas'}];
  aseguradorasMotos: Object[] = [{value: '', aseguradora: 'Todas'}];
  cotizacionesAliForm: any = {
    idCotizacion: null,
    idSubRamo: null,
    peticion: null,
    respuesta: null,
    // prima: null,
    fechaActualizacion: null,
  };
  productoSolicitudForm: any = {
    'idProspecto': null,
    'idTipoCategoria': null,
    'datos': null,
  };
  cotizacionesForm: any = {
    'idProducto': null,
    'idPagina': null,
    'idMedioDifusion': null,
    'idTipoContacto': 1,
    'idEstadoCotizacion': 1,
  };
  solicitudForm: any = {
    'idCotizacionAli': null,
    'idEmpleado': 74,
    'idEstadoSolicitud': 1,
    'comentarios': ' ',
  };
  todas: boolean;
  /**Variable que almacena el ID de solicitud para emitirlo al modal Emisione*/
  idProductoSolicitud: any;
  loader: boolean = false;
  show: any = -1;
  datos: any[] = [];
  coberturas: {
    AsitenciaCompleta: String,
    Cristales: String,
    DanosMateriales: String,
    DanosMaterialesPP: String,
    DefensaJuridica: String,
    GastosMedicosEvento: String,
    GastosMedicosOcupantes: String,
    MuerteAccidental: String,
    RC: String,
    RCBienes: String,
    RCPersonas: String,
    RCExtension: String,
    RoboTotal: String,
  };
  danosMateriales: any[] = [];
  roboTotal: any[] = [];
  rc_bienes: any[] = [];
  rc_personas: any[] = [];
  asistenciaVial: any[] = [];
  gastosLegales: any[] = [];
  gastosMedicos: any[] = [];

  constructor(private catalogoService: CatalogoData, private cotizarService: CotizarData, private paginaService: PaginasData,
              private rutaActiva: ActivatedRoute, private tipoProductoService: TipoProductoData,
              private productoSolicitudService: ProductoSolicitudData, private cotizacionesService: CotizacionesData,
              private cotizacionesAliService: CotizacionesAliData, private solictudesVnService: SolicitudesVNData,
              private router: Router, private dialogoService: NbDialogService, private sociosService: SociosData,
              private sepomexService: SepomexData, private fb: FormBuilder, private renderer: Renderer2) {

    // $('#title').on('click', function()  {
    //   console.log('pressed');
    // });

    // console.log(this.aseguradoraContent);

    this.paginaService.getPaginaById(5).subscribe(z => {
      this.pagina = z;
      if (this.pagina) {
        /**
         * Parsea configuracion de autos
         */
        try {
          this.config = JSON.parse(this.pagina.configuracion)['autos']['aseguradoras'];
          this.config.forEach(c => {
            this.aseguradoras.push({value: c['aseguradora'], aseguradora: c['aseguradora']});
          });
        } catch (e) {
          console.error(`Error al parsear JSON ${e}`);
        }

        /**
         * Parsea configuracion de motos
         */
        try {
          this.configMotos = JSON.parse(this.pagina.configuracion)['motos']['aseguradoras'];
          this.configMotos.forEach(c => {
            this.aseguradorasMotos.push({value: c['aseguradora'], aseguradora: c['aseguradora']});
          });
        } catch (e) {
          console.error(`Error al parsear JSON ${e}`);
        }

      }
    });
    this.getMarcas();
  }

  ngOnInit() {
    // this.video.controls = false;

    this.formCotizacion = this.fb.group({
      'aseguradoras': new FormControl(''),
      'edad': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.compose([Validators.required])),
      'genero': new FormControl('', Validators.required),
      'marca': new FormControl('', Validators.required),
      'modelos': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'subdescripcion': new FormControl('', Validators.required),
      'tipoSubRamo': new FormControl('', Validators.required),
      'descuento': new FormControl(''),
      'detalles': new FormControl(''),
    });

    this.getTipoProducto();
  }


  toggle(i) {
    if (this.show === i) {
      this.show = -1;
    } else {
      this.show = i;
    }

  }

  resetForm() {
    this.formCotizacion.reset();
    this.disabledBtn = false;
  }

  getMarcas() {
    this.autoSelected.marca = new Catalogo();
    this.autoSelected.modelo = new Catalogo();
    this.autoSelected.descripcion = new Catalogo();
    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.cotizacionResponse = null;
    this.catalogoService.getMarcas(this.autoSelected.aseguradora).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {

    this.autoSelected.descripcion = new Catalogo();
    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getModelos(this.autoSelected.aseguradora, this.autoSelected.marca.text).subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {
    // console.log(this.autoSelected.aseguradora.length);
    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getDescripcion(this.autoSelected.aseguradora, this.autoSelected.marca.text, this.autoSelected.modelo.text)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {
    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getSubdescripcion(this.autoSelected.aseguradora,
      this.autoSelected.marca.text, this.autoSelected.modelo.text,
      this.autoSelected.descripcion.text).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  getDetalles() {
    if (this.autoSelected.aseguradora === '') {
      this.catalogoService.getDetallesHomologado(this.autoSelected.marca.text, this.autoSelected.modelo.text,
        this.autoSelected.descripcion.text, this.autoSelected.subdescripcion.text)
        .subscribe(d => {
          this.detalles = d;
        });
    } else {
      this.catalogoService.getDetalles(this.autoSelected.aseguradora, this.autoSelected.marca.text, this.autoSelected.modelo.text
        , this.autoSelected.descripcion.text, this.autoSelected.subdescripcion.text).subscribe(m => {
        this.detalles = m;
      });
    }

  }

  getTipoProducto() {
    this.tipoProductoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(result => {
      this.tipoProductos = result;
    });
  }

  validarCp(value) {
    const cpParse = parseInt(value);
    this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
      if (res.length !== 0) {
        this.formCotizacion.controls['cp'].setErrors(null);
      } else {
        this.formCotizacion.controls['cp'].setErrors({'incorrect': true});
      }
    }, error => error);
  }

  cotizar() {
    const aseguradorasConPermiso = [];
    this.cotizacionResponse = [];
    if (this.autoSelected.aseguradora === '') {
      this.todas = true;
      this.detalles.forEach(detalle => {
        const aseguradora = this.config.filter(c => {
          if (c['aseguradora'] === detalle['aseguradora'] && c['cotizacion']) {
            aseguradorasConPermiso.push(detalle);
            return true;
          }
        });
      });
      aseguradorasConPermiso.forEach(a => {
        this.cotizacionRequest = new CotizarRequest(a['aseguradora'], a['id'], this.formCotizacion.controls['cp'].value,
          this.autoSelected.descripcion.text, 0, this.autoSelected.edad,
          '01/01/' + (new Date().getFullYear() - Number(this.autoSelected.edad)),
          this.autoSelected.genero, this.autoSelected.marca.text,
          this.autoSelected.modelo.text, 'cotizacion', 'AMPLIA', 'PARTICULAR');
        this.cotizarService.cotizar(this.cotizacionRequest).subscribe(z => {
          // ****************************
          this.disabledBtn = true;
          this.cotizacionResponse.push(z);
        });
      });
      // this.cotizacionRequest.aseguradora = 'TODAS'
    } else {
      this.cotizacionRequest = new CotizarRequest(this.autoSelected.aseguradora, this.detalles[0]['id'],
        this.formCotizacion.controls['cp'].value,
        this.autoSelected.descripcion.text, Number(this.autoSelected.descuento), this.autoSelected.edad,
        '01/01/' + (new Date().getFullYear() - Number(this.autoSelected.edad)),
        this.autoSelected.genero, this.autoSelected.marca.text,
        this.autoSelected.modelo.text, 'cotizacion', 'AMPLIA', 'PARTICULAR');
      this.cotizarService.cotizar(this.cotizacionRequest).subscribe(z => {
        // ***************************

        this.disabledBtn = true;
        this.cotizacionResponse.push(z);
        // **Obtener datos de Coberturas -> Detalles de Producto**//
        this.cotizacionResponse.forEach(res => {

          this.coberturas = res['Coberturas'][0];

          const danos = this.coberturas.DanosMateriales.split(/\s*-\s*/);
          const roboTotal = this.coberturas.RoboTotal.split(/\s*-\s*/);
          const rcBienes = this.coberturas.RCBienes.split(/\s*-\s*/);
          const rcPersonas = this.coberturas.RCPersonas.split(/\s*-\s*/);
          const asistenciaVial = this.coberturas.AsitenciaCompleta.split(/\s*-\s*/);
          const gastosLegales = this.coberturas.DefensaJuridica.split(/\s*-\s*/);
          const gastosMedicos = this.coberturas.GastosMedicosOcupantes.split(/\s*-\s*/);


          if (this.coberturas.DanosMateriales === '-') {
            this.danosMateriales.push('-'); // nombre
          } else {
            this.danosMateriales.push(danos[1].substr(1)); // nombre
            this.danosMateriales.push(danos[2].substr(1)); // Cobertura $
            this.danosMateriales.push(danos[3].substr(1)); // Deducible $
          }

          if (this.coberturas.RoboTotal === '-') {
            this.roboTotal.push('-');
          } else {
            this.roboTotal.push(roboTotal[1].substr(1));
            this.roboTotal.push(roboTotal[2].substr(1));
            this.roboTotal.push(roboTotal[3].substr(1));
          }

          if (this.coberturas.RCBienes === '-') {
            this.rc_bienes.push('-');
          } else {
            this.rc_bienes.push(rcBienes[1].substr(1));
            this.rc_bienes.push(rcBienes[2].substr(1));
            this.rc_bienes.push(rcBienes[3].substr(1));
          }

          if (this.coberturas.RCPersonas === '-') {
            this.rc_personas.push(rcPersonas[1]);
            this.rc_personas.push(rcPersonas[2]);
            this.rc_personas.push(rcPersonas[3]);
          } else {
            this.rc_personas.push(rcPersonas[1].substr(1));
            this.rc_personas.push(rcPersonas[2].substr(1));
            this.rc_personas.push(rcPersonas[3].substr(1));
          }

          if (this.coberturas.AsitenciaCompleta === '-') {
            this.asistenciaVial.push('-');
          } else {
            this.asistenciaVial.push(asistenciaVial[1].substr(1));
            this.asistenciaVial.push(asistenciaVial[2].substr(1));
            this.asistenciaVial.push(asistenciaVial[3].substr(1));
          }


          if (this.coberturas.DefensaJuridica === '-') {
            this.gastosLegales.push('-');
          } else {
            this.gastosLegales.push(gastosLegales[1].substr(1));
            this.gastosLegales.push(gastosLegales[2].substr(1));
            this.gastosLegales.push(gastosLegales[3].substr(1));
          }

          if (this.coberturas.GastosMedicosOcupantes === '-') {
            this.gastosMedicos.push('-');
          } else {
            this.gastosMedicos.push(gastosMedicos[1].substr(1));
            this.gastosMedicos.push(gastosMedicos[2].substr(1));
            this.gastosMedicos.push(gastosMedicos[3].substr(1));
          }

          //
          // console.log(this.danosMateriales);
          // console.log(this.roboTotal);
          // console.log(this.rc_bienes);
          // console.log(this.rc_personas);
          // console.log(this.asistenciaVial);
          // console.log(this.gastosLegales);
          // console.log(this.gastosMedicos);

          // console.log(this.danosMateriales);
          // console.log(this.roboTotal);
          // console.log(this.rc_bienes);
          // console.log(this.rc_personas);
          // console.log(this.asistenciaVial);
          // console.log(this.gastosLegales);
          // console.log(this.gastosMedicos);

        });
      });
    }
    // *******************************************************************************************************
    // this.cotizacionResponse = [{'Aseguradora': 'QUALITAS', 'Cotizacion': {'PrimaTotal': 16000, 'Logo': 'qualitas'}},
    //   {'Aseguradora': 'GNP', 'Cotizacion': {'PrimaTotal': 16000, 'Logo': 'gnp'}},
    //   {'Aseguradora': 'AXA', 'Cotizacion': {'PrimaTotal': 16000, 'Logo': 'axa'}},
    //   {'Aseguradora': 'MAPFRE', 'Cotizacion': {'PrimaTotal': 16000, 'Logo': 'mapfre'}}];
    // if (this.todas) {
    //   this.cotizacionRequest.aseguradora = 'TODAS';
    // }else {
    //   for (let i = 0; i < this.cotizacionResponse.length; i++) {
    //     if (this.cotizacionRequest.aseguradora === this.cotizacionResponse[i].Aseguradora) {
    //       const cotResponse: any = this.cotizacionResponse[i];
    //       this.cotizacionResponse = [];
    //       this.cotizacionResponse.push(cotResponse);
    //     }
    //   }
    // }
    this.productoSolicitudForm.idProspecto = this.idProspecto;
    this.productoSolicitudForm.datos = JSON.stringify(this.cotizacionRequest);
    this.productoSolicitudForm.idTipoCategoria = this.idTipoCategoria;
    this.productoSolicitudService.post(this.productoSolicitudForm).subscribe(result => {
        this.idProductoSolicitud = this.cotizacionesForm.idProducto = result.id;
      },
      error1 => {
      },
      () => {
        this.cotizacionesService.post(this.cotizacionesForm).subscribe(
          result => {
            this.cotizacionesAliForm.idCotizacion = result.id;
            this.cotizacionesAliForm.peticion = JSON.stringify(this.cotizacionRequest);
          },
        );
      },
    );
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
    }, 7000);
  }

  crearEmision(aseguradora, indice) {
    /**servicio getbydescripcion en sub ramo para obtener el ID*/
    this.sociosService.getIdByAlias(aseguradora).subscribe(
      result => {
        this.cotizacionesAliForm.idSubRamo = result.id;
      },
      error1 => { },
      () => {
        this.cotizacionesAliForm.respuesta = JSON.stringify(this.cotizacionResponse[indice]);
        this.cotizacionesAliForm.fechaActualizacion = new Date();
        this.cotizacionesAliService.post(this.cotizacionesAliForm).subscribe(data => {
            this.solicitudForm.idCotizacionAli = data.id;
            this.solictudesVnService.post(this.solicitudForm).subscribe(
              r => {
                this.idSolicitud = r.id;
              },
              e => {},
              () => {
                // this.dialogoService.open(CreateEmisionComponent, {
                //   context: {
                //     idProductoSolicitud: this.idProductoSolicitud,
                //     idSolicitud: this.idSolicitud,
                //     idSubRamo: this.cotizacionesAliForm.idSubRamo,
                //   }
                this.router.navigate(['modulos/venta-nueva/step-cotizador'], {
                  state: {
                    aseguradora: aseguradora,
                    idProductoSolicitud: this.idProductoSolicitud,
                    idSolicitud: this.idSolicitud,
                    idSubRamo: this.cotizacionesAliForm.idSubRamo,
                  },
                });
              });
          },
        );
      });
  }


  guardarProducto() {
    // let prima: any;

    // productoSolicitudForm = {
    //   'idProspecto': Number(this.idProspecto),
    //   'idTipoCategoria': this.idTipoCategoria,
    //   'datos': this.cotizacionRequest,
    // };
    // const cotizacionesForm: any = {
    //   'idProducto': null,
    //   'idPagina': null,
    //   'idMedioDifusion': null,
    //   'idTipoContacto': 1,
    //   'idEstadoCotizacion': 1,
    // };
    // const solicitudes: any = {
    //   'idCotizacionAli': null,
    //   'idEmpleado': 74,
    //   'idEstadoSolicitud': 1,
    //   'comentarios': ' ',
    // };
    // productoSolicitudForm.datos = JSON.stringify(productoSolicitudForm.datos);
    // this.productoSolicitudService.post(productoSolicitudForm).subscribe(result => {
    //   cotizacionesForm.idProducto = result.id;
    // },
    //  error1 => {},
    //   () => {
    //     this.cotizacionesService.post(cotizacionesForm).subscribe(
    //     result => {
    //       prima = this.cotizacionResponse[0].Cotizacion.PrimaTotal;
    //       if (prima !== null) {
    //         prima = Number(prima);
    //       } else {
    //         prima = 0;
    //       }
    //       this.cotizacionesAliForm.idCotizacion = result.id;
    //       this.cotizacionesAliForm.respuesta = JSON.stringify(this.cotizacionResponse);
    //       if (this.todas) {
    //         this.cotizacionRequest.aseguradora = 'TODAS';
    //       }
    //       this.cotizacionesAliForm.peticion = JSON.stringify(this.cotizacionRequest);
    //       this.cotizacionesAliForm.prima = prima;
    //       // console.log(this.cotizacionRequest);
    //   ********************************************************************************************
    //         this.cotizacionesAliService.post(this.cotizacionesAliForm).subscribe(data => {
    //           solicitudes.idCotizacionAli = data.id;
    //           this.solictudesVnService.post(solicitudes).subscribe(
    //             r => {},
    //             e => {},
    //             () => {
    //               this.router.navigateByUrl('/modulos/venta-nueva/solicitudes-vn');
    //             },
    //             );
    //         });
    //       });
    //     },
    //     );
  }

}
