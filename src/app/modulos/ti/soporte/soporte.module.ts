import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SoporteRoutingModule} from './soporte-routing.module';
import {SoporteComponent} from './soporte.component';
import {ThemeModule} from '../../../@theme/theme.module';
import {TableModule} from 'primeng/table';
import {NbDialogModule} from '@nebular/theme';
import {AutoCompleteModule, DropdownModule} from 'primeng/primeng';
import {AsignarEquipoCreateComponent} from './modals/asignar-equipo-create/asignar-equipo-create.component';
import {TooltipModule} from 'primeng/tooltip';
import { InventarioComponent } from './inventario/inventario.component';
import {
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatDialogModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule,
  MatTableModule, MatToolbarModule, MatAutocompleteModule,
} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import { VerDetalleComponent } from './modals/ver-detalle/ver-detalle.component';
import { CpuCreateComponent } from './modals/cpu-create/cpu-create.component';
import { MonitorCreateComponent } from './modals/monitor-create/monitor-create.component';
import { LaptopCreateComponent } from './modals/laptop-create/laptop-create.component';
import { TecladoCreateComponent } from './modals/teclado-create/teclado-create.component';
import { MouseCreateComponent } from './modals/mouse-create/mouse-create.component';
import { DiademeCreateComponent } from './modals/diademe-create/diademe-create.component';
import { TabletCreateComponent } from './modals/tablet-create/tablet-create.component';
import {AsignarEquipoComponent} from './modals/asignar-equipo/asignar-equipo.component';
import {DesactivarEquipoComponent} from './catalogos-soporte/modals/desactivar-equipo/desactivar-equipo.component';
import { BajaInventarioComponent } from './baja-inventario/baja-inventario.component';
import {CreateMarcaComponent} from './catalogos-soporte/modals/create-marca/create-marca.component';
import { InventarioRedesComponent } from './inventario-redes/inventario-redes.component';
import { InventarioSoporteComponent } from './inventario-soporte/inventario-soporte.component';
import { SwitchsCreateComponent } from './modals/switchs-create/switchs-create.component';
import { ServidoresCreateComponent } from './modals/servidores-create/servidores-create.component';
import { TarjetaRedCreateComponent } from './modals/tarjeta-red-create/tarjeta-red-create.component';
import { TelefonoIpCreateComponent } from './modals/telefono-ip-create/telefono-ip-create.component';
import { PatchPanelCreateComponent } from './modals/patch-panel-create/patch-panel-create.component';
import { CharolasRackCreateComponent } from './modals/charolas-rack-create/charolas-rack-create.component';
import { RacksCreateComponent } from './modals/racks-create/racks-create.component';
import { BajaInventarioRedesComponent } from './baja-inventario-redes/baja-inventario-redes.component';
import { DiscoDuroExternoCreateComponent } from './modals/disco-duro-externo-create/disco-duro-externo-create.component';
import { DiscoDuroInternoCreateComponent } from './modals/disco-duro-interno-create/disco-duro-interno-create.component';
import { ChromeBitCreateComponent } from './modals/chrome-bit-create/chrome-bit-create.component';
import { ApsCreateComponent } from './modals/aps-create/aps-create.component';
import { BiometricosCreateComponent } from './modals/biometricos-create/biometricos-create.component';
import { DvrCreateComponent } from './modals/dvr-create/dvr-create.component';
import { CamaraCreateComponent } from './modals/camara-create/camara-create.component';
import { BarraMulticontactoCreateComponent } from './modals/barra-multicontacto-create/barra-multicontacto-create.component';
import { CorrienteCamaraCreateComponent } from './modals/corriente-camara-create/corriente-camara-create.component';
import { BajaInventarioSoporteComponent } from './baja-inventario-soporte/baja-inventario-soporte.component';


@NgModule({
  declarations: [SoporteComponent,
    AsignarEquipoCreateComponent,
    InventarioComponent,
    VerDetalleComponent,
    CpuCreateComponent,
    MonitorCreateComponent,
    LaptopCreateComponent,
    TecladoCreateComponent,
    MouseCreateComponent,
    DiademeCreateComponent,
    TabletCreateComponent,
    AsignarEquipoComponent,
    DesactivarEquipoComponent,
    BajaInventarioComponent,
    InventarioRedesComponent,
    InventarioSoporteComponent,
    SwitchsCreateComponent,
    ServidoresCreateComponent,
    TarjetaRedCreateComponent,
    TelefonoIpCreateComponent,
    PatchPanelCreateComponent,
    CharolasRackCreateComponent,
    RacksCreateComponent,
    BajaInventarioRedesComponent,
    DiscoDuroExternoCreateComponent,
    DiscoDuroInternoCreateComponent,
    ChromeBitCreateComponent,
    ApsCreateComponent,
    BiometricosCreateComponent,
    DvrCreateComponent,
    CamaraCreateComponent,
    BarraMulticontactoCreateComponent,
    CorrienteCamaraCreateComponent,
    BajaInventarioSoporteComponent,
  ],
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatTabsModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    CommonModule,
    SoporteRoutingModule,
    ThemeModule,
    TableModule,
    NbDialogModule.forChild(),
    DropdownModule,
    AutoCompleteModule,
    TooltipModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatAutocompleteModule,
  ],
  entryComponents: [
    AsignarEquipoCreateComponent,
    InventarioComponent,
    VerDetalleComponent,
    CpuCreateComponent,
    MonitorCreateComponent,
    LaptopCreateComponent,
    TecladoCreateComponent,
    MouseCreateComponent,
    DiademeCreateComponent,
    TabletCreateComponent,
    AsignarEquipoComponent,
    DesactivarEquipoComponent,
    SwitchsCreateComponent,
    ServidoresCreateComponent,
    TarjetaRedCreateComponent,
    TelefonoIpCreateComponent,
    PatchPanelCreateComponent,
    CharolasRackCreateComponent,
    RacksCreateComponent,
    DiscoDuroExternoCreateComponent,
    DiscoDuroInternoCreateComponent,
    ChromeBitCreateComponent,
    ApsCreateComponent,
    BiometricosCreateComponent,
    DvrCreateComponent,
    CamaraCreateComponent,
    BarraMulticontactoCreateComponent,
    CorrienteCamaraCreateComponent,

  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTabsModule,
    MatAutocompleteModule,
  ],
})
export class SoporteModule {
}
