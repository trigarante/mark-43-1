import {Component, OnInit, ViewChild} from '@angular/core';
import {
  AnalisisCompetencia,
  AnalisisCompetenciaData,
} from '../../../@core/data/interfaces/comerciales/analisis-competencia';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {DatosAnalisiCompetenciaComponent} from '../modals/datos-analisi-competencia/datos-analisi-competencia.component';
import {AsignarCoberturaComponent} from '../modals/asignar-cobertura/asignar-cobertura.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from "@angular/router";

@Component({
  selector: 'ngx-analisis-competencia',
  templateUrl: './analisis-competencia.component.html',
  styleUrls: ['./analisis-competencia.component.scss'],
})
export class AnalisisCompetenciaComponent implements OnInit {
  cols: any[];
  analisisCompetencia: AnalisisCompetencia[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(protected analisisCompetenciaService: AnalisisCompetenciaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {}

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getAnalisisCompetencia(0);
    this.cols = [
      'detalle',
      'idCompetencia',
      'precios',
      'participacion',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getAnalisisCompetencia(mensajeAct) {
    this.analisisCompetenciaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.analisisCompetencia = [];
      this.analisisCompetencia = data;
      for (let i = 0; i < this.analisisCompetencia.length; i++) {
        let objetivo: any = this.analisisCompetencia[i].participacion;
        let formatNumber: any = '';
        objetivo = objetivo.toString().replace(/,/g, '');
        if (objetivo.length > 3 && objetivo.length < 7) {
          formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
          this.analisisCompetencia[i].participacion = formatNumber;
        } else if (objetivo.length > 6 && objetivo.length < 10) {
          formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
          this.analisisCompetencia[i].participacion = formatNumber;
        } else if (objetivo.length > 9 && objetivo.length < 13) {
          formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
            ',' + objetivo.slice(-3);
          this.analisisCompetencia[i].participacion = formatNumber;
        }
        /**E***************************************************************/
        let objetivoPre: any = this.analisisCompetencia[i].precios;
        let formatNumberPre: any = '';
        objetivoPre = objetivoPre.toString().replace(/,/g, '');
        if (objetivoPre.length > 3 && objetivoPre.length < 7) {
          formatNumberPre = objetivoPre.slice(0, objetivoPre.length - 3) + ',' + objetivoPre.slice(-3);
          this.analisisCompetencia[i].precios = formatNumberPre;
        } else if (objetivoPre.length > 6 && objetivoPre.length < 10) {
          formatNumberPre = objetivoPre.slice(0, objetivoPre.length - 6) + ',' + objetivoPre.slice(-6, -3) + ',' + objetivoPre.slice(-3);
          this.analisisCompetencia[i].precios = formatNumberPre;
        } else if (objetivoPre.length > 9 && objetivoPre.length < 13) {
          formatNumberPre = objetivoPre.slice(0, objetivoPre.length - 9) + ',' + objetivoPre.slice(-9, -6) + ',' +
            objetivoPre.slice(-6, -3) + ',' + objetivoPre.slice(-3);
          this.analisisCompetencia[i].precios = formatNumberPre;
        }
      }
      this.dataSource = new MatTableDataSource(this.analisisCompetencia); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Analisis Competencia`);
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  verDatos(idAnalisisCompetencia) {
    this.dialogService.open(DatosAnalisiCompetenciaComponent, {
      context: {
        idAnalisisCompetencia: idAnalisisCompetencia,
      },
    }).onClose.subscribe(x => {
      this.getAnalisisCompetencia(1);
    });
  }

  asignarCobertura(idProducto) {
    this.dialogService.open(AsignarCoberturaComponent, {
      context: {
        idTipo: 1,
        idProducto: idProducto,
      },
    }).onClose.subscribe( x => {
      this.getAnalisisCompetencia(0);
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }


  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'analisis-competencia');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelAnalisisCompetencia', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getAnalisisCompetencia(0);
        }, 500);
      });
    })
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.analisisCompetencia.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.analisisCompetencia.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
