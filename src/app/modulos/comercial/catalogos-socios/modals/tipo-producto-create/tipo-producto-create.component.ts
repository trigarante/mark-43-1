import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {Router} from '@angular/router';
import {TipoProducto, TipoProductoData} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-producto';

@Component({
  selector: 'ngx-tipo-producto-create',
  templateUrl: './tipo-producto-create.component.html',
  styleUrls: ['./tipo-producto-create.component.scss'],
})
export class TipoProductoCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTipoProducto: number;
  tipoProductoCreateForm: FormGroup;
  submitted: Boolean;
  tipoProducto: TipoProducto;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoPreoductoServe: TipoProductoData) {
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  guardarTipoProducto() {
    this.submitted = true;
    if (this.tipoProductoCreateForm.invalid) {
      return;
    }
    this.tipoPreoductoServe.post(this.tipoProductoCreateForm.value).subscribe((result) => {
      this.tipoPreoductoServe.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoProductoCreateForm.reset();
      this.ref.close();
    });
  }

  updateTipoProducto() {
    this.tipoPreoductoServe.put(this.idTipoProducto, this.tipoProductoCreateForm.value).subscribe(result => {
      this.tipoPreoductoServe.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoProductoCreateForm.reset();
      this.ref.close();
    });
  }

  getTipoProductoById() {
    this.tipoPreoductoServe.getTipoProducto(this.idTipoProducto).subscribe(data => {
      this.tipoProducto = data;
      this.tipoProductoCreateForm.controls['tipo'].setValue(this.tipoProducto.tipo);
    });
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTipoProductoById();
    }
    this.tipoProductoCreateForm = this.fb.group({
      'tipo': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }

}
