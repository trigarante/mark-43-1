import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ComercialRoutingModule} from './comercial-routing.module';
import {ComercialComponent} from './comercial.component';
import {ThemeModule} from '../../@theme/theme.module';
import {ProductoComponent} from './producto/producto.component';
import {CoberturaComponent} from './cobertura/cobertura.component';
import {EjecutivoCuentaComponent} from './ejecutivo-cuenta/ejecutivo-cuenta.component';
import {AsignarUrlComponent} from './asignar-url/asignar-url.component';
import {ConveniosComponent} from './convenios/convenios.component';
import {CompetenciaComponent} from './competencia/competencia.component';
import {AnalisisCompetenciaComponent} from './analisis-competencia/analisis-competencia.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {
  NbAlertModule,
  NbDatepickerModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbTooltipModule,
} from '@nebular/theme';
import {DialogModule, DropdownModule, FileUploadModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import { SociosCreateComponent } from './modals/socios-create/socios-create.component';
import { AsignarProductoComponent } from './modals/asignar-producto/asignar-producto.component';
import { AsignarCoberturaComponent } from './modals/asignar-cobertura/asignar-cobertura.component';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { ContratosComponent } from './contratos/contratos.component';
import { UrlCreateComponent } from './modals/url-create/url-create.component';
import { EjecutivoCuentaCreateComponent } from './modals/ejecutivo-cuenta-create/ejecutivo-cuenta-create.component';
import { AsignarPresupuestoComponent } from './modals/asignar-presupuesto/asignar-presupuesto.component';
import { AsignarContratosComponent } from './modals/asignar-contratos/asignar-contratos.component';
import { AsignarConveniosComponent } from './modals/asignar-convenios/asignar-convenios.component';
import { AsignarCompetenciaComponent } from './modals/asignar-competencia/asignar-competencia.component';
import { AsignarAnalisisCompetenciaComponent } from './modals/asignar-analisis-competencia/asignar-analisis-competencia.component';
import { DatosSociosComponent } from './modals/datos-socios/datos-socios.component';
import { DatosEjecutivoCuentaComponent } from './modals/datos-ejecutivo-cuenta/datos-ejecutivo-cuenta.component';
import {
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatStepperModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule, MatCardHeader, MatPaginatorIntl,
} from '@angular/material';
import { UpdateBajaSociosComponent } from './modals/update-baja-socios/update-baja-socios.component';
import { DatosConveniosComponent } from './modals/datos-convenios/datos-convenios.component';
import { DatosProductoComponent } from './modals/datos-producto/datos-producto.component';
import { DatosCompetenciaComponent } from './modals/datos-competencia/datos-competencia.component';
import { DatosAnalisiCompetenciaComponent } from './modals/datos-analisi-competencia/datos-analisi-competencia.component';
import { DatosCoberturaComponent } from './modals/datos-cobertura/datos-cobertura.component';
import { DocumentosComercialComponent } from './documentos-comercial/documentos-comercial.component';
import { RamoComponent } from './ramo/ramo.component';
import { AsignarSubramoComponent } from './modals/asignar-subramo/asignar-subramo.component';
import { DatosRamoComponent } from './modals/datos-ramo/datos-ramo.component';
import { AsignarRamoComponent } from './modals/asignar-ramo/asignar-ramo.component';
import { SubRamoComponent } from './sub-ramo/sub-ramo.component';
import { DatosSubramoComponent } from './modals/datos-subramo/datos-subramo.component';
import {SociosComponent} from './socios/socios.component';
import {SociosService} from '../../@core/data/services/comerciales/socios.service';




@NgModule({
  declarations: [
    ComercialComponent,
    SociosComponent,
    ProductoComponent,
    CoberturaComponent,
    EjecutivoCuentaComponent,
    AsignarUrlComponent,
    ConveniosComponent,
    CompetenciaComponent,
    AnalisisCompetenciaComponent,
    SociosCreateComponent,
    AsignarProductoComponent,
    AsignarCoberturaComponent,
    PresupuestoComponent,
    ContratosComponent,
    UrlCreateComponent,
    EjecutivoCuentaCreateComponent,
    AsignarPresupuestoComponent,
    AsignarContratosComponent,
    AsignarConveniosComponent,
    AsignarCompetenciaComponent,
    AsignarAnalisisCompetenciaComponent,
    DatosSociosComponent,
    DatosEjecutivoCuentaComponent,
    UpdateBajaSociosComponent,
    DatosConveniosComponent,
    DatosProductoComponent,
    DatosCompetenciaComponent,
    DatosAnalisiCompetenciaComponent,
    DatosCoberturaComponent,
    DocumentosComercialComponent,
    RamoComponent,
    AsignarSubramoComponent,
    DatosRamoComponent,
    AsignarRamoComponent,
    SubRamoComponent,
    DatosSubramoComponent,
  ],
  imports: [
    CommonModule,
    ComercialRoutingModule,
    ThemeModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatCardModule,
    MatDatepickerModule,
  ],
  entryComponents: [
    SociosCreateComponent,
    AsignarCoberturaComponent,
    AsignarUrlComponent,
    UrlCreateComponent,
    EjecutivoCuentaCreateComponent,
    AsignarPresupuestoComponent,
    AsignarContratosComponent,
    AsignarContratosComponent,
    AsignarConveniosComponent,
    AsignarProductoComponent,
    AsignarCompetenciaComponent,
    AsignarAnalisisCompetenciaComponent,
    DatosSociosComponent,
    DatosEjecutivoCuentaComponent,
    UpdateBajaSociosComponent,
    DatosConveniosComponent,
    DatosProductoComponent,
    DatosCompetenciaComponent,
    DatosCoberturaComponent,
    DatosAnalisiCompetenciaComponent,
    AsignarSubramoComponent,
    DatosRamoComponent,
    AsignarRamoComponent,
    DatosSubramoComponent,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: SociosComponent,
    },
    SociosService,
  ],
})
export class ComercialModule {
}
