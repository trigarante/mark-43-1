import {Observable} from 'rxjs';

export interface TelefonoIp {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class TelefonoIpData {
  abstract get(): Observable<TelefonoIp[]>;
  abstract post(telefonoIp): Observable<TelefonoIp>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getTelefonoIpById(idTelefonoIp): Observable<TelefonoIp>;
  abstract put(idTelefonoIp, telefonoIp): Observable<TelefonoIp>;
}
