import {Injectable} from '@angular/core';
import {
  DiscoDuroExterno,
  DiscoDuroExternoData,
} from '../../../../interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class DiscoDuroExternoService extends DiscoDuroExternoData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<DiscoDuroExterno[]> {
    return this.http.get<DiscoDuroExterno[]>(this.baseURL + 'v1/discodExterno');
  }

  post(discoDuroExterno: DiscoDuroExterno): Observable<DiscoDuroExterno> {
    return this.http.post<DiscoDuroExterno>(this.baseURL + 'v1/discodExterno', discoDuroExterno);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'discoduroexterno/saveDiscoDuroExterno', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajadiscoduroexterno/saveBajaDiscoDuroExterno', json);
  }
  getDiscoExternoById(idDiscoExterno): Observable<DiscoDuroExterno> {
    return this.http.get<DiscoDuroExterno>(this.baseURL + 'v1/discodExterno/' + idDiscoExterno);
  }

  put(idDiscoExterno, discoDuroExterno: DiscoDuroExterno): Observable<DiscoDuroExterno> {
    return this.http.put<DiscoDuroExterno>(this.baseURL + 'v1/discodExterno/' + idDiscoExterno + '/'
      + sessionStorage.getItem('Empleado'), discoDuroExterno);
  }
}
