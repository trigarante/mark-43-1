import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import {TiComponent} from './ti.component';
import {TiRoutingModule} from './ti-routing.module';
import {NbDatepickerModule, NbDialogModule} from '@nebular/theme';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import { CrearCorreoComponent } from './modals/crear-correo/crear-correo.component';
import { CrearExtensionComponent } from './modals/crear-extension/crear-extension.component';
import { AdministrativoComponent } from './administrativo/administrativo.component';
import { EjecutivoComponent } from './ejecutivo/ejecutivo.component';
import { CrearEjecutivoComponent } from './modals/crear-ejecutivo/crear-ejecutivo.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { CrearCpuComponent } from './modals/crear-cpu/crear-cpu.component';
import { CrearUsuarioComponent } from './modals/crear-usuario/crear-usuario.component';
import {DropdownModule, KeyFilterModule} from 'primeng/primeng';
import { CrearAdministrativoComponent } from './modals/crear-administrativo/crear-administrativo.component';
import { SaveComponent } from './modals/save/save.component';
import { PermisosComponent } from './permisos/permisos.component';
import { CrearPermisosComponent } from './crear-permisos/crear-permisos.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { CrearSupervisorComponent } from './modals/crear-supervisor/crear-supervisor.component';
import {TooltipModule} from 'primeng/tooltip';
import { CrearModoSeguroComponent } from './modals/crear-modo-seguro/crear-modo-seguro.component';
import { CoordinadorComponent } from './coordinador/coordinador.component';
import { CrearCoordinadorComponent } from './modals/crear-coordinador/crear-coordinador.component';
import { GerenteComponent } from './gerente/gerente.component';
import { CrearGerenteComponent } from './modals/crear-gerente/crear-gerente.component';
import { ModificarTotalLicenciasComponent } from './modals/modificar-total-licencias/modificar-total-licencias.component';
import { CrearAuditoriaComponent } from './modals/crear-auditoria/crear-auditoria.component';
import { DatosUsuariosComponent } from './modals/datos-usuarios/datos-usuarios.component';
import { ModificarPermisosComponent } from './modals/modificar-permisos/modificar-permisos.component';
import { ActualizarPermisosComponent } from './actualizar-permisos/actualizar-permisos.component';
import { ExtensionesComponent } from './extensiones/extensiones.component';
import { ModificarAliasComponent } from './modals/modificar-alias/modificar-alias.component';
import { SaveModificadoComponent } from './modals/save-modificado/save-modificado.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatOptionModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatChipsModule,
  MatDatepickerModule, MatNativeDateModule, MatButtonToggleModule, MatPaginatorIntl, MatCheckboxModule, MatExpansionModule,
} from '@angular/material';
import { ExtensionSaveComponent } from './modals/extension-save/extension-save.component';
import { ModificarExtensionComponent } from './modals/modificar-extension/modificar-extension.component';
import { EliminarPermisosComponent } from './eliminar-permisos/eliminar-permisos.component';
import { ControlLlamadasComponent } from './control-llamadas/control-llamadas.component';



@NgModule({
  declarations: [
    TiComponent,
    CrearCorreoComponent,
    CrearExtensionComponent,
    AdministrativoComponent,
    EjecutivoComponent,
    CrearEjecutivoComponent,
    UsuarioComponent,
    CrearCpuComponent,
    CrearUsuarioComponent,
    CrearAdministrativoComponent,
    SaveComponent,
    PermisosComponent,
    CrearPermisosComponent,
    SupervisorComponent,
    CrearSupervisorComponent,
    CrearModoSeguroComponent,
    CoordinadorComponent,
    CrearCoordinadorComponent,
    GerenteComponent,
    CrearGerenteComponent,
    ModificarTotalLicenciasComponent,
    CrearAuditoriaComponent,
    DatosUsuariosComponent,
    ModificarPermisosComponent,
    ActualizarPermisosComponent,
    ExtensionesComponent,
    ModificarAliasComponent,
    SaveModificadoComponent,
    ExtensionSaveComponent,
    ModificarExtensionComponent,
    EliminarPermisosComponent,
    ControlLlamadasComponent,
    ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatDialogModule,
    ThemeModule,
    CommonModule,
    Ng2SmartTableModule,
    TableModule,
    TiRoutingModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    ButtonModule,
    DropdownModule,
    TooltipModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    NgbModule,
    KeyFilterModule,
  ],
  entryComponents: [
    CrearCorreoComponent,
    CrearExtensionComponent,
    CrearEjecutivoComponent,
    CrearCpuComponent,
    CrearUsuarioComponent,
    CrearAdministrativoComponent,
    SaveComponent,
    SaveModificadoComponent,
    ExtensionSaveComponent,
    CrearSupervisorComponent,
    CrearModoSeguroComponent,
    CrearCoordinadorComponent,
    CrearGerenteComponent,
    ModificarTotalLicenciasComponent,
    CrearAuditoriaComponent,
    DatosUsuariosComponent,
    ModificarAliasComponent,
    ModificarExtensionComponent,
    PermisosComponent,
    ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    NgbModule,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: UsuarioComponent,
    },
  ],
})
export class TiModule { }
