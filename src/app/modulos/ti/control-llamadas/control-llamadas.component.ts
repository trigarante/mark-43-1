import {Component, OnInit, ViewChild} from '@angular/core';
import {Drive, DriveData} from '../../../@core/data/interfaces/ti/drive';
import {SelectionModel} from '@angular/cdk/collections';
import {ErrorStateMatcher, MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource} from '@angular/material';
import swal from 'sweetalert';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'ngx-control-llamadas',
  templateUrl: './control-llamadas.component.html',
  styleUrls: ['./control-llamadas.component.scss'],
})
export class ControlLlamadasComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('ref') ref: any;
  acd: boolean = false;
  manual: boolean = false;
  fechaInicio: Date;
  fechaFin: Date = null;
  desactivarParametros: boolean = true;
  desactivarDescargarSeleccion: boolean = true;
  activarBotonBuscar: boolean = false;
  fechaMinima: Date = new Date(1451628000000);
  fechaMinima2: Date;
  fechaMaxima: Date = new Date();
  displayedColumns: string[] = ['seleccion', 'tipo', 'fecha', 'numero'];
  selection = new SelectionModel<Drive>(true, []);
  llamadas: Drive [];
  dataSource: any;
  numeros: RegExp = /[0-9]+/;
  textoBotonBuscar: string = 'Buscar';
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  telefonoFormControl: FormControl = new FormControl( '',
                                       Validators.compose([
                                         Validators.required, Validators.maxLength(10),
                                         Validators.minLength(3),
                                       ]));

  constructor(private llamadasService: DriveData) {
    super();
    this.getAndInitTranslations();
  }

  ngOnInit() {
    this.fechaMaxima.setDate(new Date().getDate() - 1);
  }

  activarFechaFinal () {
    this.desactivarParametros = false;

    if (this.fechaFin !== undefined) {
      this.fechaFin = undefined;
    }

    this.fechaMinima2 = new Date(this.fechaInicio);
    this.fechaMinima2.setDate(this.fechaMinima2.getDate() + 1);
  }

  activaBoton(): void {
    if ((this.acd === true || this.manual === true) && this.telefonoFormControl.valid ) {
      this.activarBotonBuscar = true;
    } else {
      this.activarBotonBuscar = false;
    }
  }

  getLlamadas () {
    this.activarBotonBuscar = false;
    this.textoBotonBuscar = 'Buscando';
    this.selection.clear();
    this.desactivarDescargarSeleccion = true;
    this.llamadasService.get(this.fechaInicio, this.fechaFin, this.acd, this.manual, this.telefonoFormControl.value).subscribe( data => {
      this.llamadas = data;
    }, error => {
      swal( {
        title: 'Se ha producido un error',
        text: 'Vuelve a intentarlo más tarde',
        icon: 'error',
        dangerMode: true,
      });
      this.textoBotonBuscar = 'Buscar';
      this.activarBotonBuscar = true;
    }, () => {
      this.selection.clear();
      this.ref.checked = false;
      this.dataSource = new MatTableDataSource(this.llamadas);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (this.llamadas.length === 0) {
        swal( {
          title: 'Sin datos',
          text: 'No hay llamadas que cumplan con los criterios seleccionados',
          icon: 'info',
        });
      }
      this.activarBotonBuscar = true;
      this.textoBotonBuscar = 'Buscar';
    });
  }

  abrirLink () {
    swal({
      title: 'Por favor acepta todas las pestañas emergentes',
      text: 'Cada uno de los elementos seleccionados se abrirá en Google Drive para su descarga',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        succes: {
          text: 'Aceptar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        for (const llamada of this.selection.selected) {
          window.open(llamada.downloadLink.toString(), '_blank');
        }
      }
    });
  }

  comprobacionDatos () {
    if (this.selection.selected.length >= 1 && this.selection.selected.length <= 10) {
      this.desactivarDescargarSeleccion = false;
    } else {
      this.desactivarDescargarSeleccion = true;
      if (this.selection.selected.length !== 0) {
        swal({
          title: 'Solo puedes seleccionar 10 elementos a la vez',
          icon: 'error',
          dangerMode: true,
        });
      }
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(_ref: any) {
    if (this.dataSource === undefined) {
      this.ref.checked = false;
    } else {
      if (_ref.checked) {
        this.checkPageOnly();
        this.comprobacionDatos();
      } else {
        this.selection.clear();
        this.desactivarDescargarSeleccion = true;
      }
    }
  }

  checkPageOnly() {
    let i = this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize;
    const end = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
    for (i; i < end; i++) {
      this.selection.select(this.dataSource.data[i]);
    }
  }

  pageChange() {
    this.ref.checked = false;
    if (this.selection.selected.length > 10) {
      this.masterToggle(this.ref);
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAndInitTranslations() {
    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

}
