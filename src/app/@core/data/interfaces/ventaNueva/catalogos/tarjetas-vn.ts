import {Observable} from 'rxjs';

export interface TarjetasVn {
  id: string;
  idBanco: string;
  idCarrier: string;
  tarjeta: string;
  descripcion: string;
  activo: string;
}

export abstract class TarjetasVnData {
  abstract getTarjetaById(id): Observable<TarjetasVn>;
  abstract get(): Observable<TarjetasVn[]>;
  abstract post(tarjeta): Observable<TarjetasVn>;
  abstract put(id, tarjeta): Observable<TarjetasVn>;
}
