import {Observable} from 'rxjs';

export interface FormaPago {
  id: number;
  description: string;
  activo: number;
}

export abstract class FormaPagoData {
  abstract get(): Observable<FormaPago[]>;
  abstract post(formaPago): Observable<FormaPago>;
  abstract getFormaPagoById(idFormaPago): Observable<FormaPago>;
  abstract put(idformaPago, formaPago): Observable<FormaPago>;
}
