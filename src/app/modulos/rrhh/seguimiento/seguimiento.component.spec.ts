import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SeguimientoComponent} from './seguimiento.component';
import {
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {SeguimientoData} from '../../../@core/data/interfaces/rrhh/seguimiento';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';


describe('SeguimientoComponent', () => {
  let component: SeguimientoComponent;
  let fixture: ComponentFixture<SeguimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SeguimientoComponent],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
      ],
      providers: [
        SeguimientoData,
        EstadoRrhhData,
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
