import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {PermisosVisualizacion, PermisosVisualizacionData} from '../../interfaces/ti/catalogos/permisos-visualizacion';

@Injectable({
  providedIn: 'root',
})
export class PermisosVisualizacionService extends  PermisosVisualizacionData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<PermisosVisualizacion[]> {
    return this.http.get<PermisosVisualizacion[]>(this.baseURL + 'v1/permisos-visualizacion' );
  }

  getByID(idPermisos): Observable<PermisosVisualizacion> {
    return this.http.get<PermisosVisualizacion>(this.baseURL + 'v1/permisos-visualizacion/' + idPermisos );
  }
}
