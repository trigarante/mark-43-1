import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Grupo, GrupoData} from '../../../../@core/data/interfaces/catalogos/grupo';
import {Paises, PaisesData} from '../../../../@core/data/interfaces/catalogos/paises';
import {NbDialogRef} from '@nebular/theme';
import {SedeComponent} from '../../../rrhh/catalogos/sede/sede.component';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-create-grupo',
  templateUrl: './create-grupo.component.html',
  styleUrls: ['./create-grupo.component.scss'],
})
export class CreateGrupoComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idSede obtenido. */
  @Input() idGrupo: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una sede. */
  grupoCreateForm: FormGroup;
  /** Arreglo que se inicializa con los valores de la Sede desde el Microservicio. */
  grupo: Grupo;
  /** Arreglo que se inicializa con los valores del País desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para los países. */
  pais: any[];

  constructor(
    protected paisesService: PaisesData,
    protected grupoService: GrupoData,
    private fb: FormBuilder,
    protected ref: NbDialogRef<SedeComponent>,
  ) { }
  /** Función que obtiene los paises, filtrándolos por aquellos que estén activos y los almacena en el arreglo pais. */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  guardarGrupo() {
    if (this.grupoCreateForm.invalid) {
      return;
    }
    this.grupoService.post(this.grupoCreateForm.value).subscribe((result) => {
      this.grupoCreateForm.reset();
      this.ref.close();
    });
  }

  getGrupoById() {
    this.grupoService.getGrupoById(this.idGrupo).subscribe( result => {
      this.grupo = result;
      this.grupoCreateForm.controls['idPais'].setValue(this.grupo.idPais);
      this.grupoCreateForm.controls['nombre'].setValue(this.grupo.nombre);
      this.grupoCreateForm.controls['descripcion'].setValue(this.grupo.descripcion);
      this.grupoCreateForm.controls['codigo'].setValue(this.grupo.codigo);
    });
  }

  updateGrupo() {
    this.grupoService.put(this.idGrupo, this.grupoCreateForm.value).subscribe( result => {
      this.grupoCreateForm.reset();
      this.ref.close();
    });
  }
  ngOnInit() {
    this.getPaises();
    if (this.idTipo === 2) {
      this.getGrupoById();
    }
    this.grupoCreateForm = this.fb.group({
      'idPais': new FormControl('', Validators.compose([Validators.required])),
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
      'codigo': new FormControl('', Validators.compose([Validators.required])),
    });
  }
}
