import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Categoria, CategoriaData} from '../../../interfaces/comerciales/catalogo/categoria';
import {environment} from '../../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CategoriaService extends CategoriaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.baseURL + 'v1/categoria');
  }

  post(categoria: Categoria): Observable<Categoria> {
    return this.http.post<Categoria>(this.baseURL + 'v1/categoria', categoria);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON> (this.socketURL + 'categoria/saveCategoria', json);
  }

  getCategoriaById(idCategoria): Observable<Categoria> {
    return this.http.get<Categoria>(this.baseURL + 'v1/categoria/' + idCategoria);
  }
  put(idCategoria, categoria: Categoria): Observable<Categoria> {
    return this.http.put<Categoria>(this.baseURL + 'v1/categoria/' + idCategoria + '/'
      + sessionStorage.getItem('Empleado') , categoria);
  }
}
