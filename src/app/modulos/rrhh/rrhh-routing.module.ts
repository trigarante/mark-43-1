import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RrhhComponent} from './rrhh.component';
import {SolicitudesComponent} from './solicitudes/solicitudes.component';
import {PrecandidatosComponent} from './precandidatos/precandidatos.component';
import {CandidatosComponent} from './candidatos/candidatos.component';
import {CapacitacionComponent} from './capacitacion/capacitacion.component';
import {SeguimientoComponent} from './seguimiento/seguimiento.component';
import {PreEmpleadosComponent} from './pre-empleados/pre-empleados.component';
import {EmpleadosComponent} from './empleados/empleados.component';
import {PrecandidatoCreateComponent} from './precandidato-create/precandidato-create.component';
import {EmpleadoCreateComponent} from './empleado-create/empleado-create.component';
import {EmpleadoUpdateComponent} from './empleado-update/empleado-update.component';
import {EmpleadoBajaComponent} from './empleado-baja/empleado-baja.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';
import {EmpleadosImssComponent} from './empleados-imss/empleados-imss.component';
import {PrecandidatoUpdateComponent} from './precandidato-update/precandidato-update.component';
import {VacantesComponent} from './vacantes/vacantes.component';
import {BajasrhComponent} from './bajasrh/bajasrh.component';
import {SeguimientoPracticoComponent} from './seguimiento-practico/seguimiento-practico.component';
import {DocumentosComponent} from './documentos/documentos.component';
import {PreguntasComponent} from './preguntas/preguntas.component';
import {GenerarPreguntasComponent} from './preguntas/generar-preguntas/generar-preguntas.component';
import {GenerarRespuestasComponent} from './preguntas/generar-respuestas/generar-respuestas.component';
// import {ResultadosComponent} from './preguntas/resultados/resultados.component';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: RrhhComponent,
  children: [
    {
      path: 'vacantes',
      component: VacantesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.vacantes.activo},
    },
    {
      path: 'solicitudes',
      component: SolicitudesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.solicitudes.activo},
    },
    {
      path: 'precandidatos',
      component: PrecandidatosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.precandidatos.activo},
    },
    {
      path: 'candidatos',
      component: CandidatosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.candidato.activo},
    },
    {
      path: 'capacitacion',
      component: CapacitacionComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.teorico.activo},
    },
    {
      path: 'seguimiento',
      component: SeguimientoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.roleplay.activo},
    },
    {
      path: 'seguimiento-practico',
      component: SeguimientoPracticoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.ventaNueva.activo},
    },
    {
      path: 'bajas',
      component: BajasrhComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajas.activo},
    },
    {
      path: 'pre-empleados',
      component: PreEmpleadosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.preEmpleado.activo},
    },
    {
      path: 'pre-empleados/:idCandidato',
      component: PreEmpleadosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.preEmpleado.activo},
    },
    {
      path: 'empleados',
      component: EmpleadosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.empleados.activo},
    },
    {
      path: 'baja-empelados',
      component: EmpleadoBajaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajaEmpleados.activo},
    },
    {
      path: 'precandidatos/create/:tipoId/:idSolicitud',
      component: PrecandidatoCreateComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.precandidatos.escritura},
    },
    {
      path: 'updatePrecandidato/:idPrecandidato',
      component: PrecandidatoUpdateComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.precandidatos.escritura},
    },
    {
      path: 'empleados/create/:idCandidato/:idPrecandidato',
      component: EmpleadoCreateComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.empleados.escritura},
    },
    {
      path: 'catalogos',
      loadChildren: './catalogos/catalogos.module#CatalogosModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'updateEmpleado/:idEmpleado/:idPrecandidato/:idTipo',
      component: EmpleadoUpdateComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.empleados.escritura},
    },
    {
      path: 'empleados/altaImss',
      component: EmpleadosImssComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.altaDeIMSS.activo},
    },
    {
      path: 'documentos/:documento/:id',
      component: DocumentosComponent,
    },
    {
      path: 'preguntas',
      component: PreguntasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.preguntas.activo},
    },
    {
      path: 'generar-preguntas',
      component: GenerarPreguntasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.generarPreguntas.activo},
    },
    {
      path: 'generar-respuestas',
      component: GenerarRespuestasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.generarRespuestas.activo},
    },
    // {
    //   path: 'resultados',
    //   component: ResultadosComponent,
    //   canActivate: [AuthGuard],
    //   data: { permiso: permisos.modulos.resultados.activo},
    // },
  ],
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class RrhhRoutingModule {
}


