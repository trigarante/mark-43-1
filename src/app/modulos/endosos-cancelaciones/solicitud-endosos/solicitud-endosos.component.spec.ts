import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudEndososComponent } from './solicitud-endosos.component';

describe('SolicitudEndososComponent', () => {
  let component: SolicitudEndososComponent;
  let fixture: ComponentFixture<SolicitudEndososComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudEndososComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudEndososComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
