import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {formatDate} from '@angular/common';
import {GuardadoComponent} from '../guardado/guardado.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-agregar-tipo',
  templateUrl: './agregar-tipo.component.html',
  styleUrls: ['./agregar-tipo.component.scss'],
})
export class AgregarTipoComponent implements OnInit {
  @Input() id: number;
  agregarTipoForm: FormGroup;
  pagina: Pagina;
  aseguradoras: any;

  constructor( private fb: FormBuilder, protected ref: NbDialogRef<AgregarTipoComponent> ,
               private paginaService: PaginasData, private router: Router, public dialogService: NbDialogService ) {
    this.agregarTipoForm = this.fb.group({
      'tipo': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  guardarTipo() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      this.aseguradoras[this.agregarTipoForm.value.tipo] = {'aseguradoras': []};
      console.log(this.aseguradoras);
      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));

      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.ref.close();
        this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      }).onClose.subscribe(x => {

      });
    });
  }
  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
  }

}
