import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ReportesComponent} from './reportes.component';
import {ReportePrecandidatoComponent} from './rrhh/reporte-precandidato/reporte-precandidato.component';
import {ReporteCapacitacionComponent} from './rrhh/reporte-capacitacion/reporte-capacitacion.component';
import {ReporteCandidatoComponent} from './rrhh/reporte-candidato/reporte-candidato.component';
import {ReporteEmpleadoComponent} from './rrhh/reporte-empleado/reporte-empleado.component';
import {ReporteSeguimientoComponent} from './rrhh/reporte-seguimiento/reporte-seguimiento.component';
import {ReporteSocioComponent} from './comercial/reporte-socio/reporte-socio.component';
import {ReporteCampanaComponent} from './marketing/reporte-campana/reporte-campana.component';
import {ReporteSolicitudesComponent} from './venta-nueva/reporte-solicitudes/reporte-solicitudes.component';
import { ReporteUsuariosComponent } from './ti/reporte-usuarios/reporte-usuarios.component';
import { ReporteExtensionesComponent } from './ti/reporte-extensiones/reporte-extensiones.component';
import { ReportesGrupoPermisosComponent } from './ti/reporte-grupo-permisos/reportes-grupo-permisos.component';
import { ReporteInventarioUsuarioComponent } from './ti/reporte-inventario-usuario/reporte-inventario-usuario.component';
import { ReporteInventarioRedesComponent } from './ti/reporte-inventario-redes/reporte-inventario-redes.component';
import { ReporteInventarioSoporteComponent } from './ti/reporte-inventario-soporte/reporte-inventario-soporte.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: ReportesComponent,
  children: [
    {
      path: 'reporte-precandidato', // RH
      component: ReportePrecandidatoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reportePrecandidato.activo},
    },
    {
      path: 'reporte-candidato',
      component: ReporteCandidatoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteCandidato.activo},
    },
    {
      path: 'reporte-capacitacion',
      component: ReporteCapacitacionComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteCapacitacion.activo},
    },
    {
      path: 'reporte-seguimiento',
      component: ReporteSeguimientoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteTeorico.activo},
    },
    {
      path: 'reporte-empleado',
      component: ReporteEmpleadoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteEmpleado.activo},
    },
    {
      path: 'reporte-socios',
      component: ReporteSocioComponent,
    },
    {
      path: 'reporte-campana',
      component: ReporteCampanaComponent,
    },
    {
      path: 'reporte-solicitudes',
      component: ReporteSolicitudesComponent,
    },
    {
      path: 'reportes-grupo-permisos', // TI
      component: ReportesGrupoPermisosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteGrupoPermiso.activo},
    },
    {
      path: 'reportes-usuarios',
      component: ReporteUsuariosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteUsuarios.activo},
    },
    {
      path: 'reportes-extensiones',
      component: ReporteExtensionesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteExtensiones.activo},
    },
    {
      path: 'reportes-inventario-usuario',
      component: ReporteInventarioUsuarioComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteInventarioUsuario.activo},
    },
    {
      path: 'reportes-inventario-redes',
      component: ReporteInventarioRedesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteInventarioRedes.activo},
    },
    {
      path: 'reportes-inventario-soporte',
      component: ReporteInventarioSoporteComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reporteInventarioSoporte.activo},
    },
  ],
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ReportesRoutingModule {
}
