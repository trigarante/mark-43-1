import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {ConveniosData} from '../../../../@core/data/interfaces/comerciales/convenios';
import {map} from 'rxjs/operators';
import {SubCategoria, SubCategoriaData} from '../../../../@core/data/interfaces/comerciales/catalogo/sub-categoria';
import {Divisas, DivisasData} from '../../../../@core/data/interfaces/comerciales/catalogo/divisas';
import {TipoSubramo, TipoSubramoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';

@Component({
  selector: 'ngx-asignar-convenios',
  templateUrl: './asignar-convenios.component.html',
  styleUrls: ['./asignar-convenios.component.scss'],
})
export class AsignarConveniosComponent implements OnInit {
  @Input() idSubramo: number;
  @Input() idTipo: number;
  @Input() idConvenio: number;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  asignarConveniosForm: FormGroup;
  submitted: Boolean;
  tipoConvenios: any[];
  subCategioria: SubCategoria[];
  subCategiorias: any[];
  tipoSubramo: TipoSubramo[];
  divisa: Divisas[];
  /**Bandera que desactica ñlso botones al realizar una peticion*/
  activo: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected conveniosService: ConveniosData,
              private tipoSubramoService: TipoSubramoData, private divisasService: DivisasData,
              private subCategoriaServive: SubCategoriaData, private categoriasService: SubramoData,
              ) {
  }

  guardarConvenios() {
    this.submitted = true;
    this.activo = true;
    if (this.asignarConveniosForm.invalid) {
      return;
    }
    let cantida: any;
    cantida = this.asignarConveniosForm.controls['cantidad'].value;
    cantida = cantida.replace(/,/g, '');
    this.asignarConveniosForm.controls['cantidad'].setValue(Number(cantida));
    this.conveniosService.post(this.asignarConveniosForm.value).subscribe((result) => {
      this.conveniosService.postSocket(this.asignarConveniosForm.value).subscribe(x => {});
      this.asignarConveniosForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/convenios']);
    });
  }

  formatoNumerosConComas(objetivo) {
    /**La asignacion de comas llega hasta 100,000,000,000 para mayor cantidad, agregar maas validaciones o hacerlo dinamico*/
    let formatNumber: any = '';
    objetivo = objetivo.replace(/,/g, '');
    objetivo = objetivo.replace(/ /g, '');
    if (objetivo.length > 3 && objetivo.length < 7) {
      formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
      this.asignarConveniosForm.controls['cantidad'].setValue(formatNumber);
    } else if (objetivo.length > 6 && objetivo.length < 10) {
      formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
      this.asignarConveniosForm.controls['cantidad'].setValue(formatNumber);
    } else if (objetivo.length > 9 && objetivo.length < 13) {
      formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
        ',' + objetivo.slice(-3);
      this.asignarConveniosForm.controls['cantidad'].setValue(formatNumber);
    }else {
      this.asignarConveniosForm.controls['cantidad'].setValue(objetivo);
    }
  }

  dismiss() {
    this.ref.close();
  }

  getTipoSubramo() {
    this.tipoSubramoService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.tipoSubramo = data;
    });
  }
getConvenioById() {
  this.conveniosService.getConveniosById(this.idConvenio).subscribe(data => {
    // this.asignarConveniosForm.controls['idTipoSubramo'].setValue(data.idTipoSubRamo);
    this.formatoNumerosConComas(data.cantidad.toString());
    this.getSubCategoriaById(data.idSubCategoria);
    this.asignarConveniosForm.controls['idTipoDivisa'].setValue(data.idTipoDivisa);
    this.asignarConveniosForm.controls['idSubRamo'].setValue(data.idSubRamo);
  });
}
  getSubCategoriaById(idSubCategoria) {
    this.subCategoriaServive.getSubCategoriaById(idSubCategoria).subscribe(data => {
      this.asignarConveniosForm.controls['idSubCategoria'].setValue(data.id);
    });
  }
  getDivisa() {
    this.divisasService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.divisa = data;
    });
  }
updateConvenio() {
  this.activo = true;
  let cantida: any;
  cantida = this.asignarConveniosForm.controls['cantidad'].value;
  cantida = cantida.replace(/,/g, '');
  this.asignarConveniosForm.controls['cantidad'].setValue(Number(cantida));
  // console.log(this.idConvenio, this.asignarConveniosForm.value);
 this.conveniosService.put(this.idConvenio, this.asignarConveniosForm.value).subscribe((result) => {
   this.conveniosService.postSocket(this.asignarConveniosForm.value).subscribe(x => {});
    this.asignarConveniosForm.reset();
    this.ref.close();
  });
}
  getSubCategoria() {
    this.subCategoriaServive.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.subCategioria = data;
    });
  }
  ngOnInit() {
    this.activo = false;
    if (this.idTipo === 2) {
      this.getConvenioById();
    }
    this.getDivisa();
    this.getSubCategoria();
    this.getTipoSubramo();
    this.asignarConveniosForm = this.fb.group({
      'idSubRamo': this.idSubramo,
      'idSubCategoria': new FormControl('', Validators.required),
      // 'idTipoSubramo': new FormControl,
      'idTipoDivisa': new FormControl('', Validators.required),
      'cantidad': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'activo': 1,
    });
  }

}
