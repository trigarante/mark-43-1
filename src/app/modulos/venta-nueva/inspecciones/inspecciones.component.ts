import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {InspeccionesData} from '../../../@core/data/interfaces/ventaNueva/inspecciones';
import {ConductorHabitualData} from '../../../@core/data/interfaces/ventaNueva/conductor-habitual';
import {EstadoInspeccion, EstadoInspeccionData} from '../../../@core/data/interfaces/ventaNueva/catalogos/estado-inspeccion';
import {Paises, PaisesData} from '../../../@core/data/interfaces/catalogos/paises';
import {map} from 'rxjs/operators';
import {Sepomex, SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';
import {SolicitudesVNData} from '../../../@core/data/interfaces/ventaNueva/solicitudes';
import {MatCheckbox, MatSlideToggle} from '@angular/material';

@Component({
  selector: 'ngx-inspecciones',
  templateUrl: './inspecciones.component.html',
  styleUrls: ['./inspecciones.component.scss'],
})
export class InspeccionesComponent implements OnInit {

  inspeccionesForm: FormGroup;
  conductorHabitualForm: FormGroup;
  colonias: Sepomex[];
  coloniasDos: Sepomex[];
  paises: Paises[];
  estadosInspeccion: EstadoInspeccion[];
  urlPostVenta: any;
  @ViewChild('conductorCheck') checkin: MatSlideToggle;

  constructor(
    private fb: FormBuilder,
    private inspeccionService: InspeccionesData,
    private ConductorHabitualService: ConductorHabitualData,
    private estadoInspeccionService: EstadoInspeccionData,
    private paisesService: PaisesData,
    private sepomexService: SepomexData,
    private solicitudesService: SolicitudesVNData,
    ) { }

  ngOnInit() {
    this.solicitudesService.returnentryURLType().subscribe(vall => {
      this.urlPostVenta = vall;
    });
    this.inspeccionesForm = this.fb.group({
      'idRegistro': new FormControl(''),
      'idEstadoInspeccion': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'idColonia': new FormControl('', Validators.required),
      'fechaInspeccion': new FormControl('', Validators.required),
      'kilometraje': new FormControl('', Validators.required),
      'placas': new FormControl('', Validators.required),
      'urlPreventa': new FormControl(''),
      'urlPostventa': new FormControl(''),
    });
    this.conductorHabitualForm = this.fb.group({
      'idPais': new FormControl('', Validators.required),
      'idRegistro': new FormControl(10),
      'nombre': new FormControl('', Validators.required),
      'paterno': new FormControl('', Validators.required),
      'materno': new FormControl('', Validators.required),
      'correo': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'idColonia': new FormControl('', Validators.required),
      'calle': new FormControl('', Validators.required),
      'numInt': new FormControl('', Validators.required),
      'numExt': new FormControl('', Validators.required),
      'genero': new FormControl('', Validators.required),
      'telefonoFijo': new FormControl('', Validators.required),
      'telefonoMovil': new FormControl('', Validators.required),
      'fechaNacimiento': new FormControl('', Validators.required),
      'fechaRegistro': new FormControl('', Validators.required),
    });
    this.getEstadoInspeccion();
    this.getPaises();
  }

  getEstadoInspeccion () {
    this.estadoInspeccionService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(
      dato => { this.estadosInspeccion = dato; },
    );
  }

  getPaises () {
    this.paisesService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(
      dato => { this.paises = dato; },
    );
  }

  validarCp(value, control) {
    const cpParse = parseInt(value);
    this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
      if (res.length !== 0) {
        if (control === 1) {
          this.inspeccionesForm.controls['cp'].setErrors(null);
          this.colonias = res;
        } else {
          this.conductorHabitualForm.controls['cp'].setErrors(null);
          this.coloniasDos = res;
        }
      } else {
        if (control === 1) {
          this.inspeccionesForm.controls['cp'].setErrors({'incorrect': true});
        } else {
          this.conductorHabitualForm.controls['cp'].setErrors({'incorrect': true});
        }
      }
    }, error => error);
  }

  guardarInspeccion () {
    this.inspeccionesForm.controls['idRegistro'].setValue(8);
    this.inspeccionService.post(this.inspeccionesForm.value).subscribe(
      result => {},
    );
  }

  guardarConductor () {
    this.ConductorHabitualService.post(this.conductorHabitualForm.value).subscribe(
      result => {},
    );
  }

}
