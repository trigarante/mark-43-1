import {Component, OnInit, ViewChild} from '@angular/core';
import {Puesto, PuestoData} from '../../../../@core/data/interfaces/catalogos/puesto';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {PuestoCreateComponent} from '../modals/puesto-create/puesto-create.component';
import {map} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-puesto',
  templateUrl: './puesto.component.html',
  styleUrls: ['./puesto.component.scss'],
})
export class PuestoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  puesto: Puesto[];
  /** Arreglo para el puesto que se dará de baja. */
  puest: Puesto;
  dataSource: any;
  permisos: any;
  escritura: boolean = false;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * En este caso: Servicio de Puesto.
   * @param {PuestoData} puestoService Servicio del componente puesto para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes (diálogos). */
  constructor(private puestoService: PuestoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'puesto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelPuesto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getpuesto(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene los puestos y lo asigna al arreglo puesto. */
  getpuesto(mensajeAct) {
    this.puestoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.puesto = [];
      this.puesto = data;
      this.dataSource = new MatTableDataSource(this.puesto); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Función que obtiene la información del puesto dependiendo de su ID. */
  getPuestoById(idPuesto) {
    this.puestoService.getById(idPuesto).subscribe(data => {
      this.puest = data;
    });
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Puesto`);
  }
  /** Función que despliega un modal del componente "PuestoCreateComponent" para poder "crear un puesto". */
  crearPuesto() {
    this.dialogoService.open(PuestoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getpuesto(0);
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de un puesto. */
  updatePuesto(idPuesto) {
    this.dialogoService.open(PuestoCreateComponent,  {
      context: {
        idTipo: 2,
        idPuesto: idPuesto,
      },
    }).onClose.subscribe(x => {
      this.getpuesto(1);
    });
  }

  bajaPuesto(idPuesto) {
    this.getPuestoById(idPuesto);
    swal({
      title: '¿Deseas dar de baja este puesto?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.puest['activo'] = 0;
        this.puestoService.put(idPuesto, this.puest).subscribe( result => {
          this.puestoService.postSocket({idPuesto: idPuesto, puest: this.puest}).subscribe(x => {});
        });
        swal('¡El puesto se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getpuesto(0);
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.puesto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.puesto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nombre', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getpuesto(0);
  }
}
