import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {Router} from '@angular/router';
import {
  TipoCobertura,
  TipoCoberturaData,
} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-cobertura';

@Component({
  selector: 'ngx-tipo-cobertura-create',
  templateUrl: './tipo-cobertura-create.component.html',
  styleUrls: ['./tipo-cobertura-create.component.scss'],
})
export class TipoCoberturaCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTipoCobertura: number;
  tipoCoberturaCreateForm: FormGroup;
  submitted: Boolean;
  tipoCobertura: TipoCobertura;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoCobertitaService: TipoCoberturaData) {
  }

  updateTipoCobertura() {
    this.tipoCobertitaService.put(this.idTipoCobertura, this.tipoCoberturaCreateForm.value).subscribe(result => {
      this.tipoCobertitaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoCoberturaCreateForm.reset();
      this.ref.close();
    });
  }

  guardarTipoCobertura() {
    this.submitted = true;
    if (this.tipoCoberturaCreateForm.invalid) {
      return;
    }
    this.tipoCobertitaService.post(this.tipoCoberturaCreateForm.value).subscribe((result) => {
      this.tipoCobertitaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoCoberturaCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  getTipoCoberturaById() {
    this.tipoCobertitaService.getTipoCoberturaById(this.idTipoCobertura).subscribe(data => {
      this.tipoCobertura = data;
      this.tipoCoberturaCreateForm.controls['cobertura'].setValue(this.tipoCobertura.cobertura);
    });

  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTipoCoberturaById();
    }
    this.tipoCoberturaCreateForm = this.fb.group({
      'cobertura': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }

}
