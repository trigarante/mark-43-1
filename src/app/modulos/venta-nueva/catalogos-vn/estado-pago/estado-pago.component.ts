import {Component, OnInit, ViewChild} from '@angular/core';
import {EstadoPago} from '../../../../@core/data/interfaces/ventaNueva/catalogos/estado-pago';
import {EstadoPagoService} from '../../../../@core/data/services/ventaNueva/catalogos/estado-pago.service';
import {
  MatDialog,
  MatDrawer,
  MatPaginator,
  MatSidenav,
  MatSlideToggle,
  MatSort,
  MatTableDataSource,
} from '@angular/material';
import {NbDialogService} from '@nebular/theme';
import {EstadoPagoCreateComponent} from '../modals/estado-pago-create/estado-pago-create.component';
import swal from 'sweetalert';

@Component({
  selector: 'ngx-estado-pago',
  templateUrl: './estado-pago.component.html',
  styleUrls: ['./estado-pago.component.scss'],
})
export class EstadoPagoComponent implements OnInit {

  checkActive: boolean;
  cols: any[];
  estadoPagoData: EstadoPago[] = [];
  estadoPagoId: EstadoPago;
  dataSource: any;

  @ViewChild(MatDrawer) menu: MatDrawer;
  @ViewChild(MatSlideToggle) toggle: MatSlideToggle;
  activo: any;
  id: any;
  description: any;

  constructor(private estadoPagoService: EstadoPagoService,
              public dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getestadoPagos();
    this.cols = ['estado'];
  }

  getestadoPagos() {
    this.estadoPagoService.get().subscribe(res => {
      this.estadoPagoData = res;
      this.dataSource = new MatTableDataSource(this.estadoPagoData);
      if (this.dataSource.activo === 1) {
        this.toggle.checked = true;
      } else {
        this.toggle.checked = false;
      }
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  getEstadoPagoById(idEstadoPago) {
    this.estadoPagoService.getEstadoPagoById(idEstadoPago).subscribe(res => {
      this.estadoPagoId = res;
    });
  }

  crearEstadoPago() {
      this.dialog.open(EstadoPagoCreateComponent, {
        data: {
          idTipo: 1,
        },
      }).afterClosed().subscribe( v => {
        this.getestadoPagos();
      });
  }

  checkIn(activo, id) {
    if (activo === 1) {
      this.bajaEstadoPago(id);
    } else {
      this.reactivarEstadoPago(id);
    }

  }

  updateEstadoPago(idEstadoPago) {
    this.dialog.open(EstadoPagoCreateComponent, {
      data: {
        idTipo: 2,
        idEstadoPago: idEstadoPago,
      },
    }).afterClosed().subscribe( v => {
      this.menu.close();
      this.getestadoPagos();
    });
  }

  bajaEstadoPago(idEstado) {
    this.getEstadoPagoById(idEstado);
      swal({
        title: '¿Deseas desactivar este estado pago?',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          confirm: {
            text: 'DESACTIVAR',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.estadoPagoId['activo'] = 0;
          this.estadoPagoService.put(idEstado, this.estadoPagoId).subscribe(result => {
          });
          swal('El estado pago se ha desactivado exitosamente', {
            icon: 'success',
          }).then((resultado => {
            this.menu.close();
            this.getestadoPagos();
          }));
        } else {
          this.menu.close();
          this.getestadoPagos();
        }
      });
    }

  reactivarEstadoPago(idEstado) {
    this.getEstadoPagoById(idEstado);
    swal({
      title: '¿Deseas activar este estado pago?',
      icon: 'warning',
      dangerMode: false,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'ACTIVAR',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.estadoPagoId['activo'] = 1;
        this.estadoPagoService.put(idEstado, this.estadoPagoId).subscribe(result => {
        });
        swal('¡El estado pago se ha activado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.menu.close();
          this.getestadoPagos();
        }));
      } else {
        this.getestadoPagos();
      }
    });
  }

  openMenu(activo, id, desc) {
    if (activo === 1) {
      this.toggle.checked = true;
    } else if (activo !== 1) {
      this.toggle.checked = false;
    }
    this.activo = activo;
    this.description = desc;
    this.id = id;
    this.menu.open();
  }


}
