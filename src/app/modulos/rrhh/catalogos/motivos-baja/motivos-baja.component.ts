import {Component, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {EstadoRrhh, EstadoRrhhData} from '../../../../@core/data/interfaces/catalogos/estado-rrhh';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MotivoBajaCreateComponent} from '../modals/motivo-baja-create/motivo-baja-create.component';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@ Component({
  selector: 'ngx-motivos-baja',
  templateUrl: './motivos-baja.component.html',
  styleUrls: ['./motivos-baja.component.scss'],
})
export class MotivosBajaComponent implements OnInit {
  dataSource: any;
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  motivosBaja: EstadoRrhh[];
  /** Arreglo para el motivo que se dará de baja. */
  motivos: EstadoRrhh;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {EstadoRrhhData} motivosService Servicio del componente EstadoRrhh para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio para diálogos (mensajes). */
  constructor(private motivosService: EstadoRrhhData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'motivosbaja');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelMotivosBaja', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getMotivos(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ ViewChild(MatPaginator) paginator: MatPaginator; //
  @ ViewChild(MatSort) sort: MatSort; //
  /** Función que despliega un modal para agregar un nuevo motivo de baja. */
  crearMotivoBaja() {
    this.dialogoService.open(MotivoBajaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getMotivos(0);
    });
  }

  /** Función que despliega un modal para actualizar un motivo de baja. */
  updateMotivo(idMotivo) {
    this.dialogoService.open(MotivoBajaCreateComponent, {
      context: {
        idTipo: 2,
        idMotivo: idMotivo,
      },
    }).onClose.subscribe(x => {
      this.getMotivos(1);
    });
  }

  /** Función que despliega un modal para dar de baja un motivo. */
  bajaMotivo(idMotivo) {
    this.getMotivoById(idMotivo);
    swal({
      title: '¿Deseas dar de baja este motivo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.motivos['activo'] = 0;
        this.motivosService.updateEstadoRrhh(idMotivo, this.motivos).subscribe( result => {
          this.motivosService.postSocket({idMotivo: idMotivo, motivos: this.motivos}).subscribe(x => {});
        });
        swal('¡El motivo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getMotivos(0);
        }));
      }
    });
  }

  /** Función que obtiene los motivos de baja y los asigna al arreglo motivos. */
  getMotivos(mensajeAct) {
    this.motivosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.motivosBaja = [];
      this.motivosBaja = data;
      this.dataSource = new MatTableDataSource(this.motivosBaja); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Motivos-Baja`);
  }
  /** Funcón que obtiene la información de un motivo de baja según su ID. */
  getMotivoById(idMotivo) {
    this.motivosService.getEstadoRrhhById(idMotivo).subscribe(data => {
      this.motivos = data;
    });
  }

  /** Función que inicializa el componente, mandando a llamar a getMotivos para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.cols = ['etapa', 'motivo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getMotivos(0);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.motivosDeBaja.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.medioDeTransporte.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
