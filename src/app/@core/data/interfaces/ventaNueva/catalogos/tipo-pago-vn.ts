import {Observable} from 'rxjs';

export interface TipoPagoVn {
  id: number;
  cantidadPagos: number;
  tipoPago: string;
  activo: number;
}

export abstract class TipoPagoData {
  abstract get(): Observable<TipoPagoVn[]>;

  abstract post(tipoPago): Observable<TipoPagoVn>;

  abstract getTipoPagoById(idTipoPago): Observable<TipoPagoVn>;

  abstract put(idTipoPago, tipoPago): Observable<TipoPagoVn>;

  abstract postSocket(json): Observable<JSON>;
}
