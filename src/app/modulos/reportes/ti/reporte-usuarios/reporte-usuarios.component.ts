import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {map} from 'rxjs/operators';
import { UsuarioData, Usuario } from '../../../../@core/data/interfaces/ti/usuario';
import * as d3Select from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';

@Component({
  selector: 'ngx-reporte-usuarios',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reporte-usuarios.component.html',
  styleUrls: ['./reporte-usuarios.component.scss'],
})

export class ReporteUsuariosComponent implements OnInit {
  private margin = {top: 20, right: 20, bottom: 20, left: 20};
  private width: number;
  private height: number;
  rangeDates: Date[];
  cols: any[];
  etapas: any[];
  usuario: Usuario[];
  dataSource: any;
  contadorUsuarios: number;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  private svg: any;
  private svg2: any;
  private Xscale: any;
  private Yscale: any;
  private xAxis: any;
  private yAxis: any;
  private YAX: any;
  private back: any;
  private label: any;
  private root: any;
  private barr: any;
  private glove: any;
  private datosBackUp: any;
  private arc: any;
  private datosMostrar: any;
  private lastColor: any;
  private width0: any;
  private height0: number;
  private colors: string[];

  //
  constructor(
    private usuarioService: UsuarioData,
  ) {
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 510 - this.margin.top - this.margin.bottom;
    this.colors = ['#15aa96', '#c83660', '#714288']
    this.width0 = 180 - this.margin.left - this.margin.right;
    this.height0 = 510 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this.drawSVG();

    this.setUsuarios();
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
    this.cols = [
      'grupoUsuarios',
      'descripcionGrupo',
      'nombre',
    ];
  }

  setUsuarios() {
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.id !== 1 );
    })).subscribe(data => {
      this.usuario = data;
      this.contadorUsuarios = this.usuario.length;
      this.dataSource = new MatTableDataSource(this.usuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.drawdash(data);

    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawSVG() {
    this.svg = d3Select.select('#graph-dash1').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width0} ${this.height0}`)
      .attr('id', 'svg1');

    const Gcircle = this.svg.append('circle')
      .attr('cx', '50%')
      .attr('cy', '18%')
      .attr('r', '13%')
      .style('fill',  this.colors[0])
      .attr('stroke', this.colors[0]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.admin());
    const start1 = this.svg.append('rect').attr('x', '50%').attr('y', '1%').attr('height', '4.5%').attr('width', 3).attr('fill', this.colors[0]);
    const percent1 = this.svg.append('text').attr('x', '55%').attr('y', '5%').attr('font-size', 30).text('%').attr('fill', this.colors[0]);
    const text1 = this.svg.append('text').attr('x', '1%').attr('y', '5%').attr('font-size', 8).text('ADMINISTRATIVO').attr('fill', this.colors[0]);

    const Gcircle2 = this.svg.append('circle')
      .attr('cx', '50%')
      .attr('cy', '50%')
      .attr('r', '13%')
      .style('fill', this.colors[1])
      .attr('stroke', this.colors[1]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.ejec());
    const start2 = this.svg.append('rect').attr('x', '50%').attr('y', '33%').attr('height', '4.5%').attr('width', 3).attr('fill', this.colors[1]);
    const percent2 = this.svg.append('text').attr('x', '55%').attr('y', '37%').attr('font-size', 30).text('%').attr('fill', this.colors[1]);
    const text2 = this.svg.append('text').attr('x', '1%').attr('y', '36%').attr('font-size', 11).text('EJECUTIVO').attr('fill', this.colors[1]);

    const Gcircle3 = this.svg.append('circle')
      .attr('cx', '50%')
      .attr('cy', '82%')
      .attr('r', '13%')
      .style('fill', this.colors[2])
      .attr('stroke', this.colors[2]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => console.log('p2s'))
      .attr('cursor', 'pointer')
      .on('click', () => this.asist());
    const start3 = this.svg.append('rect').attr('x', '50%').attr('y', '64.5%').attr('height', '4.5%').attr('width', 3).attr('fill', this.colors[2]);
    const percent3 = this.svg.append('text').attr('x', '55%').attr('y', '69%').attr('font-size', 30).text('%').attr('fill', this.colors[2]);
    const text3 = this.svg.append('text').attr('x', '2%').attr('y', '69%').attr('font-size', 11).text('ASISTENCIA').attr('fill', this.colors[2]);

    this.arc = d3Shape.arc()
      .innerRadius(58)
      .outerRadius(61)
      .startAngle(0);

  }

  drawSVG2(data, allData, originalData) {
    const ancho = 500;
    this.lastColor = '#ffbb25';
    this.root = data;
    this.label = [];
    for (let j = 1; j <= this.root.length; j++) {
      this.label.push(j); }

    this.svg2 = d3Select.select('#graph-dash11').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.height} ${this.width}`)
      .attr('transform', 'rotate(-90 0 0)')
      .attr('id', 'svg2');

    this.back = this.svg2
      .append('rect')
      .attr('class', 'fondo')
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', '100%')
      .attr('width', '100%')
      .style('opacity', '0')
      .on('click', () => this.returnOrigin(originalData));

    this.Xscale = d3Scale.scaleLinear()
      .domain([d3Array.max(this.root), 0])
      .range([this.height - 50, 0]);
    this.Yscale = d3Scale.scaleBand()
      .domain(this.label)
      .range([20, this.height + 10]);
    this.xAxis = d3Axis.axisLeft().scale(this.Xscale);
    this.yAxis = d3Axis.axisBottom().scale(this.Yscale);
    this.YAX = this.svg2.append('g')
      .attr('class', 'axis')
      .attr('transform', 'rotate(90 20 0)')
      .call(this.yAxis);

    this.barr = this.svg2.selectAll('rect .barr')
      .data(this.root)
      .enter()
      .append('rect')
      .attr('y', (d, i) => (i * (this.height - 10) / this.root.length ) + 3 )
      .attr('x', 21)
      .attr('height', '4.5%')
      .attr('width', 0) // (d) => this.Xscale(d) )
      .attr('fill', '#ffbb25')
      .attr('rx', '5')
      .attr('cursor', 'pointer')
      .on('mouseover', this.handleMouseOver)
      .on('mousemove',   function (d, i) {
        this.glove
          .html('Grupo: ' + allData[i][0].grupoUsuarios + '<br>' + 'Cantidad: ' + d)
          .style('bottom', ( d3Select.mouse(this)[0] + 5)  + 'px')
          .style('left', (d3Select.mouse(this)[1] ) + 'px');
      } )
      .on('mouseout', this.handleMouseOut)
      .on('click', (d, i) => this.table_barr(d, i, originalData));


    this.barr.transition()
      .duration(1000)
      .delay((d, i) => i * 60)
      .attr('width', (d) => this.Xscale(d) + 7)
      .transition()
      .duration(200)
      .attr('width', (d) => this.Xscale(d) - 6)
      .transition()
      .duration(150)
      .attr('width', (d) => this.Xscale(d) + 4)
      .transition()
      .duration(100)
      .attr('width', (d) => this.Xscale(d));
  }

  drawdash(d) {
    this.datosMostrar = this.dataCompose(d);
    const dataConv = this.dataFilter(d);
    this.drawSVG2(dataConv[0], dataConv[1], d);

    const ringAdmin = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', '#15aa96')
      .attr('transform', 'translate(70,85)')
      .attr('d', this.arc);
    const percAdmin = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    var percAdministrativo = this.svg.append('text').attr('x', 50).attr('y', 95).attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.admin());

    const ringEjec = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', '#c83660')
      .attr('transform', 'translate(70,235)')
      .attr('d', this.arc);
    const percEjec = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percEjecutivo = this.svg.append('text').attr('x', 52).attr('y', 248).attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.ejec());

    const ringAsist = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[3].length})
      .attr('fill', '#714288')
      .attr('transform', 'translate(70,383)')
      .attr('d', this.arc);
    const percAsist = Math.trunc((this.datosMostrar[3].length / this.datosMostrar[0]) * 100 );
    var percAsistencia = this.svg.append('text').attr('x', 52).attr('y', 400).attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.asist());

    ringAdmin.transition()
      .duration(5000)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)
      ));
    percAdministrativo.transition()
      .duration(5000)
      .attrTween('text', () => (t) => percAdministrativo.text(Math.trunc(d3Inter.interpolate(0, percAdmin)(t))) );

    ringEjec.transition()
      .duration(5000)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percEjecutivo.transition()
      .duration(5000)
      .attrTween('text', () => (t) => percEjecutivo.text(Math.trunc(d3Inter.interpolate(0, percEjec)(t))) );

    ringAsist.transition()
      .duration(5000)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percAsistencia.transition()
      .duration(5000)
      .attrTween('text', () => (t) => percAsistencia.text(Math.trunc(d3Inter.interpolate(0, percAsist)(t))) );

  }

  dataFilter(data) {
    const grupos = [];
    const grupo = [];
    const grupo1 = [];
    const grupo2 = [];
    const grupo3 = [];
    const grupo4 = [];
    const grupo5 = [];
    const grupo6 = [];
    const grupo7 = [];
    const grupo8 = [];
    const grupo9 = [];
    const grupo10 = [];
    const grupo11 = [];
    const grupo12 = [];
    const grupo13 = [];
    const grupo14 = [];
    const grupo15 = [];
    const grupo16 = [];
    const grupo17 = [];
    const grupo18 = [];
    const grupo19 = [];
    const grupo20 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idGrupo === 1) {
        grupo1.push(data[i]);
      } else if (data[i].idGrupo === 2) {
        grupo2.push(data[i]);
      } else if (data[i].idGrupo === 3) {
        grupo3.push(data[i]);
      } else if (data[i].idGrupo === 4) {
        grupo4.push(data[i]);
      } else if (data[i].idGrupo === 5) {
        grupo5.push(data[i]);
      } else if (data[i].idGrupo === 6) {
        grupo6.push(data[i]);
      } else if (data[i].idGrupo === 7) {
        grupo7.push(data[i]);
      } else if (data[i].idGrupo === 8) {
        grupo8.push(data[i]);
      } else if (data[i].idGrupo === 9) {
        grupo9.push(data[i]);
      } else if (data[i].idGrupo === 10) {
        grupo10.push(data[i]);
      } else if (data[i].idGrupo === 11) {
        grupo11.push(data[i]);
      } else if (data[i].idGrupo === 12) {
        grupo12.push(data[i]);
      } else if (data[i].idGrupo === 13) {
        grupo13.push(data[i]);
      } else if (data[i].idGrupo === 14) {
        grupo14.push(data[i]);
      } else if (data[i].idGrupo === 15) {
        grupo15.push(data[i]);
      } else if (data[i].idGrupo === 16) {
        grupo16.push(data[i]);
      } else if (data[i].idGrupo === 17) {
        grupo17.push(data[i]);
      } else if (data[i].idGrupo === 18) {
        grupo18.push(data[i]);
      } else if (data[i].idGrupo === 19) {
        grupo19.push(data[i]);
      } else if (data[i].idGrupo === 20) {
        grupo20.push(data[i]);
      }
    }
    grupos.push(grupo1.length);
    grupos.push(grupo2.length);
    grupos.push(grupo3.length);
    grupos.push(grupo4.length);
    grupos.push(grupo5.length);
    grupos.push(grupo6.length);
    grupos.push(grupo7.length);
    grupos.push(grupo8.length);
    grupos.push(grupo9.length);
    grupos.push(grupo10.length);
    grupos.push(grupo11.length);
    grupos.push(grupo12.length);
    grupos.push(grupo13.length);
    grupos.push(grupo14.length);
    grupos.push(grupo15.length);
    grupos.push(grupo16.length);
    grupos.push(grupo17.length);
    grupos.push(grupo18.length);
    grupos.push(grupo19.length);
    grupos.push(grupo20.length);
    grupo.push(grupo1);
    grupo.push(grupo2);
    grupo.push(grupo3);
    grupo.push(grupo4);
    grupo.push(grupo5);
    grupo.push(grupo6);
    grupo.push(grupo7);
    grupo.push(grupo8);
    grupo.push(grupo9);
    grupo.push(grupo10);
    grupo.push(grupo11);
    grupo.push(grupo12);
    grupo.push(grupo13);
    grupo.push(grupo14);
    grupo.push(grupo15);
    grupo.push(grupo16);
    grupo.push(grupo17);
    grupo.push(grupo18);
    grupo.push(grupo19);
    grupo.push(grupo20);

    return [grupos, grupo];
  }

  returnOrigin(d) {
    this.dataSource = new MatTableDataSource(d);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorUsuarios = d.length;
  }

  table_barr (x, i, data) {

    this.datosBackUp = data;
    const grupoUsuarioFilter = [];
    for ( let k = 0; k < data.length; k++) {
      if ( data[k].idGrupo === i + 1 ) {
        grupoUsuarioFilter.push(data[k]);
      }
    }
    this.dataSource = new MatTableDataSource(grupoUsuarioFilter);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorUsuarios = grupoUsuarioFilter.length;
  }

  dataCompose(d) {
    const tamaño = d.length;
    const administrativo = [];
    const ejecutvio = [];
    const asistencia = [];

    for (let i = 0; i < tamaño; i++) {
      if (d[i].idTipo === 1) {
        administrativo.push(d[i]);
      } else if (d[i].idTipo === 2) {
        ejecutvio.push(d[i]);
      } else if (d[i].idTipo === 3) {
        asistencia.push(d[i]);
      }
    }
    return [tamaño, administrativo, ejecutvio, asistencia];
  }

  admin() {
    const data = this.datosMostrar[1];
    this.svg2.remove();
    this.contadorUsuarios = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola1 = this.dataFilter(data);
    this.drawSVG2(hola1[0], hola1[1], data);
    this.barr.attr('fill', '#15aa96');
    this.lastColor = 1;
  }
  ejec() {
    const data = this.datosMostrar[2];
    this.svg2.remove();
    this.contadorUsuarios = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola1 = this.dataFilter(data);
    this.drawSVG2(hola1[0], hola1[1], data);
    this.barr.attr('fill', '#c83660');
    this.lastColor = 2;
  }
  asist() {
    const data = this.datosMostrar[3];
    this.svg2.remove();
    this.contadorUsuarios = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola1 = this.dataFilter(data);
    this.drawSVG2(hola1[0], hola1[1], data);
    this.barr.attr('fill', '#714288');
    this.lastColor = 3;
  }

  handleMouseOver(d, i) {
    // d3Select.select(this)
    //     //     //   .transition()
    //     //     //   .duration(200)
    //     //     //   .attr('fill', '#0fab7a');

    this.glove = d3Select.select('#graph-dash11')
      .append('div')
      .attr('class', 'tooltip')
      .attr('id', 'toolTip')
      .style('opacity', 0.8);
  }

  handleMouseOut() {
    document.getElementById('toolTip').remove();
  }

  
}
