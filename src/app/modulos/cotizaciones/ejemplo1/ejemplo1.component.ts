import {Component, OnInit} from '@angular/core';
import {Auto, Catalogo, CatalogoData} from '../../../@core/data/interfaces/cotizador/catalogo';
import {CotizarData, CotizarRequest, CotizarResponse} from '../../../@core/data/interfaces/cotizador/cotizar';
import {Pagina, PaginasData} from '../../../@core/data/interfaces/marketing/pagina';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'ngx-ejemplo1',
  templateUrl: './ejemplo1.component.html',
  styleUrls: ['./ejemplo1.component.scss'],
})
export class Ejemplo1Component implements OnInit {

  autoSelected: Auto = new Auto();
  marcas: Catalogo[];
  modelos: Catalogo[];
  descripciones: Catalogo[];
  subdescripciones: Catalogo[];
  detalles: any[];
  cotizacionRequest = null;
  cotizacionResponse = [];
  pagina: Pagina;
  cp = new FormControl('');
  config: Object[];
  configMotos: Object[];
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  descuentos = Array.from(Array(20).keys(), n => n + 1);
  aseguradoras: Object[] = [{value: '', aseguradora: 'Todas'}];
  aseguradorasMotos: Object[] = [{value: '', aseguradora: 'Todas'}];


  constructor(private catalogoService: CatalogoData, private cotizarService: CotizarData, private paginaService: PaginasData) {
    this.paginaService.getPaginaById(5).subscribe(z => {
      this.pagina = z;
      if (this.pagina) {
        /**
         * Parsea configuracion de autos
         */
        try {
          this.config = JSON.parse(this.pagina.configuracion)['autos']['aseguradoras'];
          this.config.forEach(c => {
            this.aseguradoras.push({value: c['aseguradora'], aseguradora: c['aseguradora']});
          });
        } catch (e) {
          console.error(`Error al parsear JSON ${e}`);
        }

        /**
         * Parsea configuracion de motos
         */
        try {
          this.configMotos = JSON.parse(this.pagina.configuracion)['motos']['aseguradoras'];
          this.configMotos.forEach(c => {
            this.aseguradorasMotos.push({value: c['aseguradora'], aseguradora: c['aseguradora']});
          });
        } catch (e) {
          console.error(`Error al parsear JSON ${e}`);
        }

      }
    });
    this.getMarcas();
  }

  getMarcas() {
    this.autoSelected.marca = new Catalogo();
    this.autoSelected.modelo = new Catalogo();
    this.autoSelected.descripcion = new Catalogo();
    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.cotizacionResponse = null;
    this.catalogoService.getMarcas(this.autoSelected.aseguradora).subscribe(m => {
      this.marcas = m;
    });
  }

  getModelos() {

    this.autoSelected.descripcion = new Catalogo();
    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getModelos(this.autoSelected.aseguradora, this.autoSelected.marca.text).subscribe(m => {
      this.modelos = m;
    });
  }

  getDescripcion() {

    this.autoSelected.subdescripcion = new Catalogo();
    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getDescripcion(this.autoSelected.aseguradora, this.autoSelected.marca.text, this.autoSelected.modelo.text)
      .subscribe(m => {
        this.descripciones = m;
      });
  }

  getSubdescripcion() {

    this.autoSelected.detalle = new Catalogo();
    this.catalogoService.getSubdescripcion(this.autoSelected.aseguradora, this.autoSelected.marca.text, this.autoSelected.modelo.text
      , this.autoSelected.descripcion.text).subscribe(m => {
      this.subdescripciones = m;
    });
  }

  /**
   Metodo para distinguir entre que detalles tiene que traer homologado o por aseguradora
   */
  getDetalles() {
    if (this.autoSelected.aseguradora === '') {
      this.catalogoService.getDetallesHomologado(this.autoSelected.marca.text, this.autoSelected.modelo.text,
        this.autoSelected.descripcion.text, this.autoSelected.subdescripcion.text)
        .subscribe(d => {
          this.detalles = d;
        });
    } else {
      this.catalogoService.getDetalles(this.autoSelected.aseguradora, this.autoSelected.marca.text, this.autoSelected.modelo.text
        , this.autoSelected.descripcion.text, this.autoSelected.subdescripcion.text).subscribe(m => {
        this.detalles = m;
      });
    }

  }


  cotizar() {
    const aseguradorasConPermiso = [];
    this.cotizacionResponse = [];
    if (this.autoSelected.aseguradora === '') {
      this.detalles.forEach(detalle => {
        const aseguradora = this.config.filter(c => {
          if (c['aseguradora'] === detalle['aseguradora'] && c['cotizacion']) {
            aseguradorasConPermiso.push(detalle);
            return true;
          }
        });
      });
      aseguradorasConPermiso.forEach(a => {
        this.cotizacionRequest = new CotizarRequest(a['aseguradora'], a['id'], this.cp.value,
          this.autoSelected.descripcion.text, 0, this.autoSelected.edad,
          '01/01/' + (new Date().getFullYear() - Number(this.autoSelected.edad)),
          this.autoSelected.genero, this.autoSelected.marca.text,
          this.autoSelected.modelo.text, 'cotizacion', 'AMPLIA', 'PARTICULAR');
        this.cotizarService.cotizar(this.cotizacionRequest).subscribe(z => {
          this.cotizacionResponse.push(z);
        });
      });
    } else {
      this.cotizacionRequest = new CotizarRequest(this.autoSelected.aseguradora, this.detalles[0]['id'], this.cp.value,
        this.autoSelected.descripcion.text, Number(this.autoSelected.descuento), this.autoSelected.edad,
        '01/01/' + (new Date().getFullYear() - Number(this.autoSelected.edad)),
        this.autoSelected.genero, this.autoSelected.marca.text,
        this.autoSelected.modelo.text, 'cotizacion', 'AMPLIA', 'PARTICULAR');
      this.cotizarService.cotizar(this.cotizacionRequest).subscribe(z => {
        this.cotizacionResponse.push(z);
      });
    }

  }

  ngOnInit() {


  }
}
