import {Component, OnInit, ViewChild} from '@angular/core';
import {RazonSocial, RazonSociosData} from '../../../../@core/data/interfaces/catalogos/razon-social';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {RazonSocialCreateComponent} from '../modals/razon-social-create/razon-social-create.component';
import {map} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import swal from 'sweetalert';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-razon-social',
  templateUrl: './razon-social.component.html',
  styleUrls: ['./razon-social.component.scss'],
})
export class RazonSocialComponent implements OnInit {
  razonSocial: RazonSocial[];
  dataSource: any;
  cols: any[];
  razon: any;
  permisos: any;
  escritura: boolean = false;
  // Web Sockets
  private stompClient = null;
  constructor(protected razonSocialService: RazonSociosData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'razonsocial');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelRazonSocial', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getRazonSocial(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  crearRazonSocial() {
    this.dialogoService.open(RazonSocialCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getRazonSocial(0);
    });
  }

  updateRazonSocial(idRazonSocial) {
    this.dialogoService.open(RazonSocialCreateComponent, {
      context: {
        idTipo: 2,
        idRazonSocial: idRazonSocial,
      },
    }).onClose.subscribe(x => {
      this.getRazonSocial(1);
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getRazonSocial(mensajeAct) {
    this.razonSocialService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.razonSocial = [];
      this.razonSocial = data;
      this.dataSource = new MatTableDataSource(this.razonSocial); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Vacantes`);
  }

  bajaArea(idArea) {
    this.razonSocialService.getAreaById(idArea).subscribe(data => {
      this.razon = data;
    swal({
      title: '¿Deseas dar de baja esta razón social?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.razon['activo'] = 0;
        this.razonSocialService.put(idArea, this.razon).subscribe( result => {
          this.razonSocialService.postSocket({idArea: idArea, razon: this.razon}).subscribe(x => {});
        });
        swal('¡La area se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          // window.location.reload();
          this.getRazonSocial(0);
        }));
      }
    });
   });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.razonSocial.escritura;
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nombreComercial', 'alias', 'acciones'];
    this.getPermisos();
    this.getRazonSocial(0);
  }

}
