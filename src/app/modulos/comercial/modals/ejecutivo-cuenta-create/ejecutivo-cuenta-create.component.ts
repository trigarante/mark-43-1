import {Component, Input, OnInit} from '@angular/core';
import {EjecutivoCuenta, EjecutivoCuentaData} from '../../../../@core/data/interfaces/comerciales/ejecutivo-cuenta';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-ejecutivo-cuenta-create',
  templateUrl: './ejecutivo-cuenta-create.component.html',
  styleUrls: ['./ejecutivo-cuenta-create.component.scss'],
})
export class EjecutivoCuentaCreateComponent implements OnInit {
  @Input() idSocio: number;
  @Input() idEjecutivoCuenta: number;
  @Input() idTipo: number;
  @Input()
  checked: boolean;
  submitted: Boolean;
  ejecutivoCuenta: EjecutivoCuenta;
  ejecutivoCuentaCreateForm: FormGroup;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  favoritoBool: boolean;
  fav: any;

  // Web Sockets
  disabled = true;
  datos: any[] = [];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected ejecutivoCuentaService: EjecutivoCuentaData, private _router: Router) {
  }

  getEjecutivoCuenta() {
    this.ejecutivoCuentaService.getEjectutivoCuentaById(this.idEjecutivoCuenta).subscribe(data => {
      this.ejecutivoCuenta = data;
      this.ejecutivoCuentaCreateForm.controls['nombre'].setValue(this.ejecutivoCuenta.nombre);
      this.ejecutivoCuentaCreateForm.controls['apellidoPaterno'].setValue(this.ejecutivoCuenta.apellidoPaterno);
      this.ejecutivoCuentaCreateForm.controls['apellidoMaterno'].setValue(this.ejecutivoCuenta.apellidoMaterno);
      this.ejecutivoCuentaCreateForm.controls['telefono'].setValue(this.ejecutivoCuenta.telefono);
      this.ejecutivoCuentaCreateForm.controls['ext'].setValue(this.ejecutivoCuenta.ext);
      this.ejecutivoCuentaCreateForm.controls['celular'].setValue(this.ejecutivoCuenta.celular);
      this.ejecutivoCuentaCreateForm.controls['email'].setValue(this.ejecutivoCuenta.email);
      this.ejecutivoCuentaCreateForm.controls['puesto'].setValue(this.ejecutivoCuenta.puesto);
      this.ejecutivoCuentaCreateForm.controls['direccion'].setValue(this.ejecutivoCuenta.direccion);
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(this.ejecutivoCuenta.favorito);
      if (data.favorito === 1) {
        this.favoritoBool = true;
      } else {
        this.favoritoBool = false;
      }
    });
  }

  guardarEjecutivoCuenta() {
    this.submitted = true;
    if (this.ejecutivoCuentaCreateForm.invalid) {
      return;
    }
    if (this.ejecutivoCuentaCreateForm.controls['favorito'].value === true) {
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(1);
    } else if (this.ejecutivoCuentaCreateForm.controls['favorito'].value === false) {
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(0);
    }
    this.ejecutivoCuentaService.post(this.ejecutivoCuentaCreateForm.value).subscribe((result) => {
      this.ejecutivoCuentaService.postSocket(this.ejecutivoCuentaCreateForm.value).subscribe(x => {});
      this.ejecutivoCuentaCreateForm.reset();
      this.ref.close();
      this._router.navigate(['/modulos/comercial/contacto']);
    });
  }

  dismiss() {
    this.ref.close();
  }

  updateEjecutivoCuenta() {
    if (this.ejecutivoCuentaCreateForm.controls['favorito'].value === true) {
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(1);
    }else if (this.ejecutivoCuentaCreateForm.controls['favorito'].value === false) {
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(0);
    }
    this.ejecutivoCuentaService.put(this.idEjecutivoCuenta, this.ejecutivoCuentaCreateForm.value).subscribe(result => {
      this.ejecutivoCuentaService.postSocket(this.ejecutivoCuentaCreateForm.value).subscribe(x => {});
      this.ejecutivoCuentaCreateForm.reset();
      this.ref.close();
    });
  }

  favoritoSelec() {
    this.favoritoBool = null;
    if (this.idTipo !== 2) {
      if (this.ejecutivoCuentaCreateForm.controls['favorito'].value === 0 ||
        this.ejecutivoCuentaCreateForm.controls['favorito'].value === false ||
        this.ejecutivoCuentaCreateForm.controls['favorito'].value === undefined) {
        this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(0);
      } else {
        this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(1);
      }
    }
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEjecutivoCuenta();
    }
    this.ejecutivoCuentaCreateForm = this.fb.group({
      'idSocio': this.idSocio,
      'nombre': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'apellidoPaterno': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'apellidoMaterno': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'telefono': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])),
      'ext': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(5)])),
      'celular': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])),
      'activo': 1,
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'puesto': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'direccion': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'favorito': new FormControl(),
    });
    if (this.idTipo !== 2) {
      this.ejecutivoCuentaCreateForm.controls['favorito'].setValue(0);
    }
  }
}
