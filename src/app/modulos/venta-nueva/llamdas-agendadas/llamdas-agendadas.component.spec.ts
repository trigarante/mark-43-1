import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamdasAgendadasComponent } from './llamdas-agendadas.component';

describe('LlamdasAgendadasComponent', () => {
  let component: LlamdasAgendadasComponent;
  let fixture: ComponentFixture<LlamdasAgendadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlamdasAgendadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlamdasAgendadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
