import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../interfaces/catalogos/estado-escolaridad';
import {Observable} from 'rxjs';

@Injectable()
export class EstadoEscolaridadService extends EstadoEscolaridadData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<EstadoEscolaridad[]> {
    return this.http.get<EstadoEscolaridad[]>(this.baseURL + 'v1/estado-escolar');
  }

  getEstadoEscolaridadById(idEstadoEscolaridad): Observable<EstadoEscolaridad> {
    return this.http.get<EstadoEscolaridad>(this.baseURL + 'v1/estado-escolar/' + idEstadoEscolaridad);
  }

  post(estadoEscolaridad: EstadoEscolaridad): Observable<EstadoEscolaridad> {
    return this.http.post<EstadoEscolaridad>(this.baseURL + 'v1/estado-escolar', estadoEscolaridad);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'estadoescolaridad/saveEstadoEscolaridad', json);
  }

  put(idEstadoEscolaridad, estadoEscolaridad: EstadoEscolaridad) {
    return this.http.put<EstadoEscolaridad>(this.baseURL + 'v1/estado-escolar/' + idEstadoEscolaridad + '/'
      + sessionStorage.getItem('Empleado'), estadoEscolaridad);
  }
}
