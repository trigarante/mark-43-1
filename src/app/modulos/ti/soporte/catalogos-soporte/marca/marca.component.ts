import {Component, OnInit, ViewChild} from '@angular/core';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {NbDialogService} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CreateMarcaComponent} from '../modals/create-marca/create-marca.component';
import swal from 'sweetalert';
import {environment} from '../../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.scss'],
})
export class MarcaComponent implements OnInit {
  cols: any[];
  marca: Marca[];
  dataSource: any;
  /** Variable que se inicializa con los valores del Microservicio. */
  marcaget: Marca;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(protected marcaSoporteService: MarcaSoporteData, private dialogoService: NbDialogService,
              private router: Router) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    /**---------------------------------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'soportemarcas');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      _this.stompClient.subscribe('/task/panelSoporteMarcas', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getMarcasSoporte();
        }, 500);

      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getMarcasSoporte() {
    this.marcaSoporteService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.marca = [];
      this.marca = data;
      this.dataSource = new MatTableDataSource(this.marca); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearMarca() {
    this.dialogoService.open(CreateMarcaComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getMarcasSoporte();


    });
  }
  /** Función que obtiene la información de una marca dependiendo de su ID. */
  getMarcaById(idmarca) {
    this.marcaSoporteService.getMarcasSoporte(idmarca).subscribe(data => {
      this.marcaget = data;
    });
  }
  /** Función que despliega un modal en el cual se puede actualizar la información de una Marca. */
  updateMarca(idMarca) {
    this.dialogoService.open(CreateMarcaComponent, {
      context: {
        idMarca: idMarca,
        idTipo: 2,
      },
    }).onClose.subscribe(x => {
      this.getMarcasSoporte();
    });
  }
  /** Función que despliega un modal para seleccionar si se da de baja una marca. */
  bajaMarca(idMarca) {
    this.getMarcaById(idMarca);
    swal({
      title: '¿Deseas dar de baja esta Marca?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.marcaget['activo'] = 0;
        this.marcaSoporteService.put(idMarca, this.marcaget).subscribe( result => {
          this.marcaSoporteService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡la Marca se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getMarcasSoporte();
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.connect();
    this.getMarcasSoporte();
    this.getPermisos();
    this.estaActivo();
    this.cols = ['marca', 'acciones'];

  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.marcas.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.marcas.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
