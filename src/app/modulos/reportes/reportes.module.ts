import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReportesComponent} from './reportes.component';
import {ReportesRoutingModule} from './reportes-routing.module';
import {ReportePrecandidatoComponent} from './rrhh/reporte-precandidato/reporte-precandidato.component';
import {ReporteCandidatoComponent} from './rrhh/reporte-candidato/reporte-candidato.component';
import {ReporteCapacitacionComponent} from './rrhh/reporte-capacitacion/reporte-capacitacion.component';
import {ReporteEmpleadoComponent} from './rrhh/reporte-empleado/reporte-empleado.component';
import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {FormsModule} from '@angular/forms';
import {InputTextModule, TooltipModule} from 'primeng/primeng';
import {ReporteSeguimientoComponent} from './rrhh/reporte-seguimiento/reporte-seguimiento.component';
import { ReporteSocioComponent } from './comercial/reporte-socio/reporte-socio.component';
import { ReporteCampanaComponent } from './marketing/reporte-campana/reporte-campana.component';
import { ReporteSolicitudesComponent } from './venta-nueva/reporte-solicitudes/reporte-solicitudes.component';
import {
  MatBottomSheetModule,
  MatButtonModule, MatButtonToggleModule,
  MatCardModule, MatCheckboxModule, MatChipsModule,
  MatDatepickerModule, MatTabsModule,
  MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatNativeDateModule,
  MatOptionModule, MatPaginatorModule, MatProgressSpinnerModule, MatRadioModule,
  MatSelectModule, MatSortModule, MatTableModule, MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import { ReporteUsuariosComponent } from './ti/reporte-usuarios/reporte-usuarios.component';
import { ReporteExtensionesComponent } from './ti/reporte-extensiones/reporte-extensiones.component';
import { ReporteInventarioUsuarioComponent } from './ti/reporte-inventario-usuario/reporte-inventario-usuario.component';
import { ReportesGrupoPermisosComponent } from './ti/reporte-grupo-permisos/reportes-grupo-permisos.component';
import { ReporteInventarioRedesComponent } from './ti/reporte-inventario-redes/reporte-inventario-redes.component';
import { ReporteInventarioSoporteComponent } from './ti/reporte-inventario-soporte/reporte-inventario-soporte.component';
@NgModule({
  declarations: [
    ReportesComponent,
    ReportePrecandidatoComponent,
    ReporteCandidatoComponent,
    ReporteCapacitacionComponent,
    ReporteEmpleadoComponent,
    ReporteSeguimientoComponent,
    ReportesGrupoPermisosComponent,
    ReporteSocioComponent,
    ReporteCampanaComponent,
    ReporteSolicitudesComponent,
    ReporteUsuariosComponent,
    ReporteExtensionesComponent,
    ReporteInventarioUsuarioComponent,
    ReporteInventarioRedesComponent,
    ReporteInventarioSoporteComponent,
  ],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    TableModule,
    FormsModule,
    CalendarModule,
    InputTextModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    TooltipModule,
  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
  ],
})
export class ReportesModule {}
