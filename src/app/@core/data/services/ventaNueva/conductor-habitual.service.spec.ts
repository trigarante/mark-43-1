import { TestBed } from '@angular/core/testing';

import { ConductorHabitualService } from './conductor-habitual.service';

describe('ConductorHabitualService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConductorHabitualService = TestBed.get(ConductorHabitualService);
    expect(service).toBeTruthy();
  });
});
