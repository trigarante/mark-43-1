import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import * as moment from 'moment';
@Component({
  selector: 'ngx-asignar-finiquito',
  templateUrl: './asignar-finiquito.component.html',
  styleUrls: ['./asignar-finiquito.component.scss'],
})
export class AsignarFiniquitoComponent implements OnInit {
  /**Variable que recibe el tipo de dato a mostrar en el componente*/
  @Input() idTipo: number;
  /**Variable que recibe el tipo de dato a mostrar en el componente*/
  @Input() idEmpleado: number;
  /**Variable que REcibe el nombre del Empledo*/
  @Input() nombre: string;
  /** Variable que permite la inicialización del formulario para asignar el IMSS a un empleado. */
  asignarFiniquitoForm: FormGroup;
  /** Arreglo auxiliar para la Razon Social. */
  estadoFiniquito: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empleado: Empleados;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  // private stompClientEmpleadoBaja = null;
  constructor(
    protected ref: NbDialogRef<AsignarFiniquitoComponent>,
    private fb: FormBuilder,
    protected empleadosService: EmpleadosData,
  ) {
  }
  // Web socket connection

  getEstadoFiniquito() {
    this.estadoFiniquito = [];
    this.estadoFiniquito.push({'label': 'ENTREGADO', 'value': 1});
    this.estadoFiniquito.push({'label': 'PROCESO', 'value': 2});
    this.estadoFiniquito.push({'label': 'RECHAZADO', 'value': 0});
  }

  verificarFormulario() {
    if (this.asignarFiniquitoForm.valid && this.asignarFiniquitoForm.dirty && this.asignarFiniquitoForm !== null) {
      return false;
    } else {
      return true;
    }
  }

  agragarFiniquito() {
    let fecha: any;
    fecha = moment(this.asignarFiniquitoForm.controls['fechaFiniquito'].value).format('YYYY-MM-DD');
    let fechaFiniquito: any;
    fechaFiniquito = Date.parse(fecha + 'T24:00');

    this.empleadosService.finiquito(this.idEmpleado, fechaFiniquito,
      this.asignarFiniquitoForm.value.idEstadoFiniquito, this.asignarFiniquitoForm.value.montoFiniquito).subscribe(result => {
      // this.stompClientEmpleadoBaja.send('/tasks/saveEmpleadoBaja', {}, JSON.stringify(this.asignarFiniquitoForm.value));
      this.empleadosService.postSocketBaja(this.asignarFiniquitoForm.value).subscribe(x => {});
      this.asignarFiniquitoForm.reset();
      this.ref.close();
      /**window.location.reload();*/
    });
  }

  updateFiniquito() {
    this.empleadosService.getEmpleadoById(this.idEmpleado).subscribe(data => {
      this.empleado = data;
      let feAlta: any;
      feAlta = moment(this.empleado.fechaFiniquito).format('YYYY-MM-DD');
      /**this.asignarFiniquitoForm.controls['fechaFiniquito'].setValue(feAlta);*/
      this.asignarFiniquitoForm.controls['fechaFiniquito'].setValue(new Date(feAlta));
      this.asignarFiniquitoForm.controls['idEstadoFiniquito'].setValue(this.empleado.idEstadoFiniquito);
      this.asignarFiniquitoForm.controls['montoFiniquito'].setValue(this.empleado.montoFiniquito);
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    // this.connect();
    if (this.idTipo === 2) {
      this.updateFiniquito();
    }
    this.getEstadoFiniquito();
    this.asignarFiniquitoForm = this.fb.group({
      'fechaFiniquito': ['', Validators.required],
      'idEstadoFiniquito': ['', Validators.required],
      'montoFiniquito': ['', Validators.required],
    });
  }

}
