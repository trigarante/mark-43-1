import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {ProvedoresLeadService} from './provedores-lead.service';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';
fdescribe('ProvedoresLead service', () => {
  let injector: TestBed;
  let service: ProvedoresLeadService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    nombre: 'Proveedor 1',
    url: 'asdxxxxx',
    activo: 0,
  },
    {
      id: 'proveedoreslead',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ProvedoresLeadService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ProvedoresLeadService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/proveedores-lead');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/proveedores-lead', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/proveedores-lead');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/proveedores-lead', '/1/74');
      });
    });
  }
})
