import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SoporteComponent} from './soporte.component';
import {InventarioComponent} from './inventario/inventario.component';
import {BajaInventarioComponent} from './baja-inventario/baja-inventario.component';
import {InventarioRedesComponent} from './inventario-redes/inventario-redes.component';
import {InventarioSoporteComponent} from './inventario-soporte/inventario-soporte.component';
import {BajaInventarioRedesComponent} from './baja-inventario-redes/baja-inventario-redes.component';
import {BajaInventarioSoporteComponent} from './baja-inventario-soporte/baja-inventario-soporte.component';
import {AuthGuard} from '../../../@core/data/services/login/auth.guard';


 const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: SoporteComponent,
  children: [
    {
      path: 'inventario/:tipo',
      component: InventarioComponent,
       canActivate: [AuthGuard],
       data: { permiso: permisos.modulos.inventarioUsuario.activo},

    },
    {
      path: 'inventarioRedes/:tipo',
      component: InventarioRedesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.inventarioRedes.activo},
    },
    {
      path: 'catalogos-soporte',
      loadChildren: './catalogos-soporte/catalogos-soporte.module#CatalogosSoporteModule',
    },
    {
      path: 'baja-inventario',
      component: BajaInventarioComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajasInventarioUsuario.activo},
    },
    {
      path: 'inventarioSoporte/:tipo',
      component: InventarioSoporteComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.inventarioSoporte.activo},
    },
    {
      path: 'baja-inventario-redes',
      component: BajaInventarioRedesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajasInventarioRedes.activo},
    },
    {
      path: 'baja-inventario-soporte',
      component: BajaInventarioSoporteComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bajasInventarioSoporte.activo},
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SoporteRoutingModule {

}
