import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {LlamadasData} from '../../../../@core/data/interfaces/ventaNueva/llamadas';
import {UserService} from '../../../../@core/data/users.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../../@core/data/interfaces/ti/user';

@Component({
  selector: 'ngx-llamadas-mycc',
  templateUrl: './llamadas-mycc.component.html',
  styleUrls: ['./llamadas-mycc.component.scss'],
})
export class LlamadasMyccComponent implements OnInit {
  /**Variable que almacena el ID de Solicitud*/
  @Input() idSolicitud: number;
  /** Variable para la fecha mínima. */
  min: Date;
  /** Variable para la fecha máxima. */
  max: Date;
  /** Variable para inicializar el formulario. */
  formAgendarLlamada: FormGroup;
  /** Bandera para verificar que el formulario se ha enviado.*/
  submitted: boolean;
  /** Variable que alamacena la fecha de cita. */
  fecha: any;
  /** Variable para conocer el número de día de la fecha de cita. */
  dia: number;
  /**Varibale que almacena el body*/
  formLlamadas: any;
  /**Variable para establecer la fecha actual en el calendario*/
  minDate: Date;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**VAribale que almacenara el usuarios del GetCurrent*/
  usuario: any;
  /**VAriable que obtiene el nombre del Token*/
  name?: any;
  /**Variable que hace visible el iframe*/
  mostrarIframe: boolean;

  constructor(protected ref: NbDialogRef<LlamadasMyccComponent>, private fb: FormBuilder, private llamadasService: LlamadasData,
              private userService: UserService, private route: ActivatedRoute, private dialogoService: NbDialogService,
              private router: Router) {
    this.formLlamadas = {
      'numero': 5515261043,
      'fechaRegistro': null,
      'fecha': null,
      'idSolicitud': null,
    };
  }

  ngOnInit() {
    this.formAgendarLlamada = this.fb.group({
      'fechaCita': new FormControl('', Validators.compose( [Validators.required])),
      'horaCita': new FormControl('', Validators.compose([Validators.required, Validators.min(8.00), Validators.max(19.00)])),
    });
    this.name = localStorage.token;
    this.userService.getCurrentUser(this.name)
      .subscribe((users: User) => {
        this.usuario = users.usuario;
      });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Validar que no se ingrese manualmente una fecha anterior a la actual*/
  validarFechaEscritaManualmente(date: Date) {
    const hoy = moment(new Date()).format('YYYYMMDD');
    const fecha = moment(date).format('YYYYMMDD');
    if (fecha < hoy) {
      this.inactivo = true;
    }else if (fecha >= hoy) {
      this.inactivo = false;
    }
  }

  /** Función que permite verificar el formulario para asignar la fecha de cita, validando que sea un día laborable. */
  verificarFormulario() {
    // Creando variable de tipo Date para validar que el día introducido sea laborable
    if (this.formAgendarLlamada.valid) {
      this.fecha = new Date(this.formAgendarLlamada.value.fechaCita);  // Obtiene la fecha del día anterior
//      this.fecha.setDate(this.fecha.getDate() + 1); // Le sumamos un día para que concuerde con la fecha ingresada
      this.dia = this.fecha.getDay();
      if (this.dia === 0 || this.dia === 6) { // Si el dia de cita es sábado o domingo, no debe permitir asignar la cita
        return true;  // Botón deshabilitado
      } else {
        return false; // Botón habilitado
      }
    } else {
      return true;  // Botón deshabilitado
    }
  }

  getLlamadas() {
    this.llamadasService.get().subscribe( result => {
      // console.log(result);
    });
  }

  agendarLlamada() {
    let fechaRegistroLlamada: any;
    this.inactivo = true;
    fechaRegistroLlamada = moment(this.formAgendarLlamada.value.fechaCita).format('YYYY-MM-DD') + 'T' +
      this.formAgendarLlamada.value.horaCita;
    fechaRegistroLlamada = Date.parse(fechaRegistroLlamada);
    this.formLlamadas.fechaRegistro = this.formLlamadas.fecha = fechaRegistroLlamada;
    this.formLlamadas.idSolicitud = this.idSolicitud;
    this.usuario = 'mpineda@trigarante.com';
    // console.log(this.formLlamadas);
    this.llamadasService.post(this.usuario, this.formLlamadas).subscribe(
      result => {},
      error1 => {},
      () => {
        this.ref.close();
        this.router.navigateByUrl('/modulos/venta-nueva/mycc-registro/' + this.idSolicitud);
      },
    );
  }

}
