import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CotizacionesAli} from '../../interfaces/ventaNueva/cotizaciones-ali';

@Injectable()
export class CotizacionesAliService {

  private baseURL;

  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<CotizacionesAli[]> {
    return this.http.get<CotizacionesAli[]>(this.baseURL + 'v1/cotizacionesAli');
  }

  post(cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.post<CotizacionesAli>(this.baseURL + 'v1/cotizacionesAli', cotizacionAli);
  }

  getCotizacionAliById(id): Observable<CotizacionesAli> {
    return this.http.get<CotizacionesAli>(this.baseURL + 'v1/cotizacionesAli/' + id);
  }

  put(idCotizacionAli, cotizacionAli: CotizacionesAli): Observable<CotizacionesAli> {
    return this.http.put<CotizacionesAli>(this.baseURL + 'v1/cotizacionesAli/' + idCotizacionAli, cotizacionAli);
  }
}
