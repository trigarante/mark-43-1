import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { MarkcalleComponent } from './markcalle.component';
import { StartMarkcalleComponent } from './start-markcalle/start-markcalle.component';
import { LogmanualMarkcalleComponent } from './agent/logManual-markcalle/logmanual-markcalle.component';
import { AgendaMarkcalleComponent } from './agent/agenda-markcalle/agenda-markcalle.component';
import { LogacdMarkcalleComponent } from './agent/logAcd-markcalle/logacd-markcalle.component';
import { LogcallbackMarkcalleComponent } from './agent/logCallback-markcalle/logcallback-markcalle.component';
import { LogpredictivaMarkcalleComponent } from './agent/logPredictiva-markcalle/logpredictiva-markcalle.component';
import { LogprogresivaMarkcalleComponent } from './agent/logProgresiva-markcalle/logprogresiva-markcalle.component';



const routes: Routes = [{
  path: '',
  component: MarkcalleComponent,
  children: [
    {
      path: 'start',
      component: StartMarkcalleComponent,
    },
    {
      path: 'agenda',
      component: AgendaMarkcalleComponent,
    },
    {
      path: 'log-manual',
      component: LogmanualMarkcalleComponent,
    },
    {
      path: 'log-ACD',
      component: LogacdMarkcalleComponent,
    },
    {
      path: 'log-callback',
      component: LogcallbackMarkcalleComponent,
    },
    {
      path: 'log-predictiva',
      component: LogpredictivaMarkcalleComponent,
    },
    {
      path: 'log-progresiva',
      component: LogprogresivaMarkcalleComponent,
    },
  ],
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class MarkcalleRoutingModule {
}


