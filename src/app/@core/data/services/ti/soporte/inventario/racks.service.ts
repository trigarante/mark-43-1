import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Racks, RacksData} from '../../../../interfaces/ti/soporte/inventario/racks';


@Injectable()
export class RacksService extends RacksData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Racks[]> {
    return this.http.get<Racks[]>( this.baseURL + 'v1/racks');
  }

  post(racks: Racks): Observable<Racks> {
    return this.http.post<Racks>(this.baseURL + 'v1/racks', racks);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'rack/saveRack', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajarack/saveBajaRack', json);
  }
  getRacksById(idRacks): Observable<Racks> {
    return this.http.get<Racks>(this.baseURL + 'v1/racks/' + idRacks);
  }
  put(idRacks, racks: Racks): Observable<Racks> {
    return this.http.put<Racks>(this.baseURL + 'v1/racks/' + idRacks + '/'
      + sessionStorage.getItem('Empleado'), racks);
  }
}
