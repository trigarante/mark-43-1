import {getTestBed, TestBed} from '@angular/core/testing';
import {SolicitudesService} from './solicitudes.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Global} from '../global';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Solicitudes Servicios', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 285,
    nombre: 'PAULO',
    apellidoPaterno: 'MOARLES',
    apellidoMaterno: 'ESPINOSA',
    telefono: '5556125695',
    correo: 'TOMTEST@GMAIl.COM',
    cp: '01229',
    edad: 29,
    idBolsaTrabajo: 3,
    idReclutador: 1,
    idVacante: 2,
    fecha: '1564159920811',
    fechaCita: null,
    documentos: null,
    comentarios: null,
    fechaDescartado: '1564159920811',
    estado: 2,
    },
    {
      id: 'solicitudes',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/solicitudes/74');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/solicitudes', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/solicitudes');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/solicitudes', '/158/74');
      });
    });
  }
});
