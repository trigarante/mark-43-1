import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {AsignarProductoComponent} from '../modals/asignar-producto/asignar-producto.component';
import {AsignarConveniosComponent} from '../modals/asignar-convenios/asignar-convenios.component';
import {SubRamo, SubramoData} from '../../../@core/data/interfaces/comerciales/sub-ramo';
import {DatosSubramoComponent} from '../modals/datos-subramo/datos-subramo.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from "@angular/router";
@Component({
  selector: 'ngx-sub-ramo',
  templateUrl: './sub-ramo.component.html',
  styleUrls: ['./sub-ramo.component.scss'],
})
export class SubRamoComponent implements OnInit {
  cols: any[];
  subramo: SubRamo[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private subramosService: SubramoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getSubramo(0);
    this.connect();
    this.cols = [
      'detalle',
      'giro',
      'tipoSubramo',
      'prioridad',
      'descripcion',
      'acciones',
    ];
    this.getPermisos();
    this.estaActivo();
  }

  getSubramo(mensajeAct) {
    this.subramosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.subramo = [];
      this.subramo = data;
      this.dataSource = new MatTableDataSource(this.subramo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Sub Ramo`);
  }

  asignarProducto(idSubramo) {
    this.dialogService.open(AsignarProductoComponent, {
      context: {
        idSubramo: idSubramo,
      },
    });
  }

  asignarConvenios(idSubramo) {
    this.dialogService.open(AsignarConveniosComponent, {
      context: {
        idSubramo: idSubramo,
      },
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  verDatos(idSubramo, idRamo) {
    this.dialogService.open(DatosSubramoComponent, {
      context: {
        idSubramo: idSubramo,
        idRamo: idRamo,
      },
    }).onClose.subscribe(x => {
      this.getSubramo(1);
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'subramo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelSubRamo', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getSubramo(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verSubRamo.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.verSubRamo.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }


}
