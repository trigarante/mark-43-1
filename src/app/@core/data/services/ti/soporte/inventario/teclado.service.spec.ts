import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Teclado Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 12,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'FV6',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 4,
    fechaRecepcion: '2019-09-24T16:27:25.000+0000',
    fechaSalida: null,
    comentarios: 'EXCELENTE',
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    idMarca: 1,
    idSede: 2,
    origenRecursos: 'propio',
    sedeNombre: 'QUERETARO',
  },
    {
      id: 'teclado',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/teclado');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/teclado', '/12');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/teclado');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/teclado', '/12/74');
      });
    });
  }
});
