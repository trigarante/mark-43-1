import {getTestBed, inject, TestBed} from '@angular/core/testing';
import { AnalisisCompetenciasService } from './analisis-competencias.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';


fdescribe('AnalisisCompetenciasService', () => {
  let injector: TestBed;
  let service: AnalisisCompetenciasService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idCompetencia: 3,
    precios: 100.0,
    participacion: 1.0,
    activo: 1,
    idProducto: 2,
    idTipoCompetencia: 1,
    competencia: 'TEST FINAL01',
    idSubRamo: 1,
    prioridad: 1,
    nombre: 'testing',
    idRamo: 30,
    idTipoSubRamo: 1,
    tipoRamo: 'SEGUROS',
    descripcion: 'TEST FINALL',
    nombreComercial: 'KK',
    alias: 'KK',
    idEstadoSocio: 2,
    estado: 'INACTIVO',
    idEstado: 2,
    tipoSubRamo: 'AUTO',
    tipoCompetencia: 'DIRECTA',
  },
    {
      id: 'analisiscompetencias',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [AnalisisCompetenciasService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(AnalisisCompetenciasService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });

  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/analisis-competencias');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/analisis-competencias', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/analisis-competencias');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/analisis-competencias', '/1/74');
      });
    });
  }
});
