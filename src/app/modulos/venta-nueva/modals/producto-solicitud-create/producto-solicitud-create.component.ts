import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../comercial/socios/socios.component';
import {Router} from '@angular/router';
import {ProductoSolicitudData, ProductoSolicitudvn} from '../../../../@core/data/interfaces/ventaNueva/producto-solicitud';

@Component({
  selector: 'ngx-producto-solicitud-create',
  templateUrl: './producto-solicitud-create.component.html',
  styleUrls: ['./producto-solicitud-create.component.scss'],
})
export class ProductoSolicitudCreateComponent implements OnInit {
  @Input() idProspecto: number;
  /**Variable que inicializa el Formulario*/
  productoCreateForm: FormGroup;
  /**Variable que almacenara el TipoCategoria del Microservicio*/
  tipoCategorias: ProductoSolicitudvn[];
  /**Arreglo aux del microservicio*/
  tipoCategoria: any;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, private productoSolicitudService: ProductoSolicitudData) {
  }

  ngOnInit() {
    this.getTipoCategoria();
    this.productoCreateForm = this.fb.group({
      'idProspecto': this.idProspecto,
      'idTipoCategoria': new FormControl('', Validators.required),
      'datos': new FormControl('', Validators.compose([Validators.required, Validators.email])),
    });
  }

  dismiss() {
    this.ref.close();
  }

  getTipoCategoria() {
    this.productoSolicitudService.get().subscribe(data => {
      this.tipoCategorias = data;
      this.tipoCategoria = [];
      for (let i = 1; i < this.tipoCategoria.length; i++) {

      }
    });
  }

}
