import {Component, OnInit, ViewChild} from '@angular/core';
import {Presupuesto, PresupuestoData} from '../../../@core/data/interfaces/comerciales/presupuesto';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarPresupuestoComponent} from '../modals/asignar-presupuesto/asignar-presupuesto.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.scss'],
})
export class PresupuestoComponent implements OnInit {
  cols: any[];
  presupuesto: Presupuesto[];
  presupuestos: Presupuesto;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private presupuestoService: PresupuestoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getPresupuesto(0);
    this.cols = [
      'alias',
      'presupuesto',
      'anio',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getPresupuesto(mensajeAct) {
    this.presupuestoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
      // return result.filter(data => data.idEstadoSocio === 1 && data.idEstado === 1);
    })).subscribe(data => {
      this.presupuesto = [];
      this.presupuesto = data;
      this.dataSource = new MatTableDataSource(this.presupuesto); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Objetivo de Venta`);
  }

  updatePresupuesto(idPresupuesto, idSocio) {
    this.dialogService.open(AsignarPresupuestoComponent, {
      context: {
        idTipo: 2,
        idPresupuesto: idPresupuesto,
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getPresupuesto(1));
  }

  bajaPresupuesto(idPresupuesto) {
    this.getPresupuestosById(idPresupuesto);
    swal({
      title: '¿Deseas dar de baja este Objetivo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.presupuestos.activo = 0;
        this.presupuestoService.put(idPresupuesto, this.presupuestos).subscribe( result => {
          this.presupuestoService.postSocket({presupuesto: 'ok'}).subscribe(x => {});
        });
        swal('¡El Objetivo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getPresupuesto(0);
        }));
      }
    });
  }

  getPresupuestosById(idPresupuesto) {
    this.presupuestoService.getPresupuestoById(idPresupuesto).subscribe(data => {
      this.presupuestos = data;
    });
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'presupuesto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelPresupuesto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPresupuesto(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
    // this.router.navigate(['/modulos/comercial/socios']);
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.objetivoDeVentas.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.objetivoDeVentas.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
