import { Component } from '@angular/core';

@Component({
  selector: 'ngx-ti',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class TiComponent  {

  constructor() { }
}
