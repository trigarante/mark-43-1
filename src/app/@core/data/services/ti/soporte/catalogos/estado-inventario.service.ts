import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EstadoInventario, EstadoInventarioData} from '../../../../interfaces/ti/catalogos/estado-inventario';

@Injectable()
export class EstadoInventarioService extends EstadoInventarioData {
  private baseURL;
  private socketURl;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<EstadoInventario[]> {
    return this.http.get<EstadoInventario[]>(this.baseURL + 'v1/estado-inventario');
  }
  post(estadoInventario: EstadoInventario): Observable<EstadoInventario> {
    return this.http.post<EstadoInventario>(this.baseURL + 'v1/estado-inventario', estadoInventario);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'estadoinventario/saveEstadoInventario', json);
  }
  getEstadoInventarioById(idEstadoInventario): Observable<EstadoInventario> {
    return this.http.get<EstadoInventario>(this.baseURL + 'v1/estado-inventario/' + idEstadoInventario);
  }
  put(idEstadoInventario, estadoInventario: EstadoInventario): Observable<EstadoInventario> {
    return this.http.put<EstadoInventario>(this.baseURL + 'v1/estado-inventario/' + idEstadoInventario + '/'
      + sessionStorage.getItem('Empleado') , estadoInventario);
  }
}
