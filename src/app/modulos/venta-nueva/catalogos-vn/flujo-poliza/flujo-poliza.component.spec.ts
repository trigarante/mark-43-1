import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlujoPolizaComponent } from './flujo-poliza.component';

describe('FlujoPolizaComponent', () => {
  let component: FlujoPolizaComponent;
  let fixture: ComponentFixture<FlujoPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlujoPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlujoPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
