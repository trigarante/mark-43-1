import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Usuario, UsuarioData} from '../../interfaces/ti/usuario';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';



@Injectable({
  providedIn: 'root',
})
export class UsuarioService extends UsuarioData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseURL + 'v1/usuarios');
  }

  getUsuarioByIdSubarea(idSubarea, Estado): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseURL + 'v1/usuarios/idSubarea/' + idSubarea + '/asignado/' + Estado);
  }

  postUsuarios(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseURL + 'v1/usuarios', usuario);
  }

  getUsuarioByEstado(Estado): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseURL + 'v1/usuarios/asignado/' + Estado);
  }

  getUsuarioByRango(idEstado, menor, mayor): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseURL + 'v1/usuarios/findByRango/idEstado/' + idEstado + '/' + menor + '/' + mayor);
  }

  postUsuariosidEmpleados(jsonUsuario, idempleado): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseURL + 'v1/usuarios/idEmpleado/' + idempleado, jsonUsuario);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'supervisor/saveSupervisor', json);
  }
  postSocketTiUsuarios(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'tiusuarios/saveTiUsuarios', json);
  }

  put(idUsuario, usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.baseURL + 'v1/usuarios/idEmpleado/' + idUsuario + '/'
      + sessionStorage.getItem('Empleado'), usuario);
  }

  putUsuario(idUsuario, usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.baseURL + 'v1/usuarios/' + idUsuario + '/'
      + sessionStorage.getItem('Empleado'), usuario);
  }

  getUsuarioById(idUsuario): Observable<Usuario> {
    return this.http.get<Usuario>(this.baseURL + 'v1/usuarios/' + idUsuario);
  }

  // PutBajaGrupo(idGrupo): Observable<string> {
  //   return this.http.put<string>(this.baseURL + 'v1/usuarios/baja-grupo/' + idGrupo + '/'
  //     + sessionStorage.getItem('Empleado'), null);
  // }
  PutBajaGrupo(idGrupo): Observable<string> {
    return this.http.put<string>(this.baseURL + 'v1/usuarios/baja-grupo/' + idGrupo + '/'
      + sessionStorage.getItem('Empleado'), null, {headers: null , responseType: 'text' as 'json'});
  }
}
