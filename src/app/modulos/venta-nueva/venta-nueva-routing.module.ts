import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {VentaNuevaComponent} from './venta-nueva.component';
import {SolicitidesVnComponent} from './solicitides-vn/solicitides-vn.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';
import {CotizadorComponent} from './cotizador/cotizador.component';
import {LlamdasAgendadasComponent} from './llamdas-agendadas/llamdas-agendadas.component';
import {HistorialLlamadasComponent} from './historial-llamadas/historial-llamadas.component';
import {EmisionesComponent} from './emisiones/emisiones.component';
import {AlertComponent} from './modals/alert/alert.component';
import {StepCotizadorComponent} from './step-cotizador/step-cotizador.component';
import {PagosComponent} from './pagos/pagos.component';
import {ClienteComponent} from './cliente/cliente.component';

const routes: Routes = [{
  path: '',
  component: VentaNuevaComponent,
  children: [
    {
      path: 'solicitudes-vn',
      component: SolicitidesVnComponent,
    },
    {
      path: 'cotizador-vn/:idProspectoExis',
      component: CotizadorComponent,
    },
    {
      path: 'mycc-registro/:idSolicitud',
      component: LlamdasAgendadasComponent,
    },
    {
      path: 'historial-llamadas/:idSolicitud',
      component: HistorialLlamadasComponent,
    },
    {
      path: 'emisiones/:idSolicitud',
      component: EmisionesComponent,
    },
    {
      path: 'pagos',
      component: PagosComponent,
    },
    {
      path: 'cliente',
      component: ClienteComponent,
    },
    {
      path: 'catalogos',
      loadChildren: './catalogos-vn/catalogos-vn.module#CatalogosVnModule',
      canActivate:
        [AuthGuard],
    },
    {
      path: 'alert',
      component: AlertComponent,
    },
    {
      path: 'step-cotizador',
      component: StepCotizadorComponent,
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class VentaNuevaRoutingModule {
}
