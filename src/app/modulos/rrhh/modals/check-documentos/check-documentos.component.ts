import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {DocumentosSolicitudes, SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';
/** Componente del modal "check documentos" */
@Component({
  selector: 'ngx-check-documentos',
  templateUrl: './check-documentos.component.html',
  styleUrls: ['./check-documentos.component.scss'],
})
export class CheckDocumentosComponent implements OnInit {

  /** Variable que almacena el id de la solicitud obtenida. */
  @Input() idSolicitud: number;
  /** Variable que almacena el listado de documentos. */
  documentos: DocumentosSolicitudes;
  solicitudEliminada: boolean;
  /**Variable que desactiva boton al ejecutar*/
  inactivo: boolean;
  private stompClient = null;
  /**
   * El método constructor únicamente hace uso de dos servicios.
   *
   * @param {NbDialogRef<CheckDocumentosComponent>} ref Referencia al diálogo (modal).
   * @param solicitudesService Servicio del componente solicitudes para poder invocar los métodos. */
  constructor(protected ref: NbDialogRef<CheckDocumentosComponent>, private solicitudesService: SolicitudesData) {
    this.documentos = new class implements DocumentosSolicitudes {
      actaNacimiento = false;
      comprobanteDomicilio = false;
      comprobanteEstudios = false;
      curp = false;
      identificacionOficial = false;
      imss = false;
      rfc = false;
    };
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que obtiene el listado de documentos dependiendo del idSolicitud. */
  getSolicitudById() {
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe(data => {
      if (data.documentos == null) {
        this.documentos = new class implements DocumentosSolicitudes {
          actaNacimiento = false;
          comprobanteDomicilio = false;
          comprobanteEstudios = false;
          curp = false;
          identificacionOficial = false;
          imss = false;
          rfc = false;
        };
      } else {
        this.documentos = JSON.parse(data.documentos);
      }
    });
  }

  /** Función que permite guardar los documentos que han sido checados en el modal. Además realiza un refresh de la ventana. */
  checkDocumentos() {
    this.inactivo = true;
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe(data => {
      if (Number(data.estado) === 1 && data.idEtapa === 0) {
        this.solicitudesService.checkDocumentos(this.idSolicitud, this.documentos).subscribe((result) => {
          this.solicitudesService.postSocket({status: 'ok'}).subscribe(d => {});
          this.ref.close();
        });
      } else {
        this.solicitudEliminada = true;
      }
    });
  }

  /** Función que inicializa el componente mandando a llamar getSolicitudById. */
  ngOnInit() {
    this.getSolicitudById();
  }

}
