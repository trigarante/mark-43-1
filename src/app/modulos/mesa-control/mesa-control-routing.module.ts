import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {MesaControlComponent} from './mesa-control.component';

const routes: Routes = [{
  path: '',
  component: MesaControlComponent,
  children: [],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class MesaControlRoutingModule {
}
