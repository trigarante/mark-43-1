import {Component, Input, OnInit} from '@angular/core';
import {ProductoSolicitudData} from '../../../@core/data/interfaces/ventaNueva/producto-solicitud';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-llamdas-agendadas',
  templateUrl: './llamdas-agendadas.component.html',
  styleUrls: ['./llamdas-agendadas.component.scss'],
})
export class LlamdasAgendadasComponent implements OnInit {

  constructor(private productoSolicitudService: ProductoSolicitudData, private rutaActiva: ActivatedRoute) { }

  /**Variable que alamcena parametro por URL*/
  @Input() idSolicitud: number= this.rutaActiva.snapshot.params.idSolicitud;
  /**Variables que almacenan los valores del Producto*/
  cp: any;
  clave: any;
  marca: any;
  modelo: any;
  paquete: any;
  servicio: any;
  solicitud: any;
  descuento: any;
  movimiento: any;
  aseguradora: any;
  descripcion: any;

  ngOnInit() {
    this.getSolictud(this.idSolicitud);
  }

  getSolictud(idSolicitud) {
    this.productoSolicitudService.getProductoSolicitud(idSolicitud).subscribe(data => {
      if (data !== null) {
        this.solicitud = JSON.parse(data.datos);
        // this.mostrarDetalle = true;
        this.cp = this.solicitud.cp;
        this.clave = this.solicitud.clave;
        this.marca = this.solicitud.marca;
        this.modelo = this.solicitud.modelo;
        this.paquete = this.solicitud.paquete;
        this.servicio = this.solicitud.servicio;
        this.descuento = this.solicitud.descuento;
        this.movimiento = this.solicitud.movimiento;
        this.aseguradora = this.solicitud.aseguradora;
        this.descripcion = this.solicitud.descripcion;
      }
    });
  }

}
