import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ComercialComponent} from './comercial.component';
import {SociosComponent} from './socios/socios.component';
import {EjecutivoCuentaComponent} from './ejecutivo-cuenta/ejecutivo-cuenta.component';
import {AsignarUrlComponent} from './asignar-url/asignar-url.component';
import {ConveniosComponent} from './convenios/convenios.component';
import {ProductoComponent} from './producto/producto.component';
import {CompetenciaComponent} from './competencia/competencia.component';
import {AnalisisCompetenciaComponent} from './analisis-competencia/analisis-competencia.component';
import {CoberturaComponent} from './cobertura/cobertura.component';
import {PresupuestoComponent} from './presupuesto/presupuesto.component';
import {ContratosComponent} from './contratos/contratos.component';
import {CommonModule} from '@angular/common';
import {RamoComponent} from './ramo/ramo.component';
import {SubRamoComponent} from './sub-ramo/sub-ramo.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: ComercialComponent,
  children: [
    {
      path: 'socios',
      component: SociosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.verSocios.activo},
    },
    {
      path: 'contacto',
      component: EjecutivoCuentaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.contacto.activo},
    },
    {
      path: 'asignar-url',
      component: AsignarUrlComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.url.activo},
    },
    {
      path: 'ramo',
      component: RamoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.ramo.activo},
    },
    {
      path: 'sub-ramo',
      component: SubRamoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.verSubRamo.activo},
    },
    {
      path: 'convenios',
      component: ConveniosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.convenios.activo},
    },
    {
      path: 'producto',
      component: ProductoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.verProducto.activo},
    },
    {
      path: 'competencia',
      component: CompetenciaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.competenciaProducto.activo},
    },
    {
      path: 'analisis-competencia',
      component: AnalisisCompetenciaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.analisisCompetencia.activo},
    },
    {
      path: 'cobertura',
      component: CoberturaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.cobertura.activo},
    },
    {
      path: 'objetivo-de-ventas',
      component: PresupuestoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.objetivoDeVentas.activo},
    },
    {
      path: 'contratos',
      component: ContratosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.contrato.activo},
    },
    {
      path: 'catalogos-socios',
      loadChildren: './catalogos-socios/catalogos-socios.module#CatalogosSociosModule',
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ComercialRoutingModule {
}
