import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerDetalleComponent } from './ver-detalle.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {TelefonoIpData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {LaptopData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {SwitchsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {ServidoresData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {TarjetaRedData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {RacksData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanelData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRackData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {DiscoDuroExternoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {DiscoDuroInternoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBitData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {ApsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {BiometricosData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {CamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {BarrasMulticontactoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {FuenteCamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';
import {DvrData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';

describe('VerDetalleComponent', () => {
  let component: VerDetalleComponent;
  let fixture: ComponentFixture<VerDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerDetalleComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        CpuData,
        MonitorDate,
        LaptopData,
        DiademaData,
        TabletData,
        TecladoData,
        MouseData,
        SwitchsData,
        ServidoresData,
        TarjetaRedData,
        TelefonoIpData,
        RacksData,
        PatchPanelData,
        CharolasRackData,
        DiscoDuroExternoData,
        DiscoDuroInternoData,
        ChromeBitData,
        ApsData,
        BiometricosData,
        CamarasData,
        DvrData,
        BarrasMulticontactoData,
        FuenteCamarasData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerDetalleComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
