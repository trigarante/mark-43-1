import { Component, OnInit, ViewChild } from '@angular/core';
import { SociosComercial, SociosData } from '../../../../@core/data/interfaces/comerciales/socios-comercial';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import {map} from 'rxjs/operators';
import { AsignarUrlData, AsignarUrl } from '../../../../@core/data/interfaces/comerciales/asignar-url';
import { EjecutivoCuentaData, EjecutivoCuenta } from '../../../../@core/data/interfaces/comerciales/ejecutivo-cuenta';
import { Presupuesto, PresupuestoData } from '../../../../@core/data/interfaces/comerciales/presupuesto';
import { Contratos, ContratosData } from '../../../../@core/data/interfaces/comerciales/contratos';
import * as d3Select from 'd3-selection';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';
import * as d3Scale from 'd3-scale';
import * as d3Axis from 'd3-axis';
import * as d3Array from 'd3-array';

@Component({
  selector: 'ngx-reporte-socio',
  templateUrl: './reporte-socio.component.html',
  styleUrls: ['./reporte-socio.component.scss'],
})

export class ReporteSocioComponent implements OnInit {
  cols: any[];
  url: AsignarUrl[];
  socios: SociosComercial[];
  contacto: EjecutivoCuenta[];
  presupuesto: Presupuesto[]; // objetivo venta
  contrato: Contratos[];
  dataSource: any;
  tipoEquipos: string;
  contadorPermisos: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  private svgEstado: any;
  private svgPrioridad: any;
  private svgPresupuesto: any;
  private svgContrato: any;
  private arc: any;
  private colorPrioridad: string[];
  private back: any;
  private Xscale: any;
  private d3Array: any;
  private Yscale: any;
  private xAxis: any;
  private yAxis: any;
  private YAX: any;
  private barr: any;

  constructor(
    private sociosService: SociosData,
    private urlService: AsignarUrlData,
    private contactoService: EjecutivoCuentaData,
    private presupuestoService: PresupuestoData,
    private contratosService: ContratosData,
  ) {
    this.colorPrioridad = ['#c6225b', '#0288d1', '#ffb900'];
  }

  ngOnInit() {
//     this.drawSVG([]);
//     this.getEquipos(0);
//     this.cols = [
//       'nombreComercial',
//       'alias',
//       'estado',
//     ];
//   }
//    // socio, presupuesto, contrato
//   getEquipos(tipoEquipo) {
//     this.tipoEquipos = tipoEquipo.toLowerCase()
//     if (this.tipoEquipos === 'socio') {
//       this.sociosService.get().pipe(map(result => {   //  SOCIOS SERVICE
//         return result.filter(data => data.activo === '1');
//       })).subscribe(data => {
//         this.socios = data;
//         this.prioridadSVG(data);
//       });
//
//       this.presupuestoService.get().pipe(map(result => {    //  PRESUPUESTO SERVICE
//         return result.filter(data => data.activo === 1);
//       })).subscribe(data => {
//         this.presupuesto = data;
//         this.presupuestoSVG(data);
//
//       });
//
//       this.contratosService.get().pipe(map(result => {    //  CONTRATO SERVICE
//         return result.filter(data => data.activo === 1);
//       })).subscribe(data => {
//         this.contrato = data;
//         this.contratoSVG(data);
//       });
//   }
//
//   applyFilter(filterValue: string) {
//     this.dataSource.filter = filterValue.trim().toLowerCase();
//   }
//
//   drawSVG(d) {
//     this.estadoSVG(d);
//   }
//   estadoSVG (data) {
//     const size = [500, 150];
//     this.svgEstado = d3Select.select('#estado').append('svg')
//       .attr('width', '100%')
//       .attr('height', '100%')
//       .attr('viewBox', `${0} ${0} ${size[1]} ${size[0]}` );
//
//     const fit1 = this.svgEstado.append('circle')
//       .attr('cx', 60).attr('cy', 120)
//       .attr('r', 50)
//       .style('fill', 'blue');
//
//     const fit2 = this.svgEstado.append('circle')
//       .attr('cx', 60).attr('cy', 300)
//       .attr('r', 50)
//       .style('fill', 'blue');
//   }
//
//   prioridadSVG(data) {
//     const tiempoAnimacion = 5000;
//     const datos = this.socioData(data);
//
//     const size = [100, 500];
//     this.svgPrioridad = d3Select.select('#prioridad').append('svg')
//       .attr('width', '100%')
//       .attr('height', '100%')
//       .attr('viewBox', `${0} ${0} ${size[1]} ${size[0]}` );
// // ---------------------    ALTA  -------------------------------
//     const prioriAlta = this.svgPrioridad.append('circle')
//       .attr('cx', 80).attr('cy',50)
//       .attr('r', 40)
//       .style('fill', this.colorPrioridad[0]);
//
//     const ringAlta = this.svgPrioridad
//       .append('path')
//       .datum({endAngle: (6.24 / datos[3]) * datos[0].length})
//       .attr('fill', this.colorPrioridad[0])
//       .attr('transform', 'translate(80,50)')
//       .attr('d', this.arc);
//
//     ringAlta.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
//
//     const porcentajeAlta = Math.trunc((datos[0].length / datos[3]) * 100 );
//     var percAlta = this.svgPrioridad.append('text').attr('x', 63).attr('y', 60).attr('font-size', 30)
//       .attr('id', 'txtValu').text(0).attr('fill', 'white')
//       .attr('cursor', 'pointer');
//     percAlta.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('text', () => (t) => percAlta.text(Math.trunc(d3Inter.interpolate(0, porcentajeAlta)(t))) );
//
//     const inicioA = this.svgPrioridad.append('rect').attr('x', 80).attr('y', 2).attr('width', 4 ).attr('height', 10).attr('fill', this.colorPrioridad[0]);
//     const textoA = this.svgPrioridad.append('text').attr('x', 20).attr('y', 12).attr('fill', this.colorPrioridad[0]).attr('font-size', 16).text('ALTA');
// // ------------------------  MEDIA   ----------------------------------
//     const prioriMedia = this.svgPrioridad.append('circle')
//       .attr('cx', 250).attr('cy', 50)
//       .attr('r', 40)
//       .style('fill', this.colorPrioridad[1]);
//
//     const ringMedia = this.svgPrioridad
//       .append('path')
//       .datum({endAngle: (6.24 / datos[3]) * datos[1].length})
//       .attr('fill',  this.colorPrioridad[1])
//       .attr('transform', 'translate(250,50)')
//       .attr('d', this.arc);
//
//     ringMedia.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
//
//     const porcentajeMedia = Math.trunc((datos[1].length / datos[3]) * 100 );
//     var percMedia = this.svgPrioridad.append('text').attr('x', 233).attr('y', 60).attr('font-size', 30)
//       .attr('id', 'txtValu').text(0).attr('fill', 'white')
//       .attr('cursor', 'pointer');
//     percMedia.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('text', () => (t) => percMedia.text(Math.trunc(d3Inter.interpolate(0, porcentajeMedia)(t))) );
//
//     const inicioM = this.svgPrioridad.append('rect').attr('x', 250).attr('y', 2).attr('width', 4 ).attr('height', 10).attr('fill', this.colorPrioridad[1]);
//     const textoM = this.svgPrioridad.append('text').attr('x', 180).attr('y', 12).attr('fill', this.colorPrioridad[1]).attr('font-size', 16).text('MEDIA');
// // ---------------------   BAJA  ------------------------------------------------------
//     const prioriBaja = this.svgPrioridad.append('circle')
//       .attr('cx', 420).attr('cy', 50)
//       .attr('r', 40)
//       .style('fill', this.colorPrioridad[2]);
//
//     const ringBaja = this.svgPrioridad
//       .append('path')
//       .datum({endAngle: (6.24 / datos[3]) * datos[2].length})
//       .attr('fill', this.colorPrioridad[2])
//       .attr('transform', 'translate(420,50)')
//       .attr('d', this.arc);
//
//     ringBaja.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
//
//     const porcentajeBaja = Math.trunc((datos[2].length / datos[3]) * 100 );
//     var percBaja = this.svgPrioridad.append('text').attr('x', 405).attr('y', 60).attr('font-size', 30)
//       .attr('id', 'txtValu').text(0).attr('fill', 'white')
//       .attr('cursor', 'pointer');
//     percBaja.transition()
//       .duration(tiempoAnimacion)
//       .attrTween('text', () => (t) => percBaja.text(Math.trunc(d3Inter.interpolate(0, porcentajeBaja)(t))) );
//
//     const inicioB = this.svgPrioridad.append('rect').attr('x', 420).attr('y', 2).attr('width', 4 ).attr('height', 10).attr('fill', this.colorPrioridad[2]);
//     const textob = this.svgPrioridad.append('text').attr('x', 358).attr('y', 12).attr('fill', this.colorPrioridad[2]).attr('font-size', 16).text('BAJA');
//     //-----------------------------------------------------------------------------------------------------------------
//
//     this.arc = d3Shape.arc()
//       .innerRadius(45)
//       .outerRadius(48)
//       .startAngle(0);
//   }
//
//   presupuestoSVG(data) {
//     const root = this.presupuestoData(data);
//     const label = [];
//     for (let j = 1; j <= data.length; j++) {
//       label.push(j); }
//     const sizeP = [500, 500];
//
//     this.svgPresupuesto = d3Select.select('#presupuesto').append('svg')
//       .attr('width', '100%')
//       .attr('height', '100%')
//       .attr('viewBox', `${0} ${0} ${sizeP[0]} ${sizeP[1]}` )
//       .attr('transform', 'rotate(-90 0 0)');
//
//     this.Xscale = d3Scale.scaleLinear()
//       .domain([d3Array.max(root), 0])
//       .range([sizeP[0] - 100, 0]);
//     this.Yscale = d3Scale.scaleBand()
//       .domain(label)
//       .range([20, sizeP[1] + 17]);
//     this.xAxis = d3Axis.axisLeft().scale(this.Xscale).ticks(25);
//     this.yAxis = d3Axis.axisBottom().scale(this.Yscale).ticks(25);
//     this.YAX = this.svgPresupuesto.append('g')
//       .attr('class', 'axis')
//       .attr('transform', 'rotate(90 20 0)')
//       .call(this.yAxis);
//
//     this.barr = this.svgPresupuesto.selectAll('rect .barr')
//       .data(root)
//       .enter()
//       .append('rect')
//       .attr('y', (d, i) => (i * (sizeP[0] ) / root.length))
//       .attr('x', 21)
//       .attr('height', '2%')
//       .attr('width', 0) // (d) => this.Xscale(d) )
//       .attr('fill', '#ffc039')
//       .attr('rx', '3')
//       .attr('cursor', 'pointer');
//
//     this.barr.transition()
//       .duration(1000)
//       .delay((d, i) => i * 60)
//       .attr('width', (d) => this.Xscale(d) + 7)
//       .transition()
//       .duration(200)
//       .attr('width', (d) => this.Xscale(d) - 6)
//       .transition()
//       .duration(150)
//       .attr('width', (d) => this.Xscale(d) + 4)
//       .transition()
//       .duration(100)
//       .attr('width', (d) => this.Xscale(d));
//
//   }
//
//   contratoSVG(data) {
//     const root = this.contratoData(data);
//     const sizeC = [600, 360];
//     const label = [];
//     for (let j = 1; j <= data.length; j++) {
//       label.push(j); }
//
//     this.svgContrato = d3Select.select('#contrato').append('svg')
//       .attr('width', '100%')
//       .attr('height', '100%')
//       .attr('viewBox', `${0} ${0} ${sizeC[1]} ${sizeC[0]}` );
//
//     this.Xscale = d3Scale.scaleLinear()
//       .domain([d3Array.max(root), 0])
//       .range([sizeC[1] - 100, 0]);
//     this.Yscale = d3Scale.scaleBand()
//       .domain(label)
//       .range([20, sizeC[0] + 17]);
//     this.xAxis = d3Axis.axisLeft().scale(this.Xscale).ticks(25);
//     this.yAxis = d3Axis.axisBottom().scale(this.Yscale).ticks(25);
//     this.YAX = this.svgContrato.append('g')
//       .attr('class', 'axis')
//       .attr('transform', 'rotate(90 20 0)')
//       .call(this.yAxis);
//
//     this.barr = this.svgContrato.selectAll('rect .barr')
//       .data(root)
//       .enter()
//       .append('rect')
//       .attr('y', (d, i) => (i * (sizeC[0] ) / root.length))
//       .attr('x', 21)
//       .attr('height', '2%')
//       .attr('width', 0) // (d) => this.Xscale(d) )
//       .attr('fill', '#499c54')
//       .attr('rx', '3')
//       .attr('cursor', 'pointer');
//
//     this.barr.transition()
//       .duration(1000)
//       .delay((d, i) => i * 60)
//       .attr('width', (d) => this.Xscale(d) + 7)
//       .transition()
//       .duration(200)
//       .attr('width', (d) => this.Xscale(d) - 6)
//       .transition()
//       .duration(150)
//       .attr('width', (d) => this.Xscale(d) + 4)
//       .transition()
//       .duration(100)
//       .attr('width', (d) => this.Xscale(d));
//   }
//
//   estado
//
//   socioData (data) {
//     const alta = [];
//     const media = [];
//     const baja = [];
//     for (let i = 0; i < data.length; i++) {
//       if (data[i].prioridad === 1) {
//         alta.push(data[i]);
//       } else if (data[i].prioridad === 2) {
//         media.push(data[i]);
//       } else if (data[i].prioridad === 3) {
//         baja.push(data[i]);
//       }
//     }
//     return [alta, media, baja, data.length]
//   }
//
//   presupuestoData (data) {
//     const socio = [] ;
//     let socio1: any ;
//     let socio2: any  ;
//     let socio3: any  ;
//     let socio4: any ;
//     let socio5: any  ;
//     let socio6: any  ;
//     let socio7: any  ;
//     let socio8: any  ;
//     let socio9: any  ;
//     let socio10: any ;
//     let socio11: any ;
//     let socio12: any ;
//     let socio13: any ;
//     let socio14: any ;
//     let socio15: any ;
//     let socio16: any ;
//     let socio17: any ;
//     let socio18: any ;
//     let socio19: any ;
//     let socio20: any ;
//     let socio21: any ;
//     let socio22: any ;
//     let socio23: any ;
//     let socio24: any ;
//     let socio25: any ;
//     let socio26: any ;
//     let socio27: any ;
//     let socio28: any ;
//     let socio29: any ;
//     let socio30: any ;
//     let socio31: any ;
//     let socio32: any ;
//     let socio33: any ;
//     let socio34: any ;
//     let socio35: any ;
//     let socio36: any ;
//     let socio37: any ;
//     let socio38: any ;
//     let socio39: any ;
//     let socio40: any ;
//
//     for (let j = 0; j < data.length; j++) {
//       if (data[j].idSocio === 1) {
//         socio1 = data[j].presupuesto;
//       } else if (data[j].idSocio === 2) {
//         socio2 = data[j].presupuesto;
//       } else if (data[j].idSocio === 3) {
//         socio3 = data[j].presupuesto;
//       } else if (data[j].idSocio === 4) {
//         socio4 = data[j].presupuesto;
//       } else if (data[j].idSocio === 5) {
//         socio5 = data[j].presupuesto;
//       } else if (data[j].idSocio === 6) {
//         socio6 = data[j].presupuesto;
//       } else if (data[j].idSocio === 7) {
//         socio7 = data[j].presupuesto;
//       } else if (data[j].idSocio === 8) {
//         socio8 = data[j].presupuesto;
//       } else if (data[j].idSocio === 9) {
//         socio9 = data[j].presupuesto;
//       } else if (data[j].idSocio === 10) {
//         socio10 = data[j].presupuesto;
//       } else if (data[j].idSocio === 11) {
//         socio11 = data[j].presupuesto;
//       } else if (data[j].idSocio === 12) {
//         socio12 = data[j].presupuesto;
//       } else if (data[j].idSocio === 13) {
//         socio13 = data[j].presupuesto;
//       } else if (data[j].idSocio === 14) {
//         socio14 = data[j].presupuesto;
//       } else if (data[j].idSocio === 15) {
//         socio15 = data[j].presupuesto;
//       } else if (data[j].idSocio === 16) {
//         socio16 = data[j].presupuesto;
//       } else if (data[j].idSocio === 17) {
//         socio17 = data[j].presupuesto;
//       } else if (data[j].idSocio === 18) {
//         socio18 = data[j].presupuesto;
//       } else if (data[j].idSocio === 19) {
//         socio19 = data[j].presupuesto;
//       } else if (data[j].idSocio === 20) {
//         socio20 = data[j].presupuesto;
//       } else if (data[j].idSocio === 21) {
//         socio21 = data[j].presupuesto;
//       } else if (data[j].idSocio === 22) {
//         socio22 = data[j].presupuesto;
//       } else if (data[j].idSocio === 23) {
//         socio23 = data[j].presupuesto;
//       } else if (data[j].idSocio === 24) {
//         socio24 = data[j].presupuesto;
//       } else if (data[j].idSocio === 25) {
//         socio25 = data[j].presupuesto;
//       } else if (data[j].idSocio === 26) {
//         socio26 = data[j].presupuesto;
//       } else if (data[j].idSocio === 27) {
//         socio27 = data[j].presupuesto;
//       } else if (data[j].idSocio === 28) {
//         socio28 = data[j].presupuesto;
//       } else if (data[j].idSocio === 29) {
//         socio29 = data[j].presupuesto;
//       } else if (data[j].idSocio === 30) {
//         socio30 = data[j].presupuesto;
//       } else if (data[j].idSocio === 31) {
//         socio31 = data[j].presupuesto;
//       } else if (data[j].idSocio === 32) {
//         socio32 = data[j].presupuesto;
//       } else if (data[j].idSocio === 33) {
//         socio33 = data[j].presupuesto;
//       } else if (data[j].idSocio === 34) {
//         socio34 = data[j].presupuesto;
//       } else if (data[j].idSocio === 35) {
//         socio35 = data[j].presupuesto;
//       } else if (data[j].idSocio === 36) {
//         socio36 = data[j].presupuesto;
//       } else if (data[j].idSocio === 37) {
//         socio37 = data[j].presupuesto;
//       } else if (data[j].idSocio === 38) {
//         socio38 = data[j].presupuesto;
//       } else if (data[j].idSocio === 39) {
//         socio39 = data[j].presupuesto;
//       } else if (data[j].idSocio === 40) {
//         socio40 = data[j].presupuesto;
//       }
//     }
//     socio.push(parseInt(socio1, 10));
//     socio.push(parseInt(socio2, 10));
//     socio.push(parseInt(socio3, 10));
//     socio.push(parseInt(socio4, 10));
//     socio.push(parseInt(socio5, 10));
//     socio.push(parseInt(socio6, 10));
//     socio.push(parseInt(socio7, 10));
//     socio.push(parseInt(socio8, 10));
//     socio.push(parseInt(socio9, 10));
//     socio.push(parseInt(socio10, 10));
//     socio.push(parseInt(socio11, 10));
//     socio.push(parseInt(socio12, 10));
//     socio.push(parseInt(socio13, 10));
//     socio.push(parseInt(socio14, 10));
//     socio.push(parseInt(socio15, 10));
//     socio.push(parseInt(socio16, 10));
//     socio.push(parseInt(socio17, 10));
//     socio.push(parseInt(socio18, 10));
//     socio.push(parseInt(socio19, 10));
//     socio.push(parseInt(socio20, 10));
//     socio.push(parseInt(socio21, 10));
//     socio.push(parseInt(socio22, 10));
//     socio.push(parseInt(socio23, 10));
//     socio.push(parseInt(socio24, 10));
//     socio.push(parseInt(socio25, 10));
//     socio.push(parseInt(socio26, 10));
//     socio.push(parseInt(socio27, 10));
//     socio.push(parseInt(socio28, 10));
//     socio.push(parseInt(socio29, 10));
//     socio.push(parseInt(socio30, 10));
//     socio.push(parseInt(socio31, 10));
//     socio.push(parseInt(socio32, 10));
//     socio.push(parseInt(socio33, 10));
//     socio.push(parseInt(socio34, 10));
//     socio.push(parseInt(socio35, 10));
//     socio.push(parseInt(socio36, 10));
//     socio.push(parseInt(socio37, 10));
//     socio.push(parseInt(socio38, 10));
//     socio.push(parseInt(socio39, 10));
//     socio.push(parseInt(socio40, 10));
//    return socio;
//   }
//
//   contratoData(data) {
//     const socio = [] ;
//     let socio1: any ;
//     let socio2: any  ;
//     let socio3: any  ;
//     let socio4: any ;
//     let socio5: any  ;
//     let socio6: any  ;
//     let socio7: any  ;
//     let socio8: any  ;
//     let socio9: any  ;
//     let socio10: any ;
//     let socio11: any ;
//     let socio12: any ;
//     let socio13: any ;
//     let socio14: any ;
//     let socio15: any ;
//     let socio16: any ;
//     let socio17: any ;
//     let socio18: any ;
//     let socio19: any ;
//     let socio20: any ;
//     let socio21: any ;
//     let socio22: any ;
//     let socio23: any ;
//     let socio24: any ;
//     let socio25: any ;
//     let socio26: any ;
//     let socio27: any ;
//     let socio28: any ;
//     let socio29: any ;
//     let socio30: any ;
//     let socio31: any ;
//     let socio32: any ;
//     let socio33: any ;
//     let socio34: any ;
//     let socio35: any ;
//     let socio36: any ;
//     let socio37: any ;
//     let socio38: any ;
//     let socio39: any ;
//     let socio40: any ;
//
//     for (let j = 0; j < data.length; j++) {
//       if (data[j].idSocio === 1) {
//         socio1 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 2) {
//         socio2 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 3) {
//         socio3 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 4) {
//         socio4 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 5) {
//         socio5 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 6) {
//         socio6 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 7) {
//         socio7 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 8) {
//         socio8 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 9) {
//         socio9 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 10) {
//         socio10 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 11) {
//         socio11 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 12) {
//         socio12 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 13) {
//         socio13 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 14) {
//         socio14 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 15) {
//         socio15 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 16) {
//         socio16 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 17) {
//         socio17 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 18) {
//         socio18 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 19) {
//         socio19 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 20) {
//         socio20 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 21) {
//         socio21 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 22) {
//         socio22 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 23) {
//         socio23 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 24) {
//         socio24 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 25) {
//         socio25 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 26) {
//         socio26 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 27) {
//         socio27 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 28) {
//         socio28 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 29) {
//         socio29 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 30) {
//         socio30 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 31) {
//         socio31 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 32) {
//         socio32 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 33) {
//         socio33 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 34) {
//         socio34 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 35) {
//         socio35 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 36) {
//         socio36 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 37) {
//         socio37 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 38) {
//         socio38 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 39) {
//         socio39 = data[j].diasRestantes;
//       } else if (data[j].idSocio === 40) {
//         socio40 = data[j].diasRestantes;
//       }
//     }
//     socio.push(socio1);
//     socio.push(socio2);
//     socio.push(socio3);
//     socio.push(socio4);
//     socio.push(socio5);
//     socio.push(socio6);
//     socio.push(socio7);
//     socio.push(socio8);
//     socio.push(socio9);
//     socio.push(socio10);
//     socio.push(socio11);
//     socio.push(socio12);
//     socio.push(socio13);
//     socio.push(socio14);
//     socio.push(socio15);
//     socio.push(socio16);
//     socio.push(socio17);
//     socio.push(socio18);
//     socio.push(socio19);
//     socio.push(socio20);
//     socio.push(socio21);
//     socio.push(socio22);
//     socio.push(socio23);
//     socio.push(socio24);
//     socio.push(socio25);
//     socio.push(socio26);
//     socio.push(socio27);
//     socio.push(socio28);
//     socio.push(socio29);
//     socio.push(socio30);
//     socio.push(socio31);
//     socio.push(socio32);
//     socio.push(socio33);
//     socio.push(socio34);
//     socio.push(socio35);
//     socio.push(socio36);
//     socio.push(socio37);
//     socio.push(socio38);
//     socio.push(socio39);
//     socio.push(socio40);
//     return socio;
  }
}
