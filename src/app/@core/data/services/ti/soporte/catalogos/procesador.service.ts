import {Injectable} from '@angular/core';
import {Procesador, ProcesadorSoporteData} from '../../../../interfaces/ti/catalogos/procesador';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProcesadorService extends ProcesadorSoporteData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Procesador[]> {
    return this.http.get<Procesador[]>(this.baseURL + 'v1/procesador');
  }

  post(procesador: Procesador): Observable<Procesador> {
    return this.http.post<Procesador>(this.baseURL + 'v1/procesador', procesador);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'procesadores/saveProcesadores', json);
  }

  getProcesadorSoporte(idProcesador): Observable<Procesador> {
    return this.http.get<Procesador>(this.baseURL + 'v1/procesador/' + idProcesador);
  }

  put(idProcesador, procesador: Procesador): Observable<Procesador> {
    return this.http.put<Procesador>(this.baseURL + 'v1/procesador/' + idProcesador + '/'
      + sessionStorage.getItem('Empleado'), procesador);
  }
}
