import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {Campana, CampanaData} from '../../../../@core/data/interfaces/catalogos/campana';
import {map} from 'rxjs/operators';
import {NbDialogRef} from '@nebular/theme';
import {Seguimiento, SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import {Router} from '@angular/router';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import * as moment from 'moment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/** Componente del modal "asignar seguimiento". */
@Component({
  selector: 'ngx-asignar-seguimiento',
  templateUrl: './asignar-seguimiento.component.html',
  styleUrls: ['./asignar-seguimiento.component.scss'],
})
export class AsignarSeguimientoComponent implements OnInit {
  seguimientoEliminado: boolean;
  teoricoEliminado: boolean;
  /** Variable que almacena el idCapacitacion obtenido. */
  @ Input() idCapacitacion: number;
  /** Variable que almacena el idPrecandidato obtenido. */
  @ Input() idPrecandidato: number;
  @ Input() idTipo: number;
  @ Input() idSeguimiento: number;
  /**Variable que almacena el Nombre*/
  @ Input() nombre: string;
  /**Variable que almacena el apellidoPaterno*/
  @ Input() apellidoPaterno: string;
  /**Variable que almacena el apellidoMaterno*/
  @ Input() apellidoMaterno: string;
  /**Variable que almacena el nombreCompleto*/
  nombreCompleto: string;
  /** Variable que permite la inicialización del formulario para asignar el seguimiento a un precandidato. */
  seguimientoForm: FormGroup;
  /** Lista de elementos de rollplay. */
  rollplay: SelectItem[];
  /** Arreglo que se inicializa con los valores del campo campana (campaña) desde el Microservicio. */
  campanas: Campana[];
  /** Arreglo auxiliar para campanas */
  campanaSelect: any[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los valores del campo sede desde el Microservicio.  */
  reclutadores: Empleados[];
  /** Arreglo auxiliar para reclutadores. */
  reclutador: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  /**Variable que almacena el segumiento ByID*/
  seguimiento: any;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**variable que valida que la fecha haya sido modificada al actualizar*/
  feIngreso: any;
  /**
   * El método constructor invoca Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {CampanaData} campanasService Servicio del componente campanas para poder invocar los métodos.
   * @param {NbDialogRef<AsignarSeguimientoComponent>} ref Referencia al diálogo (mensaje).
   * @param {SeguimientoData} seguimientoService Servicio del componente seguimiento para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected campanasService: CampanaData, protected ref: NbDialogRef<AsignarSeguimientoComponent>,
              protected seguimientoService: SeguimientoData,  private router: Router, protected empleadoService: EmpleadosData,
              private subareaService: SubareaData, protected capacitacionService: CapacitacionData,
              ) {
  }
  /** Función que inicializa la lista "rollplay", agregándole los valores de "aprobado" y "no aprobado". */
  calificacionRollplay() {
    this.rollplay = [];
    this.rollplay.push({label: 'Aprobado', value: true});
    this.rollplay.push({label: 'No aprobado', value: false});
  }

  getReclutador() {
    this.empleadoService.get().pipe(map(result => {
      return result.filter(data => data.idPuesto === 30);
    })).subscribe(data => {
      this.reclutadores = data;
      this.reclutador = [];
      for (let i = 0; i < this.reclutadores.length; i++) {
        this.reclutador.push({'label': this.reclutadores[i].nombre + ' ' + this.reclutadores[i].apellidoPaterno
            + ' ' + this.reclutadores[i].apellidoMaterno + ' - ' + this.reclutadores[i].id, 'value': this.reclutadores[i].id});
      }
    });
  }

  getSubarea() {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.subareas = data;
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda la información que ha sido vaciada dentro del formulario, realizando una petición post. */
    guardarSegimiento() {
    this.inactivo = true;
    this.submitted = true;
    if (this.seguimientoForm.invalid) {
      return;
    }
      const idEtapa = 5;
    let fecha: any;
    fecha = moment(this.seguimientoForm.value.fechaIngreso).format('YYYY-MM-DD');
    let fechaIngreso: any;
    fechaIngreso = Date.parse(fecha + 'T01:00');
    this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(data => {
      if (data.idEtapa === 4 && data.idEstado === 1 || data.idEtapa === 4 && data.idEstado === 16) {
        this.seguimientoService.post(this.seguimientoForm.value, this.idPrecandidato, idEtapa, fechaIngreso).subscribe(
          (result) => {
          },
          error => { },
          () => {
            this.capacitacionService.postSocket(this.seguimientoForm.value).subscribe(x => {});
            this.seguimientoService.postSocketRolplay(this.seguimientoForm.value).subscribe(x => {});
            this.seguimientoForm.reset();
            this.ref.close();
            this.router.navigate(['modulos/rrhh/seguimiento']);
          },

        );
      }else {
        this.teoricoEliminado = true;
      } });
  }

  getSeguimientoById(id) {
    this.seguimientoService.getSeguimientoById(id).subscribe(result => {
      this.seguimiento = result;
      if (this.seguimiento.calificacionRollplay === 1) {
        this.seguimientoForm.controls['calificacionRollPlay'].setValue(true);
      } else {
        this.seguimientoForm.controls['calificacionRollPlay'].setValue(false);
      }
      this.seguimiento.fechaIngreso = this.seguimiento.fechaIngreso.slice(0, 10);
      this.seguimientoForm.controls['idCoach'].setValue(this.seguimiento.idCoach);
      this.seguimientoForm.controls['comentarios'].setValue(this.seguimiento.comentarios);
      this.seguimientoForm.controls['idCapacitacion'].setValue(this.seguimiento.idCapacitacion);
      // this.seguimientoForm.controls['idPrecandidato'].setValue(this.seguimiento.idPrecandidato);
      this.seguimientoForm.controls['fechaIngreso'].setValue(new Date(this.seguimiento.fechaIngreso.replace('-', '/')));
      this.seguimientoForm.controls['idSubarea'].setValue(this.seguimiento.idSubarea);
      // if (this.idTipo === 3) {
        this.seguimientoForm.controls['asistencia'].setValue(this.seguimiento.asistencia);
      // }
      this.feIngreso = this.seguimiento.fechaIngreso;
    });
  }

  updateSeguimiento() {
    this.inactivo = true;
    if (this.idTipo !== 3 && this.feIngreso !== moment(this.seguimientoForm.controls['fechaIngreso'].value).format('YYYY-MM-DD')) {
        this.seguimientoForm.controls['asistencia'].setValue(null);
      }
    let fecha: any;
    fecha = moment(this.seguimientoForm.value.fechaIngreso).format('YYYY-MM-DD');
    let fechaIngreso: any;
    fechaIngreso = Date.parse(fecha + 'T01:00');
        this.seguimientoService.update(this.idSeguimiento, this.seguimientoForm.value, fechaIngreso).subscribe(
          result => {
          },
          error => { },
          () => {
            this.seguimientoForm.reset();
            this.ref.close();
            // window.location.reload();
            this.router.navigate(['/modulos/rrhh/seguimiento-practico']);
            if (this.idTipo === 2) {
              this.seguimientoService.postSocketRolplay(this.seguimientoForm.value).subscribe(x => {});
              this.router.navigate(['/modulos/rrhh/seguimiento']);
            }else if (this.idTipo === 3) {
              this.seguimientoService.postSocketAignaciones(this.seguimientoForm.value).subscribe(x => {});
              this.router.navigate(['/modulos/rrhh/seguimiento-practico']);
            }
          },
        );
  }

  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    this.nombreCompleto = this.nombre + ' ' + this.apellidoPaterno + ' ' + this.apellidoMaterno;
    this.calificacionRollplay();
    this.getSubarea();
    this.getReclutador();
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getSeguimientoById(this.idSeguimiento);
    }
    this.seguimientoForm = this.fb.group({
      'calificacionRollPlay': new FormControl('', Validators.required),
      'comentarios': new FormControl('', Validators.required),
      'idCapacitacion': this.idCapacitacion,
      // 'idPrecandidato': this.idPrecandidato,
      'idCoach': new FormControl('', Validators.required),
      'fechaIngreso': new FormControl('', Validators.required),
      // 'idEtapa': 5,
      'idSubarea': new FormControl('', Validators.required),
      'asistencia': null,
      });

  }

}
