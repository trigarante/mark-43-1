import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TurnoEmpleado, TurnoEmpleadoData} from '../../interfaces/catalogos/turno-empleado';


@Injectable()
export class TurnoEmpleadoService extends TurnoEmpleadoData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<TurnoEmpleado[]> {
    return this.http.get<TurnoEmpleado[]>(this.baseURL + 'v1/turnos');
  }

  post(turnoEmpleado: TurnoEmpleado): Observable<TurnoEmpleado> {
    return this.http.post<TurnoEmpleado>(this.baseURL + 'v1/turnos', turnoEmpleado);
  }

  getTurnoEmpleadoById(idTurno): Observable<TurnoEmpleado> {
    return this.http.get<TurnoEmpleado>(this.baseURL + 'v1/turnos' + idTurno);
  }

  put(idTurno, turnoEmpleado: TurnoEmpleado): Observable<TurnoEmpleado> {
    return this.http.put< TurnoEmpleado>(this.baseURL + 'v1/turnos' + idTurno + '/'
      + sessionStorage.getItem('Empleado'), turnoEmpleado);
  }
}
