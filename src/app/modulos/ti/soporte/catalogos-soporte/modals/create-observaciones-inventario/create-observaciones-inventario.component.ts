import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ObservacionesInventario,
  ObservacionesInventarioData} from '../../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {NbDialogRef} from '@nebular/theme';
import {ObservacionesInventarioComponent} from '../../observaciones-inventario/observaciones-inventario.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-create-observaciones-inventario',
  templateUrl: './create-observaciones-inventario.component.html',
  styleUrls: ['./create-observaciones-inventario.component.scss'],
})
export class CreateObservacionesInventarioComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idObservacionesInventario: number;
  observacionesInventarioCreateForm: FormGroup;
  submitted: boolean;
  observacionesInventario: ObservacionesInventario;

  constructor(private observacionesInventarioService: ObservacionesInventarioData, private fb: FormBuilder,
              protected ref: NbDialogRef<ObservacionesInventarioComponent>,
              private router: Router) {
  }

  updateObservacionesInventario() {
    this.observacionesInventarioService.put(this.idObservacionesInventario, this.observacionesInventarioCreateForm.value)
      .subscribe(result => {
        this.observacionesInventarioService.postSocket({status: 'ok'}).subscribe(x => {});
      this.observacionesInventarioCreateForm.reset();
      this.ref.close();
      // window.location.reload();
        this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/observaciones-inventario']);
    });
  }

  guardarObservacionesInventario() {
    if (this.observacionesInventarioCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.observacionesInventarioService.post(this.observacionesInventarioCreateForm.value).subscribe((result) => {
      this.observacionesInventarioService.postSocket({status: 'ok'}).subscribe(x => {});
      this.observacionesInventarioCreateForm.reset();
      this.ref.close();
      window.location.reload();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/observaciones-inventario']);
    });
  }

  getObservacionesInventarioById() {
    this.observacionesInventarioService.getObservacionesInventario(this.idObservacionesInventario).subscribe(data => {
      this.observacionesInventario = data;
      this.observacionesInventarioCreateForm.controls['observaciones'].setValue(this.observacionesInventario.observaciones);
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getObservacionesInventarioById();
    }
    this.observacionesInventarioCreateForm = this.fb.group({
      'observaciones': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
