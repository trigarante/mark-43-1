import {Injectable} from '@angular/core';
import {CampanaMarketing, CampanaMarketingData} from '../../interfaces/marketing/campana';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CampanaMarketingService extends CampanaMarketingData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<CampanaMarketing[]> {
    return this.http.get<CampanaMarketing[]>(this.baseURL + 'v1/campanas-marketing');
  }

  post(campanaMarketing: CampanaMarketing): Observable<CampanaMarketing> {
    return this.http.post<CampanaMarketing>(this.baseURL + 'v1/campanas-marketing', campanaMarketing);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'campania/saveCampania', json);
  }

  getCampanasMarketingBiId(idCampanaMarketing): Observable<CampanaMarketing> {
    return this.http.get<CampanaMarketing>(this.baseURL + 'v1/campanas-marketing/' + idCampanaMarketing);
  }

  put(idCampanaMarketing, campanaMarketing: CampanaMarketing) {
    return this.http.put<CampanaMarketing>(this.baseURL + 'v1/campanas-marketing/' + idCampanaMarketing + '/'
      + sessionStorage.getItem('Empleado') , campanaMarketing);
  }
}
