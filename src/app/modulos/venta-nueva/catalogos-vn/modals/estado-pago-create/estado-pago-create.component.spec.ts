import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoPagoCreateComponent } from './estado-pago-create.component';

describe('EstadoPagoCreateComponent', () => {
  let component: EstadoPagoCreateComponent;
  let fixture: ComponentFixture<EstadoPagoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoPagoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoPagoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
