import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlujoPolizaCreateComponent } from './flujo-poliza-create.component';

describe('FlujoPolizaCreateComponent', () => {
  let component: FlujoPolizaCreateComponent;
  let fixture: ComponentFixture<FlujoPolizaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlujoPolizaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlujoPolizaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
