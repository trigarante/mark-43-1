import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {Alias, Correo, CorreoData, Domains, UnidadOrganizativa} from '../../interfaces/ti/correo';

@Injectable({
  providedIn: 'root',
})
export class CorreoService extends CorreoData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(tipo): Observable<Correo[]> {
    return this.http.get<Correo[]>( this.baseURL + 'v1/api-admin/get-users/' + tipo );
  }

  getById(tipo: string, id: string): Observable<Correo> {
    return this.http.get<Correo>(this.baseURL + 'v1/api-admin/get-user/' + tipo + '/' + id);
  }

  validarCorreo(tipo, alias) {
    return this.http.get( this.baseURL + 'v1/api-admin/find-alias/' + tipo + '/' + alias);
  }

  enviarByAlias(tipo, jsonCorreo) {
    return this.http.put(this.baseURL + 'v1/api-admin/add-primary-alias/'  + tipo ,  jsonCorreo);
  }
  cambiarAlias (tipo, jsonCorreo) {
    return this.http.put(this.baseURL + 'v1/api-admin/add-alias/' + tipo, jsonCorreo);
  }
  post(tipo, jsonCorreo): Observable<string> {
    return this.http.post<string>(this.baseURL + 'v1/api-admin/create-user/' + tipo, jsonCorreo,
      {headers: null , responseType: 'text' as 'json'});
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'ejecutivo/saveEjecutivo', json);
  }
  postSocketDatosTiUsuarios(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'datostiusuarios/saveDatosTiUsuarios', json);
  }
  getDominios(tipo): Observable<Domains[]> {
    return this.http.get<Domains[]>(this.baseURL + 'v1/api-admin/get-available-domains/' + tipo);
}
  getAlias(tipo, idCorreo): Observable<Alias> {
    return this.http.get<Alias>( this.baseURL + 'v1/api-admin/get-user-aliases/' + tipo + '/' + idCorreo);
  }
  deleteAlias(tipo, id, alias) {
    return this.http.delete( this.baseURL + 'v1/api-admin/delete-alias/' + tipo + '/' + id + '/' + alias);
  }
  getUnidadesOrganizativas(tipo): Observable<UnidadOrganizativa[]> {
    return this.http.get<UnidadOrganizativa[]> (this.baseURL + 'v1/api-admin/unidades-organizativas/' + tipo);
  }
}
