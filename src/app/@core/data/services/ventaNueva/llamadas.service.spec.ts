import { TestBed } from '@angular/core/testing';

import { LlamadasService } from './llamadas.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('LlamadasService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [LlamadasService, HttpClient],
  }));

  it('should be created', () => {
    const service: LlamadasService = TestBed.get(LlamadasService);
    expect(service).toBeTruthy();
  });
});
