import {Injectable} from '@angular/core';
import {ProductoData, ProductoSocios} from '../../interfaces/comerciales/producto-socios';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProductoSociosService extends ProductoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ProductoSocios[]> {
    return this.http.get<ProductoSocios[]>(this.baseURL + 'v1/productos-socio');
  }

  post(productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.post<ProductoSocios>(this.baseURL + 'v1/productos-socio', productoSocios);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'producto/saveProducto', json);
  }

  getProductoSociosById(idProductoSocios): Observable<ProductoSocios> {
    return this.http.get<ProductoSocios>(this.baseURL + 'v1/productos-socio/' + idProductoSocios);
  }

  put(idProductoSocios, productoSocios: ProductoSocios): Observable<ProductoSocios> {
    return this.http.put<ProductoSocios>(this.baseURL + 'v1/productos-socio/' + idProductoSocios + '/'
      + sessionStorage.getItem('Empleado') , productoSocios);
  }
}
