import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BancoComponent} from '../../banco/banco.component';
import {EstadoCivil, EstadoCivilData} from '../../../../../@core/data/interfaces/catalogos/estado-civil';
import {Router} from '@angular/router';
/** Componente del modal del catálogo para "crear estado civil". */
@Component({
  selector: 'ngx-estado-civil-create',
  templateUrl: './estado-civil-create.component.html',
  styleUrls: ['./estado-civil-create.component.scss'],
})
export class EstadoCivilCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idEstadoCivil obtenido. */
  @Input() idEstadoCivil: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un estado civil. */
  estadoCicilCreateForm: FormGroup;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z ]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  estadoCvivil: EstadoCivil;
  estadocivilEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EstadoCivilData} estadoCivilService Servicio del componente estado-civil para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BancoComponent>, private estadoCivilService: EstadoCivilData,
              private router: Router) { }
   /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarEstadoCivil() {
    if (this.estadoCicilCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.estadoCivilService.post(this.estadoCicilCreateForm.value).subscribe((result) => {
      this.estadoCivilService.postSocket(this.estadoCicilCreateForm.value).subscribe(x => {});
      this.estadoCicilCreateForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/estadoCivil']);
      this.estadoCicilCreateForm.reset();
      this.ref.close();
      //window.location.reload();

    });
  }

  /** Función que actualiza la información del estado civil. */
  updateEstacoCivil() {
    this.estadoCivilService.getEstadoCivilById(this.idEstadoCivil).subscribe( data => {
      if(data.activo === 1){
        this.estadoCivilService.put(this.idEstadoCivil, this.estadoCicilCreateForm.value).subscribe( result => {
          this.estadoCivilService.postSocket(this.estadoCicilCreateForm.value).subscribe(x => {});
          this.estadoCicilCreateForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.estadocivilEliminado = true;
      }
    });

  }

  /** Función que obtiene la información del estado civil dependiendo de su ID. */
  getEstadoCivilById() {
    this.estadoCivilService.getEstadoCivilById(this.idEstadoCivil).subscribe( data => {
      this.estadoCvivil = data;
      this.estadoCicilCreateForm.controls['descripcion'].setValue(this.estadoCvivil.descripcion);
      this.estadoCicilCreateForm.controls['activo'].setValue(this.estadoCvivil.activo);
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que da de baja un estado civil, cambiando el atributo activo a 0. */
  /*bajaEstadoCivil() {
    this.estadoCicilCreateForm.controls['activo'].setValue(0);
    this.estadoCivilService.put(this.idEstadoCivil, this.estadoCicilCreateForm.value).subscribe( result => {
      this.estadoCicilCreateForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEstadoCivilById();
    } else if (this.idTipo === 3) {
      this.getEstadoCivilById();
    }
    this.estadoCicilCreateForm = this.fb.group( {
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
