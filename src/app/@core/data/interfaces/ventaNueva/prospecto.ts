import {Observable} from 'rxjs';
export interface Prospecto {
  id: number;
  numero: string;
  correo: string;
  sexo: number;
  edad: number;
  fechaCreacion: string;
  nombre: string;
}

export abstract class ProspectoData {
  abstract get(): Observable<Prospecto[]>;

  abstract post(prospecto): Observable<Prospecto>;

  abstract getProspectoById(idProspecto): Observable<Prospecto>;

  abstract put(idProspecto, prospecto): Observable<Prospecto>;

  abstract validaCorreo(correo): Observable<Prospecto>;
}
