import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {GenerarUrlComponent} from './generar-url.component';
import {
  MatCardModule, MatChipsModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbAlertModule, NbCardModule, NbDateService, NbDialogRef, NbRadioModule, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {BolsaTrabajoData} from '../../../../@core/data/interfaces/catalogos/bolsa-trabajo';


describe('GenerarUrlComponent', () => {
  let component: GenerarUrlComponent;
  let fixture: ComponentFixture<GenerarUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarUrlComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
        MatChipsModule,
      ],
      providers: [
        VacanteData,
        BolsaTrabajoData,
        NbToastrService,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarUrlComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
