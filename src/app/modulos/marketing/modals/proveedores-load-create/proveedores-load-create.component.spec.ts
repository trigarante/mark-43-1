import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedoresLoadCreateComponent } from './proveedores-load-create.component';

describe('ProveedoresLoadCreateComponent', () => {
  let component: ProveedoresLoadCreateComponent;
  let fixture: ComponentFixture<ProveedoresLoadCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedoresLoadCreateComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedoresLoadCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
