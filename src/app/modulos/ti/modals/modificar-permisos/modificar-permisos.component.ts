import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'ngx-modificar-permisos',
  templateUrl: './modificar-permisos.component.html',
  styleUrls: ['./modificar-permisos.component.scss'],
})
export class ModificarPermisosComponent implements OnInit {
  /** Variable que permite la inicialización del formulario para crear un permiso. */
  permisosForm: FormGroup;
  permisos: string[];
  @Input() idGrupo: number;

  constructor( private dialogoService: NbDialogService, protected ref: NbDialogRef<ModificarPermisosComponent> ,
               private fb: FormBuilder) {
    // se inicializa el formulario
    this.permisosForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'descripcion': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'subareaSelected' : {},
      'puestoSelected' : {},
      'descargarPNC': false,
      'descargarPNE': false,
      'ACTIVOPAGOS': false, 'ACTIVONOMINA': false, 'ACTIVOPOLIZAS': false, 'ACTIVOREPORTES': false, 'ACTIVOUSUARIOS': false,
      'ACTIVOVACANTES': false, 'ACTIVOCANDIDATO': false, 'ACTIVOCATALOGOS': false, 'ACTIVOCONTACTOS': false, 'ACTIVOEMPLEADOS': false,
      'ACTIVOSINIESTRO': false, 'ACTIVOINVENTARIOS': false, 'ACTIVOPREEMPLEADOS': false, 'ACTIVOSEGUIMIENTO': false,
      'ACTIVOAUTORIZACION': false, 'ACTIVOCAPACITACION': false, 'ACTIVOCARGAPOLIZAS': false, 'ACTIVOBAJAEMPLEADOS': false,
      'ACTIVOPRECANDIDATOS': false, 'ACTIVOTRANFERENCIAS': false, 'ACTIVOPENALIZACIONES': false, 'ACTIVOVERIFICACIONES': false,
      'ACTIVOAGENTESEXTERNOS': false, 'ACTIVOENDOSOSCANCELACIONES': false, 'ACTIVOTRANFERENCIASMASIVAS' : false,
      'ESCRITURAPAGOS': false, 'ESCRITURANOMINA': false, 'ESCRITURAPOLIZAS': false, 'ESCRITURAREPORTES': false, 'ESCRITURAUSUARIOS': false,
      'ESCRITURAVACANTES': false, 'ESCRITURACANDIDATO': false, 'ESCRITURACATALOGOS': false, 'ESCRITURACONTACTOS': false,
      'ESCRITURAEMPLEADOS': false, 'ESCRITURASINIESTRO': false, 'ESCRITURAINVENTARIOS': false, 'ESCRITURAPREEMPLEADOS': false,
      'ESCRITURASEGUIMIENTO': false, 'ESCRITURAAUTORIZACION': false, 'ESCRITURACAPACITACION': false, 'ESCRITURACARGAPOLIZAS': false,
      'ESCRITURABAJAEMPLEADOS': false, 'ESCRITURAPRECANDIDATOS': false, 'ESCRITURATRANFERENCIAS': false, 'ESCRITURAPENALIZACIONES': false,
      'ESCRITURAVERIFICACIONES': false, 'ESCRITURAAGENTESEXTERNOS': false,
      'ESCRITURAENDOSOSCANCELACIONES': false, 'ESCRITURATRANFERENCIASMASIVAS' : false,
    });
    this.permisos = ['PAGOS', 'NOMINA', 'POLIZAS', 'REPORTES', 'USUARIOS', 'VACANTES', 'CANDIDATO',
      'CATALOGOS', 'CONTACTOS', 'EMPLEADOS', 'SINIESTRO', 'INVENTARIOS', 'PREEMPLEADOS',
      'SEGUIMIENTO', 'AUTORIZACION', 'CAPACITACION', 'CARGAPOLIZAS', 'BAJAEMPLEADOS',
      'PRECANDIDATOS', 'TRANFERENCIAS', 'PENALIZACIONES', 'VERIFICACIONES', 'AGENTESEXTERNOS',
      'ENDOSOSCANCELACIONES', 'TRANFERENCIASMASIVAS'];
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
  }

}
