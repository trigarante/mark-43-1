import {Observable} from 'rxjs';

export interface Banco {
  id: number;
  nombre: string;
  desripcion: string;
  idEmpresa: number;
  activo: number;
}

export abstract class BancoData {
  abstract get(): Observable<Banco[]>;

  abstract post(banco): Observable<Banco>;
  abstract postSocket(json): Observable<JSON>;

  abstract getBancoById(idBanco): Observable<Banco>;

  abstract put(idBanco, banco): Observable<Banco>;

}
