import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {PuestoTipo, PuestoTipoData} from '../../interfaces/catalogos/puesto-tipo';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class PuestoTipoService extends PuestoTipoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<PuestoTipo[]> {
    return this.http.get<PuestoTipo[]>(this.baseURL + 'v1/tipo-puesto');
  }

  post(puestoTipo: PuestoTipo): Observable<PuestoTipo> {
    return this.http.post<PuestoTipo>(this.baseURL + 'v1/tipo-puesto', puestoTipo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tipopuesto/saveTipoPuesto', json);
  }

  getPuestoTipoById(idPuestoTipo): Observable<PuestoTipo> {
    return this.http.get<PuestoTipo>(this.baseURL + 'v1/tipo-puesto/' + idPuestoTipo);
  }

  put(idPuestoTipo, puestoTipo: PuestoTipo): Observable<PuestoTipo> {
    return this.http.put< PuestoTipo>(this.baseURL + 'v1/tipo-puesto/' + idPuestoTipo + '/'
      + sessionStorage.getItem('Empleado'), puestoTipo);
  }
}
