import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {GuardadoComponent} from '../guardado/guardado.component';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';

@Component({
  selector: 'ngx-agregar-aseguradora',
  templateUrl: './agregar-aseguradora.component.html',
  styleUrls: ['./agregar-aseguradora.component.scss'],
})
export class AgregarAseguradoraComponent implements OnInit {
  @Input() id: number;
  @Input() tiponombre: string;
  agregarAseguradoraForm: FormGroup;
  pagina: Pagina;
  aseguradoras: any;
  numero: number;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<AgregarAseguradoraComponent>,
              private paginaService: PaginasData,  private router: Router, public dialogService: NbDialogService) {
    this.agregarAseguradoraForm = this.fb.group({
      'nombreAseguradora': new FormControl('', Validators.compose([Validators.required])),
    });
  }

  /** Funcion que agrega una nueva aseguradora*/
  guardarAseguradora() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      this.numero = this.aseguradoras[this.tiponombre].aseguradoras.length;
          /** agrega una nueva aseguradora  */
          this.aseguradoras[this.tiponombre].aseguradoras[this.numero]
            = {'nombre': this.agregarAseguradoraForm.value.nombreAseguradora};

      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));

      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.ref.close();
        this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      }).onClose.subscribe(x => {

      });
    });


  }
  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
  }

}
