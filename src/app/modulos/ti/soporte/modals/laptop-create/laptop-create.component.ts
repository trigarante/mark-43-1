import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Router} from '@angular/router';
import {Laptop, LaptopData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {NbDialogRef} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {Procesador, ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {SelectItem} from 'primeng/api';
import {SedeData, Sede} from '../../../../../@core/data/interfaces/catalogos/sede';
import {TipoSede, TipoSedeData} from "../../../../../@core/data/interfaces/ti/catalogos/tipo-sede";


@Component({
  selector: 'ngx-laptop-create',
  templateUrl: './laptop-create.component.html',
  styleUrls: ['./laptop-create.component.scss'],
})
export class LaptopCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idLaptop: number;

  laptopCreateForm: FormGroup;
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  estadoInventario: any[];
  estado: EstadoInventario[];
  submitted: boolean;
  laptop: Laptop;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  /**Areglo que inicia los valores del Microservicio*/
  procesadores: Procesador[];
  /**Arreglo auxiliar para  marcas*/
  procesador: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<LaptopCreateComponent>, private laptopService: LaptopData,
              private observacionesInventarioService: ObservacionesInventarioData, private router: Router,
              private estadoInventarioService: EstadoInventarioData,
              private marcaService: MarcaSoporteData,
              private procesadorService: ProcesadorSoporteData, private tipoSedeService: TipoSedeData,
  ) { }

  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getLaptopById() {
    this.laptopService.getLaptopById(this.idLaptop).subscribe(data => {
      this.laptop = data;
      this.laptopCreateForm.controls['numeroSerie'].setValue(this.laptop.numeroSerie);
      this.laptopCreateForm.controls['idProcesador'].setValue(this.laptop.idProcesador);
      this.laptopCreateForm.controls['hdd'].setValue(this.laptop.hdd);
      this.laptopCreateForm.controls['ram'].setValue(this.laptop.ram);
      this.laptopCreateForm.controls['idMarca'].setValue(this.laptop.idMarca);
      this.laptopCreateForm.controls['modelo'].setValue(this.laptop.modelo);
      this.laptopCreateForm.controls['idEstadoInventario'].setValue(this.laptop.idEstadoInventario);
      this.laptopCreateForm.controls['idObservacionesInventario'].setValue(this.laptop.idObservacionesInventario);
      this.laptopCreateForm.controls['comentarios'].setValue(this.laptop.comentarios);
      this.laptopCreateForm.controls['idEmpleado'].setValue(this.laptop.idEmpleado);
      this.laptopCreateForm.controls['idTipoSede'].setValue(this.laptop.idTipoSede);
      this.laptopCreateForm.controls['origenRecurso'].setValue(this.laptop.origenRecurso);
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  getProcesador() {
    this.procesadorService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.procesador = [];
      this.procesadores = data;
      for (let i = 0; i < this.procesadores.length; i++) {
        this.procesador.push({'label': this.procesadores[i].nombreProcesador, 'value': this.procesadores[i].id});
      }
    });
  }

  guardarLaptop() {
    if (this.laptopCreateForm.invalid) {
      return;
    }

    this.submitted = true;
    this.laptopService.post(this.laptopCreateForm.value).subscribe((result) => {
      this.laptopCreateForm.reset();
      this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'laptop' ]);
    });
  }

  updateLaptop() {
    this.laptopService.put(this.idLaptop, this.laptopCreateForm.value).subscribe(result => {
      this.laptopCreateForm.reset();
      this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'laptop' ]);
    });
  }

  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'PROPIO', value: 'PROPIO'});
    this.origenEquipos.push({label: 'RENTA', value: 'RENTA'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getLaptopById();
    }
    this.getMarca();
    this.getProcesador();
    this.getObservacionInventario();
    this.getEstadoInventario();
    this.getOrigen();
    this.getSedes();
    this.laptopCreateForm = this.fb.group({
      'numeroSerie': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'modelo': new FormControl('', Validators.compose([Validators.required])),
      'idProcesador': new FormControl('', Validators.compose([Validators.required])),
      'hdd': new FormControl('', Validators.compose([Validators.required])),
      'ram': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede' : new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });
  }

}
