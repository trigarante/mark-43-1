import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BolsaTrabajo, BolsaTrabajoData} from '../../../../@core/data/interfaces/catalogos/bolsa-trabajo';
import {Vacante, VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {map} from 'rxjs/operators';

/** Componente del modal "generar url" */
@Component({
  selector: 'ngx-generar-url',
  templateUrl: './generar-url.component.html',
  styleUrls: ['./generar-url.component.scss'],
})
export class GenerarUrlComponent implements OnInit {
  /**Variable que lamacena el idVacante para generar url*/
  @Input () idVacante;
  /** Variable que permite la inicialización del formulario para generar la URL. */
  generarUrlForm: FormGroup;
  /** Cadena para almacenar la URL. */
  url: string;
  /** Arreglo que se inicializa con los valores del campo bolsa-trabajo desde el Microservicio. */
  bolsasTrabajo: BolsaTrabajo[];
  /** Arreglo auxiliar para bolsasTrabajo. */
  bolsa: any[];
  /** Arreglo que se inicializa con los valores del campo vacante desde el Microservicio. */
  vacantes: Vacante[];
  /** Arreglo auxiliar para bolsasTrabajo. */
  vacante: any[];
/**Variable que muestra el mensaje de URL al generarla*/
  listo: boolean;
  /**Ocultar input url si no se ha seleccionado Bolsa de trabajo**/
  linkMostrar = false;
  /**
   * El método constructor manda a llamar Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<GenerarUrlComponent>} ref Referencia al diálogo (modal).
   * @param bolsaTrabajoService Servicio del componente bolsa-trabajo para poder invocar los métodos.
   * @param vacantesService Servicio del componente vacante para poder invocar los métodos.
   */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<GenerarUrlComponent>, protected bolsaTrabajoService: BolsaTrabajoData,
              protected  vacantesService: VacanteData) {
    this.generarUrlForm = this.fb.group({
      'idBolsaTrabajo': new FormControl('', Validators.required),
    });
  }

  /** Función para obtener la bolsa de trabajo, dichos valores se almacenan en el arreglo "bolsasTrabajo". */
  getBolsaTrabajo() {
    this.bolsaTrabajoService.getBolsaTrabajo().pipe(map(result => {
      return result.filter( data => data.activo === 1);
    })).subscribe(data => {
      this.bolsasTrabajo = data;
      this.bolsa = [];
      for (let i = 0; i < this.bolsasTrabajo.length; i++) {
        this.bolsa.push({'label': this.bolsasTrabajo[i].nombre, 'value': this.bolsasTrabajo[i].id});
      }
      //
    });
  }

  /** Función para obtener las vacantes disponibles y los datos los guarda en el arreglo "vacantes" */
  getVacantes() {
    this.vacantesService.get().pipe(map(result => this.vacantes = result )).subscribe(data => {
      this.vacante = [];
      for (let i = 0; i < this.vacantes.length; i++) {
        this.vacante.push({'label': this.vacantes[i].nombre, 'value': this.vacantes[i].id});
      }
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  verificar () {
    if (!this.generarUrlForm.valid && !this.generarUrlForm.dirty) {
      return true;
    }
  }

  /** Función que genera la URL para la vacante y bolsa de trabajo en cuestión. */
  generarUrl() {
    const hash = btoa('{"idReclutador":' +  window.sessionStorage.getItem('Empleado')
      + ',"idBolsaTrabajo":' + this.generarUrlForm.value.idBolsaTrabajo +
      ',"idVacante":' + this.idVacante + '}');
    this.url = 'https://bolsa-de-trabajo.ahorraseguros.mx?id=' + hash;
    // this.url = 'http://localhost:2500?id=' + hash;
    this.listo = true;
  }

  /** Función que manda a llamar a los getters al inicializar el componente. */
  ngOnInit() {
    console.log(this.idVacante);
    this.getBolsaTrabajo();
    this.getVacantes();
  }

}
