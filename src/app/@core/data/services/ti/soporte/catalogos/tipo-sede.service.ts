import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {TipoSede, TipoSedeData} from '../../../../interfaces/ti/catalogos/tipo-sede';
import {Observable} from 'rxjs';

@Injectable()
export class TipoSedeService extends TipoSedeData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoSede[]> {
    return this.http.get<TipoSede[]>(this.baseURL + 'v1/tipo-sede');
  }

  post(tipoSede: TipoSede): Observable<TipoSede> {
    return this.http.post<TipoSede>(this.baseURL + 'v1/tipo-sede', tipoSede);
  }

  // postSocket(json): Observable<JSON> {
  //   return this.http.post<JSON>(this.socketURL + 'tipo-sede/saveBanco', json);
  // }

  getTipoSedeById(idTipoSede): Observable<TipoSede> {
    return this.http.get<TipoSede>(this.baseURL + 'v1/tipo-sede/' + idTipoSede);
  }

  put(idTipoSede, tipoSede: TipoSede): Observable<TipoSede> {
    return this.http.put<TipoSede>(this.baseURL + 'v1/tipo-sede/' + idTipoSede + '/'
      + sessionStorage.getItem('Empleado'), tipoSede);
  }
}
