import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Empleados, EmpleadosData} from '../../../../../@core/data/interfaces/rrhh/empleados';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import swal from 'sweetalert';
import {Cpu, CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {Monitor, MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Laptop, LaptopData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {Diadema, DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {Tablet, TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Teclado, TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Mouse, MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {ActivatedRoute, Router} from '@angular/router';
import {NbDialogRef} from '@nebular/theme';
import {Switchs, SwitchsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {TarjetaRed, TarjetaRedData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIp, TelefonoIpData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {Racks, RacksData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanel, PatchPanelData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRack, CharolasRackData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {Servidores, ServidoresData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {
  DiscoDuroExterno,
  DiscoDuroExternoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {
  DiscoDuroInterno,
  DiscoDuroInternoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBit, ChromeBitData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {Aps, ApsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {
  Biometricos,
  BiometricosData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {Dvr, DvrData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {Camaras, CamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {
  BarrasMulticontacto,
  BarrasMulticontactoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {
  FuenteCamaras,
  FuenteCamarasData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';




@Component({
  selector: 'ngx-asignar-equipo',
  templateUrl: './asignar-equipo.component.html',
  styleUrls: ['./asignar-equipo.component.scss'],
})
export class AsignarEquipoComponent implements OnInit {
  /** Variable que se obtiene de un componente padre - Inventario, obtiene el id de equipo y el tipo de Equipo*/
  @Input() id: number;
  @Input() tipoEquipos: string;
  /** Variable que inicializa las columnas*/
  cols: any[];
  /** Arreglo que mostrara todos los empleados*/
  empleado: Empleados[];
  /** Variable que permite la inicialización del formulario para inventario . */
  AsignarForm: FormGroup;
  dataSource: any;
  /**arreglo del cpu*/
  cpu: Cpu;
  /**arreglo del monitor*/
  monitor: Monitor;
  /**arreglo del laptop*/
  laptop: Laptop;
  /**arreglo de la diadema*/
  diadema: Diadema;
  /**arreglo del tablet*/
  tablet: Tablet;
  /**arreglo del teclado*/
  teclado: Teclado;
  /**arreglo del mouse*/
  mouse: Mouse;
  /** arreglos del inventario Redes */
  switch: Switchs;
  servidores: Servidores;
  tarjetaRed: TarjetaRed;
  telefonoIp: TelefonoIp;
  racks: Racks;
  patchPanel: PatchPanel;
  charolaRack: CharolasRack;
  /** arregloa para el inventario Soporte */
  ddexterno: DiscoDuroExterno;
  ddinterno: DiscoDuroInterno;
  chromeBit: ChromeBit;
  aps: Aps;
  biometricos: Biometricos;
  dvr: Dvr;
  camara: Camaras;
  barraMulti: BarrasMulticontacto;
  corrienteCam: FuenteCamaras;

  constructor(private empleadosService: EmpleadosData, private fb: FormBuilder, private cpuService: CpuData,
              private monitorService: MonitorDate, private laptopService: LaptopData,
              private diademaService: DiademaData, private tabletService: TabletData,
              private tecladoService: TecladoData, private servidoresService: ServidoresData,
              private mouseService: MouseData, private router: Router, protected ref: NbDialogRef<AsignarEquipoComponent>,
              private switchsService: SwitchsData, private tarjetaRedService: TarjetaRedData,
              private telefonoIdService: TelefonoIpData, private racksService: RacksData,
              private patchPanelService: PatchPanelData, private charolasRackService: CharolasRackData,
              private discoduroexternoService: DiscoDuroExternoData, public discodurointernoService: DiscoDuroInternoData,
              private chromeBitServices: ChromeBitData, private apsService: ApsData, private biometricosService: BiometricosData,
              private dvrService: DvrData, private camaraService: CamarasData, private barraMultiService: BarrasMulticontactoData,
              private fuenteCorrienteService: FuenteCamarasData) {
    this.AsignarForm = this.fb.group({
      'nombrecontrol': new FormControl(),
    });
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** funcion que manda a llamar el servicio que mostrara los empleados*/
  getEmpleados() {
    this.empleadosService.get().pipe(map(result => {
      return result.filter(data => data.idUsuario !== null);
    })).subscribe(data => {
      this.empleado = data;
      this.dataSource = new MatTableDataSource(this.empleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  /**obtiene la informacion de cpu por id*/
  getCpuById(id) {
    this.cpuService.getCpuById(id).subscribe(data => {
      this.cpu = data;
    });
  }
  /**obtiene la informacion de monitor por id*/
  getMonitorById(id) {
    this.monitorService.getMonitorById(id).subscribe(data => {
    this.monitor = data;
  });
  }
  /**obtiene la informacion de laptop por id*/
  getLaptopById(id) {
    this.laptopService.getLaptopById(id).subscribe(data => {
      this.laptop = data;
    });
  }
  /**obtiene la informacion de tablet por id*/
  getTabletById(id) {
    this.tabletService.getTabletById(id).subscribe(data => {
      this.tablet = data;
    });
  }
  /**obtiene la informacion de teclado por id*/
  getTecladoById(id) {
    this.tecladoService.getTecladoById(id).subscribe(data => {
      this.teclado = data;
    });
  }
  /**obtiene la informacion de mouse por id*/
  getMouseById(id) {
    this.mouseService.getMouseById(id).subscribe(data => {
      this.mouse = data;
    });
  }
  /**obtiene la informacion de diameda por id*/
  getDiademaById(id) {
    this.diademaService.getDiademaById(id).subscribe(data => {
      this.diadema = data;
    });
  }
                                                                        /** Inventario Redes*/
  getSwitchById(id) {
    this.switchsService.getSwitchsById(id).subscribe(data => {
      this.switch = data;
    });
  }
  getServidoresById(id) {
    this.servidoresService.getServidoresById(id).subscribe(data => {
      this.servidores = data;
    });
  }
  getTarjetaRedById(id) {
    this.tarjetaRedService.getTarjetaRedById(id).subscribe(data => {
      this.tarjetaRed = data;
    });
  }
  getTelefonosIpById(id) {
    this.telefonoIdService.getTelefonoIpById(id).subscribe(data => {
      this.telefonoIp = data;
    });
  }
  getRackById(id) {
    this.racksService.getRacksById(id).subscribe(data => {
      this.racks = data;
    });
  }
  getPatchPanelById(id) {
    this.patchPanelService.getPatchPanelById(id).subscribe(data => {
      this.patchPanel = data;
    });
  }
  getCharolasRackById(id) {
    this.charolasRackService.getCharolasRackById(id).subscribe(data => {
      this.charolaRack = data;
    });
  }
                                                                  /** Inventario Soporte*/
  getDiscoDuroExternoById(id) {
    this.discoduroexternoService.getDiscoExternoById(id).subscribe(data => {
      this.ddexterno = data;
    });
  }
  getDiscoDuroInternoById(id) {
    this.discodurointernoService.getDiscoInternoById(id).subscribe(data => {
      this.ddinterno = data;
    });
  }
  getChromeBitById(id) {
    this.chromeBitServices.getChromeBit(id).subscribe(data => {
      this.chromeBit = data;
    });
  }
  getApsIpById(id) {
    this.apsService.getApsById(id).subscribe(data => {
      this.aps = data;
    });
  }
  getBiometricById(id) {
    this.biometricosService.getBiometricosById(id).subscribe(data => {
      this.biometricos = data;
    });
  }
  getDvrById(id) {
    this.dvrService.getDvrById(id).subscribe(data => {
      this.dvr = data;
    });
  }
  getCamarasById(id) {
    this.camaraService.getCamarasById(id).subscribe(data => {
      this.camara = data;
    });
  }
  getBarrasMulticontactoById(id) {
    this.barraMultiService.getBarrasMulticontactoById(id).subscribe(data => {
      this.barraMulti = data;
    });
  }
  getCorrienteCamaraById(id) {
    this.fuenteCorrienteService.getFuenteCamaraById(id).subscribe(data => {
      this.corrienteCam = data;
    });
  }
  /** Funcion que asigna un equipo a un empleado, asignandole un idEmpleado  Equipo*/
  asignarEquipo(idEmpleado) {
    switch (this.tipoEquipos) {
      case 'cpu':
    this.getCpuById(this.id);
        break;
      case 'monitor':
        this.getMonitorById(this.id);
        break;
      case 'laptop':
        this.getLaptopById(this.id);
        break;
      case 'tablet':
        this.getTabletById(this.id);
        break;
      case 'teclado':
        this.getTecladoById(this.id);
        break;
      case 'mouse':
        this.getMouseById(this.id);
        break;
      case 'diadema':
        this.getDiademaById(this.id);
        break;
                             /** Inventario Redes*/
      case 'switch':
        this.getSwitchById(this.id);
        break;
      case 'servidores':
        this.getServidoresById(this.id);
        break;
      case 'tarjeta de red':
        this.getTarjetaRedById(this.id);
        break;
      case 'telefonos ip':
        this.getTelefonosIpById(this.id);
        break;
      case 'rack':
        this.getRackById(this.id);
        break;
      case 'patch panel':
        this.getPatchPanelById(this.id);
        break;
      case 'charolas rack':
        this.getCharolasRackById(this.id);
        break;
                                /** Inventario Soporte*/
      case 'discoduroexterno':
        this.getDiscoDuroExternoById(this.id);
        break;
      case 'disco duro externo':
        this.getDiscoDuroExternoById(this.id);
        break;
      case 'disco duro interno':
        this.getDiscoDuroInternoById(this.id);
        break;
      case 'chrome bit':
        this.getChromeBitById(this.id);
        break;
      case 'aps':
        this.getApsIpById(this.id);
        break;
      case 'biometricos':
        this.getBiometricById(this.id);
        break;
      case 'dvr':
        this.getDvrById(this.id);
        break;
      case 'camara':
        this.getCamarasById(this.id);
        break;
      case 'barra-multicontacto':
        this.getBarrasMulticontactoById(this.id);
        break;
      case 'corriente-camara':
        this.getCorrienteCamaraById(this.id);
        break;
      default:
        break;
    }
    swal({
      title: '¿Deseas asignar este equipo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Asignar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {

      if (valor) {
        switch (this.tipoEquipos) {
          case 'cpu':
        this.cpu['idEmpleado'] = idEmpleado;
        this.cpuService.put(this.id, this.cpu).subscribe( result => {
          this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
           });
            break;
          case 'monitor':
            this.monitor['idEmpleado'] = idEmpleado;
            this.monitorService.put(this.id, this.monitor).subscribe(result => {
              this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
            });

            break;
          case 'laptop':
            this.laptop['idEmpleado'] = idEmpleado;
            this.laptopService.put(this.id, this.laptop).subscribe(result => {
              this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'tablet':
            this.tablet['idEmpleado'] = idEmpleado;
            this.tabletService.put(this.id, this.tablet).subscribe(result => {
              this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'teclado':
            this.teclado.idEmpleado = idEmpleado;
            this.tecladoService.put(this.id, this.teclado).subscribe(result => {
              this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'mouse':
            this.mouse['idEmpleado'] = idEmpleado;
            this.mouseService.put(this.id, this.mouse).subscribe(result => {
              this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'diadema':
            this.diadema['idEmpleado'] = idEmpleado;
            this.diadema['fechaSalida'] = new Date();
            this.diademaService.put(this.id, this.diadema).subscribe(result => {
              this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'switch':
            this.switch['idEmpleado'] = idEmpleado;
            this.switchsService.put(this.id, this.switch).subscribe(result => {
              this.switchsService.postSocket({status: 'ok'}).subscribe(x => {});
           });
            break;
          case 'servidores':
            this.servidores['idEmpleado'] = idEmpleado;
            this.servidoresService.put(this.id, this.servidores).subscribe(result => {
              this.servidoresService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'tarjeta de red':
            this.tarjetaRed['idEmpleado'] = idEmpleado;
            this.tarjetaRedService.put(this.id, this.tarjetaRed).subscribe(result => {
              this.tarjetaRedService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'telefonos ip':
            this.telefonoIp['idEmpleado'] = idEmpleado;
            this.telefonoIdService.put(this.id, this.telefonoIp).subscribe(result => {
              this.telefonoIdService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'rack':
            this.racks['idEmpleado'] = idEmpleado;
            this.racksService.put(this.id, this.racks).subscribe(result => {
              this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'patch panel':
            this.patchPanel['idEmpleado'] = idEmpleado;
            this.patchPanelService.put(this.id, this.patchPanel).subscribe(result => {
              this.patchPanelService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'charolas rack':
            this.charolaRack['idEmpleado'] = idEmpleado;
            this.charolasRackService.put(this.id, this.charolaRack).subscribe(result => {
              this.charolasRackService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'discoduroexterno':
            this.ddexterno['idEmpleado'] = idEmpleado;
            this.discoduroexternoService.put(this.id, this.ddexterno).subscribe(result => {
              this.discoduroexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'disco duro externo':
            this.ddexterno['idEmpleado'] = idEmpleado;
            this.discoduroexternoService.put(this.id, this.ddexterno).subscribe(result => {
              this.discoduroexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'disco duro interno':
            this.ddinterno['idEmpleado'] = idEmpleado;
            this.discodurointernoService.put(this.id, this.ddinterno).subscribe(result => {
              this.discodurointernoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'chrome bit':
            this.chromeBit['idEmpleado'] = idEmpleado;
            this.chromeBitServices.put(this.id, this.chromeBit).subscribe(result => {
              this.chromeBitServices.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'aps':
            this.aps['idEmpleado'] = idEmpleado;
            this.apsService.put(this.id, this.aps).subscribe(result => {
              this.apsService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'biometricos':
            this.biometricos['idEmpleado'] = idEmpleado;
            this.biometricosService.put(this.id, this.biometricos).subscribe(result => {
              this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'dvr':
            this.dvr['idEmpleado'] = idEmpleado;
            this.dvrService.put(this.id, this.dvr).subscribe(result => {
              this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'camara':
            this.camara['idEmpleado'] = idEmpleado;
            this.camaraService.put(this.id, this.camara).subscribe(result => {
              this.camaraService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'barra-multicontacto':
            this.barraMulti['idEmpleado'] = idEmpleado;
            this.barraMultiService.put(this.id, this.barraMulti).subscribe(result => {
              this.barraMultiService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'corriente-camara':
            this.corrienteCam['idEmpleado'] = idEmpleado;
            this.fuenteCorrienteService.put(this.id, this.corrienteCam).subscribe(result => {
              this.fuenteCorrienteService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          default:
            break;
        }
        swal('¡El equipo se ha asignado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          switch (this.tipoEquipos) {
            case 'cpu':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'cpu' ]);
              this.cerrarVentana();
              break;
            case 'monitor':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'monitor' ]);
              this.cerrarVentana();
              break;
            case 'laptop':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'laptop' ]);
              this.cerrarVentana();
              break;
            case 'tablet':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'tablet' ]);
              this.cerrarVentana();
              break;
            case 'teclado':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'teclado' ]);
              this.cerrarVentana();
              break;
            case 'mouse':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'mouse' ]);
              this.cerrarVentana();
              break;
            case 'diadema':
              this.router.navigate(['/modulos/ti/soporte/inventario', 'diadema' ]);
              this.cerrarVentana();
              break;
            case 'switch':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'switch']);
              this.cerrarVentana();
              break;
            case 'servidores':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'servidores']);
              this.cerrarVentana();
              break;
            case 'tarjeta de red':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'tarjetadered']);
              this.cerrarVentana();
              break;
            case 'telefonos ip':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'telefonosip']);
              this.cerrarVentana();
              break;
            case 'rack':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'rack']);
              this.cerrarVentana();
              break;
            case 'patch panel':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'patch panel']);
              this.cerrarVentana();
              break;
            case 'charolas rack':
              this.router.navigate(['/modulos/ti/soporte/inventarioRedes', 'charolas rack']);
              this.cerrarVentana();
              break;
            case 'discoduroexterno':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'discoduroexterno']);
              this.cerrarVentana();
              break;
            case 'disco duro externo':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'disco duro externo']);
              this.cerrarVentana();
              break;
            case 'disco duro interno':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'disco duro interno']);
              this.cerrarVentana();
              break;
            case 'chrome bit':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'chromebit']);
              this.cerrarVentana();
              break;
            case 'aps':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'aps']);
              this.cerrarVentana();
              break;
            case 'biometricos':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'biometricos']);
              this.cerrarVentana();
              break;
            case 'dvr':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'dvr']);
              this.cerrarVentana();
              break;
            case 'camara':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'camara']);
              this.cerrarVentana();
              break;
            case 'barra-multicontacto':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'barra-multicontacto']);
              this.cerrarVentana();
              break;
            case 'corriente-camara':
              this.router.navigate(['/modulos/ti/soporte/inventarioSoporte', 'corriente-camara']);
              this.cerrarVentana();
              break;
            default:
              break;
          }

        }));
      }
    });

  }
  cerrarVentana() {

    this.ref.close();

  }
  ngOnInit() {
    this.getEmpleados();
    this.cols = ['nombre', 'correo', 'sede', 'area' , 'subarea', 'asignar Equipo'];

  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
