import { Component, OnInit } from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-save-modificado',
  templateUrl: './save-modificado.component.html',
  styleUrls: ['./save-modificado.component.scss'],
})
export class SaveModificadoComponent implements OnInit {

  constructor(protected ref: NbDialogRef<SaveModificadoComponent>, private router: Router) { }
  dismiss() {

    this.ref.close();
     // this.router.navigate(['/modulos/ti/ejecutivo/']);
  }
  ngOnInit() {
  }

}
