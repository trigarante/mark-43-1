import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AgendarCitaComponent} from './agendar-cita.component';
import {
  MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {
  NbAlertModule,
  NbCardModule,
  NbCheckboxModule,
  NbDateService,
  NbDialogRef,
  NbRadioModule,
  NbToastrService,
} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';


describe('AgendarCitaComponent', () => {
  let component: AgendarCitaComponent;
  let fixture: ComponentFixture<AgendarCitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendarCitaComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
        MatChipsModule,
        NbCheckboxModule,
        MatCheckboxModule,
      ],
      providers: [
        SolicitudesData,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide: Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(AgendarCitaComponent);
    /*component = fixture.componentInstance;*/
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    /*expect(component).toBeTruthy();*/
  });
});
