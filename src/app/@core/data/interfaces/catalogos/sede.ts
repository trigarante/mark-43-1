import {Observable} from 'rxjs';

export interface Sede {
  id: number;
  idEmpresa: number;
  idPais: number;
  nombre: string;
  cp: string;
  colonia: string;
  calle: string;
  numero: string;
  codigo: string;
}

export abstract class SedeData {
  abstract get(): Observable<Sede[]>;

  abstract post(sede): Observable<Sede>;

  abstract getSedeById(idSede): Observable<Sede>;

  abstract put(idSede, sede): Observable<Sede>;
}
