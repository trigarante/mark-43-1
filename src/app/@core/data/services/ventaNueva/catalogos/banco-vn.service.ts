import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {BancoVn, BancoVnData} from '../../../interfaces/ventaNueva/catalogos/banco-vn';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class BancoVnService extends BancoVnData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<BancoVn[]> {
    return this.http.get<BancoVn[]>(this.baseURL + 'v1/bancos');
  }

  getBancoById(id): Observable<BancoVn> {
    return this.http.get<BancoVn>(this.baseURL + 'v1/bancos' + id);
  }

  post(banco): Observable<BancoVn> {
    return this.http.post<BancoVn>(this.baseURL + 'v1/bancos', banco);
  }

  put(id, banco): Observable<BancoVn> {
    return this.http.put<BancoVn>(this.baseURL + 'v1/bancos' + id, banco);
  }
}
