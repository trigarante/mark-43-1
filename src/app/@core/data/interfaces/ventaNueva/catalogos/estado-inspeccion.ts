import {Observable} from 'rxjs';

export interface EstadoInspeccion {
  id: number;
  estado: string;
  activo: number;
}

export abstract class EstadoInspeccionData {

  abstract get(): Observable<EstadoInspeccion[]>;

  abstract post(estadoInspeccion): Observable<EstadoInspeccion>;

  abstract getEstadoInsoeccionById(idEstadoInspeccion): Observable<EstadoInspeccion>;

  abstract put(idEstadoInspeccion, estadoInspeccion): Observable<EstadoInspeccion>;

}
