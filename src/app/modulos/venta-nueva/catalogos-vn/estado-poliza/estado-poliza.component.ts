import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {
  EstadoPolizaVn,
  EstadoPolizaVnData,
} from '../../../../@core/data/interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {EstadoPolizaCreateComponent} from '../modals/estado-poliza-create/estado-poliza-create.component';
import {environment} from "../../../../../environments/environment";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-estado-poliza',
  templateUrl: './estado-poliza.component.html',
  styleUrls: ['./estado-poliza.component.scss'],
})
export class EstadoPolizaComponent implements OnInit {

  cols: any[];


  descripciones: EstadoPolizaVn[];

  // baja descripcion
  estadoPoliza: EstadoPolizaVn;

  dataSource: any;
  // sockets
  private stompClient = null;

  constructor(private descService: EstadoPolizaVnData,
              private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vnestadopoliza');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelVnEstadoPoliza', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEstadoPoliza(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  ngOnInit() {
    this.connect();
    this.getEstadoPoliza(0);
    this.cols = ['estado', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  crearEstado() {
    this.dialogoService.open(EstadoPolizaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => {
      this.getEstadoPoliza(1);
    });
  }



  showToast() {
    this.toastrService.success('DATOS ACTUALIZADOS!', `Estado Póliza`);
  }

  // Obtener estadoPolizas
  getEstadoPoliza(mensajeAct) {
    this.descService.get().pipe(map(resultado => {
      return resultado.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.descripciones = [];
      this.descripciones = data;
      this.dataSource = new MatTableDataSource(this.descripciones); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }


  // Obtener ID de estadoPoliza
  getEstadoPolizaById(idEstado) {
    this.descService.getEstadoPolizaById(idEstado).subscribe(data => {
      this.estadoPoliza = data;
    });
  }

  // Actualizar información del estadoPoliza
  updateEstadoPoliza(idEstado) {
    this.dialogoService.open(EstadoPolizaCreateComponent, {
      context: {
        idTipo: 2,
        idEstado: idEstado,
      },
    }).onClose.subscribe(x => {
      this.getEstadoPoliza(1);
    });
  }

  // Dar de baja estadoPoliza
  bajaEstado(idEstado) {
    this.getEstadoPolizaById(idEstado);
    swal({
      title: '¿Deseas dar de baja este tipo estado póliza?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.estadoPoliza['activo'] = 0;
        this.descService.put(idEstado, this.estadoPoliza).subscribe(result => {
          this.descService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡Este estado póliza se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getEstadoPoliza(0);
        }));
      }
    });

  }
}
