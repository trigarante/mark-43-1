import {Component, OnInit, ViewChild} from '@angular/core';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {NbDialogService} from '@nebular/theme';
import {CreateObservacionesInventarioComponent} from '../modals/create-observaciones-inventario/create-observaciones-inventario.component';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-observaciones-inventario',
  templateUrl: './observaciones-inventario.component.html',
  styleUrls: ['./observaciones-inventario.component.scss'],
})
export class ObservacionesInventarioComponent implements OnInit {
  cols: any[];
  observacionesInventario: ObservacionesInventario[];
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private observacionesINventarioService: ObservacionesInventarioData, private dialogService: NbDialogService,
              private router: Router) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    /**---------------------------------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'observacionesinventario');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      _this.stompClient.subscribe('/task/panelObservacionesInventario', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getObservacionesInventario();
        }, 500);

      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  /** crea una observacion*/
  crearObservacionesInventario() {
    this.dialogService.open(CreateObservacionesInventarioComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getObservacionesInventario();


    });
  }
  /**Muestra las Observaciones*/
  getObservacionesInventario() {
    this.observacionesINventarioService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.observacionesInventario = data;
      this.dataSource = new MatTableDataSource(this.observacionesInventario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });  }
    /** modifica las Observaciones*/
  updateObservacionesInventario(idObservacionesInventario) {
    this.dialogService.open( CreateObservacionesInventarioComponent, {
      context: {
        idTipo: 2,
        idObservacionesInventario: idObservacionesInventario,
      },
    }).onClose.subscribe(x => {
      this.getObservacionesInventario();


    });
  }

  ngOnInit() {
    this.connect();
    this.getObservacionesInventario();
    this.cols = [ 'observaciones', 'acciones' ];
    // this.cols = [
    //   {field: 'observaciones', header: 'Observaciones'},
    //   {field: 'acciones', header: 'Acciones'},
    //
    // ];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.observacionesInventario.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.observacionesInventario.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
