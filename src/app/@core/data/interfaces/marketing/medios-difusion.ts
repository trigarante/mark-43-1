import {Observable} from 'rxjs';

export interface MediosDifusion {
  idProveedor: number;
  idExterno?: number;
  descripcion: string;
  regla: string;
  presupuesto: string;
  idCampana: number;
  activo: number;
  nombre?: string;
  campana?: string;
}

export abstract class MediosDifusionData {
  abstract get(): Observable<MediosDifusion[]>;

  abstract post(mediosDifusion): Observable<MediosDifusion>;
  abstract postSocket(json): Observable<JSON>;

  abstract getMediosDifusionById(idMediosDifusion): Observable<MediosDifusion>;

  abstract put(idMediosDifusion, mediosDifusion): Observable<MediosDifusion>;
}
