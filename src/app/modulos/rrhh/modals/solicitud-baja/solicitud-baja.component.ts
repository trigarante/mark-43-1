import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {Solicitudes, SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-solicitud-baja',
  templateUrl: './solicitud-baja.component.html',
  styleUrls: ['./solicitud-baja.component.scss'],
})
export class SolicitudBajaComponent implements OnInit {

  /** Variable que almacena el id de la solicitud obtenida. */
  @Input() idSolicitud: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  crearSolicitudForm: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  solicitudes: Solicitudes;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SolicitudBajaComponent>, private solicitudesService: SolicitudesData) { }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que obtiene el listado de documentos dependiendo del idSolicitud. */
  getSolicitudById() {
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe(data => {
      this.solicitudes = data;
    });
  }

  /** Función que da de baja una solicitud, cambiando su atributo "estado" a 0. */
  bajaSolicitud() {
    /*this.solicitudesService.bajaSolicitud(this.idSolicitud).subscribe( result => {
      this.ref.close();
      window.location.reload();
    });*/
  }

  ngOnInit() {
    this.getSolicitudById();
    this.crearSolicitudForm = this.fb.group({
      'id': this.idSolicitud,
    });
  }
}
