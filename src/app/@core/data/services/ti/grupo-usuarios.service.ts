import { Injectable } from '@angular/core';
import {GrupoUsuarios, GrupoUsuariosData} from '../../interfaces/ti/grupoUsuarios';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GrupoUsuariosService extends  GrupoUsuariosData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<GrupoUsuarios[]> {
    return this.http.get<GrupoUsuarios[]>(this.baseURL + 'v1/grupos' );
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'grupopermisos/saveGrupoPermisos', json); //
  }
  post(jsonGrupoUsuario): Observable<GrupoUsuarios> {
    return this.http.post<GrupoUsuarios>(this.baseURL + 'v1/grupos', jsonGrupoUsuario);
  }
  getByID(idGrupo): Observable<GrupoUsuarios> {
    return this.http.get<GrupoUsuarios>(this.baseURL + 'v1/grupos/' + idGrupo  );
  }
  put(idgrupo, jsonGrupoUsuario): Observable<GrupoUsuarios> {
    return this.http.put<GrupoUsuarios>(this.baseURL + 'v1/grupos/' + idgrupo + '/'
      + sessionStorage.getItem('Empleado'), jsonGrupoUsuario);
  }
}
