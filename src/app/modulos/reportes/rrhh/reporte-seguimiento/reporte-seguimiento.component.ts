import {Component, ViewEncapsulation, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from 'd3-format';
import * as d3Inter from 'd3-interpolate';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import 'd3-transition';
import * as moment from 'moment';
import {SeguimientoService} from '../../../../@core/data/services/rrhh/seguimiento.service';
import {Seguimiento} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

@Component({
  selector: 'ngx-reporte-seguimiento',
  encapsulation: ViewEncapsulation.None,    // Necesaria para que se vea chidito. :v
  templateUrl: './reporte-seguimiento.component.html',
  styleUrls: ['./reporte-seguimiento.component.scss'],
  providers: [SeguimientoService],
})
export class ReporteSeguimientoComponent implements OnInit {
  title = 'Prueba';
  seguimiento = [];
  flares: any;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;
  private maxRadius: number;
  x;
  y;
  partition;
  formatNumber;
  root;
  private arc: any;
  private color: any;
  private svg: any;
  datas: any;
  cols: any[];
  etapas: any[];
  dates: any;
  label = 'TOTAL EN SEGUIMIENTO:'
  rangeDates: Date[];
  dataSource: any;
  rollPlay: any[]; // *********************************************************************************

  constructor(private seguimientoService: SeguimientoService) {
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    // this.radius = Math.min(this.width, this.height) / 2;
    this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
    // this.cols = [
    //  {field: 'nombre', header: 'Candidato'},
    //  {field: 'usuario', header: 'Coach'},
    //  {field: 'nombreCampana', header: 'Subárea'},
    //  {field: 'rollplay[calificacionRollplay]', header: 'Roll Play'},
    // ];
    this.cols = ['nombre', 'sede', 'nombreCampana', 'calificacionRollplay']
    this.etapas = ['Precandidato', 'Candidato', 'Empleado', 'Capacitación', 'Seguimiento 1', 'Seguimiento 2'];
    this.rollPlay = ['Reprobado', 'Aprobado'];
    this.rangeDates = [
      new Date(2018, 11, 1, 0, 0),
      new Date(),
    ];
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.initSvg();
    this.getSeguimiento(null);
  }
  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  private initSvg() {
    const colors = ['#2d3a85', '#ff1456', '#006ebb'];
    this.formatNumber = d3Format.format(',d');
    this.color = d3Scale.scaleOrdinal(colors);
    // this.color = d3Scale.scaleOrdinal(d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition = d3Partition.partition();

    this.arc = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }

  getSeguimiento(rangeData: Date[]) {
    this.seguimientoService.get()
      .subscribe(data => {
        if (rangeData == null) {
          this.seguimiento = data;
          this.dates = this.seguimiento.length;
          this.drawPie(this.seguimiento);
        } else {
          this.seguimiento = [];

          // console.log(' FORMATO rangeData:: ' + moment(rangeData[1]).valueOf());
          // console.log('Fecha de servicio:: ' + moment(data[0].fechaCreacion).valueOf());

          for (let i = 0; i < data.length; i++) {
            // this.dates = data[i].fechaIngreso;
            if (moment(data[i].fechaIngreso).valueOf() >= moment(rangeData[0]).valueOf()
              && moment(data[i].fechaIngreso).valueOf() <= moment(rangeData[1]).valueOf()) {
              this.seguimiento.push(data[i]);
            }
          }

          this.removeSvg();
          this.drawPie(this.seguimiento);
        }
      });
  }

  private drawPie(data) {
    console.log(data);
    this.root = d3Partition.hierarchy(this.dataTransform(data));
    this.root.sum(d => d.size);
    this.root.each(d => d.current = d);
    this.svg = d3.select('#graph')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn());
    const slice = this.svg.selectAll('g.slice')
      .data(this.partition(this.root).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn(d);
      });
    this.datas = this.dataFilter({data: this.flares});
    this.dataSource = new MatTableDataSource(this.dataFilter({data: this.flares}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.8 : 0.6)
      .attr('d', this.arc);

    newSlice.append('path')
      .attr('class', 'hidden-arc')
      .attr('id', (_, i) => 'hiddenArc' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', '#ffffff');

    return this.dataSource;
  }

  private dataTransform(data: Seguimiento[]) {
    const candidatos = [];

    //  const coach1 = [];
    //  const coach1Qua = [];
    //  const coach1QuaAprobado = [];
    //  const coach1QuaReprobado = [];
    //  const coach1Gnp = [];
    //  const coach1GnpAprobado = [];
    //  const coach1GnpReprobado = [];
    //  const coach2 = [];
    //  const coach2Qua = [];
    //  const coach2QuaAprobado = [];
    //  const coach2QuaReprobado = [];
    //  const coach2Gnp = [];
    //  const coach2GnpAprobado = [];
    //  const coach2GnpReprobado = [];
    //  const coach3 = [];
    //  const coach3Qua = [];
    //  const coach3QuaAprobado = [];
    //  const coach3QuaReprobado = [];
    //  const coach3Gnp = [];
    //  const coach3GnpAprobado = [];
    //  const coach3GnpReprobado = [];
    //  const coach4 = [];
    //  const coach4Qua = [];
    //  const coach4QuaAprobado = [];
    //  const coach4QuaReprobado = [];
    //  const coach4Gnp = [];
    //  const coach4GnpAprobado = [];
    //  const coach4GnpReprobado = [];
    //  const coach5 = [];
    //  const coach5Qua = [];
    //  const coach5QuaAprobado = [];
    //  const coach5QuaReprobado = [];
    //  const coach5Gnp = [];
    //  const coach5GnpAprobado = [];
    //  const coach5GnpReprobado = [];
    //  const coach6 = [];
    //  const coach6Qua = [];
    //  const coach6QuaAprobado = [];
    //  const coach6QuaReprobado = [];
    //  const coach6Gnp = [];
    //  const coach6GnpAprobado = [];
    //  const coach6GnpReprobado = [];

    const qualitas = [];
    const qualitasAprobado = [];
    const qualitasReprobado = [];
    const gnp = [];
    const gnpAprobado = [];
    const gnpReprobado = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idEtapa >= 3) {
        candidatos.push(data[i]);
        if (data[i].nombreSubarea === 'QUALITAS') {      // Qualitas
          qualitas.push(data[i]);
          if (data[i].calificacionRollplay === 1) {
            qualitasAprobado.push(data[i]);
          } else {
            qualitasReprobado.push(data[i]);
          }
        } else if (data[i].nombreSubarea === 'GNP') {     // GNP
          gnp.push(data[i]);
          if (data[i].calificacionRollplay) {
            gnpAprobado.push(data[i]);
          } else {
            gnpReprobado.push(data[i]);
          }
        }
      }
    }

    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].idEtapa >= 3) {
    //     candidatos.push(data[i]);
    //     if (data[i].idCoach === 1) {      // Ejecutivo sin usuario
    //       coach1.push(data[i]);
    //       if (data[i].idSubarea === 1) {      // Qualitas
    //         coach1Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach1QuaAprobado.push(data[i]);
    //         } else {
    //           coach1QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach1Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach1GnpAprobado.push(data[i]);
    //         } else {
    //           coach1GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 2) {      // Admin
    //       coach2.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach2Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach2QuaAprobado.push(data[i]);
    //         } else {
    //           coach2QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach2Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach2GnpAprobado.push(data[i]);
    //         } else {
    //           coach2GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 3) {      // EjecutivoWS
    //       coach3.push(data[i]);
    //       if (data[i].idSubarea === 1) {      // Qualitas
    //         coach3Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach3QuaAprobado.push(data[i]);
    //         } else {
    //           coach3QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach3Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach3GnpAprobado.push(data[i]);
    //         } else {
    //           coach3GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 4) {      // Programación
    //       coach4.push(data[i]);
    //       console.log(data[i].idSubarea);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach4Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach4QuaAprobado.push(data[i]);
    //         } else {
    //           coach4QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach4Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach4GnpAprobado.push(data[i]);
    //         } else {
    //           coach4GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 5) {      // ventaleads@trigarante.com
    //       coach5.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach5Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach5QuaAprobado.push(data[i]);
    //         } else {
    //           coach5QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach5Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach5GnpAprobado.push(data[i]);
    //         } else {
    //           coach5GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 6) {      // prueba@trigarante.com
    //       coach6.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach6Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach6QuaAprobado.push(data[i]);
    //         } else {
    //           coach6QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach6Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach6GnpAprobado.push(data[i]);
    //         } else {
    //           coach6GnpReprobado.push(data[i]);
    //         }
    //       }
    //     }
    //   }
    // }

    this.flares = {
      name: 'Total de Seguimiento: ' + data.length,
      id: 'seguimiento',
      children: [
        {
          name: 'Total de Qualitas: ' + qualitas.length,
          id: 'qualitas',
          children: [
            {
              name: 'Total de Aprobado: ' + qualitasAprobado.length,
              id: 'qualitas.aprobado',
              size: qualitasAprobado.length,
            },
            {
              name: 'Total de Reprobado: ' + qualitasReprobado.length,
              id: 'qualitas.reprobado',
              size: qualitasReprobado.length,
            },
          ],
        },
        {
          name: 'Total de GNP: ' + gnp.length,
          id: 'gnp',
          children: [
            {
              name: 'Total de Aprobado: ' + gnpAprobado.length,
              id: 'gnp.aprobado',
              size: gnpAprobado.length,
            },
            {
              name: 'Total de Reprobado: ' + gnpReprobado.length,
              id: 'gnp.reprobado',
              size: gnpReprobado.length,
            },
          ],
        },
      ],
    };

    // const coa1 = {
    //   name: coach1.length + ' Ejecutivo sin usuario',
    //   id: 'coach1',
    //   children: [
    //     {
    //       name: coach1Qua.length + ' QUALITAS',
    //       id: 'coach1.qua',
    //       children: [
    //         {
    //           name: coach1QuaAprobado.length + ' Aprobado',
    //           id: 'coach1.qua.aprobado',
    //           size: coach1QuaAprobado.length,
    //         },
    //         {
    //           name: coach1QuaReprobado.length + ' Reprobado',
    //           id: 'coach1.qua.reprobado',
    //           size: coach1QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach1Gnp.length + ' GNP',
    //       id: 'coach1.gnp',
    //       children: [
    //         {
    //           name: coach1GnpAprobado.length + ' Aprobado',
    //           id: 'coach1.gnp.aprobado',
    //           size: coach1GnpAprobado.length,
    //         },
    //         {
    //           name: coach1GnpReprobado.length + ' Reprobado',
    //           id: 'coach1.gnp.reprobado',
    //           size: coach1GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };
    //
    // const coa2 = {
    //   name: coach2.length + ' Admin',
    //   id: 'coach2',
    //   children: [
    //     {
    //       name: coach2Qua.length + ' QUALITAS',
    //       id: 'coach2.qua',
    //       children: [
    //         {
    //           name: coach2QuaAprobado.length + ' Aprobado',
    //           id: 'coach2.qua.aprobado',
    //           size: coach2QuaAprobado.length,
    //         },
    //         {
    //           name: coach2QuaReprobado.length + ' Reprobado',
    //           id: 'coach2.qua.reprobado',
    //           size: coach2QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach2Gnp.length + ' GNP',
    //       id: 'coach2.gnp',
    //       children: [
    //         {
    //           name: coach1GnpAprobado.length + ' Aprobado',
    //           id: 'coach2.gnp.aprobado',
    //           size: coach1GnpAprobado.length,
    //         },
    //         {
    //           name: coach1GnpReprobado.length + ' Reprobado',
    //           id: 'coach2.gnp.reprobado',
    //           size: coach1GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };
    //
    // const coa3 = {
    //   name: coach3.length + ' Ejecutivo WS',
    //   id: 'coach3',
    //   children: [
    //     {
    //       name: coach3Qua.length + ' QUALITAS',
    //       id: 'coach3.qua',
    //       children: [
    //         {
    //           name: coach3QuaAprobado.length + ' Aprobado',
    //           id: 'coach3.qua.aprobado',
    //           size: coach3QuaAprobado.length,
    //         },
    //         {
    //           name: coach3QuaReprobado.length + ' Reprobado',
    //           id: 'coach3.qua.reprobado',
    //           size: coach3QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach3Gnp.length + ' GNP',
    //       id: 'coach3.gnp',
    //       children: [
    //         {
    //           name: coach3GnpAprobado.length + ' Aprobado',
    //           id: 'coach3.gnp.aprobado',
    //           size: coach3GnpAprobado.length,
    //         },
    //         {
    //           name: coach3GnpReprobado.length + ' Reprobado',
    //           id: 'coach3.gnp.reprobado',
    //           size: coach3GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };
    //
    // const coa4 = {
    //   name: coach4.length + ' Programación',
    //   id: 'coach4',
    //   children: [
    //     {
    //       name: coach4Qua.length + ' QUALITAS',
    //       id: 'coach4.qua',
    //       children: [
    //         {
    //           name: coach4QuaAprobado.length + ' Aprobado',
    //           id: 'coach4.qua.aprobado',
    //           size: coach4QuaAprobado.length,
    //         },
    //         {
    //           name: coach4QuaReprobado.length + ' Reprobado',
    //           id: 'coach4.qua.reprobado',
    //           size: coach4QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach4Gnp.length + ' GNP',
    //       id: 'coach4.gnp',
    //       children: [
    //         {
    //           name: coach4GnpAprobado.length + ' Aprobado',
    //           id: 'coach4.gnp.aprobado',
    //           size: coach4GnpAprobado.length,
    //         },
    //         {
    //           name: coach4GnpReprobado.length + ' Reprobado',
    //           id: 'coach4.gnp.reprobado',
    //           size: coach4GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };
    //
    // const coa5 = {
    //   name: coach5.length + ' Ventas Leads',
    //   id: 'coach5',
    //   children: [
    //     {
    //       name: coach5Qua.length + ' QUALITAS',
    //       id: 'coach5.qua',
    //       children: [
    //         {
    //           name: coach5QuaAprobado.length + ' Aprobado',
    //           id: 'coach5.qua.aprobado',
    //           size: coach5QuaAprobado.length,
    //         },
    //         {
    //           name: coach5QuaReprobado.length + ' Reprobado',
    //           id: 'coach5.qua.reprobado',
    //           size: coach5QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach5Gnp.length + ' GNP',
    //       id: 'coach5.gnp',
    //       children: [
    //         {
    //           name: coach5GnpAprobado.length + ' Aprobado',
    //           id: 'coach5.gnp.aprobado',
    //           size: coach5GnpAprobado.length,
    //         },
    //         {
    //           name: coach5GnpReprobado.length + ' Reprobado',
    //           id: 'coach5.gnp.reprobado',
    //           size: coach5GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };
    //
    // const coa6 = {
    //   name: coach6.length + ' Prueba Trigarante',
    //   id: 'coach6',
    //   children: [
    //     {
    //       name: coach6Qua.length + ' QUALITAS',
    //       id: 'coach6.qua',
    //       children: [
    //         {
    //           name: coach6QuaAprobado.length + ' Aprobado',
    //           id: 'coach6.qua.aprobado',
    //           size: coach6QuaAprobado.length,
    //         },
    //         {
    //           name: coach6QuaReprobado.length + ' Reprobado',
    //           id: 'coach6.qua.reprobado',
    //           size: coach6QuaReprobado.length,
    //         },
    //       ],
    //     },
    //     {
    //       name: coach6Gnp.length + ' GNP',
    //       id: 'coach6.gnp',
    //       children: [
    //         {
    //           name: coach6GnpAprobado.length + ' Aprobado',
    //           id: 'coach6.gnp.aprobado',
    //           size: coach6GnpAprobado.length,
    //         },
    //         {
    //           name: coach6GnpReprobado.length + ' Reprobado',
    //           id: 'coach6.gnp.reprobado',
    //           size: coach6GnpReprobado.length,
    //         },
    //       ],
    //     },
    //   ],
    // };

    // this.flares.children.push(coa1);
    // this.flares.children.push(coa2);
    // this.flares.children.push(coa3);
    // this.flares.children.push(coa4);
    // this.flares.children.push(coa5);
    // this.flares.children.push(coa6);
    return this.flares;
  }

  private focusOn(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg.transition()
      .duration(750)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc')
      .attrTween('d', d => () => this.arc(d));

    transition.selectAll('path.hidden-arc')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private dataFilter(d: any) {
    const data = this.seguimiento;

    const candidatos = [];

    const coach1 = [];
    const coach1Qua = [];
    const coach1QuaAprobado = [];
    const coach1QuaReprobado = [];
    const coach1Gnp = [];
    const coach1GnpAprobado = [];
    const coach1GnpReprobado = [];
    const coach2 = [];
    const coach2Qua = [];
    const coach2QuaAprobado = [];
    const coach2QuaReprobado = [];
    const coach2Gnp = [];
    const coach2GnpAprobado = [];
    const coach2GnpReprobado = [];
    const coach3 = [];
    const coach3Qua = [];
    const coach3QuaAprobado = [];
    const coach3QuaReprobado = [];
    const coach3Gnp = [];
    const coach3GnpAprobado = [];
    const coach3GnpReprobado = [];
    const coach4 = [];
    const coach4Qua = [];
    const coach4QuaAprobado = [];
    const coach4QuaReprobado = [];
    const coach4Gnp = [];
    const coach4GnpAprobado = [];
    const coach4GnpReprobado = [];
    const coach5 = [];
    const coach5Qua = [];
    const coach5QuaAprobado = [];
    const coach5QuaReprobado = [];
    const coach5Gnp = [];
    const coach5GnpAprobado = [];
    const coach5GnpReprobado = [];
    const coach6 = [];
    const coach6Qua = [];
    const coach6QuaAprobado = [];
    const coach6QuaReprobado = [];
    const coach6Gnp = [];
    const coach6GnpAprobado = [];
    const coach6GnpReprobado = [];

    const qualitas = [];
    const qualitasAprobado = [];
    const qualitasReprobado = [];
    const gnp = [];
    const gnpAprobado = [];
    const gnpReprobado = [];


    for (let i = 0; i < data.length; i++) {
      if (data[i].idEtapa >= 3) {
        candidatos.push(data[i]);
        if (data[i].nombreSubarea === 'QUALITAS') {      // Qualitas
          qualitas.push(data[i]);
          if (data[i].calificacionRollplay === 1) {
            qualitasAprobado.push(data[i]);
          } else {
            qualitasReprobado.push(data[i]);
          }
        } else if (data[i].nombreSubarea === 'GNP') {     // GNP
          gnp.push(data[i]);
          if (data[i].calificacionRollplay) {
            gnpAprobado.push(data[i]);
          } else {
            gnpReprobado.push(data[i]);
          }
        }
      }
    }

    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].idEtapa >= 3) {
    //     candidato.push(data[i]);
    //     if (data[i].idCoach === 1) {      // Ejecutivo sin usuario
    //       coach1.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach1Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach1QuaAprobado.push(data[i]);
    //         } else {
    //           coach1QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach1Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach1GnpAprobado.push(data[i]);
    //         } else {
    //           coach1GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 2) {      // Admin
    //       coach2.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach2Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach2QuaAprobado.push(data[i]);
    //         } else {
    //           coach2QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach2Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach2GnpAprobado.push(data[i]);
    //         } else {
    //           coach2GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 3) {      // EjecutivoWS
    //       coach3.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach3Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach3QuaAprobado.push(data[i]);
    //         } else {
    //           coach3QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach3Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach3GnpAprobado.push(data[i]);
    //         } else {
    //           coach3GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 4) {      // Programación
    //       coach4.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach4Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach4QuaAprobado.push(data[i]);
    //         } else {
    //           coach4QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach4Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach4GnpAprobado.push(data[i]);
    //         } else {
    //           coach4GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 5) {      // ventaleads@trigarante.com
    //       coach5.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach5Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach5QuaAprobado.push(data[i]);
    //         } else {
    //           coach5QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach5Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach5GnpAprobado.push(data[i]);
    //         } else {
    //           coach5GnpReprobado.push(data[i]);
    //         }
    //       }
    //     } else if (data[i].idCoach === 6) {      // prueba@trigarante.com
    //       coach6.push(data[i]);
    //       if (data[i].nombreCampana === 'QUALITAS') {      // Qualitas
    //         coach6Qua.push(data[i]);
    //         if (data[i].calificacionRollplay === 1) {
    //           coach6QuaAprobado.push(data[i]);
    //         } else {
    //           coach6QuaReprobado.push(data[i]);
    //         }
    //       } else if (data[i].nombreCampana === 'GNP') {     // GNP
    //         coach6Gnp.push(data[i]);
    //         if (data[i].calificacionRollplay) {
    //           coach6GnpAprobado.push(data[i]);
    //         } else {
    //           coach6GnpReprobado.push(data[i]);
    //         }
    //       }
    //     }
    //   }
    // }

    // if (d.data.id === 'seguimiento') {
    //   return data;
    // } else if (d.data.id === 'coach1') {      /******** Ejecutivos sin usuario *********/
    //   return coach1;
    // } else if (d.data.id === 'coach1.qua') {
    //   return coach1Qua;
    // } else if (d.data.id === 'coach1.qua.aprobado') {
    //   return coach1QuaAprobado;
    // } else if (d.data.id === 'coach1.qua.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach1.gnp') {
    //   return coach1Qua;
    // } else if (d.data.id === 'coach1.gnp.aprobado') {
    //   return coach1QuaAprobado;
    // } else if (d.data.id === 'coach1.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach2') {      /******** Admin *********/
    //   return coach2;
    // } else if (d.data.id === 'coach2.qua') {
    //   return coach2Qua;
    // } else if (d.data.id === 'coach2.qua.aprobado') {
    //   return coach2QuaAprobado;
    // } else if (d.data.id === 'coach2.qua.reprobado') {
    //   return coach2QuaReprobado;
    // } else if (d.data.id === 'coach2.gnp') {
    //   return coach2Qua;
    // } else if (d.data.id === 'coach2.gnp.aprobado') {
    //   return coach2QuaAprobado;
    // } else if (d.data.id === 'coach2.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach3') {      /******** Ejecutivos WS *********/
    //   return coach3;
    // } else if (d.data.id === 'coach3.qua') {
    //   return coach3Qua;
    // } else if (d.data.id === 'coach3.qua.aprobado') {
    //   return coach3QuaAprobado;
    // } else if (d.data.id === 'coach3.qua.reprobado') {
    //   return coach3QuaReprobado;
    // } else if (d.data.id === 'coach3.gnp') {
    //   return coach3Qua;
    // } else if (d.data.id === 'coach3.gnp.aprobado') {
    //   return coach3QuaAprobado;
    // } else if (d.data.id === 'coach3.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach4') {      /******** Programación *********/
    //   return coach4;
    // } else if (d.data.id === 'coach4.qua') {
    //   return coach4Qua;
    // } else if (d.data.id === 'coach4.qua.aprobado') {
    //   return coach4QuaAprobado;
    // } else if (d.data.id === 'coach4.qua.reprobado') {
    //   return coach4QuaReprobado;
    // } else if (d.data.id === 'coach4.gnp') {
    //   return coach4Qua;
    // } else if (d.data.id === 'coach4.gnp.aprobado') {
    //   return coach4QuaAprobado;
    // } else if (d.data.id === 'coach4.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach5') {      /******** Ventas Leads *********/
    //   return coach5;
    // } else if (d.data.id === 'coach5.qua') {
    //   return coach5Qua;
    // } else if (d.data.id === 'coach5.qua.aprobado') {
    //   return coach5QuaAprobado;
    // } else if (d.data.id === 'coach5.qua.reprobado') {
    //   return coach5QuaReprobado;
    // } else if (d.data.id === 'coach5.gnp') {
    //   return coach5Qua;
    // } else if (d.data.id === 'coach5.gnp.aprobado') {
    //   return coach5QuaAprobado;
    // } else if (d.data.id === 'coach5.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach6') {      /******** Prueba Trigarante *********/
    //   return coach6;
    // } else if (d.data.id === 'coach6.qua') {
    //   return coach1Qua;
    // } else if (d.data.id === 'coach6.qua.aprobado') {
    //   return coach1QuaAprobado;
    // } else if (d.data.id === 'coach6.qua.reprobado') {
    //   return coach1QuaReprobado;
    // } else if (d.data.id === 'coach6.gnp') {
    //   return coach1Qua;
    // } else if (d.data.id === 'coach6.gnp.aprobado') {
    //   return coach1QuaAprobado;
    // } else if (d.data.id === 'coach6.gnp.reprobado') {
    //   return coach1QuaReprobado;
    // }

    if (d.data.id === 'seguimiento') {
      this.label = 'TOTAL EN SEGUIMINTO:';
      this.dates = data.length;
      return data;
    } else if (d.data.id === 'qualitas') {
      this.label = 'TOTAL EN QUIALITAS:';
      this.dates = qualitas.length;
      return qualitas;
    } else if (d.data.id === 'qualitas.aprobado') {
      this.label = 'TOTAL DE APROBADOS:';
      this.dates = qualitasAprobado.length;
      return qualitasAprobado;
    } else if (d.data.id === 'qualitas.reprobado') {
      this.label = 'TOTAL DE REPROBADOS:';
      this.dates = qualitasReprobado.length;
      return qualitasReprobado;
    } else if (d.data.id === 'gnp') {
      this.label = 'TOTAL EN GNP:';
      this.dates = gnp.length;
      return gnp;
    } else if (d.data.id === 'gnp.aprobado') {
      this.label = 'TOTAL DE APROBADO:';
      this.dates = gnpAprobado.length;
      return gnpAprobado;
    } else if (d.data.id === 'gnp.reprobado') {
      this.label = 'TOTAL DE REPROBADO:';
      this.dates = gnpReprobado.length;
      return gnpReprobado;
    }
  }

  private middleArcLine(d) {
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw

    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path.path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);

    return path.toString();
  }

  private textFits(d) {
    const CHAR_SPACE = 8;
    const deltaAngle = this.x(d.x1) - this.x(d.x0);
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const perimeter = r * deltaAngle;

    return d.data.name.length * CHAR_SPACE < perimeter;
  }

  private removeSvg() {
    this.svg.remove();
  }
}
