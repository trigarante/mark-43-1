import {Component, Input, OnInit} from '@angular/core';

import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {DialogService, DynamicDialogConfig, SelectItem} from 'primeng/api';
import {
  Correo,
  CorreoData,
  Domains,
  UnidadOrganizativa,
} from '../../../../@core/data/interfaces/ti/correo';
import {map} from 'rxjs/operators';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {SaveComponent} from '../save/save.component';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {
  LicenciasDisponibles,
  LicenciasDisponiblesData,
} from '../../../../@core/data/interfaces/ti/licencias-disponibles';
import {SaveModificadoComponent} from '../save-modificado/save-modificado.component';
import {TipoUsuarioData} from '../../../../@core/data/interfaces/catalogos/tipo-usuario';
import {Router} from '@angular/router';


@Component({
  selector: 'ngx-crear-ejecutivo',
  templateUrl: './crear-ejecutivo.component.html',
  styleUrls: ['./crear-ejecutivo.component.scss'],
  providers: [ DialogService, DynamicDialogConfig ],
})
export class CrearEjecutivoComponent implements OnInit {
  /** variable que se puede obtener el empleado*/
  @Input() id: number;
  /** variable para obtener la subarea del modal */
  @Input() subarea: number;
  /** variable que obtiene el area*/
  @Input() tipoarea: number;
  /** variable que obtiene el nombre del area*/
  @Input() area: string;
  /** variable que obtiene el nombre de la subarea*/
  @Input() nombresubarea: string;
  /** Variable que permite la inicialización del formulario para crear un ejecutivo. */
  ejecutivoform: FormGroup;
  /**  se obtenie un arreglo empleado*/
  empleado: Empleados;
  /**  se obtenie un arreglo correo*/
  correo: Correo[];
  /** una variable para utilizar el alias de un ejecutivo*/
  alias: string;
  /** variable para obtener las extensiones*/
  extensiones: any[];
  /** arreglo para obtener las extensiones disponibles*/
  extension: Extension[];
  /** variable para obtener el correo de Ejecutivo*/
  correoEjecutivo: string;
  /** variable para manejar un usuario*/
  usuario: Usuario[];
  /** Arreglo que guarda todos los usuarios disponibles*/
  correosdisponibles: any[];
  /** variable que guarda el correoprimario para remplazar*/
  correoprimario: string;
  /** obtiene un arreglo de id de correos del api */
  idCorreoslista: any[];
  idCorreo: any[];
  idCorreoprincipal: string;
  idUsuario: number;
  /** objeto que guarda los datos del usuario para mandarlo en formato json */
  datosUsuarios: object;
  usuarioput: Usuario;
  datosUsuario: any;
  nombreUnidadesOrganizativas: any[];
  datosGrup: object;

  usuario2: Usuario;
  usuarioput1: Usuario;
  extensionejecutivo: string;
  bandera: boolean = true;
  bandera2: boolean;
  usuario3: Usuario;
  extensionput: Extension;
  extensionput1: Extension;
  extensiondisponible: number;
  idCorreoejecutivo: string;
  private extensionnuevoejecutivo: number;
  extensionbandera: boolean = false;
  datosGrupos: any;
  grupoUsuario: GrupoUsuarios[];
  grupUsuario: any[];
  ejecutivo: RegExp = /^ejecutivo/;
  asesor: RegExp = /^asesor/;
  endosos: RegExp = /^endosos/;
  negocio: RegExp = /^negocio/;
  cobranza: RegExp = /^cobranza/;
  capa: RegExp = /^capacitacion/;
  negociotradicional: RegExp = /^negociotradicional/;
  asesoresindependientes: RegExp = /^asesoresindependientes/;
  /** ejemplo*/

  tipo: string = 'ejecutivo';
  tipo2: string;
  emailejecutivo: string;
  estadoCorreo2: boolean = false;
  banderaemailmodificado: boolean;
  estadoCorreo3: boolean = true;
  estadofinal: string;
  tipodominio: string;
  tipodominioCorreo: string;
  licencias: LicenciasDisponibles[];
  correoAdministrativos: Correo[];
  banderalicencias: boolean;
  tipoLicencia: number;
  estadofindalias: boolean;
  tiposdominios: SelectItem[];
  /** inicializa arrelo de dominios*/
  domainss: any[];
  domains: Domains[];
  banderaadministrativo: boolean;


  /**
   * El método constructor manda a llamar Servicios y Formularios .
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EmpleadoData} empleadoService Servicio del componente empleado para poder invocar los métodos.
   * @param {CorreoData} correoService Servicio del componente correo para poder invocar los métodos.
   * @param {UsuarioData} usuarioService Servicio del componente usuario para poder invocar los métodos.
   * @param {ExtensionesData} usuarioService Servicio del componente extensiones para poder invocar los métodos.
   * @param {NbToastrService} Servicio para poder mostar mensajes
   * */

  constructor( private empleadosService: EmpleadosData, protected ref: NbDialogRef<CrearEjecutivoComponent>,
               public dialogService: NbDialogService, public config: DynamicDialogConfig, private fb: FormBuilder,
               private correoService: CorreoData, private extensionesService: ExtensionData, private usuarioService: UsuarioData
               , private grupoUsuariosService: GrupoUsuariosData, private toastrService: NbToastrService,
               private licenciasService: LicenciasDisponiblesData, private tipoUsuarioService: TipoUsuarioData,
               private router: Router ) { }

  getEmpleados() {
    this.empleadosService.getEmpleadoById(this.id).subscribe(data => {
      this.empleado = data;
      this.alias = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoPaterno.substring(0);
    });
  }

  getExtensiones() {
    this.extensionesService.getExtensionByIdSubarea(this.subarea, 0).pipe(map(data => this.extension = data)).subscribe(data => {
      this.extensiones = [];
      for (let i = 0; i < this.extension.length; i++) {
        this.extensiones.push({'label': this.extension[i].id, 'value': this.extension[i].id});
      }
    });
  }

  /** obtene la extension dada si la bandera es true el correoEjecutivo es ejecutivo mas la extension mas ahorraseguro
   *sii extencionbandera es true , extension ejecutivo obtiene solo la extension
   * @param event
   **/

  getTipo() {
    this.tiposdominios = [];
    this.tipoUsuarioService.get().subscribe(
      data => {
        for (const nombre of data) {
          this.tiposdominios.push({label: nombre.tipo, value: nombre.tipo});
        }
      },
    );
  }
  /** obtiene los dominios disponibles por tipo de correo y obtiene el numero de licencias de google*/
  getadmin(event) {
    this.getcorreosdisponibles(this.subarea);
    this.getUnidadesOrganizativas(event);
    this.correoService.getDominios(event).pipe(map(data => this.domains = data)).subscribe(data => {
      this.domainss = [];
      for (let i = 0; i < this.domains.length; i++) {
        this.domainss.push({'label': this.domains[i].domainName, 'value': this.domains[i].domainName});
      }
    });
    if (event === 'administrativo') {
      this.banderaadministrativo = true;
    } else if (event === 'ejecutivo') {
      this.banderaadministrativo = false;
    }
    this.getcorreosdisponiblesSubareas();
  }

  getextension(event) {
     // si no hay ningun correo disponible
    if (this.bandera2 === true) {
      if (event) {

        if (this.ejecutivoform.value.tiposdominios === 'administrativo') {
          this.correoprimario = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoPaterno.substring(0) + '@'
            + this.ejecutivoform.value.dominio;
          this.correoprimario = this.correoprimario.toLowerCase();

          this.tipodominio = this.ejecutivoform.value.tiposdominios;
          this.getnuevocorreo(this.correoprimario);

        }else if (this.ejecutivoform.value.tiposdominios === 'ejecutivo') {
          this.emailejecutivo = 'ejecutivo' + event + '@' + this.ejecutivoform.value.dominio;
          this.tipodominio = this.ejecutivoform.value.tiposdominios;
          this.getnuevocorreo(this.emailejecutivo);
        }else if (this.ejecutivoform.value.tiposdominios === 'asistencia') {
          this.emailejecutivo = 'asesor' + event + '@' + this.ejecutivoform.value.dominio;
          this.tipodominio = this.ejecutivoform.value.tiposdominios;
          this.getnuevocorreo(this.emailejecutivo);
        }

        /** obtiene si hay numero de licencias disponibles*/

        if (this.tipodominio === 'ejecutivo') {
          this.tipoLicencia = 0;
        }
        if (this.tipodominio === 'asistencia') {
          this.tipoLicencia = 1;
        }
        if (this.tipodominio === 'administrativo') {
          this.tipoLicencia = 2;
        }
        this.licenciasService.get().subscribe(data2 => {
          this.licencias = data2;

          this.correoService.get(this.tipodominio).subscribe(data3 => {
            this.correoAdministrativos = data3;
            if (this.correoAdministrativos.length === this.licencias[this.tipoLicencia].cantidad ||
              this.correoAdministrativos.length >= this.licencias[this.tipoLicencia].cantidad) {
              // checar licencias no hay licencias
              this.banderalicencias = true;
            } else if (this.correoAdministrativos.length <= this.licencias[this.tipoLicencia].cantidad) {
              // licencias disponibles
              this.banderalicencias = false;
            }
          });
        });

        this.extensionnuevoejecutivo = event;
      }
    }
    if (this.extensionbandera === true ) {
      if (event) {

        if (this.ejecutivoform.value.tiposdominios3 === 'administrativo') {
          this.emailejecutivo = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoPaterno.substring(0) + '@'
            + this.ejecutivoform.value.dominio;
          this.emailejecutivo = this.emailejecutivo.toLowerCase();

          this.tipodominio = this.ejecutivoform.value.tiposdominios3;
          this.getnuevocorreo(this.emailejecutivo);

        }else if (this.ejecutivoform.value.tiposdominios3 === 'ejecutivo') {
          this.emailejecutivo = 'ejecutivo' + event + '@' + this.ejecutivoform.value.dominio;
          this.getnuevocorreo(this.emailejecutivo);
          this.extensionejecutivo = event;
          this.tipodominio = this.ejecutivoform.value.tiposdominios3;
        }else if (this.ejecutivoform.value.tiposdominios3 === 'asistencia') {
          this.emailejecutivo = 'asesor' + event + '@' + this.ejecutivoform.value.dominio;
          this.tipodominio = this.ejecutivoform.value.tiposdominios3;
          this.getnuevocorreo(this.emailejecutivo);
        }



      }
    }
  }
  getnuevocorreo( emailejecutivo) {
    this.banderaemailmodificado = true;
      this.correoService.validarCorreo(this.tipodominio, emailejecutivo).subscribe(response => {
        this.estadoCorreo3 = response;
        this.ejecutivoform.controls['emailmodificado'].setValue(emailejecutivo);
        if (this.estadoCorreo3 === true) {
          if (this.ejecutivoform.value.tiposdominios2 === 'administrativo') {
            this.emailejecutivo = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoMaterno.substring(0) + '@'
              + this.ejecutivoform.value.dominio;
             this.emailejecutivo = this.emailejecutivo.toLowerCase();
              this.getnuevocorreo2(this.emailejecutivo);
          }
          this.estadofinal = 'Correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.estadofinal = 'Correo disponible';
          this.correoEjecutivo = emailejecutivo;

        }
      });
    }
  /** solo entrara si la primera ves que comprueba si existe correo da true en el metodo  getnuevocorreo*/
    getnuevocorreo2(emailejecutivo) {
      this.estadoCorreo3 = false;
      this.correoService.validarCorreo(this.tipodominio, emailejecutivo).subscribe(response => {
        this.estadoCorreo3 = response;
        this.ejecutivoform.controls['emailmodificado'].setValue(emailejecutivo);
        if (this.estadoCorreo3 === true) {

          this.estadofinal = 'Correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.estadofinal = 'Correo disponible';
          this.correoEjecutivo = emailejecutivo;

        }
      });
    }
/**En campanas especificas se da un nombre especifico a ejecutivo y ve si esta disponible o no*/
  getcorreofinal(event) {
    if (event) {
      this.estadoCorreo3 = false;
      /** valida correo en subareas especificas y valida en el api de administrativos*/
        this.correoService.validarCorreo(this.tipodominio, event).subscribe(response => {
          this.estadoCorreo3 = response;
          if (this.estadoCorreo3 === true) {
            this.estadofinal = 'Correo ya en uso';
          } else if (this.estadoCorreo3 === false) {
            this.correoEjecutivo = event;
            this.estadofinal = 'Correo disponible';
          }
        });
      }
    }
  /** funcion para conseguir el grupo de permiso*/
  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.grupoUsuario = data;
      this.grupUsuario = [];
      for (let i = 0; i < this.grupoUsuario.length; i++) {
        this.grupUsuario.push({'label': this.grupoUsuario[i].nombre, 'value': this.grupoUsuario[i].id});
      }
    });
  }

  getUnidadesOrganizativas(event) {
    this.correoService.getUnidadesOrganizativas(event).subscribe( data => {
      this.nombreUnidadesOrganizativas = [];
      for (const unidad of data){
        this.nombreUnidadesOrganizativas.push({'label': unidad.name, 'value': unidad.orgUnitPath});
      }
    });
  }
  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  /**   1.- Función para obtener los correosdisponibles por area, dichos valores se almacenan en el arreglo "correosdisponibles"
   * . si no obtiene correos disponibles por area obtiene todos los correos disponibles*/
  getcorreosdisponibles(subarea) {
    // obtiene los correos disponibles
     if (this.ejecutivoform.value.tiposdominios2 === 'asistencia') {
      this.usuarioService.getUsuarioByIdSubarea(subarea, 0).pipe(map(result => {
        return result.filter(data => data.usuario.match(/ahorraasistencia.com/));
      })).subscribe(data => {
        this.usuario = data;
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        for (let i = 0; i < this.usuario.length; i++) {
          // obtiene los nombres de los usuarios --correos
          this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
          // obtiene los id de los corrreos
          this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
          // obtiene el id del usuario
          this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
        }
        if (this.usuario.length !== 0) {
          /** si hay correos disponibles por area pone bandera a true*/
          this.bandera = true;
        } else if (this.usuario.length === 0) {
          // si no hay correos busca correos de todas las areas
          this.bandera = false;
          this.bandera2 = false;
          this.getcorreosdisponiblesSubareas();
        }

      });
     }
     if (this.ejecutivoform.value.tiposdominios2 === 'administrativo') {
      this.usuarioService.getUsuarioByIdSubarea(subarea, 0).pipe(map(result => {
        return result.filter(data => data.usuario.match(/trigarante.com/) || data.usuario.match(/nexosmedia.com/)
          || data.usuario.match(/grupopg.com.mx/) || data.usuario.match(/migoseguros.com/)
          || data.usuario.match(/migoseguros.com.mx/) || data.usuario.match(/grupoahorra.com/)
          || data.usuario.match(/serviciostrigarante.com/));
      })).subscribe(data => {
        this.usuario = data;
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        // console.log(this.idCorreo);
        for (let i = 0; i < this.usuario.length; i++) {
          // obtiene los nombres de los usuarios --correos
          this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
          // obtiene los id de los corrreos
          this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
          // obtiene el id del usuario
          this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
        }
        if (this.usuario.length !== 0) {
          /** si hay correos disponibles por area pone bandera a true*/
          this.bandera = true;
        } else if (this.usuario.length === 0) {
          // si no hay correos busca correos de todas las areas
          this.bandera = false;
          this.bandera2 = false;
          this.getcorreosdisponiblesSubareas();
        }
      });
     }
     if (this.ejecutivoform.value.tiposdominios2 === 'ejecutivo') {
      this.usuarioService.getUsuarioByIdSubarea(subarea, 0).pipe(map(result => {
        return result.filter(data => data.usuario.match(/ahorraseguros.mx/) || data.usuario.match(/ahorracredit.com/)
          || data.usuario.match(/mejorsegurodeauto.mx/) || data.usuario.match(/mejorsegurodevida.mx/)
          || data.usuario.match(/qualitassegurosautos.com/) || data.usuario.match(/inprove.mx/)
          || data.usuario.match(/segurosmotos.com.mx/) || data.usuario.match(/seguroselpotosi.mx/)
          || data.usuario.match(/segurosdetaxi.com.mx/) || data.usuario.match(/multivaseguros.com/)
          || data.usuario.match(/lalatinoseguros.mx/)
          || data.usuario.match(/gnpsegurosautos.com/) || data.usuario.match(/comparaya.mx/));
      })).subscribe(data => {
        this.usuario = data;
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        for (let i = 0; i < this.usuario.length; i++) {
          // obtiene los nombres de los usuarios --correos
          this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
          // obtiene los id de los corrreos
          this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
          // obtiene el id del usuario
          this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
        }
        if (this.usuario.length !== 0) {
          /** si hay correos disponibles por area pone bandera a true*/
          this.bandera = true;
        } else if (this.usuario.length === 0) {
          // si no hay correos busca correos de todas las areas
          this.bandera = false;
          this.bandera2 = false;
          this.getcorreosdisponiblesSubareas();
        }
      });
    }
  }

  /**  Función para obtener los correosdisponibles por todas las areas , dichos valores se almacenan en el arreglo "correosdisponibles"
   * si no obtiene los correosdisponibles, mostrara la seleccion de extensiones disponibles y hara nuevo correo ejecutivo
   */
  getcorreosdisponiblesSubareas() {
    if (this.bandera === false) {
      /** area de ahorraasintencia*
       */
      /** si se selecciono la area de ahorraasistencia se mostraran los correos disponibles que tengan ahorraasistencia*/
      if (this.ejecutivoform.value.tiposdominios3 === 'asistencia') {
        this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
          return result.filter(data => data.usuario.match(/ahorraasistencia.com/));
        })).subscribe(data => {
          this.correosdisponibles = [];
          this.idCorreoslista = [];
          this.usuario = data;

          for (let i = 0; i < this.usuario.length; i++) {
            this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
            this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
            this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
          }

          if (this.usuario.length !== 0) {
            this.extensionbandera = true;
          }
          if (this.usuario.length === 0) {
            this.bandera2 = true; // true si no hay correos disponibles hacer uno nuevo seleccionando las extensiones disponibles
          }
          // console.log('entro a asistencia');
        });
        /** busca todas las subareas administrativo*/
        /** si se selecciono la areas de ventra tradicional se mostraran los correos disponibles que tengan licencias de administrativo*/
      }
      if (this.ejecutivoform.value.tiposdominios3 === 'administrativo') {
        this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
          return result.filter(data => data.usuario.match(/trigarante.com/) || data.usuario.match(/nexosmedia.com/)
            || data.usuario.match(/grupopg.com.mx/) || data.usuario.match(/migoseguros.com/)
            || data.usuario.match(/migoseguros.com.mx/) || data.usuario.match(/grupoahorra.com/)
            || data.usuario.match(/serviciostrigarante.com/));
        })).subscribe(data => {
          this.correosdisponibles = [];
          this.idCorreoslista = [];
          this.usuario = data;

          for (let i = 0; i < this.usuario.length; i++) {
            this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
            this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
            this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
          }

          if (this.usuario.length !== 0) {
            this.extensionbandera = true;
          }
          if (this.usuario.length === 0) {
            this.bandera2 = true; // true si no hay correos disponibles hacer uno nuevo seleccionando las extensiones disponibles
            // console.log('entro a admin');
          }
        });
        /** busca todas las subareas ejecutivo*/
        /** si se selecciono la areas de ejecutivos se mostraran los correos disponibles que tengan licencias de ejecutivos*/
      }
      if (this.ejecutivoform.value.tiposdominios3 === 'ejecutivo') {
        this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
          return result.filter(data => data.usuario.match(/ahorraseguros.mx/) || data.usuario.match(/ahorracredit.com/)
            || data.usuario.match(/mejorsegurodeauto.mx/) || data.usuario.match(/mejorsegurodevida.mx/)
            || data.usuario.match(/qualitassegurosautos.com/) || data.usuario.match(/inprove.mx/)
            || data.usuario.match(/segurosmotos.com.mx/) || data.usuario.match(/seguroselpotosi.mx/)
            || data.usuario.match(/segurosdetaxi.com.mx/) || data.usuario.match(/multivaseguros.com/)
            || data.usuario.match(/lalatinoseguros.mx/)
            || data.usuario.match(/gnpsegurosautos.com/) || data.usuario.match(/comparaya.mx/));
        })).subscribe(data => {
          this.usuario = data;
          this.correosdisponibles = [];
          this.idCorreoslista = [];


          for (let i = 0; i < this.usuario.length; i++) {
            this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
            this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
            // obtiene el id del usuario
            this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
          }
          if (this.usuario.length !== 0) {
            this.extensionbandera = true;
          }
          if (this.usuario.length === 0) {
            this.bandera2 = true; // true si no hay correos disponibles hacer uno nuevo seleccionando las extensiones disponibles
          }
        });

    }
    }
  }

  getcorreoprincial(event) {
    if (event) {
      this.correoprimario = event;
       this.extensionejecutivo = this.correoprimario.substring(9, 13) ;
    }
    /** se obtiene si es tipo administrativo o ejecutivo */
    if (this.correoprimario.match(/trigarante.com/) || this.correoprimario.match(/nexosmedia.com/)
      || this.correoprimario.match(/grupopg.com.mx/) || this.correoprimario.match(/migoseguros.com/)
      || this.correoprimario.match(/migoseguros.com.mx/) || this.correoprimario.match(/grupoahorra.com/)
      || this.correoprimario.match(/serviciostrigarante.com/)) {
      this.tipodominio = 'administrativo';
    } else if (this.correoprimario.match(/ahorraseguros.mx/) || this.correoprimario.match(/ahorracredit.com/)
      || this.correoprimario.match(/mejorsegurodeauto.mx/) || this.correoprimario.match(/mejorsegurodevida.mx/)
      || this.correoprimario.match(/qualitassegurosautos.com/) || this.correoprimario.match(/inprove.mx/)) {
      this.tipodominio = 'ejecutivo';
    } else if (this.correoprimario.match(/ahorraasistencia.com/)) {
      this.tipodominio = 'asistencia';
    }
    if (this.tipodominio === 'ejecutivo') {
      this.estadoCorreo3 = false;
    }
    if (this.tipodominio === 'asistencia') {
      this.estadoCorreo3 = false;
    }
  }

  getcorreoprincialSubareas(event) {
    if (event) {
      this.correoprimario = event;
    }
    /** se obtiene si es tipo administrativo o ejecutivo */
    if (this.correoprimario.match(/trigarante.com/) || this.correoprimario.match(/nexosmedia.com/)
      || this.correoprimario.match(/grupopg.com.mx/) || this.correoprimario.match(/migoseguros.com/)
      || this.correoprimario.match(/migoseguros.com.mx/) || this.correoprimario.match(/grupoahorra.com/)
      || this.correoprimario.match(/serviciostrigarante.com/)) {
      this.tipodominio = 'administrativo';
      this.tipodominioCorreo = 'administrativo';
    } else if (this.correoprimario.match(/ahorraseguros.mx/) || this.correoprimario.match(/ahorracredit.com/)
      || this.correoprimario.match(/mejorsegurodeauto.mx/) || this.correoprimario.match(/comparaya.mx/)
      || this.correoprimario.match(/mejorsegurodevida.mx/) || this.correoprimario.match(/gnpsegurosautos.com/)
      || this.correoprimario.match(/lalatinoseguros.mx/) || this.correoprimario.match(/multivaseguros.com/)
      || this.correoprimario.match(/segurosdetaxi.com.mx/) || this.correoprimario.match(/seguroselpotosi.mx/)
      || this.correoprimario.match(/segurosmotos.com.mx/) || this.correoprimario.match(/ahorraseguros.com/)
      || this.correoprimario.match(/qualitassegurosautos.com/) || this.correoprimario.match(/inprove.mx/)) {
      this.tipodominio = 'ejecutivo';
      this.tipodominioCorreo = 'ejecutivo';
    } else if (this.correoprimario.match(/ahorraasistencia.com/)) {
      this.tipodominio = 'asistencia';
      this.tipodominioCorreo = 'asistencia';
    }
  }

  getdomainss(event) {
    if (event) {
      /** en correo por misma area si se selecciona tipo administrativo da nombre correo por nombre y apellido empleado */
      if (this.ejecutivoform.value.tiposdominios2 === 'administrativo') {
        this.correoEjecutivo = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoPaterno.substring(0) + '@' + event;
        this.correoEjecutivo = this.correoEjecutivo.toLowerCase();

        this.getnuevocorreo(this.correoEjecutivo);

      }
    }
  }
  /** funcion que hace correos ejecutivos cuando hay correos disponibles y guarda la informacion dentro de usuarios*/
  guardarejecutivo() {
// saca el id del usuario del correo primario
    for (let i = 0; i < this.idCorreo.length; i++) {
      if (this.correoprimario === this.idCorreo[i].label) {
        this.idUsuario = this.idCorreo[i].value;
      }
    }
    /**mismo correo de subarea*/
    /** metodo que cambia solo los nombres del usuario, mismo nombre de correo*/
    if (this.extensionbandera === false) {
      /** si es administrativo cambia todos los datos admin*/
      if ( this.ejecutivoform.value.tiposdominios2 === 'administrativo') {

        const objeto = {
          'id': this.idCorreoprincipal, // id del correoprimario
          'givenName': this.empleado.nombre,
          'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
          'email': this.correoEjecutivo, // nombre del correo para remplazar mismo correo que el idprimario
          'orgUnitPath': this.ejecutivoform.value.unidadOrganizativa,
        };
        /** si el usuario selecciona en tipodedominio administrativo, cambiara la variable de administrativo a administrativocorreo
         *
         */
        if (this.ejecutivoform.value.tiposdominios2 === 'administrativo') {
          this.tipodominio = 'administrativo';
        }
        this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => { // nuevo servicio cambia nombre
          this.estadofindalias = response;
        });
      }else {
        /** entra a ejecutivo a ahorraasistencia solo cambia los nombres*/
        const objeto = {
          'id': this.idCorreoprincipal, // id del correoprimario
          'givenName': this.empleado.nombre,
          'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
          'email': this.correoprimario, // nombre del correo para remplazar mismo correo que el idprimario
          'orgUnitPath': this.ejecutivoform.value.unidadOrganizativa,
        };
        this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => { // nuevo servicio cambia nombre
          this.estadofindalias = response;
        });
      }
      this.usuarioService.getUsuarioById(this.idUsuario).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
        // this.datosGrupos = this.usuarioput.grupoUsuariosByIdGrupo;
        /** si es tipo administrativo*/
        if ( this.ejecutivoform.value.tiposdominios2 === 'administrativo') {

          this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.datosUsuario.idExtension};

          this.usuarioput1 = {
            'id': this.usuarioput.id,
            'idCorreo': this.idCorreoprincipal,
            'idGrupo': this.ejecutivoform.value.privilegiocontrol,
            'idTipo': 1,
            'idSubarea': this.subarea,
            'usuario': this.correoEjecutivo,
            'password': 'mark-43',
            'datosUsuario': JSON.stringify(this.datosUsuarios),
            'estado': 1,
            'nombre': this.empleado.nombre + ' ' +  this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
            // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
          };
        }else {
           /** hace si es tipo ejecutivo o ahoraasistencia*/
          this.datosUsuarios = {
            'idCampania': this.subarea, 'idEmpleado': this.empleado.id, 'idExtension': this.datosUsuario.idExtension,
            'idProtocolo': this.datosUsuario.idProtocolo, 'idPerfilMarcacion': this.datosUsuario.idPerfilMarcacion,
          };
          if ( this.ejecutivoform.value.tiposdominios2 === 'ejecutivo') {
            this.usuarioput1 = {
              'id': this.usuarioput.id,
              'idCorreo': this.idCorreoprincipal,
              'idGrupo': this.usuarioput.idGrupo,
              'idTipo': 2,
              'idSubarea': this.subarea,
              'usuario': this.correoprimario,
              'password': 'mark-43',
              'datosUsuario': JSON.stringify(this.datosUsuarios),
              'estado': 1,
              'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
              // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
            };
          } else {
            this.usuarioput1 = {
              'id': this.usuarioput.id,
              'idCorreo': this.idCorreoprincipal,
              'idGrupo': this.usuarioput.idGrupo,
              'idTipo': 3,
              'idSubarea': this.subarea,
              'usuario': this.correoprimario,
              'password': 'mark-43',
              'datosUsuario': JSON.stringify(this.datosUsuarios),
              'estado': 1,
              'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
              // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
            };
          }
        }
        this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(data => {
          this.correoService.postSocket({jeje: 'jeje'}).subscribe(x => {});
          this.usuarioService.postSocketTiUsuarios({status: 'ok'}).subscribe(x => {});
          this.usuario3 = data;
          if (this.usuario3 !== null) {

            // this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
            //   this.extensionput = dataextensiones;
            //   this.extensionput1 = {
            //     'id': this.extensionput.id,
            //     'idSubarea': this.extensionput.idSubarea,
            //     'descrip': this.extensionput.descrip,
            //     'idEstado': 1,
            //   };
            //   this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {
            //   });
            // });
            this.ref.close();
            this.dialogService.open(SaveModificadoComponent, {});
          }// fin del if
        }); // fin del put
      });
    }
    /** cuando se selecciona correos por todas las subareas*/
    if (this.extensionbandera === true) {
      const objeto = {
         'id': this.idCorreoprincipal, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.correoEjecutivo,
        'orgUnitPath': this.ejecutivoform.value.unidadOrganizativa,
      };
      this.correoService.enviarByAlias(this.tipodominioCorreo, objeto).subscribe(response => {
       this.estadofindalias = response;
     });
      // console.log(this.idUsuario);
      this.usuarioService.getUsuarioById(this.idUsuario).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
        // this.datosGrupos = this.usuarioput.grupoUsuariosByIdGrupo;

        if ( this.ejecutivoform.value.tiposdominios3 === 'administrativo') {

          this.datosUsuarios = {
            'idEmpleado': this.empleado.id,
            'idExtension': this.ejecutivoform.value.extensioncontrol,
          };
        }else {
          this.datosUsuarios = {
            'idCampania': this.subarea,
            'idEmpleado': this.empleado.id,
            'idExtension': this.ejecutivoform.value.extensioncontrol,
            'idProtocolo': 1,
            'idPerfilMarcacion': 1,
          };
        }
        if ( this.ejecutivoform.value.tiposdominios3 === 'administrativo') {
          this.usuarioput1 = {
            'id': this.usuarioput.id,
            'idCorreo': this.idCorreoprincipal,
            'idGrupo': this.ejecutivoform.value.privilegiocontrol,
            'idTipo': 1,
            'idSubarea': this.subarea,
            'usuario': this.correoEjecutivo,
            'password': 'mark-43',
            'datosUsuario': JSON.stringify(this.datosUsuarios),
            'estado': 1,
            'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
            // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
          };
        }
        if ( this.ejecutivoform.value.tiposdominios3 === 'ejecutivo') {
          this.usuarioput1 = {
            'id': this.usuarioput.id,
            'idCorreo': this.idCorreoprincipal,
            'idGrupo': this.ejecutivoform.value.privilegiocontrol,
            'idTipo': 2,
            'idSubarea': this.subarea,
            'usuario': this.correoEjecutivo,
            'password': 'mark-43',
            'datosUsuario': JSON.stringify(this.datosUsuarios),
            'estado': 1,
            'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
            // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
          };
        }
        if ( this.ejecutivoform.value.tiposdominios3 === 'asistencia') {
          this.usuarioput1 = {
            'id': this.usuarioput.id,
            'idCorreo': this.idCorreoprincipal,
            'idGrupo': this.ejecutivoform.value.privilegiocontrol,
            'idTipo': 3,
            'idSubarea': this.subarea,
            'usuario': this.correoEjecutivo,
            'password': 'mark-43',
            'datosUsuario': JSON.stringify(this.datosUsuarios),
            'estado': 1,
            'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
            // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
          };
        }
        // console.log('entro1');
        this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(data => {
          this.correoService.postSocket({jeje: 'jeje'}).subscribe(x => {});
          this.usuarioService.postSocketTiUsuarios({status: 'ok'}).subscribe(x => {});
          this.usuario3 = data;
          if (this.usuario3 !== null) {
            // desactiva la extension anterior
            this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
              this.extensionput = dataextensiones;
              this.extensionput1 = {
                'id': this.extensionput.id,
                'idSubarea': this.extensionput.idSubarea,
                'descrip': this.extensionput.descrip,
                'idEstado': 0,
              };
              this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {
              });
            });
            // la nueva extension la pone ocupada
            this.extensionesService.get(this.ejecutivoform.value.extensioncontrol).subscribe(dataextensiones => {
              this.extensionput = dataextensiones;
              this.extensionput1 = {
                'id': this.extensionput.id,
                'idSubarea': this.extensionput.idSubarea,
                'descrip': this.extensionput.descrip,
                'idEstado': 1,
              };
              this.extensionesService.put(this.ejecutivoform.value.extensioncontrol, this.extensionput1).subscribe(result => {
              });
            });
            this.ref.close();
            this.dialogService.open(SaveModificadoComponent, {});
            // this.router.navigate(['/modulos/ti/ejecutivo/']);
          }
        });
      });
    } // fin if del banderaextension
  }// fin de guardar executivo
  /** funciona para hacer nuevos correos de ejecutivos apartir de la extension libre, y los guarda en la tabla executivos*/
  guardarNuevoEjecutivo() {
    let idCorreo: string;
    if (this.banderalicencias === true) {
      this.showToast2('danger');
    }else if (this.banderalicencias === false) {

      const objeto = {
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.correoEjecutivo,
        'orgUnitPath': this.ejecutivoform.value.unidadOrganizativa,
      };

      this.correoService.post(this.tipodominio, objeto).subscribe(
        result => {
            idCorreo = result;
        },
        error => {},
        () => {
          // una ves guardado el correo en el api, busca su id y lo manda a la tabla usuarios
          this.correoService.get(this.tipodominio).subscribe(data => {
            this.correo = data;
            if ( this.ejecutivoform.value.tiposdominios === 'administrativo') {

              this.datosUsuarios = {
                'idEmpleado': this.empleado.id,
                'idExtension': this.ejecutivoform.value.extensioncontrol,
              };
            }else {
              this.datosUsuarios = {
                'idCampania': this.subarea, 'idEmpleado': this.empleado.id, 'idExtension': this.extensionnuevoejecutivo,
                'idProtocolo': 1, 'idPerfilMarcacion': 1,
              };
            }

            if ( this.ejecutivoform.value.tiposdominios === 'administrativo') {
              this.usuario2 = {
                'idCorreo': idCorreo,
                'idGrupo': this.ejecutivoform.value.privilegiocontrol,
                'idTipo': 1,
                'idSubarea': this.subarea,
                'usuario': this.correoEjecutivo,
                'password': 'mark-43',
                'datosUsuario': JSON.stringify(this.datosUsuarios),
                'estado': 1,
                'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
                // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
              };
            }
            if ( this.ejecutivoform.value.tiposdominios === 'ejecutivo') {
              this.usuario2 = {
                'idCorreo': idCorreo,
                'idGrupo': this.ejecutivoform.value.privilegiocontrol,
                'idTipo': 2,
                'idSubarea': this.subarea,
                'usuario': this.correoEjecutivo,
                'password': 'mark-43',
                'datosUsuario': JSON.stringify(this.datosUsuarios),
                'estado': 1,
                'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
                // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
              };
            }
            if ( this.ejecutivoform.value.tiposdominios === 'asistencia') {
              this.usuario2 = {
                'idCorreo': idCorreo,
                'idGrupo': this.ejecutivoform.value.privilegiocontrol,
                'idTipo': 3,
                'idSubarea': this.subarea,
                'usuario': this.correoEjecutivo,
                'password': 'mark-43',
                'datosUsuario': JSON.stringify(this.datosUsuarios),
                'estado': 1,
                'nombre' : this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
                // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
              };
            }
            // guarda en la tabla usuario y modifica el idUsuario de la tabla empleado
            this.usuarioService.postUsuariosidEmpleados(this.usuario2, this.empleado.id).subscribe(data2 => {
              this.usuario3 = data2;
              this.ejecutivoform.reset();
              // si guardo bien modifica al extension a ocupado
              if (this.usuario3 !== null) {
                // cambia el estado a 1 a la extencion dada
                this.extensionesService.get(this.extensionnuevoejecutivo).subscribe(dataextensiones => {
                  this.extensionput = dataextensiones;
                  this.extensionput1 = {
                    'id': this.extensionput.id,
                    'idSubarea': this.extensionput.idSubarea,
                    'descrip': this.extensionput.descrip,
                    'idEstado': 1,
                  };
                  this.extensionesService.put(this.extensionnuevoejecutivo, this.extensionput1).subscribe(result => {
                  });
                });
                this.ref.close();
                this.dialogService.open(SaveComponent, {
                  context: {
                    modo: 1,
                  },
                });
              }
            });
          });
        });

    }
  }
  /** funcion para hacer los correos ejecutivos desde el boton*/
  guardar() {

    // si hay correos disponibles entra a remplazar un correo
    if (this.correosdisponibles.length !== 0) {
      // saca el id del correo que se va a ocupar
      for (let i = 0; i < this.idCorreoslista.length; i++) {
        if (this.correoprimario === this.idCorreoslista[i].label) {
          this.idCorreoprincipal = this.idCorreoslista[i].value;
        }
      }
      if (this.correoprimario) {
        /** si la bandera es false guardara correos modificando el alias*/
        if (this.bandera === false) {
          this.guardarejecutivo();
        } else {
          /** de lo contrario si la bandera es true manda a guardar el mismo correo con diferentes nombres*/
          this.guardarejecutivo();
        }
      } else  this.showToast('danger');
    }
       // si no hay ningun correo disponible manda guardar nuevo ejecutivo
    if (this.correosdisponibles.length === 0) {
      {
        this.guardarNuevoEjecutivo();
      }
    }
  }/**funcion que mostrara un mensaje cuando el usuario no ha seleccionado un correo disponible*/
  showToast(status) {
    this.toastrService.danger('Atencion', `Hay correos disponibles selecciona uno`);
  }
  showToast2(status) {
    this.toastrService.danger('Atencion', `No hay licencias disponibles`);
  }
  ngOnInit() {
    this.getGrupoUsuarios();
    this.getExtensiones();
    this.getEmpleados();
    // this.getcorreosdisponibles(this.subarea);
    this.getcorreosdisponiblesSubareas();
    this.getTipo();
    this.ejecutivoform = this.fb.group({
      'correosdisponible': new FormControl(),
      'correosdisponibleSubareas': new FormControl(),
      'extensioncontrol': new FormControl(),
      'privilegiocontrol': new FormControl(),
      'unidadOrganizativa': new FormControl(),
      'emailmodificado': new FormControl(),
      'tiposdominios': new FormControl(),
      'tiposdominios2': new FormControl(),
      'tiposdominios3': new FormControl(),
      'dominio': new FormControl(),

    });
  }
}
