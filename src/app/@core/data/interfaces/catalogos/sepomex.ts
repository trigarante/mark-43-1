import {Observable} from 'rxjs';

export interface Sepomex {
    idCp: number;
    cp: number;
    asenta: string;
    tipoAsenta: string;
    delMun: string;
    estado: string;
    ciudad: string;
    dCp: number;
    cEdo: number;
    cOficina: number;
    cCP: number;
    cTipoAsenta: number;
    cMnpio: number;
    idAsentaCpcons: number;
    dZona: string;
    cCveCiudad: number;
}

export abstract class SepomexData {
  abstract getColoniaByCp(cp): Observable<Sepomex[]>;
}
