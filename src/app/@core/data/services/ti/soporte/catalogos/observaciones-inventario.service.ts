import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../interfaces/ti/catalogos/observaciones-inventario';

@Injectable()
export class ObservacionesInventarioService extends ObservacionesInventarioData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ObservacionesInventario[]> {
    return this.http.get<ObservacionesInventario[]>(this.baseURL + 'v1/observaciones-inventario');
  }

  post(observacionesInventario: ObservacionesInventario): Observable<ObservacionesInventario> {
    return this.http.post<ObservacionesInventario>(this.baseURL + 'v1/observaciones-inventario', observacionesInventario);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'observacionesinventario/saveObservacionesInventario', json);
  }

  getObservacionesInventario(idObservacionesInventario): Observable<ObservacionesInventario> {
    return this.http.get<ObservacionesInventario>(this.baseURL + 'v1/observaciones-inventario/' + idObservacionesInventario);
  }

  put(idObservacionesInventario, observacionesInventario: ObservacionesInventario): Observable<ObservacionesInventario> {
    return this.http.put<ObservacionesInventario>(this.baseURL + 'v1/observaciones-inventario/' +
      idObservacionesInventario + '/'
      + sessionStorage.getItem('Empleado') , observacionesInventario);
  }
}
