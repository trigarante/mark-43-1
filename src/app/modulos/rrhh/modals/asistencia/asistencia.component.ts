import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDateService, NbDialogRef, NbToastrService} from '@nebular/theme';
import {RadioButton} from 'primeng/primeng';
import {CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import * as moment from 'moment';
// import * as moment from 'moment';
/**import _date = moment.unitOfTime._date;*/
@Component({
  selector: 'ngx-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss'],
})
export class AsistenciaComponent implements OnInit {
  teoricoEliminado: boolean;
  @Input() idCapacitacion: number;

  @Input() idTipo: number;
  /** Variable para inicializar el formulario. */
  formAsistencia: FormGroup;
  @ViewChild('radioSi')
  radioSi: RadioButton;
  @ViewChild('radioNo')
  radioNo: RadioButton;
  //
  asistio: boolean;
  /**arreglo auxiliar del microservicio*/
  capcitacion: any;
  //
  fechaIngreso: any;
  //
  guarTrue: boolean;
  /**Variable que obtendra la fecha de sistema*/
  date: Date = new Date();
  /**Variable que almacena la fecha en formato AAAA/MM/DD*/
  fechaCompleta: any;
  /**Variable que almacenara la hora actual de sistema*/
  horaActual: any;
  /**Variable que mostrara el dia de capacitacion actual*/
  diaCapacitacion: any;
  //
  fechasAsistencia: any;
  creasAsistencia: any;
  c: any;
  /**Variable que almacena el JSOn de asistencias*/
    jsonFecha: any;
  /**Variables que alamacenan las fechas por dia de capacitacion*/
  anadir1: any;
  anadir2: any;
  anadir3: any;
  anadir4: any;
  /**Variables que muestran los elemento si hay registro de asistencias*/
  visible1: boolean;
  visible2: boolean;
  visible3: boolean;
  visible4: boolean;

  activoAsistencia: boolean;

crearFecha: any;
  constructor(
    protected ref: NbDialogRef<AsistenciaComponent>,
    private fb: FormBuilder,
    protected dateService: NbDateService<Date>,
    protected capacitacionService: CapacitacionData,
    private toastrService: NbToastrService,
  ) {
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  habilita() {
    if (this.radioSi.checked) {
      this.asistio = true;
      this.guarTrue = true;
      // if (this.formAsistencia.controls['horaAsistenciaG'].valid &&
      //   this.formAsistencia.controls['horaAsistenciaG'].dirty) {
      //   this.guarTrue = true;
      // }
    } else if (this.radioNo.checked) {
      this.asistio = false;
      this.guarTrue = true;
    }
  }

  getFecha() {
    this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(result => {
      this.capcitacion = result;
      let month;
      month = this.date.getMonth() + 1;
      month = Number(month);
      if (month < 10) {
        month = '0' + month;
      }
      let day;
      day = this.date.getDate();
      day = Number(day);
      if (day < 10) {
        day = '0' + day;
      }
      this.fechaCompleta = this.date.getFullYear() + '-' + month + '-' + day + 'T05:00:00.000Z';
      this.horaActual = this.date.getHours() + ':' + this.date.getMinutes();
      this.diaCapacitacion = Number(this.capcitacion.fechaIngreso.slice(8, 10));
      this.diaCapacitacion = (day - this.diaCapacitacion) + 1;
      if (this.diaCapacitacion > 4) {
        this.diaCapacitacion = 4;
      }
      if (this.capcitacion.asistencias !== null) {
        this.jsonFecha = this.capcitacion.asistencias;
        this.jsonFecha = JSON.parse(this.jsonFecha);
        if (typeof(this.jsonFecha) === 'string') {
          this.jsonFecha = JSON.parse(this.jsonFecha);
        }
        if (this.jsonFecha.hasOwnProperty('asistencia1') === false) {
          this.anadir1 = 1; } else { this.anadir1 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia2') === false) {
          this.anadir2 = 1; } else { this.anadir2 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia3') === false) {
          this.anadir3 = 1; } else { this.anadir3 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia4') === false) {
          this.anadir4 = 1; } else { this.anadir4 = 0; }
        this.jsonFecha = JSON.stringify(this.jsonFecha);
      }
      /**Validacion para activar el registro de asistencia*/
      if (this.diaCapacitacion > 0 ) {
        this.activoAsistencia = true;
      }else {
        this.activoAsistencia = false;
      }
      if (this.idTipo === 2) {
        this.getAsistenciaByID();
      }
    });
  }

  generarAsistencia() {
          /**si no existe en la bd lo crea de lo contrario agrega al dia correspondiente*/
          if (this.capcitacion.asistencias === null) {
            if (this.radioSi.checked) {
              this.creasAsistencia = '{' +
                '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' + this.diaCapacitacion +
                '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' + this.diaCapacitacion +
                '" :"ASISTIO A LAS ' + this.formAsistencia.controls['horaAsistenciaG'].value + ' HORAS"}' +
                '}';
            } else if (this.radioNo.checked) {
              this.creasAsistencia = '{' +
                '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' + this.diaCapacitacion +
                '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' + this.diaCapacitacion +
                '" :"FALTA"}' +
                '}';
            }
          }else {
            this.creasAsistencia = this.capcitacion.asistencias;
            this.creasAsistencia = this.creasAsistencia.slice(0, this.creasAsistencia.length - 1);
            if (this.radioSi.checked) {
              this.fechasAsistencia = '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' +
                this.diaCapacitacion + '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' +
                this.diaCapacitacion + '" :"ASISTIO A LAS ' + this.formAsistencia.controls['horaAsistenciaG'].value + ' HORAS"}' +
                '}';
            } else if (this.radioNo.checked) {
              this.fechasAsistencia = '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' +
                this.diaCapacitacion + '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' +
                this.diaCapacitacion + '" :"FALTA"}' +
                '}';
            }
            this.creasAsistencia = this.creasAsistencia.concat(',', this.fechasAsistencia);
          }
          this.c = {
            'id': this.idCapacitacion,
            'idCandidato': this.capcitacion.idCandidato,
            'idCompetenciaCandidato': this.capcitacion.idCompetenciaCandidato,
            'idCapacitador': this.capcitacion.idCapacitador,
            'idCalificacionCompetencia': this.capcitacion.idCalificacionCompetencia,
            'calificacion': this.capcitacion.calificacion,
            'comentarios': this.capcitacion.comentarios,
            'asistencia': this.creasAsistencia,
          };
          let fechaIngreso: any;
          fechaIngreso = this.capcitacion.fechaIngreso;
          fechaIngreso = Date.parse(fechaIngreso + 'T01:00');
          /**Guardar  creasAsitencia en BD*/
          this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(data => {
            if (data.idEtapa === 4 && data.idEstado === 1 || data.idEtapa === 4 && data.idEstado === 16) {
              this.capacitacionService.updateCapacitacion(this.c, fechaIngreso).subscribe(result => {
                this.capacitacionService.postSocket(this.c).subscribe(d => {});
                this.showToast();
                this.ref.close();
              });
            }else {
              this.teoricoEliminado = true;
            } });
  }

  getAsistenciaByID() {
    this.jsonFecha = this.capcitacion.asistencias;
    if (this.jsonFecha === null) {
      this.ref.close();
      this.showToastsa();
    } else {
      this.jsonFecha = JSON.parse(this.jsonFecha);
      if (typeof(this.jsonFecha) === 'string') {
        this.jsonFecha = JSON.parse(this.jsonFecha);
      }
      for (let i = 1; i < 5; i++) {
        if (this.jsonFecha.hasOwnProperty('asistencia' + i)) {
          this.formAsistencia.controls['fechaAsistencia' + i].setValue(this.jsonFecha['asistencia' + i]['fecha' + i]);
          this.formAsistencia.controls['detalleAsistencia' + i].setValue(this.jsonFecha['asistencia' + i]['detalle' + i]);
          if (i > 0) {
            for (let j = 1; j < i + 1; j++) {
              this['visible' + j] = true;
            }
          }
          if (i === 4) {
            this.activaSeguimiento();
          }
        } else {
          this['visible' + i] = false;
        }
      }
    }
  }

  modificarDetalle(indice, detalle) {
    if (this['anadir' + indice] !== 1) {
      this.jsonFecha['asistencia' + indice]['detalle' + indice] = detalle;
    }
  }

  modificarFecha(indice, fecha) {
    if (this['anadir' + indice] !== 1) {
      this.jsonFecha['asistencia' + indice]['fecha' + indice] = fecha;
    }
  }

  agregarAsistencia(indice) {
    if (this.formAsistencia.controls['fechaAsistencia' + indice].value === '' ||
      this.formAsistencia.controls['detalleAsistencia' + indice].value === '') {
      this.showToastNot();
    }else {
      this.jsonFecha = JSON.stringify(this.jsonFecha);
      this.creasAsistencia = this.jsonFecha;
      this.creasAsistencia = this.creasAsistencia.slice(0, this.creasAsistencia.length - 1);
      this.fechasAsistencia = '"asistencia' + indice + '":{"dia' + indice + '":"' +
        indice + '","fecha' + indice + '":"' + (moment(this.formAsistencia.controls['fechaAsistencia' + indice].value).
        format('YYYY-MM-DD') + 'T05:00:00.000Z') + '","detalle' +
        indice + '":"' + this.formAsistencia.controls['detalleAsistencia' + indice].value + '"}' +
        '}';
      this.creasAsistencia = this.creasAsistencia.concat(',', this.fechasAsistencia);
      this.jsonFecha = this.creasAsistencia;
      this.updateAsistencia();
      this['anadir' + indice] = 1;
      this.jsonFecha = JSON.parse(this.jsonFecha);
    }
  }

  updateAsistencia() {
    this.jsonFecha = JSON.stringify(this.jsonFecha);
    this.c = {
      'id': this.idCapacitacion,
      'idCandidato': this.capcitacion.idCandidato,
      'idCompetenciaCandidato': this.capcitacion.idCompetenciaCandidato,
      'idCapacitador': this.capcitacion.idCapacitador,
      'idCalificacionCompetencia': this.capcitacion.idCalificacionCompetencia,
      'calificacion': this.capcitacion.calificacion,
      'comentarios': this.capcitacion.comentarios,
      'asistencia': this.jsonFecha,
    };
    let fechaIngreso: any;
    fechaIngreso = this.capcitacion.fechaIngreso;
    fechaIngreso = Date.parse(fechaIngreso + 'T01:00');
    /**Guardar  creasAsitencia en BD*/
    this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(data => {
      if (data.idEtapa === 4 && data.idEstado === 1 || data.idEtapa === 4 && data.idEstado === 16) {
        this.capacitacionService.updateCapacitacion(this.c, fechaIngreso).subscribe(result => {
          this.capacitacionService.postSocket(this.c).subscribe(d => {});
          this.showToastU();
          this.ref.close();
        });
      }else {
        this.teoricoEliminado = true;
      } });
  }

  showToastU() {
    this.toastrService.success('!!Modificación Exitosa¡¡', `Asistencia`);
  }

  showToastNot() {
    this.toastrService.danger('!Hay campos vacíos¡', `Asistencia`);
  }

  showToast() {
    this.toastrService.success('!!Asistencia registrada con Exito¡¡', `Asistencia`);
  }

  showToastsa() {
    this.toastrService.info('!!Aún NO registra Asistencia¡¡', `Asistencia`);
  }

  activaSeguimiento() {
    ///
  }

  ngOnInit() {
    this.asistio = false;
    this.guarTrue = false;
    this.getFecha();
    this.formAsistencia = this.fb.group({
      'horaAsistenciaG': new FormControl('', Validators.compose([Validators.required, Validators.min(9.00), Validators.max(17.00)])),
      'detalleAsistencia1': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia1': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia2': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia2': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia3': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia3': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia4': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia4': new FormControl('', Validators.compose([Validators.required])),
    });
  }

}
