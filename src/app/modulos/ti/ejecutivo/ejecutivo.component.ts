import {Component, OnInit, ViewChild} from '@angular/core';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {NbDialogService} from '@nebular/theme';
import {DialogService} from 'primeng/api';
import {CrearEjecutivoComponent} from '../modals/crear-ejecutivo/crear-ejecutivo.component';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {map} from 'rxjs/operators';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { environment } from '../../../../environments/environment';
import {Router} from '@angular/router';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-ejecutivo',
  templateUrl: './ejecutivo.component.html',
  styleUrls: ['./ejecutivo.component.scss'],
  providers: [ DialogService],
})
export class EjecutivoComponent implements OnInit {

  cols: any[];
  empleado: Empleados[];
  area: any[];
  subarea: Subarea[];
  idSubarea: any;
  UserForm: FormGroup;
  areas: any[];
  subareas: Area[];
  tipoarea: number;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  permisosupervisor: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    private empleadosService: EmpleadosData,
    public dialogService: NbDialogService,
    private subareaService: SubareaData,
    private fb: FormBuilder,
    private areaService: AreaData,
    private router: Router
  ) {
    this.initForma();
  }

  ngOnInit() {
    this.getArea();
    // this.getSubArea();
    this.getEmpleados();
    this.cols = [
      'nombre',
      'telefono',
      'sede',
      'area',
      'subarea',
      'asignar Correo',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  initForma() {
    this.UserForm = this.fb.group({
      'subareacontrol': new FormControl(),
      'areacontrol': new FormControl(),
    });
  }

  getEmpleados() {
    this.empleadosService.get().pipe(map( result => {
      return result.filter( data => data.idUsuario === null  );
    })).subscribe(data => {
      this.empleado = data;
      this.dataSource = new MatTableDataSource(this.empleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    this.idSubarea = event;
  }

  getsubareas(event) {
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.area = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.area.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});
        this.empleado = null;
      }
    });
    this.tipoarea = event;
  }

  getArea() {
    this.areaService.get().pipe(map(data => this.subareas = data)).subscribe(data => {
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }

  asignarEjecutivo(id, idSubarea, tipoarea, area, subarea) {
    this.dialogService.open(CrearEjecutivoComponent, {
      context: {
        id: id,
        subarea: idSubarea,
        tipoarea: tipoarea,
        area: area,
        nombresubarea: subarea,
      },
    }).onClose.subscribe(x => {
      this.getEmpleados();
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'ejecutivo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelEjecutivo', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.getEmpleados();
        }, 500);
      });
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.usuarioAsignaciones.escritura;
    this.permisosupervisor = this.permisos.modulos.supervisor.activo;
    console.log(this.permisosupervisor);
  }

  estaActivo() {
    this.activo = this.permisos.modulos.usuarioAsignaciones.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
