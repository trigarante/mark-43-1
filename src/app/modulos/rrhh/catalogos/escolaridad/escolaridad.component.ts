import {Component, OnInit, ViewChild} from '@angular/core';
import {Escolaridad, EscolaridadData} from '../../../../@core/data/interfaces/catalogos/escolaridad';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {EscolaridadCreateComponent} from '../modals/escolaridad-create/escolaridad-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de escolaridad (catálogo).
 */
@Component({
  selector: 'ngx-escolaridad',
  templateUrl: './escolaridad.component.html',
  styleUrls: ['./escolaridad.component.scss'],
})
export class EscolaridadComponent implements OnInit {
  cols: any[];
  escolaridad: Escolaridad[];
  esc: Escolaridad;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {EscolaridadData} escolaridadService Servicio del componente escolaridad para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private escolaridadService: EscolaridadData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'escolaridad');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelEscolaridad', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEscolaridad(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene los datos del campo "escolaridad" desde el Microservicio y los almacena en el arreglo "escolaridad". */
  getEscolaridad(mensajeAct) {
    this.escolaridadService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.escolaridad = [];
      this.escolaridad = data;
      this.dataSource = new MatTableDataSource(this.escolaridad); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Escolaridad`);
  }
  /** Función que obtiene la información de la escolaridad dependiendo de su ID. */
  getEscolaridadById(idEscolaridad) {
    this.escolaridadService.getEscolaridadById(idEscolaridad).subscribe(data => {
      this.esc = data;
    });
  }

  /** Función que despliega un modal del componente "EscolaridadCreateComponent" para poder "crear escolaridad". */
  crearEscolaridad() {
    this.dialogoService.open(EscolaridadCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEscolaridad(0);
    });
  }

  /** Función que despliega un modal en el cual se puede actualizar la información de una escolaridad. */
  updateEscolaridad(idEscolaridad) {
    this.dialogoService.open(EscolaridadCreateComponent, {
      context: {
        idTipo: 2,
        idEscolaridad: idEscolaridad,
      },
    }).onClose.subscribe(x => {
      this.getEscolaridad(1);
    });
  }

  bajaEscolaridad(idEscolaridad) {
    this.getEscolaridadById(idEscolaridad);
    swal({
      title: '¿Deseas dar de baja esta escolaridad?',
      text: 'Una vez dada de baja, no podrás ver la escolaridad de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.esc['activo'] = 0;
        this.escolaridadService.put(idEscolaridad, this.esc).subscribe( result => {
          this.escolaridadService.postSocket({idEscolaridad: idEscolaridad, esc: this.esc}).subscribe(x => {});
        });
        swal('¡La escolaridad se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getEscolaridad(0);
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.escolaridad.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.escolaridad.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nivel', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getEscolaridad(0);
  }
}
