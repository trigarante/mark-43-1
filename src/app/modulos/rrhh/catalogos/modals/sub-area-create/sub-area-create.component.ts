import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subarea, SubareaData} from '../../../../../@core/data/interfaces/catalogos/subarea';
import {Area, AreaData} from '../../../../../@core/data/interfaces/catalogos/area';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {SubAreaComponent} from '../../sub-area/sub-area.component';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-sub-area-create',
  templateUrl: './sub-area-create.component.html',
  styleUrls: ['./sub-area-create.component.scss'],
})
export class SubAreaCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idSubArea: number;
  subAreaCreateForm: FormGroup;
  submitted: boolean;
  subArea: Subarea;
  area: any[];
  Areas: Area[];

  constructor(protected subAreaService: SubareaData, protected areaSeervice: AreaData,
              private fb: FormBuilder, protected ref: NbDialogRef<SubAreaComponent>, private router: Router) {
  }

  guardarSubArea() {
    if (this.subAreaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.subAreaService.posting(this.subAreaCreateForm.value).subscribe((result) => {
      this.subAreaCreateForm.reset();
      this.ref.close();
    });
  }

  getArea() {
    this.areaSeervice.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.Areas = result;
      this.area = [];
      for (let i = 0; i < this.Areas.length; i++) {
        this.area.push({'label': this.Areas[i].nombre, 'value': this.Areas[i].id});
      }
    });
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  updateSubArea() {
    this.subAreaService.put(this.idSubArea, this.subAreaCreateForm.value).subscribe(result => {
      this.subAreaCreateForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  getSubAreaById() {
    this.subAreaService.getById(this.idSubArea).subscribe(data => {
      this.subArea = data;
      this.subAreaCreateForm.controls['idArea'].setValue(this.subArea.idArea);
      this.subAreaCreateForm.controls['subarea'].setValue(this.subArea.subarea);
      this.subAreaCreateForm.controls['descripcion'].setValue(this.subArea.descripcion);
    });
  }

  ngOnInit() {
    this.getArea();
    if (this.idTipo === 2) {
      this.getSubAreaById();
    }
    this.subAreaCreateForm = this.fb.group({
      'idArea': new FormControl('', Validators.compose([Validators.required])),
      'subarea': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
