import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {Router} from '@angular/router';
import {GuardadoComponent} from '../guardado/guardado.component';
import {formatDate} from '@angular/common';


@Component({
  selector: 'ngx-agregar-nodo',
  templateUrl: './agregar-nodo.component.html',
  styleUrls: ['./agregar-nodo.component.scss'],
})
export class AgregarNodoComponent implements OnInit {
  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  /** variable que se puede obtener la posicion de la aseguradora*/
  @Input() nombre: number;
  @Input() tiponombre: string;
  /** Variable que permite la inicialización del formulario para crear los campos. */
  agregarNodoForm: FormGroup;
  /** Arreglo que se inicializa con los valores del Microservicio de pagina. */
  pagina: Pagina;
  aseguradoras: any;
  siexisteaseguradora: any;
  astring: any;
  nombreaseguradora: any;
  datosprincipales: any;

  constructor(private paginaService: PaginasData, private fb: FormBuilder, protected ref: NbDialogRef<AgregarNodoComponent>,
              private router: Router,  public dialogService: NbDialogService ) {
    this.agregarNodoForm = this.fb.group({
      'nombreNodo': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  /** Metodo que hace un nuevo nodo y guarda en el json*/
  guardarNodo() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      this.datosprincipales = JSON.parse(this.pagina.configuracion);
      /** si hay una aseguradora*/
      /** Se obtiene los datos para ver si existe un campo llamado aseguradoras y despues se pasa a string*/
      this.siexisteaseguradora = Object.keys(this.aseguradoras);

      this.astring = JSON.stringify(this.siexisteaseguradora);
      /** si no existe aseguradoras*/
      if (this.astring !== '["aseguradoras"]') {

        /** agrega un nuevo nodo(aseguradora) vacio*/
        this.aseguradoras[this.tiponombre].aseguradoras[this.nombre][this.agregarNodoForm.value.nombreNodo] = [{}];
      }

      if (this.astring === '["aseguradoras"]') {
        // this.aseguradoras.aseguradoras[this.agregarNodoForm.value.nombreNodo] = {};
        /** agrega un nuevo nodo(aseguradora) vacio*/
         this.aseguradoras.aseguradoras[this.nombre][this.agregarNodoForm.value.nombreNodo] = [{}];
      }
      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));
      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocketAseguradoras({status: 'ok'}).subscribe(x => {});
        this.ref.close();
        this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      }).onClose.subscribe(x => {

      });
    });


  }
  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
  }

}
