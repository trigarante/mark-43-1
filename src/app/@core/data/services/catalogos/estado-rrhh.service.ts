import {Injectable} from '@angular/core';
import {EstadoRrhh, EstadoRrhhData} from '../../interfaces/catalogos/estado-rrhh';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class EstadoRrhhService extends EstadoRrhhData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<EstadoRrhh[]> {
    return this.http.get<EstadoRrhh[]>(this.baseURL + 'v1/estadosRrhh');
  }

  createEstadoRrhh(estadoRrhh: EstadoRrhh): Observable<EstadoRrhh> {
    return this.http.post<EstadoRrhh>(this.baseURL + 'v1/estadosRrhh', estadoRrhh);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'motivosbaja/saveMotivosBaja', json);
  }

  updateEstadoRrhh(idEstadoRh, estadoRrh: EstadoRrhh): Observable<EstadoRrhh> {
    return this.http.put<EstadoRrhh>(
      this.baseURL + 'v1/estadosRrhh/' + idEstadoRh + '/' + sessionStorage.getItem('Empleado'), estadoRrh);
  }

  getEstadoRrhhById(idEstadoRh): Observable<EstadoRrhh> {
    return this.http.get<EstadoRrhh>(this.baseURL + 'v1/estadosRrhh/' + idEstadoRh);
  }
}
