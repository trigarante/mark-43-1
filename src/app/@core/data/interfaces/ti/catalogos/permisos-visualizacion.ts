import {Observable} from 'rxjs';

export interface PermisosVisualizacion {
  id: number;
  descripcion: string;
  activo: number;
}
export abstract class PermisosVisualizacionData {
  abstract get(): Observable<PermisosVisualizacion[]>;
  abstract getByID(idPermiso): Observable<PermisosVisualizacion>;
}
