import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment.prod';
import {RespuestasInterfaces} from '../../interfaces/rrhh/respuestas-interface';

@Injectable({
  providedIn: 'root',
})
export class RespuestasService {

  constructor(private httpClient: HttpClient) {
  }

  public getRespuestaByPregunta(idR: number) {
    return this.httpClient
      .get<RespuestasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/respuestas-by-pregunta/' + idR);
  }

  public deleteRespuestaByPregunta(idR: number) {
    return this.httpClient
      .delete<RespuestasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/respuestas-by-pregunta/' + idR);
  }
}
