import {Observable} from 'rxjs';

export interface Vacante {
  area: string;
  empresa: string;
  idGrupo: number;
  id: number;
  idArea: number;
  idEmpresa: number;
  idPuesto: number;
  idSede: number;
  idSubarea: number;
  idTipoPuesto: number;
  idTipoUsuario: number;
  nombre: string;
  puesto: string;
  sede: string;
  subarea: string;
  tipoUsuario: string;
  puestoTipo: string;
  activo: number;
  idMarcaEmpresarial: number;
}

export abstract class VacanteData {
  abstract get(): Observable<Vacante[]>;

  abstract post(vacante): Observable<Vacante>;
  abstract postSocket(json): Observable<JSON>;

  abstract getVacantesById(idVacante): Observable<Vacante>;

  abstract put(idVacante, vacante): Observable<Vacante>;

  abstract putBaja(idVacante): Observable<Vacante>;
}
