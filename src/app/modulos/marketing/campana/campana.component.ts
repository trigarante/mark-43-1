import {Component, OnInit, ViewChild} from '@angular/core';
import {CampanaMarketing, CampanaMarketingData} from '../../../@core/data/interfaces/marketing/campana';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {CampanaMarketingCreateComponent} from '../modals/campana-marketing-create/campana-marketing-create.component';
import {AsignarCategoriaCamapanaComponent} from '../modals/asignar-categoria-camapana/asignar-categoria-camapana.component';
import {AsignarMediosDifusionComponent} from '../modals/asignar-medios-difusion/asignar-medios-difusion.component';
import {ActualizarCampanaComponent} from '../modals/actualizar-campana/actualizar-campana.component';
import {environment} from '../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-campana',
  templateUrl: './campana.component.html',
  styleUrls: ['./campana.component.scss'],
})
export class CampanaComponent extends MatPaginatorIntl implements OnInit {
  dataSource: any;
  camapanaMarketing: CampanaMarketing[];
  cols: any[];
  // sockets
  private stompClient = null;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    protected campanaMarketingService: CampanaMarketingData,
    private dialogoService: NbDialogService,
  ) {
    super();
    this.getAndInitTranslations();
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'campania');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelCampania', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCampanasMarketing();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getAndInitTranslations() {

    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} / ${length}`;
  }


  getCampanasMarketing() {
    this.campanaMarketingService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.camapanaMarketing = [];
      this.camapanaMarketing = data;
      this.dataSource = new MatTableDataSource(this.camapanaMarketing); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearCampanaMarketing() {
    this.dialogoService.open(CampanaMarketingCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getCampanasMarketing();
    });
  }

  asignarMediosDifusion(idMediosDifusion) {
    this.dialogoService.open(AsignarMediosDifusionComponent, {
      context: {
        idTipo: 1,
        idMedio: idMediosDifusion,
      },
    });

  }

  updateCampanaMarketing() {
  }

  asignarCategoriaCampana(idCampanaMarketing) {
    this.dialogoService.open(AsignarCategoriaCamapanaComponent, {
      context: {
        idTipo: 1,
        idCampanaMarketing: idCampanaMarketing,
      },
    });
  }

  /** modal que actualiza los datos de la campana*/
  actualizarCampana(idCampana) {
    this.dialogoService.open(ActualizarCampanaComponent, {
      context: {
        id: idCampana,

      },
    }).onClose.subscribe(x => {
      this.getCampanasMarketing();
    });
  }

  verDatos() {
  }

  ngOnInit() {
    this.connect();
    this.getCampanasMarketing();
    this.cols = ['idSubarea', 'nombre', 'descripcion', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
