import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {EstadoCivil, EstadoCivilData} from '../../../@core/data/interfaces/catalogos/estado-civil';
import {Escolaridad, EscolaridadData} from '../../../@core/data/interfaces/catalogos/escolaridad';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {Paises, PaisesData} from '../../../@core/data/interfaces/catalogos/paises';
import {MedioTransporte, MedioTransporteData} from '../../../@core/data/interfaces/catalogos/medio-transporte';
import {EstacionesLineas, EstacionesLineasData} from '../../../@core/data/interfaces/catalogos/estaciones-lineas';
import {Puesto, PuestoData} from '../../../@core/data/interfaces/catalogos/puesto';
import {PuestoTipo, PuestoTipoData} from '../../../@core/data/interfaces/catalogos/puesto-tipo';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {Empresa, EmpresaData} from '../../../@core/data/interfaces/catalogos/empresa';
import {Banco, BancoData} from '../../../@core/data/interfaces/catalogos/banco';
import {Sepomex, SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';
import {SelectItem} from 'primeng/api';
import {Dropdown, RadioButton} from 'primeng/primeng';
import {Precandidatos, PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {Grupo, GrupoData} from '../../../@core/data/interfaces/catalogos/grupo';
import {Sede, SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import * as moment from 'moment';
import _date = moment.unitOfTime._date;
import {MarcaEmpresarial, MarcaEmpresarialData} from '../../../@core/data/interfaces/catalogos/marca-empresarial';
import {Turno, TurnoData} from '../../../@core/data/interfaces/catalogos/turno';
/**
 * Componente para actualizar los datos de un empleado.
 * */

@Component({
  selector: 'ngx-empleado-update',
  templateUrl: './empleado-update.component.html',
  styleUrls: ['./empleado-update.component.scss'],
})
export class EmpleadoUpdateComponent implements OnInit {
  empleadoEliminado: boolean;
  updateDatosEmpresariaes: FormGroup;
  idTipo: any;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para puntos y letras */
  puntoLetras: RegExp = /[\\A-Za-zñÑ. ]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  /** Variable que permite la inicialización del formulario para actualizar al empleado. */
  updateEmpleadoForm: FormGroup;
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del campo Paises desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para paisOrigen. */
  pais: any[];
  /** Arreglo que se inicializa con los valores del campo Puesto desde el Microservicio. */
  puestos: Puesto[];
  /** Arreglo auxiliar para puesto. */
  puesto: any[];
  /** Arreglo que se inicializa con los valores del campo Tipo de Puesto desde el Microservicio. */
  puestoTipo: PuestoTipo[];
  /** Arreglo auxiliar para puestoTipo. */
  puestotipo: any[];
  /** Arreglo que se inicializa con los valores de las subAreas desde el Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subArea */
  subarea: any[];
  /** Arreglo que se inicializa con los valores del campo Empresa desde el Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para empresa. */
  empresa: any[];
  /** Arreglo que se inicializa con los valores del campo Banco desde el Microservicio. */
  bancos: Banco[];
  /** Arreglo auxiliar para bancos */
  banco: any[];
  /** Arreglo que se inicializa con los valores del campo Estado Civil desde el Microservicio. */
  estadoCivil: EstadoCivil[];
  /** Arreglo auxiliar para civil. */
  civil: any[];
  /** Arreglo que se inicializa con los valores del campo Escolaridad desde el Microservicio. */
    // escolaridad
  escolaridad: Escolaridad[];
  /** Arreglo auxiliar para escolaridad. */
  gradoEscolar: any[];
  /** Arreglo que se inicializa con los valores del campo Estado Escolaridad desde el Microservicio. */
    // estadoEscolar
  estadoEscolar: EstadoEscolaridad[];
  /** Arreglo auxiliar para estadoEscolar. */
  estado: any[];
  /** Arreglo que se inicializa con los valores del campo Medio de Transporte desde el Microservicio. */
  medioTransporte: MedioTransporte[];
  /** Arreglo auxiliar para medioTransporte */
  medio: any[];
  /** Arreglo que se inicializa con los valores del campo Lineas Metro desde el Microservicio. */
  lineas = [{'label': '', 'value': ''}];
  /** Arreglo auxiliar para las estaciones de la linea del metro . */
  estacionesLineas: EstacionesLineas[];
  /** Arreglo para las estaciones de la linea del metro. */
  estaciones = [{'label': '', 'value': 0}];
  /** Variable que almacena los elementos seleccionados respecto al género. */
  genero: SelectItem[];
  /** Variable que almacena los elementos seleccionados respecto al tiempo de llegada. */
  tiempoLlegada: SelectItem[];
  /** Arreglo que se inicializa con los valores del campo Sepomex desde el Microservicio. */
  cpSepomex: Sepomex[];
  /** Arreglo auxiliar para las colonias. */
  colonias: any[];
  /** Variable de tipo Empleados. */
  empleados: Empleados;
  precandidato: Precandidatos;
  /** Arreglo auxiliar para los documentos. */
  documentos: any;
  /** Variable auxiliar para una subcadena de la fecha de ingreso, sin el Timestamp. */
  auxFechaIngreso: String;
  /** Variable que almacena el nombre del empleado que se va a actualizar. */
  nombre: String;
  /** Variable que almacena el apellido paterno del empleado que se va a actualizar. */
  apellidoPaterno: String;
  /** Variable que almacena el apellido materno del empleado que se va a actualizar. */
  apellidoMaterno: String;
  /**Variable que genera el body para actualizar datos personales*/
  formact: any;
  /**Variable que valida el codigo postal*/
  mostrar: boolean;
  /** Variable para darle valor a la imagen del encabezado */
  fotoEmpleado: any;
  /**variables para establecer maximo y minimo de caracteres en cuenta clave*/
  maxNum: any;
  minNum: any;
  /**Variable para cambiar el label de la clabe de cuenta en datos empresariales*/
  labelNumeroCuenta: string;
  /**Variables para activar o desactivar los radioButtons de cuenta clabe*/
  radioCuentaCheck: boolean;
  radioClaveCheck: boolean;
  /**Variables que almacenan el valor de los radiobutton al dar check*/
  @ViewChild('radioCuenta')
  radioCuenta: RadioButton;
  @ViewChild('radioClave')
  radioClave: RadioButton;
  /** Variable para referenciar al dropdown de las lineas del metro. */
  @ViewChild('lineaDropdown')
  lineaDropdown: Dropdown;

  /** Variable para referenciar al dropdown de las estaciones de la linea del metro seleccionada. */
  @ViewChild('estacionesDropdown')
  estacionesDropdown: Dropdown;
  idPrecandidato: number = this.rutaActiva.snapshot.params.idPrecandidato;

  /** Bandera para mostrar la ventana de tomar foto. */
  ventanaFoto = false;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  @ViewChild('video')
  public video: ElementRef;
  /** Variable para mostrar el video en el canvas. */
  @ViewChild('canvasVideo')
  public canvasVideo: ElementRef;
  /** Variable que almacena la foto tomada en un canvas. */
  @ViewChild('canvas')
  public canvas: ElementRef;
  /*Variable que identifica si el empleado tiene foto en la base de datos*/
  public fotoDisponible: Boolean;
  /** Bandera para saber si la foto ha sido tomada. */
  public fotoTomada = false;
  /** Bandera para saber si la foto ha sido guardada. */
  private fotoGuardada: boolean = false;
  /** Variable que define el parámetro de altura en la WebCam. */
  private altoWebCam = 350;
  /** Variable que define el parámetro de anchura en la WebCam. */
  private anchoWebCam = 465;
  /** Variable que define el parámetro de anchura de la foto tomada. */
  private anchoFoto = 240;
  /** Variable que almacena la foto tomada en una cadena de bytes. */
  private foto: Blob;
  fileToUpload: File = null;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  tipoPuestos: PuestoTipo[];
  /** Arreglo auxiliar para puesto. */
  tipoPuesto: any[];
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarPuesto: boolean;
  /**.....validar kpi existente*/
  kpi: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Turnos: Turno[];

  /** El método constructor manda a llamar Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EmpleadosData} empleadosService Servicio del componente Empleados para poder invocar los métodos.
   * @param {ActivatedRoute} rutaActiva Establece la ruta que se encuentra activa actualmente.
   * @param {EstadoCivilData} estadoCivilService Servicio del componente estado-civil para poder invocar los métodos.
   * @param {EscolaridadData} escolaridadService Servicio del componente escolaridad para poder invocar los métodos.
   * @param {EstadoEscolaridadData} estadoEscolarService Servicio del componente estado-escolaridad para poder invocar los métodos.
   * @param {PaisesData} paisesService Servicio del componente paises para poder invocar los métodos.
   * @param {MedioTransporteData} medioTransporteService Servicio del componente medio-transporte para poder invocar los métodos.
   * @param {EstacionesLineasData} estacionesService Servicio del componente estaciones-lineas para poder invocar los métodos.
   * @param {PuestoData} puestoService Servicio del componente puesto para poder invocar los métodos.
   * @param {PuestoTipoData} puestoTipoService Servicio del componente puesto-tipo para poder invocar los métodos.
   * @param {SubareaData} subareaService Servicio del componente subarea para poder invocar los métodos.
   * @param {EmpresaData} empresasService Servicio del componente empresa para poder invocar los métodos.
   * @param {BancoData} bancoService Servicio del componente banco para poder invocar los métodos.
   * @param {SepomexData} sepomexService Servicio del componente sepomex para poder invocar los métodos.
   * @param {Router} router Variable para navegar entre componentes. */
  constructor(private fb: FormBuilder, protected empleadosService: EmpleadosData, private rutaActiva: ActivatedRoute,
              protected estadoCivilService: EstadoCivilData, protected escolaridadService: EscolaridadData,
              protected estadoEscolarService: EstadoEscolaridadData, protected paisesService: PaisesData,
              protected medioTransporteService: MedioTransporteData, protected estacionesService: EstacionesLineasData,
              protected puestoService: PuestoData, protected puestoTipoService: PuestoTipoData, protected subareaService: SubareaData,
              protected empresaService: EmpresaData, protected bancoService: BancoData, protected sepomexService: SepomexData,
              private router: Router, protected precandidatoService: PrecandidatosData, private sanitizer: DomSanitizer,
              private grupoService: GrupoData, private sedesService: SedeData, private areaService: AreaData,
              private tipoPuestoService: PuestoTipoData, private turnoService: TurnoData) {
    this.updateDatosEmpresariaes = this.fb.group({
      'rfc': new FormControl('', Validators.compose([
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][1-9][1-9][A-Z-0-9][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(13), Validators.minLength(13)])),
      'fechaIngreso': new FormControl('', Validators.required),
      'puestoDetalle': new FormControl('', Validators.required),
      'idPuesto': new FormControl('', Validators.required),
      'idTipoPuesto': new FormControl('', Validators.required),
      'idCampana': new FormControl(''),
      'idSubarea': new FormControl('', Validators.required),
      'sueldoDiario': new FormControl('', Validators.compose([Validators.required, Validators.max(5000), Validators.min(1)])),
      'sueldoMensual': new FormControl('', Validators.compose([Validators.required, Validators.max(50000), Validators.min(1)])),
      'kpi': new FormControl(''),
      'kpiMensual': new FormControl(''),
      'kpiTrimestral': new FormControl(''),
      'kpiSemestral': new FormControl(''),
      'ctaClabe': new FormControl('', Validators.compose([Validators.required])),
      'tarjetaSecundaria': new FormControl(''),
      'idEmpresa': new FormControl('', Validators.required),
      'idBanco': new FormControl('', Validators.required),
      'imss': new FormControl('', Validators.compose([Validators.maxLength(11), Validators.minLength(11)])),
      'documentosAdministrativos': null,
      'documentosPersonales': null,
      'idHuella': 1,
      // 'idMarcaEmpresarial': new FormControl('', Validators.compose([Validators.required])),
      'fechaRegistro': new FormControl(''),
      'idCandidato': new FormControl(''),
      'idEstadoPrecandidato': new FormControl(''),
      'idSolicitudRRHH': new FormControl(''),
      'idEtapa': new FormControl(''),
      'idEmpleado': new FormControl(''),
      'idPrecandidato': new FormControl(''),
      'tipoUsuario': new FormControl(''),
      'comentarios': new FormControl(''),
      'fechaAltaIMSS': new FormControl(''),
      'fechaAsignacion': new FormControl(''),
      'fechaCambioSueldo': new FormControl(''),
      'idUsuario': new FormControl(''),
      // 'imagenEmpleado': new FormControl(''),
      'razonSocial': new FormControl(''),
      'idGrupo': new FormControl(''),
      'idSede': new FormControl(''),
      'idArea': new FormControl(''),
      'idTurnoEmpleado': new FormControl(''),
      'horario': new FormControl(''),
    });
    this.updateEmpleadoForm = this.fb.group({
      'curp': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]'),
        Validators.maxLength(18), Validators.minLength(18)])),
      'nombre': new FormControl('', Validators.required), // Validators.pattern('/^[\\pL\\s\\-]+$/u\n'
      'apellidoPaterno': new FormControl('', Validators.required),
      'apellidoMaterno': new FormControl('', Validators.required),
      'fechaNacimiento': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'genero': new FormControl('', Validators.required),
      'idEstadoCivil': new FormControl('', Validators.required),
      'idEscolaridad': new FormControl('', Validators.required),
      'idEstadoEscolaridad': new FormControl('', Validators.required),
      'idPais': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'colonia': new FormControl('', Validators.required),
      'calle': new FormControl('', Validators.required),
      'numeroInterior': new FormControl(''),
      'numeroExterior': new FormControl('', Validators.required),
      'telefonoMovil': new FormControl('', Validators.required),
      'telefonoFijo': new FormControl(''),
      'tiempoTraslado': new FormControl('', Validators.required),
      'idMedioTraslado': new FormControl('', Validators.required),
      'lineaMetro': new FormControl(''),
      'idEstacion': new FormControl(''),
    });
  }
/**.............validar si se activa el campo nss, si ya se cumplieron 5 dias posteriores a su fecha de ingreso*/
/**validarFechaIngreso(fechaIngreso) {
  const fechaSemana: any = moment(Date.now()).toISOString();
  if ( Math.abs(moment(fechaIngreso).diff(fechaSemana, 'days')) + 1 > 4) {
    this.activarNss = true;
  }else {
    this.activarNss = false;
  }
}*/
  /** Función que obtiene los datos desde el servidor del empleado seleccionado y los almacena en la variable empleados. */
  getEmpleado() {
    this.empleadosService.getEmpleadoById(this.rutaActiva.snapshot.params.idEmpleado)
      .subscribe(data => {
        this.empleados = data;
        this.auxFechaIngreso = String(this.empleados.fechaIngreso);
        this.auxFechaIngreso = this.auxFechaIngreso.substring(0, 10); // FORMATO yyyy-mm-dd
        this.updateDatosEmpresariaes.controls['fechaIngreso'].setValue(new Date(this.auxFechaIngreso + ' 00:00:00'));
        this.updateDatosEmpresariaes.controls['puestoDetalle'].setValue(this.empleados.puestoDetalle);
        this.updateDatosEmpresariaes.controls['idPuesto'].setValue(this.empleados.idPuesto);
        this.updateDatosEmpresariaes.controls['idBanco'].setValue(this.empleados.idBanco);
        this.updateDatosEmpresariaes.controls['idTipoPuesto'].setValue(this.empleados.idTipoPuesto);
        this.updateDatosEmpresariaes.controls['idCampana'].setValue(this.empleados.idCampana);
        this.updateDatosEmpresariaes.controls['idSubarea'].setValue(this.empleados.idSubarea);
        this.updateDatosEmpresariaes.controls['sueldoDiario'].setValue(this.empleados.sueldoDiario);
        this.updateDatosEmpresariaes.controls['sueldoMensual'].setValue(this.empleados.sueldoMensual);
        this.updateDatosEmpresariaes.controls['kpi'].setValue(this.empleados.kpi);
        this.updateDatosEmpresariaes.controls['kpiMensual'].setValue(this.empleados.kpiMensual);
        this.updateDatosEmpresariaes.controls['kpiTrimestral'].setValue(this.empleados.kpiTrimestral);
        this.updateDatosEmpresariaes.controls['kpiSemestral'].setValue(this.empleados.kpiSemestral);
        this.updateDatosEmpresariaes.controls['ctaClabe'].setValue(this.empleados.ctaClabe);
        this.radioButtonsCuentaClabe(this.empleados.ctaClabe);
        this.updateDatosEmpresariaes.controls['tarjetaSecundaria'].setValue(this.empleados.tarjetaSecundaria);
        this.updateDatosEmpresariaes.controls['fechaRegistro'].setValue(this.empleados.fechaCreacion);
        this.updateDatosEmpresariaes.controls['idCandidato'].setValue(this.empleados.idCandidato);
        this.updateDatosEmpresariaes.controls['idEstadoPrecandidato'].setValue(this.empleados.idEstado);
        this.updateDatosEmpresariaes.controls['idSolicitudRRHH'].setValue(this.empleados.idSolicitudRRHH);
        this.updateDatosEmpresariaes.controls['idEtapa'].setValue(this.empleados.idEtapa);
        this.updateDatosEmpresariaes.controls['idEmpleado'].setValue(this.empleados.id);
        this.updateDatosEmpresariaes.controls['tipoUsuario'].setValue(this.empleados.idTipoUsuario);
        this.updateDatosEmpresariaes.controls['idPrecandidato'].setValue(this.empleados.idPrecandidato);
        this.updateDatosEmpresariaes.controls['idEmpresa'].setValue(this.empleados.idEmpresa);
        this.updateDatosEmpresariaes.controls['rfc'].setValue(this.empleados.rfc);
        this.updateDatosEmpresariaes.controls['imss'].setValue(this.empleados.imss);
        this.updateDatosEmpresariaes.controls['comentarios'].setValue(this.empleados.comentarios);
        this.updateDatosEmpresariaes.controls['fechaAltaIMSS'].setValue(this.empleados.fechaAltaIMSS);
        this.updateDatosEmpresariaes.controls['fechaAsignacion'].setValue(this.empleados.fechaAsignacion);
        this.updateDatosEmpresariaes.controls['fechaCambioSueldo'].setValue(this.empleados.fechaCambioSueldo);
        this.updateDatosEmpresariaes.controls['idUsuario'].setValue(this.empleados.idUsuario);
        this.updateDatosEmpresariaes.controls['documentosPersonales'].setValue(this.empleados.documentosPersonales);
        this.updateDatosEmpresariaes.controls['documentosAdministrativos'].setValue(this.empleados.documentosAdministrativos);
        this.updateDatosEmpresariaes.controls['idGrupo'].setValue(this.empleados.idGrupo);
        this.updateDatosEmpresariaes.controls['idSede'].setValue(this.empleados.idSede);
        this.updateDatosEmpresariaes.controls['idArea'].setValue(this.empleados.idArea);
        this.updateDatosEmpresariaes.controls['idTurnoEmpleado'].setValue(this.empleados.idTurnoEmpleado);
        this.updateDatosEmpresariaes.controls['horario'].setValue(this.empleados.idTurnoEmpleado);
        // this.updateDatosEmpresariaes.controls['idMarcaEmpresarial'].setValue(this.empleados.idMarcaEmpresarial);
        this.getFoto(this.rutaActiva.snapshot.params.idEmpleado);
        this.getEmpresa(this.empleados.idGrupo);
        this.getSedes(this.empleados.idEmpresa);
        this.getArea(this.empleados.idSede);
        this.getSubarea(this.empleados.idArea);
        this.getPuesto();
        // this.getMarcaEmprearial(this.empleados.idSede);
      });
  }

  getEmpleadoPersonalesById() {
    this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      this.precandidato = data;
      this.updateEmpleadoForm.controls['nombre'].setValue(this.precandidato.nombre);
      this.updateEmpleadoForm.controls['apellidoPaterno'].setValue(this.precandidato.apellidoPaterno);
      this.updateEmpleadoForm.controls['apellidoMaterno'].setValue(this.precandidato.apellidoMaterno);
      this.updateEmpleadoForm.controls['genero'].setValue(this.precandidato.genero);
      this.updateEmpleadoForm.controls['curp'].setValue(this.precandidato.curp);
      this.updateEmpleadoForm.controls['email'].setValue(this.precandidato.email);
      this.updateEmpleadoForm.controls['idPais'].setValue(this.precandidato.idPais);
      this.updateEmpleadoForm.controls['idEstadoCivil'].setValue(this.precandidato.idEstadoCivil);
      this.updateEmpleadoForm.controls['idEscolaridad'].setValue(this.precandidato.idEscolaridad);
      this.updateEmpleadoForm.controls['idEstadoEscolaridad'].setValue(this.precandidato.idEstadoEscolaridad);
      this.updateEmpleadoForm.controls['calle'].setValue(this.precandidato.calle);
      this.updateEmpleadoForm.controls['fechaNacimiento'].setValue(new Date(data.fechaNacimiento + ' 00:00:00'));
      this.updateEmpleadoForm.controls['cp'].setValue(this.precandidato.cp);
      this.getColonias(this.precandidato.cp);
      this.updateEmpleadoForm.controls['colonia'].setValue(this.precandidato.idColonia);
      this.updateEmpleadoForm.controls['numeroExterior'].setValue(this.precandidato.numeroExterior);
      this.updateEmpleadoForm.controls['numeroInterior'].setValue(this.precandidato.numeroInterior);
      this.updateEmpleadoForm.controls['telefonoMovil'].setValue(this.precandidato.telefonoMovil);
      this.updateEmpleadoForm.controls['telefonoFijo'].setValue(this.precandidato.telefonoFijo);
      this.updateEmpleadoForm.controls['tiempoTraslado'].setValue(this.precandidato.tiempoTraslado);
      this.updateEmpleadoForm.controls['idMedioTraslado'].setValue(this.precandidato.idMedioTraslado);
      this.getLineas(this.precandidato.idMedioTraslado);
      this.estacionesService.getEstacionById(this.precandidato.idEstacion).subscribe(response => {
        this.updateEmpleadoForm.controls['lineaMetro'].setValue(response.lineas);
        this.getEstaciones(response.lineas);
        this.updateEmpleadoForm.controls['idEstacion'].setValue(this.precandidato.idEstacion);
      });
    });
  }
  // getMarcaEmprearial(idSede) {
  //   this.marcaEmpresarialService.get().pipe(map(result => {
  //     return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
  //   })).subscribe(data => {
  //     this.marcaEmpresarial = data;
  //     if (this.marcaEmpresarial.length === 0) {
  //       this.mostrarMarcasEmpresariales = true;
  //     } else {
  //       this.mostrarMarcasEmpresariales = false;
  //     }
  //     this.marcasEmpresariales  = [];
  //     for (let i = 0; i < this.marcaEmpresarial.length; i++) {
  //       this.marcasEmpresariales.push({'label': this.marcaEmpresarial[i].marca, 'value': this.marcaEmpresarial[i].id});
  //     }
  //   });
  // }
  /** Función para obtener el estado civil del empleado y lo almacena en el arreglo "civil". */
  getEstadoCivil() {
    this.estadoCivilService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoCivil = result;
      this.civil = [];
      for (let i = 0; i < this.estadoCivil.length; i++) {
        this.civil.push({'label': this.estadoCivil[i].descripcion, 'value': this.estadoCivil[i].id});
      }
    });
  }

  getTurnos() {
    this.turnoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Turnos = data;
    });
  }

  /** Función para obtener el nivel de escolaridad del empleado y lo almacena en el arreglo "gradoEscolar". */
  getEscolaridad() {
    this.escolaridadService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.escolaridad = result;
      this.gradoEscolar = [];
      for (let i = 0; i < this.escolaridad.length; i++) {
        this.gradoEscolar.push({'label': this.escolaridad[i].nivel, 'value': this.escolaridad[i].id});
      }
    });
  }

  /** Función para obtener el estado de escolaridad del empleado y lo almacena en el arreglo "estado". */
  getEstadoEscolaridad() {
    this.estadoEscolarService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoEscolar = result;
      this.estado = [];
      for (let i = 0; i < this.estadoEscolar.length; i++) {
        this.estado.push({'label': this.estadoEscolar[i].estado, 'value': this.estadoEscolar[i].id});
      }
    });
  }

  /** Función para obtener el país de origen del empleado y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }

  /** Función para obtener la información acerca del medio de transporte que utiliza el empleado para llegar a la empresa
   * y lo almacena en el arreglo "medio". */
  getMedioTransporte() {
    this.medioTransporteService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.medioTransporte = result;
      this.medio = [];
      for (let i = 0; i < this.medioTransporte.length; i++) {
        this.medio.push({'label': this.medioTransporte[i].medio, 'value': this.medioTransporte[i].id});
      }
    });
  }

  /** Función que obtiene el ID del medio de transporte utilizado. */
  getLineas(idMedioTraslado) {
    this.estacionesService.getByIdMedio(idMedioTraslado).subscribe(data => {
      this.lineas = [];
      this.estacionesLineas = data;
      // if (idMedioTraslado === 1 || idMedioTraslado === 2) { // Si el medio seleccionado es metro o metrobus
      //   this.lineaDropdown.value = 'Selecciona una línea';  // Reseteando el dropdown de linea del metro
      //   this.estaciones = [];
      // }
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.lineas.push({'label': this.estacionesLineas[i].lineas, 'value': this.estacionesLineas[i].lineas});
      }
    });
    this.updateEmpleadoForm.controls['lineaMetro'].setValue('');
    this.updateEmpleadoForm.controls['idEstacion'].setValue('');
    this.estaciones = [];
    this.estacionesLineas = [];
  }

  /** Función que obtiene las líneas del metro y las almacena en el arreglo "estaciones". */
  getEstaciones(lineaMetro) {
    /**console.log(lineaMetro);*/
    this.estacionesService.getEstacionByIdLinea(this.updateEmpleadoForm.value.idMedioTraslado, lineaMetro).subscribe(data => {
      this.estacionesLineas = data;
      this.estaciones = [];
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.estaciones.push({'label': this.estacionesLineas[i].estacion, 'value': this.estacionesLineas[i].id});
        /**console.log(this.estacionesLineas[i].estacion);*/
      }
      this.estacionesLineas = [];
    });
  }

  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }

  /** Función que obtiene el puesto desempeñado por el empleado y lo almacena en el arreglo "puest". */
  getPuesto() {
    this.puestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.puestos = data;
      if (this.puestos.length === 0) {
        this.mostrarPuesto = true;
      }else {
        this.mostrarPuesto = false;
      }
      this.puesto = [];
      for (let i = 0; i < this.puestos.length; i++) {
        this.puesto.push({'label': this.puestos[i].nombre, 'value': this.puestos[i].id});
      }
    });
  }

  /** Función para conocer el tipo de puesto que funge el empleado y lo almacena en el arreglo "puestoTipo". */
  getPuestoTipo() {
    this.puestoTipoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.puestoTipo = result;
      this.puestotipo = [];
      for (let i = 0; i < this.puestoTipo.length; i++) {
        this.puestotipo.push({'label': this.puestoTipo[i].nombre, 'value': this.puestoTipo[i].id});
      }
    });
  }

  /** Función para conocer la subárea en la que el empleado se encuentra y la agrega al arreglo "subarea". */
  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }

  /** Función que obtiene el atributo Empresa y lo agrega al arreglo "empres". */
  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  getSedes(idEmpresa) {
    this.sedesService.get().pipe(map(result => {
      return result.filter( valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarSede = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }

  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      }else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  getTipoPuesto() {
    this.tipoPuestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.tipoPuestos = data;
      this.tipoPuesto = [];
      for (let i = 0; i < this.tipoPuestos.length; i++) {
        this.tipoPuesto.push({'label': this.tipoPuestos[i].nombre, 'value': this.tipoPuestos[i].id});
      }
    });
  }

  /** Función que obtiene el atributo Banco y lo agrega al arreglo "banco" */
  getBanco() {
    this.bancoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.bancos = result;
      this.banco = [];
      for (let i = 0; i < this.bancos.length; i++) {
        this.banco.push({'label': this.bancos[i].nombre, 'value': this.bancos[i].id});
      }
    });
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    // if (cp.length === 5) {
      this.sepomexService.getColoniaByCp(cp)
        .pipe(map(data => this.cpSepomex = data)).subscribe(data => {
        this.colonias = [];
        if (data.length === 0) {
          this.mostrar = true;
        }else {
          this.mostrar = false;
        }
        for (let i = 0; i < this.cpSepomex.length; i++) {
          this.colonias.push({'label': this.cpSepomex[i].asenta, 'value': this.cpSepomex[i].idCp});
        }
      });
    // } else {
    //   this.mostrar = true;
    // }
  }

  getFoto(id: number) {
    this.empleadosService.getFoto(id).subscribe( data => {
      // Pasa el arreglo de bytes que se recibe del microservicio a un String para que este pueda ser convertido a imagen
      const cadena = String.fromCharCode.apply(null, new Uint8Array(data));
      if (cadena === '') {
        this.fotoDisponible = false;
      } else {
        this.fotoEmpleado = this.sanitizer.bypassSecurityTrustUrl(cadena);
        this.fotoDisponible = true;
      }
    });
  }

  /** Función que inicializa el arreglo "genero" para usarlo en el Select. */
  generoSelect() {
    this.genero = [];
    this.genero.push({label: 'Masculino', value: 'M'});
    this.genero.push({label: 'Femenino', value: 'F'});
  }

  /** Función que inicializa el arreglo "tiempoLlegada" para usarlo en el Select. */
  tiempoTrasladoSelect() {
    this.tiempoLlegada = [];
    this.tiempoLlegada.push({label: '0 - 15 min', value: 15});
    this.tiempoLlegada.push({label: '15 - 30 min', value: 30});
    this.tiempoLlegada.push({label: '30 - 45 min', value: 45});
    this.tiempoLlegada.push({label: '45 - 60 min', value: 60});
    this.tiempoLlegada.push({label: '60 - 1:15 hrs', value: 75});
    this.tiempoLlegada.push({label: '1:15 - 1:30 hrs', value: 90});
    this.tiempoLlegada.push({label: '1:30 - 1:45 hrs', value: 105});
    this.tiempoLlegada.push({label: '1:45- 2:00 hrs', value: 120});
  }

  /** Esta función actualiza todos los datos del empleado (datos personales y documentos). */
  update() {
    this.inactivo = true;
    // this.updateDatosEmpresariaes.value.documentos =
    //   '{"rfc":"' + this.updateDatosEmpresariaes.value.rfc + '","imss":"' + this.updateDatosEmpresariaes.value.imss + '"}';
    let formEmp: any;
    formEmp = {
      'comentarios': this.updateDatosEmpresariaes.controls['comentarios'].value,
      'ctaClabe': this.updateDatosEmpresariaes.controls['ctaClabe'].value,
      'tarjetaSecundaria': this.updateDatosEmpresariaes.controls['tarjetaSecundaria'].value,
      'documentosAdministrativos': this.updateDatosEmpresariaes.controls['documentosAdministrativos'].value,
      'documentosPersonales': this.updateDatosEmpresariaes.controls['documentosPersonales'].value,
      'fechaAltaIMSS': this.updateDatosEmpresariaes.controls['fechaAltaIMSS'].value,
      'fechaAsignacion': this.updateDatosEmpresariaes.controls['fechaAsignacion'].value,
      'fechaCambioSueldo': this.updateDatosEmpresariaes.controls['fechaCambioSueldo'].value,
      'fechaIngreso': moment(this.updateDatosEmpresariaes.controls['fechaIngreso'].value).format('YYYY-MM-DD'),
      'fechaRegistro': this.updateDatosEmpresariaes.controls['fechaRegistro'].value,
      'idBanco': this.updateDatosEmpresariaes.controls['idBanco'].value,
      'idCandidato': this.updateDatosEmpresariaes.controls['idCandidato'].value,
      'idPuesto': this.updateDatosEmpresariaes.controls['idPuesto'].value,
      'idTipoPuesto': this.updateDatosEmpresariaes.controls['idTipoPuesto'].value,
      'idUsuario': this.updateDatosEmpresariaes.controls['idUsuario'].value,
      'imss': this.updateDatosEmpresariaes.controls['imss'].value,
      'kpi': '',
      'kpiMensual': this.updateDatosEmpresariaes.controls['kpiMensual'].value,
      'kpiSemestral': this.updateDatosEmpresariaes.controls['kpiSemestral'].value,
      'kpiTrimestral': this.updateDatosEmpresariaes.controls['kpiTrimestral'].value,
      'puestoDetalle': this.updateDatosEmpresariaes.controls['puestoDetalle'].value,
      'razonSocial': this.updateDatosEmpresariaes.controls['razonSocial'].value,
      'rfc': this.updateDatosEmpresariaes.controls['rfc'].value,
      'sueldoDiario': this.updateDatosEmpresariaes.controls['sueldoDiario'].value,
      'sueldoMensual': this.updateDatosEmpresariaes.controls['sueldoMensual'].value,
      'idTurnoEmpleado': this.updateDatosEmpresariaes.controls['idTurnoEmpleado'].value,
      // 'horario': this.updateDatosEmpresariaes.controls['idTurno'].value,
    };
    formEmp.fechaIngreso = Date.parse(formEmp.fechaIngreso + 'T01:00');
    this.empleadosService.getEmpleadoById(this.rutaActiva.snapshot.params.idEmpleado).subscribe(data => {
      if (data.idEtapa === 3 && data.idEstadoRH === 1 || data.idEtapa === 3 && data.idEstadoRH === 16) {
        if (this.fotoGuardada) {
          this.empleadosService.saveFoto(this.fileToUpload, this.rutaActiva.snapshot.params.idEmpleado).subscribe();
        }
        if (formEmp.kpiMensual !== 0 || formEmp.kpiTrimestral !== 0 || formEmp.kpiSemestral !== 0) {
          formEmp.kpi = 1;
        } else {
          formEmp.kpi = 0;
        }
        this.empleadosService.put(this.rutaActiva.snapshot.params.idEmpleado, formEmp,
          formEmp.fechaIngreso).subscribe(result => {
          this.empleadosService.postSocket(formEmp).subscribe(x => {});
          this.router.navigate(['modulos/rrhh/empleados']);
        });
      } else {
        this.empleadoEliminado = true;
      }
    });
  }

  saveDatosEmpleadosPersonales() {
    this.inactivo = true;
    this.formact = {
      'apellidoMaterno': this.updateEmpleadoForm.value.apellidoMaterno,
      'apellidoPaterno': this.updateEmpleadoForm.value.apellidoPaterno,
      'calle': this.updateEmpleadoForm.value.calle,
      'idColonia': this.updateEmpleadoForm.value.colonia,
      'cp': this.updateEmpleadoForm.value.cp,
      'curp': this.updateEmpleadoForm.value.curp,
      'email': this.updateEmpleadoForm.value.email,
      'fechaNacimiento': moment(this.updateEmpleadoForm.value.fechaNacimiento).format('YYYY-MM-DD'),
      'genero': this.updateEmpleadoForm.value.genero,
      'idEscolaridad': this.updateEmpleadoForm.value.idEscolaridad,
      'idEstacion': this.updateEmpleadoForm.value.idEstacion,
      'idEstadoCivil': this.updateEmpleadoForm.value.idEstadoCivil,
      'idPais': this.updateEmpleadoForm.value.idPais,
      'idEstadoEscolaridad': this.updateEmpleadoForm.value.idEstadoEscolaridad,
      'idEstadoRH': this.precandidato.idEstadoRH,
      'idEtapa': 3,
      'idMedioTraslado': this.updateEmpleadoForm.value.idMedioTraslado,
      'idSolicitudRRHH': this.updateEmpleadoForm.value.idSolicitudRRHH,
      'nombre': this.updateEmpleadoForm.value.nombre,
      'numeroExterior': this.updateEmpleadoForm.value.numeroExterior,
      'numeroInterior': this.updateEmpleadoForm.value.numeroInterior,
      'telefonoFijo': this.updateEmpleadoForm.value.telefonoFijo,
      'telefonoMovil': this.updateEmpleadoForm.value.telefonoMovil,
      'tiempoTraslado': this.updateEmpleadoForm.value.tiempoTraslado,
    };
    this.formact.fechaNacimiento = Date.parse(this.formact.fechaNacimiento + 'T01:00');
    this.empleadosService.getEmpleadoById(this.rutaActiva.snapshot.params.idEmpleado).subscribe(data => {
      if (data.idEtapa === 3 && data.idEstadoRH === 1 || data.idEtapa === 3  && data.idEstadoRH === 16) {
        this.precandidatoService.updatePrecandidato(this.rutaActiva.snapshot.params.idPrecandidato,
          this.formact, this.formact.fechaNacimiento).subscribe(result => {
          this.empleadosService.postSocket(this.formact).subscribe(x => {});
          this.router.navigate(['modulos/rrhh/empleados']);
        });
      } else {
        this.empleadoEliminado = true;
      }
    });
  }


  habilita() {
    this.updateDatosEmpresariaes.controls['ctaClabe'].setValue('');
    if (this.radioCuenta.checked) {
      this.maxNum = 10;
      this.minNum = 10;
    }else if (this.radioClave.checked) {
      this.maxNum = 16;
      this.minNum = 16;
    }
  }
  /**Funcion para activar o desactivar los radiobuttons dependiendo del tipo de cuenta clabe*/
  radioButtonsCuentaClabe(clabe) {
    this.labelNumeroCuenta = 'Clabe incorrecta';
    this.radioCuentaCheck = false;
    this.radioClaveCheck = false;
      if (clabe.toString().length === 10) {
        this.radioCuentaCheck = true;
        this.maxNum = 10;
        this.minNum = 10;
        this.labelNumeroCuenta = 'Cuenta Clabe';
      }
      if (clabe.toString().length === 16) {
        this.radioClaveCheck = true;
        this.maxNum = 16;
        this.minNum = 16;
        this.labelNumeroCuenta = 'Clave Interbancaria';
      }
  }

  /** Función que manda a llamar a todos los demás métodos al inicializar el componente. */
  ngOnInit() {
    this.idTipo = this.rutaActiva.snapshot.params.idTipo;
    this.getEmpleado();
    this.getEstadoCivil();
    this.getEscolaridad();
    this.getEstadoEscolaridad();
    this.getPaises();
    this.getMedioTransporte();
    this.getPuesto();
    this.getPuestoTipo();
    // this.getSubArea();
    // this.getEmpresa();
    this.getBanco();
    this.getGrupo();
    this.getTipoPuesto();
    this.generoSelect();
    this.tiempoTrasladoSelect();
    this.getEmpleadoPersonalesById();
    this.getTurnos();
    this.inactivo = false;
  }

  /** Función que permite activar la WebCam, sino está activada o conectada, manda un error (alert). */
  activarCamara() {
    navigator.mediaDevices.getUserMedia({video: true})
      .then(stream => {
        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.play();

        this.video.nativeElement.height = this.altoWebCam;
        this.video.nativeElement.width = this.anchoWebCam;

        this.canvas.nativeElement.height = this.altoWebCam;
        this.canvas.nativeElement.width = this.anchoFoto;

        this.canvasVideo.nativeElement.height = this.altoWebCam;
        this.canvasVideo.nativeElement.width = this.anchoFoto;
        this.ventanaFoto = true;
        this.fotoTomada = false;
        /** El video que se muestra en la pantalla está dentro de un canvas y en esta parte del código es donde se asignan
         los datos que va mandando "frame por frame"*/
        this.video.nativeElement.addEventListener('play',
          funcion => {
            setInterval(
              dibujaFrame => {
                this.canvasVideo.nativeElement.getContext('2d')
                  .drawImage(this.video.nativeElement, 0, 0, this.anchoWebCam, this.altoWebCam);
              }
              , 0);
          }, false);
      })
      .catch(controlError => {
        alert('¡Conecta una camara Web y autoriza que el navegador acceda a ella!');
      });
  }

  /** Función que permite tomar la foto de la WebCam. */
  tomarFoto() {
    this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, this.anchoWebCam, this.altoWebCam);
    this.fotoTomada = true;
  }

  /** Función que permite guardar en una variable la foto que ha sido tomada en el canvas. */
  guardarFoto() {
    this.foto = this.canvas.nativeElement.toDataURL('image/jpeg');
    this.fotoEmpleado = this.foto;
    this.ventanaFoto = false;
    this.fileToUpload = new File([this.foto], 'imagen', { type: 'image/jpeg', lastModified: Date.now()});
    this.video.nativeElement.srcObject.getTracks()[0].stop();
    this.fotoGuardada = true;
    if (!this.fotoDisponible) {
      this.fotoDisponible = true;
    }
  }

  /** Función que permite cancelar el proceso para tomar la foto al empleado, desactivando la cámara y cerrando la ventana de tomar foto. */
  cancelarFoto() {
    this.video.nativeElement.srcObject.getTracks()[0].stop();
    this.ventanaFoto = false;
  }
}
