import { Component, Input, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {NbDialogRef} from '@nebular/theme';
import {Tablet, TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Procesador, ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {SelectItem} from 'primeng/api';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-tablet-create',
  templateUrl: './tablet-create.component.html',
  styleUrls: ['./tablet-create.component.scss'],
})
export class TabletCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTablet: number;
  tablet: Tablet;
  tabletCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  procesadores: Procesador[];
  procesador: any;

  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];

  mostrarSede: boolean;

  constructor(private tabletService: TabletData, private fb: FormBuilder, protected ref: NbDialogRef<TabletCreateComponent>,
              private router: Router, private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData,
              private procesadorService: ProcesadorSoporteData, private tipoSedeService: TipoSedeData) { }
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map( data => this.observaciones = data)).subscribe( data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({'label': this.observaciones[i].observaciones, 'value': this.observaciones[i].id});
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }


  getProcesador() {
    this.procesadorService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.procesador = [];
      this.procesadores = data;
      for (let i = 0; i < this.procesadores.length; i++) {
        this.procesador.push({'label': this.procesadores[i].nombreProcesador, 'value': this.procesadores[i].id});
      }
    });
  }

  guardarTablet() {
    if (this.tabletCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.tabletService.post(this.tabletCreateForm.value).subscribe((result) => {
      this.tabletCreateForm.reset();
      this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'tablet']);
    });
  }
  updateTablet() {
    this.tabletService.put(this.idTablet, this.tabletCreateForm.value).subscribe( result => {
      this.tabletCreateForm.reset();
      this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'tablet']);
    });
  }
  getTabletById() {
    this.tabletService.getTabletById(this.idTablet).subscribe( data => {
      this.tablet = data;
      this.tabletCreateForm.controls['numFolio'].setValue(this.tablet.numFolio);
      this.tabletCreateForm.controls['idProcesador'].setValue(this.tablet.idProcesador);
      this.tabletCreateForm.controls['capacidad'].setValue(this.tablet.capacidad);
      this.tabletCreateForm.controls['ram'].setValue(this.tablet.ram);
      this.tabletCreateForm.controls['idMarca'].setValue(this.tablet.idMarca);
      this.tabletCreateForm.controls['idEstadoInventario'].setValue(this.tablet.idEstadoInventario);
      this.tabletCreateForm.controls['idObservacionesInventario'].setValue(this.tablet.idObservacionesInventario);
      this.tabletCreateForm.controls['comentarios'].setValue(this.tablet.comentarios);
      this.tabletCreateForm.controls['idEmpleado'].setValue(this.tablet.idEmpleado);
      this.tabletCreateForm.controls['idTipoSede'].setValue(this.tablet.idTipoSede);
      this.tabletCreateForm.controls['origenRecurso'].setValue(this.tablet.origenRecurso);
    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result  => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2 ) {
      this.getTabletById();
    }
    this.getMarca();
    this.getProcesador();
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getOrigen();
    this.getSedes();
    this.tabletCreateForm = this.fb.group({
      'numFolio': new FormControl('', Validators.compose([Validators.required])),
      'idProcesador': new FormControl('', Validators.compose([Validators.required])),
      'capacidad': new FormControl('', Validators.compose([Validators.required])),
      'ram': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });
  }

}
