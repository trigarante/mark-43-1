export interface EstadoCotizacion {
    estado: string;
    descipcion: string;
    activo: number;
}
