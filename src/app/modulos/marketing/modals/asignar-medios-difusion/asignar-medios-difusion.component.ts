import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MediosDifusionComponent} from '../../medios-difusion/medios-difusion.component';
import {ProveedorLeads, ProveedorLeadsData} from '../../../../@core/data/interfaces/catalogos/proveedor-leads';
import {CampanaMarketing, CampanaMarketingData} from '../../../../@core/data/interfaces/marketing/campana';
import {map} from 'rxjs/operators';
import {NbDialogRef} from '@nebular/theme';
import {MediosDifusionData} from '../../../../@core/data/interfaces/marketing/medios-difusion';


@Component({
  selector: 'ngx-asignar-medios-difusion',
  templateUrl: './asignar-medios-difusion.component.html',
  styleUrls: ['./asignar-medios-difusion.component.scss'],
})
export class AsignarMediosDifusionComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idMedio: number;
  mediosDifusionForm: FormGroup;
  proovedorLead: ProveedorLeads[];
  proveedor: any[];
  capanasMarketing: CampanaMarketing[];
  camapanaMarketing: any[];
  submitted: boolean;
  inactivo: boolean;
  numeros: RegExp = /[0-9.,]+/;

  constructor(protected fb: FormBuilder, protected proveedorLeadoService: ProveedorLeadsData, protected mediosService: MediosDifusionData,
              protected campanasService: CampanaMarketingData, protected ref: NbDialogRef<AsignarMediosDifusionComponent>) {
  }

  getProveedorLead() {
    this.proveedorLeadoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.proovedorLead = result;
      this.proveedor = [];
      for (let i = 0; i < this.proovedorLead.length; i++) {
        this.proveedor.push({'label': this.proovedorLead[i].nombre, 'value': this.proovedorLead[i].id});
      }
    });
  }

  getCampanasMarketing() {
    this.campanasService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.capanasMarketing = result;
      this.camapanaMarketing = [];
      for (let i = 0; i < this.capanasMarketing.length; i++) {
        this.camapanaMarketing.push({'label': this.capanasMarketing[i].nombre, 'value': this.capanasMarketing[i].id});
      }
    });
  }

  guardarMediosDifusion() {
    if (this.mediosDifusionForm.invalid) {
      return;
    }
    this.submitted = true;
    this.mediosService.post(this.mediosDifusionForm.value).subscribe((result) => {
      this.mediosService.postSocket({status: 'ok'}).subscribe(x => {});
      this.mediosDifusionForm.reset();
      this.ref.close();
    });
  }

  updateMediosDifusion() {
    this.campanasService.put(this.idMedio, this.mediosDifusionForm.value).subscribe(data => {
        this.mediosService.postSocket({status: 'ok'}).subscribe(x => {});
        this.inactivo = true;
      },
      error => {
      },
      () => {
        this.ref.close();
        this.mediosDifusionForm.reset();
      });
  }

  getMediosDifusionById() {
    this.mediosService.getMediosDifusionById(this.idMedio).subscribe(data => {
      this.mediosDifusionForm.controls['idCampana'].setValue(data.idCampana);
      this.mediosDifusionForm.controls['idProveedor'].setValue(data.idProveedor);
      this.mediosDifusionForm.controls['regla'].setValue(data.regla);
    });
  }

  formatoNumerosConComas(objetivo) {
    /**La asignacion de comas llega hasta 100,000,000,000 para mayor cantidad, agregar maas validaciones o hacerlo dinamico*/
    let formatNumber: any = '';
    objetivo = objetivo.replace(/,/g, '');
    if (objetivo.length > 3 && objetivo.length < 7) {
      formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
      this.mediosDifusionForm.controls['presupuesto'].setValue(formatNumber);
    } else if (objetivo.length > 6 && objetivo.length < 10) {
      formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
      this.mediosDifusionForm.controls['presupuesto'].setValue(formatNumber);
    } else if (objetivo.length > 9 && objetivo.length < 13) {
      formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
        ',' + objetivo.slice(-3);
      this.mediosDifusionForm.controls['presupuesto'].setValue(formatNumber);
    } else {
      this.mediosDifusionForm.controls['presupuesto'].setValue(objetivo);
    }
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getMediosDifusionById();
    }

    this.mediosDifusionForm = this.fb.group({
      'idProveedor': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'regla': new FormControl('', Validators.required),
      'presupuesto': new FormControl('', Validators.required),
      'idCampana': new FormControl('', Validators.required),
      'activo': 1,
    });
    this.getProveedorLead();
    this.getCampanasMarketing();
  }

  dismiss() {
    this.ref.close();
  }

}
