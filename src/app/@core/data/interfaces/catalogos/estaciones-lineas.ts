import {Observable} from 'rxjs';

export interface EstacionesLineas {
    id: number;
    idTransporte: number;
    estacion: string;
    lineas: string;
}

export abstract class EstacionesLineasData {
  abstract getByIdMedio(id): Observable<EstacionesLineas[]>;
  abstract getEstacionByIdLinea(idTransporte, idLinea): Observable<EstacionesLineas[]>;
  abstract getEstacionById(id): Observable<EstacionesLineas>;
}
