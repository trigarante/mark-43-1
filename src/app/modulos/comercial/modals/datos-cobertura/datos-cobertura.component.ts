import {Component, Input, OnInit} from '@angular/core';
import {Cobertura, CoberturaData} from '../../../../@core/data/interfaces/comerciales/cobertura';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {AsignarCoberturaComponent} from '../asignar-cobertura/asignar-cobertura.component';
import swal from 'sweetalert';

@Component({
  selector: 'ngx-datos-cobertura',
  templateUrl: './datos-cobertura.component.html',
  styleUrls: ['./datos-cobertura.component.scss'],
})
export class DatosCoberturaComponent implements OnInit {
  @Input() idCobertura: number;
  @Input() idProducto: number;
  alias: any;
  descripcion: any;
  detalle: any;
  estado: any;
  nombre: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoCobertura: any;
  tipoGiro: any;
  cobertura: Cobertura;
  permisos: any;
  escritura: boolean;

  constructor(private coberturaService: CoberturaData, protected ref: NbDialogRef<DatosCoberturaComponent>,
              private dialogService: NbDialogService) { }

  ngOnInit() {
    this.getCobertura();
    this.getPermisos();
  }

  getCobertura() {
    this.coberturaService.getCoberturaById(this.idCobertura).subscribe(
      result => {
        this.alias = result.alias;
        this.descripcion = result.descripcion;
        this.detalle = result.detalle;
        this.estado = result.estado;
        this.nombre = result.nombre;
        this.nombreComercial = result.nombreComercial;
        this.tipoCategoria = result.tipoSubRamo;
        this.tipoCobertura = result.tipoCobertura;
        this.tipoGiro = result.tipoRamo;
      },
    );
  }
  updateCobertura() {
    this.dialogService.open(AsignarCoberturaComponent, {
      context: {
        idTipo: 2,
        idProducto: this.idProducto,
        idCobertura: this.idCobertura,
      },
    });
  }

  bajaCobertura() {
    this.coberturaService.getCoberturaById(this.idCobertura).subscribe(data => {
      this.cobertura = data; });
    swal({
      title: '¿Deseas dar de baja esta Cobertura?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.cobertura.activo = 0;
        let coberturaForm: any;
        coberturaForm = {
          'activo': this.cobertura.activo,
          'id': this.cobertura.id,
          'idProducto': this.cobertura.idProducto,
          'idTipoCobertura': this.cobertura.idTipoCobertura,
          'detalle': this.cobertura.detalle,
        };
        this.coberturaService.put(this.idCobertura, coberturaForm).subscribe(result => {
        });
        swal('¡La Cobertura se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          /**window.location.reload();*/
        }));
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.cobertura.escritura;
  }
  dismiss() {
    this.ref.close();
  }


}
