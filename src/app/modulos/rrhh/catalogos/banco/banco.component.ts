import {Component, OnInit, ViewChild} from '@angular/core';
import {Banco, BancoData} from '../../../../@core/data/interfaces/catalogos/banco';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {BancoCreateComponent} from '../modals/banco-create/banco-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de banco (catálogo).
 */
@Component({
  selector: 'ngx-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.scss'],
})
export class BancoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  bancoss: Banco[];
  banco: Banco;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {BancoData} bancoService Servicio del componente banco para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio para diálogos (mensajes). */
  constructor(private bancoService: BancoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bancos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelBancos', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getBancos(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que despliega un modal para agregar un nuevo banco. */
  crearBanco() {
    this.dialogoService.open(BancoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getBancos(1);
    });
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Bancos`);
  }
  /** Función que obtiene el campo Banco. */
  getBancos(mensajeAct) {
    this.bancoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.bancoss = [];
      this.bancoss = data;
      this.dataSource = new MatTableDataSource(this.bancoss); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcón que obtiene la información de un banco según su ID. */
  getBancoById(idBanco) {
    this.bancoService.getBancoById(idBanco).subscribe(data => {
      this.banco = data;
    });
  }
  /** Función que despliega un modal en el cual se puede actualizar la información de un banco. */
  updateBanco(idBanco) {
    this.dialogoService.open(BancoCreateComponent, {
      context: {
        idTipo: 2,
        idBanco: idBanco,
      },
    }).onClose.subscribe(x => {
      this.getBancos(1);
    });
  }
  /** Función que despliega un modal para seleccionar si se da de baja un banco. */
  bajaBanco(idBanco) {
    this.getBancoById(idBanco);
    swal({
      title: '¿Deseas dar de baja este banco?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.banco['activo'] = 0;
        this.bancoService.getBancoById(idBanco).subscribe(data => {
          if (data.activo === 1) {
            this.bancoService.put(idBanco, this.banco).subscribe( result => {
              this.bancoService.postSocket({idBanco: idBanco, banco: this.banco}).subscribe(x => {});
            });
            swal('¡El banco se ha dado de baja exitosamente!', {
              icon: 'success',
            }).then((resultado => {
              this.getBancos(0);
            }));
          }else {
            swal('¡Ateción el banco ya ha sido dado de baja!', {
              icon: 'warning',
            }).then((resultado => {
              this.getBancos(0);
            }));
          }
        });
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nombre', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/catalogos/banco') {
        this.getBancos(0);
    //   }
    // }, 1000);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.banco.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.banco.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
