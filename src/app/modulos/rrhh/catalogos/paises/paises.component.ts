import {Component, OnInit, ViewChild} from '@angular/core';
import {Paises, PaisesData} from '../../../../@core/data/interfaces/catalogos/paises';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {PaisesCreateComponent} from '../modals/paises-create/paises-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss'],
})
export class PaisesComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  paises: Paises[];
  /** Arreglo para el país que se dará de baja. */
  pais: Paises;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * En este caso: Servicio de Países.
   * @param {PaisesData} paisesService Servicio del componente paises para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes (diálogos). */
  constructor(private paisesService: PaisesData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'paises');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelPaises', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPaises(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene los países y lo asigna al arreglo estadoEscolaridad. */
  getPaises(mensajeAct) {
    this.paisesService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.paises = [];
      this.paises = data;
      this.dataSource = new MatTableDataSource(this.paises); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Paises`);
  }
  /** Función que obtiene la información del país dependiendo de su ID. */
  getPaisesById(idPais) {
    this.paisesService.getPaisesById(idPais).subscribe(data => {
      this.pais = data;
    });
  }

  /** Función que despliega un modal del componente "PaisesCreateComponent" para poder "crear un país". */
  crearPaises() {
    this.dialogoService.open(PaisesCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getPaises(0);
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de un país. */
  updatePaises(idPais) {
    this.dialogoService.open(PaisesCreateComponent, {
      context: {
        idTipo: 2,
        idPais: idPais,
      },
    }).onClose.subscribe(x => {
      this.getPaises(1);
    });
  }

  /** Función que despliega un modal para dar de baja un país. */
  bajaPaises(idPais) {
    /*this.dialogoService.open(PaisesCreateComponent, {
      context: {
        idTipo: 3,
        idPais: idPais,
      },
    });*/
    this.getPaisesById(idPais);
    swal({
      title: '¿Deseas dar de baja este país?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.pais['activo'] = 0;
        this.paisesService.put(idPais, this.pais).subscribe( result => {
          this.paisesService.postSocket({idPais: idPais, pais: this.pais}).subscribe(x => {});
        });
        swal('¡El país se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getPaises(0);
        }));
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.cols = ['nombre', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getPaises(0);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.paises.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.paises.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
