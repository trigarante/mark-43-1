import {Injectable} from '@angular/core';
import {MediosDifusion, MediosDifusionData} from '../../interfaces/marketing/medios-difusion';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class MediosDifusionService extends MediosDifusionData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<MediosDifusion[]> {
    return this.http.get<MediosDifusion[]>(this.baseURL + 'v1/medio-difusion');
  }

  post(mediosDifusion: MediosDifusion): Observable<MediosDifusion> {
    return this.http.post<MediosDifusion>(this.baseURL + 'v1/medio-difusion', mediosDifusion);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'mediodifusion/saveMedioDifusion', json);
  }
  getMediosDifusionById(idMediosDifusion): Observable<MediosDifusion> {
    return this.http.get<MediosDifusion>(this.baseURL + 'v1/medio-difusion/' + idMediosDifusion);
  }

  put(idMediosDifusion, mediosDifusion: MediosDifusion) {
    return this.http.put<MediosDifusion>(this.baseURL + 'v1/medio-difusion/' + idMediosDifusion + '/'
      + sessionStorage.getItem('Empleado'), mediosDifusion);
  }

  geMediosDifusionById(idMediosDifusion): Observable<MediosDifusion> {
    return undefined;
  }
}
