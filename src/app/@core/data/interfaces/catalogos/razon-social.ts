import {Observable} from 'rxjs';

export interface RazonSocial {
  id: number;
  activo: number;
  nombreComercial: string;
  alias: string;
}

export abstract class RazonSociosData {
  abstract get(): Observable<RazonSocial[]>;

  abstract post(razonSocial): Observable<RazonSocial>;
  abstract postSocket(json): Observable<JSON>;

  abstract getAreaById(idRazonSocial): Observable<RazonSocial>;

  abstract put(idRazonSocial, razonSocial): Observable<RazonSocial>;
}
