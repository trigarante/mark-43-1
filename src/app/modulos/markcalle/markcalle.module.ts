import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/primeng';
import { MarkcalleRoutingModule } from './markcalle-routing.module';
import { MarkcalleComponent } from './markcalle.component';
import { StartMarkcalleComponent } from './start-markcalle/start-markcalle.component';

import { LogmanualMarkcalleComponent } from './agent/logManual-markcalle/logmanual-markcalle.component';




import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {NbAlertModule, NbDatepickerModule, NbDialogModule, NbSelectModule, NbToastrModule, NbMenuModule} from '@nebular/theme';
import {DialogModule, DropdownModule, KeyFilterModule} from 'primeng/primeng';
import { AgendaMarkcalleComponent } from './agent/agenda-markcalle/agenda-markcalle.component';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { LogacdMarkcalleComponent } from './agent/logAcd-markcalle/logacd-markcalle.component';
import { LogcallbackMarkcalleComponent } from './agent/logCallback-markcalle/logcallback-markcalle.component';
import { LogpredictivaMarkcalleComponent } from './agent/logPredictiva-markcalle/logpredictiva-markcalle.component';
import { LogprogresivaMarkcalleComponent } from './agent/logProgresiva-markcalle/logprogresiva-markcalle.component';
import {ClockMarkcalleComponent} from './clock-markcalle/clock-markcalle.component';


@NgModule({
  declarations: [
    MarkcalleComponent,
    StartMarkcalleComponent,
    // MenuMarkcalleComponent,
    AgendaMarkcalleComponent,
    // InfoMarkcalleComponent,
    // ClockMarkcalleComponent,
    // LlamadasComponent,
   AgendaMarkcalleComponent,
    LogmanualMarkcalleComponent,
    LogacdMarkcalleComponent,
    LogcallbackMarkcalleComponent,
    LogpredictivaMarkcalleComponent,
    LogprogresivaMarkcalleComponent,
    ClockMarkcalleComponent,

  ],
  imports: [
    CommonModule,
    MarkcalleRoutingModule,
    TableModule,
    FormsModule,
    CalendarModule,
    InputTextModule,
    CommonModule,
    ThemeModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    FullCalendarModule,
  ],
})
export class MarkcalleModule {
}
