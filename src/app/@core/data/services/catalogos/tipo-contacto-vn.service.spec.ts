import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {TipoContactoVnService} from './tipo-contacto-vn.service';

describe('TipoContactoVn service', () => {
  let injector: TestBed;
  let service: TipoContactoVnService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoContactoVnService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(TipoContactoVnService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
})
