import {getTestBed, TestBed} from '@angular/core/testing';

import { CoberturaService } from './cobertura.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('CoberturaService', () => {
  let injector: TestBed;
  let service: CoberturaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idProducto: 2,
    idTipoCobertura: 1,
    detalle: 'testfinal',
    activo: 0,
    tipoCobertura: 'TOTAL01--',
    idSubRamo: 1,
    prioridad: 1,
    nombre: 'testing',
    idRamo: 30,
    idTipoSubRamo: 1,
    tipoRamo: 'SEGUROS',
    descripcion: 'TEST FINALL',
    nombreComercial: 'KK',
    alias: 'KK',
    idEstadoSocio: 2,
    estado: 'INACTIVO',
    idEstado: 2,
    tipoSubRamo: 'AUTO',
  },
    {
      id: 'coberturas',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CoberturaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(CoberturaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/coberturas');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/coberturas', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/coberturas');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/coberturas', '/1/74');
      });
    });
  }

});
