import {Observable} from 'rxjs';

export interface EstadoCivil {
    id: number;
    descripcion: string;
    activo: number;
}

export abstract class EstadoCivilData {
  abstract get(): Observable<EstadoCivil[]>;
  abstract post(estadoCivil: EstadoCivil): Observable<EstadoCivil>;
  abstract postSocket(json): Observable<JSON>;
  abstract getEstadoCivilById(idEstadoCivil): Observable<EstadoCivil>;
  abstract put(idEstadoCivil, estadoCivil): Observable<EstadoCivil>;
}
