import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import swal from 'sweetalert';
import {Ramo, RamoData} from '../../../../@core/data/interfaces/comerciales/ramo';
import {AsignarRamoComponent} from '../asignar-ramo/asignar-ramo.component';
@Component({
  selector: 'ngx-datos-ramo',
  templateUrl: './datos-ramo.component.html',
  styleUrls: ['./datos-ramo.component.scss'],
})
export class DatosRamoComponent implements OnInit {
  @Input() idRamo: number;
  @Input() idSocio: number;
  ramo: Ramo;
  alias: any;
  descripcion: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  tipoRamo: any;
  ramos: Ramo;
  permisos: any;
  escritura: boolean;

  constructor(private ramoService: RamoData, protected ref: NbDialogRef<DatosRamoComponent>, private dialogService: NbDialogService) {
  }

  getRamoById() {
    this.ramoService.getRamoById(this.idRamo).subscribe(data => {
      this.ramo = data;
      this.alias = this.ramo.alias;
      this.descripcion = this.ramo.descripcion;
      this.nombreComercial = this.ramo.nombreComercial;
      this.estado = this.ramo.estado;
      this.prioridades = this.ramo.prioridades;
      this.tipoRamo = this.ramo.tipoRamo;
    });
  }
  /**Funcion actualizar ramo*/
  updateRamo() {
    this.dialogService.open(AsignarRamoComponent, {
      context: {
        idTipo: 2,
        idRamo: this.idRamo,
        idSocio: this.idSocio,
      },
    });
  }
  getRamosById(idRamo) {
    this.ramoService.getRamoById(idRamo).subscribe(data => {
      this.ramos = data;
    });
  }
  bajaRamo() {
    this.getRamosById(this.idRamo);
    swal({
      title: '¿Deseas dar de baja este Ramo?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.ramos.activo = 0;
        this.ramoService.put(this.idRamo, this.ramos).subscribe(result => {
        });
        swal('¡El Ramo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          /**this.getGiro(0);*/
        }));
      }
    });
  }
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getRamoById();
    this.getPermisos();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.ramo.escritura;
  }
}
