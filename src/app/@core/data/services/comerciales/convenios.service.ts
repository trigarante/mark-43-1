import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Convenios, ConveniosData} from '../../interfaces/comerciales/convenios';
import {Observable} from 'rxjs';

@Injectable()
export class ConveniosService extends ConveniosData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Convenios[]> {
    return this.http.get<Convenios[]>(this.baseURL + 'v1/convenios');
  }

  post(convenios: Convenios): Observable<Convenios> {
    return this.http.post<Convenios>(this.baseURL + 'v1/convenios', convenios);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'convenios/saveConvenio/1', json);
  }

  getConveniosById(idConvenios): Observable<Convenios> {
    return this.http.get<Convenios>(this.baseURL + 'v1/convenios/' + idConvenios);
  }

  put(idConvenios, convenios: Convenios): Observable<Convenios> {
    return this.http.put<Convenios>(this.baseURL + 'v1/convenios/' + idConvenios + '/'
      + sessionStorage.getItem('Empleado'), convenios);
  }
}
