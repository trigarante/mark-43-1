import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material';
import {Pagina, PaginasData} from '../../../@core/data/interfaces/marketing/pagina';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AgregarCamposComponent} from '../modals/agregar-campos/agregar-campos.component';
import {NbDialogService} from '@nebular/theme';
import {AgregarNodoComponent} from '../modals/agregar-nodo/agregar-nodo.component';
import {ModificarDatosComponent} from '../modals/modificar-datos/modificar-datos.component';
import {AgregarAseguradoraComponent} from '../modals/agregar-aseguradora/agregar-aseguradora.component';
import {DatosNodosComponent} from '../modals/datos-nodos/datos-nodos.component';
import {environment} from '../../../../environments/environment';
import {AgregarTipoComponent} from '../modals/agregar-tipo/agregar-tipo.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-aseguradoras',
  templateUrl: './aseguradoras.component.html',
  styleUrls: ['./aseguradoras.component.scss'],
})
export class AseguradorasComponent implements OnInit {
  /** variable que se puede obtener de la pagina*/
  id: number = this.rutaActiva.snapshot.params.id;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  pagina: Pagina;
  aseguradoras: any;
  nombres: any[];
  nombresprincipales: any[];
  tiponombre: string;
  camposaseguradora: any;
  valoraseguradora: any;
  /** Variable que permite la inicialización del formulario pagina. */
  paginasForm: FormGroup;
  cols: any[];
  dataSource: any;
  arregloconfiguracionjson: any[];
  siexisteaseguradora: any;
  astring: any;
  mostrarselec: boolean;
  mostrarselec2: boolean;
  /** valor que obtiene para identificar la posicion de la aseguradora*/
  nombreaseguradora: any;
  // sockets
  private stompClient = null;

  constructor( private rutaActiva: ActivatedRoute, private paginaService: PaginasData, private fb: FormBuilder,
               public dialogService: NbDialogService) {
    this.paginasForm = this.fb.group({
      'aseguradoras': new FormControl(),
      'nombreprincipal': new FormControl(),
    });
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'aseguradoras');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelAseguradoras', (content) => {
        if (sessionStorage.getItem('nombreAseguradora') || content.body) {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getaseguradoras(sessionStorage.getItem('nombreAseguradora'));
          }, 500);
        }
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
   recarga() {
     this.paginaService.getPaginaById(this.id).subscribe(data => {
       this.pagina = data;
       this.aseguradoras = JSON.parse(this.pagina.configuracion);
       this.siexisteaseguradora = Object.keys(this.aseguradoras);
       this.astring = JSON.stringify(this.siexisteaseguradora);
       if (this.astring === '["aseguradoras"]' ) {
         this.mostrarselec = false;
         /** obtiene el valor de los name, los nombres de todas las aseguradoras*/
         this.nombres = [];
         for (let i = 0; i < this.aseguradoras.aseguradoras.length; i++) {
           this.nombres.push({
             'value': i, 'label': this.aseguradoras.aseguradoras[i].aseguradora,
           });
         }
       }
     });
   }
   /** Metodo inicial que carga el dropdonw que muestra las aseguradoras*/
  getPaginas() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
       /** Se obtiene los datos para ver si existe un campo llamado aseguradoras y despues se pasa a string*/
      this.siexisteaseguradora = Object.keys(this.aseguradoras);
      this.astring = JSON.stringify(this.siexisteaseguradora);


        /** si no existe aseguradoras, va directo a mostrar los datos directamente a la tabla de solo una aseguradora*/
      if (this.astring !== '["aseguradoras"]') {

        this.mostrarselec = false;
        this.mostrarselec2 = true;
            /**obtiene los tipos de nombres principales y los muestra en el select y podra seleccionar las aseguradoras
             * que esten en ese tipo*/
        this.nombresprincipales = [];
        this.nombresprincipales = Object.keys(this.aseguradoras);


      }
      /** si el json tiene aseguradoras mostrara el select y seleccionara los datos dependiendo de la asegurador         --ya*/
      if (this.astring === '["aseguradoras"]' ) {
        this.mostrarselec = false;
        /** obtiene el valor  los nombres de todas las aseguradoras*/
         this.nombres = [];
        for (let i = 0; i < this.aseguradoras.aseguradoras.length; i++) {
          this.nombres.push({  'value': i, 'label': this.aseguradoras.aseguradoras[i].aseguradora,
          });
        }
      }
    });
  }
  /**Funcion que dependiendo del metodo mostrara las aseguradoras*/
  getnombres(event) {

    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      this.nombres = [];
      for (let i = 0; i < this.aseguradoras[event].aseguradoras.length; i++) {
        this.nombres.push({
          'value': i, 'label': this.aseguradoras[event].aseguradoras[i].nombre,
        });
      }
      this.dataSource = new MatTableDataSource();
    });
    this.tiponombre = event;
  }
  /** Metodo que dependiendo de la aseguradora seleccionada se mostrara los campos y valores en la tabla*/
  getaseguradoras(event) {
    sessionStorage.setItem('nombreAseguradora', event);
    /** dependiendo del select mostrara el contienido de una aseguradora*/
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      /** obtiene solo los campos name*/
      this.camposaseguradora = Object.keys(this.aseguradoras[this.tiponombre].aseguradoras[event]);
      /** obtiene solo los campos value*/
      this.valoraseguradora = Object.values(this.aseguradoras[this.tiponombre].aseguradoras[event]);
      this.arregloconfiguracionjson = [];
      for (let i = 0; i < this.camposaseguradora.length; i++) {
        this.arregloconfiguracionjson.push({'campo': this.camposaseguradora[i], 'value': this.valoraseguradora[i]});
      }

      this.dataSource = new MatTableDataSource(this.arregloconfiguracionjson);

    });
    this.nombreaseguradora = event;
  }
  getaseguradora(event) {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      /** obtiene solo los campos name*/
      this.camposaseguradora = Object.keys(this.aseguradoras);
      /** obtiene solo los campos value*/
      this.valoraseguradora = Object.values(this.aseguradoras);
      this.arregloconfiguracionjson = [];
      for (let i = 0; i < this.camposaseguradora.length; i++) {
        this.arregloconfiguracionjson.push({'campo': this.camposaseguradora[i], 'value': this.valoraseguradora[i]});
      }

      this.dataSource = new MatTableDataSource(this.arregloconfiguracionjson);
    });
    this.mostrarselec = true; this.dialogService.open(AgregarCamposComponent, {
      context: {
        id: this.id,
        nombreaseguradora: this.nombreaseguradora,

      },
    }).onClose.subscribe( x => {
      if (this.astring === '["aseguradoras"]' ) {
        this.getaseguradoras(this.nombreaseguradora);
      } else if (this.astring !== '["aseguradoras"]') {
        this.getaseguradora(this.nombreaseguradora);
      }
    });
  }
  /** Modal que que agrega campos a las aseguradora*/
  agregarCampos() {
    this.dialogService.open(AgregarCamposComponent, {
      context: {
        id: this.id,
        nombreaseguradora: this.nombreaseguradora,
        tiponombre: this.tiponombre,

      },
    }).onClose.subscribe( x => {
      if (this.astring !== '["aseguradoras"]' ) {
        this.getaseguradoras(this.nombreaseguradora);
      }
    });
  }
  /**  Modal para agregar nodos a las aseguradoras*/
  agregarNodo(nombreaseguradora) {
    this.dialogService.open(AgregarNodoComponent, {
      context: {
        id: this.id,
        nombre: nombreaseguradora,
        tiponombre: this.tiponombre,

      },
    }).onClose.subscribe(x => {
      this.getaseguradoras(this.nombreaseguradora);
    });
  }
  /** Modal para modificar los datos de las aseguradoras*/
  modificarDatos(campo, nombre) {
    this.dialogService.open(ModificarDatosComponent, {
      context: {
        campo: campo,
        nombre: nombre,
        id: this.id,
        nombreaseguradora: this.nombreaseguradora,
        tiponombre: this.tiponombre,
      },
    }).onClose.subscribe( x => {
      if (this.astring === '["aseguradoras"]' ) {
        this.getaseguradoras(this.nombreaseguradora);
      } else if (this.astring !== '["aseguradoras"]') {
        this.getaseguradoras(this.nombreaseguradora);
      }
    });

  }
  /** Modal para agregar nueva aseguradora*/
  agregarAseguradora() {
    this.dialogService.open( AgregarAseguradoraComponent, {
      context: {
        id: this.id,
        tiponombre: this.tiponombre,
      },
    }).onClose.subscribe(x => {
      this.getnombres(this.tiponombre);
    });

  }
  /** Modal para agregar nuevo Tipo*/
  agregarTipo() {
    this.dialogService.open( AgregarTipoComponent, {
      context: {
        id: this.id,
      },
    }).onClose.subscribe(x => {
       this.getPaginas();
    });

  }
  /** Modal para ver los campos del Nodo*/
  datosNodos( nombrenodo , id, nombreaseguradora) {
    this.dialogService.open( DatosNodosComponent, {
      context: {
        id: id,
        nombrenodo: nombrenodo,
        nombre: nombreaseguradora,
        tiponombre: this.tiponombre,
      },
    }).onClose.subscribe(x => {
      this.recarga();
    });
  }

  ngOnInit() {
    this.connect();
    this.getPaginas();
    this.cols = ['campo', 'valor', 'acciones'];
  }

}
