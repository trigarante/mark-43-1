import { TestBed } from '@angular/core/testing';

import { HistorialLlamadasService } from './historial-llamadas.service';

describe('HistorialLlamadasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistorialLlamadasService = TestBed.get(HistorialLlamadasService);
    expect(service).toBeTruthy();
  });
});
