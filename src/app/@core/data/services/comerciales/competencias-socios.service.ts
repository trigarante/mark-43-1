import {Injectable} from '@angular/core';
import {CompetenciaSocioData, CompetenciasSocios} from '../../interfaces/comerciales/competencias-socios';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CompetenciasSociosService extends CompetenciaSocioData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<CompetenciasSocios[]> {
    return this.http.get<CompetenciasSocios[]>(this.baseURL + 'v1/competencia-socio');
  }

  post(competenciaSocios: CompetenciasSocios): Observable<CompetenciasSocios> {
    return this.http.post<CompetenciasSocios>(this.baseURL + 'v1/competencia-socio', competenciaSocios);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'competencia/saveCompetencia/1', json);
  }
  getCompetenciaSociosById(idCompetenciaSocios): Observable<CompetenciasSocios> {
    return this.http.get<CompetenciasSocios>(this.baseURL + 'v1/competencia-socio/' + idCompetenciaSocios);
  }

  put(idCompetenciaSocios, competenciaSocios: CompetenciasSocios): Observable<CompetenciasSocios> {
    return this.http.put<CompetenciasSocios>(this.baseURL + 'v1/competencia-socio/' + idCompetenciaSocios + '/'
      + sessionStorage.getItem('Empleado') , competenciaSocios);
  }
}
