import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Competencia, CompetenciaData} from '../../interfaces/catalogos/competencia';

@Injectable()
export class CompetenciasService extends CompetenciaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Competencia[]> {
    return this.http.get<Competencia[]>(this.baseURL + 'v1/competencias');
  }

  post(competencia: Competencia): Observable<Competencia> {
    return this.http.post<Competencia>(this.baseURL + 'v1/competencias', competencia);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'competencias/saveCompetencia', json);
  }

  getCompetenciasById(idCompetencias): Observable<Competencia> {
    return this.http.get<Competencia>(this.baseURL + 'v1/competencias/' + idCompetencias);
  }

  put(idCompetencias, competencia: Competencia): Observable<Competencia> {
    return this.http.put<Competencia>(this.baseURL + 'v1/competencias/' + idCompetencias + '/'
      + sessionStorage.getItem('Empleado'), competencia);
  }
}
