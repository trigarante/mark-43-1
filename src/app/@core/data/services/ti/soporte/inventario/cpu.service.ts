import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Cpu, CpuData} from '../../../../interfaces/ti/soporte/inventario/cpu';

@Injectable()
export class CpuService extends CpuData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Cpu[]> {
    return this.http.get<Cpu[]>( this.baseURL + 'v1/cpu');
  }

  post(cpu: Cpu): Observable<Cpu> {
    return this.http.post<Cpu>(this.baseURL + 'v1/cpu', cpu);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'cpu/saveCpu', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajacpu/saveBajaCpu', json);
  }
  getCpuById(idCpu): Observable<Cpu> {
    return this.http.get<Cpu>(this.baseURL + 'v1/cpu/' + idCpu);
  }
  put(idCpu, cpu: Cpu): Observable<Cpu> {
    return this.http.put<Cpu>(this.baseURL + 'v1/cpu/' + idCpu + '/'
      + sessionStorage.getItem('Empleado'), cpu);
}

}
