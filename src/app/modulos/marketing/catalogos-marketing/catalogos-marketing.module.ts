import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CatalogosMarketingRoutingModule} from './catalogos-marketing-routing.module';
import {ThemeModule} from '../../../@theme/theme.module';
import {TableModule} from 'primeng/table';
import {NbDialogModule} from '@nebular/theme';
import {AutoCompleteModule, DropdownModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import {CatalogosMarketingComponent} from './catalogos-marketing.component';
import {TipoPaginaComponent} from './tipo-pagina/tipo-pagina.component';

import { ProveedorLeadComponent} from './provedor-lead/provedor-lead.component';
import {ProveedoresLeadCreateComponent} from './modals/proveedores-lead-create/proveedores-lead-create.component';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
} from '@angular/material';
import {TipoPaginaCreateComponent} from './modals/tipo-pagina-create/tipo-pagina-create.component';

@NgModule({
  declarations: [
    CatalogosMarketingComponent,
    TipoPaginaComponent,
    ProveedorLeadComponent,
    ProveedoresLeadCreateComponent,
    TipoPaginaCreateComponent,
  ],
  imports: [
    CommonModule,
    CatalogosMarketingRoutingModule,
    ThemeModule,
    TableModule,
    NbDialogModule.forChild(),
    DropdownModule,
    AutoCompleteModule,
    TooltipModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    KeyFilterModule,
  ],
  entryComponents: [
    ProveedoresLeadCreateComponent,
    TipoPaginaCreateComponent,
    ProveedoresLeadCreateComponent,
  ],
})
export class CatalogosMarketingModule {
}
