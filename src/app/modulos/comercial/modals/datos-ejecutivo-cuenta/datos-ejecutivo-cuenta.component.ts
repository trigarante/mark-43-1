import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {EjecutivoCuenta, EjecutivoCuentaData} from '../../../../@core/data/interfaces/comerciales/ejecutivo-cuenta';
import {EjecutivoCuentaCreateComponent} from '../ejecutivo-cuenta-create/ejecutivo-cuenta-create.component';
import swal from 'sweetalert';
import {Route, Router} from '@angular/router';
@Component({
  selector: 'ngx-datos-ejecutivo-cuenta',
  templateUrl: './datos-ejecutivo-cuenta.component.html',
  styleUrls: ['./datos-ejecutivo-cuenta.component.scss'],
})
export class DatosEjecutivoCuentaComponent implements OnInit {
  @Input() idSocio: number;
  @Input() idEjecutivoCuenta: number;
  alias: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  telefono: string;
  ext: string;
  celular: string;
  ejecutivoCuenta: EjecutivoCuenta;
   // Web Sockets
   disabled = true;
   datos: any[] = [];
  permisos: any;
  escritura: boolean;

  constructor(protected ref: NbDialogRef<SociosComponent>, private dialogService: NbDialogService,
              protected ejecutivoCuentaService: EjecutivoCuentaData, private route: Router) {
  }

  getById(idEjecutivoCuenta) {
    this.ejecutivoCuentaService.getEjectutivoCuentaById(idEjecutivoCuenta).subscribe(data => {
      this.ejecutivoCuenta = data;
    });
  }

  dismiss() {
    this.ref.close();
  }

  updateEjecutivoCuenta() {
    this.dialogService.open(EjecutivoCuentaCreateComponent, {
      context: {
        idSocio: this.idSocio,
        idEjecutivoCuenta: this.idEjecutivoCuenta,
        idTipo: 2,
      },
    });
  }

  bajaEjecutivoCuenta() {
    this.getById(this.idEjecutivoCuenta);
    swal({
      title: '¿Deseas dar de baja este Contacto?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.ejecutivoCuenta['activo'] = 0;
        this.ejecutivoCuentaService.put(this.idEjecutivoCuenta, this.ejecutivoCuenta).subscribe( result => {
          this.ejecutivoCuentaService.postSocket({data: this.ejecutivoCuenta}).subscribe(x => {});
        });
        swal('¡El Ejecutivo de Cuenta se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.dismiss();
        }));
        this.route.navigate(['modulos/comercial/contacto']);
      }
    });
  }

  getEjecutivoCuentasByI() {
    this.ejecutivoCuentaService.getEjectutivoCuentaById(this.idEjecutivoCuenta).subscribe(data => {
      this.alias = data.alias;
      this.nombre = data.nombre;
      this.apellidoPaterno = data.apellidoPaterno;
      this.apellidoMaterno = data.apellidoMaterno;
      this.telefono = data.telefono;
      this.ext = data.ext;
      this.celular = data.celular;
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  ngOnInit() {
    this.getEjecutivoCuentasByI();
    this.getPermisos();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.contacto.escritura;
  }
}
