import {Observable} from 'rxjs';

export interface EstadoEscolaridad {
    id: number;
    estado: string;
    activo: number;
}

export abstract class EstadoEscolaridadData {
  abstract get(): Observable<EstadoEscolaridad[]>;
  abstract post(estadoEscolaridad: EstadoEscolaridad): Observable<EstadoEscolaridad>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idEstadoEscolaridad, estadoEscolaridad): Observable<EstadoEscolaridad>;
  abstract getEstadoEscolaridadById(idEstadoEscolaridad): Observable<EstadoEscolaridad>;
}
