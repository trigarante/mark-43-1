import {Component, OnInit, ViewChild} from '@angular/core';
import {Seguimiento, SeguimientoData} from '../../../@core/data/interfaces/rrhh/seguimiento';
import {map} from 'rxjs/operators';
import {AsignarEtapaPracticaComponent} from '../modals/asignar-etapa-practica/asignar-etapa-practica.component';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarSeguimientoComponent} from '../modals/asignar-seguimiento/asignar-seguimiento.component';
import Swal from 'sweetalert2';
import {BajaPrecandidatoComponent} from '../modals/baja-precandidato/baja-precandidato.component';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosSeguimientoPractivoComponent} from '../modals/datos-seguimiento-practivo/datos-seguimiento-practivo.component';
import {environment} from '../../../../environments/environment';
import swal from 'sweetalert2';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-seguimiento-practico',
  templateUrl: './seguimiento-practico.component.html',
  styleUrls: ['./seguimiento-practico.component.scss'],
})
export class SeguimientoPracticoComponent implements OnInit {
  seguimiento: Seguimiento[];
  cols: any;
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  dataSource: any;
  permisos: any;
  escritura: boolean;
  /** **/
    // Web Sockets
  private stompClient = null;
  constructor(protected seguimientoService: SeguimientoData,
              private dialogService: NbDialogService,
              protected motivosService: EstadoRrhhData,
              private router: Router,
              private toastrService: NbToastrService,
  ) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'asignaciones');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelAsignaciones', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getSeguimiento(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /** Esta función obtiene toda la información asociada al seguimiento que se le ha dado a cada candidato. */
  getSeguimiento(mensajeAct) {
    this.seguimientoService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 6 && data.asistencia !== null &&
        data.idEstadoRRHH === 1 || data.idEtapa === 6 && data.asistencia !== null && data.idEstadoRRHH === 16);
    })).subscribe(data => {
      this.seguimiento = data;
      this.dataSource = new MatTableDataSource(this.seguimiento);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (this.seguimiento.length === 0) {
        swal.fire({
          type: 'error',
          title: 'Oops',
          text: 'No tienes candidatos por mostrar',
        });
      }
    }, () => {
        swal.fire({
          type: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      }, () => {
        if (mensajeAct === 1 && this.seguimiento.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Seguimiento`);
        }
      });
  }

  /** Esta función despliega un modal en el que se le asigna si el "RollPlay" fue aprobado. */
  asignarEtapaPractica(idSeguimiento, idPrecandidato) {
    this.dialogService.open(AsignarEtapaPracticaComponent, {
      context: {
        idTipo: 1,
        idSeguimiento: idSeguimiento,
        idPrecandidato: idPrecandidato,
      },
    });
  }

  updateSeguimiento(idSeguimiento, idPrecandidato, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarSeguimientoComponent, {
      context: {
        idSeguimiento: idSeguimiento,
        idTipo: 3,
        idPrecandidato: idPrecandidato,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
      },
    }).onClose.subscribe(x => {
      this.getSeguimiento(1);
    });
  }

  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }

  /** Función que despliega un modal para dar de baja un precandidato. */
  async bajaSeguimiento(idPrecandidato) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }

    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await Swal.fire({
      title: '¿Deseas dar de baja Seguimiento?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada un motivo de baja
      this.dialogService.open(BajaPrecandidatoComponent, {
        context: {
          idPrecandidato: idPrecandidato,
          idEstadoRh: motivo,
        },
      }).onClose.subscribe(x => {
        this.getSeguimiento(0);
      });
    }
  }

  verDatos(idSeguimiento) {
    this.dialogService.open(DatosSeguimientoPractivoComponent, {
      context: {
        idSeguimiento: idSeguimiento,
      },
    });
  }

  ngOnInit() {
    this.connect();
    this.getSeguimiento(0);
    this.getMotivosBaja();
    this.cols = [ 'detalle', 'nombre', 'calificacion', 'sede', 'subarea', 'acciones'];
    this.getPermisos();
    this.getSeguimiento(0);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.ventaNueva.escritura; // Asignaciones
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
