import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {SolicitudBajaComponent} from './solicitud-baja.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbAlertModule, NbCardModule, NbDateService, NbDialogRef, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';


describe('SolicitudBajaComponent', () => {
  let component: SolicitudBajaComponent;
  let fixture: ComponentFixture<SolicitudBajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudBajaComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
      ],
      providers: [
        SolicitudesData,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudBajaComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
