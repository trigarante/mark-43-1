import {Component, Input, OnInit} from '@angular/core';
import {Precandidatos, PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {PrecandidatosComponent} from '../../precandidatos/precandidatos.component';


@Component({
  selector: 'ngx-datos-precandidato',
  templateUrl: './datos-precandidato.component.html',
  styleUrls: ['./datos-precandidato.component.scss'],
})
export class DatosPrecandidatoComponent implements OnInit {
  precandidato: Precandidatos;
  @Input() idPrecandidato: number;
  idSolicitudRrhh: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  fechaNacimiento: Date;
  genero: string;
  idEstadoCivil: number;
  idEscolaridad: number;
  idEstadoEscolaridad: number;
  idPais: number;
  email: string;
  cp: number;
  colonia: number;
  calle: string;
  numeroInterior: number;
  numeroExterior: number;
  telefonoFijo: number;
  telefonoMovil: number;
  fechaCreacion: string;
  fechaBaja: number;
  tiempoTraslado: number;
  idMedioTraslado: number;
  idEstacion: number;
  idEstadoRH: number;
  idEstadoRRHH: number;
  idEtapa: number;
  curp: string;
  detalleBaja: string;
  nombreEtapa: string;
  recontratable: number;
  sede: string;
  idSubarea: number;



  constructor(protected precandidatoService: PrecandidatosData, protected ref: NbDialogRef<PrecandidatosComponent>,
              private dialogService: NbDialogService) {
  }

  getPrecandidatoById() {
    this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      this.precandidato = data;
      this.nombre = data.nombre;
      this.telefonoMovil = data.telefonoMovil;
      this.email = data.email;
      this.curp = data.curp;
      this.calle = data.calle;
      this.sede = data.sede;
      this.idSubarea = data.idSubarea;
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getPrecandidatoById();
  }

}
