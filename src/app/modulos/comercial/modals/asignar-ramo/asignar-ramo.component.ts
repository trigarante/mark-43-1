import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Ramo, RamoData} from '../../../../@core/data/interfaces/comerciales/ramo';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {TipoRamo, TipoRamoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-ramo';
import {environment} from '../../../../../environments/environment';
@Component({
  selector: 'ngx-asignar-ramo',
  templateUrl: './asignar-ramo.component.html',
  styleUrls: ['./asignar-ramo.component.scss'],
})
export class AsignarRamoComponent implements OnInit {
  @Input() idSocio: number;
  @Input() idRamo: number;
  @Input() idTipo: number;
  /**Variable que almacena la prioridad*/
  prioridadVal: any[];
  submitted: Boolean;
  asignarRamoForm: FormGroup;
  tipoRamo: TipoRamo[];
  tipoRamos: any[];
  ramo: Ramo;
  // socket
  disabled = true;
  datos: any[] = [];
  newDatos: any;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected ramoService: RamoData, protected tipoRamoService: TipoRamoData) {
  }
  guardarRamo() {
    this.submitted = true;
    if (this.asignarRamoForm.invalid) {
      return;
    }
    this.ramoService.post(this.asignarRamoForm.value).subscribe((result) => {
      this.ramoService.postSocket(this.asignarRamoForm.value).subscribe(x => {});
      this.asignarRamoForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/ramo']);
    });
  }

  getTipoRamo() {
    this.tipoRamoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoRamo = result;
      this.tipoRamos = [];
      for (let i = 0; i < this.tipoRamo.length; i++) {
        this.tipoRamos.push({'label': this.tipoRamo[i].tipo, 'value': this.tipoRamo[i].id});
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  updateRamo() {
    this.ramoService.put(this.idRamo, this.asignarRamoForm.value).subscribe(result => {
      this.ramoService.postSocket(this.asignarRamoForm.value).subscribe(x => {});
      this.asignarRamoForm.reset();
      this.ref.close();
    });
  }

  getGirsById() {
    this.ramoService.getRamoById(this.idRamo).subscribe(data => {
      this.ramo = data;
      this.asignarRamoForm.controls['descripcion'].setValue(data.descripcion);
      this.asignarRamoForm.controls['prioridad'].setValue(data.prioridad);
      this.asignarRamoForm.controls['idTipoRamo'].setValue(data.idTipoRamo);
    });
  }

  prioridad() {
    this.prioridadVal = [];
    this.prioridadVal.push({label: 'ALTA', value: 1});
    this.prioridadVal.push({label: 'MEDIANA', value: 2});
    this.prioridadVal.push({label: 'BAJA', value: 3});
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getGirsById();
    }
    this.prioridad();
    this.getTipoRamo();
    this.asignarRamoForm = this.fb.group({
      'idSocio': this.idSocio,
      'idTipoRamo': new FormControl('', Validators.required),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(4)]),
      'prioridad': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

}
