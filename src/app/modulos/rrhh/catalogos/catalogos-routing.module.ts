import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CatalogosComponent} from './catalogos.component';
import {AreaComponent} from './area/area.component';
import {CommonModule} from '@angular/common';
import {BancoComponent} from './banco/banco.component';
import {BolsaTrabajoComponent} from './bolsa-trabajo/bolsa-trabajo.component';
import {CompetenciasComponent} from './competencias/competencias.component';
import {CompetenciasCandidatoComponent} from './competencias-candidato/competencias-candidato.component';
import {EmpresaComponent} from './empresa/empresa.component';
import {EscolaridadComponent} from './escolaridad/escolaridad.component';
import {EstadoEscolaridadComponent} from './estado-escolaridad/estado-escolaridad.component';
import {EstadoCivilComponent} from './estado-civil/estado-civil.component';
import {MedioTransporteComponent} from './medio-transporte/medio-transporte.component';
import {PaisesComponent} from './paises/paises.component';
import {PuestoComponent} from './puesto/puesto.component';
import {TipoPuestoComponent} from './tipo-puesto/tipo-puesto.component';
import {SedeComponent} from './sede/sede.component';
import {MotivosBajaComponent} from './motivos-baja/motivos-baja.component';
import {SubAreaComponent} from './sub-area/sub-area.component';
import {GruposComponent} from './grupos/grupos.component';
import {RazonSocialComponent} from './razon-social/razon-social.component';
import {
  MatAutocompleteModule,
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatChipsModule, MatDialogModule, MatFormFieldControl,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule,
  MatTableModule, MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import {TurnoComponent} from './turno/turno.component';
import {AuthGuard} from '../../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: CatalogosComponent,
  children: [
    {
      path: 'area',
      component: AreaComponent,
    },
    {
      path: 'banco',
      component: BancoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.banco.activo},
    },
    {
      path: 'bolsaTrabajo',
      component: BolsaTrabajoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.bolsaDeTrabajo.activo},
    },
    {
      path: 'competencias',
      component: CompetenciasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.competencias.activo},
    },
    {
      path: 'competenciasCandidato',
      component: CompetenciasCandidatoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.competenciasCandidato.activo},
    },
    {
      path: 'empresa',
      component: EmpresaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.empresa.activo},
    },
    {
      path: 'escolaridad',
      component: EscolaridadComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.escolaridad.activo},
    },
    {
      path: 'estadoEscolaridad',
      component: EstadoEscolaridadComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.estadoDeEscolaridad.activo},
    },
    {
      path: 'estadoCivil',
      component: EstadoCivilComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.estadoCivil.activo},
    },
    // {
    //   path: 'grupo',
    //   component: GruposComponent,
    //   canActivate: [AuthGuard],
    //   data: { permiso: permisos.modulos.grupo.activo},
    // },
    {
      path: 'medioTransporte',
      component: MedioTransporteComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.medioDeTransporte.activo},
    },
    {
      path: 'motivosBaja',
      component: MotivosBajaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.motivosDeBaja.activo},
    },
    {
      path: 'paises',
      component: PaisesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.paises.activo},
    },
    {
      path: 'puesto',
      component: PuestoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.puesto.activo},
    },
    {
      path: 'tipoPuesto',
      component: TipoPuestoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoDePuesto.activo},
    },
    {
      path: 'turno',
      component: TurnoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.turno.activo},
    },
    {
      path: 'sede',
      component: SedeComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'sub-area',
      component: SubAreaComponent,
    },
    {
      path: 'razon-social',
      component: RazonSocialComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.razonSocial.activo},
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatChipsModule,
  ],
  exports: [
    RouterModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatChipsModule,
  ],
})
export class CatalogosRoutingModule {
}
