import {Injectable} from '@angular/core';
import {Inventario, InventarioData} from '../../../interfaces/ti/soporte/inventario/inventario';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class InventarioService extends InventarioData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Inventario[]> {
    return undefined;
  }

  post(inventario: Inventario): Observable<Inventario> {
    return this.http.post<Inventario>(this.baseURL + 'v1/inventario', inventario);
  }
}
