import {Observable} from 'rxjs';

export interface ClienteVn {
  id: number;
  idPais: number;
  personaMoral: number;
  nombre: string;
  paterno: string;
  materno: string;
  cp: number;
  calle: string;
  numInt: string;
  numExt: string;
  idColonia: number;
  genero: string;
  telefonoFijo: string;
  telefonoMovil: string;
  correo: string;
  fechaNacimiento: string;
  fechaRegistro: string;
  curp: string;
  rfc: string;
}

export abstract class ClientesData {
  abstract get(): Observable <ClienteVn[]>;

  abstract getClienteById(idCliente): Observable <ClienteVn>;

  abstract curpExist(curp);

  abstract rfcExist(rfc);

  abstract post(cliente): Observable <ClienteVn>;

  abstract put(idCliente, cliente): Observable <ClienteVn>;
}

export abstract class ClienteVentaNUevaData {
  abstract get(): Observable<ClienteVn[]>;

  abstract post(clienteVentaNueva): Observable<ClienteVn>;

  abstract getClienteVentaNuevaById(idClienteVentaNueva): Observable<ClienteVn>;

  abstract put(idClienteVentaNueva, clienteVentaNueva): Observable<ClienteVn>;
}
