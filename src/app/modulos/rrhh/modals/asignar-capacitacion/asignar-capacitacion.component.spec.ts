import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AsignarCapacitacionComponent} from './asignar-capacitacion.component';
import {
  MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {
  NbAlertModule,
  NbCardModule,
  NbCheckboxModule,
  NbDateService,
  NbDialogRef,
  NbRadioModule,
  NbToastrService,
} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {CompetenciaData} from '../../../../@core/data/interfaces/catalogos/competencia';
import {CompetenciaCandidatoData} from '../../../../@core/data/interfaces/catalogos/competencias-candidato';
import {CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';


describe('AsignarCapacitacionComponent', () => {
  let component: AsignarCapacitacionComponent;
  let fixture: ComponentFixture<AsignarCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AsignarCapacitacionComponent],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
        MatChipsModule,
        NbCheckboxModule,
        MatCheckboxModule,
      ],
      providers: [
        CompetenciaData,
        CompetenciaCandidatoData,
        CapacitacionData,
        EmpleadosData,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide: Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCapacitacionComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
