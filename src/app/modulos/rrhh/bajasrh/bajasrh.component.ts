import {Component, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {Precandidatos, PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {EtapasModulos, EtapasModulosData} from '../../../@core/data/interfaces/catalogos/etapasModulos';
import {SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {RecontratableComponent} from '../modals/recontratable/recontratable.component';
import {ReactivarEmpleadoComponent} from '../modals/reactivar-empleado/reactivar-empleado.component';
import {DatosBajaRhComponent} from '../modals/datos-baja-rh/datos-baja-rh.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'ngx-bajasrh',
  templateUrl: './bajasrh.component.html',
  styleUrls: ['./bajasrh.component.scss'],
})
export class BajasrhComponent implements OnInit {
  precandidatos: Precandidatos[];
  precandidato: any;
  activoPrecandidato: any;
  cols: any[];
  etapasBaja: EtapasModulos[];
  etapas: any[];
  dataSource: any;
  permisos: any;
  escritura: boolean;
  // Web Sockets
  private stompClient = null;
  /** **/
  constructor(private precandidatosService: PrecandidatosData,
              private dialogService: NbDialogService,
              private etapasService: EtapasModulosData,
              private solicitudesService: SolicitudesData,
              private toastrService: NbToastrService,
  ) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bajasrh');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelBajasRH', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPrecandidatos(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ ViewChild(MatPaginator) paginator: MatPaginator;
  @ ViewChild(MatSort) sort: MatSort;

  getPrecandidatos(mensajeAct) {
    this.precandidatosService.get().pipe(map(result => {
      return result.filter(data => data.idEstadoRH !== 1 && data.idEstadoRH !== 16
        && data.idEtapa !== 3 && data.idEtapa !== 7);
    })).subscribe(data => {
      this.precandidatos = data;
      this.dataSource = new MatTableDataSource(this.precandidatos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Bajas`);
  }

  activarEtapa(idPrecandidato) {
    this.precandidatosService.getPrecandidatoById(idPrecandidato).subscribe(result => {
      this.precandidato = result;
      swal({
        title: '¿Desea Reactivar?',
        text: 'El ususario regresará a la etapa donde se dio de Baja.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          succes: {
            text: 'Activar',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.precandidato.idEstadoRH = 16;
          this.dialogService.open(ReactivarEmpleadoComponent, {
            context: {
              idPrecandidato: idPrecandidato,
              idTipo: 2,
            },
          }).onClose.subscribe(x => {
            this.getPrecandidatos(0);
          });
        }
      });
    });
  }

  recontratable(idPrecandidato) {
    this.dialogService.open(RecontratableComponent, {
      context: {
        idPrecandidato: idPrecandidato,
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getPrecandidatos(1);
    });
  }

  getEtapasBaja() {
    this.etapasService.get().subscribe(result => {
      this.etapasBaja = result;
      this.etapas = [];
      for (let i = 0; i < this.etapasBaja.length; i++) {
        if (this.etapasBaja[i].id !== 3 && this.etapasBaja[i].id !== 7) {
          this.etapas.push({'label': this.etapasBaja[i].etapa, 'value': this.etapasBaja[i].id});
        }
      }
      this.etapas.push({'label': 'Todos', 'value': 'todo'});
    });
  }
  verDatos(idPrecandidato) {
    this.dialogService.open(DatosBajaRhComponent, {
      context: {
        idPrecandidato: idPrecandidato,
      },
    });
  }
  ngOnInit() {
    this.connect();
    this.getEtapasBaja();
    this.cols = ['detalle', 'nombre', 'nombreEtapa', 'sede', 'subarea', 'recontratables', 'acciones'];
    this.getPermisos();
    this.getPrecandidatos(0);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bajas.escritura;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
