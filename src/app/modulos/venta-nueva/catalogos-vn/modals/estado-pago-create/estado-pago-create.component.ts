import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {EstadoPagoService} from '../../../../../@core/data/services/ventaNueva/catalogos/estado-pago.service';
import {EstadoPago} from '../../../../../@core/data/interfaces/ventaNueva/catalogos/estado-pago';

export interface Data {
  idTipo: number;
  idEstadoPago: any;
}

@Component({
  selector: 'ngx-estado-pago-create',
  templateUrl: './estado-pago-create.component.html',
  styleUrls: ['./estado-pago-create.component.scss'],
})
export class EstadoPagoCreateComponent implements OnInit {



  estadosData: EstadoPago;
  estadoPagoForm: FormGroup;
  idTipo: number;

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<EstadoPagoCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Data,
              private estadoPagoService: EstadoPagoService) { }

  ngOnInit() {
    this.idTipo = this.data.idTipo;
    this.estadoPagoForm = this.fb.group({
      'description': new FormControl('', Validators.required),
      'activo': new FormControl(1),
    });
    if (this.idTipo === 2 ) {
      this.getEstadosPago();
    }
  }

  crearEstadoPago() {
    if (this.estadoPagoForm.invalid) {
      return;
    }
    this.estadoPagoService.post(this.estadoPagoForm.value).subscribe( res => {
      this.estadoPagoForm.reset();
      this.dialogRef.close();
    });
  }

  getEstadosPago() {
    this.estadoPagoService.getEstadoPagoById(this.data.idEstadoPago).subscribe(data => {
      this.estadosData = data;
      this.estadoPagoForm.controls['description'].setValue(this.estadosData.description);
    });
  }

  dismiss() {
    this.dialogRef.close();
  }

  updateEstadoPago() {
    this.estadoPagoService.put(this.data.idEstadoPago, this.estadoPagoForm.value).subscribe( res => {
      this.dialogRef.close();

    });
  }

}
