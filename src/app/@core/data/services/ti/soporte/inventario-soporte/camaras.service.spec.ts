import { TestBed } from '@angular/core/testing';

import { CamarasService } from './camaras.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('CamarasService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 5,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    folio: '0001C',
    idMarca: 1,
    fechaRecepcion: '2019-09-24T18:48:12.000+0000',
    fechaSalida: null,
    activo: 0,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    origenRecurso: 'propio',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'camaras',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CamarasService, HttpClient],
  }));
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/camaras');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/camaras', '/5');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/camaras');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/camaras', '/5/74');
      });
    });
  }
});
