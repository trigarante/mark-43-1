import {Component, Input, OnInit} from '@angular/core';
import {Vacante, VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbToastrService} from '@nebular/theme';
import {VacantesComponent} from '../../vacantes/vacantes.component';
import {Router} from '@angular/router';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {map} from 'rxjs/operators';
import {Puesto, PuestoData} from '../../../../@core/data/interfaces/catalogos/puesto';
import {PuestoTipo, PuestoTipoData} from '../../../../@core/data/interfaces/catalogos/puesto-tipo';
import {TipoUsuario, TipoUsuarioData} from '../../../../@core/data/interfaces/catalogos/tipo-usuario';
import {Sede, SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {Grupo, GrupoData} from '../../../../@core/data/interfaces/catalogos/grupo';
import {Empresa, EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {Area, AreaData} from '../../../../@core/data/interfaces/catalogos/area';
/** Componente del modal del catálogo para "crear vacante". */
@Component({
  selector: 'ngx-vacantes-create',
  templateUrl: 'vacantes-create.component.html',
  styleUrls: ['vacantes-create.component.scss'],
})

export class VacantesCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idVacante obtenido. */
  @Input() idVacante: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una vacante. */
  vacanteCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  vacantes: Vacante;
  /** Arreglo auxiliar para vacantes. */
  vacante: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  puestos: Puesto[];
  /** Arreglo auxiliar para puesto. */
  puesto: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  tipoPuestos: PuestoTipo[];
  /** Arreglo auxiliar para puesto. */
  tipoPuesto: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  tipoUsuarios: TipoUsuario[];
  /** Arreglo auxiliar para puesto. */
  tipoUsuario: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para puesto. */
  empresa: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  //
  vacanteupd: any;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarPuesto: boolean;
  vacanteEliminada: boolean;
  /**Bandera para activar los puestos*/
  activoPuesto: boolean;

  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {VacanteData} vacanteService Servicio del componente vacante para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(
    private vacanteService: VacanteData,
    private fb: FormBuilder,
    protected ref: NbDialogRef<VacantesComponent>,
    private router: Router,
    private subareaService: SubareaData,
    private puestoService: PuestoData,
    private tipoPuestoService: PuestoTipoData,
    private tipoUsuarioService: TipoUsuarioData,
    private sedesService: SedeData,
    private grupoService: GrupoData,
    private empresaService: EmpresaData,
    private areaService: AreaData,
    private toastrService: NbToastrService,
  ) {}

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarVacante() {
    this.inactivo = true;
    if (this.vacanteCreateForm.invalid) {
      return;
    }
    this.inactivo = true;
    this.submitted = true;
    this.vacanteService.post(this.vacanteCreateForm.value)
    .subscribe(result => {},
      error => { },
      () => {
        this.vacanteService.postSocket(this.vacanteCreateForm.value).subscribe(data => {});
        this.vacanteCreateForm.reset();
        this.ref.close();
        this.router.navigate(['/modulos/rrhh/vacantes']);
      },
    );
  }

  /** Función que actualiza la información de una vacante. */
  updateVacante() {
    this.inactivo = true;
    this.vacanteService.getVacantesById(this.idVacante).subscribe(data => {
      if (data.activo === 1) {
        this.inactivo = true;
        this.vacanteupd = {
          'idPuesto': this.vacanteCreateForm.value.idPuesto,
          'idSubarea': this.vacanteCreateForm.value.idSubarea,
          'idTipoPuesto': this.vacanteCreateForm.value.idTipoPuesto,
          'nombre': this.vacanteCreateForm.value.nombre,
          'activo': this.vacanteCreateForm.value.activo,
        };
        this.vacanteService.put(this.idVacante, this.vacanteupd).subscribe(result => {
          this.vacanteService.postSocket(this.vacanteupd).subscribe(d => {});
          this.vacanteCreateForm.reset();
          this.ref.close();
        });
      }else {
        this.vacanteEliminada = true;
        /**this.toastrService.danger('!!Esta vacante ha sido Eliminada¡¡', `Vacantes`);*/
      }
    });

  }


  /** Función que obtiene toda la información de la vacante dependiendo de su ID. */
  getVacanteById() {
    this.vacanteService.getVacantesById(this.idVacante).subscribe(
      data => {
        this.vacantes = data;
    },
      e => {},
      () => {
        // console.log(this.vacantes);
        this.vacanteCreateForm.controls['nombre'].setValue(this.vacantes.nombre);
        this.vacanteCreateForm.controls['idGrupo'].setValue(this.vacantes.idGrupo);
        this.vacanteCreateForm.controls['idSubarea'].setValue(this.vacantes.idSubarea);
        this.vacanteCreateForm.controls['idEmpresa'].setValue(this.vacantes.idEmpresa);
        this.vacanteCreateForm.controls['idSede'].setValue(this.vacantes.idSede);
        this.vacanteCreateForm.controls['idArea'].setValue(this.vacantes.idArea);
        this.vacanteCreateForm.controls['idPuesto'].setValue(this.vacantes.idPuesto);
        this.vacanteCreateForm.controls['idTipoPuesto'].setValue(this.vacantes.idTipoPuesto);
        this.getEmpresa(this.vacantes.idGrupo);
        this.getSedes(this.vacantes.idEmpresa);
        this.getArea(this.vacantes.idSede);
        this.getSubarea(this.vacantes.idArea);
        this.getPuesto();
        this.inactivo = false;
      });
  }

  getGrupo() {

    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }

  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  getSedes(idEmpresa) {
    this.sedesService.get().pipe(map(result => {
      return result.filter( valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarSede = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }
  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      }else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
        this.activoPuesto = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }

  getPuesto() {
    this.puestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.puestos = data;
      if (this.puestos.length === 0) {
        this.mostrarPuesto = true;
      }else {
        this.mostrarPuesto = false;
      }
      this.puesto = [];
      for (let i = 0; i < this.puestos.length; i++) {
        this.puesto.push({'label': this.puestos[i].nombre, 'value': this.puestos[i].id});
      }
    });
  }

  getTipoPuesto() {
    this.tipoPuestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.tipoPuestos = data;
      this.tipoPuesto = [];
      for (let i = 0; i < this.tipoPuestos.length; i++) {
        this.tipoPuesto.push({'label': this.tipoPuestos[i].nombre, 'value': this.tipoPuestos[i].id});
      }
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    this.vacanteCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'idGrupo': new FormControl('', Validators.compose([Validators.required])),
      'idEmpresa': new FormControl('', Validators.compose([Validators.required])),
      'idSede': new FormControl('', Validators.compose( [Validators.required])),
      'idArea': new FormControl('', Validators.compose( [Validators.required])),
      'idSubarea': new FormControl('', Validators.compose([Validators.required])),
      'idPuesto': new FormControl('', Validators.compose([Validators.required])),
      'idTipoPuesto': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
    this.getGrupo();
    this.getTipoPuesto();
    // this.getTipoUsuario();
    if (this.idTipo === 2 ) {
      this.inactivo = true;
      this.getVacanteById();
    }
    this.activoPuesto = true;
    this.getPuesto();
  }
}
