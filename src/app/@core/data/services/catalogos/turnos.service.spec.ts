import { TestBed } from '@angular/core/testing';

import { TurnosService } from './turnos.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('TurnosService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    activo: 8,
    horario: 'B',
    turno: 'A',
  },
    {
      id: 'turnos',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({}));
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/turnos');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/turnos', '/8');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/turnos');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/turnos', '/8/74');
      });
    });
  }
});
