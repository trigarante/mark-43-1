import {Observable} from 'rxjs';

export interface EjecutivoCuenta {
  alias: string;
  idSocio: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  telefono: string;
  ext: string;
  celular: string;
  activo: number;
  idEstadoSocio: number;
  email: string;
  puesto: string;
  direccion: string;
  favorito: number;
}

export abstract class EjecutivoCuentaData {
  abstract get(): Observable<EjecutivoCuenta[]>;

  abstract post(ejecutivoCuenta): Observable<EjecutivoCuenta>;
  abstract postSocket(json): Observable<JSON>;

  abstract getEjectutivoCuentaById(idEjecutivoCuenta): Observable<EjecutivoCuenta>;

  abstract put(idEjecutivoCuenta, ejecutivoCuenta): Observable<EjecutivoCuenta>;
}
