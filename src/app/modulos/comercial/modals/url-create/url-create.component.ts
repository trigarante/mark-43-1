import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AsignarUrl, AsignarUrlData} from '../../../../@core/data/interfaces/comerciales/asignar-url';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {AsignarUrlComponent} from '../../asignar-url/asignar-url.component';
@Component({
  selector: 'ngx-url-create',
  templateUrl: './url-create.component.html',
  styleUrls: ['./url-create.component.scss'],
})
export class UrlCreateComponent implements OnInit {
  @Input() idUrl: number;
  @Input() idSocio: number;
  @Input() idTipo: number;
  urlCreateForm: FormGroup;
  url: AsignarUrl;
  submitted: Boolean;
  // Web Sockets
  disabled = true;
  datos: any[] = [];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<AsignarUrlComponent>,
              private _router: Router, protected urlService: AsignarUrlData) {
  }

  getUrlById() {
    this.urlService.getAsignarUrlById(this.idUrl).subscribe(data => {
      this.url = data;
      this.urlCreateForm.controls['url'].setValue(this.url.url);
      this.urlCreateForm.controls['descripcion'].setValue(this.url.descripcion);
    });
  }

  guardarUrl() {
    this.submitted = true;
    if (this.urlCreateForm.invalid) {
      return;
    }
    this.urlService.post(this.urlCreateForm.value).subscribe((result) => {
      this.urlService.postSocket(this.urlCreateForm.value).subscribe(x => {});
      this.ref.close();
      this.urlCreateForm.reset();
      this._router.navigate(['/modulos/comercial/asignar-url']);
    });
  }

  updateUrl() {
    this.urlService.put(this.idUrl, this.urlCreateForm.value).subscribe(result => {
      this.urlCreateForm.reset();
      this.urlService.postSocket(this.urlCreateForm.value).subscribe(x => {});
      this.ref.close();
    });
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getUrlById();
    }
    this.urlCreateForm = this.fb.group({
      'idSocio': this.idSocio,
      'url': new FormControl('', [Validators.required,
        Validators.pattern('http?.+')]),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }

}
