import {Injectable} from '@angular/core';
import {Puesto, PuestoData} from '../../interfaces/catalogos/puesto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class PuestoService extends PuestoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Puesto[]> {
    return this.http.get<Puesto[]>(this.baseURL + 'v1/puestos');
  }

  post(puesto: Puesto): Observable<Puesto> {
    return this.http.post<Puesto>(this.baseURL + 'v1/puestos', puesto);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'puesto/savePuesto', json);
  }

  getById(idPuesto): Observable<Puesto> {
    return this.http.get<Puesto>(this.baseURL + 'v1/puestos/' + idPuesto);
  }

  put(idPuesto, puesto: Puesto): Observable<Puesto> {
    return this.http.put<Puesto>(this.baseURL + 'v1/puestos/' + idPuesto + '/'
      + sessionStorage.getItem('Empleado'), puesto);
  }
}
