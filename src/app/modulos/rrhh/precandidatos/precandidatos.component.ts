import {Component, OnInit, ViewChild} from '@angular/core';
import {Precandidatos, PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarCandidatoComponent} from '../modals/asignar-candidato/asignar-candidato.component';
import {BajaPrecandidatoComponent} from '../modals/baja-precandidato/baja-precandidato.component';
import Swal from 'sweetalert2';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosPrecandidatoComponent} from '../modals/datos-precandidato/datos-precandidato.component';
import {environment} from '../../../../environments/environment';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import swal from 'sweetalert2';
/**
 * Componente de precandidatos.
 */
@ Component({
  selector: 'ngx-precandidatos',
  templateUrl: './precandidatos.component.html',
  styleUrls: ['./precandidatos.component.scss'],
})
export class PrecandidatosComponent implements OnInit {
  precandidatos: Precandidatos[];
  cols: any[];
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  dataSource: any;
  permisos: any;
  escritura: boolean;

  private stompClient = null;
  /**
   * @param {PrecandidatosData} precandidatosService Servicio del componente precandidatos para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes).
   * @param {EstadoRrhhData} motivosService Servicio del componente EstadoRrhh para poder invocar los métodos. */
  constructor(private precandidatosService: PrecandidatosData,
              private dialogService: NbDialogService,
              private motivosService: EstadoRrhhData,
              private toastrService: NbToastrService) {
  }
  @ ViewChild(MatPaginator) paginator: MatPaginator;
  @ ViewChild(MatSort) sort: MatSort;


  /** Web socket connection **/
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'precandidatos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame){
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelPrecandidatos/1', (content) => {
        // _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPrecandidatos(0);
        }, 400);
      });
    });
  }

  // showMessage(message) {
  //   this.dataSource.data.unshift(message);
  //   this.dataSource.data = this.dataSource.data.slice();
  // }

  /** Con esta función se obtienen los precandidatos existentes en el servidor. */
  getPrecandidatos(mensajeAct) {
    this.precandidatosService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 1 && data.idEstadoRH === 1 || data.idEtapa === 1 && data.idEstadoRH === 16);
    })).subscribe(data => {
      this.precandidatos = data;
      this.dataSource = new MatTableDataSource(this.precandidatos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

      if (this.precandidatos.length === 0) {
        swal.fire({
          type: 'error',
          title: 'Oops',
          text: 'No tienes precandidatos por mostrar',
        });
      }
    }, () => {
        swal.fire({
          type: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      },
      () => {
        if (mensajeAct === 1 && this.precandidatos.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Precandidatos`);
        }
      });
  }

  /** Función que obtiene los motivos de baja. */
  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }
  /** Función que despliega un modal para dar de baja un precandidato. */
  async bajaPrecandidato(idPrecandidato) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await Swal.fire({
      title: '¿Deseas dar de baja este precandidato?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada un motivo de baja
      this.dialogService.open(BajaPrecandidatoComponent, {
        context: {
          idPrecandidato: idPrecandidato,
          idEstadoRh: motivo,
        },
      }).onClose.subscribe(x => {
        this.getPrecandidatos(0);
      });
    }
  }

  verDatos(idPrecandidato) {
    this.dialogService.open(DatosPrecandidatoComponent, {
      context: {
        idPrecandidato: idPrecandidato,
      },
    });
  }

  /** Esta función despliega un modal el cual permite llenar los datos solicitados para asignar un candidato.
   * @param idPrecandidato
   * */
  asignarCandidato(idEtapa, idPrecandidato, fechaIngreso) {
    this.dialogService.open(AsignarCandidatoComponent, {
      context: {
        idTipo: 1,
        idEtapa: idEtapa,
        idPrecandidato: idPrecandidato,
        fechaIngreso: fechaIngreso,
      },
    });
  }
  ngOnInit() {
  this.connect();
    this.getMotivosBaja();
    this.cols = [ 'detalle', 'nombre', 'telefono', 'sede', 'subarea', 'acciones'];
    this.getPermisos();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/precandidatos') {
        this.getPrecandidatos(0);
    //   }
    // }, 1000);
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.precandidatos.escritura;
    /**console.log('escritura', this.escritura);*/
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
