import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionComponent } from './create-emision.component';

describe('CreateEmisionComponent', () => {
  let component: CreateEmisionComponent;
  let fixture: ComponentFixture<CreateEmisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
