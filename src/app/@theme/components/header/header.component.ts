import {Component, Input, OnInit} from '@angular/core';
import {NbMenuService, NbSidebarService} from '@nebular/theme';
import {UserService} from '../../../@core/data/users.service';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {Usuario, UsuarioData} from '../../../@core/data/interfaces/ti/usuario';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TokenStorageService} from '../../../@core/data/services/login/token-storage.service';
import {AnalyticsService} from '../../../@core/utils/analytics.service';
import {filter, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {User} from '../../../@core/data/interfaces/ti/user';
import { TareasService } from '../../../@core/data/services/tareas/tareas.service';
import { FasesTareas } from '../../../@core/data/interfaces/tareas/fasesTareas';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  public permisos: String;
  public ejecutivo: any [];
  public empleado: Empleados;
  public usuarioput: Usuario;
  public datosUsuario: any;
  fasesTareas: FasesTareas[];
  user: User;
  userMenu = [{title: 'Cerrar sesion'}];
  items = [{title: 'ee'}, {title: 'jeje'}];
  contadorItems: number;
  @Input() position = 'normal';

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private analyticsService: AnalyticsService,
    private token: TokenStorageService,
    private router: Router,
    protected empleadoService: EmpleadosData,
    protected usuarioService: UsuarioData,
    private tareasService: TareasService,
  ) {
    const helper = new JwtHelperService();
  }

  ngOnInit() {
    this.getCurrentUser();
    this.getTareas();
  }

  getTareas() {
    this.tareasService
    .get()
    .pipe(
      map(result => {
          return result.map( pe => {
            return {
              title: pe.descripcion,
            };
          });
      }),
    )
    .subscribe(data => {
      this.items = data;
      this.contadorItems = this.items.length;
    });
  }



  getCurrentUser() {
    this.userService.getCurrentUser(window.localStorage.getItem('token'))
      .subscribe((users: User) => this.user = users);
    this.menuService.onItemClick()
      .pipe(
        filter(({tag}) => tag === 'logout'),
        map(({item: {title}}) => title),
      )
      .subscribe(title => this.logout());
  }

  logout() {
    this.token.signOut();
    this.router.navigate(['/login']);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

}
