import {getTestBed, TestBed} from '@angular/core/testing';
import {CapacitacionService} from './capacitacion.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';

fdescribe('Capacitacion Service', () => {
  let service: CapacitacionService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 110,
    calificacion: 1,
    idCalificacionCompetencia: 1,
    idCompetenciaCandidato: 1,
    comentarios: 1,
    idCapacitador: 1,
    idCandidato: 1,
    asistencia: 1,
    fechaIngreso: 1,
  },
    {
      id: 'capacitacion',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CapacitacionService, HttpClient],
  }));

  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(CapacitacionService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/capacitacion/74');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/capacitacion/get-by-id', '/110');
      });  /*
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/capacitacion');
      }); */
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/capacitacion', '/110/1008136800000/74');
      });
    });
  }
});
