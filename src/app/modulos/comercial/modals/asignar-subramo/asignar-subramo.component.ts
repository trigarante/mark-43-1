import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {TipoSubramo, TipoSubramoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';
@Component({
  selector: 'ngx-asignar-subramo',
  templateUrl: './asignar-subramo.component.html',
  styleUrls: ['./asignar-subramo.component.scss'],
})
export class AsignarSubramoComponent implements OnInit {
  @Input() idRamo: number;
  @Input() idSubramo: number;
  @Input() idTipo: number;
  /**Variable que almacena la prioridad*/
  prioridadVal: any[];
  asignarSubramoForm: FormGroup;
  submitted: Boolean;
  tipoSubramo: TipoSubramo[];
  tipoSubramos: any[];
  dataSource: any;
  // socket
  disabled = true;
  datos: any[] = [];
  newDatos: any;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected SubramoService: SubramoData, protected tipoSubramoServe: TipoSubramoData) {
  }
  initForma() {
    this.asignarSubramoForm = this.fb.group({
      'idRamo': this.idRamo,
      'idTipoSubRamo': new FormControl('', Validators.required),
      'prioridad': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

  guardarSubramo() {
    this.submitted = true;
    if (this.asignarSubramoForm.invalid) {
      return;
    }
    this.SubramoService.post(this.asignarSubramoForm.value).subscribe((result) => {
      this.asignarSubramoForm.reset();
      this.SubramoService.postSocket(this.asignarSubramoForm.value).subscribe(x => {});
      this.ref.close();
      this.router.navigate(['/modulos/comercial/sub-ramo']);
    });
  }

  getTipoSubramo() {
    this.tipoSubramoServe.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoSubramo = result;
      this.tipoSubramos = [];
    });
  }

  getSubramoById() {
    this.SubramoService.getSubramoById(this.idSubramo).subscribe(data => {
      this.asignarSubramoForm.controls['idTipoSubRamo'].setValue(data.idTipoSubRamo);
      this.asignarSubramoForm.controls['prioridad'].setValue(data.prioridad);
      this.asignarSubramoForm.controls['descripcion'].setValue(data.descripcion);
    });
  }

  updateSubramo() {
    this.SubramoService.put(this.idSubramo, this.asignarSubramoForm.value)
    .subscribe(data => {
      this.asignarSubramoForm.reset();
      this.SubramoService.postSocket(this.asignarSubramoForm.value).subscribe(x => {});
      this.ref.close();
    });
  }

  prioridad() {
    this.prioridadVal = [];
    this.prioridadVal.push({label: 'ALTA', value: 1});
    this.prioridadVal.push({label: 'MEDIANA', value: 2});
    this.prioridadVal.push({label: 'BAJA', value: 3});
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSubramoById();
    }
    this.prioridad();
    this.getTipoSubramo();
    this.initForma();
    this.asignarSubramoForm = this.fb.group({
      'idRamo': this.idRamo,
      'idTipoSubRamo': new FormControl('', Validators.required),
      'prioridad': new FormControl('', Validators.required),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'activo': 1,
    });
  }
  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

}
