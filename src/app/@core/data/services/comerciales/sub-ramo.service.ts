import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SubRamo, SubramoData} from '../../interfaces/comerciales/sub-ramo';

@Injectable({
  providedIn: 'root',
})
export class SubRamoService extends SubramoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<SubRamo[]> {
    return this.http.get<SubRamo[]>(this.baseURL + 'v1/subramo');
  }

  post(subramo: SubRamo): Observable<SubRamo> {
    return this.http.post<SubRamo>(this.baseURL + 'v1/subramo', subramo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'subramo/saveSubRamo/1', json);
  }

  getSubramoById(idSubRamo): Observable<SubRamo> {
    return this.http.get<SubRamo>(this.baseURL + 'v1/subramo/' + idSubRamo);
  }

  put(idSubRamo, subramo: SubRamo): Observable<SubRamo> {
    return this.http.put<SubRamo>(this.baseURL + 'v1/subramo/' + idSubRamo + '/'
      + sessionStorage.getItem('Empleado'), subramo);
  }
}
