import {Component, Input, OnInit} from '@angular/core';
import {CompetenciaSocioData} from '../../../../@core/data/interfaces/comerciales/competencias-socios';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {AsignarCompetenciaComponent} from '../asignar-competencia/asignar-competencia.component';
import swal from 'sweetalert';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-datos-competencia',
  templateUrl: './datos-competencia.component.html',
  styleUrls: ['./datos-competencia.component.scss'],
})
export class DatosCompetenciaComponent implements OnInit {
  @Input() idCompetencia: number;
  alias: any;
  competencia: any;
  descripcion: any;
  estado: any;
  nombre: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoCompetencia: any;
  tipoGiro: any;
  permisos: any;
  escritura: boolean;

  constructor(protected competenciaService: CompetenciaSocioData, protected ref: NbDialogRef<DatosCompetenciaComponent>,
              private dialogService: NbDialogService, private route: Router) { }

  ngOnInit() {
    this.getCompetencia();
    this.getPermisos();
  }

  getCompetencia() {
    this.competenciaService.getCompetenciaSociosById(this.idCompetencia).subscribe(
      result => {
        this.alias = result.alias;
        this.competencia = result.competencia;
        this.descripcion = result.descripcion;
        this.estado = result.estado;
        this.nombre = result.nombre;
        this.nombreComercial = result.nombreComercial;
        this.tipoCategoria = result.tipoSubRamo;
        this.tipoCompetencia = result.tipoCompetencia;
        this.tipoGiro = result.tipoRamo;
      },
    );
  }

  updateCompetencia() {
    this.dialogService.open(AsignarCompetenciaComponent, {
      context: {
        idTipo: 2,
        idCompetencia: this.idCompetencia,
      },
    });
  }

  bajaCompetencia() {
    this.competenciaService.getCompetenciaSociosById(this.idCompetencia).subscribe(data => {
      this.competencia = data; });
    swal({
      title: '¿Deseas dar de baja esta Competencia?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.competencia.activo = 0;
        let competenciaForm: any;
        competenciaForm = {
          'activo': this.competencia.activo,
          'competencia': this.competencia.competencia,
          'id': this.competencia.id,
          'idProducto': this.competencia.idProducto,
          'idTipoCompetencia': this.competencia.idTipoCompetencia,
        };
        this.competenciaService.put(this.idCompetencia, competenciaForm).subscribe(result => {
          this.competenciaService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡La Competencia se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.dismiss();
        }));
        this.route.navigate(['modulos/comercial/competencia']);
      }
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.competenciaProducto.escritura;
  }

  dismiss() {
    this.ref.close();
  }


}
