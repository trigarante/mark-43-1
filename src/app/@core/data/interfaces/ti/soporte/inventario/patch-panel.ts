import {Observable} from 'rxjs';


export interface PatchPanel {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class PatchPanelData {
  abstract get(): Observable<PatchPanel[]>;
  abstract post(patchPanel): Observable<PatchPanel>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getPatchPanelById(idPatchPanel): Observable<PatchPanel>;
  abstract put(idPatchPanel, patchPanel): Observable<PatchPanel>;
}
