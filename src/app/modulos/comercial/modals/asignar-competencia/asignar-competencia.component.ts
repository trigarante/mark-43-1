import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {CompetenciaSocioData} from '../../../../@core/data/interfaces/comerciales/competencias-socios';
import {TipoCometencia, TipoCompetenciaData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-cometencia';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-asignar-competencia',
  templateUrl: './asignar-competencia.component.html',
  styleUrls: ['./asignar-competencia.component.scss'],
})
export class AsignarCompetenciaComponent implements OnInit {
  @Input() idProducto: number;
  @Input() idTipo: number;
  @Input() idCompetencia: number;
  asignarCompetenciaForm: FormGroup;
  submitted: Boolean;
  tipoCompetencia: TipoCometencia[];
  tipoCompetencias: any[];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected competenciaSociosService: CompetenciaSocioData,
              protected tipoCompetenciaServie: TipoCompetenciaData) {
  }

  guardarCompetencia() {
    this.submitted = true;
    if (this.asignarCompetenciaForm.invalid) {
      return;
    }
    this.competenciaSociosService.post(this.asignarCompetenciaForm.value).subscribe((result) => {
      this.competenciaSociosService.postSocket(this.asignarCompetenciaForm.value).subscribe(x => {});
      this.asignarCompetenciaForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/competencia']);    });
  }

  getTipoCompetencia() {
    this.tipoCompetenciaServie.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoCompetencia = result;
      this.tipoCompetencias = [];
      for (let i = 0; i < this.tipoCompetencia.length; i++) {
        this.tipoCompetencias.push({'label': this.tipoCompetencia[i].tipo, 'value': this.tipoCompetencia[i].id});
      }
    });
  }
  getCompetenciaById() {
    this.competenciaSociosService.getCompetenciaSociosById(this.idCompetencia).subscribe( data => {
      this.asignarCompetenciaForm.controls['idTipoCompetencia'].setValue(data. idTipoCompetencia);
      this.asignarCompetenciaForm.controls['competencia'].setValue(data.competencia);
      this.asignarCompetenciaForm.controls['idProducto'].setValue(data.idProducto);
    });
  }
  updateCompetencia() {
   this.competenciaSociosService.put(this.idCompetencia, this.asignarCompetenciaForm.value).subscribe((result) => {
     this.competenciaSociosService.postSocket(this.asignarCompetenciaForm.value).subscribe(x => {});
      this.asignarCompetenciaForm.reset();
      this.ref.close();
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCompetenciaById();
    }
    this.getTipoCompetencia();
    this.asignarCompetenciaForm = this.fb.group({
      'idProducto': this.idProducto,
      'idTipoCompetencia': new FormControl('', Validators.required),
      'competencia': new FormControl('', [Validators.required, Validators.minLength(4)]),
      'activo': 1,
    });
  }

}
