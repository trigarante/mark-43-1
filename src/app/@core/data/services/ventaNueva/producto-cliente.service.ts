import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {ProductoCliente, ProductoClienteData} from '../../interfaces/ventaNueva/producto-cliente';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ProductoClienteService extends ProductoClienteData {

  private baseURL;
  public aseguradora = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<ProductoCliente[]> {
    return this.http.get<ProductoCliente[]>(this.baseURL + 'v1/producto-cliente');
  }

  post(productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.post<ProductoCliente>(this.baseURL + 'v1/producto-cliente', productoCliente);
  }

  getProductoClienteById(idProductoCliente): Observable<ProductoCliente> {
    return this.http.get<ProductoCliente>(this.baseURL + 'v1/producto-cliente/' + idProductoCliente);
  }

  put(idProductoCliente, productoCliente: ProductoCliente): Observable<ProductoCliente> {
    return this.http.put<ProductoCliente>(this.baseURL + 'v1/producto-cliente/' + idProductoCliente, productoCliente);
  }

  entryAseguradora(value) {
    this.aseguradora.next(value);
  }

  returnAseguradora(): Observable<number> {
    return this.aseguradora.asObservable();
  }
}
