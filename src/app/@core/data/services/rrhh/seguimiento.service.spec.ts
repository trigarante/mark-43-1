import {getTestBed, TestBed} from '@angular/core/testing';
import {SeguimientoService} from './seguimiento.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';


fdescribe('SeguimientoService', () => {
  let service: SeguimientoService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 89,
    idSubarea: 2,
    idCapacitacion: 104,
    idCoach: 74,
    comentarios: 'APROBADO PRUEBA',
    fechaIngreso: '1564159920811',
    asistencia: null,
    registro: null,
    comentariosFaseDos: 'COMENTARIO TEST UNICO',
    calificacionRollPlay: 1,
  },
    {
      id: 'seguimiento',
    }];

  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SeguimientoService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(SeguimientoService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/seguimiento/74');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/seguimiento/get-by-id', '/89');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/seguimiento/209/6/1564159920811');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/seguimiento', '/89/1008136800000/74');
      });
    });
  }
});
