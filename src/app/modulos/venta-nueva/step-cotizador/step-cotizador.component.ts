import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PaisesData} from '../../../@core/data/interfaces/catalogos/paises';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SolicitudesVNData} from '../../../@core/data/interfaces/ventaNueva/solicitudes';


@Component({
  selector: 'ngx-step-cotizador',
  templateUrl: './step-cotizador.component.html',
  styleUrls: ['./step-cotizador.component.scss'],
})
export class StepCotizadorComponent implements OnInit {

  idCliente: number;
  idProductoSolicitud: number;
  idSubRamo: number;
  idSolicitud: number;
  idProductocliente: number;
  aseguradora: any;
  idRegistroPoliza: number;

  datosProducto: boolean ;
  registroPoliza: boolean ;
  recibos: boolean;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private router: Router, protected paisesService: PaisesData, private _formBuilder: FormBuilder,
              private solicitudesService: SolicitudesVNData) {

    this.idProductoSolicitud = this.router.getCurrentNavigation().extras.state.idProductoSolicitud;
    this.idSubRamo = this.router.getCurrentNavigation().extras.state.idSubRamo;
    this.idSolicitud = this.router.getCurrentNavigation().extras.state.idSolicitud;
    this.aseguradora = this.router.getCurrentNavigation().extras.state.aseguradora;
    this.idSolicitud = this.router.getCurrentNavigation().extras.state.idSolicitud;
    this.idSubRamo = this.router.getCurrentNavigation().extras.state.idSubRamo;
    console.log(this.idProductoSolicitud);
    console.log(this.aseguradora);
    console.log(this.idSolicitud);
    console.log(this.idSubRamo);
  }

  ngOnInit() {
    // console.log(this.router.getCurrentNavigation().extras.state.aseguradora);


    if (this.idCliente != null && this.idProductoSolicitud != null && this.idSubRamo != null && this.idSolicitud != null) {
      this.datosProducto = true;
    }

    if (this.idProductocliente != null ) {
      this.registroPoliza = true;
    }
  }

  activarDatosProducto(value: boolean) {
    this.datosProducto = value;
  }


  activarRegistroPoliza(value: boolean) {
    this.registroPoliza = value;
  }

  activarRecibos(value: boolean) {
    this.recibos = value;
  }

}
