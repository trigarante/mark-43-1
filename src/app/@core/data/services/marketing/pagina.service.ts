import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Pagina, PaginasData} from '../../interfaces/marketing/pagina';
import {Observable} from 'rxjs';

@Injectable()
export class PaginaService extends PaginasData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Pagina[]> {
    return this.http.get<Pagina[]>(this.baseURL + 'v1/pagina');
  }

  post(pagina: Pagina): Observable<Pagina> {
    return this.http.post<Pagina>(this.baseURL + 'v1/pagina', pagina);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'paginas/savePaginas', json);
  }
  postSocketAseguradoras(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'aseguradoras/saveAseguradoras', json);
  }
  postSocketDatosNodo(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'datosnodo/saveDatosNodo', json);
  }

  getPaginaById(idPagina): Observable<Pagina> {
    return this.http.get<Pagina>(this.baseURL + 'v1/pagina/' + idPagina);
  }

  put(idPagina, fechaRegistro, fechaActualizacion, pagina: Pagina) {
    return  this.http.put(this.baseURL + 'v1/pagina/' + idPagina + '/' + fechaRegistro + '/' + fechaActualizacion + '/'
      + sessionStorage.getItem('Empleado'), pagina);
  }
}
