import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CandidatoData, Candidatos} from '../../interfaces/rrhh/candidatos';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CandidatosService extends CandidatoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Candidatos[]> {
    return this.http.get<Candidatos[]>(this.baseURL + 'v1/candidato/' + sessionStorage.getItem('Usuario'));
  }

  post(idEtapa, candidato: Candidatos): Observable<Candidatos> {
    return this.http.post<Candidatos>(this.baseURL + 'v1/candidato/' + idEtapa, candidato);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'candidato/saveCandidato', json);
  }

  getCandidatoById(idCandidato): Observable<Candidatos> {
    return this.http.get<Candidatos>(this.baseURL + 'v1/candidato/get-by-id/' + idCandidato);
  }

  put(idCandidato, candidato): Observable<Candidatos> {
    return this.http.put<Candidatos>(this.baseURL + 'v1/candidato/update/' + idCandidato + '/' +
      sessionStorage.getItem('Empleado'), candidato);
  }

}
