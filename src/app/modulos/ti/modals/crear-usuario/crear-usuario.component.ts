import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';

@Component({
  selector: 'ngx-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.scss'],
})
export class CrearUsuarioComponent implements OnInit {
  @Input() id: number;
  @Input() emails: string;
  userForm: FormGroup;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<CrearUsuarioComponent>) {

    this.userForm = this.fb.group({
      'usuario': new FormControl(''),
      'password' : new FormControl(''),
      'idCorreo' : {id: this.id},
      'token' : 0,
      'idGrupo' : 1,
      'idtipo': 1,
      'idSubarea': 1,
    });
  }
  dismiss() {
    this.ref.close();
  }

  /*guardarUsuario() {

    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    this.UsuarioService.post(this.userForm.value).subscribe((result) => {
      this.userformForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  ngOnInit() {
  }

}
