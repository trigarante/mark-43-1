import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Select from 'd3-selection';
import * as d3Axis from 'd3-axis';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';

@Component({
  selector: 'ngx-reportes-grupo-permisos',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reportes-grupo-permisos.component.html',
  styleUrls: ['./reportes-grupo-permisos.component.scss'],
})

export class ReportesGrupoPermisosComponent implements OnInit {
  private margin = {top: 20, right: 20, bottom: 20, left: 20};
  private width: number;
  private height: number;
  cols: any[];
  etapas: any[];
  grupoUsuario: GrupoUsuarios[];
  dataSource: any;
  contadorPermisos: number;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  rangeDates: Date[];
  root: any;
  svg: any;
  Yscale: any;
  Xscale: any;
  private barr: any;
  yAxis: any;
  xAxis: any;
  YAX: any;
  XAX: any;
  private glove: any;
  private label: any[];
  datosBackUp: any;
  private back: any;
  private circ: any;
  private text: any;
  first_Gradient: any;
  second_Gradient: any;
  private colors: string[];
  private arc: any;
  private svg2: any;
  private datosMostrar: any;
  private width0: number;
  private height0: number;

  //
  constructor(
    private grupoUsuariosService: GrupoUsuariosData,
  ) {
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 510 - this.margin.top - this.margin.bottom;
    this.colors = ['rgba(40,150,82,0.85)', '#601d7b', '#5f67d3'];
    this.width0 = 180 - this.margin.left - this.margin.right;
    this.height0 = 510 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this.drawSVG();
    this.setGrupoUsuarios();
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
    this.cols = [
      'nombre',
      'descripcion',
      'idPuesto',
    ];
    //this.initSvg();
  }

  setGrupoUsuarios() {
    this.grupoUsuariosService
      .get()
      .pipe(
        map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
      this.grupoUsuario = data;
      this.contadorPermisos = this.grupoUsuario.length;
      this.dataSource = new MatTableDataSource(this.grupoUsuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.drawdash(data);
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawSVG() {
    this.svg = d3Select.select('#graph-dash0').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width0} ${this.height0}`)
      .attr('id', 'svg1');

    const Gcircle = this.svg.append('circle')
      .attr('cx', '45%')
      .attr('cy', '25%')
      .attr('r', '13%')
      .style('fill', this.colors[0])
      .attr('stroke', this.colors[0]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());
    const start1 = this.svg.append('rect').attr('x', '45%').attr('y', '8%').attr('height', '4%').attr('width', 3).attr('fill', this.colors[0]);
    const percent1 = this.svg.append('text').attr('x', '50%').attr('y', '12%').attr('font-size', 30).text('%').attr('fill', this.colors[0]);
    const text1 = this.svg.append('text').attr('x', '1%').attr('y', '11%').attr('font-size', 15).text('ACTIVOS').attr('fill', this.colors[0])
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());

    const Gcircle2 = this.svg.append('circle')
      .attr('cx', '45%')
      .attr('cy', '75%')
      .attr('r', '13%')
      .style('fill', this.colors[1])
      .attr('stroke', this.colors[1]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.ocupado());
    const start2 = this.svg.append('rect').attr('x', '45%').attr('y', '57%').attr('height', '5.5%').attr('width', 3).attr('fill', this.colors[1]);
    const percent2 = this.svg.append('text').attr('x', '50%').attr('y', '61%').attr('font-size', 30).text('%').attr('fill', this.colors[1]);
    const text2 = this.svg.append('text').attr('x', '1%').attr('y', '60%').attr('font-size', 12).text('INACTIVOS').attr('fill', this.colors[1])
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());

    this.arc = d3Shape.arc()
      .innerRadius(58)
      .outerRadius(61)
      .startAngle(0);
  }

  drawSVG2(data, allData, originalData) {
    this.root = data;
    console.log(this.root);
    this.label = [];
    for (let j = 1; j <= this.root.length; j++) {
      this.label.push(j); }

    this.svg2 = d3Select.select('#graph-dash00').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width} ${this.height}`)
      .attr('transform', 'rotate(-90 0 0)')
      .attr('id', 'svg2');

    this.back = this.svg2
      .append('rect')
      .attr('class', 'fondo')
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', '100%')
      .attr('width', '100%')
      .style('opacity', '0')
      .on('click', () => this.returnOrigin(originalData));

    this.Xscale = d3Scale.scaleLinear()
      .domain([d3Array.max(this.root), 0])
      .range([this.height - 100, 0]);
    this.Yscale = d3Scale.scaleBand()
      .domain(this.label)
      .range([10, this.height + 10]); //435
    this.xAxis = d3Axis.axisLeft().scale(this.Xscale).ticks(5);
    this.yAxis = d3Axis.axisBottom().scale(this.Yscale).ticks(5);
    this.YAX = this.svg2.append('g')
      .attr('class', 'axis')
      .attr('transform', 'rotate(90 20 0)')
      .call(this.yAxis);

    this.barr = this.svg2.selectAll('rect .barr')
      .data(this.root)
      .enter()
      .append('rect')
      .attr('y', (d, i) => (i * (this.height ) / this.root.length))
      .attr('x', 21)
      .attr('height', '7%')
      .attr('width', 0) // (d) => this.Xscale(d) )
      .attr('fill', this.colors[2])
      .attr('rx', '6')
      .attr('cursor', 'pointer')
      .on('mouseover', this.handleMouseOver)
      .on('mousemove',   function (d, i) {
        this.glove
          .html('Area: ' + allData[i][0].puestoNombre + '<br>' + 'Cantidad: ' + d)
          .style('bottom', ( d3Select.mouse(this)[0] + 10 ) + 'px' )
          .style('left', (d3Select.mouse(this)[1] + 5 ) + 'px');
      } )
      .on('mouseout', this.handleMouseOut)
      .on('click', (d, i) => this.table_barr(d, i, originalData));


    this.barr.transition()
      .duration(1000)
      .delay((d, i) => i * 60)
      .attr('width', (d) => this.Xscale(d) + 7)
      .transition()
      .duration(200)
      .attr('width', (d) => this.Xscale(d) - 6)
      .transition()
      .duration(150)
      .attr('width', (d) => this.Xscale(d) + 4)
      .transition()
      .duration(100)
      .attr('width', (d) => this.Xscale(d));
  }

  drawdash(d) {
    const tiempoAnimacion = 5000;
    this.datosMostrar = this.dataCompose(d);
    const dataConv = this.dataFilter(d);
    this.drawSVG2(dataConv[0], dataConv[1], d);

    const ringAdmin = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', this.colors[0])
      .attr('transform', 'translate(63,117)')
      .attr('d', this.arc);
    const percAdmin = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    var percAdministrativo = this.svg.append('text').attr('x', '32%').attr('y', '28%').attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());

    const ringEjec = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', this.colors[1])
      .attr('transform', 'translate(63,353)')
      .attr('d', this.arc);
    const percEjec = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percEjecutivo = this.svg.append('text').attr('x', '32%').attr('y', '78%').attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.ocupado());

    ringAdmin.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percAdministrativo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percAdministrativo.text(Math.trunc(d3Inter.interpolate(0, percAdmin)(t))) );

    ringEjec.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percEjecutivo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percEjecutivo.text(Math.trunc(d3Inter.interpolate(0, percEjec)(t))) );
  }

  dataFilter(data) {
    const puestos = [];
    const puesto = [];
    const puesto1 = [];
    const puesto2 = [];
    const puesto3 = [];
    const puesto4 = [];
    const puesto5 = [];
    const puesto6 = [];
    const puesto7 = [];
    const puesto8 = [];
    const puesto9 = [];
    const puesto10 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idPuesto === 1) {
        puesto1.push(data[i]);
      } else if (data[i].idPuesto === 2) {
        puesto2.push(data[i]);
      } else if (data[i].idPuesto === 3) {
        puesto3.push(data[i]);
      } else if (data[i].idPuesto === 4) {
        puesto4.push(data[i]);
      } else if (data[i].idPuesto === 5) {
        puesto5.push(data[i]);
      } else if (data[i].idPuesto === 6) {
        puesto6.push(data[i]);
      } else if (data[i].idPuesto === 7) {
        puesto7.push(data[i]);
      } else if (data[i].idPuesto === 8) {
        puesto8.push(data[i]);
      } else if (data[i].idPuesto === 9) {
        puesto9.push(data[i]);
      } else if (data[i].idPuesto === 10) {
        puesto10.push(data[i]);
      }
    }
    puestos.push(puesto1.length);
    puestos.push(puesto2.length);
    puestos.push(puesto3.length);
    puestos.push(puesto4.length);
    puestos.push(puesto5.length);
    puestos.push(puesto6.length);
    puestos.push(puesto7.length);
    puestos.push(puesto8.length);
    puestos.push(puesto9.length);
    puestos.push(puesto10.length);
    puesto.push(puesto1);
    puesto.push(puesto2);
    puesto.push(puesto3);
    puesto.push(puesto4);
    puesto.push(puesto5);
    puesto.push(puesto6);
    puesto.push(puesto7);
    puesto.push(puesto8);
    puesto.push(puesto9);
    puesto.push(puesto10);

    return [puestos, puesto];
  }

  returnOrigin(d) {
    this.dataSource = new MatTableDataSource(d);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorPermisos = d.length;
  }

  table_barr (x, i, data) {
    this.datosBackUp = data;
    const grupoUsuarioFilter = [];
    for ( let k = 0; k < data.length; k++) {
      if ( data[k].idPuesto === i + 1 ) {
        grupoUsuarioFilter.push(data[k]);
      }
    }
    this.dataSource = new MatTableDataSource(grupoUsuarioFilter);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorPermisos = grupoUsuarioFilter.length;
  } // **************************

  dataCompose(d) {
    const tamaño = d.length;
    const libre = [];
    const ocupado = [];


    for (let i = 0; i < tamaño; i++) {
      if (d[i].activo === 1) {
        libre.push(d[i]);
      } else if (d[i].activo === 0) {
        ocupado.push(d[i]);
      }
    }
    return [tamaño, libre, ocupado];
  } // *******************************

  libre() {
    const data = this.datosMostrar[1];
    this.svg2.remove();
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola = this.dataFilter(data);
    this.drawSVG2(hola[0], hola[1], data);
    this.barr.attr('fill', this.colors[0]);
  }
  ocupado() {
    const data = this.datosMostrar[2];
    this.svg2.remove();
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola = this.dataFilter(data);
    this.drawSVG2(hola[0], hola[1], data);
    this.barr.attr('fill', this.colors[1]);
  }

  handleMouseOver(d, i) {
    this.glove = d3Select.select('#graph-dash00')
      .append('div')
      .attr('class', 'tooltip')
      .attr('id', 'toolTip')
      .style('opacity', 0.8);
  }

  handleMouseOut() {
    document.getElementById('toolTip').remove();
  }
}
