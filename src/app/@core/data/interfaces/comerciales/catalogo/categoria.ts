import {Observable} from 'rxjs';

export interface Categoria {
  id: number;
  tipo: string;
  regla: string;
  activo: number;
}

export abstract class CategoriaData {
  abstract get(): Observable<Categoria[]>;

  abstract post(tipoConvenio): Observable<Categoria>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCategoriaById(idCategoria): Observable<Categoria>;

  abstract put(idCategoria, categoria): Observable<Categoria>;
}
