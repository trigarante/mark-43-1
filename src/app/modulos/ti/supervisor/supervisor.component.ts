import {Component, OnInit, ViewChild} from '@angular/core';
import {DialogService} from 'primeng/api';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NbDialogService} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {CrearSupervisorComponent} from '../modals/crear-supervisor/crear-supervisor.component';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { environment } from '../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.scss'],
  providers: [ DialogService],
})
export class SupervisorComponent implements OnInit {
  cols: any[];
  empleado: Empleados[];
  area: any[];
  subarea: Subarea[];
  idSubarea: any;
  UserForm: FormGroup;
  areas: any[];
  subareas: Area[];
  areaprincipal: number;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  permisoejecutivo: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    private empleadosService: EmpleadosData,
    public dialogService: NbDialogService,
    private subareaService: SubareaData,
    private fb: FormBuilder,
    private areaService: AreaData,
    private router: Router,
  ) {
    this.initForma();
  }

  ngOnInit() {
    this.getEmpleados();
    this.getAreas();
    this.cols = [
      'Nombre',
      'Telefono',
      'Area' ,
      'Subarea',
      'Asignar Correo',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  initForma() {
    this.UserForm = this.fb.group({
      'subareacontrol': new FormControl(),
        'areacontrol': new FormControl(),
      });
  }

  getEmpleados() {
    this.empleadosService.get().pipe(map( result => {
      return result.filter( data => data.idUsuario === null  );
    })).subscribe(data => {
      this.empleado = data;
      this.dataSource = new MatTableDataSource(this.empleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  /** consigue todas las areas de ejecutivos*/
  getAreas() {
    this.areaService.get().pipe(map(data => this.subareas = data)).subscribe(data => {
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }
  getSubarea(event) {
    this.areaprincipal = event;
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.area = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.area.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});
        this.empleado = null;
      }
    });
  }
  asignarsupervisor(idUsuario, idSubarea, id, areaprincipal) {
    this.dialogService.open(CrearSupervisorComponent, {
      context: {
        idUsuario: idUsuario,
        idSubarea: idSubarea,
        id: id,
        areaprincipal: areaprincipal,
      },
    }).onClose.subscribe(x => {
     this.getEmpleados();
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'supervisor');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelSupervisor', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.getEmpleados();
        }, 500);
      });
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.supervisor.escritura;
    this.permisoejecutivo = this.permisos.modulos.usuarioAsignaciones.activo;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.supervisor.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
