import { Component, OnInit } from '@angular/core';
import {map} from 'rxjs/operators';
import {NbDialogService} from '@nebular/theme';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {JwtHelperService} from '@auth0/angular-jwt';
import {StateService} from '../../../@core/data/state.service';
import {TokenStorageService} from '../../../@core/data/services/login/token-storage.service';
import {JwtResponseData} from '../../../@core/data/services/login/jwt-response';
import { MarkcalleService } from '../../../@core/data/services/markcalle/markcalle';
import { stringify } from '@angular/compiler/src/util';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import * as moment from 'moment';




@Component({
  selector: 'ngx-start-markcalle',
  templateUrl: './start-markcalle.component.html',
  styleUrls: ['./start-markcalle.component.scss'],
})
export class StartMarkcalleComponent implements OnInit {
  public saludo: String;
  public permisos: String;
  public ejecutivo : any [];
  public  empleado: Empleados;
  public horas: any;
  public minutos: any;
  public segundos: any;
  public hora: any;

  
  
  constructor(  private token: TokenStorageService,
                private markcalleService: MarkcalleService,
                protected empleadoService: EmpleadosData) { 
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(this.token.getToken());
    this.permisos = decodedToken.sub;
    
    this.ejecutivo = this.permisos.split(" ");
    }
  

  ngOnInit() {
    
    this.getEmpleado();
    setTimeout
    setTimeout(this.ActualizarHora, 1000);
    
    
  }

  getEmpleado() {
    this.empleadoService.getEmpleadoById(this.ejecutivo[2]).subscribe(data => {
    this.empleado = data;
  
    console.log(this.empleado);
  
      setInterval(() =>{
        this.ActualizarHora();
      },1000);
  
  
    });
  }
  
  ActualizarHora(){
    var fecha = new Date();
    this.minutos = fecha.getMinutes();
    this.horas = fecha.getHours();
    this.hora = moment().format('LTS');
    if (this.horas >= 8 && this.minutos >= 1 && this.horas < 12) {
       this.saludo = "BUENOS DÍAS";
    }
    if (this.horas >= 12 && this.minutos >= 1 && this.horas < 19) {
      this.saludo = "BUENAS TARDES";
    }
    if (this.horas >= 19 && this.minutos >= 1) {
      this.saludo = "BUENAS NOCHES";
    }
}
  
  
  


}
