import {Component, OnInit, ViewChild} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Cpu, CpuData} from '../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Monitor, MonitorDate} from '../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Laptop, LaptopData} from '../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {Diadema, DiademaData} from '../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {Tablet, TabletData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Teclado, TecladoData} from '../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Mouse, MouseData} from '../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {NbDialogService} from '@nebular/theme';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import {CpuCreateComponent} from '../modals/cpu-create/cpu-create.component';
import {MonitorCreateComponent} from '../modals/monitor-create/monitor-create.component';
import {LaptopCreateComponent} from '../modals/laptop-create/laptop-create.component';
import {TabletCreateComponent} from '../modals/tablet-create/tablet-create.component';
import {TecladoCreateComponent} from '../modals/teclado-create/teclado-create.component';
import {MouseCreateComponent} from '../modals/mouse-create/mouse-create.component';
import {DiademeCreateComponent} from '../modals/diademe-create/diademe-create.component';
import {AsignarEquipoComponent} from '../modals/asignar-equipo/asignar-equipo.component';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import swal2 from 'sweetalert2';
import swal from 'sweetalert';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss'],
})
export class InventarioComponent implements OnInit {
  /** Variable que permite la inicialización del formulario para inventario . */
  InventarioForm: FormGroup;
  tiposinventario: SelectItem[];
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;
  /** Variable que inicializa las columnas*/
  cols: any[];
  /** Variable para el arreglo cpu*/
  cpu: Cpu[];
  /** Variable para el arreglo monitor*/
  monitor: Monitor[];
  laptop: Laptop[];
  diadema: Diadema[];
  tablet: Tablet[];
  teclado: Teclado[];
  mouse: Mouse[];

  cpudesvincular: Cpu;
  monitordesvincular: Monitor;
  laptopdesvincular: Laptop;
  diademadesvincular: Diadema;
  tabletdesvincular: Tablet;
  tecladodesvincular: Teclado;
  mousedesvincular: Mouse;

  dataSource: any;
  tipoEquipos: string;

  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  bajaCpu: any;
  actualizar: string;
  private stompClientCPU = null;
  private stompClientMonitor = null;
  private stompLap = null;
  private stompTab = null;
  private stompTec = null;
  private stompM = null;
  private stompD = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(
    private fb: FormBuilder,
    private cpuService: CpuData,
    private monitorService: MonitorDate,
    private laptopService: LaptopData,
    private diademaService: DiademaData,
    private tabletService: TabletData,
    private tecladoService: TecladoData,
    private mouseService: MouseData,
    public dialogService: NbDialogService,
    private estadoInventarioServive: EstadoInventarioData,
    private rutaActiva: ActivatedRoute,
    private router: Router,
  ) {
    this.InventarioForm = this.fb.group({});
  }

  connect() {
    const _this = this;
    /**----------------------CPU-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'cpu');
    this.stompClientCPU = Stomp.over(socket);
    this.stompClientCPU.connect({}, (frame) => {
      _this.stompClientCPU.subscribe('/task/panelCpu', (content) => {
        if (this.actualizar === 'cpu' || this.tipoEquipo === 'cpu') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('cpu');
          _this.getEquipo();
        }, 500);
      }
      });
    });
    /**----------------------MONITOR-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'monitor');
    this.stompClientMonitor = Stomp.over(socket1);
    this.stompClientMonitor.connect({}, (frame) => {
      _this.stompClientMonitor.subscribe('/task/panelMonitor', (content) => {
        if (this.actualizar === 'monitor') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('monitor');
        }, 500);
      }
      });
    });
    /**----------------------LAPTOP-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'laptop');
    this.stompLap = Stomp.over(socket2);
    this.stompLap.connect({}, (frame) => {
      _this.stompLap.subscribe('/task/panelLaptop', (content) => {
        if (this.actualizar === 'laptop') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('laptop');
        }, 500);
      }
      });
    });
    /**----------------------TABLET-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'tablet');
    this.stompTab = Stomp.over(socket3);
    this.stompTab.connect({}, (frame) => {
      _this.stompTab.subscribe('/task/panelTablet', (content) => {
        if (this.actualizar === 'tablet') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('tablet');
        }, 500);
      }
      });
    });
    /**----------------------teclado-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'teclado');
    this.stompTec = Stomp.over(socket4);
    this.stompTec.connect({}, (frame) => {
      _this.stompTec.subscribe('/task/panelTeclado', (content) => {
        if (this.actualizar === 'teclado') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('teclado');
        }, 500);
      }
      });
    });
    /**----------------mouse-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'mouse');
    this.stompM = Stomp.over(socket5);
    this.stompM.connect({}, (frame) => {
      _this.stompM.subscribe('/task/panelMouse', (content) => {
        if (this.actualizar === 'mouse') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('mouse');
        }, 500);
      }
      });
    });
    /**----------------diadema-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'diadema');
    this.stompD = Stomp.over(socket6);
    this.stompD.connect({}, (frame) => {
      _this.stompD.subscribe('/task/panelDiadema', (content) => {
        if (this.actualizar === 'diadema') {
          _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEquipos('diadema');
        }, 500);
      }
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.connect();
    this.getEstado();
    this.getEquipo();
    /** Se inicializa los nombres de las columnas*/
    this.cols = ['detalle', 'id', 'nombre', 'usuario', 'numeroSerie', 'marca', 'activo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }

  /** muestra los valores de cpu*/
  getEquipo() {
    // console.log(this.tipoEquipo);
    switch (this.tipoEquipo) {
      case 'cpu':
        this.cpuService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.cpu = data;

          this.dataSource = new MatTableDataSource(this.cpu);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        });

        break;
      case 'monitor':
        this.monitorService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.monitor = data;
          this.dataSource = new MatTableDataSource(this.monitor);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        break;
      case 'laptop':
        this.laptopService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.laptop = data;
          this.dataSource = new MatTableDataSource(this.laptop);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        break;
      case 'tablet':
        this.tabletService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.tablet = data;
          this.dataSource = new MatTableDataSource(this.tablet);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'teclado':
        this.tecladoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.teclado = data;
          this.dataSource = new MatTableDataSource(this.teclado);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'mouse':
        this.mouseService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.mouse = data;
          this.dataSource = new MatTableDataSource(this.mouse);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'diadema':
        this.diademaService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.diadema = data;
          this.dataSource = new MatTableDataSource(this.diadema);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        break;
      default:

        break;
    }
    this.tipoEquipos = this.tipoEquipo;
  }

  /** Se muestra un menu con el componente TAB, con el selectedTabChange se selecciona que datos mostrara en la tabla*/
  getEquipos(tipoEquipo) {
    tipoEquipo = tipoEquipo.toLowerCase();
    this.actualizar = tipoEquipo;
    this.tipoEquipos = tipoEquipo;
    /**Obtiene los datos del cpu*/
    if (tipoEquipo === 'cpu') {
      this.cpuService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.cpu = data;
        this.dataSource = new MatTableDataSource(this.cpu);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'cpu';
      /**Obtiene los datos de monitor*/
    } else if (tipoEquipo === 'monitor') {
      this.monitorService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.monitor = data;
        this.dataSource = new MatTableDataSource(this.monitor);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'monitor';

    } else if (tipoEquipo === 'laptop') {
      this.laptopService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.laptop = data;
        this.dataSource = new MatTableDataSource(this.laptop);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'laptop';

    } else if (tipoEquipo === 'diadema') {
      this.diademaService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.diadema = data;
        this.dataSource = new MatTableDataSource(this.diadema);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'diadema';
    } else if (tipoEquipo === 'tablet') {
      this.tabletService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.tablet = data;
        this.dataSource = new MatTableDataSource(this.tablet);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'tablet';

    } else if (tipoEquipo === 'teclado') {
      this.tecladoService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.teclado = data;
        this.dataSource = new MatTableDataSource(this.teclado);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'teclado';

    } else if (tipoEquipo === 'mouse') {
      this.mouseService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.mouse = data;
        this.dataSource = new MatTableDataSource(this.mouse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'mouse';


    }

  }

  /** funcion que muestra los detalles del equipo*/
  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }

  /** funcion que envia a un modal para crear un cpu*/
  crearCpu() {
    this.dialogService.open(CpuCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un cpu*/
  updateCpu(idCpu) {
    this.dialogService.open(CpuCreateComponent, {
      context: {
        idTipo: 2,
        idCpu: idCpu,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear un monitor*/
  crearMonitor() {
    this.dialogService.open(MonitorCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para modificar un monitor*/
  updateMonitor(idMonitor) {
    this.dialogService.open(MonitorCreateComponent, {
      context: {
        idTipo: 2,
        idMonitor: idMonitor,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear un laptop*/
  crearLaptop() {
    this.dialogService.open(LaptopCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un Laptop*/
  updateLaptop(idLaptop) {
    this.dialogService.open(LaptopCreateComponent, {
      context: {
        idTipo: 2,
        idLaptop: idLaptop,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para crear una tablet*/
  crearTablet() {
    this.dialogService.open(TabletCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  /** funcion que envia a un modal para modificar un tablet*/
  updateTablet(idTablet) {
    this.dialogService.open(TabletCreateComponent, {
      context: {
        idTipo: 2,
        idTablet: idTablet,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  crearTeclado() {
    this.dialogService.open(TecladoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  updateTeclado(idTeclado) {
    this.dialogService.open(TecladoCreateComponent, {
      context: {
        idTipo: 2,
        idTeclado: idTeclado,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  crearMouse() {
    this.dialogService.open(MouseCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  updateMouse(idMouse) {
    this.dialogService.open(MouseCreateComponent, {
      context: {
        idTipo: 2,
        idMouse: idMouse,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  crearDiadema() {
    this.dialogService.open(DiademeCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();


    });
  }

  updateDiadema(idDiadema) {
    this.dialogService.open(DiademeCreateComponent, {
      context: {
        idTipo: 2,
        idDiadema: idDiadema,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();

    });
  }

  /** Funcion que manda un modal para asignar un equipo a un empleado*/
  asignarEquipo(id, tipoEquipos) {
    this.dialogService.open(AsignarEquipoComponent, {
      context: {
        id: id,
        tipoEquipos: tipoEquipos,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  // eliminarEquipo(idEquipo, tipoEquipos) {
  //   this.dialogService.open(DesactivarEquipoComponent, {
  //     context: {
  //       idEquipo: idEquipo,
  //       tipoEquipos: tipoEquipos,
  //
  //     },
  //   });
  // }

  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 0);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({
          'id': this.estadoInventariodata[i].id,
          'value': this.estadoInventariodata[i].estado,
        });

      }
    });
  }

  /** Función que desprende un modal para dar de baja un Equipo. */
  async eliminarEquipo(idEquipo, tipoEquipo) {

    /** switch de seleccion dependiendo el tipo de Equipo que de el usuario hara la busqueda por idEquipo*/
    switch (this.tipoEquipos) {
      case 'cpu':
        this.getCpuById(idEquipo);
        break;
      case 'monitor':
        this.getMonitorById(idEquipo);
        break;
      case 'laptop':
        this.getLaptopById(idEquipo);
        break;
      case 'tablet':
        this.getTabletById(idEquipo);
        break;
      case 'teclado':
        this.getTecladoById(idEquipo);
        break;
      case 'mouse':
        this.getMouseById(idEquipo);
        break;
      case 'diadema':
        this.getDiademaById(idEquipo);
        break;
      default:
        break;
    }

    /** Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,*/
    /** correspondiendo con los obtenidos desde el microservicio*/
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de baja este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { /** Si ya ha sido seleccionada una opción, debe de desactivar*/

      /**buscara por el tipo de Equipo y modificara el equipo*/
      switch (this.tipoEquipos) {
        case 'cpu':
          this.cpudesvincular['idEstadoInventario'] = Number(motivo);
          this.cpudesvincular['activo'] = 0;
          this.cpuService.put(idEquipo, this.cpudesvincular).subscribe(result => {
            this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
            this.cpuService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'monitor':
          this.monitordesvincular['idEstadoInventario'] = Number(motivo);
          this.monitordesvincular['activo'] = 0;
          this.monitorService.put(idEquipo, this.monitordesvincular).subscribe(result => {
            this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
            this.monitorService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });

          break;
        case 'laptop':
          this.laptopdesvincular['idEstadoInventario'] = Number(motivo);
          this.laptopdesvincular['activo'] = 0;
          this.laptopService.put(idEquipo, this.laptopdesvincular).subscribe(result => {
            this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
            this.laptopService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'tablet':
          this.tabletdesvincular['idEstadoInventario'] = Number(motivo);
          this.tabletdesvincular['activo'] = 0;
          this.tabletService.put(idEquipo, this.tabletdesvincular).subscribe(result => {
            this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tabletService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'teclado':
          this.tecladodesvincular['idEstadoInventario'] = Number(motivo);
          this.tecladodesvincular['activo'] = 0;
          this.tecladoService.put(idEquipo, this.tecladodesvincular).subscribe(result => {
            this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tecladoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'mouse':
          this.mousedesvincular['idEstadoInventario'] = Number(motivo);
          this.mousedesvincular['activo'] = 0;
          this.mouseService.put(idEquipo, this.mousedesvincular).subscribe(result => {
            this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
            this.mouseService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'diadema':
          this.diademadesvincular['idEstadoInventario'] = Number(motivo);
          this.diademadesvincular['activo'] = 0;
          this.diademaService.put(idEquipo, this.diademadesvincular).subscribe(result => {
            this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
            this.diademaService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de baja exitosamente!',
      }).then((resultado => {

        this.getEquipo();
      }));

    }
  }

  /**Busca el Equipo por medio de su id*/
  getCpuById(id) {
    this.cpuService.getCpuById(id).subscribe(data => {
      this.cpudesvincular = data;
    });
  }

  getMonitorById(id) {
    this.monitorService.getMonitorById(id).subscribe(data => {
      this.monitordesvincular = data;
    });
  }

  getLaptopById(id) {
    this.laptopService.getLaptopById(id).subscribe(data => {
      this.laptopdesvincular = data;
    });
  }

  getTabletById(id) {
    this.tabletService.getTabletById(id).subscribe(data => {
      this.tabletdesvincular = data;
    });
  }

  getTecladoById(id) {
    this.tecladoService.getTecladoById(id).subscribe(data => {
      this.tecladodesvincular = data;
    });
  }

  getMouseById(id) {
    this.mouseService.getMouseById(id).subscribe(data => {
      this.mousedesvincular = data;
    });
  }

  getDiademaById(id) {
    this.diademaService.getDiademaById(id).subscribe(data => {
      this.diademadesvincular = data;
    });
  }

  /**funcion para desasingnar el equipo de un empleado*/
  desasignarEquipo(idEquipo) {

    switch (this.tipoEquipos) {
      case 'cpu':
        this.getCpuById(idEquipo);
        break;
      case 'monitor':
        this.getMonitorById(idEquipo);
        break;
      case 'laptop':
        this.getLaptopById(idEquipo);
        break;
      case 'tablet':
        this.getTabletById(idEquipo);
        break;
      case 'teclado':
        this.getTecladoById(idEquipo);
        break;
      case 'mouse':
        this.getMouseById(idEquipo);
        break;
      case 'diadema':
        this.getDiademaById(idEquipo);
        break;
      default:
        break;
    }
    swal({
      title: '¿Deseas desvincular este equipo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Desvincular',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        switch (this.tipoEquipos) {
          case 'cpu':
            this.cpudesvincular.idEmpleado = null;
            this.cpuService.put(idEquipo, this.cpudesvincular).subscribe(result => {
              this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'monitor':
            this.monitordesvincular.idEmpleado = null;
            this.monitorService.put(idEquipo, this.monitordesvincular).subscribe(result => {
              this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
            });

            break;
          case 'laptop':
            this.laptopdesvincular.idEmpleado = null;
            this.laptopService.put(idEquipo, this.laptopdesvincular).subscribe(result => {
              this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'tablet':
            this.tabletdesvincular.idEmpleado = null;
            this.tabletService.put(idEquipo, this.tabletdesvincular).subscribe(result => {
              this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'teclado':
            this.tecladodesvincular.idEmpleado = null;
            this.tecladoService.put(idEquipo, this.tecladodesvincular).subscribe(result => {
              this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'mouse':
            this.mousedesvincular.idEmpleado = null;
            this.mouseService.put(idEquipo, this.mousedesvincular).subscribe(result => {
              this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'diadema':
            this.diademadesvincular.idEmpleado = null;
            this.diademaService.put(idEquipo, this.diademadesvincular).subscribe(result => {
              this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          default:
            break;
        }
        swal('¡El equipo se ha desvinculado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          // window.location.reload();
          this.getEquipo();
        }));
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.inventarioUsuario.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.inventarioUsuario.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
