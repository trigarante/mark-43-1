import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Turno, TurnoData} from '../../interfaces/catalogos/turno';

@Injectable({
  providedIn: 'root',
})
export class TurnosService extends TurnoData {
  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Turno[]> {
    return this.http.get<Turno[]>(this.baseURL + 'v1/turnos');
  }
  post(turno: Turno): Observable<Turno> {
    return this.http.post<Turno>(this.baseURL + 'v1/turnos', turno);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'turnos/saveTurno', json);
  }

  getTurnoById(idTurno): Observable<Turno> {
    return this.http.get<Turno>(this.baseURL + 'v1/turnos/' + idTurno);
  }

  put(idTurno, turno: Turno) {
    return this.http.put<Turno>(this.baseURL + 'v1/turnos/' + idTurno + '/'
      + sessionStorage.getItem('Empleado'), turno);
  }
  // putBaja(idTurno) {
   // return this.http.put<Turno>(this.baseURL + 'v1/turnos/baja-turno/' + idTurno + '/'
    //  + sessionStorage.getItem('Empleado'), idTurno);
  // }
}
