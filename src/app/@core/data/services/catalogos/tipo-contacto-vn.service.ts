import { Injectable } from '@angular/core';
import {TipoContactoVnData, TipoContactoVn} from '../../interfaces/catalogos/tipo-contacto-vn';
import { Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment} from '../../../../../environments/environment';

@Injectable()
export class TipoContactoVnService extends TipoContactoVnData {

  private baseURL;
  private socketURL;

  constructor( private http: HttpClient ) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.baseURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoContactoVn[]> {
    return this.http.get<TipoContactoVn[]>(this.baseURL + 'v1/tipoContactoVn');
  }

  post(contacto: TipoContactoVn): Observable<TipoContactoVn> {
    return this.http.post<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn', contacto);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vntipocontacto/saveVnTipoContacto', json);
  }

  getContactoById(idContacto): Observable<TipoContactoVn> {
    return this.http.get<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn/' + idContacto);
  }

  put(idContacto, contacto: TipoContactoVn): Observable<TipoContactoVn> {
    return this.http.put<TipoContactoVn>(this.baseURL + 'v1/tipoContactoVn/' + idContacto, contacto);
  }
}
