import {getTestBed, TestBed} from '@angular/core/testing';
import {PrecandidatosService} from './precandidatos.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';

fdescribe('PrecandidatosService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  let service: PrecandidatosService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = [{
    id: 24,
    idSolicitudRrhh: 416,
    nombre: 'AAAA',
    apellidoPaterno: 'N',
    apellidoMaterno: 'B',
    fechaNacimiento: '2001-12-07',
    email: 'D@WSE',
    genero: 'M',
    idEstadoCivil: 1,
    idEscolaridad: 23,
    idEstadoEscolaridad: 1,
    idPais: 1,
    cp: 1220,
    idColonia: 28007,
    calle: 'DASD',
    numeroExterior: 'ASD',
    numeroInterior: '',
    telefonoFijo: null,
    telefonoMovil: 5222222222,
    idEstadoRH: 3,
    fechaCreacion: '2019-09-18T05:00:00.000+0000',
    fechaBaja: '2019-09-12T06:00:00.000+0000',
    tiempoTraslado: 15,
    idMedioTraslado: 6,
    idEstacion: null,
    estadoCivil: 'SOLTERO',
    escolaridad: 'POSGRADO',
    estadoEscolar: 'TERMINADO',
    estadoRh: 'ABANDONO DE TRABAJO',
    medio: 'PRUEBA',
    estacion: null,
    lineas: null,
    idEtapa: 1,
    curp: 'HEHM930303HDFLRR01',
    recontratable: 0,
    detalleBaja: 'EDWSF',
    fechaRegistroBaja: '2019-09-18T18:06:19.000+0000',
    nombreEtapa: 'PreCandidatos',
    idEstadoRRHH: 0,
    subarea: 'RENOVACIONES GNP',
    sede: 'VALLEJO',
    idSubarea: 6,
    idSede: 1,
    idGrupo: 1,
    grupo: 'GRUPO AHORRA',
    recontratables: 'NO',
    colonia: 'Cuevitas',
  },
    {
      id: 'precandidatos',
    }];
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [PrecandidatosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(PrecandidatosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/precandidatos/get-all/74');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/precandidatos', '/id/24');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/precandidatos/440/1008136800000');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/precandidatos', '/24/861134400000/74');
      });
    });
  }
});
