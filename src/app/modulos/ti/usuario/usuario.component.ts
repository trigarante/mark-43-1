import {Component, OnInit, ViewChild} from '@angular/core';
import {Correo, CorreoData} from '../../../@core/data/interfaces/ti/correo';
import {NbDialogService} from '@nebular/theme';
import {Usuario, UsuarioData} from '../../../@core/data/interfaces/ti/usuario';
import {LicenciasDisponibles, LicenciasDisponiblesData} from '../../../@core/data/interfaces/ti/licencias-disponibles';
import {map} from 'rxjs/operators';
import {DatosUsuariosComponent} from '../modals/datos-usuarios/datos-usuarios.component';
import {ModificarAliasComponent} from '../modals/modificar-alias/modificar-alias.component';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {Extension, ExtensionData} from '../../../@core/data/interfaces/ti/extension';
import {ModificarTotalLicenciasComponent} from '../modals/modificar-total-licencias/modificar-total-licencias.component';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource} from '@angular/material';
import {ModificarExtensionComponent} from '../modals/modificar-extension/modificar-extension.component';
import swal from 'sweetalert';
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss'],
})
export class UsuarioComponent extends MatPaginatorIntl implements OnInit {
  cols: any[];
  cols2: any[];
  /**variables para ver el total de licencias disponibles*/
  correoejecutivos: Correo[];
  correoAdministrativos: Correo[];
  cAdminusados: number;
  correoEjecutivo: Correo[];
  correoAsistencia: Correo[];
  cEjecusados: number;
  cAsistenciausados: number;
  totalAdmin: number;
  totalEjecu: number;
  totalAsistencia: number;
  libresadministrativos: any;
  libresejecutivos: any;
  libresasistencia: any;
  usuario: Usuario[];
  licencias: LicenciasDisponibles[];
  datosUsuario: string;
  datosusuarios: any[];
  empleado: Empleados;
  datosusuarioss: any;
  extensiondato: any;
  extension: Extension;
  usuarioid: Usuario;
  dataSource: any;
  dataSource2: any;
  // socket
  private stompClient1 = null;
  private stompClient2 = null;
  boton: number;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(
    private correoService: CorreoData,
    public dialogService: NbDialogService,
    private usuarioService: UsuarioData,
    private licenciasService: LicenciasDisponiblesData,
    private EmpleadoService: EmpleadosData,
    private extensionService: ExtensionData,
    private router: Router) {
    super();
    this.getAndInitTranslations();
  }

  connect() {
    const _this = this;
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tiusuarioslicencias');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient1.subscribe('/task/panelTiUsuariosLicencias', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.conseguirlicencias();
        }, 500);
      });
    });
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'tiusuarios');
    this.stompClient2 = Stomp.over(socket2);
    this.stompClient2.connect({}, function (frame) {
      if (this.boton === undefined || this.boton === 0) {
        console.log('------>0 ' + this.boton);
        console.log(frame);
        _this.stompClient2.subscribe('/task/panelTiUsuarios', (content) => {
          console.log(content.body);
          setTimeout(() => {
            _this.setUsuario();
          }, 500);
        });
      }
      if (this.boton === 1) {
        console.log('------>1 ' + this.boton);
        console.log(frame);
        _this.stompClient2.subscribe('/task/panelTiUsuarios', (content) => {
          console.log(content.body);
          setTimeout(() => {
            _this.setUsuariosSinCorreos();
          }, 500);
        });
      }
      if (this.boton === 2) {
        console.log('------>2 ' + this.boton);
        console.log(frame);
        _this.stompClient2.subscribe('/task/panelTiUsuarios', (content) => {
          console.log(content.body);
          setTimeout(() => {
            _this.setUsuariosConCorreos();
          }, 500);
        });
      }
    });
  }

  getAndInitTranslations() {

    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  setUsuario() {
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.id !== 1);
    })).subscribe(data => {
      this.usuario = data;
      this.dataSource = new MatTableDataSource(this.usuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  setUsuariosSinCorreos() {
    this.boton = 1;
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.asignado === 0 && data.id !== 1);
    })).subscribe(data => {
      this.usuario = data;
      this.dataSource = new MatTableDataSource(this.usuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  setUsuariosConCorreos() {
    this.boton = 2;
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.asignado === 1 && data.id !== 1);
    })).subscribe(data => {
      this.usuario = data;
      this.dataSource = new MatTableDataSource(this.usuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

  }

  conseguirlicencias() {
    this.licenciasService.get().subscribe(data => {
      this.licencias = data;
      this.dataSource2 = new MatTableDataSource(this.licencias);
      this.correoService.get('ejecutivo').subscribe(dataejecutivo => {
        this.correoEjecutivo = dataejecutivo;

        this.cEjecusados = this.correoEjecutivo.length;

        this.correoService.get('administrativo').subscribe(dataAdmin => {
          this.correoAdministrativos = dataAdmin;
          this.cAdminusados = this.correoAdministrativos.length;
          this.correoService.get('asistencia').subscribe(dataAsist => {
            this.correoAsistencia = dataAsist;
            this.cAsistenciausados = this.correoAsistencia.length;
            this.libresejecutivos = this.licencias[0].cantidad - this.cEjecusados;
            this.libresadministrativos = this.licencias[2].cantidad - this.cAdminusados;
            this.libresasistencia = this.licencias[1].cantidad - this.cAsistenciausados;

          }); // fin de Asist
        }); // fin de admi
      }); // fin de ejecutivos

    }); // fin de licencias
  }

  licenciasEjecutivos() {

  }

  setLicencias() {
    this.dialogService.open(ModificarTotalLicenciasComponent, {
      context: {},
    }).onClose.subscribe(x => {
      this.conseguirlicencias();
    });
  }

  verDatosUsuarios(idUsuario: number, usuario: string, idCorreo: string) {
    this.dialogService.open(DatosUsuariosComponent, {
      context: {
        idUsuario: idUsuario,
        usuario: usuario,
        idCorreo: idCorreo,
      },
    });
  }

  cambiarAlias(idUsuario: number, usuario: string, idCorreo: string) {
    this.dialogService.open(ModificarAliasComponent, {
      context: {
        idUsuario: idUsuario,
        usuario: usuario,
        idCorreo: idCorreo,
      }
      ,
    });
  }

  /** Funcón que obtiene la información de un empleado según su ID. */
  getEmpleadoById(idEmpleado) {
    this.EmpleadoService.getEmpleadoById(idEmpleado).subscribe(data => {
      this.empleado = data;
    });
  }

  /** funcion que desvincula el correo, borrando de empleado el idUsuario, asi queda libre el correo*/
  desvincularCorreo(idEmpleado) {
    this.getEmpleadoById(idEmpleado);
    swal({
      title: '¿Deseas desvincular este correo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.empleado['idUsuario'] = null;
        let fechIngreso: any;
        fechIngreso = Date.parse(this.empleado.fechaIngreso);
        this.EmpleadoService.put(idEmpleado, this.empleado, fechIngreso).subscribe(result => {
          this.usuarioService.postSocketTiUsuarios({status: 'ok'}).subscribe(x => {
          });
        });
        swal('¡El correo se ha desvinculado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.setUsuario();
        }));
      }
    });
  }

  getExtensionById(id) {
    this.extensionService.get(id).subscribe(data => {
      this.extension = data;
    });
  }

  /** funcion para dejar libre la extension*/
  modificarExtension(idUsuario, datos) {
    this.dialogService.open(ModificarExtensionComponent, {
      context: {
        idUsuario: idUsuario,
        // usuario: usuario,
        // idCorreo: idCorreo,
      },
    });
  }

  ngOnInit() {
    this.connect();
    this.setUsuario();

    this.licenciasEjecutivos();
    this.conseguirlicencias();
    this.cols = [
      'datosusuarios',
      'id',
      'idcorreo',
      'usuario',
      'idempleado',
      'grupousuario',
      'asignado',
      'asignaciones',
    ];
    this.cols2 = [
      'idlicencia',
      'cantidad',
      'libres',
    ];
    this.getPermisos();
    this.estaActivo();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.usuariosTi.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.usuariosTi.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
