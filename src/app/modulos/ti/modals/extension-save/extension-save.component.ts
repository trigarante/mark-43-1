import { Component, OnInit } from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-extension-save',
  templateUrl: './extension-save.component.html',
  styleUrls: ['./extension-save.component.scss'],
})
export class ExtensionSaveComponent implements OnInit {

  constructor(protected ref: NbDialogRef<ExtensionSaveComponent>, private router: Router) { }
  dismiss() {

    this.ref.close();
    this.router.navigate(['modulos/ti/extensiones']);
  }
  ngOnInit() {
  }

}
