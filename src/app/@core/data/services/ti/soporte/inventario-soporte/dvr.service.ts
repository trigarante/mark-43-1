import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Dvr, DvrData} from '../../../../interfaces/ti/soporte/inventario-soporte/dvr';

@Injectable()
export class DvrService extends DvrData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Dvr[]> {
    return this.http.get<Dvr[]>(this.baseURL + 'v1/Dvr');
  }

  post(dvr: Dvr): Observable<Dvr> {
    return this.http.post<Dvr>(this.baseURL + 'v1/Dvr', dvr);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'dvr/saveDvr', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajadvr/saveBajaDvr', json);
  }

  getDvrById(idDvr): Observable<Dvr> {
    return this.http.get<Dvr>(this.baseURL + 'v1/Dvr/' + idDvr);
  }

  put(idDvr, dvr: Dvr): Observable<Dvr> {
    return this.http.put<Dvr>(this.baseURL + 'v1/Dvr/' + idDvr + '/'
      + sessionStorage.getItem('Empleado')  , dvr);
  }
}
