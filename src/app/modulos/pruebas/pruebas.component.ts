import { Component, OnInit } from '@angular/core';
import { UserService } from '../../@core/data/users.service';
import { User } from '../../@core/data/interfaces/ti/user';

@Component({
  selector: 'ngx-pruebas',
  templateUrl: './pruebas.component.html',
  styleUrls: ['./pruebas.component.scss'],
})
export class PruebasComponent implements OnInit {
  users: User;
  cols: any[];
  dataSource: any;
  tareas: any[];
  constructor(
    private userService: UserService,
  ) {}
  ngOnInit() {}

}
