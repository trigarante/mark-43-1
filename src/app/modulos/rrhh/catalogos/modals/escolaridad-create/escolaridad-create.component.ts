import {Component, Input, OnInit} from '@angular/core';
import {Escolaridad, EscolaridadData} from '../../../../../@core/data/interfaces/catalogos/escolaridad';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {EscolaridadComponent} from '../../escolaridad/escolaridad.component';
import {Router} from '@angular/router';
import {withIdentifier} from 'codelyzer/util/astQuery';
/** Componente del modal del catálogo para "crear escolaridad". */
@Component({
  selector: 'ngx-escolaridad-create',
  templateUrl: './escolaridad-create.component.html',
  styleUrls: ['./escolaridad-create.component.scss'],
})
export class EscolaridadCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idEscolaridad obtenido. */
  @Input() idEscolaridad: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una escolaridad. */
  escolaridadCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z ]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  escolaridad: Escolaridad;
  escolaridadEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EscolaridadData} escolaridadService Servicio del componente escolaridad para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(protected escolaridadService: EscolaridadData, private fb: FormBuilder, protected ref: NbDialogRef<EscolaridadComponent>,
              private router: Router) {
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarEscolaridad() {
    if (this.escolaridadCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.escolaridadService.post(this.escolaridadCreateForm.value).subscribe((result) => {
      this.escolaridadService.postSocket(this.escolaridadCreateForm.value).subscribe(x => {});
      this.escolaridadCreateForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/escolaridad']);
      this.escolaridadCreateForm.reset();
      this.ref.close();
      //window.location.reload();

    });
  }

  /** Función que obtiene la información de la escolaridad dependiendo de su ID. */
  getEscolaridadById() {
    this.escolaridadService.getEscolaridadById(this.idEscolaridad).subscribe( data => {
      this.escolaridad = data;
      this.escolaridadCreateForm.controls['nivel'].setValue(this.escolaridad.nivel);
      this.escolaridadCreateForm.controls['activo'].setValue(this.escolaridad.activo);

    });
  }

  /** Función que actualiza los datos de la escolaridad. */
  updateEscolaridad() {
    this.escolaridadService.getEscolaridadById(this.idEscolaridad).subscribe( data => {
      if(data.activo === 1){
        this.escolaridadService.put(this.idEscolaridad, this.escolaridadCreateForm.value).subscribe( result => {
          this.escolaridadService.postSocket(this.escolaridadCreateForm.value).subscribe(x => {});
          this.escolaridadCreateForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.escolaridadEliminado = true;
      }
    });

  }

  /** Función que da de baja una escolaridad, cambiando el atributo activo a 0. */
  bajaEscolaridad() {
    this.escolaridadCreateForm.controls['activo'].setValue(0);
    this.escolaridadService.put(this.idEscolaridad, this.escolaridadCreateForm.value).subscribe( result => {
      this.escolaridadCreateForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEscolaridadById();
    } else if (this.idTipo === 3) {
      this.getEscolaridadById();
    }
    this.escolaridadCreateForm = this.fb.group({
      'nivel': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
