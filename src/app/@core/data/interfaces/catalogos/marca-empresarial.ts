import {Observable} from 'rxjs';

export interface MarcaEmpresarial {
  marca: string;
  id: number;
  activo: number;
  idSede: number;
  detalle: string;
  codigo: string;
}
export abstract class MarcaEmpresarialData {
  abstract get(): Observable<MarcaEmpresarial[]>;

  abstract post(marcaEmpresarial): Observable<MarcaEmpresarial>;

  abstract getMarcaEmpresarialById(idMarcaEmpresarial): Observable<MarcaEmpresarial>;

  abstract put(idMarcaEmpresarial, marcaEmpresarial): Observable<MarcaEmpresarial>;

}
