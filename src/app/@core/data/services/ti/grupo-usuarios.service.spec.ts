import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Grupo Permisos service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
  },
    {
      id: 'grupos',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/grupos');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/grupos', '/84');
      }); /*
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/grupos');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/grupos', '/3/74');
      });*/
    });
  }
})
