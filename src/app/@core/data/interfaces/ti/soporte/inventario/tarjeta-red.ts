import {Observable} from 'rxjs';


export interface TarjetaRed {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class TarjetaRedData {
  abstract get(): Observable<TarjetaRed[]>;
  abstract post(tarjetaRed): Observable<TarjetaRed>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getTarjetaRedById(idTarjetaRed): Observable<TarjetaRed>;
  abstract put(idTarjetaRed, tarjetaRed): Observable<TarjetaRed>;
}
