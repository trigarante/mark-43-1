import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarPaginaComponent } from './actualizar-pagina.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Router, RouterModule} from '@angular/router';
import {PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {HttpClient} from '@angular/common/http';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData} from '../../../../@core/data/interfaces/catalogos/area';

describe('ActualizarPaginaComponent', () => {
  let component: ActualizarPaginaComponent;
  let fixture: ComponentFixture<ActualizarPaginaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarPaginaComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
      ],
      providers: [
        PaginasData,
        SubareaData,
        SedeData,
        GrupoData,
        EmpresaData,
        AreaData,
        HttpClient,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarPaginaComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
