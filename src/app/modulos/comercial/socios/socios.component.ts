import {Component, OnInit, ViewChild} from '@angular/core';
import {SociosComercial, SociosData} from '../../../@core/data/interfaces/comerciales/socios-comercial';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {SociosCreateComponent} from '../modals/socios-create/socios-create.component';
import {UrlCreateComponent} from '../modals/url-create/url-create.component';
import {EjecutivoCuentaCreateComponent} from '../modals/ejecutivo-cuenta-create/ejecutivo-cuenta-create.component';
import {AsignarPresupuestoComponent} from '../modals/asignar-presupuesto/asignar-presupuesto.component';
import {AsignarContratosComponent} from '../modals/asignar-contratos/asignar-contratos.component';
import {DatosSociosComponent} from '../modals/datos-socios/datos-socios.component';
import {map} from 'rxjs/operators';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource} from '@angular/material';
import {AsignarRamoComponent} from '../modals/asignar-ramo/asignar-ramo.component';
import swal from 'sweetalert';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.scss'],
})
export class SociosComponent extends MatPaginatorIntl implements OnInit {
  cols: any[];
  socios: SociosComercial[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  socio: SociosComercial;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private sociosService: SociosData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
  ) {
    super();
    this.getAndInitTranslations();
  }

  ngOnInit() {
    this.getSocios(0);
    this.cols = [
      'detalle',
      'nombreComercial',
      'razonSocial',
      'alias',
      'prioridad',
      'estado',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) {
      this.datos = [];
    }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'socios');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame){
      _this.setConnected(true);

      _this.stompClient.subscribe('/task/panelSocios', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getSocios(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getAndInitTranslations() {
    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) =>  {
    if (length === 0 || pageSize === 0)  {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getSocioById(idSocio) {
    this.sociosService.getSociosById(idSocio).subscribe(res => {
      this.socio = res;
    });
  }

  getSocios(mensajeAct) {
    this.sociosService.get().pipe(map(result => {
      return result.filter(data => data.activo === '1');
    })).subscribe(data => {
      this.socios = [];
      this.socios = data;
      this.dataSource = new MatTableDataSource(this.socios); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      for (let i = 0; i < data.length; i++) {
        if (this.socios[i].idEstadoSocio === 1) {
          this.socios[i].idEstadoSocio = 0;
        }else if (this.socios[i].idEstadoSocio === 2) {
          this.socios[i].idEstadoSocio = 1;
        }
      }
    });
    if (mensajeAct === 1) {
      this.showToast() ;
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Socios`);
  }

  crearSocios() {
    this.dialogService.open(SociosCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => {
      this.getSocios(0);
    });
  }

  AsignarUrl(idSocio) {
    this.dialogService.open(UrlCreateComponent, {
      context: {
        idTipo: 1,
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(0));
  }

  verDatos(idSocio) {
    this.dialogService.open(DatosSociosComponent, {
      context: {
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(1));
  }

  asignarEjecutivoCuenta(idSocio) {
    this.dialogService.open(EjecutivoCuentaCreateComponent, {
      context: {
        idTipo: 1,
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(0));
  }

  asignarPresupuesto(idSocio) {
    this.dialogService.open(AsignarPresupuestoComponent, {
      context: {
        idTipo: 1,
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(0));
  }

  asignarContratos(idSocio) {
    this.dialogService.open(AsignarContratosComponent, {
      context: {
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(0));
  }

  asignarRamo(idSocio) {
    this.dialogService.open(AsignarRamoComponent, {
      context: {
        idTipo: 1,
        idSocio: idSocio,
      },
    }).onClose.subscribe( x => this.getSocios(0));
  }

  desactivarSocio(idSocio) {
    this.getSocioById(idSocio);
    swal({
      title: '¿Deseas desactivar este socio?',
      text: 'Una vez desactivado lo podrás visualizar de color ROJO',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Desactivar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.socio['idEstadoSocio'] = 1;
        this.sociosService.put(idSocio, this.socio).subscribe( result => {
        });
        swal('¡El socio se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getSocios(0);
        }));
      }
    });
  }

  reactivarSocio(idSocio) {
    this.getSocioById(idSocio);
    swal({
      title: '¿Deseas reactivar este socio?',
      // text: 'Una vez activado lo podrás visualizar de color ROJO',
      icon: 'info',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Reactivar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.socio['idEstadoSocio'] = 2;
        this.sociosService.put(idSocio, this.socio).subscribe( result => {
        });
        swal('¡El socio se ha reactivado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getSocios(0);
        }));
      }
    });
  }

  bajaSocio(idSocio) {
    this.getSocioById(idSocio);
    swal({
      title: '¿Deseas dar de baja este socio?',
      // text: 'Una vez activado lo podrás visualizar de color ROJO',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.socio['activo'] = '0';
        this.sociosService.put(idSocio, this.socio).subscribe( result => {
          this.sociosService.postSocket({idSocio: idSocio, socio: this.socio}).subscribe(x => {});
        });
        swal('¡El socio se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getSocios(0);
        }));
      }
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verSocios.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.verSocios.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
