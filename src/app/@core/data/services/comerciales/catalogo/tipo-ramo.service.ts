import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoRamo, TipoRamoData} from '../../../interfaces/comerciales/catalogo/tipo-ramo';

@Injectable({
  providedIn: 'root',
})
export class TipoRamoService extends  TipoRamoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoRamo[]> {
    return this.http.get<TipoRamo[]>(this.baseURL + 'v1/tipo-ramo');
  }

  post(tipoRamo: TipoRamo): Observable<TipoRamo> {
    return this.http.post<TipoRamo>(this.baseURL + 'v1/tipo-ramo', tipoRamo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tiporamo/saveTipoRamo', json);
  }

  getTipoRamoById(idTipoRamo): Observable<TipoRamo> {
    return this.http.get<TipoRamo>(this.baseURL + 'v1/tipo-ramo/' + idTipoRamo);
  }

  put(idTipoRamo, tipoRamo: TipoRamo): Observable<TipoRamo> {
    return this.http.put<TipoRamo>(this.baseURL + 'v1/tipo-ramo/' + idTipoRamo + '/'
      + sessionStorage.getItem('Empleado') , tipoRamo);
  }
}
