/**
 * se importan las interfaces de preguntas y areas
 */

import {AreasInterface} from './areas-interface';
import {RespuestasInterfaces} from './respuestas-interface';
import {PreguntasInterfaces} from './preguntas-interfaces';

/**
 * Se exporta la Interfaz
 */

export interface AreasPreguntasRespuestasInterface {
  /**
   * se declara el valor de las interfaces en area y pregunta y se le asigna un valor a la variable respuesta
   */
  area: AreasInterface;
  pregunta: PreguntasInterfaces;
  respuesta: RespuestasInterfaces[];

}
