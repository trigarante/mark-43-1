import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Inspecciones, InspeccionesData} from '../../interfaces/ventaNueva/inspecciones';

@Injectable()
export class InspeccionesService extends InspeccionesData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Inspecciones[]> {
    return this.http.get<Inspecciones[]>(this.baseURL + 'v1/inspecciones');
  }

  post(Inspeccion: Inspecciones): Observable<Inspecciones> {
    return this.http.post<Inspecciones>(this.baseURL + 'v1/inspecciones/', Inspeccion);
  }

  getInspeccionById(idInspeccion): Observable<Inspecciones> {
    return this.http.get<Inspecciones>(this.baseURL + 'v1/inspecciones/' + idInspeccion);
  }

  put(idInspeccion, Inspeccion: Inspecciones): Observable<Inspecciones> {
    return this.http.put<Inspecciones>(this.baseURL + 'v1/inspecciones/' + idInspeccion, Inspeccion);
  }
}
