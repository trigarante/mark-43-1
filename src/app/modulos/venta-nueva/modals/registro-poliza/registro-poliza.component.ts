import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../@core/data/users.service';
import {map} from 'rxjs/operators';
import {
  EstadoPolizaVn,
  EstadoPolizaVnData,
} from '../../../../@core/data/interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {TipoPagoData, TipoPagoVn} from '../../../../@core/data/interfaces/ventaNueva/catalogos/tipo-pago-vn';
import {TipoSubramo, TipoSubramoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {FlujoPolizaData, FlujoPolizaVn} from '../../../../@core/data/interfaces/ventaNueva/catalogos/flujo-poliza-vn';
import {ProductoData, ProductoSocios} from '../../../../@core/data/interfaces/comerciales/producto-socios';
import {FilesUploadComponent} from '../../../rrhh/filesupload/filesupload.component';
import {MatBottomSheet, MatCheckbox} from '@angular/material';
import {AppContext} from '../../../../@core/data/services/app.context';
import {RegistrosData} from '../../../../@core/data/interfaces/ventaNueva/registro';
import {SolicitudesVNData} from '../../../../@core/data/interfaces/ventaNueva/solicitudes';
import {ProductoClienteData} from '../../../../@core/data/interfaces/ventaNueva/producto-cliente';

@Component({
  selector: 'ngx-registro-poliza',
  templateUrl: './registro-poliza.component.html',
  styleUrls: ['./registro-poliza.component.scss'],
})
export class RegistroPolizaComponent implements OnInit {
  @Input () idProductoCliente: number;

  registroForm: FormGroup;
  // /**VAribale que almacenara el usuarios del GetCurrent*/
  // usuario: any;
  // /**VAriable que obtiene el nombre del Token*/
  // name?: any;
  tipoPagos: TipoPagoVn[];
  estadoPolizas: EstadoPolizaVn[];
  // estadoPoliza: any;
  SubRamos: TipoSubramo[];
  SubRamo: any;
  flujoPolizas: FlujoPolizaVn[];
  // flujPoliza: any;
  productosSocio: ProductoSocios[];
  productoSocios: any;
  @Output() _idRegistroPoliza = new EventEmitter<number>();
  @Output() _recibos = new EventEmitter<boolean>();
  idRegistroPoliza: number;
  urlPreventa: any;
  @ViewChild('checkin') checkin: MatCheckbox;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private subramoServices: TipoSubramoData,
              private productoSocioService: ProductoData,
              private tipoPagoService: TipoPagoData,
              private estadoPolizaService: EstadoPolizaVnData,
              private registroService: RegistrosData,
              private flujoPolizaService: FlujoPolizaData,
              private bottomSheet: MatBottomSheet,
              private appContext: AppContext,
              private solicitudesService: SolicitudesVNData,
              private productoClienteService: ProductoClienteData) { }

  ngOnInit() {
    this.productoClienteService.returnAseguradora().subscribe(
      result => {
        this.urlPreventa = result;
        this.registroForm = this.fb.group({
          'idEmpleado': new FormControl(''),
          'idProducto': new FormControl(''),
          'idTipoPago': new FormControl('', Validators.required),
          'idEstadoPoliza': new FormControl('', Validators.required),
          // 'idTipoSubRamo': new FormControl('', Validators.required),
          'idProductoSocio': new FormControl('', Validators.required),
          'idFlujoPoliza': new FormControl('', Validators.required),
          'poliza': new FormControl('', Validators.required),
          'fechaInicio': new FormControl('', Validators.required),
          'primaNeta': new FormControl('', Validators.required),
          'archivo': new FormControl(''),
        });
      },
      error => {},
      () => {
      });
    this.getTipoSubRamo();
    this.getProductoSocio();
    this.getTipoPago();
    this.getEstadoPoliza();
    this.getFlujoPoliza();
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getTipoPago() {
    this.tipoPagoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoPagos = result;
    });
  }

  getEstadoPoliza() {
    this.estadoPolizaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => { this.estadoPolizas = result; });
  }

  getTipoSubRamo() {
    this.subramoServices.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(resultado => {
      this.SubRamos = resultado;
      this.SubRamo = [];
      for (let i = 0; i < this.SubRamos.length; i++) {
        this.SubRamo.push({'label': this.SubRamos[i].tipo, 'value': this.SubRamos[i].id});
      }
    });
  }

  getProductoSocio() {
    this.productoSocioService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.productosSocio = result;
      this.productoSocios = [];
      for (let i = 0; i < this.productosSocio.length; i++) {
        this.productoSocios.push({'label': this.productosSocio[i].nombre, 'value': this.productosSocio[i].id});
      }
    });
  }

  getFlujoPoliza() {
    this.flujoPolizaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => { this.flujoPolizas = result; });
  }

  crearRegistro() {
    this.registroForm.controls['idEmpleado'].setValue(74);
    this.registroForm.controls['idProducto'].setValue(70);
    this.registroForm.controls['archivo'].setValue('123456tyghnj9876f');
    this.registroService.post(this.registroForm.value).subscribe(
      result => {
        this.solicitudesService.entryIdRegistroPoliza(result.id);
        /**Valida si pertenece a QUALITAS*/
        if (this.urlPreventa === 1) {
          /**Si pertenece valida si el check esta en TRUe*/
          if (this.checkin.checked) {
            this.solicitudesService.entryURLType(0);
          } else {
            this.solicitudesService.entryURLType(1);
          }
        } else {
          this.solicitudesService.entryURLType(2);
        }
      },
    );
  }

  createNewFile($event) {
    // this.appContext.Session.Gapi.signIn()
    //   .then(() => {
    //     if (this.appContext.Session.Gapi.isSignedIn) {
    // this.router.navigate(['/modulos/rrhh/documentos', documento, id],
    //   { state: { title: 'ADMINISTRATIVO' }});
    this.bottomSheet.open(FilesUploadComponent, {data: $event.target.files});
    //   }
    // });
    // this.appContext.Session.BreadCrumb.currentItem.Name = 'CURP EMPLEADO ' + this.id;
  }

}
