import {Observable} from 'rxjs';

export interface MedioTransporte {
  id: number;
  medio: string;
  activo: number;
}

export abstract class MedioTransporteData {
  abstract get(): Observable<MedioTransporte[]>;
  abstract post(medioTransporte: MedioTransporte): Observable<MedioTransporte>;
  abstract postSocket(json): Observable<JSON>;
  abstract getMedioTransporteById(idMedioTransporte): Observable<MedioTransporte>;
  abstract put(idMedioTransporte, medioTransporte): Observable<MedioTransporte>;
}
