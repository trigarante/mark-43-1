import {Component, Input, OnInit, Output} from '@angular/core';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GuardadoComponent} from '../guardado/guardado.component';
import {ClipboardService} from 'ngx-clipboard';

@Component({
  selector: 'ngx-ver-configuracion',
  templateUrl: './ver-configuracion.component.html',
  styleUrls: ['./ver-configuracion.component.scss'],
})
export class VerConfiguracionComponent implements OnInit {
  @Input() id: number;
  @Input() tipo: number;
  configuracionForm: FormGroup;
  pagina: Pagina;
  cols: any[];
  aseguradoras: any;
  textoFormateado: any;
  copiaJson: any;
  isCopied1: boolean;

  constructor( private paginaService: PaginasData, protected ref: NbDialogRef<VerConfiguracionComponent>,
               private fb: FormBuilder, public dialogService: NbDialogService,
               private _clipboardService: ClipboardService ) {
    this.configuracionForm = this.fb.group({
      'configuracion': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  getPaginaById() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      let obje;
      obje = JSON.parse(this.pagina.configuracion);
      this.textoFormateado = JSON.stringify(obje, undefined, 2);
      console.log(this.textoFormateado);
    });
  }
  guardarCampos() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.pagina['configuracion'] = this.configuracionForm.value.configuracion;


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.parse(this.pagina.fechaActualizacion);

      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocket({status: 'ok'}).subscribe(x => {});
        this.ref.close();
        this.dialogService.open(GuardadoComponent, {
          context: {
            id: this.id,
            modo: 2,
          },
        });
      });
    });
  }
  pegar() {
    navigator['clipboard'].readText().then(clipText => {
      // do something with copied text here
      this.configuracionForm.controls['configuracion'].setValue(clipText)
    });
  }
  copy(text: string) {
    this._clipboardService.copyFromContent(text);
  }
  dismiss() {
    this.ref.close();
    this.cols = [ 'configuracion'];
  }
  ngOnInit() {
    if (this.tipo === 1) {
      this.getPaginaById();
    }
  }

}
