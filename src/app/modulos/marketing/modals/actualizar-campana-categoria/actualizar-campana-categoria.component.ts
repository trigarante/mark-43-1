import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {CampanaMarketing, CampanaMarketingData} from '../../../../@core/data/interfaces/marketing/campana';
import {CampanaCategoria, CampanaCategoriaData} from '../../../../@core/data/interfaces/marketing/campana-categoria';
import {map} from 'rxjs/operators';
import {Categoria, CategoriaData} from '../../../../@core/data/interfaces/comerciales/catalogo/categoria';
import {GuardadoComponent} from '../guardado/guardado.component';
import {SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';


@Component({
  selector: 'ngx-actualizar-campana-categoria',
  templateUrl: './actualizar-campana-categoria.component.html',
  styleUrls: ['./actualizar-campana-categoria.component.scss'],
})
export class ActualizarCampanaCategoriaComponent implements OnInit {
 @Input() id: number;
  actualizarCampanaCategoriaForm: FormGroup;
  campanaCategoria: any;
  /**Areglo que inicia los valores del Microservicio*/
  campanaMarketing: CampanaMarketing[];
  /**Arreglo auxiliar para  campanaMarketing*/
  campanasMarketing: any;

  /**Arreglo auxiliar para  categoria*/
  categorias: any;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<ActualizarCampanaCategoriaComponent>,
              protected campanaMarketingService: CampanaMarketingData, private campanaCategoriaService: CampanaCategoriaData,
              private categoriaService: SubramoData, public dialogService: NbDialogService) {
    this.actualizarCampanaCategoriaForm = this.fb.group({
      'idCampana': new FormControl(),
      'idSubRamo': new FormControl(),
      'comentarios': new FormControl(),
    });
  }
  getCampanaById() {
    this.campanaCategoriaService.getCampanasCategoriaById(this.id).subscribe(data => {
      this.campanaCategoria = data;
      this.actualizarCampanaCategoriaForm.controls['idCampana'].setValue(this.campanaCategoria.idCampana);
      this.actualizarCampanaCategoriaForm.controls['idSubRamo'].setValue(this.campanaCategoria.idSubRamo);
      this.actualizarCampanaCategoriaForm.controls['comentarios'].setValue(this.campanaCategoria.comentarios);
    });

}
 getCampana() {
    this.campanaMarketingService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(campanadata => {
      this.campanaMarketing = [];
      this.campanasMarketing = campanadata;

    });
 }
 getCategoria() {
    this.categoriaService.get().pipe(map( data => {
      return data.filter(result => result.activo === 1); })).subscribe(categoriadata => {

      this.categorias = categoriadata;
    });
 }
 guardarActualizar() {
    this.campanaCategoriaService.getCampanasCategoriaById(this.id).subscribe(data => {
    this.campanaCategoria = data;
    this.campanaCategoria['idCampana'] = this.actualizarCampanaCategoriaForm.value.idCampana;
    this.campanaCategoria['idSubRamo'] = this.actualizarCampanaCategoriaForm.value.idSubRamo;
    this.campanaCategoria['comentarios'] = this.actualizarCampanaCategoriaForm.value.comentarios;

    this.campanaCategoriaService.put(this.id, this.campanaCategoria).subscribe( result => {
      this.campanaCategoriaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ref.close();
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
        },
      });
    });
    });
 }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getCampanaById();
    this.getCampana();
    this.getCategoria();
  }

}
