import { TestBed } from '@angular/core/testing';
import { TipoCompetenciaService } from './tipo-competencia.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../matchers';
fdescribe('TipoCompetenciaService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 4,
    tipo: 'COMP',
    activo: 0,
  },
    {
      id: 'tipocompetencia',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoCompetenciaService, HttpClient],
  }));

  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tipo-competencia');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tipo-competencia', '/4');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tipo-competencia');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tipo-competencia', '/4/74');
      });
    });
  }
});
