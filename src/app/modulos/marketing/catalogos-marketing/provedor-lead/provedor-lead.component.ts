import {Component, OnInit, ViewChild} from '@angular/core';
import { ProveedorLeads, ProveedorLeadsData } from '../../../../@core/data/interfaces/catalogos/proveedor-leads';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import { ProveedoresLeadCreateComponent } from '../modals/proveedores-lead-create/proveedores-lead-create.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';



@Component({
  selector: 'ngx-provedor-lead',
  templateUrl: './provedor-lead.component.html',
  styleUrls: ['./provedor-lead.component.scss'],
})
export class ProveedorLeadComponent implements OnInit {


  dataSource: any;
  proveedorLead: ProveedorLeads[];
  proveedoresLead: ProveedorLeads;
  cols: any[];
  permisos: any;
  escritura: boolean;
  // sockets
  private stompClient = null;
  constructor(protected proveedorLeadService: ProveedorLeadsData, private dialogoService: NbDialogService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'provedoresleads');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelProvedoresLeads', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getProveedoresLead();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getProveedoresLead() {
    this.proveedorLeadService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.proveedorLead = [];
      this.proveedorLead = data;
      this.dataSource = new MatTableDataSource(this.proveedorLead); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  verDatos() {
  }

  crearProveedoresLead() {
    this.dialogoService.open(ProveedoresLeadCreateComponent, {
      context: {
        idTipo: 1,
      },
    });
  }

  updateProveedorLead(idProveedoresLead) {
    this.dialogoService.open(ProveedoresLeadCreateComponent, {
      context: {
        idTipo: 2,
        idTipoProveedor: idProveedoresLead,
      },
    }).onClose.subscribe(x => {
      this.getProveedoresLead();
    });
  }

  bajaProveedorLead(idProveedoresLead) {
    {

      this.getProveedorLeadById(idProveedoresLead);
      swal({
        title: '¿Deseas dar de baja este proveedor??',
        text: 'Una vez dado de baja, no podrás verlo de nuevo.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          confirm: {
            text: 'Dar de baja',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.proveedoresLead['activo'] = 0;
          this.proveedorLeadService.put(idProveedoresLead, this.proveedoresLead).subscribe( result => {
            this.proveedorLeadService.postSocket({status: 'ok'}).subscribe(x => {});
          });
          swal('¡El proveedor se ha dado de baja exitosamente!', {
            icon: 'success',
          }).then((result => {
            this.getProveedoresLead();
          }));
        }
      });
    }
  }

  getProveedorLeadById(idProveedoresLead) {
    this.proveedorLeadService.getProveedorLeadById(idProveedoresLead).subscribe(data => {
      this.proveedoresLead = data;
    });
  }

  ngOnInit() {
    this.connect();
    this.getProveedoresLead();
    this.cols = ['nombre', 'url', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
