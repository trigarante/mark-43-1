import {Component, Input, OnInit} from '@angular/core';
import {ObservacionesInventario, ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {Monitor, MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {SedeData, Sede} from '../../../../../@core/data/interfaces/catalogos/sede';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';




@Component({
  selector: 'ngx-monitor-create',
  templateUrl: './monitor-create.component.html',
  styleUrls: ['./monitor-create.component.scss'],
})
export class MonitorCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idMonitor: number;
  monitor: Monitor;
  monitorCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ã‘ -\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];

  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<MonitorCreateComponent>, private monitorService: MonitorDate,
              private router: Router, private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private sedesService: SedeData,
              private tipoSedeService: TipoSedeData) { }

  guardarMonitor() {
    if (this.monitorCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.monitorService.post(this.monitorCreateForm.value).subscribe((result) => {
      this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
      this.monitorCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'monitor']);
    });
  }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  getObservacionInventario() {
  this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
    this.ObservacionesInventario = [];
    for (let i = 0; i < this.observaciones.length; i++) {
      this.ObservacionesInventario.push({
        'label': this.observaciones[i].observaciones,
        'value': this.observaciones[i].id,
      });
    }
  });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  getMonitorById() {
    this.monitorService.getMonitorById(this.idMonitor).subscribe( data => {
      this.monitor = data;
      this.monitorCreateForm.controls['numSerie'].setValue(this.monitor.numSerie);
      this.monitorCreateForm.controls['idMarca'].setValue(this.monitor.idMarca);
      this.monitorCreateForm.controls['dimensiones'].setValue(this.monitor.dimensiones);
      this.monitorCreateForm.controls['idEstadoInventario'].setValue(this.monitor.idEstadoInventario);
      this.monitorCreateForm.controls['idObservacionesInventario'].setValue(this.monitor.idObservacionesInventario);
      this.monitorCreateForm.controls['comentarios'].setValue(this.monitor.comentarios);
      this.monitorCreateForm.controls['idEmpleado'].setValue(this.monitor.idEmpleado);
      this.monitorCreateForm.controls['idTipoSede'].setValue(this.monitor.idTipoSede);
      this.monitorCreateForm.controls['origenRecurso'].setValue(this.monitor.origenRecurso);
    });
  }
  updateMonitor() {
    this.monitorService.put(this.idMonitor, this.monitorCreateForm.value).subscribe( result => {
      this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
      this.monitorCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'monitor']);
    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getMonitorById();
    }
    this.getMarca();
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getOrigen();
    this.getSedes();
    this.monitorCreateForm = this.fb.group({
      'numSerie': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'dimensiones': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede' : new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });
  }


}
