import {Observable} from 'rxjs';

export interface CampanaMarketing {
  id: number;
  nombre: string;
  descripcion: string;
  idSubarea: number;
  subarea: string;
  activo: number;
}

export abstract class CampanaMarketingData {
  abstract get(): Observable<CampanaMarketing[]>;

  abstract post(campanaMarketing): Observable<CampanaMarketing>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCampanasMarketingBiId(idCampanaMarketing): Observable<CampanaMarketing>;

  abstract put(idCampanaMarketing, campanaMarketing): Observable<CampanaMarketing>;
}
