
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
// import {Usuario} from './interfaces/ti/usuario';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from './interfaces/ti/user';
import {Global2} from './services/global2';
import {environment} from "../../../environments/environment";


let counter = 0;

@Injectable()
export class UserService {
  private baseURL;
  private users = {
    nick: { name: 'Nick Jones', picture: 'assets/images/nick.png' },
    eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
    jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
    lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
    alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
    kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
  };

  private userArray: any[];

  constructor(private http: HttpClient) {
    // this.userArray = Object.values(this.users);
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getUsers(): Observable<any> {
    return observableOf(this.users);
  }

  getCurrentUser(token): Observable<User> {
    const headerDict = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    };

    const requestOptions = {
      headers: new HttpHeaders(headerDict),
    };

    return this.http.get<User>(this.baseURL + 'user/me', requestOptions);
  }
  getCurrentUser2(token) {
    const headerDict = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token,
    };

    const requestOptions = {
      headers: new HttpHeaders(headerDict),
    };

    return this.http.get(this.baseURL + 'user/me', requestOptions);
  }
  getUserArray(): Observable<any[]> {
    return observableOf(this.userArray);
  }

  getUser(): Observable<any> {
    counter = (counter + 1) % this.userArray.length;
    return observableOf(this.userArray[counter]);
  }
}
