import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {SociosComponent} from '../../../comercial/socios/socios.component';
import {Router} from '@angular/router';
import {Prospecto, ProspectoData} from '../../../../@core/data/interfaces/ventaNueva/prospecto';
import {SelectItem} from 'primeng/api';
import {ProductoSolicitudCreateComponent} from '../producto-solicitud-create/producto-solicitud-create.component';
import {ProductoSolicitudData, ProductoSolicitudvn} from '../../../../@core/data/interfaces/ventaNueva/producto-solicitud';
import {map} from 'rxjs/operators';
import {SolicitudesVNData} from "../../../../@core/data/interfaces/ventaNueva/solicitudes";


@Component({
  selector: 'ngx-prospecto-create',
  templateUrl: './prospecto-create.component.html',
  // styleUrls: ['./prospecto-create.component.scss'],
})
export class ProspectoCreateComponent implements OnInit {
  @Input() idProspecto: number;
  submitted: Boolean;
  prospectoCreateForm: FormGroup;
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para un número. */
  numero: RegExp = /[0-9]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  genero: SelectItem[];
  /**Variable que muestra los controles al encontrar el correo*/
  mostrar: boolean;
  /**Variable que muestra si no existen Solicitudes*/
  productoExist: boolean;
  /**Variable que inicia los valores del microservicio*/
  prospecto: Prospecto[];
  /**Varibale que almacena el idProspecto si existe el correo electronico ingrresado*/
  idProspectoExis: any;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  productos: ProductoSolicitudvn[];
  producto: any;
  /**Variables que almacenan los valores del Producto*/
  cp: any;
  clave: any;
  marca: any;
  modelo: any;
  paquete: any;
  servicio: any;
  solicitud: any;
  descuento: any;
  movimiento: any;
  aseguradora: any;
  descripcion: any;
  /**Variable que muestra los derralles*/
  mostrarDetalle: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>, protected solicitudVentaService: SolicitudesVNData,
              private router: Router, protected prospectoService: ProspectoData, private _router: Router,
              private dialogoService: NbDialogService, private productoSolicitudService: ProductoSolicitudData) {
  }

  guardarProspecto() {
    this.submitted = true;
    this.mostrar = true;
    if (this.prospectoCreateForm.invalid) {
      return;
    }
    // let fechaCreacion: any;
    // fechaCreacion = Date.parse(this.prospectoCreateForm.controls['fechaCreacion'].value);
    this.prospectoService.post(this.prospectoCreateForm.value).subscribe((result) => {
      this.idProspectoExis = result.id;
      this.mostrar = true;
      this.productoExist = false;
      // this.ref.close();
    });
  }

  updateProspecto() {
    this.prospectoService.put(this.idProspectoExis, this.prospectoCreateForm.value).subscribe(x => {
      this.solicitudVentaService.postSocket(this.prospectoCreateForm.value).subscribe(x => {});
      this.createSolicitudProducto(this.idProspectoExis);
    });
  }

  getProspecto() {
    this.prospectoService.get().subscribe(data => {
      this.prospecto = data;
    });
  }

  createSolicitudProducto(idProspecto) {
    // this.dialogoService.open(ProductoSolicitudCreateComponent, {
    //   context: {
    //     idProspecto: idProspecto,
    //   },
    // });


  }

  validaCorreo(correo) {
    if (correo !== '' && this.mostrar !== true) {
      this.prospectoService.validaCorreo(correo).subscribe(
        data => {
          if (typeof data.correo === 'string' ) {
            this.mostrar = true;
            this.prospectoService.getProspectoById(data.id).subscribe(result => {
              this.prospectoCreateForm.controls['numero'].setValue(result.numero);
              this.prospectoCreateForm.controls['nombre'].setValue(result.nombre);
              this.prospectoCreateForm.controls['edad'].setValue(result.edad);
              this.prospectoCreateForm.controls['sexo'].setValue(result.sexo);
              // this.prospectoCreateForm.controls['fechaCreacion'].setValue(new Date(result.fechaCreacion));
              this.idProspectoExis = data.id;
              this.getProductoSolicitud(this.idProspectoExis);
            });
          } else {
            this.mostrar = false;
          }
        },
        error => {
          this.mostrar = false;
          let miError: any;
          miError = error;
        });
    }
  }

  getProductoSolicitud(idProspecto) {
    this.productoSolicitudService.get().pipe(map(data => {
      return data.filter(result => result.idProspecto === idProspecto);
    })).subscribe(data => {
      if (data.length === 0) { this.productoExist = false; }
      this.productos = data;
      this. producto = [];
      for (let i = 0; i < this.productos.length; i++) {
        if (this.productos[i].datos !== null) {
          this.productos[i].datos = JSON.parse(this.productos[i].datos);
          const c: any = this.productos[i].datos;
          this.producto.push({'labelo': c.marca + ' ' + c.descripcion + ' ' + c.modelo, 'valor': this.productos[i].id});
        }
      }
    });
  }

  getSolictud(idSolicitud) {
    this.productoSolicitudService.getProductoSolicitud(idSolicitud).subscribe(data => {
      if (data !== null) {
        this.solicitud = JSON.parse(data.datos);
        this.mostrarDetalle = true;
        this.cp = this.solicitud.cp;
        this.clave = this.solicitud.clave;
        this.marca = this.solicitud.marca;
        this.modelo = this.solicitud.modelo;
        this.paquete = this.solicitud.paquete;
        this.servicio = this.solicitud.servicio;
        this.descuento = this.solicitud.descuento;
        this.movimiento = this.solicitud.movimiento;
        this.aseguradora = this.solicitud.aseguradora;
        this.descripcion = this.solicitud.descripcion;
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  /** Función que inicializa el arreglo "genero" para usarlo en el Select. */
  getGenero() {
    this.genero = [];
    this.genero.push({label: 'MASCULINO', value: 'M'});
    this.genero.push({label: 'FEMENINO', value: 'F'});
  }

  ngOnInit() {
    this.getGenero();
    this.prospectoCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'numero': new FormControl('', Validators.required),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'sexo': new FormControl('', Validators.required),
      'edad': new FormControl('', Validators.required),
    });
  }

}
