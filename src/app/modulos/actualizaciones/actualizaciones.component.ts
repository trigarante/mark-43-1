import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-actualizaciones',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ActualizacionesComponent {
}
