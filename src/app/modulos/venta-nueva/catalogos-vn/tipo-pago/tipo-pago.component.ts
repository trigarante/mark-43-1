import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {TipoPagoCreateComponent} from '../modals/tipo-pago-create/tipo-pago-create.component';
import {TipoPagoData, TipoPagoVn} from '../../../../@core/data/interfaces/ventaNueva/catalogos/tipo-pago-vn';
import {TipoPago} from '../../../../@core/data/interfaces/catalogos/tipo-pago';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-pago',
  templateUrl: './tipo-pago.component.html',
  styleUrls: ['./tipo-pago.component.scss'],
})
export class TipoPagoComponent implements OnInit {

  cols: any[];


  descripciones: TipoPago[];

  // baja descripcion
  pago: TipoPagoVn;

  dataSource: any;
  // sockets
  private stompClient = null;

  constructor(private descService: TipoPagoData,
              private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vntipopago');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelVnTipoPago', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoPago(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  ngOnInit() {
    this.connect();
    this.getTipoPago(0);
    this.cols = ['cantidad', 'tipo', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  crearPago() {
    this.dialogoService.open(TipoPagoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => {
      this.getTipoPago(1);
    });
  }



  showToast() {
    this.toastrService.success('DATOS ACTUALIZADOS!', `Tipo Pago`);
  }

  // Obtener pagos1
  getTipoPago(mensajeAct) {
    this.descService.get().pipe(map(resultado => {
      return resultado.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.descripciones = [];
      this.descripciones = data;
      this.dataSource = new MatTableDataSource(this.descripciones); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }


  // Obtener ID de pago
  getTipoPagoById(idPago) {
    this.descService.getTipoPagoById(idPago).subscribe(data => {
      this.pago = data;
    });
  }

  // Actualizar información del pago
  updateTipoPago(idPago) {
    this.dialogoService.open(TipoPagoCreateComponent, {
      context: {
        idTipo: 2,
        idPago: idPago,
      },
    }).onClose.subscribe(x => {
      this.getTipoPago(1);
    });
  }

  // Dar de baja pago
  bajaPago(idPago) {
    this.getTipoPagoById(idPago);
    swal({
      title: '¿Deseas dar de baja este tipo pago?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.pago['activo'] = 0;
        this.descService.put(idPago, this.pago).subscribe(result => {
          this.descService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El tipo pago se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoPago(0);
        }));
      }
    });

  }
}
