import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NbAuthModule, NbDummyAuthStrategy} from '@nebular/auth';
import {NbSecurityModule, NbRoleProvider} from '@nebular/security';
import {of as observableOf} from 'rxjs';
import {throwIfAlreadyLoaded} from './module-import-guard';
import {DataModule} from './data/data.module';
import {AnalyticsService} from './utils/analytics.service';
import {SolicitudesData} from './data/interfaces/rrhh/solicitudes';
import {SolicitudesService} from './data/services/rrhh/solicitudes.service';
import {PrecandidatosData} from './data/interfaces/rrhh/precandidatos';
import {PrecandidatosService} from './data/services/rrhh/precandidatos.service';
import {BolsaTrabajoData} from './data/interfaces/catalogos/bolsa-trabajo';
import {BolsaTrabajoService} from './data/services/catalogos/bolsa-trabajo.service';
import {VacanteData} from './data/interfaces/catalogos/vacante';
import {VacantesService} from './data/services/catalogos/vacantes.service';
import {EstadoCivilData} from './data/interfaces/catalogos/estado-civil';
import {EstadoCivilService} from './data/services/catalogos/estado-civil.service';
import {PaisesData} from './data/interfaces/catalogos/paises';
import {PaisesService} from './data/services/catalogos/paises.service';
import {EscolaridadData} from './data/interfaces/catalogos/escolaridad';
import {EscolaridadesService} from './data/services/catalogos/escolaridades.service';
import {EstadoEscolaridadData} from './data/interfaces/catalogos/estado-escolaridad';
import {EstadoEscolaridadService} from './data/services/catalogos/estado-escolaridad.service';
import {SepomexData} from './data/interfaces/catalogos/sepomex';
import {SepomexService} from './data/services/catalogos/sepomex.service';
import {EmpleadosData} from './data/interfaces/rrhh/empleados';
import {EmpleadosService} from './data/services/rrhh/empleados.service';
import {MedioTransporteData} from './data/interfaces/catalogos/medio-transporte';
import {MedioTransporteService} from './data/services/catalogos/medio-transporte.service';
import {EstacionesLineasData} from './data/interfaces/catalogos/estaciones-lineas';
import {EstacionesLineasService} from './data/services/catalogos/estaciones-lineas.service';
import {CandidatosService} from './data/services/rrhh/candidatos.service';
import {CandidatoData} from './data/interfaces/rrhh/candidatos';
import {CapacitacionService} from './data/services/rrhh/capacitacion.service';
import {SeguimientoService} from './data/services/rrhh/seguimiento.service';
import {CapacitacionData} from './data/interfaces/rrhh/capacitacion';
import {CompetenciasService} from './data/services/catalogos/competencias.service';
import {CompetenciaData} from './data/interfaces/catalogos/competencia';
import {CompetenciasCandidatoService} from './data/services/catalogos/competencias-candidato.service';
import {CompetenciaCandidatoData} from './data/interfaces/catalogos/competencias-candidato';
import {CampanaData} from './data/interfaces/catalogos/campana';
import {CampanasService} from './data/services/catalogos/campanas.service';
import {SeguimientoData} from './data/interfaces/rrhh/seguimiento';
import {PuestoData} from './data/interfaces/catalogos/puesto';
import {PuestoService} from './data/services/catalogos/puesto.service';
import {PuestoTipoData} from './data/interfaces/catalogos/puesto-tipo';
import {PuestoTipoService} from './data/services/catalogos/puesto-tipo.service';
import {SubareaData} from './data/interfaces/catalogos/subarea';
import {SubareaService} from './data/services/catalogos/subarea.service';
import {BancoData} from './data/interfaces/catalogos/banco';
import {BancosService} from './data/services/catalogos/bancos.service';
import {EmpresaData} from './data/interfaces/catalogos/empresa';
import {EmpresasService} from './data/services/catalogos/empresas.service';
import {JwtResponseData} from './data/services/login/jwt-response';
import {LoginService} from './data/services/login/login.service';
import {CorreoService} from './data/services/ti/correo.service';
import {CorreoData} from './data/interfaces/ti/correo';
import {AreaService} from './data/services/catalogos/area.service';
import {AreaData} from './data/interfaces/catalogos/area';
import {EstadoRrhhData} from './data/interfaces/catalogos/estado-rrhh';
import {EstadoRrhhService} from './data/services/catalogos/estado-rrhh.service';
import {UsuarioData} from './data/interfaces/ti/usuario';
import {UsuarioService} from './data/services/ti/usuario.service';
import {CpuService} from './data/services/ti/soporte/inventario/cpu.service';
import {CpuData} from './data/interfaces/ti/soporte/inventario/cpu';
import {DiademaService} from './data/services/ti/soporte/inventario/diadema.service';
import {MonitorService} from './data/services/ti/soporte/inventario/monitor.service';
import {MouseService} from './data/services/ti/soporte/inventario/mouse.service';
import {TabletService} from './data/services/ti/soporte/inventario/tablet.service';
import {TecladoService} from './data/services/ti/soporte/inventario/teclado.service';
import {DiademaData} from './data/interfaces/ti/soporte/inventario/diadema';
import {MonitorDate} from './data/interfaces/ti/soporte/inventario/monitor';
import {MouseData} from './data/interfaces/ti/soporte/inventario/mouse';
import {TabletData} from './data/interfaces/ti/soporte/inventario/tablet';
import {TecladoData} from './data/interfaces/ti/soporte/inventario/teclado';
import {EstadoInventarioService} from './data/services/ti/soporte/catalogos/estado-inventario.service';
import {ObservacionesInventarioService} from './data/services/ti/soporte/catalogos/observaciones-inventario.service';
import {EstadoInventarioData} from './data/interfaces/ti/catalogos/estado-inventario';
import {ObservacionesInventarioData} from './data/interfaces/ti/catalogos/observaciones-inventario';
import {ExtensionService} from './data/services/ti/extension.service';
import {ExtensionData} from './data/interfaces/ti/extension';
import {InventarioData} from './data/interfaces/ti/soporte/inventario/inventario';
import {InventarioService} from './data/services/ti/soporte/inventario.service';
import {GrupoUsuariosService} from './data/services/ti/grupo-usuarios.service';
import {GrupoUsuariosData} from './data/interfaces/ti/grupoUsuarios';
import {SedeService} from './data/services/catalogos/sede.service';
import {SedeData} from './data/interfaces/catalogos/sede';
import {SociosService} from './data/services/comerciales/socios.service';
import {SociosData} from './data/interfaces/comerciales/socios-comercial';
import {ProductoSociosService} from './data/services/comerciales/producto-socios.service';
import {ProductoData} from './data/interfaces/comerciales/producto-socios';
import {EjecutivoCuentaService} from './data/services/comerciales/ejecutivo-cuenta.service';
import {EjecutivoCuentaData} from './data/interfaces/comerciales/ejecutivo-cuenta';
import {AsignarUrlService} from './data/services/comerciales/asignar-url.service';
import {AsignarUrlData} from './data/interfaces/comerciales/asignar-url';
import {ConveniosService} from './data/services/comerciales/convenios.service';
import {ConveniosData} from './data/interfaces/comerciales/convenios';
import {AnalisisCompetenciasService} from './data/services/comerciales/analisis-competencias.service';
import {AnalisisCompetenciaData} from './data/interfaces/comerciales/analisis-competencia';
import {TipoUsuarioData} from './data/interfaces/catalogos/tipo-usuario';
import {UsuarioTipoService} from './data/services/catalogos/usuario-tipo.service';
import {GrupoData} from './data/interfaces/catalogos/grupo';
import {GrupoService} from './data/services/catalogos/grupo.service';
import {EtapasModulosData} from './data/interfaces/catalogos/etapasModulos';
import {EtapasModulosService} from './data/services/catalogos/etapasModulos.service';
import {ContratosService} from './data/services/comerciales/contratos.service';
import {ContratosData} from './data/interfaces/comerciales/contratos';
import {PresupuestoService} from './data/services/comerciales/presupuesto.service';
import {PresupuestoData} from './data/interfaces/comerciales/presupuesto';
import {PreempleadosData} from './data/interfaces/rrhh/preempleados';
import {PreempleadosService} from './data/services/rrhh/preempleados.service';
import {TipoProductoService} from './data/services/comerciales/catalogo/tipo-producto.service';
import {TipoCompetenciaService} from './data/services/comerciales/catalogo/tipo-competencia.service';
import {TipoCategoriaService} from './data/services/comerciales/catalogo/tipo-categoria.service';
import {TipoProductoData} from './data/interfaces/comerciales/catalogo/tipo-producto';
import {TipoCompetenciaData} from './data/interfaces/comerciales/catalogo/tipo-cometencia';
import {TipoCategoriaData} from './data/interfaces/comerciales/catalogo/tipo-categoria';
import {LicenciasDisponiblesData} from './data/interfaces/ti/licencias-disponibles';
import {LicenciasDisponiblesService} from './data/services/ti/licencias-disponibles.service';
import {CoberturaService} from './data/services/comerciales/cobertura.service';
import {CoberturaData} from './data/interfaces/comerciales/cobertura';
import {TipoCoberturaService} from './data/services/comerciales/catalogo/tipo-cobertura.service';
import {TipoConvenioData} from './data/interfaces/comerciales/catalogo/tipo-convenio';
import {TipoCoberturaData} from './data/interfaces/comerciales/catalogo/tipo-cobertura';
import {CompetenciasSociosService} from './data/services/comerciales/competencias-socios.service';
import {CompetenciaSocioData} from './data/interfaces/comerciales/competencias-socios';
import {LaptopService} from './data/services/ti/soporte/inventario/laptop.service';
import {LaptopData} from './data/interfaces/ti/soporte/inventario/laptop';
import {SociosBajaService} from './data/services/comerciales/catalogo/socios-baja.service';
import {SociosBajaData} from './data/interfaces/comerciales/catalogo/socios-baja';
import {RazonSocialService} from './data/services/catalogos/razon-social.service';
import {RazonSociosData} from './data/interfaces/catalogos/razon-social';
import {CampanaMarketingService} from './data/services/marketing/campana-marketing.service';
import {CampanaMarketingData} from './data/interfaces/marketing/campana';
import {CampanaCategoriaService} from './data/services/marketing/campana-categoria.service';
import {CampanaCategoriaData} from './data/interfaces/marketing/campana-categoria';
import {PaginaService} from './data/services/marketing/pagina.service';
import {PaginasData} from './data/interfaces/marketing/pagina';
import {ProvedoresLeadService} from './data/services/catalogos/provedores-lead.service';
import {ProveedorLeadsData} from './data/interfaces/catalogos/proveedor-leads';
import {MediosDifusionService} from './data/services/marketing/medios-difusion.service';
import {MediosDifusionData} from './data/interfaces/marketing/medios-difusion';
import {ProcesadorService} from './data/services/ti/soporte/catalogos/procesador.service';
import {ProcesadorSoporteData} from './data/interfaces/ti/catalogos/procesador';
import {MarcaService} from './data/services/ti/soporte/catalogos/marca.service';
import {MarcaSoporteData} from './data/interfaces/ti/catalogos/marca';
import {SubCategoriaService} from './data/services/comerciales/catalogo/sub-categoria.service';
import {SubCategoriaData} from './data/interfaces/comerciales/catalogo/sub-categoria';
import {SolicitudesvnService} from './data/services/ventaNueva/solicitudes.service';
import {ProspectoService} from './data/services/ventaNueva/prospecto.service';
import {ProspectoData} from './data/interfaces/ventaNueva/prospecto';
import {SolicitudesVNData} from './data/interfaces/ventaNueva/solicitudes';
import {DivisasData} from './data/interfaces/comerciales/catalogo/divisas';
import {DivisasService} from './data/services/comerciales/catalogo/divisas.service';
import {ProductoSolicitudData} from './data/interfaces/ventaNueva/producto-solicitud';
import {ProductoSolicitudvnService} from './data/services/ventaNueva/producto-solicitudvn.service';
import {TipoContactoVnData} from './data/interfaces/catalogos/tipo-contacto-vn';
import {TipoContactoVnService} from './data/services/catalogos/tipo-contacto-vn.service';
import {TipoPaginaData} from './data/interfaces/catalogos/tipo-pagina';
import {TipoPaginaService} from './data/services/catalogos/tipo-pagina.service';
import {CatalogoData} from './data/interfaces/cotizador/catalogo';
import {CotizarData} from './data/interfaces/cotizador/cotizar';
import {CotizarService} from './data/services/cotizador/cotizar.service';
import {CatalogoService} from './data/services/cotizador/catalogo.service';
import {SwitchsData} from './data/interfaces/ti/soporte/inventario/switchs';
import {SwitchsService} from './data/services/ti/soporte/inventario/switchs.service';
import {ServidoresData} from './data/interfaces/ti/soporte/inventario/servidores';
import {ServidoresService} from './data/services/ti/soporte/inventario/servidores.service';
import {TarjetaRedData} from './data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TarjetaRedService} from './data/services/ti/soporte/inventario/tarjeta-red.service';
import {TelefonoIpData} from './data/interfaces/ti/soporte/inventario/telefono-ip';
import {TelefonoIpService} from './data/services/ti/soporte/inventario/telefono-ip.service';
import {RacksData} from './data/interfaces/ti/soporte/inventario/racks';
import {RacksService} from './data/services/ti/soporte/inventario/racks.service';
import {PatchPanelData} from './data/interfaces/ti/soporte/inventario/patch-panel';
import {PatchPanelService} from './data/services/ti/soporte/inventario/patch-panel.service';
import {CharolasRackData} from './data/interfaces/ti/soporte/inventario/charolas-rack';
import {CharolasRackService} from './data/services/ti/soporte/inventario/charolas-rack.service';
import {DiscoDuroInternoService} from './data/services/ti/soporte/inventario-soporte/disco-duro-interno.service';
import {DiscoDuroExternoData} from './data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {DiscoDuroExternoService} from './data/services/ti/soporte/inventario-soporte/disco-duro-externo.service';
import {ChromeBitService} from './data/services/ti/soporte/inventario-soporte/chrome-bit.service';
import {ApsService} from './data/services/ti/soporte/inventario-soporte/aps.service';
import {BiometricosService} from './data/services/ti/soporte/inventario-soporte/biometricos.service';
import {DvrService} from './data/services/ti/soporte/inventario-soporte/dvr.service';
import {CamarasService} from './data/services/ti/soporte/inventario-soporte/camaras.service';
import {BarrasMulticontactoService} from './data/services/ti/soporte/inventario-soporte/barras-multicontacto.service';
import {FuentaCamarasService} from './data/services/ti/soporte/inventario-soporte/fuenta-camaras.service';
import {DiscoDuroInternoData} from './data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBitData} from './data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {ApsData} from './data/interfaces/ti/soporte/inventario-soporte/aps';
import {BiometricosData} from './data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {DvrData} from './data/interfaces/ti/soporte/inventario-soporte/dvr';
import {CamarasData} from './data/interfaces/ti/soporte/inventario-soporte/camaras';
import {BarrasMulticontactoData} from './data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {FuenteCamarasData} from './data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';
import {CotizacionesData} from './data/interfaces/ventaNueva/cotizaciones';
import {CotizacionesService} from './data/services/ventaNueva/cotizaciones.service';
import {CotizacionesAliData} from './data/interfaces/ventaNueva/cotizaciones-ali';
import {CotizacionesAliService} from './data/services/ventaNueva/cotizaciones-ali.service';
import {SubramoData} from './data/interfaces/comerciales/sub-ramo';
import {SubRamoService} from './data/services/comerciales/sub-ramo.service';
import {TipoSubramoData} from './data/interfaces/comerciales/catalogo/tipo-subramo';
import {TipoSubramoService} from './data/services/comerciales/catalogo/tipo-subramo.service';
import {RamoData} from './data/interfaces/comerciales/ramo';
import {RamoService} from './data/services/comerciales/ramo.service';
import {TipoRamoData} from './data/interfaces/comerciales/catalogo/tipo-ramo';
import {TipoRamoService} from './data/services/comerciales/catalogo/tipo-ramo.service';
import {CategoriaData} from './data/interfaces/comerciales/catalogo/categoria';
import {CategoriaService} from './data/services/comerciales/catalogo/categoria.service';
import {MarcaEmpresarialService} from './data/services/catalogos/marca-empresarial.service';
import {MarcaEmpresarialData} from './data/interfaces/catalogos/marca-empresarial';
import {LlamadasService} from './data/services/ventaNueva/llamadas.service';
import {LlamadasData} from './data/interfaces/ventaNueva/llamadas';
import {HistorialLlamadasData} from './data/interfaces/ventaNueva/historial-llamadas';
import {HistorialLlamadasService} from './data/services/ventaNueva/historial-llamadas.service';
import {EstadoSolicitudService} from './data/services/ventaNueva/catalogos/estado-solicitud.service';
import {EstadoSolicitudData} from './data/interfaces/ventaNueva/catalogos/estado-solicitud';
import {ClientesData} from './data/interfaces/ventaNueva/cliente-vn';
import {ClienteVnService} from './data/services/ventaNueva/cliente-vn.service';
import {ProductoClienteService} from './data/services/ventaNueva/producto-cliente.service';
import {ProductoClienteData} from './data/interfaces/ventaNueva/producto-cliente';
import {DriveData} from './data/interfaces/ti/drive';
import {DriveService} from './data/services/ti/drive.service';
import {ProductoClienteAutoService} from './data/services/ventaNueva/producto-cliente-auto.service';
import {ProductoClienteAutoData} from './data/interfaces/ventaNueva/producto-cliente-auto';
import {EstadoPolizaVnService} from './data/services/ventaNueva/catalogos/estado-poliza-vn.service';
import {EstadoPolizaVnData} from './data/interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {TipoPagoVnService} from './data/services/ventaNueva/catalogos/tipo-pago-vn.service';
import {TipoPagoData} from './data/interfaces/ventaNueva/catalogos/tipo-pago-vn';
import {FlujoPolizaVnService} from './data/services/ventaNueva/catalogos/flujo-poliza-vn.service';
import {FlujoPolizaData} from './data/interfaces/ventaNueva/catalogos/flujo-poliza-vn';
import {RegistroService} from './data/services/ventaNueva/registro.service';
import {RegistrosData} from './data/interfaces/ventaNueva/registro';
import {PagosData} from './data/interfaces/ventaNueva/pagos';
import {PagosService} from './data/services/ventaNueva/pagos.service';
import {TurnoData} from './data/interfaces/catalogos/turno';
import {TurnosService} from './data/services/catalogos/turnos.service';
import {TurnoEmpleadoData} from './data/interfaces/catalogos/turno-empleado';
import {TurnoEmpleadoService} from './data/services/catalogos/turno-empleado.service';
import {FormaPagoData} from './data/interfaces/ventaNueva/catalogos/forma-pago';
import {FormaPagoService} from './data/services/ventaNueva/catalogos/forma-pago.service';
import {EstadoPagoData} from './data/interfaces/ventaNueva/catalogos/estado-pago';
import {EstadoPagoService} from './data/services/ventaNueva/catalogos/estado-pago.service';
import {EstadoRecibosData} from './data/interfaces/ventaNueva/catalogos/estado-recibos';
import {EstadoRecibosService} from './data/services/ventaNueva/catalogos/estado-recibos.service';
import {RecibosData} from './data/interfaces/ventaNueva/recibos';
import {RecibosService} from './data/services/ventaNueva/recibos.service';
import {InspeccionesData} from './data/interfaces/ventaNueva/inspecciones';
import {InspeccionesService} from './data/services/ventaNueva/inspecciones.service';
import {ConductorHabitualData} from './data/interfaces/ventaNueva/conductor-habitual';
import {ConductorHabitualService} from './data/services/ventaNueva/conductor-habitual.service';
import {EstadoInspeccionData} from './data/interfaces/ventaNueva/catalogos/estado-inspeccion';
import {EstadoInspeccionService} from './data/services/ventaNueva/catalogos/estado-inspeccion.service';
import {CarrierData} from './data/interfaces/ventaNueva/catalogos/carrier';
import {CarrierService} from './data/services/ventaNueva/catalogos/carrier.service';
import {BancoVnData} from './data/interfaces/ventaNueva/catalogos/banco-vn';
import {BancoVnService} from './data/services/ventaNueva/catalogos/banco-vn.service';
import {TarjetasVnData} from './data/interfaces/ventaNueva/catalogos/tarjetas-vn';
import {TarjetasVnService} from './data/services/ventaNueva/catalogos/tarjetas-vn.service';
import {TipoSedeService} from './data/services/ti/soporte/catalogos/tipo-sede.service';
import {TipoSedeData} from './data/interfaces/ti/catalogos/tipo-sede';


const socialLinks = [];

const DATA_SERVICES = [
  {provide: SolicitudesData, useClass: SolicitudesService},
  {provide: PrecandidatosData, useClass: PrecandidatosService},
  {provide: BolsaTrabajoData, useClass: BolsaTrabajoService},
  {provide: VacanteData, useClass: VacantesService},
  {provide: EstadoCivilData, useClass: EstadoCivilService},
  {provide: PaisesData, useClass: PaisesService},
  {provide: EscolaridadData, useClass: EscolaridadesService},
  {provide: EstadoEscolaridadData, useClass: EstadoEscolaridadService},
  {provide: SepomexData, useClass: SepomexService},
  {provide: EmpleadosData, useClass: EmpleadosService},
  {provide: PreempleadosData, useClass: PreempleadosService},
  {provide: CandidatoData, useClass: CandidatosService},
  {provide: CapacitacionData, useClass: CapacitacionService},
  {provide: MedioTransporteData, useClass: MedioTransporteService},
  {provide: EstacionesLineasData, useClass: EstacionesLineasService},
  {provide: CompetenciaData, useClass: CompetenciasService},
  {provide: CompetenciaCandidatoData, useClass: CompetenciasCandidatoService},
  {provide: CampanaData, useClass: CampanasService},
  {provide: SeguimientoData, useClass: SeguimientoService},
  {provide: PuestoData, useClass: PuestoService},
  {provide: PuestoTipoData, useClass: PuestoTipoService},
  {provide: SubareaData, useClass: SubareaService},
  {provide: BancoData, useClass: BancosService},
  {provide: EmpresaData, useClass: EmpresasService},
  {provide: JwtResponseData, useClass: LoginService},
  {provide: CorreoData, useClass: CorreoService},
  {provide: AreaData, useClass: AreaService},
  {provide: EstadoRrhhData, useClass: EstadoRrhhService},
  {provide: ExtensionData, useClass: ExtensionService},
  {provide: UsuarioData, useClass: UsuarioService},
  {provide: UsuarioService},
  {provide: CpuData, useClass: CpuService},
  {provide: DiademaData, useClass: DiademaService},
  {provide: MonitorDate, useClass: MonitorService},
  {provide: MouseData, useClass: MouseService},
  {provide: TabletData, useClass: TabletService},
  {provide: TecladoData, useClass: TecladoService},
  {provide: EstadoInventarioData, useClass: EstadoInventarioService},
  {provide: ObservacionesInventarioData, useClass: ObservacionesInventarioService},
  {provide: InventarioData, useClass: InventarioService},
  {provide: GrupoUsuariosData, useClass: GrupoUsuariosService},
  {provide: SedeData, useClass: SedeService},
  {provide: SociosData, useClass: SociosService},
  {provide: ProductoData, useClass: ProductoSociosService},
  {provide: EjecutivoCuentaData, useClass: EjecutivoCuentaService},
  {provide: AsignarUrlData, useClass: AsignarUrlService},
  {provide: ConveniosData, useClass: ConveniosService},
  {provide: AnalisisCompetenciaData, useClass: AnalisisCompetenciasService},
  {provide: GrupoData, useClass: GrupoService},
  {provide: TipoUsuarioData, useClass: UsuarioTipoService},
  {provide: EtapasModulosData, useClass: EtapasModulosService},
  {provide: ContratosData, useClass: ContratosService},
  {provide: PresupuestoData, useClass: PresupuestoService},
  {provide: TipoProductoData, useClass: TipoProductoService},
  {provide: TipoCompetenciaData, useClass: TipoCompetenciaService},
  {provide: TipoCategoriaData, useClass: TipoCategoriaService},
  {provide: LicenciasDisponiblesData, useClass: LicenciasDisponiblesService},
  {provide: CoberturaData, useClass: CoberturaService},
  {provide: TipoCoberturaData, useClass: TipoCoberturaService},
  {provide: CompetenciaSocioData, useClass: CompetenciasSociosService},
  {provide: LaptopData, useClass: LaptopService},
  {provide: SociosBajaData, useClass: SociosBajaService},
  {provide: RazonSociosData, useClass: RazonSocialService},
  {provide: CampanaMarketingData, useClass: CampanaMarketingService},
  {provide: CampanaCategoriaData, useClass: CampanaCategoriaService},
  {provide: PaginasData, useClass: PaginaService},
  {provide: MediosDifusionData, useClass: MediosDifusionService},
  {provide: ProcesadorSoporteData, useClass: ProcesadorService},
  {provide: MarcaSoporteData, useClass: MarcaService},
  {provide: SubCategoriaData, useClass: SubCategoriaService},
  {provide: DivisasData, useClass: DivisasService},
  {provide: SolicitudesVNData, useClass: SolicitudesvnService},
  {provide: ProspectoData, useClass: ProspectoService},
  {provide: ProductoSolicitudData, useClass: ProductoSolicitudvnService},
  {provide: TipoContactoVnData, useClass: TipoContactoVnService},
  {provide: TipoPaginaData, useClass: TipoPaginaService},
  {provide: ProveedorLeadsData, useClass: ProvedoresLeadService},
  {provide: CatalogoData, useClass: CatalogoService},
  {provide: CotizarData, useClass: CotizarService},
  {provide: DiscoDuroInternoData, useClass: DiscoDuroInternoService},
  {provide: DiscoDuroExternoData, useClass: DiscoDuroExternoService},
  {provide: ChromeBitData, useClass: ChromeBitService},
  {provide: ApsData, useClass: ApsService},
  {provide: BiometricosData, useClass: BiometricosService},
  {provide: DvrData, useClass: DvrService},
  {provide: CamarasData, useClass: CamarasService},
  {provide: BarrasMulticontactoData, useClass: BarrasMulticontactoService},
  {provide: FuenteCamarasData, useClass: FuentaCamarasService},
  {provide: SubramoData, useClass: SubRamoService},
  {provide: TipoSubramoData, useClass: TipoSubramoService},
  {provide: RamoData, useClass: RamoService},
  {provide: TipoRamoData, useClass: TipoRamoService},
  {provide: CategoriaData, useClass: CategoriaService},
  {provide: ServidoresData, useClass: ServidoresService},
  {provide: TarjetaRedData, useClass: TarjetaRedService},
  {provide: TelefonoIpData, useClass: TelefonoIpService},
  {provide: RacksData, useClass: RacksService},
  {provide: PatchPanelData, useClass: PatchPanelService},
  {provide: CharolasRackData, useClass: CharolasRackService},
  {provide: SwitchsData, useClass: SwitchsService},
  {provide: MarcaEmpresarialData, useClass: MarcaEmpresarialService},
  {provide: CotizacionesData, useClass: CotizacionesService},
  {provide: CotizacionesAliData, useClass: CotizacionesAliService},
  {provide: LlamadasData, useClass: LlamadasService},
  {provide: HistorialLlamadasData, useClass: HistorialLlamadasService},
  {provide: EstadoSolicitudData, useClass: EstadoSolicitudService},
  {provide: ClientesData, useClass: ClienteVnService},
  {provide: ProductoClienteData, useClass: ProductoClienteService},
  {provide: DriveData, useClass: DriveService},
  {provide: ProductoClienteAutoData, useClass: ProductoClienteAutoService},
  {provide: EstadoPolizaVnData, useClass: EstadoPolizaVnService},
  {provide: TipoPagoData, useClass: TipoPagoVnService},
  {provide: FlujoPolizaData, useClass: FlujoPolizaVnService},
  {provide: RegistrosData, useClass: RegistroService},
  {provide: TurnoData, useClass: TurnosService},
  {provide: TurnoEmpleadoData, useClass: TurnoEmpleadoService},
  {provide: PagosData, useClass: PagosService},
  {provide: FormaPagoData, useClass: FormaPagoService},
  {provide: EstadoPagoData, useClass: EstadoPagoService},
  {provide: EstadoRecibosData, useClass: EstadoRecibosService},
  {provide: RecibosData, useClass: RecibosService},
  {provide: InspeccionesData, useClass: InspeccionesService},
  {provide: ConductorHabitualData, useClass: ConductorHabitualService},
  {provide: EstadoInspeccionData, useClass: EstadoInspeccionService},
  {provide: CarrierData, useClass: CarrierService},
  {provide: BancoVnData, useClass: BancoVnService},
  {provide: TarjetasVnData, useClass: TarjetasVnService},
  {provide: TipoSedeData, useClass: TipoSedeService},
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...DataModule.forRoot().providers,
  ...DATA_SERVICES,
  ...NbAuthModule.forRoot({

    strategies: [
      NbDummyAuthStrategy.setup({
        name: 'email',
        delay: 3000,
      }),
    ],
    forms: {
      login: {
        socialLinks: socialLinks,
        rememberMe: false,
      },
      register: {
        socialLinks: socialLinks,
      },
    },
  }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,

  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  AnalyticsService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
}
