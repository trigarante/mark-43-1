import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactivarEmpleadoComponent } from './reactivar-empleado.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbAlertModule, NbCardModule, NbDateService, NbDialogRef, NbRadioModule, NbToastrService} from '@nebular/theme';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';

describe('ReactivarEmpleadoComponent', () => {
  let component: ReactivarEmpleadoComponent;
  let fixture: ComponentFixture<ReactivarEmpleadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactivarEmpleadoComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
      ],
      providers: [
        EmpleadosData,
        PrecandidatosData,
        NbToastrService,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactivarEmpleadoComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
