import { TestBed } from '@angular/core/testing';

import { TurnoEmpleadoService } from './turno-empleado.service';

describe('TurnoEmpleadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TurnoEmpleadoService = TestBed.get(TurnoEmpleadoService);
    expect(service).toBeTruthy();
  });
});
