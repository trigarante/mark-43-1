import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EstadoCivil, EstadoCivilData} from '../../../@core/data/interfaces/catalogos/estado-civil';
import {Paises, PaisesData} from '../../../@core/data/interfaces/catalogos/paises';
import {Escolaridad, EscolaridadData} from '../../../@core/data/interfaces/catalogos/escolaridad';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {Sepomex, SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';
import {PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {map} from 'rxjs/operators';
import {SelectItem} from 'primeng/api';
import {MedioTransporte, MedioTransporteData} from '../../../@core/data/interfaces/catalogos/medio-transporte';
import {EstacionesLineas, EstacionesLineasData} from '../../../@core/data/interfaces/catalogos/estaciones-lineas';
import {Solicitudes, SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {NbToastrService} from '@nebular/theme';
import {Dropdown} from 'primeng/primeng';
import {MatSelect, MatTableDataSource} from '@angular/material';
// import {EstadoRrhhData} from "../../../@core/data/interfaces/catalogos/estado-rrhh";
/**
 * Componente para crear un nuevo precandidato.
 */
@Component({
  selector: 'ngx-precandidato-create',
  templateUrl: './precandidato-create.component.html',

  styleUrls: ['./precandidato-create.component.scss'],
})
export class PrecandidatoCreateComponent implements OnInit {
  solicitudEliminada: boolean;
  /** Variable que permite la inicialización del formulario para crear un precandidato. */
  precandidatoForm: FormGroup;
  /** Variable para establecer el estado. */
  idEstado = 1;
  /** Variable para establecer la etapa en la que se encuentra el precandidato. */
  idEtapa = 1;
  /** Variable que almacena el id de la solicitud que fue seleccionada. */
  idSolicitud: number = this.rutaActiva.snapshot.params.idSolicitud;
  /** Arreglo que se inicializa con los valores del campo estado-civil desde el Microservicio. */
  estadoCivil: EstadoCivil[];
    /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
    paisOrigen: Paises[];
  /** Arreglo auxiliar para paisOrigen. */
  pais: any[];
  /** Arreglo que se inicializa con los valores del campo escolaridad desde el Microservicio. */
  escolaridades: Escolaridad[];
  /** Arreglo auxiliar para escolaridades. */
  gradoEscolar: any[];
  /** Arreglo que se inicializa con los valores del campo estado-escolaridad desde el Microservicio. */
  estadoEscolaridades: EstadoEscolaridad[];
  /** Arreglo auxiliar para estadoEscolaridades. */
  estadoEscoloaridad: any[];
  /** Arreglo auxiliar para coloniasSepomex */
  colonias: any[];
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  /** Bandera para conocer si el campo CURP contiene caracteres. */
  estadoCurp: boolean;
  /** Arreglo auxiliar para estadoCivil. */
  civil: any[];
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para letras y puntos. */
  puntoLetras: RegExp = /[\\A-Za-zñÑ. ]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Expresión regular para sólo números. */
  numero: RegExp = /[0-9]+/;
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders: SelectItem[];
  /** Variable de tipo "item seleccionado" para almacenar los tiempos de llegada. */
  tiempoLlegada: SelectItem[];
  /** Arreglo que se inicializa con los valores del campo medio-transporte desde el Microservicio. */
  medioTransporte: MedioTransporte[];
  /** Arreglo auxiliar para medioTransporte. */
  medioTransporteSelect: any[];
  /** Arreglo que se inicializa con los valores del campo estaciones-linead desde el Microservicio. */
  estacionesLineas: EstacionesLineas[];
  /** Arreglo auxiliar para las lineas del metro. */
  lineas = [{'label': '', 'value': ''}];
  /** Arreglo auxiliar para las estaciones de cada linea. */
  estaciones = [{'label': '', 'value': 0}];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Variable que almacena el tipoId de la ruta activa. */
  tipoPeticion: number;
  /** Variable que almacena la informaciónd de la solicitud seleccionada. */
  solicitud: Solicitudes;
  /** Variable que almacena el nombre del precandidato que se va a crear. */
  nombre: String;
  /** Variable que almacena el apellido paterno del precandidato que se va a crear. */
  apellidoPaterno: String;
  /** Variable que almacena el apellido materno del precandidato que se va a crear. */
  apellidoMaterno: String;
  /**Variable que muestra mensaje si el CP no es valido*/
  mostrar: boolean;
  /** Variable para referenciar al dropdown de las lineas del metro. */
  @ViewChild('lineaDropdown')
  lineaDropdown: MatSelect;
  /** Variable para referenciar al dropdown de las estaciones de la linea del metro seleccionada. */
  @ViewChild('estacionesDropdown')
  estacionesDropdown: Dropdown;
  /**Variable que regresa a valor default medio de traslado*/
  mTrasladoDef: any;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;

  precandidatos: any;
  /**
   * El método constructor manda a llamar Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {ActivatedRoute} rutaActiva Establece la ruta que se encuentra activa actualmente.
   * @param {EstadoCivilData} estadoCivilService Servicio del componente estado-civil para poder invocar los métodos.
   * @param {PaisesData} paisesService Servicio del componente paises para poder invocar los métodos.
   * @param {EscolaridadData} escolaridadesService Servicio del componente escolaridad para poder invocar los métodos.
   * @param {EstadoEscolaridadData} estadoEscolaridadService Servicio del componente estado-escolaridad para poder invocar los métodos.
   * @param {SepomexData} sepomexService Servicio del componente sepomex para poder invocar los métodos.
   * @param {PrecandidatosData} precandidatoService Servicio del componente precandidatos para poder invocar los métodos.
   * @param {MedioTransporteData} medioTransporteService Servicio del componente medio-transporte para poder invocar los métodos.
   * @param {EstacionesLineasData} estacionesLineasService Servicio del componente estaciones-lineas para poder invocar los métodos.
   * @param {Router} router Variable para navegar entre componentes.
   * @param {SolicitudesData} solicitudService Servicio del componente solicitudes para poder invocar los métodos.
   * @param {NbToastrService} toastrService Servicio por parte de la librería @nebular/theme para mostrar mensajes.  */
  constructor(private fb: FormBuilder, private rutaActiva: ActivatedRoute, protected estadoCivilService: EstadoCivilData,
              protected paisesService: PaisesData, protected  escolaridadesService: EscolaridadData,
              protected estadoEscolaridadService: EstadoEscolaridadData, protected sepomexService: SepomexData,
              protected precandidatoService: PrecandidatosData, protected medioTransporteService: MedioTransporteData,
              protected estacionesLineasService: EstacionesLineasData, private router: Router, private solicitudesService: SolicitudesData,
              protected solicitudService: SolicitudesData, private toastrService: NbToastrService,
              private precandidatosService: PrecandidatosData) {
    this.tipoPeticion = this.rutaActiva.snapshot.params.tipoId;
    this.precandidatoForm = this.fb.group({
      curp: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0,1][0-9][0-3][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]'),
        Validators.maxLength(18), Validators.minLength(18)])),
      'nombre': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'apellidoPaterno': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'apellidoMaterno': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'fechaNacimiento': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'genero': new FormControl('', Validators.required),
      'idEstadoCivil': new FormControl('', Validators.required),
      'idEscolaridad': new FormControl('', Validators.required),
      'idEstadoEscolaridad': new FormControl('', Validators.required),
      'idPais': new FormControl('', Validators.required),
      'cp':  new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5), Validators.minLength(4)])),
      'idColonia': new FormControl('', Validators.required),
      'calle': new FormControl('', Validators.required),
      'numeroInterior': new FormControl(''),
      'numeroExterior': new FormControl('', Validators.required),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.maxLength(10), Validators.minLength(10)])),
      'tiempoTraslado': new FormControl('', Validators.required),
      'idMedioTraslado': new FormControl('', Validators.required),
      'lineaMetro': new FormControl(''),
      'idEstacion': new FormControl(''),
      'idEstadoRH': this.idEstado,
      'idEtapa': this.idEtapa,
      'idSolicitudRRHH': null,
    });
  }
  getPrecandidatos(mensajeAct) {
    this.precandidatosService.get();
  }

  /** Función que obtiene los datos de la solicitud recibida. */
  getSolicitud() {
    this.solicitudService.getSolicitudById(this.rutaActiva.snapshot.params.idSolicitud)
      .pipe(map(data => {
        this.solicitud = data;
      })).subscribe(data => {
        this.nombre = this.solicitud.nombre.toUpperCase();
        this.precandidatoForm.controls['nombre'].setValue(this.nombre);
        this.apellidoPaterno = this.solicitud.apellidoPaterno.toUpperCase();
        this.precandidatoForm.controls['apellidoPaterno'].setValue(this.apellidoPaterno);
        this.apellidoMaterno = this.solicitud.apellidoMaterno.toUpperCase();
        this.precandidatoForm.controls['apellidoMaterno'].setValue(this.apellidoMaterno);
        this.precandidatoForm.controls['email'].setValue(this.solicitud.correo.toUpperCase());
        this.precandidatoForm.controls['telefonoMovil'].setValue(this.solicitud.telefono);
        this.precandidatoForm.controls['cp'].setValue(this.solicitud.cp);
        this.getColonias(this.solicitud.cp);
    });
  }

  /** Función para obtener el estado civil del precandidato y lo almacena en el arreglo "civil". */
  getEstadoCivil() {
    this.estadoCivilService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoCivil = result;
      this.civil = [];
      for (let i = 0; i < this.estadoCivil.length; i++) {
        this.civil.push({'label': this.estadoCivil[i].descripcion, 'value': this.estadoCivil[i].id});
      }
    });
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }

  /** Función que obtiene el grado escolar del precandidato y lo almacena en el arreglo "gradoEscolar". */
  getEscolaridades() {
    this.escolaridadesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.escolaridades = result;
      this.gradoEscolar = [];
      for (let i = 0; i < this.escolaridades.length; i++) {
        this.gradoEscolar.push({'label': this.escolaridades[i].nivel, 'value': this.escolaridades[i].id});
      }
    });
  }

  /** Función para obtener el estado de escolaridad del precandidato y lo almacena en el arreglo "estadoEscolaridad". */
  getEstadoEscolaridad() {
    this.estadoEscolaridadService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoEscolaridades = result;
      this.estadoEscoloaridad = [];
      for (let i = 0; i < this.estadoEscolaridades.length; i++) {
        this.estadoEscoloaridad.push({
          'label': this.estadoEscolaridades[i].estado,
          'value': this.estadoEscolaridades[i].id,
        });
      }
    });
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    this.sepomexService.getColoniaByCp(cp)
      .pipe(map(data => this.coloniasSepomex = data)).subscribe(data => {
      this.colonias = [];
      if (this.coloniasSepomex.length === 0) {
        this.mostrar = true;
      }else {
        this.mostrar = false;
      }
      for (let i = 0; i < this.coloniasSepomex.length; i++) {
        this.colonias.push({'label': this.coloniasSepomex[i].asenta, 'value': this.coloniasSepomex[i].idCp});
      }
    });

  }

  /** Función que permite la validación del CURP introducido. */
  validarCurp(curp) {
    if (curp.length < 18) {
      this.estadoCurp = true;
    }else if (!this.precandidatoForm.controls['curp'].valid) {
      this.estadoCurp = true;
    } else {
      this.precandidatoService.validarCurp(curp).subscribe(el => el,
        err => this.estadoCurp = false ,
        () => this.showToast('danger'));

    }
  }
  /**validarCurp(curp) {
    if (curp.length === 18) {
      this.estadoCurp = false;
      this.precandidatoService.validarCurp(curp).subscribe(response => {
        this.estadoCurp = response;
        if (this.estadoCurp === true) {
          this.showToast('danger');
        }
      });

    }
  }*/

  /** Función que inicializa el arreglo "genero" para usarlo en el Select. */
  genero() {
    this.genders = [];
    this.genders.push({label: 'MASCULINO', value: 'M'});
    this.genders.push({label: 'FEMENINO', value: 'F'});
  }

  /** Función que inicializa el arreglo "tiempoTraslado" para usarlo en el Select. */
  tiempoTraslado() {
    this.tiempoLlegada = [];
    this.tiempoLlegada.push({label: '0 - 15 min', value: 15});
    this.tiempoLlegada.push({label: '15 - 30 min', value: 30});
    this.tiempoLlegada.push({label: '30 - 45 min', value: 45});
    this.tiempoLlegada.push({label: '45 - 60 min', value: 60});
    this.tiempoLlegada.push({label: '60 - 1:15 hrs', value: 75});
    this.tiempoLlegada.push({label: '1:15 - 1:30 hrs', value: 90});
    this.tiempoLlegada.push({label: '1:30 - 1:45 hrs', value: 105});
    this.tiempoLlegada.push({label: '1:45- 2:00 hrs', value: 120});
  }

  /** Función para obtener la información acerca del medio de transporte que utiliza el empleado para llegar a la empresa
   * y lo almacena en el arreglo "medioTransporteSelect". */
  getMedioTransporte() {
    this.medioTransporteService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.medioTransporte = result;
      this.medioTransporteSelect = [];
      for (let i = 0; i < this.medioTransporte.length; i++) {
        this.medioTransporteSelect.push({'label': this.medioTransporte[i].medio, 'value': this.medioTransporte[i].id});
      }
    });
  }

  /** Función que obtiene la información acerca de las líneas del metro. */
  getLineas(idMedioTraslado) {
    this.estacionesLineasService.getByIdMedio(idMedioTraslado).subscribe(data => {
      this.lineas = [];
      this.estacionesLineas = data; /**
       if (idMedioTraslado === 1 || idMedioTraslado === 2) { // Si el medio seleccionado es metro o metrobus
        this.lineaDropdown.value = 'Selecciona una línea';  // Reseteando el dropdown de linea del metro
        this.estacionesDropdown.value = 'Seleccione una estación'; // Reseteando el dropdown de estaciones
        this.estaciones = [];
      }*/
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.lineas.push({'label': this.estacionesLineas[i].lineas, 'value': this.estacionesLineas[i].lineas});
      }
    });
    this.precandidatoForm.controls['lineaMetro'].setValue('');
    this.precandidatoForm.controls['idEstacion'].setValue('');
    this.estaciones = [];
  }

  /** Función que obtiene las estaciones de la línea del metro seleccionada. */
  getEstaciones(lineaMetro) {
    this.estacionesLineasService.getEstacionByIdLinea(this.precandidatoForm.value.idMedioTraslado, lineaMetro).subscribe(data => {
      this.estaciones = [];
      this.estacionesLineas = data;
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.estaciones.push({'label': this.estacionesLineas[i].estacion, 'value': this.estacionesLineas[i].id});
      }
    });
  }

  /** Esta función permite guardar toda la información vaciada en los campos de texto del formulario.
   * Toda esa información se envía mediante una petición POST al Microservicio. */
  guardarPrecandidato() {
    this.inactivo = true;
    this.submitted = true;
    this.precandidatoForm.value.idSolicitudRRHH = this.idSolicitud;
    if (this.precandidatoForm.invalid) {
      return;
    }
    if (this.precandidatoForm.controls['idEstacion'].value === '') {
      this.precandidatoForm.controls['idEstacion'].setValue( null);
      this.precandidatoForm.value.idSolicitudRRHH = Number(this.idSolicitud);
      let telefonoFijo;
      telefonoFijo = this.precandidatoForm.controls['telefonoFijo'].value;
      if (this.precandidatoForm.value.telefonoFijo !== '') {
        this.precandidatoForm.value.telefonoFijo = Number(telefonoFijo);
      }
      let telefonoMovil;
      telefonoMovil = this.precandidatoForm.controls['telefonoMovil'].value;
      this.precandidatoForm.value.telefonoMovil = Number(telefonoMovil);
      let cp;
      cp = this.precandidatoForm.controls['cp'].value;
      this.precandidatoForm.value.cp = Number(cp);
    }
    let fechaNacimiento: any;
    fechaNacimiento = Date.parse(this.precandidatoForm.value.fechaNacimiento);
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe(data => {
      if (Number(data.estado) === 1 && data.idEtapa === 0) {
        this.precandidatoService.post(this.precandidatoForm.value, this.precandidatoForm.value.idSolicitudRRHH, fechaNacimiento)
          .subscribe(
            (result) => {
            },
            error => { },
            () => {
              this.precandidatoService.postSocket(this.precandidatoForm.value).subscribe(d => {});
              this.solicitudesService.postSocket(this.precandidatoForm.value).subscribe(d => {});
              this.router.navigate(['/modulos/rrhh/precandidatos']);
            },
          );
      } else {
        this.solicitudEliminada = true;
      }
    });
    this.getPrecandidatos(0);
  }

  /** Función que verifica que todos los campos del formulario estén completos. */
  verificaFormulario() {
    if (this.precandidatoForm.valid === true) {
        if (this.precandidatoForm.controls['idMedioTraslado'].value === 1 ||
          this.precandidatoForm.controls['idMedioTraslado'].value === 2) {
          if (this.precandidatoForm.controls['lineaMetro'].value !== '' &&
            this.precandidatoForm.controls['idEstacion'].value !== '') {
            return false; // BOTÓN HABILITADO
          } else {
            return true;  // BOTÓN DESHABILITADO
          }
        } else {
          return false;   // BOTÓN HABILITADO
        }
    } else {
      return true;        // BOTÓN DESHABILITADO
    }
  }

  /** Método para mostrar un mensaje en pantalla, en este caso, un mensaje de advertencia. */
  showToast(status) {
    this.toastrService.danger('Atencion', `Curp ya se encuentra registrado`);
  }

  /** Función que manda a llamar a todos los demás métodos al inicializar el componente. */
  ngOnInit() {
    this.inactivo = false;
    if (this.estadoCurp === false) {
      this.showToast('danger');
    }
    this.genero();
    this.getEstadoCivil();
    this.getPaises();
    this.getEscolaridades();
    this.getEstadoEscolaridad();
    this.tiempoTraslado();
    this.getMedioTransporte();
    this.getSolicitud();
  }

}
