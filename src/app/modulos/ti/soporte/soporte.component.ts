import { Component } from '@angular/core';

@Component({
  selector: 'ngx-soporte',
  template: '<router-outlet></router-outlet>',
})
export class SoporteComponent {

}
