import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SociosComercial, SociosData} from '../../interfaces/comerciales/socios-comercial';

@Injectable()
export class SociosService extends SociosData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<SociosComercial[]> {
    return this.http.get<SociosComercial[]>(this.baseURL + 'v1/socios');
  }

  post(socios: SociosComercial): Observable<SociosComercial> {
    return this.http.post<SociosComercial>(this.baseURL + 'v1/socios', socios);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'socios/saveSocios', json);
  }

  getSociosById(idSocios): Observable<SociosComercial> {
    return this.http.get<SociosComercial>(this.baseURL + 'v1/socios/' + idSocios);
  }

  getIdByAlias(alias): Observable<SociosComercial> {
    return this.http.get<SociosComercial>(this.baseURL + 'v1/socios/alias/' + alias);
  }

  put(idSocios, socios: SociosComercial): Observable<SociosComercial> {
    return this.http.put<SociosComercial>(this.baseURL + 'v1/socios/' + idSocios + '/'
      + sessionStorage.getItem('Empleado'), socios);
  }
}
