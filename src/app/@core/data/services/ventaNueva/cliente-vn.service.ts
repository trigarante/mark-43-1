import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ClientesData, ClienteVn} from '../../interfaces/ventaNueva/cliente-vn';

@Injectable()
export class ClienteVnService extends ClientesData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<ClienteVn[]> {
    return this.http.get<ClienteVn[]>(this.baseURL + 'v1/cliente');
  }

  post(cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.post<ClienteVn>(this.baseURL + 'v1/cliente', cliente);
  }

  getClienteById(idCliente): Observable<ClienteVn> {
    return this.http.get<ClienteVn>(this.baseURL + 'v1/cliente/' + idCliente);
  }

  curpExist(curp) {
    return this.http.get(this.baseURL + 'v1/cliente/curp/' + curp);
  }

  rfcExist(rfc) {
    return this.http.get(this.baseURL + 'v1/cliente/rfc/' + rfc);
  }

  put(idCliente, cliente: ClienteVn): Observable<ClienteVn> {
    return this.http.put<ClienteVn>(this.baseURL + 'v1/cliente/' + idCliente, cliente);
  }
}
