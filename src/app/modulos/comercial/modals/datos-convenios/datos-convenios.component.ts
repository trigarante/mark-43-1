import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Convenios, ConveniosData} from '../../../../@core/data/interfaces/comerciales/convenios';
import {AsignarConveniosComponent} from '../asignar-convenios/asignar-convenios.component';
import swal from 'sweetalert';

@Component({
  selector: 'ngx-datos-convenios',
  templateUrl: './datos-convenios.component.html',
  styleUrls: ['./datos-convenios.component.scss'],
})
export class DatosConveniosComponent implements OnInit {
  @Input() idConvenio: number;
  @Input() idCategoria: number;
  estado: any;
  alias: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoConvenio: any;
  tipoGiro: any;
  cantidad: number;
  convenio: Convenios;
  idSubRamo: number;
  permisos: any;
  escritura: boolean;

  constructor(protected ref: NbDialogRef<DatosConveniosComponent>, private convenioService: ConveniosData,
              private conveniosService: ConveniosData, private dialogService: NbDialogService ) { }

  ngOnInit() {
    this.getConvenio();
    this.getPermisos();
  }

  getConvenio() {
    this.convenioService.getConveniosById(this.idConvenio).subscribe(
      result => {
        this.estado = result.estado;
        this.alias = result.alias;
        this.nombreComercial = result.nombreComercial;
        this.tipoCategoria = result.tipoSubRamo;
        this.tipoConvenio = result.detalle;
        this.tipoGiro = result.tipoRamo;
        this.cantidad = result.cantidad;
        this.idSubRamo = result.idSubRamo;
      },
    );
  }

  updateConvenios() {
    this.dialogService.open(AsignarConveniosComponent, {
      context: {
        idTipo: 2,
        idConvenio: this.idConvenio,
      },
    });
  }
  bajaConvenios() {
    this.conveniosService.getConveniosById(this.idConvenio).subscribe(data => this.convenio = data);
    swal({
      title: '¿Deseas dar de baja este Convenio?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.convenio.activo = 0;
        this.conveniosService.put(this.idConvenio, this.convenio).subscribe( result => {
        });
        swal('¡El Convenio se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          /**this.getConvenios(0);*/
        }));
      }
    });
  }

  dismiss() {
    this.ref.close();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.convenios.escritura;
  }



}
