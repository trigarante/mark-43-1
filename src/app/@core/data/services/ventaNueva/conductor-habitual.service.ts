import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ConductorHabitual, ConductorHabitualData} from '../../interfaces/ventaNueva/conductor-habitual';

@Injectable()
export class ConductorHabitualService extends ConductorHabitualData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<ConductorHabitual[]> {
    return this.http.get<ConductorHabitual[]>(this.baseURL + 'v1/conductor-habitual');
  }

  post(Conductor: ConductorHabitual): Observable<ConductorHabitual> {
    return this.http.post<ConductorHabitual>(this.baseURL + 'v1/conductor-habitual/', Conductor);
  }

  getConductorById(idConductor): Observable<ConductorHabitual> {
    return this.http.get<ConductorHabitual>(this.baseURL + 'v1/conductor-habitual/' + idConductor);
  }

  put(idConductor, Conductor: ConductorHabitual): Observable<ConductorHabitual> {
    return this.http.put<ConductorHabitual>(this.baseURL + 'v1/conductor-habitual/' + idConductor, Conductor);
  }
}
