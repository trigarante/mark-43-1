import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CatalogosSoporteComponent} from './catalogos-soporte.component';
import {EstadoInventarioComponent} from './estado-inventario/estado-inventario.component';
import {ObservacionesInventarioComponent} from './observaciones-inventario/observaciones-inventario.component';
import {MarcaComponent} from './marca/marca.component';
import {ProcesadorComponent} from './procesador/procesador.component';
import {AuthGuard} from '../../../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: CatalogosSoporteComponent,
  children: [
    {
      path: 'estado-inventario',
      component: EstadoInventarioComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.estadoInventario.activo},
    },
    {
      path: 'observaciones-inventario',
      component: ObservacionesInventarioComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.observacionesInventario.activo},
    },
    {
      path: 'marca',
      component: MarcaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.marcas.activo},
    },
    {
      path: 'procesadores',
      component: ProcesadorComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.procesadores.activo},
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogosSoporteRoutingModule {
}
