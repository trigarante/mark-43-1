import { Observable } from 'rxjs';

export interface ProductoSolicitudvn {
  id: number;
  tipo: string;
  datos: string;
  nombre: string;
  correo: string;
  numero: string;
  genero: string;
  edad: string;
  cp: string;
  clave: string;
  marca: string;
  modelo: string;
  paquete: string;
  servicio: string;
  descuento: number;
  movimiento: string;
  aseguradora: string;
  idProspecto: number;
  descripcion: string;
  numeroMotor: string;
  numeroPlacas: string;
  fechaNaimiento: string;
  idTipoCategoria: number;
}

export abstract class ProductoSolicitudData {
  abstract get(): Observable<ProductoSolicitudvn[]>;
  abstract getDatos(): Observable<ProductoSolicitudvn[]>;
  abstract post(nombre): Observable<ProductoSolicitudvn>;
  abstract getProductoSolicitud(id): Observable<ProductoSolicitudvn>;
  abstract getProductoSolicitudDatos(id): Observable<ProductoSolicitudvn>;
  abstract put(id, nombre): Observable<ProductoSolicitudvn>;
}
