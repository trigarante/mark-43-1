import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Vacante, VacanteData} from '../../interfaces/catalogos/vacante';

@Injectable()
export class VacantesService extends VacanteData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Vacante[]> {
    return this.http.get<Vacante[]>(this.baseURL + 'v1/vacantes');
  }

  post(vacante: Vacante): Observable<Vacante> {
    return this.http.post<Vacante>(this.baseURL + 'v1/vacantes', vacante);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vacantes/saveVacante/1', json);
  }

  getVacantesById(idVacante): Observable<Vacante> {
    return this.http.get<Vacante>(this.baseURL + 'v1/vacantes/' + idVacante);
  }

  put(idVacante, vacante: Vacante) {
    return this.http.put<Vacante>(this.baseURL + 'v1/vacantes/' + idVacante + '/'
      + sessionStorage.getItem('Empleado'), vacante);
  }
  putBaja(idVacante) {
    return this.http.put<Vacante>(this.baseURL + 'v1/vacantes/baja-vacante/' + idVacante + '/'
      + sessionStorage.getItem('Empleado'), idVacante);
  }
}
