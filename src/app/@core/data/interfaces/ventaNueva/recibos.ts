import {Observable} from 'rxjs';


export interface Recibos {
  id: number;
  idRegistro: number;
  idEmpleado: number;
  idEstadoRecibos: number;
  fechaVigencia: Date;
  fechaPago: Date;
  numero: number;
  cantidad: number;
}

export abstract class RecibosData {

  abstract get(): Observable<Recibos[]>;

  abstract post(Recibo): Observable<Recibos>;

  abstract getReciboById(idRecibo): Observable<Recibos>;

  abstract put(idRecibo, Recibo): Observable<Recibos>;

}
