import {Observable} from 'rxjs';

export interface Competencia {
  id: number;
  escala: number;
  estado: string;
  descripcion: string;
  activo: 1;
}

export abstract class CompetenciaData {
  abstract get(): Observable<Competencia[]>;

  abstract post(competencia: Competencia): Observable<Competencia>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCompetenciasById(idCompetencias): Observable<Competencia>;

  abstract put(idCompetencias, competencia): Observable<Competencia>;
}
