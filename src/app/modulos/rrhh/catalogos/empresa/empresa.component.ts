import {Component, OnInit, ViewChild} from '@angular/core';
import {Empresa, EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {NbDialogService} from '@nebular/theme';
import {EmpresaCreateComponent} from '../modals/empresa-create/empresa-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {BancoCreateComponent} from '../modals/banco-create/banco-create.component';

/**
 * Componente de empresa (catálogo).
 */
@Component({
  selector: 'ngx-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss'],
})
export class EmpresaComponent implements OnInit {
  cols: any[];
  empresas: Empresa[];
  empresa: Empresa;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  /**
   * @param {EmpresaData} empresaService Servicio del componente empresa para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private empresaService: EmpresaData, private dialogoService: NbDialogService) {}
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene las empresas y se las asigna al arreglo empresas. */
  getEmpresa() {
    this.empresaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.empresas = [];
      this.empresas = data;
      this.dataSource = new MatTableDataSource(this.empresas); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  /** Función que obtiene la información de la empresa dependiendo de su ID. */
  getEmpresaById(idEmpresa) {
    this.empresaService.getEmpresasById(idEmpresa).subscribe(data => {
      this.empresa = data;
    });
  }

  /** Función que despliega un modal del componente "EmpresaCreateComponent" para poder "crear empresa". */
  crearEmpresa() {
    this.dialogoService.open(EmpresaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEmpresa();
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de una empresa. */
  updateBanco(idEmpresa) {
    this.dialogoService.open(EmpresaCreateComponent, {
      context: {
        idTipo: 2,
        idEmpresa: idEmpresa,
      },
    }).onClose.subscribe(x => {
      this.getEmpresa();
    });
  }

  /** Función que despliega un modal para dar de baja una empresa. */
  bajaEmpresa(idEmpresa) {
    this.getEmpresaById(idEmpresa);
    swal({
      title: '¿Deseas dar de baja esta empresa?',
      text: 'Una vez dada de baja, no podrás ver la empresa de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.empresa['activo'] = 0;
        this.empresaService.put(idEmpresa, this.empresa).subscribe( result => {
        });
        swal('¡La empresa se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getEmpresa();
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.catalogos.escritura;
    console.log(this.escritura);
  }

  ngOnInit() {
    this.getEmpresa();
    this.cols = ['nombre', 'descripcion', 'acciones'];
    this.getPermisos();
  }
}
