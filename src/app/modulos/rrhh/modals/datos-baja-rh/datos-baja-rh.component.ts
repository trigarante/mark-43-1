import {Component, Input, OnInit} from '@angular/core';
import {Precandidatos, PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import * as moment from 'moment';
import {NbDialogRef} from '@nebular/theme';
import {BajasrhComponent} from '../../bajasrh/bajasrh.component';
@Component({
  selector: 'ngx-datos-baja-rh',
  templateUrl: './datos-baja-rh.component.html',
  styleUrls: ['./datos-baja-rh.component.scss'],
})
export class DatosBajaRhComponent implements OnInit {
  @Input() idPrecandidato: number;
  nombre: string;
  fechaBaja: string;
  detalleBaja: string;
  etapa: string;
  sede: string;
  subArea: string;
  constructor(protected precandidatoService: PrecandidatosData,
              protected ref: NbDialogRef<BajasrhComponent>,
              ) { }

  getPrecandidatoById() {
    this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      this.nombre = data.nombre + ' ' + data.apellidoMaterno + ' ' + data.apellidoPaterno;
      this.etapa = data.nombreEtapa;
      this.sede = data.sede;
      this.subArea = data.subarea;
      this.fechaBaja = moment(data.fechaBaja).format('YYYY-MM-DD') ;
      this.detalleBaja = data.detalleBaja;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getPrecandidatoById();
  }

}
