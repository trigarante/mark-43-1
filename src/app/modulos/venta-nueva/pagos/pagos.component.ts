import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PagosData} from '../../../@core/data/interfaces/ventaNueva/pagos';
import {FormaPago} from '../../../@core/data/interfaces/ventaNueva/catalogos/forma-pago';
import {FormaPagoService} from '../../../@core/data/services/ventaNueva/catalogos/forma-pago.service';
import {EstadoPagoService} from '../../../@core/data/services/ventaNueva/catalogos/estado-pago.service';
import {EstadoPago} from '../../../@core/data/interfaces/ventaNueva/catalogos/estado-pago';
import {filter, flatMap, map, reduce} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Carrier, CarrierData} from '../../../@core/data/interfaces/ventaNueva/catalogos/carrier';
import {TarjetasVn, TarjetasVnData} from '../../../@core/data/interfaces/ventaNueva/catalogos/tarjetas-vn';
import 'rxjs-compat/add/operator/mergeMap';
import {BancoVnData} from '../../../@core/data/interfaces/ventaNueva/catalogos/banco-vn';
import {DriveData} from '../../../@core/data/interfaces/ti/drive';
import Swal from 'sweetalert2';


@Component({
  selector: 'ngx-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss'],
})
export class PagosComponent implements OnInit {

  formRaw: any;
  dataSource: any;
  formaPagoData: FormaPago[] = [];
  estadoPagoData: EstadoPago[] = [];
  cols: any[];
  selectionformaPago: number;
  bancosArray: any;
  carrierArray: Carrier[];
  tarjetasArray: TarjetasVn[] = [];

  // FORMS
  formPagos: FormGroup;

  estatusFinanzas: any = [
    {nombre: 'Aplicado', id: 1},
    {nombre: 'No Aplicado', id: 2},
  ];

  estatusPago: any = [
    {nombre: 'Emitido', id: 1},
    {nombre: 'Cobrado', id: 2},
  ];

  mesesArray: any = [
    {nombre: 'Contado', id: 1},
    {nombre: '3 Meses', id: 2},
    {nombre: '6 Meses', id: 3},
    {nombre: '9 Meses', id: 4},
    {nombre: '12 Meses', id: 5},
  ];
  extensionesDeArchivoAceptadas = ['application/pdf', 'image/jpeg', 'image/png'];

  @ViewChild('fechaV') fechaV: ElementRef;
  numeros: RegExp = /[0-9]+/;
  letras: RegExp = /[\\A-Za-zñÑ ]+/;


  constructor(private pagosService: PagosData, private dc: ChangeDetectorRef,
              private formaPagoService: FormaPagoService,
              private estadoPagoService: EstadoPagoService,
              private fb: FormBuilder,
              private carrierService: CarrierData,
              private bancoService: BancoVnData,
              private tarjetasService: TarjetasVnData,
              private llamadasService: DriveData) {}

  ngOnInit() {
    const pagos = {
      idRecibo: null,
      idFormaPago: null,
      datos: this.fb.group({
        domiciliacion: this.fb.group({
          banco: '',
          primaNeta: '',
          estatusFinanzas: '',
          estatusPago: '',
        }),
        tarjetaCreditoAhorra: this.fb.group({
          estatusFinanzas: '',
          primaNeta: '',
          estatusPago: '',
          numeroTarjeta: '',
          titular: '',
          carrier: '',
          bancoTarjeta: '',
          tipoTarjeta: '',
          mesesIntereses: '',
          codigo: '',
          fechaVencimiento: '',
          notas: '',
          comentarios: '',
          notasFinanzas: '',
          registrarPoliza: '',
        }),
        tarjetaCreditoAseguradora: this.fb.group({
          estatusFinanzas: '',
          primaNeta: '',
          estatusPago: '',
          numeroTarjeta: '',
          titular: '',
          carrier: '',
          bancoTarjeta: '',
          tipoTarjeta: '',
          mesesIntereses: '',
          codigo: '',
          fechaVencimiento: '',
          notas: '',
          comentarios: '',
          notasFinanzas: '',
          registrarPoliza: '',
        }),
        tarjetaDebito: this.fb.group({
          estatusFinanzas: '',
          primaNeta: '',
          estatusPago: '',
          numeroTarjeta: '',
          titular: '',
          carrier: '',
          bancoTarjeta: '',
          tipoTarjeta: '',
          mesesIntereses: '',
          codigo: '',
          fechaVencimiento: '',
          notas: '',
          comentarios: '',
          notasFinanzas: '',
          registrarPoliza: '',
        }),
        transferencia: this.fb.group({
          banco: '',
          primaNeta: '',
          estatusFinanzas: '',
          estatusPago: '',
          noAutorizacion: '',
        }),
        depositoBancario: this.fb.group({
          comentarios: '',
        }),
      }),
      idestadoPago: null,
      fechaPago: null,
      cantidad: '',
    };
    this.formPagos = this.fb.group(pagos);

    const controlsP = this.formPagos.controls;
    controlsP.idestadoPago.setValidators(Validators.required);
    controlsP.fechaPago.setValidators(Validators.required);
    controlsP.cantidad.setValidators(Validators.required);
    controlsP.idFormaPago.setValidators(Validators.required);


    this.getFormaPago();
    this.getEstadoPago();
    this.getBancos();
  }

  getBancos() {
    this.bancoService.get().pipe(
    ).subscribe(arr => {
      this.bancosArray = arr;
    });
  }


  getFormaPago() {
    this.formaPagoService.get().pipe(map((res) => {
      return res.filter(data => data.activo === 1);
    })).subscribe(result => {
      this.formaPagoData = result;
    });
  }

  getEstadoPago() {
    this.estadoPagoService.get().pipe(map((res) => {
      return res.filter(data => data.activo === 1);
    })).subscribe(result => {
      this.estadoPagoData = result;
    });
  }

  bancoChange(value) {
    this.tarjetasService.get().pipe(
      flatMap((y) => y),
      filter((x: TarjetasVn) => x.idBanco === value),
      reduce((acc, data: TarjetasVn) => {
        acc.push({'idBanco': data.idBanco, 'descripcion': data.descripcion, 'idCarrier': data.idCarrier});
        return acc;
      }, []),
    ).subscribe(res => {
      this.tarjetasArray = res;
    });
  }

  carrierChange(value) {
    this.carrierService.getCarrierById(value[0]).pipe(
      reduce((acc: any, data: Carrier) => {
        acc.push({'id': data.id, 'carrier': data.carrier});
        return acc;
      }, []),
    ).subscribe(res => {
      this.carrierArray = res;
    });
  }


  onChange(value) {
    this.selectionformaPago = value;
    this.formPagos.controls.datos.reset();
    // console.log(value);

    if (value === 1) {
      const controlsP = this.formPagos.controls;
      const tcaControls = controlsP.datos['controls'].tarjetaCreditoAseguradora;
      tcaControls.controls.estatusFinanzas.setValidators(Validators.required);
      tcaControls.controls.primaNeta.setValidators(Validators.required);
      tcaControls.controls.estatusPago.setValidators(Validators.required);
      tcaControls.controls.numeroTarjeta.setValidators(Validators.compose([Validators.required, Validators.minLength(16),
        Validators.maxLength(16)]));
      tcaControls.controls.titular.setValidators(Validators.required);
      tcaControls.controls.carrier.setValidators(Validators.required);
      tcaControls.controls.bancoTarjeta.setValidators(Validators.required);
      tcaControls.controls.tipoTarjeta.setValidators(Validators.required);
      tcaControls.controls.mesesIntereses.setValidators(Validators.required);
      tcaControls.controls.codigo.setValidators(Validators.compose([Validators.required, Validators.minLength(3),
        Validators.maxLength(3)]));
      tcaControls.controls.fechaVencimiento.setValidators(Validators.required);
      tcaControls.controls.notas.setValidators(Validators.required);
      tcaControls.controls.comentarios.setValidators(Validators.required);
      tcaControls.controls.notasFinanzas.setValidators(Validators.required);
    } else {
      // const formularioPagos: any = this.formPagos.controls['datos'];
      this.formPagos['controls'].datos['controls'].tarjetaCreditoAseguradora.status = 'VALID';
    }

    if (value === 2) {
      const controlsP = this.formPagos.controls;
      const tcaControls = controlsP.datos['controls'].tarjetaDebito;

      tcaControls.controls.estatusFinanzas.setValidators(Validators.required);
      tcaControls.controls.primaNeta.setValidators(Validators.required);
      tcaControls.controls.estatusPago.setValidators(Validators.required);
      tcaControls.controls.numeroTarjeta.setValidators(Validators.compose([Validators.required, Validators.minLength(16),
        Validators.maxLength(16)]));
      tcaControls.controls.titular.setValidators(Validators.required);
      tcaControls.controls.carrier.setValidators(Validators.required);
      tcaControls.controls.bancoTarjeta.setValidators(Validators.required);
      tcaControls.controls.tipoTarjeta.setValidators(Validators.required);
      tcaControls.controls.mesesIntereses.setValidators(Validators.required);
      tcaControls.controls.codigo.setValidators(Validators.compose([Validators.required, Validators.minLength(3),
        Validators.maxLength(3)]));
      tcaControls.controls.fechaVencimiento.setValidators(Validators.compose([Validators.required, Validators.minLength(7),
        Validators.maxLength(7)]));
      tcaControls.controls.notas.setValidators(Validators.required);
      tcaControls.controls.comentarios.setValidators(Validators.required);
      tcaControls.controls.notasFinanzas.setValidators(Validators.required);
    } else {
      this.formPagos['controls'].datos['controls'].tarjetaDebito.status = 'VALID';
    }

    if (value === 4) {
      const controlsP = this.formPagos.controls;
      const tdControls = controlsP.datos['controls'].depositoBancario;
      tdControls.controls.comentarios.setValidators(Validators.required);
    } else {
      this.formPagos['controls'].datos['controls'].depositoBancario.status = 'VALID';
    }

    if (value === 5) {
      const controlsP = this.formPagos.controls;
      const tdControls = controlsP.datos['controls'].domiciliacion;
      tdControls.controls.banco.setValidators(Validators.required);
      tdControls.controls.primaNeta.setValidators(Validators.required);
      tdControls.controls.estatusFinanzas.setValidators(Validators.required);
      tdControls.controls.estatusPago.setValidators(Validators.required);
    } else {
      this.formPagos['controls'].datos['controls'].domiciliacion.status = 'VALID';
    }

    if (value === 6) {
      const controlsP = this.formPagos.controls;
      const tcaControls = controlsP.datos['controls'].tarjetaCreditoAhorra;

      tcaControls.controls.estatusFinanzas.setValidators(Validators.required);
      tcaControls.controls.primaNeta.setValidators(Validators.required);
      tcaControls.controls.estatusPago.setValidators(Validators.required);
      tcaControls.controls.numeroTarjeta.setValidators(Validators.compose([Validators.required, Validators.minLength(16),
        Validators.maxLength(16)]));
      tcaControls.controls.titular.setValidators(Validators.required);
      tcaControls.controls.carrier.setValidators(Validators.required);
      tcaControls.controls.bancoTarjeta.setValidators(Validators.required);
      tcaControls.controls.tipoTarjeta.setValidators(Validators.required);
      tcaControls.controls.mesesIntereses.setValidators(Validators.required);
      tcaControls.controls.codigo.setValidators(Validators.compose([Validators.required, Validators.minLength(3),
        Validators.maxLength(3)]));
      tcaControls.controls.fechaVencimiento.setValidators(Validators.required);
      tcaControls.controls.notas.setValidators(Validators.required);
      tcaControls.controls.comentarios.setValidators(Validators.required);
      tcaControls.controls.notasFinanzas.setValidators(Validators.required);
    } else {
      this.formPagos['controls'].datos['controls'].tarjetaCreditoAhorra.status = 'VALID';
    }

    if (value === 7) {
      const controlsP = this.formPagos.controls;
      const tdControls = controlsP.datos['controls'].transferencia;

      tdControls.controls.banco.setValidators(Validators.required);
      tdControls.controls.primaNeta.setValidators(Validators.required);
      tdControls.controls.estatusPago.setValidators(Validators.required);
      tdControls.controls.estatusFinanzas.setValidators(Validators.required);
      tdControls.controls.noAutorizacion.setValidators(Validators.required);
    } else {
      this.formPagos['controls'].datos['controls'].transferencia.status = 'VALID';
    }

  }

  fechaTarjeta(value) {

    if (value.length === 2 && value.charAt(1) !== '/') {
      this.fechaV.nativeElement.value = this.fechaV.nativeElement.value.substring(0, 2) + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }

    if (value.charAt(1) === '/') {
      this.fechaV.nativeElement.value = this.fechaV.nativeElement.value.substring(0, 1) + '' + '/' +
        this.fechaV.nativeElement.value.substring(2, 6);
    }

  }

  guardar() {
    this.formRaw = this.formPagos.getRawValue();
    switch (this.selectionformaPago) {
      case 1:
        const convert = JSON.stringify(this.formRaw.datos.tarjetaCreditoAseguradora);
        this.formRaw.datos = null;
        this.formRaw.datos = convert;
        break;
      case 2:
        const convert2 = JSON.stringify(this.formRaw.datos.tarjetaDebito);
        this.formRaw.datos = null;
        this.formRaw.datos = convert2;
        break;
      case 4:
        const convert3 = JSON.stringify(this.formRaw.datos.depositoBancario);
        this.formRaw.datos = null;
        this.formRaw.datos = convert3;
        break;
      case 5:
        const convert4 = JSON.stringify(this.formRaw.datos.domiciliacion);
        this.formRaw.datos = null;
        this.formRaw.datos = convert4;
        break;
      case 6:
        const convert5 = JSON.stringify(this.formRaw.datos.tarjetaCreditoAhorra);
        this.formRaw.datos = null;
        this.formRaw.datos = convert5;
        break;
      case 7:
        const convert6 = JSON.stringify(this.formRaw.datos.transferencia);
        this.formRaw.datos = null;
        this.formRaw.datos = convert6;
        break;
      default:
        break;
    }

    this.pagosService.post(this.formRaw).subscribe(msg => {
      this.formPagos.reset();
    });
  }
  guardarEnDrive($event) {
    const uploadFiles: FileList = (<HTMLInputElement>event.target).files;
    const extensionValida: Boolean = this.extensionesDeArchivoAceptadas.includes(uploadFiles.item(0).type);
    const tamanioArchivo = uploadFiles.item(0).size * .000001;

    if (uploadFiles.length === 1) {
      // El servicio en Spring solo acepta a lo más 1.048576 MB para subir archivos
      if (extensionValida &&  tamanioArchivo < 1.04) {
        Swal.fire({
          title: 'Se están subiendo los archivos',
          onBeforeOpen: () => {
            Swal.showLoading();
          },
        });

        this.llamadasService.post(uploadFiles, 'pago').subscribe({
          error: error => {
            let texto: string;

            if (error.status !== 417) {
              texto = 'Hubo un error, favor de contactar al departamento de TI';
            } else {
              texto = error.error;
            }
            Swal.fire({
              title: 'Error',
              text: texto,
              type: 'error',
            });
          }, complete: () => {
            Swal.fire({
              title: 'Archivos subidos',
              text: '¡Tus archivos han sido subidos exitosamente!',
              type: 'success',
            });
          }});
      } else {
          let titulo: string;
          let texto: string;

          if (!extensionValida) {
            titulo = 'Extensión no soportada';
            texto = 'Solo puedes subir archivos pdf, jpeg, jpg y png';
          } else {
            titulo = 'Archivo demasiado grande';
            texto = 'Los archivos que subas deben pesar menos de 1.04 MB';
          }

          Swal.fire({
            title: titulo,
            text: texto,
            type: 'error',
          });
      }
    }
  }
}

