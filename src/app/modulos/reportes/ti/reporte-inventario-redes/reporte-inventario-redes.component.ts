import { Component, OnInit, ViewChild } from '@angular/core';
import { SwitchsData, Switchs } from '../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import { ServidoresData, Servidores } from '../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import { TarjetaRedData, TarjetaRed } from '../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import { TelefonoIpData, TelefonoIp } from '../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import { PatchPanelData, PatchPanel } from '../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import { CharolasRackData, CharolasRack } from '../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import { RacksData, Racks } from '../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {map} from 'rxjs/operators';
import * as d3Select from 'd3-selection';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';

@Component({
  selector: 'ngx-reporte-inventario-redes',
  templateUrl: './reporte-inventario-redes.component.html',
  styleUrls: ['./reporte-inventario-redes.component.scss'],
})
export class ReporteInventarioRedesComponent implements OnInit {
  private margin = {top: 20, right: 20, bottom: 20, left: 20};
  private width: number;
  private height: number;
  switch: Switchs[];
  servidores: Servidores[];
  tarjetaRed: TarjetaRed[];
  telefonoIp: TelefonoIp[];
  racks: Racks[];
  patchPanel: PatchPanel[];
  charolaRack: CharolasRack[];
  dataSource: any;
  rangeDates: Date[];
  cols: any[];
  tipoEquipos: string = 'switch';
  contadorPermisos: number;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  private svg: any;
  private arc: any;
  private circle: any;
  private circle2: any;
  private rect1: any;
  private rect2: any;
  private rect3: any;
  private start1: any;
  private start2: any;
  private datosMostrar: any;
  private text1: any;
  private text2: any;
  private text3: any;
  private percent1: any;
  private percent2: any;

  constructor(
    private switchsService: SwitchsData,
    private servidoresService: ServidoresData,
    private tarjetaRedService: TarjetaRedData,
    private telefonoIdService: TelefonoIpData,
    private racksService: RacksData,
    private patchPanelService: PatchPanelData,
    private charolasRackService: CharolasRackData,
  ) {
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 480 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this.default();
    this.drawSVG();
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
    this.cols = [
      'nombre',
      'usuario',
      'nombreMarca',
    ];
    this.getEquipo();
  }

  getEquipo() {
    switch (this.tipoEquipos) {
      case 'switch':
        this.switchsService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.switch = data;
          this.dataSource = new MatTableDataSource(this.switch);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'servidores':
        this.servidoresService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.servidores = data;
          this.dataSource = new MatTableDataSource(this.servidores);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'tarjeta de red':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'telefonos ip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'rack':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.racks = data;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'patch panel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.patchPanel = data;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'charolas rack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.charolaRack = data;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      default: break;
    }
  }
  getEquipos(tipoEquipo) {
    this.tipoEquipos = tipoEquipo.toLowerCase();
    switch (this.tipoEquipos) {
      case 'switch':
        this.switchsService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.switch = data;
          this.contadorPermisos = +this.switch.length;
          this.dataSource = new MatTableDataSource(this.switch);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
        this.tipoEquipos = 'switch';
      break;
      case 'servidores':
        this.servidoresService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.servidores = data;
          this.contadorPermisos = this.servidores.length;
          this.dataSource = new MatTableDataSource(this.servidores);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      case 'tarjeta de red':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.contadorPermisos = this.tarjetaRed.length;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      case 'telefonos ip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.contadorPermisos = this.telefonoIp.length;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      case 'rack':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.racks = data;
          this.contadorPermisos = this.racks.length;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      case 'patch panel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.patchPanel = data;
          this.contadorPermisos = this.patchPanel.length;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      case 'charolas rack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.charolaRack = data;
          this.contadorPermisos = this.charolaRack.length;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.removedata();
          this.drawdash(data);
        });
      break;
      default: break;
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawdash(data) {
    this.datosMostrar = this.dataCompose(data);

    const ringAsignado = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', '#808080')
      .attr('transform', 'translate(115,150)')
      .attr('d', this.arc);

    const ringNoAsignado = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', '#808080')
      .attr('transform', 'translate(345,150)')
      .attr('d', this.arc);

    ringAsignado.transition()
      .duration(1500)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));

    ringNoAsignado.transition()
      .duration(1500)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));

    const perc1 = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    const perc2 = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percActivo = this.svg.append('text').attr('x', 150).attr('y', 60).attr('font-size', 35).attr('id', 'txtValue').text(perc1).attr('fill', '#62c442');
    const percNactivo = this.svg.append('text').attr('x', 380).attr('y', 60).attr('font-size', 35).attr('id', 'txtValue').text(perc2).attr('fill', '#5c52a6');

    // percActivo.transition().duration(1500)
    //   .tween('text', () =>  (t) => d3Select.select(this).text(d3Inter.interpolateNumber( 0, perc1)(t)) );

    const txtTotal = this.svg.append('text').attr('x', 70).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[0]).attr('fill','#a4a4a4');
    const txtActivo = this.svg.append('text').attr('x', 220).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[1].length).attr('fill','#a4a4a4');
    const txtNoactivo = this.svg.append('text').attr('x', 355).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[2].length).attr('fill','#a4a4a4');

  }

  drawSVG() {
    this.svg = d3Select.select('#graph').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width} ${this.height}`);

    this.arc = d3Shape.arc()
      .innerRadius(76)
      .outerRadius(80)
      .startAngle(0);

    this.circle = this.svg.append('circle')
      .attr('cx', 115)
      .attr('cy', 150)
      .attr('r', 60)
      .style('fill', '#62c442')
      .attr('stroke', '#62c442').style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => this.activos())
      .attr('cursor', 'pointer')
      .on('mouseover', 'fill: #000000');

    this.circle2 = this.svg.append('circle')
      .attr('cx', 345)
      .attr('cy', 150)
      .attr('r', 60)
      .style('fill', '#5c52a6')
      .attr('stroke', '#5c52a6').style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => this.inactivos())
      .attr('cursor', 'pointer');

    const textA = this.svg.append('text').attr('x', 74).attr('y', 160).text('ACTIVOS').attr('fill', 'white').attr('font-size', 20)
      .on('click', () => this.activos())
      .attr('cursor', 'pointer');
    const textNa = this.svg.append('text').attr('x', 295).attr('y', 160).text('INACTIVOS').attr('fill', 'white').attr('font-size', 20)
      .on('click', () => this.inactivos())
      .attr('cursor', 'pointer');
    this.rect1 = this.svg.append('rect').attr('y', 280).attr('x', 10).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.rect2 = this.svg.append('rect').attr('y', 280).attr('x', 156).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.rect3 = this.svg.append('rect').attr('y', 280).attr('x', 302).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.start1 = this.svg.append('rect').attr('x', 115).attr('y', 20).attr('height', 50).attr('width', 4).attr('fill', '#808080');
    this.start2 = this.svg.append('rect').attr('x', 345).attr('y', 20).attr('height', 50).attr('width', 4).attr('fill', '#808080');
    this.text1 = this.svg.append('text').attr('x', 65).attr('y', 327).attr('font-size', 13).text('Total');
    this.text2 = this.svg.append('text').attr('x', 208).attr('y', 327).attr('font-size', 13).text('Activos');
    this.text3 = this.svg.append('text').attr('x', 340).attr('y', 327).attr('font-size', 13).text('No Activos');
    this.percent1 = this.svg.append('text').attr('x', 125).attr('y', 60).attr('font-size', 30).text('%').attr('fill', '#62c442');
    this.percent2 = this.svg.append('text').attr('x', 355).attr('y', 60).attr('font-size', 30).text('%').attr('fill', '#5c52a6');
  }

  dataCompose(d) {
    const tamaño = d.length;
    const noActivos = [];
    const activos = [];

    for (let i = 0; i < tamaño; i++) {
      if (d[i].activo === 0) {
        noActivos.push(d[i]);
      } else if (d[i].activo === 1) {
        activos.push(d[i]);
      }
    }
    return [tamaño, activos, noActivos];

  }

  removedata() {
    console.log('Datos eliminados...');
    this.svg.selectAll('path').remove();
    this.svg.selectAll('#txtValue').remove();
  }

  activos() {
    const data = this.datosMostrar[1];
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  inactivos() {
    const data = this.datosMostrar[2];
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  default() {
    this.switchsService.get().pipe(map(result => {
      return result.filter(data => data);
    })).subscribe(data => {
      this.switch = data;
      this.contadorPermisos = +this.switch.length;
      this.dataSource = new MatTableDataSource(this.switch);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.drawdash(data);
    });
  }
}
