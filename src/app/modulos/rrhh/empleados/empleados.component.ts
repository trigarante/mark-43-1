import {Component, OnInit, ViewChild} from '@angular/core';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {map} from 'rxjs/operators';
import {BajaEmpleadoComponent} from '../modals/baja-empleado/baja-empleado.component';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AppContext} from '../../../@core/data/services/app.context';
import {Router} from '@angular/router';
import {DatosEmpleadoComponent} from '../modals/datos-empleado/datos-empleado.component';
import {environment} from '../../../../environments/environment';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
@Component({
  selector: 'ngx-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.scss'],
})
export class EmpleadosComponent implements OnInit {
  cols: any[];
  empleado: Empleados[];
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * En este caso: Servicio de Empleados.
   * @param dialogService
   * @param appContext
   * @param router
   * @param {EmpleadosData} empleadoService Servicio del componente Empleados para poder invocar los métodos. */
  constructor(protected empleadoService: EmpleadosData, private dialogService: NbDialogService,
  private appContext: AppContext,
  private router: Router,  private toastrService: NbToastrService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'empleados');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelEmpleados', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEmpleado(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getEmpleado(mensajeAct) {
    this.empleadoService.get().pipe(map(result => {
      return result.filter(value => value.idEtapa === 3 && value.idEstadoRH === 1 || value.idEstadoRH === 16);
    })).subscribe( data => {
      this.empleado = data;
      this.dataSource = new MatTableDataSource(this.empleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Empleados`);
  }
  /** Función que despliega un modal mandando llamar a BajaEmpleadoComponent para el formulario. */
  bajaEmpleado(idPrecandidato: number, idEmpleado: number) {
    this.dialogService.open(BajaEmpleadoComponent, {
      context: {
        idPrecandidato: idPrecandidato,
        idEmpleado: idEmpleado,
      },
    }).onClose.subscribe(x => {
      this.getEmpleado(0);
    });
  }
  verDatos(idEmpleado) {
    this.dialogService.open(DatosEmpleadoComponent, {
      context: {
        idEmpleado: idEmpleado,
      },
    });
  }
  /** Función que inicializa el componente, mandando a llamar a getEmpleado para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.cols = [ 'detalle', 'id', 'nombre', 'sede', 'subarea', 'puesto', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getEmpleado(0);
  }
  estaActivo() {
    this.activo = this.permisos.modulos.empleados.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.empleados.escritura;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  expedientePersonal(documento: string, id) {
    this.appContext.Session.Gapi.signIn()
      .then(() => {
        if (this.appContext.Session.Gapi.isSignedIn) {
          this.router.navigate(['/modulos/rrhh/documentos', documento, id],
            { state: { title: 'PERSONAL' } });
        }
      });
  }
  expedienteAdministrativo(documento: string, id) {
    this.appContext.Session.Gapi.signIn()
      .then(() => {
        if (this.appContext.Session.Gapi.isSignedIn) {
          this.router.navigate(['/modulos/rrhh/documentos', documento, id],
            { state: { title: 'ADMINISTRATIVO' }});
        }
      });
  }
  createDocumentosPFolder(id, name: string) {
    this.appContext.Session.Gapi.signIn()
      .then(() => {
        if (this.appContext.Session.Gapi.isSignedIn) {
          this.appContext.Repository.File.create(
            '1BdSVOMcUwJJoSBXt1zbeUcfmHKXxtpxj',
            name)
            .then((res) => {
              this.empleadoService.saveDocumento(id, res.Id, 0).subscribe(
                window.location.reload(),
              );
            });
        }
      });
  }
  createDocumentosAFolder(id, name: string) {
    this.appContext.Session.Gapi.signIn()
      .then(() => {
        if (this.appContext.Session.Gapi.isSignedIn) {
          this.appContext.Repository.File.create(
            '1ojzbD1HpKPZ2GQp-NzaVkBEo-qpHyL_6',
            name)
            .then((res) => {
              this.empleadoService.saveDocumento(id, res.Id, 1).subscribe(
                window.location.reload(),
              );
            });
        }
      });
  }
}
