export interface TipoDocumentos {
    nombre: string;
    idPais: number;
    activo: number;
    descripcion: string;
}
