import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Laptop Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 9,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    numeroSerie: '0001L',
    idProcesador: 1,
    hdd: '500',
    ram: '4',
    idMarca: 1,
    modelo: '0001L',
    fechaRecepcion: '2019-09-24T16:18:52.000+0000',
    fechaSalida: null,
    comentarios: 'EXCELENTE',
    medidaPantalla: null,
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    nombreProcesador: 'NVIDIA',
    idSede: 1,
    origenRecurso: 'PROPIO',
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'monitor',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/monitor');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/monitor', '/9');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/monitor');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/monitor', '/9/74');
      });
    });
  }
});
