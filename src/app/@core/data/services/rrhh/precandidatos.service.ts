import {Injectable} from '@angular/core';
import {Precandidatos, PrecandidatosData} from '../../interfaces/rrhh/precandidatos';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class PrecandidatosService extends PrecandidatosData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Precandidatos[]> {
    return this.http.get<Precandidatos[]>(this.baseURL + 'v1/precandidatos/get-all/' + sessionStorage.getItem('Usuario'));
  }

  post(precandidato: Precandidatos, idRrhh, fechaNacimiento): Observable<Precandidatos[]> {
    return this.http.post<Precandidatos[]>(this.baseURL + 'v1/precandidatos/' + idRrhh + '/' + fechaNacimiento, precandidato);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'precandidatos/savePrecandidato/1', json);
  }
  postSocketBajaRH(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'bajasrh/saveBajasRH', json);
  }

  validarCurp(curp) {
    return this.http.get(this.baseURL + 'v1/precandidatos/' + curp);
  }

  getPrecandidatoById(idPrecandidato): Observable<Precandidatos> {
    return this.http.get<Precandidatos>(this.baseURL + 'v1/precandidatos/id/' + idPrecandidato);
  }

  updatePrecandidato(idPrecandidato, precandidato: Precandidatos, fechaNacimiento): Observable<Precandidatos> {
    return this.http.put<Precandidatos>(this.baseURL + 'v1/precandidatos/' + idPrecandidato + '/' + fechaNacimiento
      + '/' + sessionStorage.getItem('Empleado'), precandidato);
  }

  activaPrecandidato(idPrecandidato): Observable<Precandidatos> {
    return this.http.put<Precandidatos>(this.baseURL + 'v1/precandidatos/pre-empleado/' + idPrecandidato
      + '/' + sessionStorage.getItem('Empleado'), idPrecandidato);
  }

  reactivarPrecandidato(idPrecandidato, date): Observable<Precandidatos> {
    return this.http.put<Precandidatos>(this.baseURL + 'v1/precandidatos/reactivar/' + idPrecandidato + '/' + date
      + '/' + sessionStorage.getItem('Empleado'), idPrecandidato);
  }

  editarRecontratable(idPrecandidato, recontratable): Observable<Precandidatos> {
    return this.http.put<Precandidatos>(this.baseURL + 'v1/precandidatos/recontratar/' + idPrecandidato + '/' + recontratable
      + '/' + sessionStorage.getItem('Empleado'), idPrecandidato);
  }

  bajaPrecandidato(idPrecandidato, precandidato: Precandidatos, fechaBaja) {
    return this.http.put<Precandidatos>(this.baseURL + 'v1/precandidatos/baja/' + idPrecandidato + '/' + fechaBaja
      + '/' + sessionStorage.getItem('Empleado'), precandidato);
  }
}
