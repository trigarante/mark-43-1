import {Global} from '../global';
import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {EtapasModulosService} from './etapasModulos.service';

describe('EtapasModulos service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: EtapasModulosService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = {
    id: 30,
    idArea: 25,
    etapa: 'TEST UNICO',
    nivel: 3,
    visible: 0,
    escritura: 0,
    activo: 0,
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EtapasModulosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EtapasModulosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  describe('funsiona servicio correctamente', () => {
    describe('get', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.get.toString());
        chai.request(url)
          .get(go)
          .end((err, res) => {
            console.log('EtapasModulosService get GET 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion get debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.get);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('getEtapasModulosById', () => {
      it('status 200', (done) => {
        const url = validarFuncion();
        const go = validarFuncion(service.getEtapasModulosById.toString());
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        chai.use(chaiHttp);
        chai.request(url)
          .get(go + 1)
          .end((err, res) => {
            console.log('EtapasModulosService getEtapasModulosById 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getEtapasModulosById debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getEtapasModulosById);
              }
            done();
          });
        done();
        setTimeout(done, 500);
      });
    });
    describe('post', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.createEtapasModulos.toString());
        chai.request(url)
          .post(go)
          .send(body)
          .end( (err, res) => {
            console.log('EtapasModulosService createEtapasModulos POST 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion createEtapasModulos debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.createEtapasModulos.toString());
              }
            /*expect(res.status).toBe(200);*/
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('put', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.updateEtapasModulos.toString());
        chai.request(url)
          .put(go + 30)
          .send(body)
          .end( (err, res) => {console.log('EtapasModulosService updateEtapasModulos PUT 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion updateEtapasModulos debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.updateEtapasModulos);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
})
