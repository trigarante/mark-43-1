import {Observable} from 'rxjs';

export interface CotizacionesAli {
  id: number;
  idCotizacion: number;
  idCategoriaSocio: number;
  cotizo: number;
  peticion: string;
  respuesta: string;
  interesado: number;
  fechaCotizacion: string;
  prima: number;
  fechaActualizacion: string;
}

export abstract class CotizacionesAliData {
  abstract get(): Observable<CotizacionesAli[]>;

  abstract post(cotizacionesAli): Observable<CotizacionesAli>;

  abstract getCotizacionAliById(idCotizacionAli): Observable<CotizacionesAli>;

  abstract put(idCotizacionAli, cotizacionesAli): Observable<CotizacionesAli>;
}
