import {Observable} from 'rxjs';

export interface PuestoTipo {
  id: number;
  nombre: string;
  activo: number;
}
export abstract class PuestoTipoData {
  abstract get(): Observable<PuestoTipo[]>;
  abstract post(puestoTipo): Observable<PuestoTipo>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idPuestoTipo, puestoTipo): Observable<PuestoTipo>;
  abstract getPuestoTipoById(idPuestoTipo): Observable<PuestoTipo>;

}
