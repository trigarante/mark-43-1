import {Observable} from 'rxjs';

export interface SociosComercial {
  idEstadoSocio: number;
  prioridad: number;
  nombreComercial: string;
  rfc: string;
  razonSocial: string;
  alias: string;
  prioridades: string;
  estado: number;
  activo: string;
  id: number;
}

export abstract class SociosData {
  abstract get(): Observable<SociosComercial[]>;

  abstract post(socios): Observable<SociosComercial>;
  abstract postSocket(json): Observable<JSON>;

  abstract getSociosById(idSocios): Observable<SociosComercial>;

  abstract getIdByAlias(alias): Observable<SociosComercial>;

  abstract put(idSocios, socios): Observable<SociosComercial>;
}
