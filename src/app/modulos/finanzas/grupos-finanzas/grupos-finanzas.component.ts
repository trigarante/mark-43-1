import {Component, OnInit, ViewChild} from '@angular/core';
import {Grupo, GrupoData} from '../../../@core/data/interfaces/catalogos/grupo';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {CreateGrupoComponent} from '../modals/create-grupo/create-grupo.component';
import {CreateEmpresaComponent} from '../modals/create-empresa/create-empresa.component';

@Component({
  selector: 'ngx-grupos-finanzas',
  templateUrl: './grupos-finanzas.component.html',
  styleUrls: ['./grupos-finanzas.component.scss'],
})
export class GruposFinanzasComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  grupo: Grupo[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  grupoo: Grupo;
  dataSource: any;
  permisos: any;
  escritura: boolean;

  constructor(
    private grupoService: GrupoData,
    private dialogoService: NbDialogService,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.grupo = [];
      this.grupo = data;
      this.dataSource = new MatTableDataSource(this.grupo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearGrupo() {
    this.dialogoService.open(CreateGrupoComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getGrupo();
    });
  }

  updateGrupo(idGrupo) {
    this.dialogoService.open(CreateGrupoComponent, {
      context: {
        idTipo: 2,
        idGrupo: idGrupo,
      },
    }).onClose.subscribe(x => {
      this.getGrupo();
    });
  }

  getGrupoById(idGrupo) {
    this.grupoService.getGrupoById(idGrupo).subscribe(data => {
      this.grupoo = data;
    });
  }

  createEmpresa(idGrupo) {
    this.dialogoService.open(CreateEmpresaComponent, {
      context: {
        idTipo: 1,
        idGrupo: idGrupo,
      },
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  bajaGrupo(idGrupo) {
    this.getGrupoById(idGrupo);
    swal({
      title: '¿Deseas dar de baja este grupo?',
      text: 'Una vez dada de baja, no podrás ver el grupo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.grupoo['activo'] = 0;
        this.grupoService.put(idGrupo, this.grupoo).subscribe(result => {
        });
        swal('¡El grupo se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getGrupo();
        }));
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.catalogos.escritura;
  }

  ngOnInit() {
    this.getGrupo();
    this.cols = ['pais', 'nombre', 'descripcion', 'acciones'];
    this.getPermisos();
  }

}
