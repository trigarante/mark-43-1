import {Observable} from 'rxjs';


export interface BarrasMulticontacto {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class BarrasMulticontactoData {
  abstract get(): Observable<BarrasMulticontacto[]>;

  abstract post(barrasMulticontacto): Observable<BarrasMulticontacto>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getBarrasMulticontactoById(idBarrasMulticontacto): Observable<BarrasMulticontacto>;

  abstract put(idBarrasMulticontacto, barrasMulticontacto): Observable<BarrasMulticontacto>;
}
