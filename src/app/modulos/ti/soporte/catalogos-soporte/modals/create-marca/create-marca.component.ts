import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BancoComponent} from '../../../../../rrhh/catalogos/banco/banco.component';
import {Router} from '@angular/router';
import {Marca, MarcaSoporteData} from '../../../../../../@core/data/interfaces/ti/catalogos/marca';
import {MarcaComponent} from '../../marca/marca.component';


@Component({
  selector: 'ngx-create-marca',
  templateUrl: './create-marca.component.html',
  styleUrls: ['./create-marca.component.scss'],
})
export class CreateMarcaComponent implements OnInit {
  /**Varible que almacena el ID de la marca para actualizar datos*/
  @Input() idMarca: number;
  /**Variable que almacena el tipo de datos que se motraran*/
  @Input() idTipo: number;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  marcaCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Variable que se inicializa con los valores del Microservicio. */
  marca: Marca;

  constructor(private fb: FormBuilder,
              protected ref: NbDialogRef<MarcaComponent>,
              private router: Router, protected marcaSoporteService: MarcaSoporteData ) { }

  ngOnInit() {
    this.marcaCreateForm = this.fb.group({
      'nombreMarca': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getMarcaById();
    }
  }

  guardarMarca() {
    if (this.marcaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.marcaSoporteService.post(this.marcaCreateForm.value).subscribe((result) => {
      this.marcaSoporteService.postSocket(this.marcaCreateForm.value).subscribe(x => {});
      this.marcaCreateForm.reset();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/marca']);
      this.ref.close();
    });
  }

  /** Función que obtiene la información de una marca dependiendo de su ID. */
  getMarcaById() {
    this.marcaSoporteService.getMarcasSoporte(this.idMarca).subscribe(data => {
      this.marca = data;
      this.marcaCreateForm.controls['nombreMarca'].setValue(this.marca.nombreMarca);
    });
  }
  updateMarca() {
    this.marcaSoporteService.put(this.idMarca, this.marcaCreateForm.value).subscribe( result => {
      this.marcaSoporteService.postSocket(this.marcaCreateForm.value).subscribe(x => {});
      this.marcaCreateForm.reset();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/marca']);
      this.ref.close();
    });
  }

  dismiss() {
    this.ref.close();
  }

}
