import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {MotivosBajaComponent} from '../../motivos-baja/motivos-baja.component';
import {EstadoRrhh, EstadoRrhhData} from '../../../../../@core/data/interfaces/catalogos/estado-rrhh';
import {Router} from '@angular/router';
import {EtapasModulos, EtapasModulosData} from '../../../../../@core/data/interfaces/catalogos/etapasModulos';
/** Componente del modal del catálogo para "crear motivo de baja" */

@Component({
  selector: 'ngx-motivo-baja-create',
  templateUrl: './motivo-baja-create.component.html',
  styleUrls: ['./motivo-baja-create.component.scss'],
})
export class MotivoBajaCreateComponent implements OnInit {

  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idMotivo obtenido. */
  @Input() idMotivo: number;
  /** Variable que permite la inicialización del formulario para realizar las operaciones de adición y actualización
   * de un motivo de baja. */
  motivoForm: FormGroup;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Arreglo que se inicializa con los valores del campo EstadoRrhh desde el Microservicio. */
  motivoBaja: EstadoRrhh;
  /** Arreglo que se inicializa con los valores del campo EtapasModulos desde el Microservicio. */
  etapasModulos: EtapasModulos[];
  /** Arreglo auxiliar para etapasModulos. */
  etapas: any[];
  motivobajaEliminado: boolean;
  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<MotivosBajaComponent>} ref Referencia al diálogo (mensaje).
   * @param {EstadoRrhhData} motivosService Servicio del componente EstadoRrhh para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes.
   * @param {EtapasModulosData} etapasModulosService Servicio del componente EtapasModulos para poder invocar los métodos. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<MotivosBajaComponent>, private motivosService: EstadoRrhhData,
              private router: Router, private etapasModulosService: EtapasModulosData) {
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que realiza la adición de un motivo de baja ejecutando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  crearMotivo() {
    if (this.motivoForm.invalid) {
      return;
    }
    this.motivosService.createEstadoRrhh(this.motivoForm.value).subscribe((result) => {
      this.motivosService.postSocket(this.motivoForm.value).subscribe(x => {});
      this.motivoForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Función que actualiza la información de un motivo de baja. */
  updateMotivo() {
    this.motivosService.getEstadoRrhhById(this.idMotivo).subscribe(data => {
      if(data.activo === 1){
        this.motivosService.updateEstadoRrhh(this.idMotivo, this.motivoForm.value).subscribe(result => {
          this.motivosService.postSocket(this.motivoForm.value).subscribe(x => {});
          this.motivoForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else {
        this.motivobajaEliminado =true;
      }
    });

  }

  /** Función que obtiene toda la información del motivo de baja según su ID. */
  getMotivoById() {
    this.motivosService.getEstadoRrhhById(this.idMotivo).subscribe(data => {
      this.motivoBaja = data;
      this.motivoForm.controls['estado'].setValue(this.motivoBaja.estado);
      this.motivoForm.controls['idEtapa'].setValue(data.idEtapa);
    });
  }

  /** Función que obtiene las etapas de los módulos. */
  getEtapasModulos() {
    this.etapasModulosService.get().subscribe(data => {
      this.etapasModulos = data;
      this.etapas = [];
      for (let i = 0; i < this.etapasModulos.length; i++) {
        this.etapas.push({'label': this.etapasModulos[i].etapa, 'value': this.etapasModulos[i].id});
      }
      //this.motivoForm.controls['idEtapa'].setValue(this.etapas);
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getMotivoById();
    }
    this.getEtapasModulos();
    this.motivoForm = this.fb.group({
      'id': this.idMotivo,
      'idEstadoRh': 0,
      'idEtapa': new FormControl('', Validators.required),
      'estado': new FormControl('', Validators.compose([Validators.pattern('^[A-Za-z].*$'), Validators.required])),
      'activo': 1,
    });
  }
}
