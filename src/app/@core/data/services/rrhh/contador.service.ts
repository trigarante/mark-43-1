import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment.prod';
@Injectable({
  providedIn: 'root',
})
export class ContadorService {
  constructor(private httpClient: HttpClient) {
  }
  public getContadorByRespuesta(idR: number) {
    return this.httpClient
      .get<number>(environment.apiUrl + 'v1/cultura-bienestar/get-contador-by-respuesta/' + idR);
  }
}
