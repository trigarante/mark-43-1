import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {Router} from '@angular/router';
import {TipoCategoria, TipoCategoriaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-categoria';

@Component({
  selector: 'ngx-tipo-categoria-create',
  templateUrl: './tipo-categoria-create.component.html',
  styleUrls: ['./tipo-categoria-create.component.scss'],
})
export class TipoCategoriaCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTipoCategoria: number;
  tipoCatgoriaCreateForm: FormGroup;
  submitted: Boolean;
  tipoCategoria: TipoCategoria;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoCategoriaSrvice: TipoCategoriaData) {
  }

  updateTipoCategoria() {
    this.tipoCategoriaSrvice.put(this.idTipoCategoria, this.tipoCatgoriaCreateForm.value).subscribe(result => {
      this.tipoCatgoriaCreateForm.reset();
      this.ref.close();
    });
  }

  guardarTipoCategoria() {
    this.submitted = true;
    if (this.tipoCatgoriaCreateForm.invalid) {
      return;
    }
    this.tipoCategoriaSrvice.post(this.tipoCatgoriaCreateForm.value).subscribe((result) => {
      this.tipoCatgoriaCreateForm.reset();
      this.ref.close();
    });
  }

  getCategoriaById() {
    this.tipoCategoriaSrvice.getTipoCategoriaById(this.idTipoCategoria).subscribe(data => {
      this.tipoCategoria = data;
      this.tipoCatgoriaCreateForm.controls['tipo'].setValue(this.tipoCategoria.tipo);
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCategoriaById();
    }
    this.tipoCatgoriaCreateForm = this.fb.group({
      'tipo': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

}
