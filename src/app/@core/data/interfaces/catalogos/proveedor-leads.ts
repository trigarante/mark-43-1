import { Observable } from 'rxjs';

export interface ProveedorLeads {

  id: number;
  nombre: string;
  url: string;
  activo: number;
}

export abstract class ProveedorLeadsData {

  abstract get(): Observable<ProveedorLeads[]>;

  abstract post(tipoProveedor): Observable<ProveedorLeads>;
  abstract postSocket(json): Observable<JSON>;
  abstract getProveedorLeadById(idProveedor): Observable<ProveedorLeads>;
  abstract put(idProveedor, tipoProveedor): Observable<ProveedorLeads>;
}
