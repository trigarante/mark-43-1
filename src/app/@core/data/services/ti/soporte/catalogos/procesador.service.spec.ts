import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {ProcesadorService} from './procesador.service';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';
fdescribe('Ti catalogos procesador service', () => {
  let injector: TestBed;
  let service: ProcesadorService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 5,
    nombreProcesador: 'INTELPRUEBAA',
    activo: 0,
  },
    {
      id: 'procesador',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ProcesadorService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ProcesadorService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/procesador');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/procesador', '/5');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/procesador');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/procesador', '/5/74');
      });
    });
  }
})
