import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {TipoPaginaService} from './tipo-pagina.service';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';

fdescribe('TipoPagina service', () => {
  let injector: TestBed;
  let service: TipoPaginaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    tipo: '1324',
    objetivo: 'AFGHFGHFFFFFF',
    activo: 0,
  },
    {
      id: 'tipopagina',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoPaginaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(TipoPaginaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tipo-pagina');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tipo-pagina', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tipo-pagina');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tipo-pagina', '/1/74');
      });
    });
  }
})
