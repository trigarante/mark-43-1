// import {Component, OnInit, ViewChild} from '@angular/core';
// import {GrupoUsuarios, GrupoUsuariosData} from '../../../@core/data/interfaces/ti/grupoUsuarios';
// import {map} from 'rxjs/operators';
// import swal from 'sweetalert2';
//
// import {Usuario, UsuarioData} from '../../../@core/data/interfaces/ti/usuario';
// import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
// import { environment } from '../../../../environments/environment';
// const SockJS = require('sockjs-client');
// const Stomp = require('stompjs');
//
// @Component({
//   selector: 'ngx-permisos',
//   templateUrl: './permisos.component.html',
//   styleUrls: ['./permisos.component.scss'],
// })
// export class PermisosComponent implements OnInit {
//   cols: any[];
//   grupoUsuario: GrupoUsuarios[];
//   usuarioSinPermiso: Usuario[];
//   grupUsuario: GrupoUsuarios;
//   variable: string = 'hola';
//   usuario: Usuario[];
//   usuariostring: any;
//   usuarioultimo; any;
//   correosdisponibles: any;
//   usuarioultimo2: any;
//   dataSource: any;
//   // socket
//   disabled = true;
//   private stompClient = null;
//   datos: any[] = [];
//   @ViewChild(MatPaginator) paginator: MatPaginator; //
//   @ViewChild(MatSort) sort: MatSort; //
//
//   constructor(
//     private grupoUsuariosService: GrupoUsuariosData,
//     private usuarioService: UsuarioData,
//     private _snackBar: MatSnackBar
//   ) {}
//
//
//   ngOnInit() {
//     this.getGrupoUsuarios();
//     this.cols = [
//       'subarea',
//       'puesto',
//       'nombre',
//       'descripcion',
//       'modificar',
//     ];
//     this.connect();
//   }
//
//   getGrupoUsuarios() {
//     this.grupoUsuariosService.get().pipe(map(result => {
//       return result.filter(data => data.activo === 1);
//     })).subscribe(data => {
//       // console.log(data);
//       this.grupoUsuario = data;
//       this.dataSource = new MatTableDataSource(this.grupoUsuario);
//       this.dataSource.sort = this.sort;
//       this.dataSource.paginator = this.paginator;
//     });
//   }
//
//   /** Función que obtiene la información de un GrupoUsuario según su ID. */
//   getGrupoUsuarioById(idGrupoUsuario) {
//     this.grupoUsuariosService.getByID(idGrupoUsuario).subscribe(data => {
//       this.grupUsuario = data;
//
//     });
//   }
//
//   grupoUsuariobyId(idGrupoUsuario) {
//     this.usuarioService.get().pipe(map(result => {
//       return result.filter(data => data.idGrupo === idGrupoUsuario);
//     })).subscribe(data => {
//       this.usuario = data;
//       this.correosdisponibles = [];
//       for (let i = 0; i < this.usuario.length; i++) {
//         this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
//       }
//     });
//   }
//
//   getUsuarios(idGrupoUsuario) {
//     this.usuarioService.get().pipe(map(result => {
//       return result.filter(data => data.idGrupo === idGrupoUsuario);
//     })).subscribe(data => {
//       this.usuarioSinPermiso = data;
//       for (let i = 0; i < this.usuarioSinPermiso.length; i++) {
//         this.usuarioSinPermiso[i]['idGrupo'] = 8;
//       }
//     });
//   }
//
//   /** Función que despliega un modal para seleccionar si se da de baja un GrupoUsuario. */
//   bajaGrupoUsuario(idGrupoUsuario) {
//     this.getGrupoUsuarioById(idGrupoUsuario);
//     this.grupoUsuariobyId(idGrupoUsuario);
//
//     this.usuariostring = '';
//     this.usuarioService.get().pipe(map(result => {
//       return result.filter(data => data.idGrupo === idGrupoUsuario);
//     })).subscribe(data => {
//       this.usuario = data;
//       this.correosdisponibles = [];
//       for (let i = 0; i < this.usuario.length; i++) {
//         this.correosdisponibles.push( this.usuario[i].usuario);
//       }
//       for (let i = 0; i < this.correosdisponibles.length; i++) {
//         this.usuariostring  = this.usuariostring + JSON.stringify(this.correosdisponibles[i]) + '\n';
//       }
//       this.usuariostring  = JSON.stringify(this.correosdisponibles);
//       const swal_html = `
//       <div class="panel" style="background:aliceblue;font-weight:bold">
//       <div class="panel-heading panel-info text-center btn-info"> <b>Cuentas Asociadas</b> </div>
//       <div class="panel-body"><div class="text-center"><b>
//       <p style="font-weight:bold">` + this.usuariostring + `</p>
//       </div></div></div>`;
//
//       swal.fire({
//       title: '¿Deseas dar de baja este GrupoUsuario?',
//       html: swal_html,
//       customClass: 'swal-wide',
//       showCancelButton: true,
//       showConfirmButton: true,
//     }).then((valor) => {
//       if (valor) {
//         this.grupUsuario['activo'] = 1; // ?
//         this.grupoUsuariosService.put(idGrupoUsuario, this.grupUsuario)
//         .subscribe( result => {
//
//         });
//         this.usuarioService.PutBajaGrupo(idGrupoUsuario)
//         .subscribe( result => {
//
//         });
//
//       }
//     });
//     });
//   }
//
//   applyFilter(filterValue: string) { //
//     this.dataSource.filter = filterValue.trim().toLowerCase();
//   }
//
//   setConnected(connected: boolean) {
//     this.disabled = !connected;
//     if (connected) { this.datos = []; }
//   }
//
//   connect() {
//     const socket = new SockJS( environment.GLOBAL_SOCKET + 'grupopermisos');
//     this.stompClient = Stomp.over(socket);
//     const _this = this;
//     this.stompClient.connect({}, function(frame) {
//       _this.setConnected(true);
//       console.log(frame);
//
//       _this.stompClient.subscribe('/task/panelGrupoPermisos', (content) => {
//         console.log(content.body);
//         setTimeout(() => {
//           _this.getGrupoUsuarios();
//         }, 500);
//       });
//     });
//   }
//
// }

import {Component, OnInit, ViewChild} from '@angular/core';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../@core/data/interfaces/ti/grupoUsuarios';
import {map} from 'rxjs/operators';
import swal from 'sweetalert2';

import {Usuario, UsuarioData} from '../../../@core/data/interfaces/ti/usuario';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
import { environment } from '../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.scss'],
})
export class PermisosComponent implements OnInit {
  cols: any[];
  grupoUsuario: GrupoUsuarios[];
  usuarioSinPermiso: Usuario[];
  grupUsuario: GrupoUsuarios;
  variable: string = 'hola';
  usuario: Usuario[];
  usuariostring: any;
  usuarioultimo; any;
  correosdisponibles: any;
  usuarioultimo2: any;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    private grupoUsuariosService: GrupoUsuariosData,
    private usuarioService: UsuarioData,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) {}


  ngOnInit() {
    this.getGrupoUsuarios();
    this.cols = [
      'subarea',
      'puesto',
      'nombre',
      'descripcion',
      'modificar',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      // console.log(data);
      this.grupoUsuario = data;
      this.dataSource = new MatTableDataSource(this.grupoUsuario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  /** Función que obtiene la información de un GrupoUsuario según su ID. */
  getGrupoUsuarioById(idGrupoUsuario) {
    this.grupoUsuariosService.getByID(idGrupoUsuario).subscribe(data => {
      this.grupUsuario = data;

    });
  }

  grupoUsuariobyId(idGrupoUsuario) {
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.idGrupo === idGrupoUsuario);
    })).subscribe(data => {
      this.usuario = data;
      this.correosdisponibles = [];
      for (let i = 0; i < this.usuario.length; i++) {
        this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
      }
    });
  }

  getUsuarios(idGrupoUsuario) {
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.idGrupo === idGrupoUsuario);
    })).subscribe(data => {
      this.usuarioSinPermiso = data;
      for (let i = 0; i < this.usuarioSinPermiso.length; i++) {
        this.usuarioSinPermiso[i]['idGrupo'] = 8;
      }
    });
  }

  /** Función que despliega un modal para seleccionar si se da de baja un GrupoUsuario. */
  bajaGrupoUsuario(idGrupoUsuario) {
    this.getGrupoUsuarioById(idGrupoUsuario);
    this.grupoUsuariobyId(idGrupoUsuario);

    this.usuariostring = '';
    this.usuarioService.get().pipe(map(result => {
      return result.filter(data => data.idGrupo === idGrupoUsuario);
    })).subscribe(data => {
      this.usuario = data;
      this.correosdisponibles = [];
      for (let i = 0; i < this.usuario.length; i++) {
        this.correosdisponibles.push( this.usuario[i].usuario);
      }
      for (let i = 0; i < this.correosdisponibles.length; i++) {
        this.usuariostring  = this.usuariostring + JSON.stringify(this.correosdisponibles[i]) + '\n';
      }
      this.usuariostring  = JSON.stringify(this.correosdisponibles);
      const swal_html = `
      <div class="panel" style="background:aliceblue;font-weight:bold">
      <div class="panel-heading panel-info text-center btn-info"> <b>Cuentas Asociadas</b> </div>
      <div class="panel-body"><div class="text-center"><b>
      <p style="font-weight:bold">` + this.usuariostring + `</p>
      </div></div></div>`;

      swal.fire({
      title: '¿Deseas dar de baja este GrupoUsuario?',
      html: swal_html,
      customClass: 'swal-wide',
      showCancelButton: true,
      showConfirmButton: true,
    }).then((valor) => {
      if (valor.value) {
        this.grupUsuario['activo'] = 1; // ?
        this.grupoUsuariosService.put(idGrupoUsuario, this.grupUsuario)
        .subscribe( result => {

        });
        this.usuarioService.PutBajaGrupo(idGrupoUsuario)
        .subscribe( result => {

        });

      }
    });
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'grupopermisos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelGrupoPermisos', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.getGrupoUsuarios();
        }, 500);
      });
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.grupoPermisos.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.grupoPermisos.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
