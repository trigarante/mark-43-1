import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {TurnoComponent} from '../../turno/turno.component';
import {Router} from '@angular/router';
import {Turno, TurnoData} from '../../../../../@core/data/interfaces/catalogos/turno';
@Component({
  selector: 'ngx-turno-create',
  templateUrl: './turno-create.component.html',
  styleUrls: ['./turno-create.component.scss'],
})
export class TurnoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idTurno obtenido. */
  @Input() idTurno: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un turno. */
  turnoCreateform: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  turno: Turno;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  turnoEliminado: boolean;
  turnoss: Turno[];
  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<TurnoComponent>} ref Referencia al diálogo (mensaje).
   * @param {TurnoData} turnoService Servicio del componente turno para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<TurnoComponent>, private turnoService: TurnoData,
              private router: Router) {
  }
  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarCrearTurno() {
    if (this.turnoCreateform.invalid) {
      return;
    }
    this.submitted = true;
    this.turnoService.post(this.turnoCreateform.value).subscribe((result) => {
      this.turnoService.postSocket(this.turnoCreateform.value).subscribe(x => {});
      this.turnoCreateform.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/turno']);
      this.ref.close();
      this.getTurno();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un turno. */
  updateTurno() {
    this.turnoService.getTurnoById(this.idTurno).subscribe(data => {
      if (data.activo === 1) {
        this.turnoService.put(this.idTurno, this.turnoCreateform.value).subscribe(result => {
          this.turnoService.postSocket(this.turnoCreateform.value).subscribe(x => {});
          this.turnoCreateform.reset();
          this.ref.close();
          // window.location.reload();
        });
      } else {
        this.turnoEliminado = true;
      }
    });
  }

  /** Función que da de baja un turno, cambiando el atributo activo a 0. */

  /*bajaTurno() {
    this.turnoCreateform.controls['activo'].setValue(0);
    this.turnoService.put(this.idTurno, this.turnoCreateform.value).subscribe( result => {
      this.turnoCreateform.reset();
      this.ref.close();
      window.location.reload();
      // alert('EL BANCO SE HA DADO DE BAJA.');
    });
  }*/

  /** Función que obtiene la información de un turno dependiendo de su ID. */
  getTurnoById() {
    this.turnoService.getTurnoById(this.idTurno).subscribe(data => {
      this.turno = data;
      this.turnoCreateform.controls['turno'].setValue(this.turno.turno);
      this.turnoCreateform.controls['horario'].setValue(this.turno.horario);
      this.turnoCreateform.controls['activo'].setValue(this.turno.activo);
    });
  }

  getTurno() {
    this.turnoService.get().subscribe(data => {
      this.turnoss = data;
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getTurnoById();
    }
    this.turnoCreateform = this.fb.group({
      'turno': new FormControl('', Validators.compose([Validators.required])),
      'horario': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }
}
