import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TelefonoIp, TelefonoIpData} from '../../../../interfaces/ti/soporte/inventario/telefono-ip';



@Injectable()
export class TelefonoIpService extends TelefonoIpData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<TelefonoIp[]> {
    return this.http.get<TelefonoIp[]>( this.baseURL + 'v1/telefonoIp');
  }

  post(telefonoIp: TelefonoIp): Observable<TelefonoIp> {
    return this.http.post<TelefonoIp>(this.baseURL + 'v1/telefonoIp', telefonoIp);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'telefonoip/saveTelefonoIP', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajatelefonoip/saveBajaTelefonoIP', json);
  }
  getTelefonoIpById(idTelefonoIp): Observable<TelefonoIp> {
    return this.http.get<TelefonoIp>(this.baseURL + 'v1/telefonoIp/' + idTelefonoIp);
  }
  put(idTelefonoIp, telefonoIp: TelefonoIp): Observable<TelefonoIp> {
    return this.http.put<TelefonoIp>(this.baseURL + 'v1/telefonoIp/' + idTelefonoIp + '/'
      + sessionStorage.getItem('Empleado') , telefonoIp);
  }
}
