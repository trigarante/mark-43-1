import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {AsignarSubramoComponent} from '../modals/asignar-subramo/asignar-subramo.component';
import {Ramo, RamoData} from '../../../@core/data/interfaces/comerciales/ramo';
import {DatosRamoComponent} from '../modals/datos-ramo/datos-ramo.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-ramo',
  templateUrl: './ramo.component.html',
  styleUrls: ['./ramo.component.scss'],
})
export class RamoComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  cols: any[];
  ramo: Ramo[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private ramoService: RamoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) { }
  ngOnInit() {
    this.getRamo(0);
    this.cols = [
      'detalle',
      'descripcion',
      'ramo',
      'alias',
      'prioridad',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }
  getRamo(mensajeAct) {
    this.ramoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.ramo = [];
      this.ramo = data;
      this.dataSource = new MatTableDataSource(this.ramo); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  asignarSubRamo(idRamo) {
    this.dialogService.open(AsignarSubramoComponent, {
      context: {
        idRamo: idRamo,
      },
    }).onClose.subscribe(x => {
      this.getRamo(0);
    });
  }

  verDatos(idRamo, idSocio) {
    this.dialogService.open(DatosRamoComponent, {
      context: {
        idRamo: idRamo,
        idSocio: idSocio,
      },
    }).onClose.subscribe(x => {
      this.getRamo(1);
    });
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Ramo`);
  }
  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'ramo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelRamo', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getRamo(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
    // this.router.navigate(['/modulos/comercial/socios']);
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.ramo.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.ramo.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
