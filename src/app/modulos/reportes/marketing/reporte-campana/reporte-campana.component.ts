import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { CampanaMarketingData, CampanaMarketing } from '../../../../@core/data/interfaces/marketing/campana';
import {map} from 'rxjs/operators';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import * as d3Select from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';

@Component({
  selector: 'ngx-reporte-campana',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reporte-campana.component.html',
  styleUrls: ['./reporte-campana.component.scss'],
})

export class ReporteCampanaComponent implements OnInit {
  private margin = {top: 10, right: 10, bottom: 10, left: 10};
  private width: number;
  private height: number;
  private width2: number;
  private height2: number;
  cols: any[];
  dataSource: any;
  campanas: CampanaMarketing[];
  contadorCampanas: number;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  private colors: string[];
  private svg: any;
  private datosMostrar: any;
  private arc: any;
  private root: any;
  private label: any[];
  private svg2: any;
  private back: any;
  private Xscale: any;
  private Yscale: any;
  private xAxis: any;
  private yAxis: any;
  private YAX: any;
  private barr: any;
  private glove: any;
  private datosBackUp: any;

  //

  constructor(
    private campanaService: CampanaMarketingData,
  ) {
    this.width = 300 - this.margin.left - this.margin.right;
    this.height = 450 - this.margin.top - this.margin.bottom;
    this.width2 = 680 - this.margin.left - this.margin.right;
    this.height2 = 450 - this.margin.top - this.margin.bottom;
    this.colors = ['rgba(227,72,201,0.83)', 'rgba(27,138,221,0.92)', '#47a30a'];
  }

  ngOnInit() {
    this.drawSVG();
    this.cols = [
      'nombre',
      'descripcion',
      'subarea',
    ];
    this.getCampanasMarketing();
  }

  getCampanasMarketing() {
    this.campanaService.get().pipe(map(result => {
      return result.filter(data => data);
    })).subscribe(data => {
      this.campanas = data;
      this.contadorCampanas = this.campanas.length;
      this.dataSource = new MatTableDataSource(this.campanas); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      this.drawdash(data);
      console.log(data);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawSVG() {
    this.svg = d3Select.select('#graph1').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${180} ${this.height}`)
      .attr('id', 'svg1');

    const Gcircle = this.svg.append('circle')
      .attr('cx', 60)
      .attr('cy', 125)
      .attr('r', 40)
      .style('fill', this.colors[0])
      .attr('stroke', this.colors[0]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.activo());
    const start1 = this.svg.append('rect').attr('x', 60).attr('y', 57).attr('height', 15).attr('width', 3).attr('fill', this.colors[0]);
    const percent1 = this.svg.append('text').attr('x', 55).attr('y', 106).attr('font-size', 17).text('%').attr('fill', 'white');
    const text1 = this.svg.append('text').attr('x', 38).attr('y', 55).attr('font-size', 15).text('ACTIVO').attr('fill', this.colors[0]);

    const Gcircle2 = this.svg.append('circle')
      .attr('cx', 60)
      .attr('cy', 305)
      .attr('r', 40)
      .style('fill', this.colors[1])
      .attr('stroke', this.colors[1]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => console.log('p2s'))
      .attr('cursor', 'pointer')
      .on('click', () => this.inactivo());
    const start2 = this.svg.append('rect').attr('x', 60).attr('y', 237).attr('height', 15).attr('width', 3).attr('fill', this.colors[1]);
    const percent2 = this.svg.append('text').attr('x', 55).attr('y', 286).attr('font-size', 17).text('%').attr('fill', 'white');
    const text2 = this.svg.append('text').attr('x', 30).attr('y', 230).attr('font-size', 15).text('INACTIVO').attr('fill', this.colors[1]);

    this.arc = d3Shape.arc()
      .innerRadius(53)
      .outerRadius(56)
      .startAngle(0);

  }

  drawdash(d) {
    const tiempoAnimacion = 5000;
    this.datosMostrar = this.dataCompose(d);
    const dataConv = this.dataFilter(d);
    this.drawSVG2(dataConv[0], dataConv[1], d);

    const ringAdmin = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', this.colors[0])
      .attr('transform', 'translate(60,125)')
      .attr('d', this.arc);
    const percAdmin = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    var percAdministrativo = this.svg.append('text').attr('x', 41).attr('y', 138).attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white');

    const ringEjec = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', this.colors[1])
      .attr('transform', 'translate(60,305)')
      .attr('d', this.arc);
    const percEjec = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percEjecutivo = this.svg.append('text').attr('x', 41).attr('y', 318).attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white');

    ringAdmin.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percAdministrativo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percAdministrativo.text(Math.trunc(d3Inter.interpolate(0, percAdmin)(t))) );

    ringEjec.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percEjecutivo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percEjecutivo.text(Math.trunc(d3Inter.interpolate(0, percEjec)(t))) );
  }

  drawSVG2(data, alldata, originalData) {
    const ancho = 500;
    this.root = data;
    this.label = [];
    for (let j = 1; j <= this.root.length; j++) {
      this.label.push(j); }

    this.svg2 = d3Select.select('#graph2').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${400} ${400}`)
      .attr('transform', 'rotate(-90 0 0)')
      .attr('id', 'svg2');

    this.back = this.svg2
      .append('rect')
      .attr('class', 'fondo')
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', ancho - 50)
      .attr('width', this.height2)
      .style('opacity', '0')
      .on('click', () => this.returnOrigin(originalData));

    this.Xscale = d3Scale.scaleLinear()
      .domain([d3Array.max(this.root), 0])
      .range([this.height2 - 100, 0]);
    this.Yscale = d3Scale.scaleBand()
      .domain(this.label)
      .range([0, 380]);
    this.xAxis = d3Axis.axisLeft().scale(this.Xscale).ticks(25);
    this.yAxis = d3Axis.axisBottom().scale(this.Yscale).ticks(25);
    this.YAX = this.svg2.append('g')
      .attr('class', 'axis')
      .attr('transform', 'rotate(90 7 12)')
      .call(this.yAxis);

    this.barr = this.svg2.selectAll('rect .barr')
      .data(this.root)
      .enter()
      .append('rect')
      .attr('y', (d, i) => (i * (400 - 33) / this.root.length) + 8)
      .attr('x', 20)
      .attr('height', 7)
      .attr('width', 0) // (d) => this.Xscale(d) )
      .attr('fill', this.colors[2])
      .attr('rx', '3')
      .attr('cursor', 'pointer')
      .on('mouseover', this.handleMouseOver)
      .on('mousemove',   function (d, i) {
        this.glove
          .html('Area: ' + alldata[i][0].subarea + '<br>' + 'Cantidad: ' + d)
          .style('bottom', ( d3Select.mouse(this)[0] + 70)  + 'px')
          .style('left', (d3Select.mouse(this)[1] ) + 'px');
      } )
      .on('mouseout', this.handleMouseOut)
      .on('click', (d, i) => this.table_barr(d, i, originalData));


    this.barr.transition()
      .duration(1000)
      .delay((d, i) => i * 60)
      .attr('width', (d) => this.Xscale(d) + 7)
      .transition()
      .duration(200)
      .attr('width', (d) => this.Xscale(d) - 6)
      .transition()
      .duration(150)
      .attr('width', (d) => this.Xscale(d) + 4)
      .transition()
      .duration(100)
      .attr('width', (d) => this.Xscale(d));

  }

  dataFilter(data) {
    const areas = [];
    const area = [];
    const area1 = [];
    const area2 = [];
    const area3 = [];
    const area4 = [];
    const area5 = [];
    const area6 = [];
    const area7 = [];
    const area8 = [];
    const area9 = [];
    const area10 = [];
    const area11 = [];
    const area12 = [];
    const area13 = [];
    const area14 = [];
    const area15 = [];
    const area16 = [];
    const area17 = [];
    const area18 = [];
    const area19 = [];
    const area20 = [];
    const area21 = [];
    const area22 = [];
    const area23 = [];
    const area24 = [];
    const area25 = [];
    const area26 = [];
    const area27 = [];
    const area28 = [];
    const area29 = [];
    const area30 = [];
    const area31 = [];
    const area32 = [];
    const area33 = [];
    const area34 = [];
    const area35 = [];
    const area36 = [];
    const area37 = [];
    const area38 = [];
    const area39 = [];
    const area40 = [];
    const area41 = [];
    const area42 = [];
    const area43 = [];
    const area44 = [];
    const area45 = [];
    const area46 = [];
    const area47 = [];
    const area48 = [];
    const area49 = [];
    const area50 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idArea === 1) {
        area1.push(data[i]);
      } else if (data[i].idSubarea === 2) {
        area2.push(data[i]);
      } else if (data[i].idSubarea === 3) {
        area3.push(data[i]);
      } else if (data[i].idSubarea === 4) {
        area4.push(data[i]);
      } else if (data[i].idSubarea === 5) {
        area5.push(data[i]);
      } else if (data[i].idSubarea === 6) {
        area6.push(data[i]);
      } else if (data[i].idSubarea === 7) {
        area7.push(data[i]);
      } else if (data[i].idSubarea === 8) {
        area8.push(data[i]);
      } else if (data[i].idSubarea === 9) {
        area9.push(data[i]);
      } else if (data[i].idSubarea === 10) {
        area10.push(data[i]);
      } else if (data[i].idSubarea === 11) {
        area11.push(data[i]);
      } else if (data[i].idSubarea === 12) {
        area12.push(data[i]);
      } else if (data[i].idSubarea === 13) {
        area13.push(data[i]);
      } else if (data[i].idSubarea === 14) {
        area14.push(data[i]);
      } else if (data[i].idSubarea === 15) {
        area15.push(data[i]);
      } else if (data[i].idSubarea === 16) {
        area16.push(data[i]);
      } else if (data[i].idSubarea === 17) {
        area17.push(data[i]);
      } else if (data[i].idSubarea === 18) {
        area18.push(data[i]);
      } else if (data[i].idSubarea === 19) {
        area19.push(data[i]);
      } else if (data[i].idSubarea === 20) {
        area20.push(data[i]);
      }else if (data[i].idSubarea === 21) {
        area21.push(data[i]);
      }else if (data[i].idSubarea === 22) {
        area22.push(data[i]);
      }else if (data[i].idSubarea === 23) {
        area23.push(data[i]);
      }else if (data[i].idSubarea === 24) {
        area24.push(data[i]);
      }else if (data[i].idSubarea === 25) {
        area25.push(data[i]);
      }else if (data[i].idSubarea === 26) {
        area26.push(data[i]);
      }else if (data[i].idSubarea === 27) {
        area27.push(data[i]);
      }else if (data[i].idSubarea === 28) {
        area28.push(data[i]);
      }else if (data[i].idSubarea === 29) {
        area29.push(data[i]);
      }else if (data[i].idSubarea === 30) {
        area30.push(data[i]);
      }else if (data[i].idSubarea === 31) {
        area31.push(data[i]);
      }else if (data[i].idSubarea === 32) {
        area32.push(data[i]);
      }else if (data[i].idSubarea === 33) {
        area33.push(data[i]);
      }else if (data[i].idSubarea === 34) {
        area34.push(data[i]);
      }else if (data[i].idSubarea === 35) {
        area35.push(data[i]);
      }else if (data[i].idSubarea === 36) {
        area36.push(data[i]);
      }else if (data[i].idSubarea === 37) {
        area37.push(data[i]);
      }else if (data[i].idSubarea === 38) {
        area38.push(data[i]);
      }else if (data[i].idSubarea === 39) {
        area39.push(data[i]);
      }else if (data[i].idSubarea === 40) {
        area40.push(data[i]);
      }else if (data[i].idSubarea === 41) {
        area41.push(data[i]);
      }else if (data[i].idSubarea === 42) {
        area42.push(data[i]);
      }else if (data[i].idSubarea === 43) {
        area43.push(data[i]);
      }else if (data[i].idSubarea === 44) {
        area44.push(data[i]);
      }else if (data[i].idSubarea === 45) {
        area45.push(data[i]);
      }else if (data[i].idSubarea === 46) {
        area46.push(data[i]);
      }else if (data[i].idSubarea === 47) {
        area47.push(data[i]);
      }else if (data[i].idSubarea === 48) {
        area48.push(data[i]);
      }else if (data[i].idSubarea === 49) {
        area49.push(data[i]);
      }else if (data[i].idSubarea === 50) {
        area50.push(data[i]);
      }
    }
    areas.push(area1.length);
    areas.push(area2.length);
    areas.push(area3.length);
    areas.push(area4.length);
    areas.push(area5.length);
    areas.push(area6.length);
    areas.push(area7.length);
    areas.push(area8.length);
    areas.push(area9.length);
    areas.push(area10.length);
    areas.push(area11.length);
    areas.push(area12.length);
    areas.push(area13.length);
    areas.push(area14.length);
    areas.push(area15.length);
    areas.push(area16.length);
    areas.push(area17.length);
    areas.push(area18.length);
    areas.push(area19.length);
    areas.push(area20.length);
    areas.push(area21.length);
    areas.push(area22.length);
    areas.push(area23.length);
    areas.push(area24.length);
    areas.push(area25.length);
    areas.push(area26.length);
    areas.push(area27.length);
    areas.push(area28.length);
    areas.push(area29.length);
    areas.push(area30.length);
    areas.push(area31.length);
    areas.push(area32.length);
    areas.push(area33.length);
    areas.push(area34.length);
    areas.push(area35.length);
    areas.push(area36.length);
    areas.push(area37.length);
    areas.push(area38.length);
    areas.push(area39.length);
    areas.push(area40.length);
    areas.push(area41.length);
    areas.push(area42.length);
    areas.push(area43.length);
    areas.push(area44.length);
    areas.push(area45.length);
    areas.push(area46.length);
    areas.push(area47.length);
    areas.push(area48.length);
    areas.push(area49.length);
    areas.push(area50.length);
    area.push(area1);
    area.push(area2);
    area.push(area3);
    area.push(area4);
    area.push(area5);
    area.push(area6);
    area.push(area7);
    area.push(area8);
    area.push(area9);
    area.push(area10);
    area.push(area11);
    area.push(area12);
    area.push(area13);
    area.push(area14);
    area.push(area15);
    area.push(area16);
    area.push(area17);
    area.push(area18);
    area.push(area19);
    area.push(area20);
    area.push(area21);
    area.push(area22);
    area.push(area23);
    area.push(area24);
    area.push(area25);
    area.push(area26);
    area.push(area27);
    area.push(area28);
    area.push(area29);
    area.push(area30);
    area.push(area31);
    area.push(area32);
    area.push(area33);
    area.push(area34);
    area.push(area35);
    area.push(area36);
    area.push(area37);
    area.push(area38);
    area.push(area39);
    area.push(area40);
    area.push(area41);
    area.push(area42);
    area.push(area43);
    area.push(area44);
    area.push(area45);
    area.push(area46);
    area.push(area47);
    area.push(area48);
    area.push(area49);
    area.push(area50);
    return [areas, area];
  } //// **************

  table_barr (x, i, data) {
    this.datosBackUp = data;
    const grupoUsuarioFilter = [];
    for ( let k = 0; k < data.length; k++) {
      if ( data[k].idSubarea === i + 1 ) {
        grupoUsuarioFilter.push(data[k]);
      }
    }
    this.dataSource = new MatTableDataSource(grupoUsuarioFilter);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorCampanas = grupoUsuarioFilter.length;
  } ///*****
  dataCompose(d) {
    const tamaño = d.length;
    const activo = [];
    const inactivo = [];


    for (let i = 0; i < tamaño; i++) {
      if (d[i].activo === 0) {
        activo.push(d[i]);
      } else if (d[i].activo === 1) {
        inactivo.push(d[i]);
      }
    }
    return [tamaño, activo, inactivo];
  }
  returnOrigin(d) {
    this.dataSource = new MatTableDataSource(d);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorCampanas = d.length;
  }

  activo() {
    const data = this.datosMostrar[1];
    this.svg2.remove();
    this.contadorCampanas = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola1 = this.dataFilter(data);
    this.drawSVG2(hola1[0], hola1[1], data);
    this.barr.attr('fill', this.colors[0]);
  }
  inactivo() {
    const data = this.datosMostrar[2];
    this.svg2.remove();
    this.contadorCampanas = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola = this.dataFilter(data);
    this.drawSVG2(hola[0], hola[1], data);
    this.barr.attr('fill', this.colors[1]);
  }

  handleMouseOver(d, i) {
    this.glove = d3Select.select('#graph2')
      .append('div')
      .attr('class', 'tooltip')
      .attr('id', 'toolTip')
      .style('opacity', 0.8);
  }
  handleMouseOut() {
    document.getElementById('toolTip').remove();
  }
}
