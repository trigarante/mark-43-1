import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FinanzasComponent} from './finanzas.component';
import {CommonModule} from '@angular/common';
import {GruposFinanzasComponent} from './grupos-finanzas/grupos-finanzas.component';
import {EmpresaFinanzasComponent} from './empresa-finanzas/empresa-finanzas.component';
import {SedesFinanzasComponent} from './sedes-finanzas/sedes-finanzas.component';
import {AreasFinanzasComponent} from './areas-finanzas/areas-finanzas.component';
import {SubAreasFinanzasComponent} from './sub-areas-finanzas/sub-areas-finanzas.component';
import {CentroCostoComponent} from './centro-costo/centro-costo.component';

const routes: Routes = [{
  path: '',
  component: FinanzasComponent,
  children: [
    {
      path: 'grupo',
      component: GruposFinanzasComponent,
    },
    {
      path: 'empresas',
      component: EmpresaFinanzasComponent,
    },
    {
      path: 'sede',
      component: SedesFinanzasComponent,
    },
    {
      path: 'area',
      component: AreasFinanzasComponent,
    },
    {
      path: 'sub-area',
      component: SubAreasFinanzasComponent,
    },
    {
      path: 'centro-costo',
      component: CentroCostoComponent,
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class FinanzasRoutingModule {
}
