import {Observable} from 'rxjs';

export interface HistorialLlamadas {
  id: number;
  id_outbound_manuales: number;
  id_ejecutivo: number;
  titulo: string;
  numero: string;
  nombre: string;
  comentario: string;
  comentario_seguimiento: string;
  fecha_registro: Date;
  fecha: Date;
  activo: string;
  id_solicitud: string;
}

export abstract class HistorialLlamadasData {
  abstract get(idSolicitud): Observable<HistorialLlamadas[]>;

  abstract getById(idSolicitud): Observable<HistorialLlamadas>;

  abstract post(solicitud): Observable<HistorialLlamadas>;

  abstract put(idSolicitud, solicitud): Observable<HistorialLlamadas>;
}
