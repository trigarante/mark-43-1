import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BancoComponent} from '../../banco/banco.component';
import {Banco, BancoData} from '../../../../../@core/data/interfaces/catalogos/banco';
import {Router} from '@angular/router';
import {map} from "rxjs/operators";
/** Componente del modal del catálogo para "crear banco" */
@Component({
  selector: 'ngx-banco-create',
  templateUrl: './banco-create.component.html',
  styleUrls: ['./banco-create.component.scss'],
})
export class BancoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idBanco obtenido. */
  @Input() idBanco: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  bancoCreateform: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  banco: Banco;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  bancoEliminado: boolean;
  bancoss: Banco[];
  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BancoComponent>} ref Referencia al diálogo (mensaje).
   * @param {BancoData} bancoService Servicio del componente banco para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BancoComponent>, private bancoService: BancoData,
              private router: Router) {
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarCrearEmpleado() {
    if (this.bancoCreateform.invalid) {
      return;
    }
    this.submitted = true;
    this.bancoService.post(this.bancoCreateform.value).subscribe((result) => {
      this.bancoService.postSocket(this.bancoCreateform.value).subscribe(x => {});
      this.bancoCreateform.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/banco']);
      this.ref.close();
      this.getBanco();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un banco. */
  updateBanco() {
    this.bancoService.getBancoById(this.idBanco).subscribe(data => {
      if (data.activo === 1) {
        this.bancoService.put(this.idBanco, this.bancoCreateform.value).subscribe(result => {
          this.bancoService.postSocket(this.bancoCreateform.value).subscribe(x => {});
          this.bancoCreateform.reset();
          this.ref.close();
          // window.location.reload();
        });
      } else {
        this.bancoEliminado = true;
      }
    });
  }

  /** Función que da de baja un banco, cambiando el atributo activo a 0. */

  /*bajaBanco() {
    this.bancoCreateform.controls['activo'].setValue(0);
    this.bancoService.put(this.idBanco, this.bancoCreateform.value).subscribe( result => {
      this.bancoCreateform.reset();
      this.ref.close();
      window.location.reload();
      // alert('EL BANCO SE HA DADO DE BAJA.');
    });
  }*/

  /** Función que obtiene la información de un banco dependiendo de su ID. */
  getBancoById() {
    this.bancoService.getBancoById(this.idBanco).subscribe(data => {
      this.banco = data;
      this.bancoCreateform.controls['nombre'].setValue(this.banco.nombre);
      this.bancoCreateform.controls['activo'].setValue(this.banco.activo);
    });
  }

  getBanco() {
    this.bancoService.get().subscribe(data => {
      this.bancoss = data;
    });
  }

    /** Función que inicializa el componente y el formulario. */
    ngOnInit() {
      if (this.idTipo === 2 || this.idTipo === 3) {
        this.getBancoById();
      }
      this.bancoCreateform = this.fb.group({
        'nombre': new FormControl('', Validators.compose([Validators.required])),
        'activo': 1,
      });
    }
  }
