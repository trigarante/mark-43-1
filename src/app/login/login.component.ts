import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {JwtResponseData} from '../@core/data/services/login/jwt-response';
import {TokenStorageService} from '../@core/data/services/login/token-storage.service';
import {Global2} from '../@core/data/services/global2';
import {NbToastrService} from '@nebular/theme';
import {environment} from '../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  baseURL: string;
  loginForm: FormGroup;
  isLoggedIn = false;
  isLoginFailed = false;

  constructor(private fb: FormBuilder, private router: Router, protected loginService: JwtResponseData,
              protected tokenStorage: TokenStorageService, private toastrService: NbToastrService, private route: ActivatedRoute) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  // login() {
  //   this.loginService.enviarCredenciales(this.loginForm.value).subscribe(
  //     // data => console.log('Cars: ' + data),
  //     // err => console.error('Ops: ' + err.status)
  //     data => {
  //       this.tokenStorage.saveToken(data.token);
  //       this.isLoginFailed = false;
  //       this.isLoggedIn = true;
  //       // this.roles = this.tokenStorage.getAuthorities();
  //       this.router.navigate(['modulos']);
  //     }, error => {
  //       if (error.status === 401) {
  //         // this.messageService.add({severity: 'error', summary: 'Credenciales Incorrectas'});
  //         console.log('Credenciales incorrectas');
  //         this.router.navigate(['']);
  //       }
  //       if (error.status === 503) {
  //         console.log('no hay servicio');
  //       }
  //     },
  //   );
  // }
  loginGoogle() {
    this.isLoginFailed = true;
    window.location.
      // href = this.baseURL + 'oauth2/authorize/google?redirect_uri=http://pruebas.ahorraseguros.mx/mark43/auth';
      //    href = this.baseURL + 'oauth2/authorize/google?redirect_uri=http://localhost:4200/trigarante2020/auth';
      // href = this.baseURL + 'oauth2/authorize/google?redirect_uri=https://trigarante2020.com/trigarante2020/auth';
     href = this.baseURL + 'oauth2/authorize/google?redirect_uri=https://pruebas.trigarante2020.com/trigarante2020/auth';
  }

  ngOnInit() {
    const log = this.route.snapshot.paramMap.get('log');
    this.loginForm = this.fb.group({
      'usuario': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required),
    });

    if (log !== null) {
      switch (log) {
        case '0':
          this.showToastNot(true, 0);
          break;
        case 'warn':
          this.showToastRerun(true, 0);
          break;
        case 'error':
          alert('Ocurrió un error al iniciar la sesión');
          break;
        case 'activo':
          alert('Iniciaste sesión en algún otro dispositivo');
          break;
      }
    }
  }

  showToastNot(destroyByClick, duration) {
    this.toastrService.danger('!Lo siento, tus credenciales no son validas :( ¡', `Logeo Fallido`, {
      destroyByClick,
      duration,
    });
  }

  showToastRerun(destroyByClick, duration) {
    this.toastrService.warning('!Lo siento, tus CREDENCIALES han Expirado :( ¡', `Expirado`, {
      destroyByClick,
      duration,
    });
  }
}
