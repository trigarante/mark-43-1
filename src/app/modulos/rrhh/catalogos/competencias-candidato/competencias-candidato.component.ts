import {Component, OnInit, ViewChild} from '@angular/core';
import {
  CompetenciaCandidatoData,
  CompetenciasCandidato,
} from '../../../../@core/data/interfaces/catalogos/competencias-candidato';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {CompetenciasCandidatoCreateComponent} from '../modals/competencias-candidato-create/competencias-candidato-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-competencias-candidato',
  templateUrl: './competencias-candidato.component.html',
  styleUrls: ['./competencias-candidato.component.scss'],
})
export class CompetenciasCandidatoComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  competenciaCandidato: CompetenciasCandidato[];
  /** Arreglo para la competencia candidato que se dará de baja. */
  compCand: CompetenciasCandidato;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {CompetenciaCandidatoData} competenciaCandidatoService Servicio del componente
   * competencias-candidato para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private competenciaCandidatoService: CompetenciaCandidatoData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'competenciascandidato');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelCompetenciasCandidato', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCompetenciaCandidato(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Esta función obtiene las competencias de cada candidato y las asigna al arreglo competenciaCandidato. */
  getCompetenciaCandidato(mensajeAct) {
    this.competenciaCandidatoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.competenciaCandidato = [];
      this.competenciaCandidato = data;
      this.dataSource = new MatTableDataSource(this.competenciaCandidato); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Competencia-Candidato`);
  }
  /** Función que obtiene la información de una competencia de candidato dependiendo su ID. */
  getCompetenciaCandidatoById(idCompetenciasCandidato) {
    this.competenciaCandidatoService.getCompetenciaCandidatoById(idCompetenciasCandidato).subscribe( data => {
      this.compCand = data;
    });
  }
  /** Función que inicializa el componente, mandando a llamar a getCompetenciaCandidato para llenar la tabla. */
  crearCompetenciaCandidato() {
    this.dialogoService.open(CompetenciasCandidatoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getCompetenciaCandidato(0);
    });
  }
  /** Función que despliega un modal en el cual se puede actualizar la información de las competencias del candidato. */
  updateCompetenciaCandidato(idCompetenciasCandidato) {
    this.dialogoService.open(CompetenciasCandidatoCreateComponent, {
      context: {
        idTipo: 2,
        idCompetenciasCandidato: idCompetenciasCandidato,
      },
    }).onClose.subscribe(x => {
      this.getCompetenciaCandidato(1);
    });
  }
  /** Función que despliega un modal para dar de baja una competencia de un candidato. */
  bajaCompetenciaCandidato(idCompetenciasCandidato) {
    this.getCompetenciaCandidatoById(idCompetenciasCandidato);
    swal({
      title: '¿Deseas dar de baja esta competencia de candidato?',
      text: 'Una vez dada de baja, no podrás verla de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.compCand['activo'] = 0;
        this.competenciaCandidatoService.put(idCompetenciasCandidato, this.compCand).subscribe( result => {
          this.competenciaCandidatoService.postSocket(
            {idCompetenciasCandidato: idCompetenciasCandidato, banco: this.compCand}).subscribe(x => {});
        });
        swal('¡La competencia de candidato se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getCompetenciaCandidato(0);
        }));
      }
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.competencias.escritura;
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Función que inicializa el componente, mandando a llamar a getCompetenciaCandidato para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.cols = ['descripcion', 'acciones'];
    this.getPermisos();
    this.getCompetenciaCandidato(0);
  }
}
