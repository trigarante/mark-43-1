import {Component, OnInit, ViewChild} from '@angular/core';
import {CandidatoData, Candidatos} from '../../../@core/data/interfaces/rrhh/candidatos';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarCapacitacionComponent} from '../modals/asignar-capacitacion/asignar-capacitacion.component';
import {AsignarCandidatoComponent} from '../modals/asignar-candidato/asignar-candidato.component';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosCandidatoComponent} from '../modals/datos-candidato/datos-candidato.component';
import Swal from 'sweetalert2';
import {BajaPrecandidatoComponent} from '../modals/baja-precandidato/baja-precandidato.component';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {environment} from '../../../../environments/environment';
import swa2 from 'sweetalert2';
import swa from 'sweetalert';
import {PreempleadosData} from '../../../@core/data/interfaces/rrhh/preempleados';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de candidatos.
 */
@Component({
  selector: 'ngx-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.scss'],
})
export class CandidatosComponent implements OnInit {
  cols: any[];
  candidatos: Candidatos[];
  candidato: Candidatos;
  idEtapa: number;
  dataSource: any;
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  inactivo: boolean;
  permisos: any;
  escritura: boolean;
  // Web Sockets
  private stompClient = null;
  private stompClientPreEmpleado = null;
  /** El método constructor únicamente manda a llamar Servicios.
   *
   * En este caso: Servicio de Candidato y Servicio de Diálogos.
   * @param {CandidatoData} candidatoService Servicio del componente candidatos para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes). */
  constructor(private candidatoService: CandidatoData, private preEmpleadoService: PreempleadosData,
              private dialogService: NbDialogService,
              private router: Router,
              private precandidatoService: PrecandidatosData,
              protected motivosService: EstadoRrhhData,
              private toastrService: NbToastrService,
              private solicitudesService: SolicitudesData,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'candidato');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelCandidatos', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCandidato(0);
        }, 500);
      });
    });
    /**-----------PreEmpleado-----------**/
    const socket2 = new SockJS('https://www.mark-43.net:445/preempleados');
    this.stompClientPreEmpleado = Stomp.over(socket2);
    this.stompClientPreEmpleado.connect({}, frame =>  {
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  verDatos(idCandidato) {
    this.dialogService.open(DatosCandidatoComponent, {
      context: {
        idCandidato: idCandidato,
      },
    });
  }

  /** Función que obtiene los candidatos existentes en el servidor. */
  getCandidato(mensajeAct) {
    this.candidatoService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 2 && data.idEstadoRH === 1 ||
        data.idEtapa === 2 && data.idEstadoRH === 16);
    })).subscribe(data => {
      this.candidatos = data;
      this.dataSource = new MatTableDataSource(this.candidatos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (this.candidatos.length === 0) {
        swa2.fire({
          type: 'error',
          title: 'Oops',
          text: 'No tienes candidatos por mostrar',
        });
      }
    }, () => {
        swa2.fire({
          type: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      },
      () => {
        if (mensajeAct === 1 && this.candidatos.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Candidatos`);
        }
      });
  }

  /** Esta función despliega un modal en el cual se le asigna capacitación a un candidato. */
  asignarCapacitacion(idCandidato, idPrecandidato, idEtapa, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarCapacitacionComponent, {
      context: {
        idTipo: 1,
        idPrecandidato: idPrecandidato,
        idCandidato: idCandidato,
        idEtapa: idEtapa,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,
      },
    }).onClose.subscribe(x => {
      this.getCandidato(0);
    });
  }

  /** Función que envia el ID de un candidato y redirecciona la página. */
  asignarPreempleados(idPrecandidato) {
    // @ts-ignore
    swa({
      title: '¿Deseas asignar a Preempleados este candidato?',
      text: 'Una vez asignado, podrás verlo de nuevo en Pre empleados.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        succes: {
          text: 'Asignar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.precandidatoService.activaPrecandidato(idPrecandidato).subscribe(
          result => {
            this.inactivo = true;
          },
          error => {
          },
          () => {
            this.candidatoService.postSocket({idPrecandidato: idPrecandidato}).subscribe(d => {});
            this.preEmpleadoService.postSocket({idPrecandidato: idPrecandidato}).subscribe(d => {});
            // @ts-ignore
            swal('¡Se ha asignado exitosamente a Pre empleado! Redireaccionando a Pre empleado...', {
              icon: 'success',
            }).then((resultado => {
              // this.router.navigate(['/modulos/rrhh/pre-empleados/']);
            }));
          },
        );
      }
    });
  }

  /** Función que actualiza la información del candidato dependiendo de su ID. */
  updateCandidatos(idCandidato, idPrecandidato, fechaIngreso) {
    this.dialogService.open(AsignarCandidatoComponent, {
      context: {
        idTipo: 2,
        idPrecandidato: idPrecandidato,
        idCandidato: idCandidato,
        fechaIngreso: fechaIngreso,
      },
    }).onClose.subscribe(x => {
      this.getCandidato(1);
    });
  }
  /** Función que obtiene la información de candidatos dependiendo de su ID. */
  getCandidatoById(idCandidato) {
    this.candidatoService.getCandidatoById(idCandidato).subscribe(data => {
      this.candidato = data;
      // console.log(this.candidato);
    });
  }

  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }
  /** Función que despliega un modal para dar de baja un precandidato. */
  async bajaCandidato(idPrecandidato) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }

    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await Swal.fire({
      title: '¿Deseas dar de baja este candidato?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada un motivo de baja
      this.dialogService.open(BajaPrecandidatoComponent, {
        context: {
          idPrecandidato: idPrecandidato,
          idEstadoRh: motivo,
        },
      }).onClose.subscribe(tt => {
        this.getCandidato(0);
      });
    }
  }
  /** Función que inicializa el componente, mandando a llamar a getCandidato para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.getMotivosBaja();
    this.cols = ['detalle', 'nombre', 'fechaIngreso', 'sede', 'subarea', 'acciones'];
    this.getPermisos();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/candidatos') {
        this.getCandidato(0);
    //   }
    // }, 1000);
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.candidato.escritura;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
