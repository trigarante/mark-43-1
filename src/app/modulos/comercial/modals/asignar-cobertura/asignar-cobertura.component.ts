import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {CoberturaData} from '../../../../@core/data/interfaces/comerciales/cobertura';
import {TipoCobertura, TipoCoberturaData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-cobertura';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-asignar-cobertura',
  templateUrl: './asignar-cobertura.component.html',
  styleUrls: ['./asignar-cobertura.component.scss'],
})
export class AsignarCoberturaComponent implements OnInit {
  @Input() idProducto: number;
  @Input() idCobertura: number;
  @Input() idTipo: number;
  asignarCoberturaForm: FormGroup;
  submitted: Boolean;
  tipoCobertura: any[];
  tipoCoberturas: TipoCobertura[];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected  coberturaService: CoberturaData, protected tipoCoberturaService: TipoCoberturaData) {
  }

  getTipoCobertura() {
    this.tipoCoberturaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoCoberturas = result;
      this.tipoCobertura = [];
      for (let i = 0; i < this.tipoCoberturas.length; i++) {
        this.tipoCobertura.push({'label': this.tipoCoberturas[i].cobertura, 'value': this.tipoCoberturas[i].id});
      }
    });
  }

  guardarCobertura() {
    this.submitted = true;
    if (this.asignarCoberturaForm.invalid) {
      return;
    }
    this.coberturaService.post(this.asignarCoberturaForm.value).subscribe((result) => {
      this.coberturaService.postSocket(this.asignarCoberturaForm.value).subscribe(x => {});
      this.asignarCoberturaForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/cobertura']);
    });
  }

  getCoberturaById() {
    this.coberturaService.getCoberturaById(this.idCobertura).subscribe(data => {
      this.asignarCoberturaForm.controls['idTipoCobertura'].setValue(data.idTipoCobertura);
      this.asignarCoberturaForm.controls['detalle'].setValue(data.detalle);
    });
  }

  updateCobertura() {
    this.coberturaService.put(this.idCobertura, this.asignarCoberturaForm.value).subscribe(n => {
      this.coberturaService.postSocket(this.asignarCoberturaForm.value).subscribe(x => {});
      this.ref.close();
    });
  }

  dismiss() {
      this.ref.close();
    }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCoberturaById();
    }
    this.getTipoCobertura();
    this.asignarCoberturaForm = this.fb.group({
      'idProducto': this.idProducto,
      'idTipoCobertura': new FormControl('', Validators.required),
      'detalle': new FormControl('', [Validators.required, Validators.minLength(4)]),
      'activo': 1,
    });
  }

}
