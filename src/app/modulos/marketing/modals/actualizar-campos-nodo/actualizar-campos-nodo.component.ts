import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Router} from '@angular/router';
import {GuardadoComponent} from '../guardado/guardado.component';
import {formatDate} from '@angular/common';

@Component({
  selector: 'ngx-actualizar-campos-nodo',
  templateUrl: './actualizar-campos-nodo.component.html',
  styleUrls: ['./actualizar-campos-nodo.component.scss'],
})
export class ActualizarCamposNodoComponent implements OnInit {
  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  /** variable que se puede obtener el nombre de la aseguradora*/
  @Input() nombrenodo: string;
  /** posicion del array de la aseguradora*/
  @Input() posicionnodo: number;
  @Input() tiponombre: string;
  @Input() campo: string;
  @Input() nombre: string;
  pagina: Pagina;
  aseguradoras: any;
  tipodenombre: any;
  seleccion2: any[];
  tipodedato: any;

  modificarDatosForm: FormGroup;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<ActualizarCamposNodoComponent>,
  public dialogService: NbDialogService, private router: Router,
  private paginaService: PaginasData) {
    this.modificarDatosForm = this.fb.group({
      'nombreCampo': new FormControl('', Validators.compose([Validators.required])),
      'valor': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  seleccion() {
    this.seleccion2 = [];

    this.seleccion2.push({'label': 'true', 'value': true});
    this.seleccion2.push({'label': 'false', 'value': false});
  }
  inicio() {
    this.modificarDatosForm.controls['nombreCampo'].setValue(this.campo);
    this.modificarDatosForm.controls['valor'].setValue(this.nombre);
    this.tipodenombre = this.campo;
    this.tipodedato = typeof this.nombre;

    if (this.tipodedato === 'string') {
      // console.log('es string');
    }
    if (this.tipodedato === 'number') {
      // console.log('es numero')
    }
    if (this.tipodedato === 'boolean') {
      // console.log(' es booleano')
    }
  }
  guardarCampos() {

    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      // delete this.aseguradoras.aseguradoras[this.posicionnodo][this.nombrenodo][0][this.campo];
      if (this.modificarDatosForm.value.nombreCampo !== this.campo) {
        delete this.aseguradoras[this.tiponombre].aseguradoras[this.posicionnodo][this.nombrenodo][0][this.campo];
      }
      this.aseguradoras[this.tiponombre].aseguradoras[this.posicionnodo][this.nombrenodo][0][this.modificarDatosForm.value.nombreCampo] =
        this.modificarDatosForm.value.valor;


      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));


      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocketDatosNodo({status: 'ok'}).subscribe(x => {});
        this.ref.close();
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      });
    });
  }


  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.seleccion();
    this.inicio();
  }

}
