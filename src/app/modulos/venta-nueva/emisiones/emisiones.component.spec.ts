import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmisionesComponent } from './emisiones.component';

describe('EmisionesComponent', () => {
  let component: EmisionesComponent;
  let fixture: ComponentFixture<EmisionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmisionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmisionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
