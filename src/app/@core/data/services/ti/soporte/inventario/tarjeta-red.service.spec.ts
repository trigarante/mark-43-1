import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Tarjeta Red Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 14,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'A ESTADO',
    idEstadoInventario: 12,
    idObservacionesInventario: 1,
    folio: '0001TX',
    idMarca: 1,
    fechaRecepcion: '2019-09-24T16:54:46.000+0000',
    fechaSalida: null,
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    origenRecurso: 'propio',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'tarjetaRed',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/tarjetaRed');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/tarjetaRed', '/14');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/tarjetaRed');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/tarjetaRed', '/14/74');
      });
    });
  }
});
