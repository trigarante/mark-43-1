import {Injectable} from '@angular/core';
import {Biometricos, BiometricosData} from '../../../../interfaces/ti/soporte/inventario-soporte/biometricos';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class BiometricosService extends BiometricosData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Biometricos[]> {
    return this.http.get<Biometricos[]>(this.baseURL + 'v1/biometricos');
  }

  post(biometricos: Biometricos): Observable<Biometricos> {
    return this.http.post<Biometricos>(this.baseURL + 'v1/biometricos', biometricos);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'biometricos/saveBiometricos', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajabiometricos/saveBajaBiometricos', json);
  }

  getBiometricosById(idBiometricos): Observable<Biometricos> {
    return this.http.get<Biometricos>(this.baseURL + 'v1/biometricos/' + idBiometricos);
  }

  put(idBiometricos, biometricos: Biometricos): Observable<Biometricos> {
    return this.http.put<Biometricos>(this.baseURL + 'v1/biometricos/' + idBiometricos + '/'
      + sessionStorage.getItem('Empleado') , biometricos);
  }
}
