import {Observable} from 'rxjs';


export class CotizarRequest {
  aseguradora?: string;
  clave?: string;
  cp?: string;
  descripcion?: string;
  descuento?: number;
  edad?: string;
  fechaNacimiento?: string;
  genero?: string;
  marca?: string;
  modelo?: string;
  movimiento?: string;
  paquete?: string;
  servicio?: string;

  constructor(aseguradora: string, clave: string, cp: string, descripcion: string, descuento: number, edad: string,
              fechaNacimiento: string, genero: string, marca: string, modelo: string, movimiento: string,
              paquete: string, servicio: string) {
    this.aseguradora = aseguradora;
    this.clave = clave;
    this.cp = cp;
    this.descripcion = descripcion;
    this.descuento = descuento;
    this.edad = edad;
    this.fechaNacimiento = fechaNacimiento;
    this.genero = genero;
    this.marca = marca;
    this.modelo = modelo;
    this.movimiento = movimiento;
    this.paquete = paquete;
    this.servicio = servicio;
  }
}

export class CotizarResponse {
  Aseguradora: string;
  Cliente: Object;
  Coberturas: Object[];
  CodigoError: Object;
  Cotizacion: Object;
  Descuento: string;
  Emision: Object;
  Pago: Object;
  Paquete: string;
  PeriodicidadDePago: number;
  Vehiculo: Object;
  urlRedireccion: Object;


  constructor(Aseguradora: string, Cliente: Object, Coberturas: Object[], CodigoError: Object, Cotizacion: Object,
              Descuento: string, Emision: Object, Pago: Object, Paquete: string, PeriodicidadDePago: number,
              Vehiculo: Object, urlRedireccion: Object) {
    this.Aseguradora = Aseguradora;
    this.Cliente = Cliente;
    this.Coberturas = Coberturas;
    this.CodigoError = CodigoError;
    this.Cotizacion = Cotizacion;
    this.Descuento = Descuento;
    this.Emision = Emision;
    this.Pago = Pago;
    this.Paquete = Paquete;
    this.PeriodicidadDePago = PeriodicidadDePago;
    this.Vehiculo = Vehiculo;
    this.urlRedireccion = urlRedireccion;
  }
}

export abstract class CotizarData {

  abstract cotizar(c: CotizarRequest): Observable<Object>;

}
