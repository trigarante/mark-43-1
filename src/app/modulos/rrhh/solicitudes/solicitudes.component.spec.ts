import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {SolicitudesComponent} from './solicitudes.component';
import {
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NbDateService, NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {Router, RouterLink, RouterModule} from '@angular/router';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {VacanteData} from '../../../@core/data/interfaces/catalogos/vacante';


describe('SolicitudesComponent', () => {
  let component: SolicitudesComponent;
  let fixture: ComponentFixture<SolicitudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
      ],
      providers: [
        SolicitudesData,
        NbDateService,
        EstadoRrhhData,
        SedeData,
        VacanteData,
        {
          provide: NbDialogService,
          useValue: Document,
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
