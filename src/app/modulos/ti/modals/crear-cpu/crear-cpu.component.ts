import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';

@Component({
  selector: 'ngx-crear-cpu',
  templateUrl: './crear-cpu.component.html',
  styleUrls: ['./crear-cpu.component.scss'],
})
export class CrearCpuComponent implements OnInit {

  userForm: FormGroup;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<CrearCpuComponent> ) {
    this.userForm = this.fb.group({
      'nombreSerie': new FormControl('', Validators.required),
      'procesador': new FormControl('', Validators.required),
      'hdd': new FormControl('', Validators.required),
      'ram': new FormControl('', Validators.compose([Validators.required])),
      'marca': new FormControl('', Validators.compose([Validators.required])),
      'modelo': new FormControl('', Validators.compose([Validators.required])),
      'fechaRecepcion': new FormControl('', Validators.compose([Validators.required])),
    });
  }

  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
  }
}
