import {Component, Input, OnInit} from '@angular/core';
import {Seguimiento, SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import {NbDialogRef} from '@nebular/theme';

@Component({
  selector: 'ngx-datos-seguimiento-practivo',
  templateUrl: './datos-seguimiento-practivo.component.html',
  styleUrls: ['./datos-seguimiento-practivo.component.scss'],
})
export class DatosSeguimientoPractivoComponent implements OnInit {
  @Input() idSeguimiento: number;
  seguimiento: Seguimiento;
  fechaIngreso: Date;
  calificacionRollplay: number;
  idCapacitacion: number;
  idCompetencia: number;
  calificacion: number;
  idCoach: number;
  idSubarea: number;
  nombreCampana: string;
  asistencia: string;
  registro: number;
  idCliente: number;
  prima: number;
  comentarios: string;
  idEtapa: number;
  empleado: string;
  nombreSubarea: string;
  comentariosFaseDos: number;
  idEstadoRRHH: number;
  sede: number;
  nombre: string;

  constructor(protected seguimientoPracticoService: SeguimientoData, protected ref: NbDialogRef<DatosSeguimientoPractivoComponent>) {
  }

  getSeguimientoPracticoById() {
    this.seguimientoPracticoService.getSeguimientoById(this.idSeguimiento).subscribe(data => {
      this.seguimiento = data;
      this.nombre = data.nombre;
      this.calificacionRollplay = data.calificacionRollplay;
      this.calificacion = data.calificacion;
      this.comentarios = data.comentarios;
      this.sede = data.sede;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getSeguimientoPracticoById();
  }

}
