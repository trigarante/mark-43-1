import { Injectable } from '@angular/core';
import {Divisas, DivisasData} from '../../../interfaces/comerciales/catalogo/divisas';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class DivisasService extends DivisasData {
private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Divisas[]> {
    return this.http.get<Divisas[]>(this.baseURL + 'v1/tipoDivisa');
  }

  post(divisa: Divisas): Observable<Divisas> {
    return this.http.post<Divisas>(this.baseURL + 'v1/tipoDivisa', divisa);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'divisas/saveDivisas', json);
  }

  getDivisaById(idDivisa): Observable<Divisas>  {
      return this.http.get<Divisas>(this.baseURL + 'v1/tipoDivisa/' + idDivisa);
    }
  put(idDivisa, divisa: Divisas): Observable<Divisas> {
    return this.http.put<Divisas>(this.baseURL + 'v1/tipoDivisa/' + idDivisa + '/'
      + sessionStorage.getItem('Empleado'), divisa);
  }
}
