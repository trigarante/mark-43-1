import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {ProductoData, ProductoSocios} from '../../../../@core/data/interfaces/comerciales/producto-socios';
import {AsignarProductoComponent} from '../asignar-producto/asignar-producto.component';
import swal from 'sweetalert';
import {DatosRamoComponent} from '../datos-ramo/datos-ramo.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-datos-producto',
  templateUrl: './datos-producto.component.html',
  styleUrls: ['./datos-producto.component.scss'],
})
export class DatosProductoComponent implements OnInit {
  @Input() idProducto: number;
  @Input() idSubramo: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombre: any;
  nombreComercial: any;
  prioridades: any;
  tipoCategoria: any;
  tipoGiro: any;
  /**Variable aux para valores de microservicio*/
  prodcuto: ProductoSocios;
  permisos: any;
  escritura: boolean;

  constructor(protected ref: NbDialogRef<DatosRamoComponent>, private productoService: ProductoData,
              private dialogService: NbDialogService, private route: Router) { }

  ngOnInit() {
    this.getProducto();
    this.getPermisos();
  }

  getProducto() {
    this.productoService.getProductoSociosById(this.idProducto).subscribe(
      result => {
        this.nombre = result.nombre;
        this.alias = result.alias;
        this.descripcion = result.descripcion;
        this.estado = result.estado;
        this.nombreComercial = result.nombreComercial;
        this.prioridades = result.prioridades;
        this.tipoCategoria = result.tipoSubRamo;
        this.tipoGiro = result.tipoRamo;
      },
      );
  }
  uptadeProducto() {
    this.dialogService.open(AsignarProductoComponent, {
      context: {
        idTipo: 2,
        idProducto: this.idProducto,
        idSubramo: this.idSubramo,
      },
    });
  }

  bajaProducto() {
    this.productoService.getProductoSociosById(this.idProducto).subscribe(data => {
      this.prodcuto = data;
    });
    swal({
      title: '¿Deseas dar de baja este Producto?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.prodcuto.activo = 0;
        let productoform: any;
        productoform = {
          'activo': this.prodcuto.activo,
          'id': this.prodcuto.id,
          'idSubRamo': this.prodcuto.idSubRamo,
          'idTipoProducto': this.prodcuto.idTipoProducto,
          'nombre': this.prodcuto.nombre,
          'prioridad': this.prodcuto.prioridad,
        };
        this.productoService.put(this.idProducto, productoform).subscribe(result => {
          this.productoService.postSocket({producto: productoform}).subscribe(x => {});
        });
        swal('¡El Producto se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          /**window.location.reload();*/
          this.dismiss();
        }));
        this.route.navigate(['modulos/comercial/producto']);
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verProducto.escritura;
  }
  dismiss() {
    this.ref.close();
  }

}
