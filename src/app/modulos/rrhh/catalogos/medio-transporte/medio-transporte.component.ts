import {Component, OnInit, ViewChild} from '@angular/core';
import {MedioTransporte, MedioTransporteData} from '../../../../@core/data/interfaces/catalogos/medio-transporte';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MedioTransporteCreateComponent} from '../modals/medio-transporte-create/medio-transporte-create.component';
import {map} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de medio de transporte (catálogo).
 */
@Component({
  selector: 'ngx-medio-transporte',
  templateUrl: './medio-transporte.component.html',
  styleUrls: ['./medio-transporte.component.scss'],
})
export class MedioTransporteComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  medioTransporte: MedioTransporte[];
  /** Arreglo para el medio de transporte que se dará de baja. */
  transporte: MedioTransporte;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {MedioTransporteData} medioTransporteService Servicio del componente medio-transporte para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private medioTransporteService: MedioTransporteData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'mediotransporte');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelMedioTransporte', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getMedioTransporte(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene los medios de transporte y los asigna al arreglo medioTransporte. */
  getMedioTransporte(mensajeAct) {
    this.medioTransporteService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.medioTransporte = [];
      this.medioTransporte = data;
      this.dataSource = new MatTableDataSource(this.medioTransporte); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Función que obtiene la información del medio de transporte dependiendo de su ID. */
  getMedioTransporteById(idMedioTransporte) {
    this.medioTransporteService.getMedioTransporteById(idMedioTransporte).subscribe(data => {
      this.transporte = data;
    });
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Medio-Transporte`);
  }
  /** Función que despliega un modal del componente "MetioTransporteCreateComponent" para poder "crear un medio de transporte". */
  crearMedioTransporte() {
    this.dialogoService.open(MedioTransporteCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getMedioTransporte(0);
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de un medio de transporte. */
  updateMedioTransporte(idMedioTransporte) {
    this.dialogoService.open(MedioTransporteCreateComponent, {
      context: {
        idTipo: 2,
        idMedioTransporte: idMedioTransporte,
      },
    }).onClose.subscribe(x => {
      this.getMedioTransporte(1);
    });
  }

  /** Función que despliega un modal para dar de baja un medio de transporte. */
  bajaMedioTransporte(idMedioTransporte) {
    /*this.dialogoService.open(MedioTransporteCreateComponent, {
      context: {
        idTipo: 3,
        idMedioTransporte: idMedioTransporte,
      },
    });*/
    this.getMedioTransporteById(idMedioTransporte);
    swal({
      title: '¿Deseas dar de baja este medio de transporte?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.transporte['activo'] = 0;
        this.medioTransporteService.put(idMedioTransporte, this.transporte).subscribe( result => {
          this.medioTransporteService.postSocket({idMedioTransporte: idMedioTransporte, transporte: this.transporte}).subscribe(x => {});
        });
        swal('¡El medio de transporte se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getMedioTransporte(0);
        }));
      }
    });
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.medioDeTransporte.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.medioDeTransporte.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  /** Función que inicializa el componente, mandando a llamar a getMedioTransporte para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.cols = ['medio', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getMedioTransporte(0);
  }
}
