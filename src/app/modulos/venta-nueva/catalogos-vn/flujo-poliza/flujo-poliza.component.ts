import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {FlujoPolizaData, FlujoPolizaVn} from '../../../../@core/data/interfaces/ventaNueva/catalogos/flujo-poliza-vn';
import {FlujoPolizaCreateComponent} from '../modals/flujo-poliza-create/flujo-poliza-create.component';
import {environment} from "../../../../../environments/environment";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-flujo-poliza',
  templateUrl: './flujo-poliza.component.html',
  styleUrls: ['./flujo-poliza.component.scss'],
})
export class FlujoPolizaComponent implements OnInit {

  cols: any[];


  descripciones: FlujoPolizaVn[];

  // baja descripcion
  estadoPoliza: FlujoPolizaVn;

  dataSource: any;
  // sockets
  private stompClient = null;

  constructor(private descService: FlujoPolizaData,
              private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vnflujopoliza');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelVnFlujoPoliza', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getFlujoPoliza(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  ngOnInit() {
    this.connect();
    this.getFlujoPoliza(0);
    this.cols = ['estado', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  crearFlujo() {
    this.dialogoService.open(FlujoPolizaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => {
      this.getFlujoPoliza(1);
    });
  }



  showToast() {
    this.toastrService.success('DATOS ACTUALIZADOS!', `Flujo Póliza`);
  }

  // Obtener estadoPolizas
  getFlujoPoliza(mensajeAct) {
    this.descService.get().pipe(map(resultado => {
      return resultado.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.descripciones = [];
      this.descripciones = data;
      this.dataSource = new MatTableDataSource(this.descripciones); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }


  // Obtener ID de estadoPoliza
  getFlujoPolizaById(idFlujo) {
    this.descService.getFlujoPolizaById(idFlujo).subscribe(data => {
      this.estadoPoliza = data;
    });
  }

  // Actualizar información del estadoPoliza
  updateFlujoPoliza(idFlujo) {
    this.dialogoService.open(FlujoPolizaCreateComponent, {
      context: {
        idTipo: 2,
        idFlujo: idFlujo,
      },
    }).onClose.subscribe(x => {
      this.getFlujoPoliza(1);
    });
  }

  // Dar de baja estadoPoliza
  bajaFlujo(idFlujo) {
    this.getFlujoPolizaById(idFlujo);
    swal({
      title: '¿Deseas dar de baja este tipo flujo póliza?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.estadoPoliza['activo'] = 0;
        this.descService.put(idFlujo, this.estadoPoliza).subscribe(result => {
          this.descService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡Este flujo póliza se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getFlujoPoliza(0);
        }));
      }
    });

  }
}
