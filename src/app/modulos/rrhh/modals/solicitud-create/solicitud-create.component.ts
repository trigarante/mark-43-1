import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BolsaTrabajo, BolsaTrabajoData} from '../../../../@core/data/interfaces/catalogos/bolsa-trabajo';
import {Vacante, VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {NbDialogRef} from '@nebular/theme';
import {SolicitudesData, Solicitudes} from '../../../../@core/data/interfaces/rrhh/solicitudes';
import {map} from 'rxjs/operators';
import {Sede, SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {Router} from '@angular/router';
/**
 * Componente del modal "crear solicitud".
 */
@Component({
  selector: 'ngx-solicitud-create',
  templateUrl: './solicitud-create.component.html',
  styleUrls: ['./solicitud-create.component.scss'],
})
export class SolicitudCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idSolicitud obtenido. */
  @Input() idSolicitud: number;
  /**Variable que almacena el idVacante obtenido*/
  @Input() idVacante: number;
  /** Variable que se inicializa con los valores del Microservicio.*/
  solicitudes: Solicitudes;
  /** Variable que permite la inicialización del formulario para crear una solicitud. */
  crearSolicitudForm: FormGroup;
  /** Arreglo auxiliar para bolsasTrabajo. */
  bolsa: any[];
  /** Arreglo que se inicializa con los valores del campo bolsa-trabajo desde el Microservicio. */
  bolsasTrabajo: BolsaTrabajo[];
  /** Arreglo que se inicializa con los valores del campo vacante desde el Microservicio. */
  vacantes: Vacante[];
  /** Arreglo que se inicializa con los valores del campo sede desde el Microservicio.  */
  // sedes: Sede[];
  /** Arreglo auxiliar para vacantes. */
  vacante: any[];
  /** Arreglo auxiliar para sedes. */
  sede: any[];
  /** Arreglo que se inicializa con los valores del campo sede desde el Microservicio.  */
  reclutadores: Empleados[];
  /** Arreglo auxiliar para reclutadores. */
  reclutador: any[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Expresión regular para letras y puntos. */
  letrasPunto: RegExp = /[\\A-Za-zñÑ. ]+/;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9]+/;
  //
  edad: any;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  solicitudEliminada: boolean;

  /**
   * El método constructor manda a llamar Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {BolsaTrabajoData} bolsaTrabajoService Servicio del componente bolsa-trabajo para poder invocar los métodos.
   * @param {VacanteData} vacantesService Servicio del componente vacante para poder invocar los métodos.
   * @param {NbDialogRef<SolicitudCreateComponent>} ref Referencia al diálogo (modal).
   * @param {SolicitudesData} solicitudesService Servicio del componente solicitudes para poder invocar los métodos.
   * @param {SedeData} sedeService Servicio del componente sede para poder invocar los métodos. */
  constructor(private fb: FormBuilder, protected bolsaTrabajoService: BolsaTrabajoData, protected  vacantesService: VacanteData,
              protected ref: NbDialogRef<SolicitudCreateComponent>, protected solicitudesService: SolicitudesData,
              protected sedeService: SedeData, protected empleadosService: EmpleadosData, private router: Router) {
    this.crearSolicitudForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'apellidoPaterno': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'apellidoMaterno': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z].*$')])),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'edad': new FormControl('', Validators.compose([Validators.required, Validators.min(18)])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5), Validators.minLength(5)])),
      'telefono': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(10)])),
      'idReclutador': new FormControl(''),
      // 'idVacante': this.idVacante,
      'idBolsaTrabajo': new FormControl('', Validators.required),
      'estado': 1,
    });
  }

  /** Función que obtiene los datos de la solicitud dependiendo de su ID. */
  getSolicitudById() {
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe( data => {
      this.solicitudes = data;
      this.crearSolicitudForm.controls['nombre'].setValue(this.solicitudes.nombre);
      this.crearSolicitudForm.controls['apellidoPaterno'].setValue(this.solicitudes.apellidoPaterno);
      this.crearSolicitudForm.controls['apellidoMaterno'].setValue(this.solicitudes.apellidoMaterno);
      this.crearSolicitudForm.controls['edad'].setValue(this.solicitudes.edad);
      this.crearSolicitudForm.controls['correo'].setValue(this.solicitudes.correo);
      this.crearSolicitudForm.controls['telefono'].setValue(this.solicitudes.telefono);
      this.crearSolicitudForm.controls['idBolsaTrabajo'].setValue(this.solicitudes.idBolsaTrabajo);
      this.crearSolicitudForm.controls['cp'].setValue(this.solicitudes.cp);
      this.crearSolicitudForm.controls['idReclutador'].setValue(this.solicitudes.idReclutador);
    });
  }

  /** Función para obtener la bolsa de trabajo, dichos valores se almacenan en el arreglo "bolsa". */
  getBolsaTrabajo() {
    this.bolsaTrabajoService.getBolsaTrabajo().pipe(map(result => {
      return result.filter( data => data.activo === 1);
    })).subscribe(data => {
      this.bolsasTrabajo = data;
      this.bolsa = [];
      for (let i = 0; i < this.bolsasTrabajo.length; i++) {
        this.bolsa.push({'label': this.bolsasTrabajo[i].nombre, 'value': this.bolsasTrabajo[i].id});
      }
    });
  }

  /** Función para obtener las vacantes disponibles y guardarlas en el arreglo "vacante" */
  getVacantes() {
    this.vacantesService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.vacantes = data;
      this.vacante = [];
      for (let i = 0; i < this.vacantes.length; i++) {
        this.vacante.push({'label': this.vacantes[i].nombre, 'value': this.vacantes[i].id});
      }
    });
  }

  /** Función para obtener las sedes disponibles y guardarlas en el arreglo "sedes" */
  /*getSede() {
    this.sedeService.get().subscribe(data => {
      this.sedes = data;
      this.sede = [];
      for (let i = 0; i < this.sedes.length; i++) {
        this.sede.push({'label': this.sedes[i].nombre, 'value': this.sedes[i].id});
      }
    });
  }*/

  /** Función que obtiene el reclutador de la solicitud. */
  getReclutador() {
    this.empleadosService.get().pipe(map(result => {
      return result.filter(data => data.idPuesto === 30);
    })).subscribe(data => {
      this.reclutadores = data;
      this.reclutador = [];
      for (let i = 0; i < this.reclutadores.length; i++) {
        this.reclutador.push({'label': this.reclutadores[i].nombre + ' ' + this.reclutadores[i].apellidoPaterno
            + ' ' + this.reclutadores[i].apellidoMaterno + ' - ' + this.reclutadores[i].id, 'value': this.reclutadores[i].id});
      }
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario y realiza un refresh de la ventana. */
  crearSolicitud() {
    this.submitted = true;
    if (this.crearSolicitudForm.invalid) {
      return;
    }
    this.inactivo = true;
    // Realizando casting de string de edad a number
    let edad;
    edad = this.crearSolicitudForm.value.edad;
    edad = Number(edad);
    this.crearSolicitudForm.value.edad = edad;
    this.crearSolicitudForm.value.idVacante = this.idVacante;
    this.solicitudesService.post(this.crearSolicitudForm.value).subscribe((result) => {
      this.solicitudesService.postSocket(this.crearSolicitudForm.value).subscribe(data => {});
      this.crearSolicitudForm.reset();
      this.ref.close();
      this.router.navigate(['modulos/rrhh/solicitudes']);
    });
  }

  /** Función que actualiza los datos de una solicitud. */
  actualizarSolicitud() {
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe( data => {
      if (Number(data.estado) === 1 && data.idEtapa === 0) {
        this.inactivo = true;
        // Realizando casting de string de edad a number
        let edad;
        edad = this.crearSolicitudForm.value.edad;
        edad = Number(edad);
        this.crearSolicitudForm.value.edad = edad;
        this.crearSolicitudForm.value.idVacante = this.idVacante;
        this.solicitudesService.updateSolicitud(this.idSolicitud, this.crearSolicitudForm.value).subscribe(result => {
          this.solicitudesService.postSocket(this.crearSolicitudForm.value).subscribe(d => {});
          this.crearSolicitudForm.reset();
          this.ref.close();
        });
      }else {
        this.solicitudEliminada = true;
      }
    });

  }

  /** Función que manda a llamar a los getters al inicializar el componente. */
  ngOnInit() {
    this.getBolsaTrabajo();
    this.getVacantes();
    this.getReclutador();
    if (this.idTipo === 1) {
      this.getSolicitudById();
    }
  }
}
