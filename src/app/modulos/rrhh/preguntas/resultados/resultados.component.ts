// import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
// import {ContadorService} from '../../../../@core/data/services/rrhh/contador.service';
// import {AreasPreguntasRespuestasInterface} from '../../../../@core/data/interfaces/rrhh/areas-preguntas-respuestas-interface';
// import {RespuestasService} from '../../../../@core/data/services/rrhh/respuestas.service';
// import {DateAdapter, MAT_DATE_FORMATS, MatDatepickerInputEvent, MatInput} from '@angular/material';
// import {APP_DATE_FORMATS, AppDateAdapter} from '../generar-preguntas/date.adapter';
// import {AreasService} from '../../../../@core/data/services/rrhh/areas.service';
// import {PreguntasService} from '../../../../@core/data/services/rrhh/preguntas.service';
// // import {ExcelService} from '../../../../@core/data/services/rrhh/excel.service';
// import {FormBuilder, FormGroup, Validators} from '@angular/forms';
//
// @Component({
//   selector: 'ngx-resultados',
//   templateUrl: './resultados.component.html',
//   styleUrls: ['./resultados.component.scss'],
//   providers: [
//     {
//       provide: DateAdapter, useClass: AppDateAdapter,
//     },
//     {
//       provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS,
//     },
//   ],
// })
// export class ResultadosComponent implements OnInit {
//   @ViewChild('fechaFin', {read: MatInput}) fechaFin: MatInput;
//   @ViewChild('fechaInicio', {read: MatInput}) fechaInicio: MatInput;
//
//   areasPreguntasRespuestas: AreasPreguntasRespuestasInterface[] = [];
//   test: any;
//   exportar: any = [];
//   respuestafin = '';
//   total: number[] = [];
//   resultadosForm: FormGroup;
//   minDate: Date = new Date();
//   submitted = false;
//
//   constructor(private areasService: AreasService, private preguntasService: PreguntasService, private respuestaService: RespuestasService,
//               private contadorService: ContadorService, private excelService: ExcelService, private formBuilder: FormBuilder) {
//   }
//
//   ngOnInit() {
//     this.resultadosForm = this.formBuilder.group({
//       fechaInicio: ['', Validators.required],
//       fechaFin: ['', Validators.required],
//     });
//   }
//   get f() { return this.resultadosForm.controls; }
//   @Output()
//   dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
//
//   filtrarFechas() {
//     this.minDate = this.resultadosForm.controls['fechaInicio'].value;
//   }
//
//   exportAsXLSX(): void {
//     if (this.resultadosForm.invalid) {
//       return;
//     }
//     this.areasPreguntasRespuestas.forEach(x => {
//       x.respuesta.forEach(y => {
//         this.respuestafin += y.respuestas + ': ' + y.contador + ',   ';
//       });
//       this.exportar.push({'area': x.area.nombre, 'pregunta': x.pregunta.pregunta, 'respuesta': this.respuestafin});
//       this.respuestafin = '';
//     });
//     this.excelService.exportAsExcelFile(this.exportar, 'sample');
//   }
//
//   filtrarPreguntas(fechaInicio: string, fechaFin: string) {
//     this.areasPreguntasRespuestas = [];
//     this.areasService.getAreas().subscribe(a => {
//       a.forEach(cicloAreas => {
//         // console.log(cicloAreas);
//         this.preguntasService.getPreguntasByAreabyFecha(cicloAreas.id, fechaInicio, fechaFin).subscribe(cicloPreguntas => {
//           cicloPreguntas.forEach(preguntas => {
//             // console.log(cicloAreas.nombre + '=>' + preguntas.pregunta);
//             this.respuestaService.getRespuestaByPregunta(preguntas.id).subscribe(respuestas => {
//               // contador
//               respuestas.forEach(c => {
//                 this.contadorService.getContadorByRespuesta(c.id).subscribe(y => {
//                   c.contador = y;
//                 });
//               });
//               this.areasPreguntasRespuestas.push({
//                 area: cicloAreas, pregunta: preguntas, respuesta: respuestas,
//               });
//             });
//           });
//         });
//       });
//     });
//   }
//
//   onSubmit() {
//     this.submitted = true;
//     // stop here if form is invalid
//     if (this.resultadosForm.invalid) {
//       return;
//     }
//     this.reset();
//   }
//   reset() {
//     this.fechaInicio.value = '';
//     this.fechaFin.value = '';
//   }
// }
