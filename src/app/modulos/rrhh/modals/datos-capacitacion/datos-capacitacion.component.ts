import {Component, Input, OnInit} from '@angular/core';
import {Capacitacion, CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import {NbDialogRef} from '@nebular/theme';
import {CapacitacionComponent} from '../../capacitacion/capacitacion.component';

@Component({
  selector: 'ngx-datos-capacitacion',
  templateUrl: './datos-capacitacion.component.html',
  styleUrls: ['./datos-capacitacion.component.scss'],
})
export class DatosCapacitacionComponent implements OnInit {
  @Input() idCapacitacion: number;
  capacitacion: Capacitacion;
  idCandidato: number;
  calificacion: number;
  idCompetenciaCanditato: number;
  idCalificacionCompetencia: number;
  calificacionCompetencia: string;
  calificacionExamen: number;
  comentarios: string;
  idCapacitador: number;
  asistencias: JSON;
  fechaIngreso: Date;
  fechaRegistro: Date;
  idEtapa: number;
  idEstado: number;
  idPrecandidato: number;
  competenciaCandidato: string;
  competencias: string;
  nombre: string;
  sede: number;
  idCompetencia: number;


  constructor(protected capacitacionService: CapacitacionData,  protected ref: NbDialogRef<CapacitacionComponent>) {
  }

  getCapacitacionById() {
    this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(data => {
      this.capacitacion = data;
      this.nombre = data.nombre;
      this.calificacion = data.calificacion;
      // this.idCompetencia = data.idCompetencia;
      this.calificacionCompetencia = data.competencias;
      this.comentarios = data.comentarios;
      this.sede = data.sede;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getCapacitacionById();
  }

}
