import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Categoria, CategoriaData} from '../../../../../@core/data/interfaces/comerciales/catalogo/categoria';
import {Router} from '@angular/router';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';

@Component({
  selector: 'ngx-categoria-create',
  templateUrl: './categoria-create.component.html',
  styleUrls: ['./categoria-create.component.scss'],
})
export class CategoriaCreateComponent implements OnInit {
  @Input() idCategoria: number;
  @Input() idTipo: number;
  categoriaCreateForm: FormGroup;
  submitted: Boolean;
  categoria: Categoria;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected categoriaService: CategoriaData) {
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  guardarCategoria() {
    this.submitted = true;
    if (this.categoriaCreateForm.invalid) {
      return;
    }
    this.categoriaService.post(this.categoriaCreateForm.value).subscribe((result) => {
      this.categoriaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.categoriaCreateForm.reset();
      this.ref.close();
    });
  }

  getCategoriaById() {
    this.categoriaService.getCategoriaById(this.idCategoria).subscribe(data => {
      this.categoria = data;
      this.categoriaCreateForm.controls['regla'].setValue(this.categoria.regla);
      this.categoriaCreateForm.controls['tipo'].setValue(this.categoria.tipo);
    });
  }

  updateCategoria() {
    this.categoriaService.put(this.idCategoria, this.categoriaCreateForm.value).subscribe(result => {
      this.categoriaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.categoriaCreateForm.reset();
      this.ref.close();
    });
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getCategoriaById();
    }
    this.categoriaCreateForm = this.fb.group({
      'tipo': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'regla': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'activo': 1,
    });
  }
}
