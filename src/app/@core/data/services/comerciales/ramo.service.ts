import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Ramo, RamoData} from '../../interfaces/comerciales/ramo';

@Injectable({
  providedIn: 'root',
})
export class RamoService extends RamoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Ramo[]> {
    return this.http.get<Ramo[]>(this.baseURL + 'v1/ramo');
  }

  post(ramo: Ramo): Observable<Ramo> {
    return this.http.post<Ramo>(this.baseURL + 'v1/ramo', ramo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'ramo/saveRamo/1', json);
  }

  getRamoById(idRamo): Observable<Ramo> {
    return this.http.get<Ramo>(this.baseURL + 'v1/ramo/' + idRamo);
  }
  put(idRamo, ramo: Ramo): Observable<Ramo> {
    return this.http.put<Ramo>(this.baseURL + 'v1/ramo/' + idRamo + '/' + sessionStorage.getItem('Empleado'), ramo);
  }
}
