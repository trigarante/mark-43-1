import { Injectable } from '@angular/core';
import { AppSession } from './sessions/app.session';
import {AppService} from './drive/app.service';

@Injectable()
export class AppContext {

    constructor(
        private appService: AppService,
        private appSession: AppSession,
    ) {

    }

    get Repository(): AppService {
        return this.appService;
    }
    get Session(): AppSession {
        return this.appSession;
    }
}
