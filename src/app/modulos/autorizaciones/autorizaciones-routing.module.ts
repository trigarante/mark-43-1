import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AutorizacionesComponent} from './autorizaciones.component';

const routes: Routes = [{
  path: '',
  component: AutorizacionesComponent,
  children: [
    {},
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class AutorizacionesRoutingModule {
}
