import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {TipoPagoComponent} from '../../tipo-pago/tipo-pago.component';
import {TipoPagoData, TipoPagoVn} from '../../../../../@core/data/interfaces/ventaNueva/catalogos/tipo-pago-vn';

@Component({
  selector: 'ngx-tipo-pago-create',
  templateUrl: './tipo-pago-create.component.html',
  styleUrls: ['./tipo-pago-create.component.scss'],
})
export class TipoPagoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idPago obtenido. */
  @Input() idPago: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  PagoCreateform: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  Pago: TipoPagoVn;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;


  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BancoComponent>} ref Referencia al diálogo (mensaje).
   * @param {TipoPagoVnData} PagoService Servicio del componente banco para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<TipoPagoComponent>, private PagoService: TipoPagoData,
              private router: Router) { }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoPago() {
    if (this.PagoCreateform.invalid) {
      return;
    }
    this.submitted = true;
    this.PagoService.post(this.PagoCreateform.value).subscribe((result) => {
      this.PagoService.postSocket(this.PagoCreateform.value).subscribe(x => {});
      this.PagoCreateform.reset();
      this.router.navigate(['/modulos/venta-nueva/catalogos/tipopago']);
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un Tipo Pago. */
  updateTipoPago() {
    this.PagoService.put(this.idPago, this.PagoCreateform.value).subscribe( result => {
      this.PagoService.postSocket(this.PagoCreateform.value).subscribe(x => {});
      this.PagoCreateform.reset();
      this.ref.close();
      // window.location.reload();
    });
  }


  /** Función que obtiene la información de un Pago dependiendo de su ID. */
  getPagoById() {
    this.PagoService.getTipoPagoById(this.idPago).subscribe(data => {
      this.Pago = data;
      this.PagoCreateform.controls['cantidadPagos'].setValue(this.Pago.cantidadPagos);
      this.PagoCreateform.controls['tipoPago'].setValue(this.Pago.tipoPago);
      this.PagoCreateform.controls['activo'].setValue(this.Pago.activo);
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getPagoById();
    }
    this.PagoCreateform = this.fb.group( {
      'cantidadPagos': new FormControl('', Validators.compose([Validators.required])),
      'tipoPago': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }
}
