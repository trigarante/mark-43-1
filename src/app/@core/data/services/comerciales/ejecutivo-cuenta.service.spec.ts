import { getTestBed, TestBed } from '@angular/core/testing';
import { EjecutivoCuentaService } from './ejecutivo-cuenta.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('EjecutivoCuenta servicio', () => {
  let injector: TestBed;
  let service: EjecutivoCuentaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;

  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    alias: 'testeo',
    idSocio: 1,
    nombre: 'testeo',
    apellidoPaterno: 'testeo',
    apellidoMaterno: 'testeo',
    telefono: '5523654870',
    ext: '22',
    celular: '5534677203',
    activo: 0,
    idEstadoSocio: 1,
    email: 'testeo@testeo.com',
    puesto: 'testeo',
    direccion: 'testeo',
    favorito: 1,
  },
    {
      id: 'contacto',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EjecutivoCuentaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EjecutivoCuentaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/ejecutivos-cuentas');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/ejecutivos-cuentas', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/ejecutivos-cuentas');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/ejecutivos-cuentas', '/1/74');
      });
    });
  }
});
