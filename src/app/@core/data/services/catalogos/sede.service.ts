import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Sede, SedeData} from '../../interfaces/catalogos/sede';

@Injectable()
export class SedeService extends SedeData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Sede[]> {
    return this.http.get<Sede[]>(this.baseURL + 'v1/sedes');
  }

  post(sede: Sede): Observable<Sede> {
    return this.http.post<Sede>(this.baseURL + 'v1/sedes', sede);
  }

  getSedeById(idSede): Observable<Sede> {
    return this.http.get<Sede>(this.baseURL + 'v1/sedes/' + idSede);
  }

  put(idSede, sede: Sede): Observable<Sede> {
    return this.http.put<Sede>(this.baseURL + 'v1/sedes/' + idSede, sede);
  }
}
