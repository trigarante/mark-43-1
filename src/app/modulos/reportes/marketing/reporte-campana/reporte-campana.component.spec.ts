import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteCampanaComponent } from './reporte-campana.component';

describe('ReporteCampanaComponent', () => {
  let component: ReporteCampanaComponent;
  let fixture: ComponentFixture<ReporteCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
