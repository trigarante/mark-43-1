import {Component, Input, OnInit} from '@angular/core';
import {
  EstadoSolicitud,
  EstadoSolicitudData,
} from '../../../../@core/data/interfaces/ventaNueva/catalogos/estado-solicitud';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {SolicitidesVnComponent} from '../../solicitides-vn/solicitides-vn.component';
import {Solicitudes, SolicitudesVNData} from '../../../../@core/data/interfaces/ventaNueva/solicitudes';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-estado-solicitud',
  templateUrl: './estado-solicitud.component.html',
  styleUrls: ['./estado-solicitud.component.scss'],
})
export class EstadoSolicitudComponent implements OnInit {
  @Input() idSolicitud: number;
  @Input() idCotizacionAli: number;
  @Input() idEmpleado: number;
  solicitudesvn: Solicitudes;
  estadoSolcitudCreateForm: FormGroup;
  estadoSolicitud: EstadoSolicitud[];
  estadoSolicitudAny: any[];

  constructor(protected estadoSolicitudService: EstadoSolicitudData,
              private fb: FormBuilder, protected ref: NbDialogRef<SolicitidesVnComponent>,
              private router: Router, protected solicitudesVentaNuevaService: SolicitudesVNData) {
  }

  dismiss() {
    this.ref.close();
  }

  actualizarEstadoSolicitud() {
    this.solicitudesVentaNuevaService.put(this.idSolicitud, this.estadoSolcitudCreateForm.value).subscribe(resul => {
      this.estadoSolcitudCreateForm.reset();
      this.ref.close();
    });
  }

  getEstadoSolicitudById() {
    this.solicitudesVentaNuevaService.getSolicitudesById(this.idSolicitud).subscribe(data => {
      this.solicitudesvn = data;
      this.estadoSolcitudCreateForm.controls['idEstadoSolicitud'].setValue(this.solicitudesvn.idEstadoSolicitud);
      this.estadoSolcitudCreateForm.controls['comentarios'].setValue(this.solicitudesvn.comentarios);
    });
  }

  getEstadoSolicitud() {
    this.estadoSolicitudService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1 && result.id > 2);
    })).subscribe(result => {
      this.estadoSolicitud = result;
      this.estadoSolicitudAny = [];
      for (let i = 0; i < this.estadoSolicitud.length; i++) {
        this.estadoSolicitudAny.push({'label': this.estadoSolicitud[i].estado, 'value': this.estadoSolicitud[i].id});
      }
    });
  }

  ngOnInit() {
    this.getEstadoSolicitudById();
    this.getEstadoSolicitud();
    this.estadoSolcitudCreateForm = this.fb.group({
      'idSolicitud': this.idSolicitud,
      'idCotizacionAli': this.idCotizacionAli,
      'idEmpleado': this.idEmpleado,
      'idEstadoSolicitud': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
    });
  }

}
