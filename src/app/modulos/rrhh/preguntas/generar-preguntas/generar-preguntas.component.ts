import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AreasInterface} from '../../../../@core/data/interfaces/rrhh/areas-interface';
import {AreasService} from '../../../../@core/data/services/rrhh/areas.service';
import {DateAdapter, MAT_DATE_FORMATS, MatInput} from '@angular/material';
import {APP_DATE_FORMATS, AppDateAdapter} from './date.adapter';
import {SedesInterface} from '../../../../@core/data/interfaces/rrhh/sedes-interface';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';


@Component({
  selector: 'ngx-generar-preguntas',
  templateUrl: './generar-preguntas.component.html',
  styleUrls: ['./generar-preguntas.component.scss'],
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS,
    },
  ],
})
export class GenerarPreguntasComponent implements OnInit {
  @ViewChild('input', {read: MatInput}) input: MatInput;
  @ViewChild('inputElement', {read: MatInput}) inputElemet: MatInput;
  @ViewChild('inputPreg', {read: MatInput}) inputPreg: MatInput;
  submitted: boolean;
  reset() {
    this.input.value = '';
    this.inputElemet.value = '';
    this.inputPreg.value = '';
  }
  selectedArea: number;
  areas: AreasInterface[];
  inputControl = new FormControl('', [Validators.required]);
  title: any;
  private preguntas: any;
  sedes: SedesInterface[];
  selectedidSede: number;
  preguntaFormGroup: FormGroup;


  constructor(private areasService: AreasService, private sedesService: SedeData,
              private formBuilder: FormBuilder) {
    this.sedesService.get().subscribe(s => {
      this.sedes = s;
    });
    this.preguntas = this.areasService.getPreguntas().subscribe(y => this.preguntas = y);
  }

  getAreasBySede() {
    this.areasService.getAreasByIdSede(this.selectedidSede).subscribe(x => this.areas = x);
  }

  agregarPreg(value: string, input: string, pic1: string) {
    if (this.preguntaFormGroup.invalid) {
      return;
    }
    const pregunta = {
      id: 0,
      pregunta: value,
      fechaInicioVigencia: new Date(input),
      fechaFinVigencia: new Date(pic1),
      idArea: this.selectedArea,
    };
    pregunta.fechaFinVigencia.setHours(22, 59, 59),
      this.areasService.addQuestion(pregunta).subscribe(z => this.preguntas = z);
    this.inputControl.reset();
    this.selectedidSede = 0;
    this.preguntas = null;
    this.areas = null;
    this.preguntaFormGroup = this.formBuilder.group({
      sedeControl: ['', Validators.required],
      areaControl: ['', Validators.required],
      preguntaControl: ['', Validators.required],
      respuestaControl: ['', Validators.required],
      radioControl: ['', Validators.required],
      colorControl: ['', Validators.required],
    });
    this.reset();
  }
  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    this.preguntaFormGroup = this.formBuilder.group({
      'sede': new FormControl('', Validators.compose([Validators.required])),
      'area': new FormControl('', Validators.compose([Validators.required])),
      'pregunta': new FormControl('', Validators.compose([Validators.required])),
      'fechaI': new FormControl('', Validators.compose([Validators.required])),
      'fechaF': new FormControl('', Validators.compose([Validators.required])),

    });

  }

  onSubmit(agregarPreg1: void) {
    this.submitted = true;
    // stop here if form is invalid
    // stop here if form is invalid
    if (this.preguntaFormGroup.invalid) {
      return;
    }
    this.reset();
  }
}
