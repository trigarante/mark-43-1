import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ThemeModule} from '../../@theme/theme.module';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {
  NbAlertModule,
  NbDatepickerModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbTooltipModule,
} from '@nebular/theme';
import {DialogModule, DropdownModule, FileUploadModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
} from '@angular/material';
import { PruebasComponent} from './pruebas.component';
import { PruebasRoutingModule } from './pruebas-routing.module';
import { EtapasModulosService } from '../../@core/data/services/catalogos/etapasModulos.service';
import { UserService } from '../../@core/data/users.service';



@NgModule({
  declarations: [
    PruebasComponent,
  ],
  imports: [
    CommonModule,
    PruebasRoutingModule,
    ThemeModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  entryComponents: [

  ],
  providers: [
    EtapasModulosService,
    UserService,
  ],
})
export class PruebasModule {
}
