import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {Router} from '@angular/router';
import {
  TipoCometencia,
  TipoCompetenciaData,
} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-cometencia';

@Component({
  selector: 'ngx-tipo-competencia-create',
  templateUrl: './tipo-competencia-create.component.html',
  styleUrls: ['./tipo-competencia-create.component.scss'],
})
export class TipoCompetenciaCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTipoCompetencia: number;
  tipoCompetenciaCreateForm: FormGroup;
  submitted: Boolean;
  tipoCompetencia: TipoCometencia;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoCompetenciaService: TipoCompetenciaData) {
  }

  guardarTipoCompetencia() {
    this.submitted = true;
    if (this.tipoCompetenciaCreateForm.invalid) {
      return;
    }
    this.tipoCompetenciaService.post(this.tipoCompetenciaCreateForm.value).subscribe((result) => {
      this.tipoCompetenciaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoCompetenciaCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  updateTipoCompetencia() {
    this.tipoCompetenciaService.put(this.idTipoCompetencia, this.tipoCompetenciaCreateForm.value).subscribe(result => {
      this.tipoCompetenciaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoCompetenciaCreateForm.reset();
      this.ref.close();
    });
  }

  getTipoCompetenciaById() {
    this.tipoCompetenciaService.getTipoCompetenciaById(this.idTipoCompetencia).subscribe(data => {
      this.tipoCompetencia = data;
      this.tipoCompetenciaCreateForm.controls['tipo'].setValue(this.tipoCompetencia.tipo);
    });
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTipoCompetenciaById();
    }
    this.tipoCompetenciaCreateForm = this.fb.group({
      'tipo': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }

}
