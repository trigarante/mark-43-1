import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-autorizaciones',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AutorizacionesComponent {
}
