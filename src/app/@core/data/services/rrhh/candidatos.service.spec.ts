import {getTestBed, TestBed} from '@angular/core/testing';
import {CandidatosService} from './candidatos.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';

fdescribe('CandidatosService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  let service: CandidatosService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = [{
    id: 39,
    calificacionPsicometrico: true,
    calificacionExamen: 10.0,
    comentarios: 'MUY MUY BUENO',
    fechaIngreso: '2019-05-03T22:38:40.000+0000',
    idPrecandidato: 52,
  },
    {
      id: 'candidatos',
    }];
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CandidatosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(CandidatosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/candidato/74');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/candidato/get-by-id', '/34');
      });  /*
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/candidatos');
      }); */
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/candidato/update', '/34/74');
      });
    });
  }
});
