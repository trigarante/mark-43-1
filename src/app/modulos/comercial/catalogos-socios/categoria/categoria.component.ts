import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {Categoria, CategoriaData} from '../../../../@core/data/interfaces/comerciales/catalogo/categoria';
import {CategoriaCreateComponent} from '../modals/categoria-create/categoria-create.component';
import {environment} from '../../../../../environments/environment';
import {Router} from "@angular/router";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss'],
})
export class CategoriaComponent implements OnInit {
  tipoCategoria: Categoria[];
  cols: any[];
  tipoCategorias: Categoria;
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private tipoCategoriaService: CategoriaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'categoria');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelCategoria', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCategoria(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getCategoria(mensajeAct) {
    this.tipoCategoriaService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.tipoCategoria = data;
      this.dataSource = new MatTableDataSource(this.tipoCategoria); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      if (mensajeAct === 1) {
        this.showToast();
      }
    });  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Categoría`);
  }

  crearCategoria() {
    this.dialogService.open(CategoriaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getCategoria(0));
  }

  updateCategorias(idCategoria) {
    this.dialogService.open(CategoriaCreateComponent, {
      context: {
        idTipo: 2,
        idCategoria: idCategoria,
      },
    }).onClose.subscribe( x => this.getCategoria(1));
  }

  bajaCategoria(idCategoria) {
    this.getTipoConeviosById(idCategoria);
    swal({
      title: '¿Deseas dar de baja esta Categoría?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoCategorias['activo'] = 0;
        this.tipoCategoriaService.put(idCategoria, this.tipoCategorias).subscribe( result => {
          this.tipoCategoriaService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡La categoría se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getCategoria(0);
        }));
      }
    });
  }

  getTipoConeviosById(idCategoria) {
    this.tipoCategoriaService.getCategoriaById(idCategoria).subscribe(data => {
      this.tipoCategorias = data;
    });
  }

  ngOnInit() {
    this.connect();
    this.getCategoria(0);
    this.cols = ['tipo', 'regla', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.categoria.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.categoria.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
