import {Observable} from 'rxjs';

export interface FasesTareas {
  idFaseTarea: number;
  idTipoTarea: number;
  idSubarea: number;
  fase: number;
  descripcion: string;
}

export abstract class FasesTareasData {
  abstract get(): Observable<FasesTareas[]>;
}
