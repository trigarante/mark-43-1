import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {CotizacionesComponent} from './cotizaciones.component';
import {Ejemplo1Component} from './ejemplo1/ejemplo1.component';

const routes: Routes = [{
  path: '',
  component: CotizacionesComponent,
  children: [
    {
      path: 'ejemplo1',
      component: Ejemplo1Component,
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CotizacionesRoutingModule {
}
