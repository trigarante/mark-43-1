import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {BolsaTrabajoService} from './bolsa-trabajo.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Bolsa trabajo service', () => {
  let injector: TestBed;
  let service: BolsaTrabajoService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 20,
    nombre: 'TRABAJO TEST',
    tipo: 'WEB',
    activo: 0,
  },
    {
      id: 'bolsatrabajo',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [BolsaTrabajoService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(BolsaTrabajoService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/bolsa-trabajo');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/bolsa-trabajo', '/20');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/bolsa-trabajo');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/bolsa-trabajo', '/20/74');
      });
    });
  }
})
