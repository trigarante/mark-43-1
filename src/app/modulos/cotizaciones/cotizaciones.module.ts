import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CotizacionesRoutingModule} from './cotizaciones-routing.module';
import {CotizacionesComponent} from './cotizaciones.component';
import { Ejemplo1Component } from './ejemplo1/ejemplo1.component';

import {MatCardModule, MatDividerModule, MatInputModule, MatSelectModule, MatTabsModule} from "@angular/material";
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [CotizacionesComponent, Ejemplo1Component],
  imports: [
    CommonModule,
    CotizacionesRoutingModule,
    MatSelectModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  entryComponents: [],
})
export class CotizacionesModule {
}
