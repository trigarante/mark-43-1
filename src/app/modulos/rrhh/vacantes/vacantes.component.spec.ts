import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {VacantesComponent} from './vacantes.component';
import {
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatPaginator,
  MatPaginatorModule, MatSort, MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {VacanteData} from '../../../@core/data/interfaces/catalogos/vacante';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
describe('VacantesComponent', () => {
  let component: VacantesComponent;
  let fixture: ComponentFixture<VacantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VacantesComponent],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
      ],
      providers: [
        VacanteData,
        SedeData,
        {
          provide: NbDialogService,
          useValue: Document,
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacantesComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
