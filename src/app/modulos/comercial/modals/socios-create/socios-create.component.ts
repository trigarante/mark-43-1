import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {SociosComercial, SociosData} from '../../../../@core/data/interfaces/comerciales/socios-comercial';
import {RadioButton} from 'primeng/primeng';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'ngx-socios-create',
  templateUrl: './socios-create.component.html',
  styleUrls: ['./socios-create.component.scss'],
})
export class SociosCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idSocio: number;
  @ViewChild('radioSi')
  radioSi: RadioButton;
  @ViewChild('radioNo')
  radioNo: RadioButton;
  /**Variable que desactiva el boton Guardar al detonar put/post*/
  activo: boolean;
  /**Variable que almacena la prioridad*/
  prioridadVal: any[];
  estadoSoc: string;
  sociosCreateForm: FormGroup;
  submitted: Boolean;
  socios: SociosComercial;
  /** Expresión regular para letras y puntos. */
  letrasPunto: RegExp = /[\\A-Za-zñÑ. ]+/;
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Variables para mostrar el activo o inactivo en getByID*/
  chkd: boolean;
  chkdn: boolean;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;

  // Web Sockets
  disabled = true;
  datos: any[] = [];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              protected sociosService: SociosData) {
  }

  dismiss() {
    this.ref.close();
  }

  guardarSocio() {
    this.submitted = true;
    this.inactivo = true;
    if (this.sociosCreateForm.invalid) {
      return;
    }
    this.sociosService.post(this.sociosCreateForm.value).subscribe((result) => {
      this.sociosCreateForm.reset();
      this.ref.close();
    });
    this.sociosService.postSocket(this.sociosCreateForm.value).subscribe(x => {
    });
  }

  prioridad() {
    this.prioridadVal = [];
    this.prioridadVal.push({label: 'ALTA', value: 1});
    this.prioridadVal.push({label: 'MEDIANA', value: 2});
    this.prioridadVal.push({label: 'BAJA', value: 3});
  }

  getSociosById() {
    this.sociosService.getSociosById(this.idSocio).subscribe(data => {
      this.socios = data;
      this.sociosCreateForm.controls['prioridad'].setValue(data.prioridad);
      this.sociosCreateForm.controls['nombreComercial'].setValue(this.socios.nombreComercial);
      this.sociosCreateForm.controls['rfc'].setValue(this.socios.rfc);
      this.sociosCreateForm.controls['razonSocial'].setValue(this.socios.razonSocial);
      this.sociosCreateForm.controls['alias'].setValue(this.socios.alias);
      this.sociosCreateForm.controls['activo'].setValue(this.socios.activo);
      this.sociosCreateForm.controls['idEstadoSocio'].setValue(this.socios.idEstadoSocio);
      if (this.socios.idEstadoSocio !== null && this.socios.idEstadoSocio === 1) {
        // this.radioSi.checked = true;
        this.chkd = true;
      } else if (this.socios.idEstadoSocio !== null && this.socios.idEstadoSocio === 2) {
        // this.radioNo.checked = true;
        this.chkdn = true;
      }
    });
  }

  updateSocio() {
    this.inactivo = true;
    this.sociosService.put(this.idSocio, this.sociosCreateForm.value).subscribe(result => {
      this.sociosCreateForm.reset();
      this.sociosService.postSocket(this.sociosCreateForm.value).subscribe(x => {
      });
      this.ref.close();
    });
  }

  estado() {
    if (this.radioSi.checked) {
      this.sociosCreateForm.controls['idEstadoSocio'].setValue('1');
    } else if (this.radioNo.checked) {
      this.sociosCreateForm.controls['idEstadoSocio'].setValue('2');
    }
  }


  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSociosById();
    }
    this.prioridad();
    this.sociosCreateForm = this.fb.group({
      'prioridad': new FormControl('', Validators.required),
      'nombreComercial': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'rfc': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][1-9][A-Z-0-9][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(13), Validators.minLength(13)])),
      'razonSocial': new FormControl('', Validators.compose([Validators.required])),
      'alias': new FormControl('', [Validators.required]),
      'idEstadoSocio': 1,
      'activo': 1,
    });
  }
}
