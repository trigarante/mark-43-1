import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global3} from '../global3';
import {Llamadas, LlamadasData} from '../../interfaces/ventaNueva/llamadas';
import {Observable} from 'rxjs';

@Injectable()
export class LlamadasService extends LlamadasData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = Global3.url;
  }
  get(): Observable<Llamadas[]> {
    return this.http.get<Llamadas[]>(this.baseURL + 'callback');
  }
  post(idUsuario, llamadas: Llamadas): Observable<Llamadas> {
   return this.http.post<Llamadas>(this.baseURL + 'callback/usuario/' + idUsuario, llamadas);
}
}
