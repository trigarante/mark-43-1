import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosSoporteRoutingModule } from './catalogos-soporte-routing.module';
import { CatalogosSoporteComponent } from './catalogos-soporte.component';
import { EstadoInventarioComponent } from './estado-inventario/estado-inventario.component';
import { ObservacionesInventarioComponent } from './observaciones-inventario/observaciones-inventario.component';
import {ThemeModule} from '../../../../@theme/theme.module';
import {DropdownModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {NbDialogModule} from '@nebular/theme';
import { CreateEstadoInventarioComponent } from './modals/create-estado-inventario/create-estado-inventario.component';
import { CreateObservacionesInventarioComponent } from './modals/create-observaciones-inventario/create-observaciones-inventario.component';
import {TooltipModule} from 'primeng/tooltip';
import {
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatDialogModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule,
  MatTableModule, MatToolbarModule, MatAutocompleteModule, MatTabsModule,
} from '@angular/material';
import { MarcaComponent } from './marca/marca.component';
import { ProcesadorComponent } from './procesador/procesador.component';
import { CrearProcesadorComponent } from './modals/crear-procesador/crear-procesador.component';
import { CreateMarcaComponent } from './modals/create-marca/create-marca.component';

@NgModule({
  declarations: [
    CatalogosSoporteComponent,
    EstadoInventarioComponent,
    ObservacionesInventarioComponent,
    CreateEstadoInventarioComponent,
    CreateObservacionesInventarioComponent,
    MarcaComponent,
    ProcesadorComponent,
    CrearProcesadorComponent,
    CreateMarcaComponent,
  ],
  imports: [
    CommonModule,
    CatalogosSoporteRoutingModule,
    ThemeModule,
    KeyFilterModule,
    TableModule,
    NbDialogModule.forChild(),
    DropdownModule,
    TooltipModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatTabsModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
  ],
  entryComponents: [
    CreateEstadoInventarioComponent,
    CreateObservacionesInventarioComponent,
    CrearProcesadorComponent,
    CreateMarcaComponent,
  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTabsModule,
    MatAutocompleteModule,
  ],
})
export class CatalogosSoporteModule { }
