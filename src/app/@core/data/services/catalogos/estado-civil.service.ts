import {Injectable} from '@angular/core';
import {EstadoCivil, EstadoCivilData} from '../../interfaces/catalogos/estado-civil';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class EstadoCivilService extends EstadoCivilData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<EstadoCivil[]> {
    return this.http.get<EstadoCivil[]>(this.baseURL + 'v1/estado-civil');
  }

  post(estadoCivil: EstadoCivil): Observable<EstadoCivil> {
    return this.http.post<EstadoCivil>(this.baseURL + 'v1/estado-civil', estadoCivil);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'estadocivil/saveEstadoCivil', json);
  }

  getEstadoCivilById(idEstadoCivil): Observable<EstadoCivil> {
    return this.http.get<EstadoCivil>(this.baseURL + 'v1/estado-civil/' + idEstadoCivil);
  }

  put(idEstadoCivil, estadoCivil: EstadoCivil): Observable<EstadoCivil> {
    return this.http.put<EstadoCivil>(this.baseURL + 'v1/estado-civil/' + idEstadoCivil + '/'
      + sessionStorage.getItem('Empleado'), estadoCivil);
  }

}
