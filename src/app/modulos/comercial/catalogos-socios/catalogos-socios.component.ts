import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-catalogos-socios',
  template: '<router-outlet></router-outlet>',
})
export class CatalogosSociosComponent {

}
