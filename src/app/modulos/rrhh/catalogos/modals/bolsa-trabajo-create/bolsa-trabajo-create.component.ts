import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BolsaTrabajoComponent} from '../../bolsa-trabajo/bolsa-trabajo.component';
import {BolsaTrabajo, BolsaTrabajoData} from '../../../../../@core/data/interfaces/catalogos/bolsa-trabajo';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/** Componente del modal del catálogo para "crear bolsa de trabajo". */
@Component({
  selector: 'ngx-bolsa-trabajo-create',
  templateUrl: './bolsa-trabajo-create.component.html',
  styleUrls: ['./bolsa-trabajo-create.component.scss'],
})
export class BolsaTrabajoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que alamacena el idBolsaTrabajo obtenido. */
  @Input() idBolsaTrabajo: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una bolsa de trabajo. */
  bolsaTrabajoForm: FormGroup;
  /** Lista de elementos para el tipo de bolsa de trabajo. */
  tipo: SelectItem[];
  /** Expresión regular para sólo letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  bolsa: BolsaTrabajo;
  bolsatrabajoEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BolsaTrabajoComponent>} ref Referencia al diálogo (mensaje).
   * @param {BolsaTrabajoData} bolsaTrabajoService Servicio del componente bolsa-trabajo para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BolsaTrabajoComponent>, private bolsaTrabajoService: BolsaTrabajoData,
              private router: Router) {
  }
  /** Función que inicicializa la lista "tipo" con los valores "Web". */
  tipos() {
    this.tipo = [];
    this.tipo.push({label: 'Web', value: 'WEB'});
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarBolsaTrabajo() {
    if (this.bolsaTrabajoForm.invalid) {
      return;
    }
    this.submitted = true;
    this.bolsaTrabajoService.post(this.bolsaTrabajoForm.value).subscribe((result) => {
      this.bolsaTrabajoService.postSocket(this.bolsaTrabajoForm.value).subscribe(x => {});
      this.bolsaTrabajoForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/bolsaTrabajo']);
      this.bolsaTrabajoForm.reset();
      this.ref.close();
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que obtiene la información de la bolsa de trabajo dependiendo de su ID. */
  getBolsaTrabajoById() {
    this.bolsaTrabajoService.getBolsaTrabajoById(this.idBolsaTrabajo).subscribe( data => {
      this.bolsa = data;
      this.bolsaTrabajoForm.controls['nombre'].setValue(this.bolsa.nombre);
      this.bolsaTrabajoForm.controls['tipo'].setValue(this.bolsa.tipo);
      this.bolsaTrabajoForm.controls['activo'].setValue(this.bolsa.activo);
    });
  }

  /** Función que actualiza una bolsa de trabajo. */
  updateBolsaTrabajo() {
    this.bolsaTrabajoService.getBolsaTrabajoById(this.idBolsaTrabajo).subscribe( data => {
      if (data.activo === 1) {
        this.bolsaTrabajoService.put(this.idBolsaTrabajo, this.bolsaTrabajoForm.value).subscribe( result => {
          this.bolsaTrabajoService.postSocket(this.bolsaTrabajoForm.value).subscribe(x => {});
          this.bolsaTrabajoForm.reset();
          this.ref.close();
        });
      }else {
        this.bolsatrabajoEliminado = true;
      }
    });
  }

  /** Función que da de baja una bolsa de trabajo, cambiando el atributo activo a 0. */
  /*bajaBolsaTrabajos() {
    this.bolsaTrabajoForm.controls['activo'].setValue(0);
    this.bolsaTrabajoService.put(this.idBolsaTrabajo, this.bolsaTrabajoForm.value).subscribe( result => {
    this.bolsaTrabajoForm.reset();
    this.ref.close();
    window.location.reload();
    });
  }*/

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getBolsaTrabajoById();
    } else if (this.idTipo === 3) {
      this.getBolsaTrabajoById();
    }
    this.tipos();
    this.bolsaTrabajoForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'tipo': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
