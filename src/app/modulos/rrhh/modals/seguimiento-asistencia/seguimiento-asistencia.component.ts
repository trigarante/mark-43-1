import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RadioButton} from 'primeng/primeng';
import {NbDateService, NbDialogRef, NbToastrService} from '@nebular/theme';
import {SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import * as moment from 'moment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-seguimiento-asistencia',
  templateUrl: './seguimiento-asistencia.component.html',
  styleUrls: ['./seguimiento-asistencia.component.scss'],
})
export class SeguimientoAsistenciaComponent implements OnInit {
  seguimientoEliminado: boolean;
  @Input() idSeguimiento: number;
  @Input() idTipo: number;

  /** Variable para inicializar el formulario. */
  formAsistencia: FormGroup;
  @ViewChild('radioSi')
  radioSi: RadioButton;
  @ViewChild('radioNo')
  radioNo: RadioButton;
  //
  asistio: boolean;
  /**arreglo auxiliar del microservicio*/
  capcitacion: any;
  //
  fechaIngreso: any;
  //
  guarTrue: boolean;
  /**Variable que obtendra la fecha de sistema*/
  date: Date = new Date();
  /**Variable que almacena la fecha en formato AAAA/MM/DD*/
  fechaCompleta: any;
  /**Variable que almacenara la hora actual de sistema*/
  horaActual: any;
  /**Variable que mostrara el dia de capacitacion actual*/
  diaCapacitacion: any;
  //
  fechasAsistencia: any;
  creasAsistencia: any;
  c: any;
  /**Variable que almacena el JSON de asistencias*/
  jsonFecha: any;
  /**Variables que alamacenan las fechas por dia de capacitacion*/
  anadir1: any;
  anadir2: any;
  anadir3: any;
  anadir4: any;
  anadir5: any;
  anadir6: any;
  anadir7: any;
  anadir8: any;
  anadir9: any;
  anadir10: any;
  anadir11: any;
  anadir12: any;
  /**Variables que muestran los elemento si hay registro de asistencias*/
  visible1: boolean;
  visible2: boolean;
  visible3: boolean;
  visible4: boolean;
  visible5: boolean;
  visible6: boolean;
  visible7: boolean;
  visible8: boolean;
  visible9: boolean;
  visible10: boolean;
  visible11: boolean;
  visible12: boolean;

  activoAsistencia: boolean;

  crearFecha: any;
  constructor(
    protected ref: NbDialogRef<SeguimientoAsistenciaComponent>,
    private fb: FormBuilder,
    protected dateService: NbDateService<Date>,
    protected seguimientoService: SeguimientoData,
    private toastrService: NbToastrService,
  ) {
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  habilita() {
    if (this.radioSi.checked) {
      this.asistio = true;
      this.guarTrue = false;
      if (this.formAsistencia.controls['horaAsistenciaG'].valid &&
        this.formAsistencia.controls['horaAsistenciaG'].dirty) {
        this.guarTrue = true;
      }
    } else if (this.radioNo.checked) {
      this.asistio = false;
      this.guarTrue = true;
    }
  }

  getFecha() {
    this.seguimientoService.getSeguimientoById(this.idSeguimiento).subscribe(result => {
      this.capcitacion = result;
      let month;
      month = this.date.getMonth() + 1;
      month = Number(month);
      if (month < 10) {
        month = '0' + month;
      }
      let day;
      day = this.date.getDate();
      day = Number(day);
      if (day < 10) {
        day = '0' + day;
      }
      this.fechaCompleta = this.date.getFullYear() + '-' + month + '-' + day + 'T05:00:00.000Z';
      this.horaActual = this.date.getHours() + ':' + this.date.getMinutes();
      this.diaCapacitacion = Number(this.capcitacion.fechaIngreso.slice(8, 10));
      this.diaCapacitacion = (day - this.diaCapacitacion) + 1;
      if (this.diaCapacitacion > 12) {
        this.diaCapacitacion = 12;
      }
      if (this.capcitacion.asistencia !== null) {
        this.jsonFecha = this.capcitacion.asistencia;
        this.jsonFecha = JSON.parse(this.jsonFecha);
        if (typeof(this.jsonFecha) === 'string') {
          this.jsonFecha = JSON.parse(this.jsonFecha);
        }
        if (this.jsonFecha.hasOwnProperty('asistencia1') === false) {
          this.anadir1 = 1; } else { this.anadir1 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia2') === false) {
          this.anadir2 = 1; } else { this.anadir2 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia3') === false) {
          this.anadir3 = 1; } else { this.anadir3 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia4') === false) {
          this.anadir4 = 1; } else { this.anadir4 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia5') === false) {
          this.anadir5 = 1; } else { this.anadir5 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia6') === false) {
          this.anadir6 = 1; } else { this.anadir6 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia7') === false) {
          this.anadir7 = 1; } else { this.anadir7 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia8') === false) {
          this.anadir8 = 1; } else { this.anadir8 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia9') === false) {
          this.anadir9 = 1; } else { this.anadir9 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia10') === false) {
          this.anadir10 = 1; } else { this.anadir10 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia11') === false) {
          this.anadir11 = 1; } else { this.anadir11 = 0; }
        if (this.jsonFecha.hasOwnProperty('asistencia12') === false) {
          this.anadir12 = 1; } else { this.anadir12 = 0; }
        this.jsonFecha = JSON.stringify(this.jsonFecha);
      }
      /**Validacion para activar el registro de asistencia*/
      if (this.diaCapacitacion > 0 ) {
        this.activoAsistencia = true;
        }else {
        this.activoAsistencia = false;
      }
      if (this.idTipo === 2) {
        this.getAsistenciaByID();
      }
    });
  }

  generarAsistencia() {
    /**si no existe en la bd lo crea de lo contrario agrega al dia correspondiente*/
    if (this.capcitacion.asistencia === null) {
      if (this.radioSi.checked) {
        this.creasAsistencia = '{' +
          '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' + this.diaCapacitacion +
          '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' + this.diaCapacitacion +
          '" :"ASISTIO A LAS ' + this.formAsistencia.controls['horaAsistenciaG'].value + ' HORAS"}' +
          '}';
      } else if (this.radioNo.checked) {
        this.creasAsistencia = '{' +
          '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' + this.diaCapacitacion +
          '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' + this.diaCapacitacion +
          '" :"FALTA"}' +
          '}';
      }
    }else {
      this.creasAsistencia = this.capcitacion.asistencia;
      this.creasAsistencia = this.creasAsistencia.slice(0, this.creasAsistencia.length - 1);
      if (this.radioSi.checked) {
        this.fechasAsistencia = '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' +
          this.diaCapacitacion + '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' +
          this.diaCapacitacion + '" :"ASISTIO A LAS ' + this.formAsistencia.controls['horaAsistenciaG'].value + ' HORAS"}' +
          '}';
      } else if (this.radioNo.checked) {
        this.fechasAsistencia = '"asistencia' + this.diaCapacitacion + '": {"dia' + this.diaCapacitacion + '": "' +
          this.diaCapacitacion + '", "fecha' + this.diaCapacitacion + '" :"' + this.fechaCompleta + '", "detalle' +
          this.diaCapacitacion + '" :"FALTA"}' +
          '}';
      }
      this.creasAsistencia = this.creasAsistencia.concat(',', this.fechasAsistencia);
    }
    this.c = {
      'idSubarea': this.capcitacion.idSubarea,
      'idCoach': this.capcitacion.idCoach,
      'idCapacitacion': this.capcitacion.idCapacitacion,
      'calificacionRollplay': this.capcitacion.calificacionRollplay,
      'comentarios': this.capcitacion.comentarios,
      'asistencia': this.creasAsistencia,
      'fechaIngreso': this.capcitacion.fechaIngreso,
      'registro': this.capcitacion.registro,
    };
    let fechaIngreso: any;
    fechaIngreso = Date.parse(this.capcitacion.fechaIngreso);
    /**Guardar  creasAsitencia en BD*/
    this.seguimientoService.getSeguimientoById(this.idSeguimiento).subscribe(data => {
      if (data.idEtapa === 5 && data.idEstadoRRHH === 1 || data.idEtapa === 5 && data.idEstadoRRHH === 16) {
        this.seguimientoService.update(this.idSeguimiento, this.c, fechaIngreso).subscribe(result => {
          this.seguimientoService.postSocketRolplay(this.c).subscribe(x => {});
          this.showToast();
          this.ref.close();
        });
      }else {
        this.seguimientoEliminado = true;
      }
    });
  }

  getAsistenciaByID() {
    this.jsonFecha = this.capcitacion.asistencia;
    if (this.jsonFecha === null) {
      this.ref.close();
      this.showToastsa();
    } else {
      this.jsonFecha = JSON.parse(this.jsonFecha);
      if (typeof(this.jsonFecha) === 'string') {
        this.jsonFecha = JSON.parse(this.jsonFecha);
      }
      for (let i = 1; i < 13; i++) {
        if (this.jsonFecha.hasOwnProperty('asistencia' + i)) {
          this.formAsistencia.controls['fechaAsistencia' + i].setValue(this.jsonFecha['asistencia' + i]['fecha' + i]);
          this.formAsistencia.controls['detalleAsistencia' + i].setValue(this.jsonFecha['asistencia' + i]['detalle' + i]);
          if (i > 0) {
            for (let j = 1; j < i + 1; j++) {
              this['visible' + j] = true;
            }
          }
          if (i === 4) {
            this.activaSeguimiento();
          }
        } else {
          this['visible' + i] = false;
        }
      }
    }
  }

  modificarDetalle(indice, detalle) {
    if (this['anadir' + indice] !== 1) {
      this.jsonFecha['asistencia' + indice]['detalle' + indice] = detalle;
    }
  }

  modificarFecha(indice, fecha) {
    if (this['anadir' + indice] !== 1) {
      this.jsonFecha['asistencia' + indice]['fecha' + indice] = fecha;
    }
  }

  agregarAsistencia(indice) {
    if (this.formAsistencia.controls['fechaAsistencia' + indice].value === '' ||
      this.formAsistencia.controls['detalleAsistencia' + indice].value === '') {
      this.showToastNot();
    }else {
      this.jsonFecha = JSON.stringify(this.jsonFecha);
      this.creasAsistencia = this.jsonFecha;
      this.creasAsistencia = this.creasAsistencia.slice(0, this.creasAsistencia.length - 1);
      this.fechasAsistencia = '"asistencia' + indice + '":{"dia' + indice + '":"' +
        indice + '","fecha' + indice + '":"' + (moment(this.formAsistencia.controls['fechaAsistencia' + indice].value).
        format('YYYY-MM-DD') + 'T05:00:00.000Z') + '","detalle' +
        indice + '":"' + this.formAsistencia.controls['detalleAsistencia' + indice].value + '"}' +
        '}';
      this.creasAsistencia = this.creasAsistencia.concat(',', this.fechasAsistencia);
      this.jsonFecha = this.creasAsistencia;
      this.updateAsistencia();
      this['anadir' + indice] = 1;
      this.jsonFecha = JSON.parse(this.jsonFecha);
    }
  }

  updateAsistencia() {

    if (typeof (this.jsonFecha) !== 'string' ) {
      this.jsonFecha = JSON.stringify(this.jsonFecha);
    }
    this.c = {
      'idSubarea': this.capcitacion.idSubarea,
      'idCapacitacion': this.capcitacion.idCapacitacion,
      'idCoach': this.capcitacion.idCoach,
      'idCalificacionRollPlay': this.capcitacion.idCalificacionRollPlay,
      'comentarios': this.capcitacion.comentarios,
      'asistencia': this.jsonFecha,
      'fechaIngreso': this.capcitacion.fechaIngreso,
    };
    let fechaIngreso: any;
    fechaIngreso = Date.parse(this.capcitacion.fechaIngreso);

    /**Guardar  creasAsitencia en BD*/
    this.seguimientoService.getSeguimientoById(this.idSeguimiento).subscribe(data => {
      if (data.idEtapa === 5 && data.idEstadoRRHH === 1 || data.idEtapa === 5 && data.idEstadoRRHH === 16) {
        this.seguimientoService.update(this.idSeguimiento, this.c, fechaIngreso).subscribe(result => {
          this.seguimientoService.postSocketRolplay(this.c).subscribe(x => {});
          this.showToastU();
          this.ref.close();
        });
      }else {
        this.seguimientoEliminado = true;
      }
    });
  }

  showToastU() {
    this.toastrService.info('!!Modificación Exitosa¡¡', `Asistencia`);
  }

  showToastNot() {
    this.toastrService.danger('!Hay campos vacíos¡', `Asistencia`);
  }

  showToast() {
    this.toastrService.info('!!Asistencia registrada con Exito¡¡', `Asistencia`);
  }

  showToastsa() {
    this.toastrService.info('!!Aún NO registra Asistencia¡¡', `Asistencia`);
  }

  activaSeguimiento() {
    ///
  }

  ngOnInit() {
    this.asistio = false;
    this.guarTrue = false;
    this.getFecha();
    this.formAsistencia = this.fb.group({
      'horaAsistenciaG': new FormControl('', Validators.compose([Validators.required, Validators.min(9.00), Validators.max(17.00)])),
      'detalleAsistencia1': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia1': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia2': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia2': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia3': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia3': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia4': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia4': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia5': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia5': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia6': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia6': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia7': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia7': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia8': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia8': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia9': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia9': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia10': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia10': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia11': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia11': new FormControl('', Validators.compose([Validators.required])),
      'detalleAsistencia12': new FormControl('', Validators.compose([Validators.required])),
      'fechaAsistencia12': new FormControl('', Validators.compose([Validators.required])),
    });
  }


}
