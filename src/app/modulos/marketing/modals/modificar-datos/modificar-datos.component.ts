import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {SedeData, Sede} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData, Grupo} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData, Empresa} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData, Area} from '../../../../@core/data/interfaces/catalogos/area';
import {SubareaData, Subarea} from '../../../../@core/data/interfaces/catalogos/subarea';
import {map} from 'rxjs/operators';
import {GuardadoComponent} from '../guardado/guardado.component';
import {Router} from '@angular/router';
/**import {Categorias, CategoriasData} from '../../../../@core/data/interfaces/comerciales/categorias';*/
import {SubRamo, SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';
import {formatDate} from "@angular/common";


@Component({
  selector: 'ngx-modificar-datos',
  templateUrl: './modificar-datos.component.html',
  styleUrls: ['./modificar-datos.component.scss'],
})
export class ModificarDatosComponent implements OnInit {
  modificarDatosForm: FormGroup;
  @Input() campo: string;
  @Input() nombre: string;
  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  /** variable que se puede obtener el nombre de la aseguradora*/
  @Input() nombreaseguradora: string;
  @Input() tiponombre: string;
  pagina: Pagina;
  aseguradoras: any;
  tipodedato: any;
  tipodenombre: any;
  seleccion2: any[];
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarcategorias: boolean;


  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para puesto. */
  empresa: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  categorias: SubRamo[];
  categoria: any[];

  areaget: Subarea;
  sedeget: any;
  empresaget: any;
  grupoempresarialget: any;
  grupoget: any;
  siexisteaseguradora: any;
  astring: any;
   x: any;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<ModificarDatosComponent>,
              public dialogService: NbDialogService, private router: Router,
              private paginaService: PaginasData,
              protected subareaService: SubareaData, private sedesService: SedeData,
              private grupoService: GrupoData, private empresaService: EmpresaData,
              private areaService: AreaData, private categoriasService: SubramoData,
              ) {
    this.modificarDatosForm = this.fb.group({
      'nombreCampo': new FormControl('', Validators.compose([Validators.required])),
      'valor': new FormControl('', Validators.compose([Validators.required])),
      'idGrupo': new FormControl(),
      'idEmpresa': new FormControl(),
      'idSede': new FormControl(),
      'idArea': new FormControl(),
    });
  }
  inicio() {
    this.modificarDatosForm.controls['nombreCampo'].setValue(this.campo);
    this.modificarDatosForm.controls['valor'].setValue(this.nombre);
    this.tipodenombre = this.campo;
    this.tipodedato = typeof this.nombre;
     // console.log(this.tipodenombre);

    if (this.tipodedato === 'string') {
      // console.log('es string');
    }
    if (this.tipodedato === 'number') {
     // console.log('es numero')
    }
    if (this.tipodedato === 'boolean') {
       // console.log(' es booleano')
    }
    if (this.campo === 'idCampana') {
      this.subareaService.getById(this.nombre).subscribe(data2 => {
        this.areaget = data2;

        this.getSubarea(this.areaget.idArea);

        this.areaService.getAreaById(this.areaget.idArea).subscribe(data3 => {
          this.sedeget = data3;
          this.getArea(this.sedeget.idSede);
          // console.log(this.sedeget);
          this.modificarDatosForm.controls['idArea'].setValue(this.sedeget.id);


          this.sedesService.getSedeById(this.sedeget.idSede).subscribe(data4 => {
            this.empresaget = data4;
            this.getSedes(this.empresaget.idEmpresa);
            this.modificarDatosForm.controls['idSede'].setValue(this.empresaget.id);

            this.empresaService.getEmpresasById(this.empresaget.idEmpresa).subscribe(data5 => {
              this.grupoempresarialget = data5;
              this.getEmpresa(this.grupoempresarialget.idGrupo);
              this.modificarDatosForm.controls['idEmpresa'].setValue(this.grupoempresarialget.id);

              this.grupoService.getGrupoById(this.grupoempresarialget.idGrupo).subscribe(data6 => {
                this.grupoget = data6;
                this.getGrupo();
                this.modificarDatosForm.controls['idGrupo'].setValue(this.grupoget.id);

              });
            });

          });
        });

      });
    }

  }


  guardarCampos() {

    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);
      /** si hay una aseguradora*/
      /** Se obtiene los datos para ver si existe un campo llamado aseguradoras y despues se pasa a string*/
      this.siexisteaseguradora = Object.keys(this.aseguradoras);
      this.astring = JSON.stringify(this.siexisteaseguradora);
      /** si no existe aseguradoras, va directo a mostrar los datos directamente a la tabla de solo una aseguradora*/
      if (this.astring !== '["aseguradoras"]') {
        // delete this.aseguradoras[this.campo];
        // this.aseguradoras[this.modificarDatosForm.value.nombreCampo] =
        //   this.modificarDatosForm.value.valor;
        //  bien this.aseguradoras[this.tiponombre].aseguradoras[this.nombreaseguradora][this.modificarDatosForm.value.nombreCampo ] =
        //   this.modificarDatosForm.value.valor

        if (this.modificarDatosForm.value.nombreCampo !== this.campo) {
          delete this.aseguradoras[this.tiponombre].aseguradoras[this.nombreaseguradora][this.campo];
        }
        this.aseguradoras[this.tiponombre].aseguradoras[this.nombreaseguradora][this.modificarDatosForm.value.nombreCampo ] =
          this.modificarDatosForm.value.valor;
      }


      if (this.astring === '["aseguradoras"]') {
        // delete this.aseguradoras.aseguradoras[this.nombreaseguradora][this.campo];
        this.aseguradoras.aseguradoras[this.nombreaseguradora][this.modificarDatosForm.value.nombreCampo] =
          this.modificarDatosForm.value.valor;
      }


      this.pagina['configuracion'] = JSON.stringify(this.aseguradoras);


      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.now();
      fechaActualizacion = Date.parse(formatDate( fechaActualizacion, 'yyyy-MM-dd hh:mm:ss', 'en-US'));


      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocketAseguradoras({status: 'ok'}).subscribe(x => {});
        this.ref.close();
        this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 1,
        },
      });
      this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
    });
  }
  seleccion() {
    this.seleccion2 = [];

    this.seleccion2.push({'label': 'true', 'value': true});
    this.seleccion2.push({'label': 'false', 'value': false});
  }
  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }

  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  getSedes(idEmpresa) {
    this.sedesService.get().pipe(map(result => {
      return result.filter( valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }
  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      }else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }
  getaseguradora() {
    this.categoriasService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.categorias = data;
      if (this.categorias.length === 0) {
        this.mostrarcategorias = true;
      }else {
        this.mostrarcategorias = false;
      }
      this.categoria = [];
      for (let i = 0; i < this.categorias.length; i++) {
        this.categoria.push({'label': this.categorias[i].nombreComercial, 'value': this.categorias[i].id});
      }
    });

  }

  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.inicio();
    this.getGrupo();
    this.seleccion();
    this.getaseguradora();
  }

}
