/**
 *@description  Interfaz de las Respuestas generadas asi como los campos que la conforman
 */
export interface RespuestasInterfaces {
  /**
   * Variables de la interfaz //idRespuesta recibe un parametro number que hace referencia a la Preguntta
   */
  id: number;
  respuestas: string;
  icono: string;
  color: string;
  idPregunta: number;
  contador?: number;
}
