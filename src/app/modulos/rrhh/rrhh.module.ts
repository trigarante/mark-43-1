import {APP_INITIALIZER, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RrhhComponent} from './rrhh.component';
import {ThemeModule} from '../../@theme/theme.module';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import {RrhhRoutingModule} from './rrhh-routing.module';
import { PrecandidatosComponent } from './precandidatos/precandidatos.component';
import { CandidatosComponent } from './candidatos/candidatos.component';
import { CapacitacionComponent } from './capacitacion/capacitacion.component';
import { SeguimientoComponent } from './seguimiento/seguimiento.component';
import { PreEmpleadosComponent } from './pre-empleados/pre-empleados.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import { AgendarCitaComponent } from './modals/agendar-cita/agendar-cita.component';
import {NbAlertModule, NbDatepickerModule, NbDialogModule, NbSelectModule, NbToastrModule, NbTooltipModule} from '@nebular/theme';
import { CheckDocumentosComponent } from './modals/check-documentos/check-documentos.component';
import { EmpleadoCreateComponent } from './empleado-create/empleado-create.component';
import { PrecandidatoCreateComponent } from './precandidato-create/precandidato-create.component';
import { SolicitudCreateComponent } from './modals/solicitud-create/solicitud-create.component';
import { GenerarUrlComponent } from './modals/generar-url/generar-url.component';
import {DialogModule, DropdownModule, FileUploadModule, KeyFilterModule} from 'primeng/primeng';
import { AsignarCapacitacionComponent } from './modals/asignar-capacitacion/asignar-capacitacion.component';
import { AsignarCandidatoComponent } from './modals/asignar-candidato/asignar-candidato.component';
import { AsignarSeguimientoComponent } from './modals/asignar-seguimiento/asignar-seguimiento.component';
import { AsignarEtapaPracticaComponent } from './modals/asignar-etapa-practica/asignar-etapa-practica.component';
import {EmpleadoSelectedComponent} from './empleado-selected/empleado-selected.component';
import {EmpleadoUpdateComponent} from './empleado-update/empleado-update.component';
import {EmpleadoBajaComponent} from './empleado-baja/empleado-baja.component';
import {BajaEmpleadoComponent} from './modals/baja-empleado/baja-empleado.component';
import { EmpleadosImssComponent } from './empleados-imss/empleados-imss.component';
import { AsignarImssComponent } from './modals/asignar-imss/asignar-imss.component';
import { SolicitudBajaComponent } from './modals/solicitud-baja/solicitud-baja.component';
import { BajaPrecandidatoComponent } from './modals/baja-precandidato/baja-precandidato.component';
import { PrecandidatoUpdateComponent } from './precandidato-update/precandidato-update.component';
import {VacantesComponent} from './vacantes/vacantes.component';
import {VacantesCreateComponent} from './modals/vacantes-create/vacantes-create.component';

import {TooltipModule} from 'primeng/tooltip';
import { AsistenciaComponent } from './modals/asistencia/asistencia.component';
import { DatosSolicitudesComponent } from './modals/datos-solicitudes/datos-solicitudes.component';
import { BajasrhComponent } from './bajasrh/bajasrh.component';
import { SeguimientoAsistenciaComponent } from './modals/seguimiento-asistencia/seguimiento-asistencia.component';
import {SeguimientoPracticoComponent} from './seguimiento-practico/seguimiento-practico.component';
import {
  MatAutocompleteModule,
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatDialogModule, MatFormFieldControl,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatSortModule, MatButtonToggleModule,
  MatTableModule, MatToolbarModule, MatRadioModule, MatPaginatorIntl, MatCheckboxModule, MatSnackBarModule, MatSnackBar,
} from '@angular/material';
import { DatosVacantesComponent } from './modals/datos-vacantes/datos-vacantes.component';
import { DatosPrecandidatoComponent } from './modals/datos-precandidato/datos-precandidato.component';
import { DatosCandidatoComponent } from './modals/datos-candidato/datos-candidato.component';
import { DatosCapacitacionComponent } from './modals/datos-capacitacion/datos-capacitacion.component';
import { DatosSeguimientoTeoricoComponent } from './modals/datos-seguimiento-teorico/datos-seguimiento-teorico.component';
import { DatosSeguimientoPractivoComponent } from './modals/datos-seguimiento-practivo/datos-seguimiento-practivo.component';
import { DatosSeguimientoPreempleadoComponent } from './modals/datos-seguimiento-preempleado/datos-seguimiento-preempleado.component';
import { DatosEmpleadoComponent } from './modals/datos-empleado/datos-empleado.component';
import { DocumentosComponent } from './documentos/documentos.component';
import {LoggedInGuard} from '../../@core/data/services/sessions/loggedInGuard';
import {GapiSession} from '../../@core/data/services/sessions/gapi.session';
import {AppSession} from '../../@core/data/services/sessions/app.session';
import {AppContext} from '../../@core/data/services/app.context';
import {UserSession} from '../../@core/data/services/sessions/user.session';
import {FileSession} from '../../@core/data/services/sessions/file.session';
import {BreadCrumbSession} from '../../@core/data/services/sessions/breadcrumb.session';
import {FilesService} from '../../@core/data/services/drive/files.service';
import {UserdService} from '../../@core/data/services/drive/userd.service';
import {AppService} from '../../@core/data/services/drive/app.service';
import {FilesUploadComponent} from './filesupload/filesupload.component';
import {DialogOneInputComponent} from './dialogoneinput/dialogoneinput.component';
import { FotoCreateComponent } from './modals/foto-create/foto-create.component';
import { ReactivarEmpleadoComponent } from './modals/reactivar-empleado/reactivar-empleado.component';
import { AsignarFiniquitoComponent } from './modals/asignar-finiquito/asignar-finiquito.component';
import { RecontratableComponent } from './modals/recontratable/recontratable.component';
import {MatTooltipModule} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { DatosPreempladoComponent } from './modals/datos-preemplado/datos-preemplado.component';
import { PreguntasComponent } from './preguntas/preguntas.component';
import { DatosBajaRhComponent } from './modals/datos-baja-rh/datos-baja-rh.component';
import {
  GenerarPreguntasComponent,
} from './preguntas/generar-preguntas/generar-preguntas.component';
import {
  GenerarRespuestasComponent,
} from './preguntas/generar-respuestas/generar-respuestas.component';
// import { ResultadosComponent } from './preguntas/resultados/resultados.component';
import {MatProgressSpinnerModule} from '@angular/material';
// import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';

@NgModule({
  declarations: [
    DialogOneInputComponent,
    FilesUploadComponent,
    RrhhComponent,
    SolicitudesComponent,
    VacantesComponent,
    PrecandidatosComponent,
    CandidatosComponent,
    CapacitacionComponent,
    SeguimientoComponent,
    PreEmpleadosComponent,
    EmpleadosComponent,
    AgendarCitaComponent,
    CheckDocumentosComponent,
    EmpleadoCreateComponent,
    PrecandidatoCreateComponent,
    SolicitudCreateComponent,
    GenerarUrlComponent,
    AsignarCandidatoComponent,
    AsignarCapacitacionComponent,
    AsignarSeguimientoComponent,
    AsignarEtapaPracticaComponent,
    EmpleadoSelectedComponent,
    EmpleadoUpdateComponent,
    EmpleadoBajaComponent,
    BajaEmpleadoComponent,
    EmpleadosImssComponent,
    AsignarImssComponent,
    SolicitudBajaComponent,
    BajaPrecandidatoComponent,
    PrecandidatoUpdateComponent,
    VacantesCreateComponent,
    AsistenciaComponent,
    DatosSolicitudesComponent,
    BajasrhComponent,
    SeguimientoPracticoComponent,
    SeguimientoAsistenciaComponent,
    DatosVacantesComponent,
    DatosPrecandidatoComponent,
    DatosCandidatoComponent,
    DatosCapacitacionComponent,
    DatosSeguimientoTeoricoComponent,
    DatosSeguimientoPractivoComponent,
    DatosSeguimientoPreempleadoComponent,
    DatosEmpleadoComponent,
    DocumentosComponent,
    FotoCreateComponent,
    ReactivarEmpleadoComponent,
    AsignarFiniquitoComponent,
    RecontratableComponent,
    DatosPreempladoComponent,
    PreguntasComponent,
    GenerarPreguntasComponent,
    GenerarRespuestasComponent,
    DatosBajaRhComponent,
    // ResultadosComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    ThemeModule,
    RrhhRoutingModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
  ],
  entryComponents: [
    DialogOneInputComponent,
    FilesUploadComponent,
    AgendarCitaComponent,
    CheckDocumentosComponent,
    SolicitudCreateComponent,
    GenerarUrlComponent,
    AsignarCandidatoComponent,
    AsignarCapacitacionComponent,
    AsignarSeguimientoComponent,
    AsignarEtapaPracticaComponent,
    BajaEmpleadoComponent,
    AsignarImssComponent,
    SolicitudBajaComponent,
    VacantesCreateComponent,
    BajaPrecandidatoComponent,
    AsistenciaComponent,
    DatosSolicitudesComponent,
    SeguimientoAsistenciaComponent,
    DatosVacantesComponent,
    DatosPrecandidatoComponent,
    DatosCandidatoComponent,
    DatosCapacitacionComponent,
    DatosSeguimientoTeoricoComponent,
    DatosSeguimientoPractivoComponent,
    DatosSeguimientoPreempleadoComponent,
    DatosEmpleadoComponent,
    FotoCreateComponent,
    ReactivarEmpleadoComponent,
    AsignarFiniquitoComponent,
    RecontratableComponent,
    DatosPreempladoComponent,
    DatosBajaRhComponent,
  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatCheckboxModule,
  ],
  providers: [
    AppContext,
    AppSession,
    FileSession,
    GapiSession,
    UserSession,
    AppService,
    BreadCrumbSession,
    FilesService,
    UserdService,
    LoggedInGuard,
    {
      provide: MatPaginatorIntl,
      useClass: VacantesComponent,
    },
    MatCardModule,
  ],
  // schemas: [
  //   MatPaginator,
  // ],
})
export class RrhhModule {
}
