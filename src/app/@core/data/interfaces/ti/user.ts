import {Observable} from 'rxjs';
import {DatosUsuarios} from './datosUsuarios';


export interface User {
  id?: number;
  usuario?: string;
  password?: string;
  idCorreo?: string;
  token?: string;
  idGrupo?: number;
  idTipo?: number;
  idSubarea?: number;
  fechaCreacion?: Date;
  datosUsuario?: string ;
  estado: number;
  grupoUsuarios?: string;
  imagenUrl?: string;
  nombre?: string;
  permisos?: string;
  descripcion?: string;
  tipoUsuario?: string;
  idEmpleado?: string;
  asignado?: number;
}
export abstract class UsuarioData {
  abstract get(): Observable<User[]>;
  abstract getUsuarioByIdSubarea(IdSubarea, Estado): Observable<User[]>;

  abstract postUsuarios(usuario: User): Observable<User>;

  abstract getUsuarioByEstado(Estado): Observable<User[]>;

  abstract getUsuarioByRango(idEstado, menor, mayor): Observable<User[]>;

  abstract postUsuariosidEmpleados(jsonUsuario, idempleado);

  abstract put(idUsuario, usuario: User): Observable<User>;
  abstract getUsuarioById(idUsuario): Observable<User>;
}
