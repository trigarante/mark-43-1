import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ProveedorLeads, ProveedorLeadsData} from '../../interfaces/catalogos/proveedor-leads';

@Injectable()
export class ProvedoresLeadService extends ProveedorLeadsData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ProveedorLeads[]> {
    return this.http.get<ProveedorLeads[]>(this.baseURL + 'v1/proveedores-lead');
  }

  post(proveedoresLead: ProveedorLeads): Observable<ProveedorLeads> {
    return this.http.post<ProveedorLeads>(this.baseURL + 'v1/proveedores-lead', proveedoresLead);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'provedoresleads/saveProvedoresLeads', json);
  }

  getProveedorLeadById(idProveedoresLead): Observable<ProveedorLeads> {
    return this.http.get<ProveedorLeads>(this.baseURL + 'v1/proveedores-lead/' + idProveedoresLead);
  }

  put(idProveedoresLead, proveedoresLead: ProveedorLeads) {
    return this.http.put<ProveedorLeads>(this.baseURL + 'v1/proveedores-lead/' + idProveedoresLead + '/'
      + sessionStorage.getItem('Empleado') , proveedoresLead);
  }

  // getProveedorById(idProveedor): Observable<ProveedorLeads> {
  //   return undefined;
  // }
}
