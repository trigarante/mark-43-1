import {Observable} from 'rxjs';

export interface Convenios {
  idTipoConvenio: number;
  tipoConvenio: string;
  cantidad: number;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  estado: any;
  alias: any;
  nombreComercial: any;
  subRamo: any;
  tipoGiro: any;
  idSubCategoria: number;
  tipoSubRamo: string;
  tipoRamo: string;
  idTipoDivisa: number;
  idTipoSubRamo: number;
  idSubRamo: number;
  detalle?: string;
}

export abstract class ConveniosData {
  abstract get(): Observable<Convenios[]>;

  abstract post(convenios): Observable<Convenios>;
  abstract postSocket(json): Observable<JSON>;

  abstract getConveniosById(idConvenios): Observable<Convenios>;

  abstract put(idConvenios, convenios): Observable<Convenios>;
}
