import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginatorIntl, TooltipPosition} from '@angular/material';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ClientesData, ClienteVn} from '../../../@core/data/interfaces/ventaNueva/cliente-vn';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss'],
})
export class ClienteComponent implements OnInit {
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  clientes: ClienteVn[];
  dataSource: any;

  constructor(private clienteService: ClientesData) {
    this.getAndInitTranslations();
  }

  ngOnInit() {
    this.cols = ['detalle', 'nombre', 'direccion', 'genero', 'telefono', 'acciones'];
    this.getClientes();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getAndInitTranslations() {
    // this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    // this.nextPageLabel = 'DATOS POR PÁGINA';
    // this.previousPageLabel = 'DATOS POR PÁGINA';
    // this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }

  getClientes() {
    this.clienteService.get().subscribe(result => {
      this.clientes = result;
      this.dataSource = new MatTableDataSource(this.clientes); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }


}
