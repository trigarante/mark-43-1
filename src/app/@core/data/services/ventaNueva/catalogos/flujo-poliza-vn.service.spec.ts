import { TestBed } from '@angular/core/testing';

import { FlujoPolizaVnService } from './flujo-poliza-vn.service';

describe('FlujoPolizaVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlujoPolizaVnService = TestBed.get(FlujoPolizaVnService);
    expect(service).toBeTruthy();
  });
});
