import { Injectable } from '@angular/core';
import {Drive, DriveData} from '../../interfaces/ti/drive';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class DriveService extends DriveData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  // Cuando se requieran subir varios archivos usar SOLAMENTE el parámetro registros
  post(files: FileList, registro?: string , registros?: Array<string>): Observable<any> {
    const formData = new FormData();

    if (registro != null) {
      formData.append('files', files.item(0), registro);
    } else {
      if (registros !== null) {
        for (let i = 0; i < files.length; i++) {
          formData.append('files', files.item(i), registro[i]);
        }
      }
    }

    return this.http.post(this.baseURL + 'v1/api-drive/upload-files/19', formData,
      {responseType: 'text' as 'json'});
  }

  get(fechaInicio: Date, fechaFin: Date, ACD: boolean, manual: boolean, numero: string): Observable<Drive[]> {
    let fechaFinal: number;

    if (fechaFin === null || fechaFin === undefined) {
      fechaFinal = 0;
    } else { fechaFinal = fechaFin.getTime(); }

    return this.http.get<Drive[]>(this.baseURL + 'v1/api-drive/' + fechaInicio.getTime() + '/'
      + fechaFinal + '/' + ACD + '/' + manual + '/' + numero);
  }
}
