import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './users.service';
import { StateService } from './state.service';
import {SolicitudesService} from './services/rrhh/solicitudes.service';
import {BolsaTrabajoService} from './services/catalogos/bolsa-trabajo.service';
import {VacantesService} from './services/catalogos/vacantes.service';
import {EstadoCivilService} from './services/catalogos/estado-civil.service';
import {PaisesService} from './services/catalogos/paises.service';
import {EscolaridadesService} from './services/catalogos/escolaridades.service';
import {EstadoEscolaridadService} from './services/catalogos/estado-escolaridad.service';
import {SepomexService} from './services/catalogos/sepomex.service';
import {CandidatosService} from './services/rrhh/candidatos.service';
import {SeguimientoService} from './services/rrhh/seguimiento.service';
import {EmpleadosService} from './services/rrhh/empleados.service';
import {MedioTransporteService} from './services/catalogos/medio-transporte.service';
import {EstacionesLineasService} from './services/catalogos/estaciones-lineas.service';
import {CampanasService} from './services/catalogos/campanas.service';
import {PuestoService} from './services/catalogos/puesto.service';
import {PuestoTipoService} from './services/catalogos/puesto-tipo.service';
import {SubareaService} from './services/catalogos/subarea.service';
import {BancosService} from './services/catalogos/bancos.service';
import {EmpresasService} from './services/catalogos/empresas.service';
import {LoginService} from './services/login/login.service';
import {TokenStorageService} from './services/login/token-storage.service';
import {EstadoRrhhService} from './services/catalogos/estado-rrhh.service';
import {RamoService} from './services/comerciales/ramo.service';
import {TipoRamoService} from './services/comerciales/catalogo/tipo-ramo.service';
import {TipoSubramoService} from './services/comerciales/catalogo/tipo-subramo.service';
import {SubRamoService} from './services/comerciales/sub-ramo.service';
import { TareasService } from './services/tareas/tareas.service';




const SERVICES = [
  UserService,
  StateService,
  SolicitudesService,
  BolsaTrabajoService,
  VacantesService,
  EstadoCivilService,
  PaisesService,
  EscolaridadesService,
  EstadoEscolaridadService,
  SepomexService,
  CandidatosService,
  SeguimientoService,
  EmpleadosService,
  MedioTransporteService,
  EstacionesLineasService,
  CampanasService,
  SeguimientoService,
  PuestoService,
  PuestoTipoService,
  SubareaService,
  BancosService,
  EmpresasService,
  LoginService,
  TokenStorageService,
  EstadoRrhhService,
  RamoService,
  TipoRamoService,
  TipoSubramoService,
  SubRamoService,
  TareasService,
];


@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
