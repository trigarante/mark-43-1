import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {ProveedorLeadsService} from './proveedor-leads.service';

describe('Proveedor leads service', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ProveedorLeadsService, HttpClient],
  }));
})
