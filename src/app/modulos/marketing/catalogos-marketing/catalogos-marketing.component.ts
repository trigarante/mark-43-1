import {Component} from '@angular/core';

@Component({
  selector: 'ngx-catalogos-marketing',
  template: '<router-outlet></router-outlet>',
})
export class CatalogosMarketingComponent {
}
