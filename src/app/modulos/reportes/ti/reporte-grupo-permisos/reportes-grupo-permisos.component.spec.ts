import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesGrupoPermisosComponent } from './reportes-grupo-permisos.component';

describe('ReportesGrupoPermisosComponent', () => {
  let component: ReportesGrupoPermisosComponent;
  let fixture: ComponentFixture<ReportesGrupoPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesGrupoPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesGrupoPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
