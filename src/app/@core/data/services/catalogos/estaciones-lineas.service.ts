import {Injectable} from '@angular/core';
import {EstacionesLineas, EstacionesLineasData} from '../../interfaces/catalogos/estaciones-lineas';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class EstacionesLineasService extends EstacionesLineasData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getByIdMedio(id): Observable<EstacionesLineas[]> {
    return this.http.get<EstacionesLineas[]>(this.baseURL + 'v1/estaciones-lineas/transporte/' + id);
  }

  getEstacionByIdLinea(idTransporte, idLinea): Observable<EstacionesLineas[]> {
    return this.http.get<EstacionesLineas[]>(this.baseURL + 'v1/estaciones-lineas/transporte/' + idTransporte + '/linea/' + idLinea);
  }

  getEstacionById(id): Observable<EstacionesLineas> {
    return this.http.get<EstacionesLineas>(this.baseURL + 'v1/estaciones-lineas/estacion/' + id);
  }


}
