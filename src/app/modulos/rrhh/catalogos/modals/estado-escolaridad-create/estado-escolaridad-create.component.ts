import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {NbDialogRef} from '@nebular/theme';
import {EstadoEscolaridadComponent} from '../../estado-escolaridad/estado-escolaridad.component';
import {Router} from '@angular/router';

/** Componente del modal del catálogo para "crear estado de escolaridad". */
@Component({
  selector: 'ngx-estado-escolaridad-create',
  templateUrl: './estado-escolaridad-create.component.html',
  styleUrls: ['./estado-escolaridad-create.component.scss'],
})
export class EstadoEscolaridadCreateComponent implements OnInit {
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z ]+/;
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idEstadoEscolaridad obtenido. */
  @Input() idEstadoEscolaridad: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un estado de escolaridad. */
  estadoEscolaridadCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  escolaridadEstado: EstadoEscolaridad;
  estadoescolaridadEliminado: boolean;

  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EstadoEscolaridadData} estadoEscolaridadService Servicio del componente estado-escolaridad para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private estadoEscolaridadService: EstadoEscolaridadData, private fb: FormBuilder,
              protected ref: NbDialogRef<EstadoEscolaridadComponent>,
              private router: Router) {
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarEstadoEscolaridad() {
    if (this.estadoEscolaridadCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.estadoEscolaridadService.post(this.estadoEscolaridadCreateForm.value).subscribe((result) => {
      this.estadoEscolaridadService.postSocket(this.estadoEscolaridadCreateForm.value).subscribe(x => {});
      this.estadoEscolaridadCreateForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/estadoEscolaridad']);
      this.estadoEscolaridadCreateForm.reset();
      this.ref.close();
      //window.location.reload();

    });
  }

  /** Función que obtiene toda la información del estado de escolaridad dependiendo de su ID. */
  getEstadoEscolaridadById() {
    this.estadoEscolaridadService.getEstadoEscolaridadById(this.idEstadoEscolaridad).subscribe( data => {
      this.escolaridadEstado = data;
      this.estadoEscolaridadCreateForm.controls['estado'].setValue( this.escolaridadEstado.estado);
      this.estadoEscolaridadCreateForm.controls['activo'].setValue( this.escolaridadEstado.activo);
    });
  }

  /** Función que actualiza la información del estado de escolaridad. */
  updateEscolaridadEstado() {
    this.estadoEscolaridadService.getEstadoEscolaridadById(this.idEstadoEscolaridad).subscribe( data => {
      if(data.activo === 1){
        this.estadoEscolaridadService.put(this.idEstadoEscolaridad, this.estadoEscolaridadCreateForm.value).subscribe( result => {
          this.estadoEscolaridadService.postSocket(this.estadoEscolaridadCreateForm.value).subscribe(x => {});
          this.estadoEscolaridadCreateForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.estadoescolaridadEliminado = true;
      }
    });

  }

  /** Función que da de baja un estado de escolaridad, cambiando el atributo activo a 0. */
  /*bajaEstadoEscolaridad() {
    this.estadoEscolaridadCreateForm.controls['activo'].setValue(0);
    this.estadoEscolaridadService.put(this.idEstadoEscolaridad, this.estadoEscolaridadCreateForm.value).subscribe( result => {
      this.estadoEscolaridadCreateForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEstadoEscolaridadById();
    } else if (this.idTipo === 3) {
      this.getEstadoEscolaridadById();
    }
    this.estadoEscolaridadCreateForm = this.fb.group({
      'estado': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
