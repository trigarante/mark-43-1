import {Component, OnInit, ViewChild} from '@angular/core';
import {Competencia, CompetenciaData} from '../../../../@core/data/interfaces/catalogos/competencia';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {CompetenciasCreateComponent} from '../modals/competencias-create/competencias-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-competencias',
  templateUrl: './competencias.component.html',
  styleUrls: ['./competencias.component.scss'],
})
export class CompetenciasComponent implements OnInit {
  cols: any[];
  competencias: Competencia[];
  comp: Competencia;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {CompetenciaData} competenciaService Servicio del componente competencia para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio para los diálogos (mensajes). */
  constructor(private competenciaService: CompetenciaData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {}
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'competencias');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelCompetencias', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCompetencia(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Esta función obtiene las competencias y las asigna al arreglo competencias. */
  getCompetencia(mensajeAct) {
    this.competenciaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.competencias = [];
      this.competencias = data;
      this.dataSource = new MatTableDataSource(this.competencias); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Competencia`);
  }
  /** Funcón que obtiene la información de una competencia según su ID. */
  getCompetenciaById(idCompetencia) {
    this.competenciaService.getCompetenciasById(idCompetencia).subscribe( data => {
      this.comp = data;
    });
  }
  /** Función que permite desprender un modal en el cual se establece la información para crear una nueva competencia. */
  crearCompetencia() {
    this.dialogoService.open(CompetenciasCreateComponent, {
     context: {
       idTipo: 1,
     },
    }).onClose.subscribe(x => {
      this.getCompetencia(0);
    });
  }
  /** Función que despliega un modal el cual permite actualizar la información de una competencia. */
  updateCompetencia(idCompetencias) {
    this.dialogoService.open(CompetenciasCreateComponent, {
      context : {
        idTipo: 2,
        idCompetencias: idCompetencias,
      },
    }).onClose.subscribe(x => {
      this.getCompetencia(1);
    });
  }

  bajaCompetencia(idCompetencias) {
    this.getCompetenciaById(idCompetencias);
    swal({
      title: '¿Deseas dar de baja esta competencia?',
      text: 'Una vez dada de baja, no podrás ver la competencia de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        // @ts-ignore
        this.comp['activo'] = 0;
        this.competenciaService.put(idCompetencias, this.comp).subscribe( result => {
          this.competenciaService.postSocket({comp: this.comp}).subscribe(x => {});
        });
        swal('¡La competencia se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getCompetencia(0);
        }));
      }
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.competencias.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bolsaDeTrabajo.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.connect();
    this.cols = ['escala', 'estado', 'descripcion', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getCompetencia(0);
  }

}
