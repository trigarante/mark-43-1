import { Component} from '@angular/core';

@Component({
  selector: 'ngx-cotizaciones',
template: `
    <router-outlet></router-outlet>
  `,
})
export class CotizacionesComponent {

}
