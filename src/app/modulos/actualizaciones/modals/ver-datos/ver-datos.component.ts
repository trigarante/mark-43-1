import { Component, OnInit, Input } from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';

@Component({
  selector: 'ver-datos',
  templateUrl: './ver-datos.component.html',
  styleUrls: ['./ver-datos.component.scss']
})
export class VerDatosComponent implements OnInit {
  @Input() descripcion: any = 'Descripción en modal';

  constructor(protected ref: NbDialogRef<VerDatosComponent>) { }

  ngOnInit() {
  }

  dismiss() {
    this.ref.close();
  }

}
