import {Observable} from 'rxjs';

export interface Solicitudes {
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    telefono: number;
    correo: string;
    cp: number;
    edad: number;
    empleado: string;
    estado: any;
    fechaCita: Date;
    idBolsaTrabajo: number;
    bolsaTrabajo: string;
    idVacante: number;
    vacante: string;
    documentos: string;
    fechaDescartado: Date;
    idSede: number;
    sede: string;
    idEstadoRH: number;
    motivo: string;
    idReclutador: number;
    comentarios: string;
    idEtapa: number;
}

export interface DocumentosSolicitudes {
  imss: boolean;
  rfc: boolean;
  identificacionOficial: boolean;
  actaNacimiento: boolean;
  comprobanteDomicilio: boolean;
  comprobanteEstudios: boolean;
  curp: boolean;
}


export abstract class SolicitudesData {
  abstract get(): Observable<Solicitudes[]>;

  abstract agendarCita(id, fechaCita);

  abstract getSolicitudById(idSolicitud): Observable<Solicitudes>;

  abstract checkDocumentos(idSolicitud, documentos);

  abstract post(solicitud): Observable<Solicitudes>;

  abstract postSocket(json): Observable<JSON>;

  abstract bajaSolicitud(idSolicitud, solicitud): Observable<Solicitudes>;

  abstract updateSolicitud(idSolicitud, solicitud): Observable<Solicitudes>;
}
