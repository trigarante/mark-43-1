import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RazonSocial, RazonSociosData} from '../../../../../@core/data/interfaces/catalogos/razon-social';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {RazonSocialComponent} from '../../razon-social/razon-social.component';
import {map} from 'rxjs/operators';
@Component({
  selector: 'ngx-razon-social-create',
  templateUrl: './razon-social-create.component.html',
  styleUrls: ['./razon-social-create.component.scss'],
})
export class RazonSocialCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idRazonSocial: number;
  razonSocialCreateForm: FormGroup;
  submitted: boolean;
  razonSocial: any;
  razonEliminado: boolean;
  constructor(protected razonSocialService: RazonSociosData, private fb: FormBuilder,
              protected ref: NbDialogRef<RazonSocialComponent>, private router: Router) {
  }
  guardarRazonSocial() {
    if (this.razonSocialCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.razonSocialService.post(this.razonSocialCreateForm.value).subscribe((result) => {
      this.razonSocialService.postSocket(this.razonSocialCreateForm.value).subscribe(x => {});
      this.razonSocialCreateForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/razon-social']);
      this.ref.close();
      //window.location.reload();
    });
  }

  updateRazonSocial() {
    this.razonSocialService.getAreaById(this.idRazonSocial).subscribe(data => {
      if(data.activo === 1){
        this.razonSocialService.put(this.idRazonSocial, this.razonSocialCreateForm.value).subscribe(result => {
          this.razonSocialService.postSocket(this.razonSocialCreateForm.value).subscribe(x => {});
          this.razonSocialCreateForm.reset();
          this.ref.close();
          //window.location.reload();
        });
      }else{
        this.razonEliminado =true;
      }
    });

  }

  getRazonSocialById() {
    this.razonSocialService.getAreaById(this.idRazonSocial).subscribe(data => {
      this.razonSocial = [];
      this.razonSocial = data;
      this.razonSocialCreateForm.controls['nombreComercial'].setValue(this.razonSocial.nombreComercial);
      this.razonSocialCreateForm.controls['alias'].setValue(this.razonSocial.alias);
    });
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getRazonSocialById();
    }
    this.razonSocialCreateForm = this.fb.group({
      'nombreComercial': new FormControl('', Validators.compose([Validators.required])),
      'alias': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
