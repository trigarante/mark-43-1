import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FinanzasRoutingModule} from './finanzas-routing.module';
import {FinanzasComponent} from './finanzas.component';
import { GruposFinanzasComponent } from './grupos-finanzas/grupos-finanzas.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {TableModule} from 'primeng/table';
import {
  NbAlertModule,
  NbDatepickerModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbTooltipModule,
} from '@nebular/theme';
import {DialogModule, DropdownModule, FileUploadModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import {
  MatAutocompleteModule,
  MatFormFieldModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
} from '@angular/material';
import { EmpresaFinanzasComponent } from './empresa-finanzas/empresa-finanzas.component';
import { SedesFinanzasComponent } from './sedes-finanzas/sedes-finanzas.component';
import { AreasFinanzasComponent } from './areas-finanzas/areas-finanzas.component';
import { SubAreasFinanzasComponent } from './sub-areas-finanzas/sub-areas-finanzas.component';
import { CentroCostoComponent } from './centro-costo/centro-costo.component';
import { CreateGrupoComponent } from './modals/create-grupo/create-grupo.component';
import { CreateEmpresaComponent } from './modals/create-empresa/create-empresa.component';
import { CreateSedeComponent } from './modals/create-sede/create-sede.component';
import { CreateAreaComponent } from './modals/create-area/create-area.component';
import { CreateSubareaComponent } from './modals/create-subarea/create-subarea.component';

@NgModule({
  declarations: [
    FinanzasComponent,
    GruposFinanzasComponent,
    EmpresaFinanzasComponent,
    SedesFinanzasComponent,
    AreasFinanzasComponent,
    SubAreasFinanzasComponent,
    CentroCostoComponent,
    CreateGrupoComponent,
    CreateEmpresaComponent,
    CreateSedeComponent,
    CreateAreaComponent,
    CreateSubareaComponent,
  ],
  imports: [
    CommonModule,
    FinanzasRoutingModule,
    Ng2SmartTableModule,
    TableModule,
    NbDialogModule.forChild(),
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    DropdownModule,
    KeyFilterModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatCardModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    CreateGrupoComponent,
    CreateEmpresaComponent,
    CreateSedeComponent,
    CreateAreaComponent,
    CreateSubareaComponent,
  ],
})
export class FinanzasModule {
}
