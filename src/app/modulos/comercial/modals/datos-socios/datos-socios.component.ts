import {Component, Input, OnInit} from '@angular/core';
import {SociosComercial, SociosData} from '../../../../@core/data/interfaces/comerciales/socios-comercial';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {SociosCreateComponent} from '../socios-create/socios-create.component';
import swal from 'sweetalert';

@Component({
  selector: 'ngx-datos-socios',
  templateUrl: './datos-socios.component.html',
  styleUrls: ['./datos-socios.component.scss'],
})
export class DatosSociosComponent implements OnInit {
  @Input() idSocio: number;
  prioridad: string;
  nombreComercial: string;
  rfc: string;
  razonSocial: string;
  alias: string;
  socio: SociosComercial;
  permisos: any;
  escritura: boolean;

  constructor(private sociosService: SociosData, protected ref: NbDialogRef<SociosComponent>, private dialogService: NbDialogService) {
  }

  updateSocios() {
    this.dialogService.open(SociosCreateComponent, {
      context: {
        idTipo: 2,
        idSocio: this.idSocio,
      },
    });
  }

  bajaSocios() {
    this.getByid(this.idSocio);
    swal({
      title: '¿Deseas dar de baja este banco?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.socio.idEstadoSocio = 2;
        // this.socio = {
        //   'alias': this.socio.alias,
        //   'idEstadoSocio': this.socio.idEstadoSocio,
        //   'nombreComercial': this.socio.nombreComercial,
        //   'prioridad': this.socio.prioridad,
        //   'razonSocial': this.socio.razonSocial,
        //   'rfc': this.socio.rfc,
        //   'estado': this.socio.estado,
        //   'prioridades': this.socio.rfc,
        // };
        // if (this.socio.activo === 'INACTIVO') {
        //   this.socio.activo = 0;
        // }else if (this.socio.activo === 'ACTIVO|') {
        //   this.socio.activo = 1;
        // }
        this.sociosService.put(this.idSocio, this.socio).subscribe(result => {
        });
        swal('¡El banco se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          window.location.reload();
        }));
      }
    });
  }

  getByid(idSocio) {
    this.sociosService.getSociosById(idSocio).subscribe(data => {
      this.socio = data;
    });
  }

  getSociosById() {
    this.sociosService.getSociosById(this.idSocio).subscribe(data => {
      this.prioridad = data.prioridades;
      this.nombreComercial = data.nombreComercial;
      this.rfc = data.rfc;
      this.razonSocial = data.razonSocial;
      this.alias = data.alias;
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getSociosById();
    this.getPermisos();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verSocios.escritura;
  }

}
