import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialLlamadasComponent } from './historial-llamadas.component';

describe('HistorialLlamadasComponent', () => {
  let component: HistorialLlamadasComponent;
  let fixture: ComponentFixture<HistorialLlamadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialLlamadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialLlamadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
