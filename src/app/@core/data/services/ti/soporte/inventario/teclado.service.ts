import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Teclado, TecladoData} from '../../../../interfaces/ti/soporte/inventario/teclado';

@Injectable()
export class TecladoService extends TecladoData {
  private baseURL;
  private socketURl;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Teclado[]> {
    return this.http.get<Teclado[]>(this.baseURL + 'v1/teclado');
  }
  post(teclado: Teclado): Observable<Teclado> {
    return this.http.post<Teclado>(this.baseURL + 'v1/teclado', teclado);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'teclado/saveTeclado', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'teclado/saveBajaTeclado', json);
  }
  getTecladoById(idTeclado): Observable<Teclado> {
    return this.http.get<Teclado>(this.baseURL + 'v1/teclado/' + idTeclado);
  }
  put(idTeclado, teclado: Teclado): Observable<Teclado> {
    return this.http.put<Teclado>(this.baseURL + 'v1/teclado/' + idTeclado + '/'
      + sessionStorage.getItem('Empleado'), teclado);
  }
}
