import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TarjetasBancoVn, TarjetasBancoVnData} from '../../../interfaces/ventaNueva/catalogos/tarjetas-banco-vn';


@Injectable({
  providedIn: 'root',
})
export class TarjetasBancoVnService extends TarjetasBancoVnData {
  private baseURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<TarjetasBancoVn[]> {
    return this.http.get<TarjetasBancoVn[]>(this.baseURL + 'v1/tarjetasvn');
  }
  getTarjetaById(id): Observable<TarjetasBancoVn> {
    return this.http.get<TarjetasBancoVn>(this.baseURL + 'v1/tarjetasvn/' + id);
  }
}
