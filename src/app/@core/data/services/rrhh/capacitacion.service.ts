import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Capacitacion, CapacitacionData} from '../../interfaces/rrhh/capacitacion';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class CapacitacionService extends CapacitacionData {
    private baseURL;
    private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

    get(): Observable<Capacitacion[]> {
        return this.http.get<Capacitacion[]>(this.baseURL + 'v1/capacitacion/' + sessionStorage.getItem('Usuario'));
    }

    post(capacitacion: Capacitacion, idPrecandidato, idEtapa, fechaIngreso): Observable<Capacitacion> {
        return this.http.post<Capacitacion>(this.baseURL + 'v1/capacitacion/' + idPrecandidato + '/' + idEtapa + '/' +
          fechaIngreso, capacitacion);
    }

    postSocket(json): Observable<JSON> {
      return this.http.post<JSON>(this.socketURL + 'capacitacion/saveCapacitacion', json);
    }

    updateCapacitacion(capacitacion: Capacitacion, fechaIngreso): Observable<Capacitacion> {
        return this.http.put<Capacitacion>(this.baseURL + 'v1/capacitacion/' + capacitacion.id + '/' +
          fechaIngreso + '/' + sessionStorage.getItem('Empleado'), capacitacion);
    }

    getCapacitacionById(idCapacitacion): Observable<Capacitacion> {
        return this.http.get<Capacitacion>(this.baseURL + 'v1/capacitacion/get-by-id/' + idCapacitacion);
    }
}
