import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TiComponent} from './ti.component';
import {AdministrativoComponent} from './administrativo/administrativo.component';
import {EjecutivoComponent} from './ejecutivo/ejecutivo.component';
import {UsuarioComponent} from './usuario/usuario.component';
import {AuthGuard} from '../../@core/data/services/login/auth.guard';
import {PermisosComponent} from './permisos/permisos.component';
import {CrearPermisosComponent} from './crear-permisos/crear-permisos.component';
import {SupervisorComponent} from './supervisor/supervisor.component';
import {CoordinadorComponent} from './coordinador/coordinador.component';
import {GerenteComponent} from './gerente/gerente.component';
import {ModificarPermisosComponent} from './modals/modificar-permisos/modificar-permisos.component';
import {ActualizarPermisosComponent} from './actualizar-permisos/actualizar-permisos.component';
import {ExtensionesComponent} from './extensiones/extensiones.component';
import { EliminarPermisosComponent } from './eliminar-permisos/eliminar-permisos.component';
import {ControlLlamadasComponent} from './control-llamadas/control-llamadas.component';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: TiComponent,
  children: [

    {
      path: 'ejecutivo',
      component: EjecutivoComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.usuarioAsignaciones.activo},
    },
    {
      path: 'usuarios',
      component: UsuarioComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.usuariosTi.activo},
    },
    {
      path: 'soporte',
      loadChildren: './soporte/soporte.module#SoporteModule',
      canActivate:
        [AuthGuard],

    },
    {
      path: 'permisos',
      component: PermisosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.grupoPermisos.activo},
    },
    {
      path: 'crear-permisos',
      component: CrearPermisosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.grupoPermisos.escritura},
    },
    {
      path: 'actualizar-permisos/:idGrupo/:idPuesto/:idSubarea',
      component: ActualizarPermisosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.grupoPermisos.escritura},
    },
    {
      path: 'supervisor',
      component: SupervisorComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.supervisor.activo},
    },
    {
      path: 'extensiones',
      component: ExtensionesComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.extensiones.activo},
    },
    {
      path: 'eliminar',
      component: EliminarPermisosComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.reactivarPermisos.activo},
    },
    {
      path: 'control-llamadas',
      component: ControlLlamadasComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.controlLlamadas.activo},
    },
  ],
}];
@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class TiRoutingModule { }
