import {Component, Input, OnInit} from '@angular/core';
import {Sede, SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SedeComponent} from '../../../rrhh/catalogos/sede/sede.component';
import {Router} from '@angular/router';
import {Sepomex, SepomexData} from '../../../../@core/data/interfaces/catalogos/sepomex';
import {Paises, PaisesData} from '../../../../@core/data/interfaces/catalogos/paises';
import {Empresa, EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-create-sede',
  templateUrl: './create-sede.component.html',
  styleUrls: ['./create-sede.component.scss'],
})
export class CreateSedeComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idEmpresa: number;
  @Input() idSede: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una sede. */
  sedeCreateForm: FormGroup;
  /** Arreglo que se inicializa con los valores de la Sede desde el Microservicio. */
  sede: Sede;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores de las colonias desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  /** Arreglo auxiliar para las colonias. */
  colonias: any[];
  /** Arreglo que se inicializa con los valores del País desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para los países. */
  pais: any[];

  /** Arreglo que se inicializa con los valores de la Empresa desde el Microservicio. */
  empresaO: Empresa[];
  /** Arreglo auxiliar para las empresas. */
  empresa: any[];

  /**
   * El método constructor manda a llamar Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {sedeData} sedeService Servicio del componente sede para poder invocar los métodos.
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<SedeComponent>} ref Referencia al modal de sede.
   * @param {Router} router Inicializa variable para poder navegar entre componentes.
   * @param {SepomexData} sepomexService Servicio del componente sepomex para poder invocar los métodos.
   * @param {PaisesData} paisesService Servicio del componente paises para poder invocar los métodos.*/
  constructor(
    private sedeService: SedeData, private fb: FormBuilder, protected ref: NbDialogRef<SedeComponent>,
    private router: Router, protected sepomexService: SepomexData, protected paisesService: PaisesData,
    private empresaService: EmpresaData,
  ) {
  }

  /** Función que permite guardar los datos de una sede, realizando una petición POST al microservicio y redirigiendo
   * la página, además de que da un refresh a la ventana. */
  guardarSede() {
    if (this.sedeCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.sedeService.post(this.sedeCreateForm.value).subscribe(result => {
      this.sedeCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/finanzas/sede']);
    });
  }

  /** Función que permite actualizar los datos de una sede, realizando una petición PUT al microservicio y da un refresh a la ventana. */
  updateSede() {
    this.sedeService.put(this.idEmpresa, this.sedeCreateForm.value).subscribe(result => {
      this.sedeCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que obtiene los paises, filtrándolos por aquellos que estén activos y los almacena en el arreglo pais. */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }

  getEmpresa() {
    this.empresaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.empresaO = result;
      this.empresa = [];
      for (let i = 0; i < this.empresaO.length; i++) {
        this.empresa.push({'label': this.empresaO[i].nombre, 'value': this.empresaO[i].id});
      }
    });
  }

  /** Función que obtiene toda la información de la sede dependiendo de su ID. */
  getSedeById() {
    this.sedeService.getSedeById(this.idEmpresa).subscribe(result => {
      this.sede = result;
      this.sedeCreateForm.controls['idPais'].setValue(this.sede.idPais);
      this.sedeCreateForm.controls['idEmpresa'].setValue(this.sede.idEmpresa);
      this.sedeCreateForm.controls['nombre'].setValue(this.sede.nombre);
      this.sedeCreateForm.controls['cp'].setValue(this.sede.cp);
      this.sedeCreateForm.controls['colonia'].setValue(this.sede.colonia);
      this.sedeCreateForm.controls['calle'].setValue(this.sede.calle);
      this.sedeCreateForm.controls['numero'].setValue(this.sede.numero);
      this.sedeCreateForm.controls['codigo'].setValue(this.sede.codigo);
      this.getColonias(this.sede.cp);
    });
  }

  /** Función que obtiene las colonias y las almacena en el arreglo colonias. */
  getColonias(cp) {
    this.sepomexService.getColoniaByCp(cp)
      .pipe(map(data => this.coloniasSepomex = data)).subscribe(data => {
      this.colonias = [];
      for (let i = 0; i < this.coloniasSepomex.length; i++) {
        this.colonias.push({'label': this.coloniasSepomex[i].asenta, 'value': this.coloniasSepomex[i].idCp});
      }
    });
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que inicializa el componente, así como también la tabla. */

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSedeById();
    }
    this.getPaises();
    this.getEmpresa();
    this.sedeCreateForm = this.fb.group({
      'idPais': new FormControl('', Validators.compose([Validators.required])),
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5), Validators.minLength(5)])),
      'colonia': new FormControl('', Validators.compose([Validators.required])),
      'calle': new FormControl('', Validators.compose([Validators.required])),
      'numero': new FormControl('', Validators.compose([Validators.required])),
      'idEmpresa': this.idEmpresa,
      'activo': 1,
      'codigo': new FormControl('', Validators.compose([Validators.required])),
    });
  }

}
