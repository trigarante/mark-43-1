import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CampanaCategoria, CampanaCategoriaData} from '../../../@core/data/interfaces/marketing/campana-categoria';
import {NbDialogService} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {ActualizarCampanaCategoriaComponent} from '../modals/actualizar-campana-categoria/actualizar-campana-categoria.component';
import {environment} from '../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-campana-categoria',
  templateUrl: './campana-categoria.component.html',
  styleUrls: ['./campana-categoria.component.scss'],
})
export class CampanaCategoriaComponent implements OnInit {
  dataSource: any;
  campanaCateogira: CampanaCategoria[];
  cols: any[];
  // sockets
  private stompClient = null;
  constructor(protected campanaCategoriaService: CampanaCategoriaData, private dialogoService: NbDialogService) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'campaniacategoria');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelCampaniaCategoria', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCampanaCategoria();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getCampanaCategoria() {
    this.campanaCategoriaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.campanaCateogira = [];
      this.campanaCateogira = data;
      this.dataSource = new MatTableDataSource(this.campanaCateogira); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  actualizarCampanaCategoria(idCampanaCategoria) {
    this.dialogoService.open(ActualizarCampanaCategoriaComponent, {
      context: {
        id: idCampanaCategoria,
      },
    }).onClose.subscribe(x => {
      this.getCampanaCategoria();
    });
  }
  verDatos() {
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.connect();
    this.getCampanaCategoria();
    this.cols = ['nombre', 'comentarios', 'prioridad', 'descripcion', 'acciones'];

  }

}
