import {Injectable} from '@angular/core';
import {Presupuesto, PresupuestoData} from '../../interfaces/comerciales/presupuesto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class PresupuestoService extends PresupuestoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Presupuesto[]> {
    return this.http.get<Presupuesto[]>(this.baseURL + 'v1/presupuesto-socio');
  }

  post(presupuesto: Presupuesto): Observable<Presupuesto> {
    return this.http.post<Presupuesto>(this.baseURL + 'v1/presupuesto-socio', presupuesto);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'presupuesto/savePresupuesto/1', json);
  }

  getPresupuestoById(idPresupuesto): Observable<Presupuesto> {
    return this.http.get<Presupuesto>(this.baseURL + 'v1/presupuesto-socio/' + idPresupuesto);
  }

  put(idPresupuesto, presupuesto: Presupuesto): Observable<Presupuesto> {
    return this.http.put<Presupuesto>(this.baseURL + 'v1/presupuesto-socio/' + idPresupuesto + '/'
      + sessionStorage.getItem('Empleado'), presupuesto);
  }
}
