import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {AreaData} from '../../../../../@core/data/interfaces/catalogos/area';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {AreaComponent} from '../../area/area.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-area-create',
  templateUrl: './area-create.component.html',
  styleUrls: ['./area-create.component.scss'],
})
export class AreaCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idArea: number;
  areaCreateForm: FormGroup;

  submitted: boolean;
  /** Arreglo que se inicializa con los valores de la sede desde el Microservicio. */
  sede: Sede[];
  /** Arreglo auxiliar para las sedes. */
  sedes: any[];
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  // Web Sockets
  private stompClient = null;
  constructor(protected sedeSevice: SedeData, protected areaService: AreaData, private fb: FormBuilder,
              protected ref: NbDialogRef<AreaComponent>, private router: Router) {
  }
  // Web socket connection
  connect() {
    const socket = new SockJS('http://localhost:8085/areas');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, frame =>  {
    });
  }
  guardarArea() {
    if (this.areaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.areaService.post(this.areaCreateForm.value).subscribe((result) => {
      this.stompClient.send('/tasks/saveArea', {}, JSON.stringify(this.areaCreateForm.value));
      this.areaCreateForm.reset();
      // this.router.navigate(['/modulos/rrhh/catalogos/area']);
      this.ref.close();
      // window.location.reload();
    });
  }

  getSede() {
    this.sedeSevice.get().pipe(map(data => this.sede = data)).subscribe(data => {
      this.sedes = [];
      for (let i = 0; i < this.sede.length; i++) {
        this.sedes.push({'label': this.sede[i].nombre , 'value': this.sede[i].id});
      }
    });
  }

  getAreaById () {
    this.areaService.getAreaById(this.idArea).subscribe(data => {
      this.areaCreateForm.controls['nombre'].setValue(data.nombre);
      this.areaCreateForm.controls['descripcion'].setValue(data.descripcion);
      this.areaCreateForm.controls['idSede'].setValue(data.idSede);
    });
  }

  updateArea() {
    this.areaService.put(this.idArea, this.areaCreateForm.value).subscribe(
      data => {
        this.inactivo = true;
      },
      error => { },
      () => {
        this.stompClient.send('/tasks/saveArea', {}, JSON.stringify(this.areaCreateForm.value));
        this.ref.close();
        this.areaCreateForm.reset();
      },
    );
  }


  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getAreaById();
    }
    this.getSede();
    this.areaCreateForm = this.fb.group({
      'idSede': new FormControl('', Validators.compose([Validators.required])),
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
