import {Component, OnInit, ViewChild} from '@angular/core';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {AreaCreateComponent} from '../../rrhh/catalogos/modals/area-create/area-create.component';
import swal from 'sweetalert';
import {CreateSubareaComponent} from '../modals/create-subarea/create-subarea.component';
import {CreateAreaComponent} from '../modals/create-area/create-area.component';

@Component({
  selector: 'ngx-areas-finanzas',
  templateUrl: './areas-finanzas.component.html',
  styleUrls: ['./areas-finanzas.component.scss'],
})
export class AreasFinanzasComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  area: Area[];
  areas: Area;
  dataSource: any;
  permisos: any;
  escritura: boolean;

  /** El método constructor únicamente manda a llamar Servicios.
   * En este caso: Servicio de Area.
   * @param {AreaData} areasService Servicio del componente area para poder invocar los métodos. */
  constructor(
    private areasService: AreaData, private dialogoService: NbDialogService,
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getArea() {
    this.areasService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.area = [];
      this.area = data;
      this.dataSource = new MatTableDataSource(this.area); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearSubarea(idArea) {
    this.dialogoService.open(CreateSubareaComponent, {
      context: {
        idTipo: 1,
        idArea: idArea,
      },
    });
  }

  updateArea(idArea) {
    this.dialogoService.open(CreateAreaComponent, {
      context: {
        idTipo: 2,
        idArea: idArea,
      },
    }).onClose.subscribe(x => {
      this.getArea();
    });
  }

  bajaArea(idArea) {
    {
      this.getAreasById(idArea);
      swal({
        title: '¿Deseas dar de baja esta Area?',
        text: 'Una vez dado de baja, no podrás verlo de nuevo.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          confirm: {
            text: 'Dar de baja',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.areas['activo'] = 0;
          this.areasService.put(idArea, this.areas).subscribe(result => {
          });
          swal('¡La area se ha dado de baja exitosamente!', {
            icon: 'success',
          }).then((resultado => {
            this.getArea();
          }));
        }
      });
    }
  }

  getAreasById(idArea) {
    this.areasService.getAreaById(idArea).subscribe(data => {
      this.areas = data;
    });
  }

  /** Función que inicializa el componente, mandando a llamar a getArea para llenar la tabla. */
  ngOnInit() {
    this.getArea();
    this.cols = ['sede', 'nombre', 'descripcion', 'acciones'];
    this.getPermisos();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.areas.escritura;
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
