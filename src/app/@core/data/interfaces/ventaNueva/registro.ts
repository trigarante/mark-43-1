import {Observable} from 'rxjs';

export interface Registro {
  id: number;
  idEmpleado: number;
  idProducto: number;
  idTipoPago: number;
  idEstadoPoliza: number;
  idTipoPoliza: number;
  idProductoSocio: number;
  idFlujoPoliza: number;
  poliza: string;
  fechaInicio: Date;
  primaNeta: number;
  fechaRegistro: Date;
  archivo: string;
}

export abstract class RegistrosData {

  abstract get(): Observable<Registro[]>;

  abstract post(registro): Observable<Registro>;

  abstract getRegistroById(idRegistro): Observable<Registro>;

  abstract put(idRegistro, registro): Observable<Registro>;

}
