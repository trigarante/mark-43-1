import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Seguimiento, SeguimientoData} from '../../interfaces/rrhh/seguimiento';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class SeguimientoService extends SeguimientoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Seguimiento[]> {
    return this.http.get<Seguimiento[]>(this.baseURL + 'v1/seguimiento/' + sessionStorage.getItem('Usuario'));
  }

  post(seguimiento: Seguimiento, idPrecandidato, idEtapa, fechaIngreso): Observable<Seguimiento> {
    return this.http.post<Seguimiento>(this.baseURL + 'v1/seguimiento/' + idPrecandidato + '/' + idEtapa +
      '/' + fechaIngreso, seguimiento);
  }

  postSocketRolplay(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'rollplay/saveRollplay', json);
  }
  postSocketAignaciones(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'asignaciones/saveAsignaciones', json);
  }

  fasePractica(idSeguimiento, calificacionRollPlay, comentariosFaseDos, idEtapa, idPrecandidato): Observable<Seguimiento> {
    return this.http.put<Seguimiento>(this.baseURL + 'v1/seguimiento/etapa-practica/' + idSeguimiento + '/' + calificacionRollPlay
      + '/' + comentariosFaseDos + '/' + idEtapa + '/' + idPrecandidato + '/' + sessionStorage.getItem('Empleado'), idSeguimiento);
  }

  update(idSeguimiento, seguimiento: Seguimiento, fechaIngreso): Observable<Seguimiento> {
    return this.http.put<Seguimiento>(this.baseURL + 'v1/seguimiento/' + idSeguimiento + '/' + fechaIngreso + '/' +
      sessionStorage.getItem('Empleado'), seguimiento);
  }

  getSeguimientoById(idSeguimiento): Observable<Seguimiento> {
    return this.http.get<Seguimiento>(this.baseURL + 'v1/seguimiento/get-by-id/' + idSeguimiento);
  }

}




