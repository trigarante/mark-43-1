import {Observable} from 'rxjs';

export interface BancoVn {
  id: string;
  nombre: string;
  activo: string;
  idCarrier: number;
}

export abstract class BancoVnData {
  abstract get(): Observable<BancoVn[]>;
  abstract getBancoById(id): Observable<BancoVn>;
  abstract post(banco): Observable<BancoVn>;
  abstract put(id, banco): Observable<BancoVn>;
}
