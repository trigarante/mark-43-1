import {Component, OnInit, ViewChild} from '@angular/core';
import {SociosBaja, SociosBajaData} from '../../../../@core/data/interfaces/comerciales/catalogo/socios-baja';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TipoBajaSociosCreateComponent} from '../modals/tipo-bajaSocios-create/tipo-bajaSocios-create';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-baja-socios',
  templateUrl: './baja-socios.component.html',
  styleUrls: ['./baja-socios.component.scss'],
})
export class BajaSociosComponent implements OnInit {
  cols: any[];
  bajaSocios: SociosBaja[];
  bajaSociosId: SociosBaja;
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(
    private sociosBajaService: SociosBajaData,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private router: Router,
  ) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bajasocios');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelBajaSocios', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getBajaSocios(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getBajaSocios(mensajeAct) {
    this.sociosBajaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.bajaSocios = data;
      this.dataSource = new MatTableDataSource(this.bajaSocios); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }


  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Vacantes`);
  }

  crearBajaSocios() {
    this.dialogService.open(TipoBajaSociosCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getBajaSocios(0));
  }

  updateTipoBajaSocios(idTipoBajaSocios) {
    this.dialogService.open(TipoBajaSociosCreateComponent, {
      context: {
        idTipo: 2,
        idBajaSocios: idTipoBajaSocios,
      },
    }).onClose.subscribe( x => this.getBajaSocios(1));
  }

  getSocioById(idSocio) {
    this.sociosBajaService.getSocioBajaById(idSocio).subscribe(data => {
      this.bajaSociosId = data;
    });
  }

  bajasSocios(idSocio) {
    this.getSocioById(idSocio);
    swal({
      title: '¿Deseas dar de baja este socio?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.bajaSociosId.activo = 0;
        this.sociosBajaService.put(idSocio, this.bajaSociosId).subscribe( result => {
          this.sociosBajaService.postSocket({status: 'ok'}).subscribe(X => {});
        });
        swal('¡El socio se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getBajaSocios(0);
        }));
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.getBajaSocios(0);
    this.cols = ['tipo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bajasSocios.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bajasSocios.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
