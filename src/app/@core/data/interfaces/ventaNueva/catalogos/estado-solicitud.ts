import {Observable} from 'rxjs';

export interface EstadoSolicitud {
  id: number;
  estado: string;
  activo: number;
}

export abstract class EstadoSolicitudData {
  abstract get(): Observable<EstadoSolicitud[]>;

  abstract post(estadoSolicitud): Observable<EstadoSolicitud>;

  abstract getAreaById(idEstadoSolicitud): Observable<EstadoSolicitud>;

  abstract put(idEstadoSolicitud, estadoSolicitud): Observable<EstadoSolicitud>;
}
