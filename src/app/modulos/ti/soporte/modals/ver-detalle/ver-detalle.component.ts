import {Component, Input, OnInit} from '@angular/core';
import {Cpu, CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {Monitor, MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Laptop, LaptopData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {Diadema, DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {Tablet, TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Teclado, TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Mouse, MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {NbDialogRef} from '@nebular/theme';
import {Switchs, SwitchsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {Servidores, ServidoresData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {TarjetaRed, TarjetaRedData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIp, TelefonoIpData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {Racks, RacksData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanel, PatchPanelData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRack, CharolasRackData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {
  DiscoDuroExterno,
  DiscoDuroExternoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {
  DiscoDuroInterno,
  DiscoDuroInternoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBit, ChromeBitData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {Aps, ApsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {
  Biometricos,
  BiometricosData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {Dvr, DvrData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {Camaras, CamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {
  BarrasMulticontacto,
  BarrasMulticontactoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {
  FuenteCamaras,
  FuenteCamarasData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';

@Component({
  selector: 'ngx-ver-detalle',
  templateUrl: './ver-detalle.component.html',
  styleUrls: ['./ver-detalle.component.scss'],
})
export class VerDetalleComponent implements OnInit {
  /** Variable que se obtiene de un componente padre - Inventario, obtiene el id de equipo y el tipo de Equipo*/
  @Input() id: number;
  @Input() opcion: string;
  cpu: Cpu;
  monitor: Monitor;
  laptop: Laptop;
  diadema: Diadema;
  tablet: Tablet;
  teclado: Teclado;
  mouse: Mouse;
  switch: Switchs;
  servidores: Servidores;
  tarjetaRed: TarjetaRed;
  telefonoIp: TelefonoIp;
  racks: Racks;
  patchPanel: PatchPanel;
  charolaRack: CharolasRack;
  /** Variable para el arreglo discoduroExterno   soporte*/
  ddexterno: DiscoDuroExterno;
  ddinterno: DiscoDuroInterno;
  chromeBit: ChromeBit;
  aps: Aps;
  biometricos: Biometricos;
  dvr: Dvr;
  camara: Camaras;
  barraMulti: BarrasMulticontacto;
  corrienteCam: FuenteCamaras;

  constructor(private cpuService: CpuData, private monitorService: MonitorDate,
              private laptopService: LaptopData, private diademaService: DiademaData,
              private tabletService: TabletData, private tecladoService: TecladoData,
              private mouseService: MouseData, protected ref: NbDialogRef<VerDetalleComponent>,
              private switchsService: SwitchsData, private servidoresService: ServidoresData,
              private tarjetaRedService: TarjetaRedData, private telefonoIdService: TelefonoIpData, private racksService: RacksData,
              private patchPanelService: PatchPanelData, private charolasRackService: CharolasRackData,
              private ddexternoService: DiscoDuroExternoData, public ddinternoService: DiscoDuroInternoData,
              private chromeBitServices: ChromeBitData, private apsService: ApsData, private biometricosService: BiometricosData,
              private dvrService: DvrData, private camaraService: CamarasData, private barraMultiService: BarrasMulticontactoData,
              private fuenteCorrienteService: FuenteCamarasData) { }
    menu(opcion) {
    console.log(opcion)
      switch (opcion) {

        case 'cpu':
          this.cpuService.getCpuById(this.id).subscribe(data => {
            this.cpu = data;
          });
          break;
        case 'monitor':
          this.monitorService.getMonitorById(this.id).subscribe(data => {
            this.monitor = data;
          });
          break;
        case 'laptop':
          this.laptopService.getLaptopById(this.id).subscribe(data => {
            this.laptop = data;
          });
           break;
        case 'tablet':
          this.tabletService.getTabletById(this.id).subscribe(data => {
            this.tablet = data;
          });
          break;
        case 'teclado':
          this.tecladoService.getTecladoById(this.id).subscribe(data => {
            this.teclado = data;
          });
          break;
        case 'mouse':
          this.mouseService.getMouseById(this.id).subscribe(data => {
            this.mouse = data;
          });
          break;
        case 'diadema':
          this.diademaService.getDiademaById(this.id).subscribe(data => {
            this.diadema = data;
          });
          break;
        case 'switch':
          this.switchsService.getSwitchsById(this.id).subscribe(data => {
            this.switch = data;
          });
          break;
        case 'servidores':
          this.servidoresService.getServidoresById(this.id).subscribe(data => {
            this.servidores = data;
          });
          break;
        case 'tarjeta de red':
          this.tarjetaRedService.getTarjetaRedById(this.id).subscribe(data => {
            this.tarjetaRed = data;
          });
          break;
        case 'telefonos ip':
          this.telefonoIdService.getTelefonoIpById(this.id).subscribe(data => {
            this.telefonoIp = data;
          });
          break;
        case 'rack':
          this.racksService.getRacksById(this.id).subscribe(data => {
            this.racks = data;
          });
          break;
        case 'patch panel':
           this.patchPanelService.getPatchPanelById(this.id).subscribe(data => {
             this.patchPanel = data;
           });
           break;
        case 'charolas rack':
          this.charolasRackService.getCharolasRackById(this.id).subscribe(data => {
            this.charolaRack = data;
          });
          break;
        case 'disco duro externo':
          this.ddexternoService.getDiscoExternoById(this.id).subscribe(data => {
            this.ddexterno = data;
          });
          break;
        case 'discoduroexterno':
          this.ddexternoService.getDiscoExternoById(this.id).subscribe(data => {
            this.ddexterno = data;
          });
          break;
        case 'disco duro interno':
          this.ddinternoService.getDiscoInternoById(this.id).subscribe(data => {
            this.ddinterno = data;
          });
          break;
        case 'chrome bit':
          this.chromeBitServices.getChromeBit(this.id).subscribe(data => {
            this.chromeBit = data;
          });
          break;
        case 'aps':
          this.apsService.getApsById(this.id).subscribe(data => {
            this.aps = data;
          });
          break;
        case 'biometricos':
          this.biometricosService.getBiometricosById(this.id).subscribe(data => {
            this.biometricos = data;
          });
          break;
        case 'dvr':
          this.dvrService.getDvrById(this.id).subscribe(data => {
            this.dvr = data;
          });
          break;
        case 'camara':
          this.camaraService.getCamarasById(this.id).subscribe(data => {
            this.camara = data;
          });
          break;
        case 'barra-multicontacto':
          this.barraMultiService.getBarrasMulticontactoById(this.id).subscribe(data => {
            this.barraMulti = data;
          });
          break;
        case 'corriente-camara':
          this.fuenteCorrienteService.getFuenteCamaraById(this.id).subscribe(data => {
            this.corrienteCam = data;
          });
          break;
        default:
          break;
      }
    }

  dismiss() {
    this.ref.close();
  }
    ngOnInit() {
    this.menu(this.opcion);
  }


}
