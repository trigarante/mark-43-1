import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GenerarPreguntasComponent} from './generar-preguntas.component';

describe('GenerarPreguntasComponent', () => {
  let component: GenerarPreguntasComponent;
  let fixture: ComponentFixture<GenerarPreguntasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerarPreguntasComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarPreguntasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
