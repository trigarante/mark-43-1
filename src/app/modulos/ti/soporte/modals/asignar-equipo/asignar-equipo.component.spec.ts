import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarEquipoComponent } from './asignar-equipo.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {ApsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {EmpleadosData} from '../../../../../@core/data/interfaces/rrhh/empleados';
import {CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {LaptopData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {ServidoresData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {SwitchsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {TarjetaRedData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIpData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {RacksData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanelData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRackData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {DiscoDuroExternoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {DiscoDuroInternoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBitData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {BiometricosData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {DvrData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {CamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {BarrasMulticontactoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {FuenteCamarasData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';

describe('AsignarEquipoComponent', () => {
  let component: AsignarEquipoComponent;
  let fixture: ComponentFixture<AsignarEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarEquipoComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        EmpleadosData,
        CpuData,
        MonitorDate,
        LaptopData,
        DiademaData,
        TabletData,
        TecladoData,
        ServidoresData,
        MouseData,
        SwitchsData,
        TarjetaRedData,
        TelefonoIpData,
        RacksData,
        PatchPanelData,
        CharolasRackData,
        DiscoDuroExternoData,
        DiscoDuroInternoData,
        ChromeBitData,
        DiscoDuroInternoData,
        ChromeBitData,
        ApsData,
        BiometricosData,
        DvrData,
        CamarasData,
        BarrasMulticontactoData,
        FuenteCamarasData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarEquipoComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
