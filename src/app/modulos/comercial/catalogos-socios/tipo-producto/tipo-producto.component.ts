import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoProducto, TipoProductoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-producto';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {TipoProductoCreateComponent} from '../modals/tipo-producto-create/tipo-producto-create.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-producto',
  templateUrl: './tipo-producto.component.html',
  styleUrls: ['./tipo-producto.component.scss'],
})
export class TipoProductoComponent implements OnInit {
  cols: any[];
  tipoProducto: TipoProducto[];
  tipoProductos: TipoProducto;
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private tipoProductoService: TipoProductoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tipoproducto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoProducto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoProducto(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getTipoProducto(mensajeAct) {
    this.tipoProductoService.get().pipe(map( result => {
      return result.filter( data => data.activo === 1);
    })).subscribe( data => {
      this.tipoProducto = data;
      this.dataSource = new MatTableDataSource(this.tipoProducto); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

/** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Producto`);
  }

  crearProducto() {
    this.dialogService.open(TipoProductoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoProducto(0));
  }

  updateTipoProducto(idTipoProducto) {
    this.dialogService.open(TipoProductoCreateComponent, {
      context: {
        idTipo: 2,
        idTipoProducto: idTipoProducto,
      },
    }).onClose.subscribe( x => this.getTipoProducto(1));
  }

  bajaTipoProducto(idTipoProducto) {
    this.getTipoProductosbyId(idTipoProducto);
    swal({
      title: '¿Deseas dar de baja este Tipo Producto?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoProductos.activo = 0;
        this.tipoProductoService.put(idTipoProducto, this.tipoProductos).subscribe( result => {
          this.tipoProductoService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El Tipo Producto se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoProducto(0);
        }));
      }
    });
  }

  getTipoProductosbyId(idTipoProducto) {
    this.tipoProductoService.getTipoProducto(idTipoProducto).subscribe(data => {
      this.tipoProductos = data;
    });
  }

  ngOnInit() {
    this.connect();
    this.getTipoProducto(0);
    this.cols = ['tipo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoProducto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoProducto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
