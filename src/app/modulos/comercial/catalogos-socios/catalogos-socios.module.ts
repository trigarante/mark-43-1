import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CatalogosSociosRoutingModule} from './catalogos-socios-routing.module';
import {ThemeModule} from '../../../@theme/theme.module';
import {TableModule} from 'primeng/table';
import {NbDialogModule} from '@nebular/theme';
import {AutoCompleteModule, DropdownModule, KeyFilterModule, TooltipModule} from 'primeng/primeng';
import { CatalogosSociosComponent } from './catalogos-socios.component';
import { TipoCategoriaComponent } from './tipo-categoria/tipo-categoria.component';
import { TipoCompetenciaComponent } from './tipo-competencia/tipo-competencia.component';
import { TipoProductoComponent } from './tipo-producto/tipo-producto.component';
import { TipoCategoriaCreateComponent } from './modals/tipo-categoria-create/tipo-categoria-create.component';
import { TipoCompetenciaCreateComponent } from './modals/tipo-competencia-create/tipo-competencia-create.component';
import { TipoProductoCreateComponent } from './modals/tipo-producto-create/tipo-producto-create.component';
import { TipoCoberturaComponent } from './tipo-cobertura/tipo-cobertura.component';
import { TipoCoberturaCreateComponent } from './modals/tipo-cobertura-create/tipo-cobertura-create.component';
import {BajaSociosComponent} from './baja-socios/baja-socios.component';
import { SubCategoriaComponent } from './sub-categoria/sub-categoria.component';
import { DivisasComponent } from './divisas/divisas.component';
import {
  MatCardModule, MatFormFieldModule, MatBottomSheetModule, MatButtonModule, MatDialogModule, MatFormFieldControl,
  MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule,
  MatTableModule, MatToolbarModule, MatTooltipModule, MatChipsModule, MatDatepickerModule, MatNativeDateModule,
} from '@angular/material';
import { SubcategoriaCreateComponent } from './modals/subcategoria-create/subcategoria-create.component';
import { DivisaCreateComponent } from './modals/divisa-create/divisa-create.component';
import { TipoRamoComponent } from './tipo-ramo/tipo-ramo.component';
import { TipoRamoCreateComponent } from './modals/tipo-ramo-create/tipo-ramo-create.component';
import { TipoSubramoComponent } from './tipo-subramo/tipo-subramo.component';
import { TipoSubramoCreateComponent } from './modals/tipo-subramo-create/tipo-subramo-create.component';
import {CategoriaComponent} from './categoria/categoria.component';
import {CategoriaCreateComponent} from './modals/categoria-create/categoria-create.component';
import {TipoBajaSociosCreateComponent} from './modals/tipo-bajaSocios-create/tipo-bajaSocios-create';

@NgModule({
  declarations: [
    CatalogosSociosComponent,
    TipoCategoriaComponent,
    TipoCompetenciaComponent,
    TipoProductoComponent,
    TipoCategoriaCreateComponent,
    TipoCompetenciaCreateComponent,
    TipoProductoCreateComponent,
    TipoCoberturaComponent,
    TipoCoberturaCreateComponent,
    BajaSociosComponent,
    SubCategoriaComponent,
    DivisasComponent,
    SubcategoriaCreateComponent,
    DivisaCreateComponent,
    TipoRamoComponent,
    TipoRamoCreateComponent,
    TipoSubramoComponent,
    TipoSubramoCreateComponent,
    CategoriaComponent,
    CategoriaCreateComponent,
    TipoBajaSociosCreateComponent,
  ],
  imports: [
    CommonModule,
    CatalogosSociosRoutingModule,
    ThemeModule,
    TableModule,
    NbDialogModule.forChild(),
    DropdownModule,
    AutoCompleteModule,
    TooltipModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatOptionModule,
    MatPaginatorModule,
    MatTableModule,
    KeyFilterModule,
  ],
  entryComponents: [
    TipoCategoriaCreateComponent,
    TipoCompetenciaCreateComponent,
    TipoProductoCreateComponent,
    TipoCoberturaCreateComponent,
    SubcategoriaCreateComponent,
    DivisaCreateComponent,
    TipoRamoCreateComponent,
    TipoSubramoCreateComponent,
    CategoriaCreateComponent,
    TipoBajaSociosCreateComponent,
  ],
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
  ],
})
export class CatalogosSociosModule {
}
