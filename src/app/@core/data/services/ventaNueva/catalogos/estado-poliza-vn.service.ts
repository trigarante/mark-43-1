import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {EstadoPolizaVn, EstadoPolizaVnData} from '../../../interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class EstadoPolizaVnService extends EstadoPolizaVnData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = Global.url;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  get(): Observable<EstadoPolizaVn[]> {
    return this.http.get<EstadoPolizaVn[]>(this.baseURL + 'estado-poliza');
  }

  post(estadoPoliza: EstadoPolizaVn): Observable<EstadoPolizaVn> {
    return this.http.post<EstadoPolizaVn>(this.baseURL + 'estado-poliza', estadoPoliza);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vnestadopoliza/saveVnEstadoPoliza', json);
  }

  getEstadoPolizaById(idEstadoPoliza): Observable<EstadoPolizaVn> {
    return this.http.get<EstadoPolizaVn>(this.baseURL + 'estado-poliza/' + idEstadoPoliza);
  }

  put(idEstadoPoliza, estadoPoliza: EstadoPolizaVn): Observable<EstadoPolizaVn> {
    return this.http.put<EstadoPolizaVn>(this.baseURL + 'estado-poliza/' + idEstadoPoliza, estadoPoliza);
    // + sessionStorage.getItem('Empleado')
  }
}
