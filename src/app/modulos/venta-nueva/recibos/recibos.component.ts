import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EstadoRecibos, EstadoRecibosData} from '../../../@core/data/interfaces/ventaNueva/catalogos/estado-recibos';
import {map} from 'rxjs/operators';
import {RecibosData} from '../../../@core/data/interfaces/ventaNueva/recibos';
import {SolicitudesVNData} from '../../../@core/data/interfaces/ventaNueva/solicitudes';

@Component({
  selector: 'ngx-recibos',
  templateUrl: './recibos.component.html',
  styleUrls: ['./recibos.component.scss'],
})
export class RecibosComponent implements OnInit {

  reciboForm: FormGroup;
  estadosRecibos: EstadoRecibos[];
  idRegistro: number;

  constructor(
    private fb: FormBuilder,
    private estadoReciboService: EstadoRecibosData,
    private recibosService: RecibosData,
    private solicitudesService: SolicitudesVNData,
  ) { }

  ngOnInit() {
    this.solicitudesService.returnIdRegistroPoliza().subscribe(res => {
      this.idRegistro = res;
    });
    this.reciboForm = this.fb.group({
      'idRegistro': new FormControl(''),
      'idEmpleado': new FormControl(74),
      'idEstadoRecibos': new FormControl('', Validators.required),
      'numero': new FormControl('', Validators.required),
      'cantidad': new FormControl('', Validators.required),
      'fechaVigencia': new FormControl('', Validators.required),
      'fechaLiquidacion': new FormControl('', Validators.required),
    });
    this.getEstadoRecibos();
  }

  getEstadoRecibos () {
    this.estadoReciboService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(
      value => { this.estadosRecibos = value; },
    );
  }

  crearRegistro() {
    this.reciboForm.controls['idRegistro'].setValue(this.idRegistro);
    this.recibosService.post(this.reciboForm.value).subscribe(
      result => {
        // console.log(result.id);
      },
    );
  }

}
