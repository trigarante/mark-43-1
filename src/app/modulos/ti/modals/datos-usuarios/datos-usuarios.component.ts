import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {CorreoData, Alias, UnidadOrganizativa} from '../../../../@core/data/interfaces/ti/correo';
import {map, startWith} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ErrorStateMatcher} from '@angular/material/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
export class MyErrorStateMatcher implements ErrorStateMatcher {
  constructor(private unidadesOrganizativasDisponibles: string []) {}

  isErrorState(myControl: FormControl): boolean {
    return !(myControl && (this.unidadesOrganizativasDisponibles.includes(myControl.value) === true)
                && (myControl.dirty || myControl.touched));
  }
}
@Component({
  selector: 'ngx-datos-usuarios',
  templateUrl: './datos-usuarios.component.html',
  styleUrls: ['./datos-usuarios.component.scss'],
})
export class DatosUsuariosComponent implements OnInit {
  @Input() idUsuario: number;
  @Input() usuario: string;
  @Input() idCorreo: string;
  tipodominio: string;
  cols: any[];
  datosUsuarios: string;
  correo: string;
  alias: any;
  datosUsuariosJson: any;
  totalalias: any;
  nombre: string;
  idEmpleado: number;
  idExtension: string;
  idCampania: number;
  idProtocolo: number;
  idPerfilMarcacion: number;
  dataSource: any;
  unidadOrganizativa: string;
  seleccionUnidadOrganizativa: boolean = false;
  unidadesOrganizativasDisponibles: string[] = [];
  // Variables para el input de modificar unidad organizativa
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  unidadOrganizativaExiste: MyErrorStateMatcher;
  // socket
  private stompClient1 = null;
  permisos: any;
  escritura: boolean;

  constructor( protected ref: NbDialogRef<DatosUsuariosComponent>,  private usuarioService: UsuarioData,
               private correoService: CorreoData) {}
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    const socket = new SockJS( environment.GLOBAL_SOCKET + 'datostiusuarios');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient1.subscribe('/task/panelDatosTiUsuarios', (content) => {
        console.log(content.body);
        setTimeout(() => {
          _this.getDatosUsuarios();
        }, 500);
      });
    });
  }
  /** esta funcion obtene los datosUsuarios de los Usuarios*/
  getDatosUsuarios() {
    this.usuarioService.getUsuarioById(this.idUsuario).subscribe(
      data => {
        this.datosUsuarios = data.datosUsuario;
        this.tipodominio = data.tipoUsuario;
        this.nombre = data.nombre;
        if (this.datosUsuarios != null) {
          this.datosUsuariosJson = JSON.parse(this.datosUsuarios);
          this.idEmpleado = data.idEmpleado;
          // this.idEmpleado = this.datosUsuariosJson.idEmpleado;
          this.idExtension = this.datosUsuariosJson.idExtension;
          this.idCampania = this.datosUsuariosJson.idCampania;
          this.idProtocolo = this.datosUsuariosJson.idProtocolo;
          this.idPerfilMarcacion = this.datosUsuariosJson.idPerfilMarcacion;
        }
      },
      error => {},
      () => {
        this.correoService.getById(this.tipodominio, this.idCorreo).subscribe(data => {
          this.unidadOrganizativa = data.orgUnitPath;
        });

        this.correoService.getAlias(this.tipodominio, this.idCorreo).subscribe(data => {
          this.alias = data;
          this.dataSource = new MatTableDataSource(this.alias);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        this.correoService.getUnidadesOrganizativas(this.tipodominio).subscribe(data => {
          this.unidadesOrganizativasDisponibles = [];

          for (const nombre of data) {
            this.unidadesOrganizativasDisponibles.push(nombre.orgUnitPath);
          }
        });
      },
    );
  }

  /** Función que despliega un modal para seleccionar si se da de baja un Alias. */
  borrarAlias(alias) {
    swal({
      title: '¿Deseas dar de baja este Alias?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {

          this.correoService.deleteAlias(this.tipodominio, this.idCorreo, alias ).subscribe( result => {
            this.correoService.postSocketDatosTiUsuarios({status: 'ok'}).subscribe(x => {});
         });
        swal('¡El Alias se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getDatosUsuarios();
        }));
      }
    });
  }

  activarDropDown() {
    this.unidadOrganizativaExiste = new MyErrorStateMatcher(this.unidadesOrganizativasDisponibles);
    this.seleccionUnidadOrganizativa = !this.seleccionUnidadOrganizativa;
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  guardarUnidadOrganizativa() {
    if (!this.unidadOrganizativaExiste.isErrorState(this.myControl)) {
      const datos = {
        'id': this.idCorreo,
        'orgUnitPath': this.myControl.value.toString(),
      };

      this.correoService.enviarByAlias(this.tipodominio, datos).subscribe();
      swal('¡La unidad organizativa se ha dado de modificado exitosamente!', {
        icon: 'success',
      }).then((resultado => {
        this.dismiss();
      }));
      // this.seleccionUnidadOrganizativa = !this.seleccionUnidadOrganizativa;
    }
  }

  /** Esta función cierra el modal.*/
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.connect();
    this.getDatosUsuarios();
    this.cols = ['Alias', 'Borrar Alias'];
      // {field: 'alias', header: 'Alias'},
      // {field: 'boton', header: 'Borrar alias'},
    // ];
    this.getPermisos();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.usuariosTi.escritura;
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.unidadesOrganizativasDisponibles.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

}
