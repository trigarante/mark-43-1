import {Injectable} from '@angular/core';
import {AreasInterface} from '../../interfaces/rrhh/areas-interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {IconInterface} from '../../interfaces/rrhh/icon-interface';
import {PreguntasInterfaces} from '../../interfaces/rrhh/preguntas-interfaces';
import {puts} from 'util';
import * as url from 'url';
import {SedesInterface} from '../../interfaces/rrhh/sedes-interface';
import {RespuestasInterfaces} from '../../interfaces/rrhh/respuestas-interface';


@Injectable(
  {providedIn: 'root'},
)
export class AreasService {
  public area: AreasInterface[];

  constructor(private httpClient: HttpClient) {

  }

  /**
   * Lee los valores de la BD conforme a la interfaz declarada
   */
  getAreas() {
    return this.httpClient
      .get<AreasInterface[]>(environment.apiUrl + 'v1/cultura-bienestar/areas');
  }

  getAreasById(idArea: number) {
    return this.httpClient
      .get<AreasInterface>(environment.apiUrl + 'v1/cultura-bienestar/areas-by-id/' + idArea);
  }

  getAreasByIdSede(idSede: number) {
    return this.httpClient
      .get<AreasInterface[]>(environment.apiUrl + 'v1/areas/active-areas/' + idSede);
  }

  addQuestion(pregunta: any) {
    return this.httpClient
      .post<PreguntasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/preguntas', pregunta);
  }

  getPreguntas() {
    return this.httpClient
      .get<AreasInterface[]>(environment.apiUrl + 'v1/cultura-bienestar/preguntas');
  }

  getIconos() {
    return this.iconos;
  }

  iconos: IconInterface[] = [
    {repositorio: 'mood', name: 'mood', checked: false},
    {repositorio: 'mood_bad', name: 'mood_bad', checked: false},
    {repositorio: 'sentiment_dissatisfied', name: 'sentiment_dissatisfied', checked: false},
    {repositorio: 'thumb_up_alt', name: 'thumb_up_alt', checked: false},
    {repositorio: 'sentiment_very_dissatisfied', name: 'sentiment_very_dissatisfied', checked: false},
    {repositorio: 'check', name: 'check', checked: false},
    {repositorio: 'close', name: 'close', checked: false},
    {repositorio: 'thumb_down_alt', name: 'thumb_down_alt', checked: false},
    {repositorio: 'star_half', name: 'star_half', checked: false},
    {repositorio: 'star_border', name: 'star_border', checked: false},
    {repositorio: 'star', name: 'star', checked: false},

  ];

  getRespuestaByPregunta(idR: number) {
    {
      return this.httpClient
        .get<RespuestasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/respuestas-by-pregunta/' + idR);
    }
  }

  createRes(value: any) {
    return this.httpClient
      .post(environment.apiUrl + 'v1/cultura-bienestar/respuestas', value)
      .subscribe(
        data => {
          console.log('Put Request is successful ', data);
        },
        error => {
          console.log('Error', error);
        },
      );
  }

  public getPreguntasByArea(idP: number) {
    return this.httpClient
      .get<PreguntasInterfaces[]>(environment.apiUrl + 'v1/cultura-bienestar/preguntas-by-area/' + idP);
  }
}

