import {Component, Input, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {SaveComponent} from '../save/save.component';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {Correo, CorreoData, Domains} from '../../../../@core/data/interfaces/ti/correo';
import {SelectItem} from 'primeng/api';
import {
  LicenciasDisponibles,
  LicenciasDisponiblesData,
} from '../../../../@core/data/interfaces/ti/licencias-disponibles';


@Component({
  selector: 'ngx-crear-auditoria',
  templateUrl: './crear-auditoria.component.html',
  styleUrls: ['./crear-auditoria.component.scss'],
})
export class CrearAuditoriaComponent implements OnInit {
  @Input() id: number;
  @Input() subareax: number;
  /** Inicializa el formulario*/
  auditoriaForm: FormGroup;
  tiposdominios: SelectItem[];
  /**arreglo de todos los usuarios disponibles por subarea*/
  usuario: Usuario[];
  /** toma los nombres de los correos*/
  correosdisponibles: any[];
  /** inicializa arrelo de permisos*/
  grupoUsuario: GrupoUsuarios[];
  grupUsuario: any[];
  /** inicializa arrelo de dominios*/
  domainss: any[];
  domains: Domains[];
  /**  se obtenie un arreglo empleado*/
  empleado: Empleados;
  /** obtiene un arreglo de id de correos del api */
  idCorreoslista: any[];
  /** variable que guarda el correoprimario para remplazar*/
  correoprimario: string;
  idCorreoprincipal: string;
  dominiocorto: RegExp= /trigarante.com/;
  dominiocorto2: RegExp = /nexosmedia.com/;
  dominiocorto3: RegExp = /ahorraseguros.mx/;

  tipodominio: string;
  idCorreo: any[];
  /**Obtiene el id de Usuario seleccionado*/
  idUsuario: number;
  /** arreglos para modificar el usuario*/
  usuarioput: Usuario;
  usuarioput1: Usuario;
  datosUsuario: any;
  datosGrupos: any;
  datosUsuarios: object;
  usuario3: Usuario;
  extensionput: Extension;
  extensionput1: Extension;
  bandera: boolean = true;
  /** variable para obtener las extensiones*/
  extensiones: any[];
  /** arreglo para obtener las extensiones disponibles*/
  extension: Extension[];
  extensionbandera: boolean;
  /** bandera que se pone en true cuando no hay ningun correo disponible*/
  bandera2: boolean ;
  extensionusuario: string;
  correoUsuario: string;
  emailusuario: string;
  n: number;
  banderaemailmodificado: boolean;
  estadoCorreo3: boolean = false;
  estadoCorreo5: boolean;
  estadofinal5: string;
  estadofinal: string;
  correofinal: string;
  estadoCorreo2: boolean;
  nombredominio: string;
  correo: Correo[];
  usuario2: Usuario;
  /** variable para obtener el correo de Usuario Nuevo*/
  corrreoNuevo: string;
  idCorreoNuevo: string;
  banderacorreousado: boolean = false;
  licencias: LicenciasDisponibles[];
  correoAdministrativos: Correo[];
  banderalicencias: boolean;
  tipoLicencia: number;
  estadofindalias: boolean;

  constructor( private fb: FormBuilder, private usuarioService: UsuarioData, protected ref: NbDialogRef<CrearAuditoriaComponent>,
               private grupoUsuariosService: GrupoUsuariosData,  private empleadosService: EmpleadosData ,
               private extensionesService: ExtensionData, public dialogService: NbDialogService,
               private correoService: CorreoData, private licenciasService: LicenciasDisponiblesData,
               private toastrService: NbToastrService) {
    this.auditoriaForm = this.fb.group({
      'privilegioscontrol': new FormControl('', Validators.compose([Validators.required])),
      'emailprincipal': new FormControl(),
      'correosdisponibles': new FormControl(),
      'correosdisponibleSubareas' : new FormControl(),
      'extensioncontrol' : new FormControl(),
      'emailmodificado' : new FormControl(),
      'dominio' : new FormControl(),
      'tiposdominios' : new FormControl(),
    });
  }
  getEmpleados() {
    this.empleadosService.getEmpleadoById(this.id).subscribe(data => {
      this.empleado = data;
    });
  }

  getExtensiones() {
    this.extensionesService.getExtensionByIdSubarea(this.subareax, 0).pipe(map(data => this.extension = data)).subscribe(data => {
      this.extensiones = [];
      for (let i = 0; i < this.extension.length; i++) {
        this.extensiones.push({'label': this.extension[i].id, 'value': this.extension[i].id});
      }
    });
  }
  /** ve los correos disponibles por subarea*/
  getcorreosdisponibles(subarea) {
    this.usuarioService.getUsuarioByIdSubarea(subarea, 0).pipe(map(data => this.usuario = data)).subscribe(data => {
      this.correosdisponibles = [];
      this.idCorreoslista = [];
      this.idCorreo = [];

      for (let i = 0; i < this.usuario.length; i++) {
        // saca los correos de los usaurios dados de baja
        this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
         // saca los id de los correos de los usuarios que esten dados de baja
         this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
         this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
      }
      if (this.usuario.length !== 0) {
        /** si hay correos disponibles por area pone bandera a true*/
        this.bandera = true;
      } else if (this.usuario.length === 0) { // si no hay correos disponibles toma correos de todas las subareas
        this.bandera = false;
        this.getcorreosdisponiblesSubareas();
      }
    });
  }
  getcorreosdisponiblesSubareas() {
    if (this.bandera === false) {
      this.usuarioService.getUsuarioByEstado(0).pipe(map(data => this.usuario = data)).subscribe(data => {
        this.correosdisponibles = [];
        this.idCorreoslista = [];

        for (let i = 0; i < this.usuario.length; i++) {
          this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
          this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
          this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
        }
        if (this.usuario.length !== 0) {
          this.extensionbandera = true;
        }
        if (this.usuario.length === 0) {
          this.bandera2 = true; // true si no hay correos disponibles hacer uno nuevo seleccionando las extensiones disponibles

        }

      });
    }
  }

  getextension(event) {
    // si hay correosdisponibles por todas las subareas
    if (this.extensionbandera === true) {
      this.extensionusuario = event;
    }
    if (this.bandera2 === true) {
      this.extensionusuario = event;
    }
  }

  /** obtiene los permisos para el usuario*/
  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.grupoUsuario = data;
      this.grupUsuario = [];
      for (let i = 0; i < this.grupoUsuario.length; i++) {
        this.grupUsuario.push({'label': this.grupoUsuario[i].nombre, 'value': this.grupoUsuario[i].id});
      }
    });
  }
  /** metodo que se activa cuando se selecciono un correo de la misma area*/
  getcorreoprincial(event) {
    if (event) {
      this.correoprimario = event;
    }
    this.banderacorreousado = false;
    /** se obtiene si es tipo administrativo o ejecutivo */
    if (this.correoprimario.match(/trigarante.com/) || this.correoprimario.match(/nexosmedia.com/)
      || this.correoprimario.match(/grupopg.com.mx/) || this.correoprimario.match(/migoseguros.com/)
      || this.correoprimario.match(/migoseguros.com.mx/) || this.correoprimario.match(/grupoahorra.com/)
      || this.correoprimario.match(/serviciostrigarante.com/)) {
      this.tipodominio = 'administrativoCorreo';
    } else if (this.correoprimario.match(/ahorraseguros.mx/) || this.correoprimario.match(/ahorracredit.com/)
      || this.correoprimario.match(/mejorsegurodeauto.mx/) || this.correoprimario.match(/comparaya.mx/)
      || this.correoprimario.match(/mejorsegurodevida.mx/) || this.correoprimario.match(/gnpsegurosautos.com/)
      || this.correoprimario.match(/lalatinoseguros.mx/) || this.correoprimario.match(/multivaseguros.com/)
      || this.correoprimario.match(/segurosdetaxi.com.mx/) || this.correoprimario.match(/seguroselpotosi.mx/)
      || this.correoprimario.match(/segurosmotos.com.mx/) || this.correoprimario.match(/ahorraseguros.com/)
      || this.correoprimario.match(/qualitassegurosautos.com/) || this.correoprimario.match(/inprove.mx/) ) {
      this.tipodominio = 'ejecutivoCorreo';
    }
    this.auditoriaForm.controls['emailprincipal'].setValue(event);
      this.correofinal = event;
  }
  getcorreoporarea(event) {
    if (event) {

      this.correoprimario = event;
      this.correoService.validarCorreo(this.tipodominio, event).subscribe(response => {
        this.estadoCorreo5 = response;
        if (this.estadoCorreo5 === true) {
          this.estadofinal5 = 'Correo ya en uso';
        } else if (this.estadoCorreo5 === false) {
          this.correofinal = event;
          this.estadofinal5 = 'Correo disponible';
        }
      });
      this.banderacorreousado = true;
    }
  }
  getcorreoprincialSubareas(event) {
    if (event) {
      this.correoprimario = event;
      /** se obtiene si es tipo administrativo o ejecutivo */
      if (this.correoprimario.match(/trigarante.com/) || this.correoprimario.match(/nexosmedia.com/)
        || this.correoprimario.match(/grupopg.com.mx/) || this.correoprimario.match(/migoseguros.com/)
        || this.correoprimario.match(/migoseguros.com.mx/) || this.correoprimario.match(/grupoahorra.com/)
        || this.correoprimario.match(/serviciostrigarante.com/)) {
        this.tipodominio = 'administrativoCorreo';
      } else if (this.correoprimario.match(/ahorraseguros.mx/) || this.correoprimario.match(/ahorracredit.com/)
        || this.correoprimario.match(/mejorsegurodeauto.mx/) || this.correoprimario.match(/comparaya.mx/)
        || this.correoprimario.match(/mejorsegurodevida.mx/) || this.correoprimario.match(/gnpsegurosautos.com/)
        || this.correoprimario.match(/lalatinoseguros.mx/) || this.correoprimario.match(/multivaseguros.com/)
        || this.correoprimario.match(/segurosdetaxi.com.mx/) || this.correoprimario.match(/seguroselpotosi.mx/)
        || this.correoprimario.match(/segurosmotos.com.mx/) || this.correoprimario.match(/ahorraseguros.com/)
        || this.correoprimario.match(/qualitassegurosautos.com/) || this.correoprimario.match(/inprove.mx/)) {
        this.tipodominio = 'ejecutivoCorreo';

      }
      switch (this.subareax) {
        // administrativo
        case 34:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 35:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'asistentededireccion@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 36:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'asistentededireccion@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 37:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorre') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 38:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreoCorreo') {
            this.emailusuario = 'coordinadorcomercial@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 39:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 40:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'analista.actuarial@migoseguros.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 41:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'admin@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'admin@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        // finanzas
        case 42:
          if (this.tipodominio === 'administrativoCorreo') {

            this.emailusuario = 'contabilidad@trigarante.com';
            this.getnuevocorreo(this.emailusuario);

          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'banorte@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);

          }
          break;
        case 43:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'mesadecontrol@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'aplicaciones@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 44:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'rentabilidad@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'backoffice@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 45:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'compras@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'compras@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 46:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'mantenimiento@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'mantenimiento@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
                                                                       // rrhh
        case 47:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'gerenciarrhh@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 48:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'desarrolloorganizacional@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 49:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'rrhh@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'rrhh@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 50:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivo') {
            this.emailusuario = 'capacitacion@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 51:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'nombre@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 52:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'capatalhumano1@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 53:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'reclutamiento1@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        // auditoria
        case 54:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);

          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'emisiondepolizas@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);

          }
          break;
        case 55:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'mesadecontrol1@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'mesadecontrol1@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 56:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'siniestros2@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'inpecciones@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 57:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'calidad@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 58:
          if (this.tipodominio === 'administrativoCorreo') {
            this.emailusuario = 'nombre@trigarante.com';
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.tipodominio === 'ejecutivoCorreo') {
            this.emailusuario = 'inspecciones@ahorraseguros.mx';
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        default:
          break;
      }
    }

  }
  /** da posible nombre de nuevo correo*/
  getdomainss() {
    if (event) {
      if (this.auditoriaForm.value.tiposdominios === 'administrativo') {
        this.tipodominio = 'administrativo';
      }
      if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
        this.tipodominio = 'ejecutivo';
      }
      switch (this.subareax) {
        // administradores
        case 34:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 35:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'asistentededireccion@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'asistentededireccion@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 36:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'asistentededireccion@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'asistentededireccion@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 37:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'juridico@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'juridico@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 38:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'coordinadorcomercial@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 39:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 40:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'analista.actuaria@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'analista.actuaria@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 41:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'admin@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'admin@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
          // finanzas
        case 42:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'contabilidad@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'banco@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 43:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'mesadecontrol@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'aplicaciones@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 44:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'rentabilidad@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'backoffice@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 45:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'compras@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'compras@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 46:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'mantenimiento@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'mantenimiento@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
                                                                        // RRHH
        case 47:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'gerencia@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 48:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'desarrolloorganizacional@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 49:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'rrhh@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 50:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'capacitacion1@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 51:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 52:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'capitalhumano1@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 53:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'reclutamiento1@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;

        // auditoria
        case 54:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {

            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'emisiondepolizas@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 55:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {
            this.emailusuario = 'mesadecontrol1@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'mesadecontrol1@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 56:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {
            this.emailusuario = 'siniestros2@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'inpecciones@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 57:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {
            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio ;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'calidad@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        case 58:
          if (this.auditoriaForm.value.tiposdominios === 'administrativo') {
            this.emailusuario = 'nombre@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          if (this.auditoriaForm.value.tiposdominios === 'ejecutivo') {
            this.emailusuario = 'inspecciones@' + this.auditoriaForm.value.dominio;
            this.getnuevocorreo(this.emailusuario);
          }
          break;
        default:
          break;
      }
    }
  }
  getnuevocorreo(email) {
    this.banderaemailmodificado = true;
    this.correoService.validarCorreo(this.tipodominio, email ).subscribe(response => {
      this.estadoCorreo3 = response;
      this.auditoriaForm.controls['emailmodificado'].setValue(email);
      if (this.estadoCorreo3 === true) {
        this.estadofinal = 'Correo ya en uso';
      } else if (this.estadoCorreo3 === false) {
        this.estadofinal = 'Correo disponible';
        this.correofinal = email;
      }
    });
  }

  getcorreofinal(event) {
    if (event) {

      this.correoService.validarCorreo(this.tipodominio, event).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.estadofinal = 'Correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.correofinal = event;
          this.estadofinal = 'Correo disponible';
        }
      });
    }
  }
 /** si no hay usuarios pone al usuario que tipo de licencia tomar*/
  getTipo() {
   this.tiposdominios = [];
   this.tiposdominios.push({label: 'administrativo', value: 'administrativo'});
   this.tiposdominios.push({label: 'ejecutivo', value: 'ejecutivo'});

  }
/** obtiene los dominios disponibles por tipo de correo y obtiene el numero de licencias de google*/
  getadmin(event) {
      this.correoService.getDominios(event).pipe(map(data => this.domains = data)).subscribe(data => {
        this.domainss = [];
        for (let i = 0; i < this.domains.length; i++) {
          this.domainss.push({'label': this.domains[i].domainName, 'value': this.domains[i].domainName});
        }
      });
      if (event === 'ejecutivo') {
        this.tipoLicencia = 0;
      }
      if (event === 'ahorraasistencia') {
      this.tipoLicencia = 1;
      }
       if (event === 'administrativo') {
      this.tipoLicencia = 2;
  }
    this.licenciasService.get().subscribe(data2 => {
      this.licencias = data2;
      this.correoService.get(event).subscribe(data3 => {
        this.correoAdministrativos = data3;
        if (this.correoAdministrativos.length === this.licencias[this.tipoLicencia].cantidad ||
          this.correoAdministrativos.length >= this.licencias[this.tipoLicencia].cantidad ) {
          // checar licencias
          this.banderalicencias = true;
        }else if (this.correoAdministrativos.length <= this.licencias[this.tipoLicencia].cantidad) {
          // licencias disponibles
          this.banderalicencias = false;
        }
      });
    });




  }
  guardarUsuario() {
// saca el id del usuario del correo primario
    for (let i = 0; i < this.idCorreo.length; i++) {
      if (this.correoprimario === this.idCorreo[i].label) {
        this.idUsuario = this.idCorreo[i].value;
      }
    }
    if (this.bandera === true) {

      // guarda correos de la misma subarea
      if (this.banderacorreousado === false) {
        const objeto = {
        'id': this.idCorreoprincipal, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
          'email': this.correofinal,
      };
        /**hace el correo con diferente alias correo misma subarea*/
        /*this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => {
          this.estadofindalias = response;
        });*/
      }
      //  hace correos de diferentes subareas b
      if (this.banderacorreousado === true) {
        const objeto = {
        'id': this.idCorreoprincipal, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
         'email': this.correofinal, // nombre del correo para remplazar mismo correo que el idprimario
      };
        /**hace el correo con diferente alias*/
        this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => {
          this.estadofindalias = response;
        });
      }


      this.usuarioService.getUsuarioById(this.idUsuario).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
        // this.datosGrupos = this.usuarioput.grupoUsuariosByIdGrupo;
        if (this.tipodominio === 'ejecutivo') {
        this.datosUsuarios = {
          'idCampania': this.subareax, 'idEmpleado': this.empleado.id, 'idExtension': this.datosUsuario.idExtension,
          'idProtocolo': this.datosUsuario.idProtocolo, 'idPerfilMarcacion': this.datosUsuario.idPerfilMarcacion,
        };
        }
        if (this.tipodominio === 'administrativo') {
        this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.datosUsuario.idExtension};
        }
        this.usuarioput1 = {
          'id': this.usuarioput.id,
          'idCorreo': this.idCorreoprincipal,
          'idGrupo': this.auditoriaForm.value.privilegioscontrol,
          'idTipo': 1,
          'idSubarea': this.subareax,
          'usuario': this.correofinal,
          'password': 'mark-43',
          'datosUsuario': JSON.stringify(this.datosUsuarios),
          'estado': 1,
          // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
        };
        this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(data => {
          this.usuario3 = data;
          if (this.usuario3 !== null) {
            this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
              this.extensionput = dataextensiones;
              this.extensionput1 = {
                'idSubarea': this.extensionput.idSubarea,
                'descrip': this.extensionput.descrip,
                'idEstado': 1,
              };
              this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {
              });
            });
            this.dialogService.open(SaveComponent, {});
          }// fin del if
        }); // fin del put
      });
    }



    /**opcion que entra para guardar con otro nombre de correo*/

    if (this.extensionbandera === true) {


      const objeto = {
        'id': this.idCorreoprincipal, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.correofinal,
      };
      this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => {
       this.estadofindalias = response;
     });
      if (this.tipodominio === 'ejecutivoCorreo') {
        this.datosUsuarios = {
          'idCampania': this.subareax, 'idEmpleado': this.empleado.id, 'idExtension': this.extensionusuario,
          'idProtocolo': 1, 'idPerfilMarcacion': 1,
        };
      }
      if (this.tipodominio === 'administrador') {
        this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.extensionusuario};
      }
      this.usuarioService.getUsuarioById(this.idUsuario).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.usuarioput1 = {
          'id': this.usuarioput.id,
          'idCorreo': this.idCorreoprincipal,
          'idGrupo': this.auditoriaForm.value.privilegioscontrol,
          'idTipo': 1,
          'idSubarea': this.subareax,
          'usuario': this.correofinal,
          'password': 'mark-43',
          'datosUsuario': JSON.stringify(this.datosUsuarios),
          'estado': 1,
          // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
        };
        this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(data => {
          this.usuario3 = data;
          if (this.usuario3 !== null) {

            this.extensionesService.get(this.extensionusuario).subscribe(dataextensiones => {
              this.extensionput = dataextensiones;
              this.extensionput1 = {
                'idSubarea': this.extensionput.idSubarea,
                'descrip': this.extensionput.descrip,
                'idEstado': 1,
              };
              this.extensionesService.put(this.extensionusuario, this.extensionput1).subscribe(result => {
              });
            });

            this.dialogService.open(SaveComponent, {});
          }
        });
      });
    } // fin if del banderaextension*/
  }
  guardarNuevoUsuario() {
    if (this.banderalicencias === true) {
      this.showToast2('danger');
    }else if (this.banderalicencias === false) {

      const objeto = {
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.correofinal,
      };

      this.correoService.post(this.tipodominio, objeto).subscribe(response => {
      });
      // una ves guardado el correo en el api, busca su id y lo manda a la tabla usuarios
      this.correoService.get(this.tipodominio).subscribe(data => {
        this.correo = data;
        this.idCorreoslista = [];
        for (let i = 0; i < this.correo.length; i++) {
          this.idCorreoslista.push({'label': this.correo[i].email, 'value': this.correo[i].id});
        }
      // obtiene el id del correo
      for (let i = 0; i < this.idCorreoslista.length; i++) {

        if (this.correofinal === this.idCorreoslista[i].label) {
          this.idCorreoNuevo = this.idCorreoslista[i].value;
        }
      }
      if (this.tipodominio === 'administrativo') {
        this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.auditoriaForm.value.extensioncontrol};
      }
      if (this.tipodominio === 'ejecutivo') {
        this.datosUsuarios = {
          'idCampania': this.subareax,
          'idEmpleado': this.empleado.id,
          'idExtension': this.auditoriaForm.value.extensioncontrol,
          'idProtocolo': 1,
          'idPerfilMarcacion': 1,
        };
      }
      this.usuario2 = {
        'idCorreo': this.idCorreoNuevo,
        'idGrupo': this.auditoriaForm.value.privilegioscontrol,
        'idTipo': 1,
        'idSubarea': this.subareax,
        'usuario': this.correofinal,
        'password': 'mark-43',
        'datosUsuario': JSON.stringify(this.datosUsuarios),
        'estado': 1,
        // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
      };
      // guarda en la tabla usuario y modifica el idUsuario de la tabla empleado
      this.usuarioService.postUsuariosidEmpleados(this.usuario2, this.empleado.id).subscribe(data2 => {
        this.usuario3 = data2;
        this.auditoriaForm.reset();
        // si guardo bien modifica al extension a ocupado
        if (this.usuario3 !== null) {
          // cambia el estado a 1 a la extencion dada
          this.extensionesService.get(this.extensionusuario).subscribe(dataextensiones => {
            this.extensionput = dataextensiones;
            this.extensionput1 = {
              'idSubarea': this.extensionput.idSubarea,
              'descrip': this.extensionput.descrip,
              'idEstado': 1,
            };
            this.extensionesService.put(this.extensionusuario, this.extensionput1).subscribe(result => {
            });
          });

          this.dialogService.open(SaveComponent, {});
        }

      });
      });
    }// fin del if de banderas licencias
  }
  /** boton para hacer correo y crear usuario*/
  guardar() { /** si hay correos disponibles va a funcion guardarusuario y utilizara un correo ya hecho*/
      if (this.correosdisponibles.length !== 0) {
        // saca el id del correo que se va a ocupar
        for (let i = 0; i < this.idCorreoslista.length; i++) {
          if (this.correoprimario === this.idCorreoslista[i].label) {
            this.idCorreoprincipal = this.idCorreoslista[i].value;
          }
        }


        /** si la bandera es false guardara correos modificando el alias*/
        if (this.bandera === false) {
          this.guardarUsuario();
        } else {
          /** de lo contrario si la bandera es true manda a guardar el mismo correo con diferentes nombres*/
          this.guardarUsuario();
        }
      }
    // si no hay ningun correo disponible manda guardar nuevo ejecutivo
    if (this.correosdisponibles.length === 0) {
      {
        this.guardarNuevoUsuario();
      }
    }
  }


  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  showToast2(status) {
    this.toastrService.danger('Atencion', `Checar numero de licencias para ` + this.tipoLicencia);
  }
  ngOnInit() {
    this.getEmpleados();
    this.getcorreosdisponibles(this.subareax);
    this.getExtensiones();
    this.getGrupoUsuarios();
    this.getTipo();

  }

}
