import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {ProductoData} from '../../../../@core/data/interfaces/comerciales/producto-socios';
import {TipoProducto, TipoProductoData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-producto';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-asignar-producto',
  templateUrl: './asignar-producto.component.html',
  styleUrls: ['./asignar-producto.component.scss'],
})
export class AsignarProductoComponent implements OnInit {
  @Input() idSubramo: number;
  @Input() idProducto: number;
  @Input() idTipo: number;
  asignarProductoForm: FormGroup;
  submitted: Boolean;
  tipoProducto: TipoProducto[];
  tipoProductos: any[];
  /**Variable que almacena la prioridad*/
  prioridadVal: any[];

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected productoServe: ProductoData, protected tipoProductoService: TipoProductoData) {
  }

  guardarProducto() {
    this.submitted = true;
    if (this.asignarProductoForm.invalid) {
      return;
    }
    this.productoServe.post(this.asignarProductoForm.value).subscribe((result) => {
      this.productoServe.postSocket(this.asignarProductoForm.value).subscribe(x => {});
      this.asignarProductoForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/producto']);
    });
  }

  prioridad() {
    this.prioridadVal = [];
    this.prioridadVal.push({label: 'ALTA', value: 1});
    this.prioridadVal.push({label: 'MEDIANA', value: 2});
    this.prioridadVal.push({label: 'BAJA', value: 3});
  }

  getTipoProducto() {
    this.tipoProductoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.tipoProducto = result;
      this.tipoProductos = [];
      for (let i = 0; i < this.tipoProducto.length; i++) {
        this.tipoProductos.push({'label': this.tipoProducto[i].tipo, 'value': this.tipoProducto[i].id});
      }
    });
  }

  dismiss() {
    this.ref.close();
  }
  getProductoById() {
    this.productoServe.getProductoSociosById(this.idProducto).subscribe(data => {
      this.asignarProductoForm.controls['idTipoProducto'].setValue(data.idTipoProducto);
      this.asignarProductoForm.controls['prioridad'].setValue(data.prioridad);
      this.asignarProductoForm.controls['nombre'].setValue(data.nombre);
      this.asignarProductoForm.controls['idSubRamo'].setValue(data.idSubRamo);
    });
  }
  updateProducto() {
    this.productoServe.put(this.idProducto, this.asignarProductoForm.value).subscribe(data => {
      this.productoServe.postSocket(this.asignarProductoForm.value).subscribe(x => {});
      this.ref.close();
    });
  }
  ngOnInit() {
    this.prioridad();
    this.getTipoProducto();
    this.asignarProductoForm = this.fb.group({
      'idSubRamo': this.idSubramo,
      'idTipoProducto': new FormControl('', Validators.required),
      'prioridad': new FormControl('', Validators.required),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'activo': 1,
    });
    if (this.idTipo === 2) {
      this.getProductoById();
    }
  }

}
