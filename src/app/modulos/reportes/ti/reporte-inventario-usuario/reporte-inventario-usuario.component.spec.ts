ng
import { ReporteInventarioUsuarioComponent } from './reporte-inventario-usuario.component';

describe('ReporteInventarioUsuarioComponent', () => {
  let component: ReporteInventarioUsuarioComponent;
  let fixture: ComponentFixture<ReporteInventarioUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteInventarioUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteInventarioUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
