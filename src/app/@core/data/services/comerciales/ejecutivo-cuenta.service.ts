import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {EjecutivoCuenta, EjecutivoCuentaData} from '../../interfaces/comerciales/ejecutivo-cuenta';
import {Observable} from 'rxjs';

@Injectable()
export class EjecutivoCuentaService extends EjecutivoCuentaData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<EjecutivoCuenta[]> {
    return this.http.get<EjecutivoCuenta[]>(this.baseURL + 'v1/ejecutivos-cuentas');
  }

  post(ejecutivoCuenta: EjecutivoCuenta): Observable<EjecutivoCuenta> {
    return this.http.post<EjecutivoCuenta>(this.baseURL + 'v1/ejecutivos-cuentas', ejecutivoCuenta);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'contactos/saveContacto/1', json);
  }

  getEjectutivoCuentaById(idEjecutivoCuenta): Observable<EjecutivoCuenta> {
    return this.http.get<EjecutivoCuenta>(this.baseURL + 'v1/ejecutivos-cuentas/' + idEjecutivoCuenta);
  }

  put(idEjecutivoCuenta, ejecutivoCuenta: EjecutivoCuenta): Observable<EjecutivoCuenta> {
    return this.http.put<EjecutivoCuenta>(this.baseURL + 'v1/ejecutivos-cuentas/' + idEjecutivoCuenta + '/'
      + sessionStorage.getItem('Empleado'), ejecutivoCuenta);
  }
}
