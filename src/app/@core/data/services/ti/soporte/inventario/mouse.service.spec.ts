import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Mouse Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 10,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'medio uso',
    estadoInventario: 'A ESTADO',
    idEstadoInventario: 12,
    idObservacionesInventario: 2,
    idMarca: 2,
    fechaRecepcion: '2019-09-24T16:35:15.000+0000',
    fechaSalida: null,
    comentarios: 'EXCELENTES',
    activo: 1,
    usuario: null,
    nombreMarca: 'HP',
    idSede: 1,
    origenRecurso: 'propio',
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'mouse',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/mouse');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/mouse', '/10');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/mouse');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/mouse', '/10/74');
      });
    });
  }
});
