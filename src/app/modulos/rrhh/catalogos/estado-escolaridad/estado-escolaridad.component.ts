import {Component, OnInit, ViewChild} from '@angular/core';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoEscolaridadCreateComponent} from '../modals/estado-escolaridad-create/estado-escolaridad-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../environments/environment';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-estado-escolaridad',
  templateUrl: './estado-escolaridad.component.html',
  styleUrls: ['./estado-escolaridad.component.scss'],
})
export class EstadoEscolaridadComponent implements OnInit {
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  estadoEscolaridad: EstadoEscolaridad[];
  /** Arreglo para la escolaridad que se dará de baja. */
  estadoEsc: EstadoEscolaridad;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * En este caso: Servicio de Estado de Escolaridad.
   * @param {EstadoEscolaridadData} estadoEscolaridadService Servicio del componente estado-escolaridad para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private estadoEscolaridadService: EstadoEscolaridadData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'estadoescolaridad');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelEstadoEscolaridad', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEstadoEscolaridad(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene el estado de escolaridad y lo asigna al arreglo estadoEscolaridad. */
  getEstadoEscolaridad(mensajeAct) {
    this.estadoEscolaridadService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.estadoEscolaridad = [];
      this.estadoEscolaridad = data;
      this.dataSource = new MatTableDataSource(this.estadoEscolaridad); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Estado-Escolaridad`);
  }

  /** Función que obtiene la información de la escolaridad dependiendo de su ID. */
  getEstadoEscolaridadById(idEstadoEscolaridad) {
    this.estadoEscolaridadService.getEstadoEscolaridadById(idEstadoEscolaridad).subscribe(data => {
      this.estadoEsc = data;
    });
  }

  /** Función que despliega un modal del componente "EstadoEscolaridadCreateComponent" para poder "crear el estado de escolaridad". */
  crearEstadoEscolaridad() {
    this.dialogoService.open(EstadoEscolaridadCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEstadoEscolaridad(0);
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información de un estado escolaridad. */
  updateEstadoEscolaridad(idEstadoEscolaridad) {
    this.dialogoService.open(EstadoEscolaridadCreateComponent, {
      context: {
        idTipo: 2,
        idEstadoEscolaridad: idEstadoEscolaridad,
      },
    }).onClose.subscribe(x => {
      this.getEstadoEscolaridad(1);
    });
  }

  bajaEstadoEscolaridad(idEstadoEscolaridad) {
    this.getEstadoEscolaridadById(idEstadoEscolaridad);
    swal({
      title: '¿Deseas dar de baja este estado de escolaridad?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.estadoEsc['activo'] = 0;
        this.estadoEscolaridadService.put(idEstadoEscolaridad, this.estadoEsc).subscribe(result => {
          this.estadoEscolaridadService.postSocket({
            idEstadoEscolaridad: idEstadoEscolaridad, estadoEsc: this.estadoEsc}).subscribe(x => {});
        });
        swal('¡El estado de escolaridad se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getEstadoEscolaridad(0);
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.estadoDeEscolaridad.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.estadoDeEscolaridad.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  ngOnInit() {
    this.connect();
    this.cols = ['estado', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getEstadoEscolaridad(0);
  }

}
