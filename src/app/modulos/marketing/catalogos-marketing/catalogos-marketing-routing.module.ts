import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {CatalogosMarketingComponent} from './catalogos-marketing.component';
import {TipoPaginaComponent} from './tipo-pagina/tipo-pagina.component';
import { ProveedorLeadComponent} from './provedor-lead/provedor-lead.component';
import {AuthGuard} from '../../../@core/data/services/login/auth.guard';

const permisos: any = JSON.parse(window.localStorage.getItem('User'));
const routes: Routes = [{
  path: '',
  component: CatalogosMarketingComponent,
  children: [
    {
      path: 'tipo-pagina',
      component: TipoPaginaComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.tipoPaginas.activo},
    },
    {
      path: 'provedor-lead',
      component: ProveedorLeadComponent,
      canActivate: [AuthGuard],
      data: { permiso: permisos.modulos.provedorLeads.activo},
    },
  ],
}];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogosMarketingRoutingModule {
}
