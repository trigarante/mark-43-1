import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBajaSociosComponent } from './update-baja-socios.component';

describe('UpdateBajaSociosComponent', () => {
  let component: UpdateBajaSociosComponent;
  let fixture: ComponentFixture<UpdateBajaSociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBajaSociosComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBajaSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
