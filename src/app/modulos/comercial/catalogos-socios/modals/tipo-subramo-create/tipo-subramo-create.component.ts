import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {TipoSubramo, TipoSubramoData} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-subramo';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-tipo-subramo-create',
  templateUrl: './tipo-subramo-create.component.html',
  styleUrls: ['./tipo-subramo-create.component.scss'],
})
export class TipoSubramoCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idTipoSubramo: number;
  tipoSubramoCreateForm: FormGroup;
  submitted: Boolean;
  tipoSubramo: TipoSubramo;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoSubramoSrvice: TipoSubramoData) {
  }

  updateTipoSubramo() {
    this.tipoSubramoSrvice.put(this.idTipoSubramo, this.tipoSubramoCreateForm.value).subscribe(result => {
      this.tipoSubramoCreateForm.reset();
      this.ref.close();
    });
  }

  guardarTipoSubramo() {
    this.submitted = true;
    if (this.tipoSubramoCreateForm.invalid) {
      return;
    }
    this.tipoSubramoSrvice.post(this.tipoSubramoCreateForm.value).subscribe((result) => {
      this.tipoSubramoCreateForm.reset();
      this.ref.close();
    });
  }

  getSubramoById() {
    this.tipoSubramoSrvice.getTipoSubramoById(this.idTipoSubramo).subscribe(data => {
      this.tipoSubramo = data;
      this.tipoSubramoCreateForm.controls['tipo'].setValue(this.tipoSubramo.tipo);
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSubramoById();
    }
    this.tipoSubramoCreateForm = this.fb.group({
      'tipo': new FormControl('', Validators.required),
      'activo': 1,
    });
  }
}
