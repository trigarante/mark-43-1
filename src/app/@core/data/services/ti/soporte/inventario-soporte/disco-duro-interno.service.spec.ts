import { TestBed } from '@angular/core/testing';

import { DiscoDuroInternoService } from './disco-duro-interno.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../../../matchers';
import {environment} from '../../../../../../../environments/environment';

fdescribe('DiscoDuroInternoService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 7,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'FALLA ELECTRICA',
    idEstadoInventario: 4,
    idObservacionesInventario: 1,
    folio: '535353',
    idMarca: 3,
    fechaRecepcion: '2019-09-23T22:26:18.000+0000',
    fechaSalida: null,
    activo: 0,
    usuario: null,
    nombreMarca: 'SAMSUMG',
    origenRecurso: 'propio',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'discodInterno',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [DiscoDuroInternoService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/discodInterno');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/discodInterno', '/7');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/discodInterno');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/discodInterno', '/7/74');
      });
    });
  }
});
