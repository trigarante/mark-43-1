import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { ExtensionData, Extension } from '../../../../@core/data/interfaces/ti/extension';
import {map} from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import * as d3Select from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';


@Component({
  selector: 'ngx-reporte-extensiones',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reporte-extensiones.component.html',
  styleUrls: ['./reporte-extensiones.component.scss'],
})
export class ReporteExtensionesComponent implements OnInit {
  private margin = {top: 20, right: 20, bottom: 20, left: 20};
  private width: number;
  private height: number;
  extensiones: Extension[];
  rangeDates: Date[];
  cols: any[];
  dataSource: any;
  contadorExtensiones: any;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  private svg: any;
  private svg2: any;
  private Xscale: any;
  private Yscale: any;
  private xAxis: any;
  private yAxis: any;
  private YAX: any;
  private back: any;
  private label: any;
  private root: any;
  private barr: any;
  private glove: any;
  private datosBackUp: any;
  private arc: any;
  private datosMostrar: any;
  private colors: any[];
  private height0: number;
  private width0: number;

  constructor(
    private extensionesService: ExtensionData,
  ) {
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 510 - this.margin.top - this.margin.bottom;
    this.colors = ['#15aa96', '#c83660'];
    this.width0 = 180 - this.margin.left - this.margin.right;
    this.height0 = 510 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this.drawSVG();
    this.getExtensiones();
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
    this.cols = [
      'area',
      'descrip',
      'estado',
    ];
  }

  getExtensiones() {
    this.extensionesService.getExtension()
    .subscribe(data => {
      this.extensiones = data;
      this.contadorExtensiones = this.extensiones.length;
      this.dataSource = new MatTableDataSource(this.extensiones);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.drawdash(data);
      console.log(data);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawSVG() {
    this.svg = d3Select.select('#graph-dash2').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width0} ${this.height0}`)
      .attr('id', 'svg1');

    const Gcircle = this.svg.append('circle')
      .attr('cx', '45%')
      .attr('cy', '25%')
      .attr('r', '13%')
      .style('fill', this.colors[0])
      .attr('stroke', this.colors[0]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());
    const start1 = this.svg.append('rect').attr('x', '45%').attr('y', '8%').attr('height', '4%').attr('width', 3).attr('fill', this.colors[0]);
    const percent1 = this.svg.append('text').attr('x', '50%').attr('y', '12%').attr('font-size', 30).text('%').attr('fill', this.colors[0]);
    const text1 = this.svg.append('text').attr('x', '7%').attr('y', '11%').attr('font-size', 15).text('LIBRE').attr('fill', this.colors[0]);

    const Gcircle2 = this.svg.append('circle')
      .attr('cx', '45%')
      .attr('cy', '75%')
      .attr('r', '13%')
      .style('fill', this.colors[1])
      .attr('stroke', this.colors[1]).style('stroke-width', 20).style('stroke-opacity', 0.7)
      .attr('cursor', 'pointer')
      .on('click', () => this.ocupado());
    const start2 = this.svg.append('rect').attr('x', '45%').attr('y', '57%').attr('height', '5.5%').attr('width', 3).attr('fill', this.colors[1]);
    const percent2 = this.svg.append('text').attr('x', '50%').attr('y', '61%').attr('font-size', 30).text('%').attr('fill', this.colors[1]);
    const text2 = this.svg.append('text').attr('x', '1%').attr('y', '60%').attr('font-size', 13).text('OCUPADO').attr('fill', this.colors[1]);

    this.arc = d3Shape.arc()
      .innerRadius(58)
      .outerRadius(61)
      .startAngle(0);
  }

  drawSVG2(data, allData, originalData) {
    const ancho = 500;
    this.root = data;
    this.label = [];
    for (let j = 1; j <= this.root.length; j++) {
      this.label.push(j); }

    this.svg2 = d3Select.select('#graph-dash22').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width} ${this.height}`)
      .attr('transform', 'rotate(-90 0 0)')
      .attr('id', 'svg2');

    this.back = this.svg2
      .append('rect')
      .attr('class', 'fondo')
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', '100%')
      .attr('width', '100%')
      .style('opacity', '0')
      .on('click', () => this.returnOrigin(originalData));

    this.Xscale = d3Scale.scaleLinear()
      .domain([d3Array.max(this.root), 0])
      .range([this.height - 50, 0]);
    this.Yscale = d3Scale.scaleBand()
      .domain(this.label)
      .range([20, this.height + 10]);
    this.xAxis = d3Axis.axisLeft().scale(this.Xscale).ticks(25);
    this.yAxis = d3Axis.axisBottom().scale(this.Yscale).ticks(25);
    this.YAX = this.svg2.append('g')
      .attr('class', 'axis')
      .attr('transform', 'rotate(90 20 0)')
      .call(this.yAxis);

    this.barr = this.svg2.selectAll('rect .barr')
      .data(this.root)
      .enter()
      .append('rect')
      .attr('y', (d, i) => (i * (this.height - 5) / this.root.length) )
      .attr('x', 20)
      .attr('height', '1.6%')
      .attr('width', 0) // (d) => this.Xscale(d) )
      .attr('fill', '#ffbb25')
      .attr('rx', '3')
      .attr('cursor', 'pointer')
      .on('mouseover', this.handleMouseOver)
      .on('mousemove',   function (d, i) {
        this.glove
          .html('Area: ' + allData[i][0].area + '<br>' + 'Cantidad: ' + d)
          .style('bottom', ( d3Select.mouse(this)[0] + 10)  + 'px')
          .style('left', (d3Select.mouse(this)[1] ) + 'px');
      } )
      .on('mouseout', this.handleMouseOut)
      .on('click', (d, i) => this.table_barr(d, i, originalData));


    this.barr.transition()
      .duration(1000)
      .delay((d, i) => i * 60)
      .attr('width', (d) => this.Xscale(d) + 7)
      .transition()
      .duration(200)
      .attr('width', (d) => this.Xscale(d) - 6)
      .transition()
      .duration(150)
      .attr('width', (d) => this.Xscale(d) + 4)
      .transition()
      .duration(100)
      .attr('width', (d) => this.Xscale(d));
  }

  drawdash(d) {
    const tiempoAnimacion = 5000;
    this.datosMostrar = this.dataCompose(d);
    const dataConv = this.dataFilter(d);
    this.drawSVG2(dataConv[0], dataConv[1], d);

    const ringAdmin = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', this.colors[0])
      .attr('transform', 'translate(63,117)')
      .attr('d', this.arc);
    const percAdmin = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    var percAdministrativo = this.svg.append('text').attr('x', '32%').attr('y', '28%').attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.libre());

    const ringEjec = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', this.colors[1])
      .attr('transform', 'translate(63,353)')
      .attr('d', this.arc);
    const percEjec = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percEjecutivo = this.svg.append('text').attr('x', '32%').attr('y', '78%').attr('font-size', 35)
      .attr('id', 'txtValu').text(0).attr('fill', 'white')
      .attr('cursor', 'pointer')
      .on('click', () => this.ocupado());

    ringAdmin.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percAdministrativo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percAdministrativo.text(Math.trunc(d3Inter.interpolate(0, percAdmin)(t))) );

    ringEjec.transition()
      .duration(tiempoAnimacion)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));
    percEjecutivo.transition()
      .duration(tiempoAnimacion)
      .attrTween('text', () => (t) => percEjecutivo.text(Math.trunc(d3Inter.interpolate(0, percEjec)(t))) );
  }

  dataFilter(data) {
    const areas = [];
    const area = [];
    const area1 = [];
    const area2 = [];
    const area3 = [];
    const area4 = [];
    const area5 = [];
    const area6 = [];
    const area7 = [];
    const area8 = [];
    const area9 = [];
    const area10 = [];
    const area11 = [];
    const area12 = [];
    const area13 = [];
    const area14 = [];
    const area15 = [];
    const area16 = [];
    const area17 = [];
    const area18 = [];
    const area19 = [];
    const area20 = [];
    const area21 = [];
    const area22 = [];
    const area23 = [];
    const area24 = [];
    const area25 = [];
    const area26 = [];
    const area27 = [];
    const area28 = [];
    const area29 = [];
    const area30 = [];
    const area31 = [];
    const area32 = [];
    const area33 = [];
    const area34 = [];
    const area35 = [];
    const area36 = [];
    const area37 = [];
    const area38 = [];
    const area39 = [];
    const area40 = [];
    const area41 = [];
    const area42 = [];
    const area43 = [];
    const area44 = [];
    const area45 = [];
    const area46 = [];
    const area47 = [];
    const area48 = [];
    const area49 = [];
    const area50 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idArea === 1) {
        area1.push(data[i]);
      } else if (data[i].idArea === 2) {
        area2.push(data[i]);
      } else if (data[i].idArea === 3) {
        area3.push(data[i]);
      } else if (data[i].idArea === 4) {
        area4.push(data[i]);
      } else if (data[i].idArea === 5) {
        area5.push(data[i]);
      } else if (data[i].idArea === 6) {
        area6.push(data[i]);
      } else if (data[i].idArea === 7) {
        area7.push(data[i]);
      } else if (data[i].idArea === 8) {
        area8.push(data[i]);
      } else if (data[i].idArea === 9) {
        area9.push(data[i]);
      } else if (data[i].idArea === 10) {
        area10.push(data[i]);
      } else if (data[i].idArea === 11) {
        area11.push(data[i]);
      } else if (data[i].idArea === 12) {
        area12.push(data[i]);
      } else if (data[i].idArea === 13) {
        area13.push(data[i]);
      } else if (data[i].idArea === 14) {
        area14.push(data[i]);
      } else if (data[i].idArea === 15) {
        area15.push(data[i]);
      } else if (data[i].idArea === 16) {
        area16.push(data[i]);
      } else if (data[i].idArea === 17) {
        area17.push(data[i]);
      } else if (data[i].idArea === 18) {
        area18.push(data[i]);
      } else if (data[i].idArea === 19) {
        area19.push(data[i]);
      } else if (data[i].idArea === 20) {
        area20.push(data[i]);
      }else if (data[i].idArea === 21) {
        area21.push(data[i]);
      }else if (data[i].idArea === 22) {
        area22.push(data[i]);
      }else if (data[i].idArea === 23) {
        area23.push(data[i]);
      }else if (data[i].idArea === 24) {
        area24.push(data[i]);
      }else if (data[i].idArea === 25) {
        area25.push(data[i]);
      }else if (data[i].idArea === 26) {
        area26.push(data[i]);
      }else if (data[i].idArea === 27) {
        area27.push(data[i]);
      }else if (data[i].idArea === 28) {
        area28.push(data[i]);
      }else if (data[i].idArea === 29) {
        area29.push(data[i]);
      }else if (data[i].idArea === 30) {
        area30.push(data[i]);
      }else if (data[i].idArea === 31) {
        area31.push(data[i]);
      }else if (data[i].idArea === 32) {
        area32.push(data[i]);
      }else if (data[i].idArea === 33) {
        area33.push(data[i]);
      }else if (data[i].idArea === 34) {
        area34.push(data[i]);
      }else if (data[i].idArea === 35) {
        area35.push(data[i]);
      }else if (data[i].idArea === 36) {
        area36.push(data[i]);
      }else if (data[i].idArea === 37) {
        area37.push(data[i]);
      }else if (data[i].idArea === 38) {
        area38.push(data[i]);
      }else if (data[i].idArea === 39) {
        area39.push(data[i]);
      }else if (data[i].idArea === 40) {
        area40.push(data[i]);
      }else if (data[i].idArea === 41) {
        area41.push(data[i]);
      }else if (data[i].idArea === 42) {
        area42.push(data[i]);
      }else if (data[i].idArea === 43) {
        area43.push(data[i]);
      }else if (data[i].idArea === 44) {
        area44.push(data[i]);
      }else if (data[i].idArea === 45) {
        area45.push(data[i]);
      }else if (data[i].idArea === 46) {
        area46.push(data[i]);
      }else if (data[i].idArea === 47) {
        area47.push(data[i]);
      }else if (data[i].idArea === 48) {
        area48.push(data[i]);
      }else if (data[i].idArea === 49) {
        area49.push(data[i]);
      }else if (data[i].idArea === 50) {
        area50.push(data[i]);
      }
    }
    areas.push(area1.length);
    areas.push(area2.length);
    areas.push(area3.length);
    areas.push(area4.length);
    areas.push(area5.length);
    areas.push(area6.length);
    areas.push(area7.length);
    areas.push(area8.length);
    areas.push(area9.length);
    areas.push(area10.length);
    areas.push(area11.length);
    areas.push(area12.length);
    areas.push(area13.length);
    areas.push(area14.length);
    areas.push(area15.length);
    areas.push(area16.length);
    areas.push(area17.length);
    areas.push(area18.length);
    areas.push(area19.length);
    areas.push(area20.length);
    areas.push(area21.length);
    areas.push(area22.length);
    areas.push(area23.length);
    areas.push(area24.length);
    areas.push(area25.length);
    areas.push(area26.length);
    areas.push(area27.length);
    areas.push(area28.length);
    areas.push(area29.length);
    areas.push(area30.length);
    areas.push(area31.length);
    areas.push(area32.length);
    areas.push(area33.length);
    areas.push(area34.length);
    areas.push(area35.length);
    areas.push(area36.length);
    areas.push(area37.length);
    areas.push(area38.length);
    areas.push(area39.length);
    areas.push(area40.length);
    areas.push(area41.length);
    areas.push(area42.length);
    areas.push(area43.length);
    areas.push(area44.length);
    areas.push(area45.length);
    areas.push(area46.length);
    areas.push(area47.length);
    areas.push(area48.length);
    areas.push(area49.length);
    areas.push(area50.length);
    area.push(area1);
    area.push(area2);
    area.push(area3);
    area.push(area4);
    area.push(area5);
    area.push(area6);
    area.push(area7);
    area.push(area8);
    area.push(area9);
    area.push(area10);
    area.push(area11);
    area.push(area12);
    area.push(area13);
    area.push(area14);
    area.push(area15);
    area.push(area16);
    area.push(area17);
    area.push(area18);
    area.push(area19);
    area.push(area20);
    area.push(area21);
    area.push(area22);
    area.push(area23);
    area.push(area24);
    area.push(area25);
    area.push(area26);
    area.push(area27);
    area.push(area28);
    area.push(area29);
    area.push(area30);
    area.push(area31);
    area.push(area32);
    area.push(area33);
    area.push(area34);
    area.push(area35);
    area.push(area36);
    area.push(area37);
    area.push(area38);
    area.push(area39);
    area.push(area40);
    area.push(area41);
    area.push(area42);
    area.push(area43);
    area.push(area44);
    area.push(area45);
    area.push(area46);
    area.push(area47);
    area.push(area48);
    area.push(area49);
    area.push(area50);
    return [areas, area];
  } // ************************

  returnOrigin(d) {
    this.dataSource = new MatTableDataSource(d);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorExtensiones = d.length;
  }

  table_barr (x, i, data) {
    this.datosBackUp = data;
    const grupoUsuarioFilter = [];
    for ( let k = 0; k < data.length; k++) {
      if ( data[k].idArea === i + 1 ) {
        grupoUsuarioFilter.push(data[k]);
      }
    }
    this.dataSource = new MatTableDataSource(grupoUsuarioFilter);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.contadorExtensiones = grupoUsuarioFilter.length;
  } //***************************************

  dataCompose(d) {
    const tamaño = d.length;
    const libre = [];
    const ocupado = [];


    for (let i = 0; i < tamaño; i++) {
      if (d[i].idEstado === 0) {
        libre.push(d[i]);
      } else if (d[i].idEstado === 1) {
        ocupado.push(d[i]);
      }
    }
    return [tamaño, libre, ocupado];
  }

  libre() {
    const data = this.datosMostrar[1];
    this.svg2.remove();
    this.contadorExtensiones = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola = this.dataFilter(data);
    this.drawSVG2(hola[0], hola[1], data);
    this.barr.attr('fill', this.colors[0]);
  }
  ocupado() {
    const data = this.datosMostrar[2];
    this.svg2.remove();
    this.contadorExtensiones = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    const hola1 = this.dataFilter(data);
    this.drawSVG2(hola1[0], hola1[1], data);
    this.barr.attr('fill', this.colors[1]);
  }

  handleMouseOver(d, i) {
      this.glove = d3Select.select('#graph-dash22')
      .append('div')
      .attr('class', 'tooltip')
      .attr('id', 'toolTip')
      .style('opacity', 0.8);
  }

  handleMouseOut() {
    document.getElementById('toolTip').remove();
  }

}
