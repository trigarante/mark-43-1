import {Component, Input, OnInit} from '@angular/core';
import {CpuData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {MonitorDate} from '../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {TecladoData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {TabletData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {NbDialogRef} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {InventarioData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/inventario';


@Component({
  selector: 'ngx-asignar-equipo-create',
  templateUrl: './asignar-equipo-create.component.html',
  styleUrls: ['./asignar-equipo-create.component.scss'],
})
export class AsignarEquipoCreateComponent implements OnInit {

  @Input() idTipo: number;
  @Input() idEmpleado: number;
  cpu: any;
  monitor: any;
  mouse: any;
  teclado: any;
  diadema: any;
  tablet: any;
  asignarEquipoForm: FormGroup;

  constructor(protected cpuService: CpuData, protected monitorService: MonitorDate, protected mouseService: MouseData,
              protected tecladoService: TecladoData, protected diademaService: DiademaData, protected tabletService: TabletData,
              protected ref: NbDialogRef<AsignarEquipoCreateComponent>, private fb: FormBuilder,
              protected inventarioService: InventarioData) {
  }

  dismiss() {
    this.ref.close();
  }

   equiposDisponibles(): any[] {
      return [
        { label: 'sdfsdf', value: '1'},
        { label: 'sdfsfghfghgfdf', value: '2' },
      ];
   }


  ngOnInit() {
    this.asignarEquipoForm = this.fb.group({
      'idEmpleado': new FormControl(this.idEmpleado),
      'idEquipo': new FormControl(''),
      'idTabla': new FormControl(''),
      'activo': new FormControl(1),
    });
  }

}
