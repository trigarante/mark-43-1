import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarCamposComponent } from './agregar-campos.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Router, RouterModule} from '@angular/router';
import {PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {HttpClient} from '@angular/common/http';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {PuestoData} from '../../../../@core/data/interfaces/catalogos/puesto';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData} from '../../../../@core/data/interfaces/catalogos/area';
import {SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';

describe('AgregarCamposComponent', () => {
  let component: AgregarCamposComponent;
  let fixture: ComponentFixture<AgregarCamposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarCamposComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
      ],
      providers: [
        PaginasData,
        SubareaData,
        PuestoData,
        SedeData,
        GrupoData,
        EmpresaData,
        AreaData,
        SubramoData,
        HttpClient,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarCamposComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
