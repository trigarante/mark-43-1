import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {Cpu, CpuData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {Monitor, MonitorDate} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Laptop, LaptopData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {Diadema, DiademaData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {Tablet, TabletData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Teclado, TecladoData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Mouse, MouseData} from '../../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import swal from 'sweetalert';

@Component({
  selector: 'ngx-desactivar-equipo',
  templateUrl: './desactivar-equipo.component.html',
  styleUrls: ['./desactivar-equipo.component.scss'],
})
export class DesactivarEquipoComponent implements OnInit {
  @Input() idEquipo: number;
  @Input() tipoEquipos: string;
  /** Variable que permite la inicialización del formulario para crear un permiso. */
  desactivarEquipoForm: FormGroup;
  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  cpu: Cpu;
  monitor: Monitor;
  laptop: Laptop;
  diadema: Diadema;
  tablet: Tablet;
  teclado: Teclado;
  mouse: Mouse;

  constructor(private fb: FormBuilder, private estadoInventarioServive: EstadoInventarioData,
              private cpuService: CpuData,
              private monitorService: MonitorDate, private laptopService: LaptopData,
              private diademaService: DiademaData, private tabletService: TabletData,
              private tecladoService: TecladoData,
              private mouseService: MouseData) {
    /** se inicializa el formulario*/
    this.desactivarEquipoForm = this.fb.group({
      'estado': new FormControl('', Validators.required),
    });
  }

  getEstado() {
    this.estadoInventarioServive.get().pipe(map(data => this.estadoInventariodata = data)).subscribe(data => {
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({'label': this.estadoInventariodata[i].estado, 'value': this.estadoInventariodata[i].id});
      }
    });
  }

  /**obtiene la informacion de cpu*/
  getCpuById(id) {
    this.cpuService.getCpuById(id).subscribe(data => {
      this.cpu = data;
    });
  }
  getMonitorById(id) {
    this.monitorService.getMonitorById(id).subscribe(data => {
      this.monitor = data;
    });
  }
  getLaptopById(id) {
    this.laptopService.getLaptopById(id).subscribe(data => {
      this.laptop = data;
    });
  }
  getTabletById(id) {
    this.tabletService.getTabletById(id).subscribe(data => {
      this.tablet = data;
    });
  }
  getTecladoById(id) {
    this.tecladoService.getTecladoById(id).subscribe(data => {
      this.teclado = data;
    });
  }
  getMouseById(id) {
    this.mouseService.getMouseById(id).subscribe(data => {
      this.mouse = data;
    });
  }
  getDiademaById(id) {
    this.diademaService.getDiademaById(id).subscribe(data => {
      this.diadema = data;
    });
  }
  EliminarEquipo() {

    switch (this.tipoEquipos) {
      case 'CPU':
        this.getCpuById(this.idEquipo);
        break;
      case 'MONITOR':
        this.getMonitorById(this.idEquipo);
        break;
      case 'LAPTOP':
        this.getLaptopById(this.idEquipo);
        break;
      case 'TABLET':
        this.getTabletById(this.idEquipo);
        break;
      case 'TECLADO':
        this.getTecladoById(this.idEquipo);
        break;
      case 'MOUSE':
        this.getMouseById(this.idEquipo);
        break;
      case 'DIADEMA':
        this.getDiademaById(this.idEquipo);
        break;
      default:
        break;
    }
    swal({
      title: '¿Deseas desactivar este equipo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Asignar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        switch (this.tipoEquipos) {
          case 'CPU':
            this.cpu['idEstadoInventario'] = this.desactivarEquipoForm.value.estado;
            this.cpu['activo'] = 0;
            this.cpuService.put(this.idEquipo, this.cpu).subscribe( result => {
            });
            break;
          case 'MONITOR':
            this.monitor['activo'] = 0;
            this.monitorService.put(this.idEquipo, this.monitor).subscribe(result => {
            });

            break;
          case 'LAPTOP':
            this.laptop['activo'] = 0;
            this.laptopService.put(this.idEquipo, this.laptop).subscribe(result => {
            });
            break;
          case 'TABLET':
            this.tablet['activo'] = 0;
            this.tabletService.put(this.idEquipo, this.tablet).subscribe(result => {
            });
            break;
          case 'TECLADO':
            this.teclado['activo'] = 0;
            this.tecladoService.put(this.idEquipo, this.teclado).subscribe(result => {
            });
            break;
          case 'MOUSE':
            this.mouse['activo'] = 0;
            this.mouseService.put(this.idEquipo, this.mouse).subscribe(result => {
            });
            break;
          case 'DIADEMA':
            this.diadema['activo'] = 0;
            this.diademaService.put(this.idEquipo, this.diadema).subscribe(result => {
            });
            break;
          default:
            break;
        }
        swal('¡El equipo se ha desactivado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          window.location.reload();
        }));
      }
    });
  }
  ngOnInit() {
    this.getEstado();
  }

}
