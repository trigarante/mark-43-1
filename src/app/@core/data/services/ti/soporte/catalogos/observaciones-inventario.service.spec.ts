import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';
import {ObservacionesInventarioService} from './observaciones-inventario.service';
fdescribe('Ti catalogos observaciones inventario service', () => {
  let injector: TestBed;
  let service: ObservacionesInventarioService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 2,
    observaciones: 'medio uso',
    activo: 0,
  },
    {
      id: 'observacionesinventario',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ObservacionesInventarioService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ObservacionesInventarioService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/observaciones-inventario');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/observaciones-inventario', '/2');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/observaciones-inventario');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/observaciones-inventario', '/2/74');
      });
    });
  }
})
