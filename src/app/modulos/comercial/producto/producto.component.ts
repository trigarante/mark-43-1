import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductoData, ProductoSocios} from '../../../@core/data/interfaces/comerciales/producto-socios';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarCompetenciaComponent} from '../modals/asignar-competencia/asignar-competencia.component';
import {AsignarCoberturaComponent} from '../modals/asignar-cobertura/asignar-cobertura.component';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosProductoComponent} from '../modals/datos-producto/datos-producto.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss'],
})
export class ProductoComponent implements OnInit {
  cols: any[];
  producto: ProductoSocios[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private productoSociosService: ProductoData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.connect();
    this.getProductosSocios(0);
    this.cols = [
      'detalle',
      'idCategoria',
      'prioridad',
      'idTipoProducto',
      'acciones',
    ];
    this.getPermisos();
    this.estaActivo();
  }

  getProductosSocios(mensajeAct) {
    this.productoSociosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1 );
    })).subscribe(data => {
      this.producto = [];
      this.producto = data;
      this.dataSource = new MatTableDataSource(this.producto); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Producto`);
  }

  verDatos(idProducto, idSubramo) {
    this.dialogService.open(DatosProductoComponent, {
      context: {
        idProducto: idProducto,
        idSubramo: idSubramo,
      },
    }).onClose.subscribe( x => this.getProductosSocios(1));
  }

  asignarCompetencia(idProducto) {
    this.dialogService.open(AsignarCompetenciaComponent, {
      context: {
        idProducto: idProducto,
        idTipo: 1,
      },
    });
  }

  asignarCobertura(idProducto) {
    this.dialogService.open(AsignarCoberturaComponent, {
      context: {
        idProducto: idProducto,
      },
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'producto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelProducto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getProductosSocios(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.verProducto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.verProducto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
