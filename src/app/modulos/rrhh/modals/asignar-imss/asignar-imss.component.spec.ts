import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarImssComponent } from './asignar-imss.component';
import {
  MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {
  NbAlertModule,
  NbCardModule,
  NbCheckboxModule,
  NbDateService,
  NbDialogRef,
  NbRadioModule,
  NbToastrService,
} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {RazonSociosData} from '../../../../@core/data/interfaces/catalogos/razon-social';

describe('AsignarImssComponent', () => {
  let component: AsignarImssComponent;
  let fixture: ComponentFixture<AsignarImssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarImssComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
        MatChipsModule,
        NbCheckboxModule,
        MatCheckboxModule,
      ],
      providers: [
        EmpleadosData,
        RazonSociosData,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarImssComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
