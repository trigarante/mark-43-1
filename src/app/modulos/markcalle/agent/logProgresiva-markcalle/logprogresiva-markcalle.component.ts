import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'ngx-logprogresiva-markcalle',
  templateUrl: './logprogresiva-markcalle.component.html',
  styleUrls: ['./logprogresiva-markcalle.component.scss'],
})
export class LogprogresivaMarkcalleComponent implements OnInit {
  cols: any[];
  constructor() { }
  ngOnInit() {
    this.cols = [
      {field: 'nombre', header: 'Nombre'},
      {field: 'calificacionPsicometrico', header: 'Calificación psicometrico'},
      {field: 'calificacionExamen', header: 'Calificación examen'},
      {field: 'comentarios', header: 'Comentarios'},
      {field: 'fechaIngreso', header: 'Fecha ingreso'},
      {field: 'acciones', header: 'Asignar capacitacion'},
    ];
    
  }
  
}
