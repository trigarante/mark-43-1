import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Escolaridad, EscolaridadData} from '../../interfaces/catalogos/escolaridad';

@Injectable()
export class EscolaridadesService extends EscolaridadData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Escolaridad[]> {
    return this.http.get<Escolaridad[]>(this.baseURL + 'v1/escolaridades');
  }

  post(escolaridades: Escolaridad): Observable<Escolaridad> {
    return this.http.post<Escolaridad>(this.baseURL + 'v1/escolaridades', escolaridades);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'escolaridad/saveEscolaridad', json);
  }

  getEscolaridadById(idEscolaridad): Observable<Escolaridad> {
    return this.http.get<Escolaridad>(this.baseURL + 'v1/escolaridades/' + idEscolaridad);
  }

  put(idEscolaridad, escolaridad: Escolaridad) {
    return this.http.put<Escolaridad>(this.baseURL + 'v1/escolaridades/' + idEscolaridad + '/'
      + sessionStorage.getItem('Empleado'), escolaridad);
  }
}
