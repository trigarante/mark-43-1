import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';
import {CampanaCategoriaService} from './campana-categoria.service';

fdescribe('Campaña Categoria service', () => {
  let injector: TestBed;
  let service: CampanaCategoriaService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idCampana: 4,
    activo: 0,
    comentarios: 'holsa',
    nombre: 'SEGUROS',
    idSubRamo: 1,
    prioridad: 1,
    descripcion: 'TEST FINAL',
  },
    {
      id: 'campanacategorias',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CampanaCategoriaService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(CampanaCategoriaService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/campana-categorias');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/campana-categorias', '/3');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/campana-categorias');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/campana-categorias', '/1/74');
      });
    });
  }
})
