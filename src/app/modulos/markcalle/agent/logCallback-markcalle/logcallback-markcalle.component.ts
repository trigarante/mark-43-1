import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'ngx-logcallback-markcalle',
  templateUrl: './logcallback-markcalle.component.html',
  styleUrls: ['./logcallback-markcalle.component.scss'],
})
export class LogcallbackMarkcalleComponent implements OnInit {
  cols: any[];
  constructor() { }
  ngOnInit() {
    this.cols = [
      {field: 'nombre', header: 'Nombre'},
      {field: 'calificacionPsicometrico', header: 'Calificación psicometrico'},
      {field: 'calificacionExamen', header: 'Calificación examen'},
      {field: 'comentarios', header: 'Comentarios'},
      {field: 'fechaIngreso', header: 'Fecha ingreso'},
      {field: 'acciones', header: 'Asignar capacitacion'},
    ];
    
  }
  
}
