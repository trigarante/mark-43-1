import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AsignarCandidatoComponent} from './asignar-candidato.component';
import {
  MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {
  NbAlertModule,
  NbCardModule,
  NbCheckboxModule,
  NbDateService,
  NbDialogRef,
  NbRadioModule,
  NbToastrService,
} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {CandidatoData} from '../../../../@core/data/interfaces/rrhh/candidatos';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';

describe('AsignarCandidatoComponent', () => {
  let component: AsignarCandidatoComponent;
  let fixture: ComponentFixture<AsignarCandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCandidatoComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
        MatNativeDateModule,
        MatChipsModule,
        NbCheckboxModule,
        MatCheckboxModule,
      ],
      providers: [
        CandidatoData,
        PrecandidatosData,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide: Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(AsignarCandidatoComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    /*expect(component).toBeTruthy();*/
  });
});
