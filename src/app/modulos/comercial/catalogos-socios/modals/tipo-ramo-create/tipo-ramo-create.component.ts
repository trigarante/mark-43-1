import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../../socios/socios.component';
import {Router} from '@angular/router';
import {TipoRamo, TipoRamoData} from '../../../../../@core/data/interfaces/comerciales/catalogo/tipo-ramo';

@Component({
  selector: 'ngx-tipo-ramo-create',
  templateUrl: './tipo-ramo-create.component.html',
  styleUrls: ['./tipo-ramo-create.component.scss'],
})
export class TipoRamoCreateComponent implements OnInit {
  @Input() idTipoRamo: number;
  @Input() idTipo: number;
  tipoRamoCreateForm: FormGroup;
  submitted: Boolean;
  tipoRamo: TipoRamo;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected tipoRamoService: TipoRamoData) {
  }

  guardarTipoRamo() {
    this.submitted = true;
    if (this.tipoRamoCreateForm.invalid) {
      return;
    }
    this.tipoRamoService.post(this.tipoRamoCreateForm.value).subscribe((result) => {
      this.tipoRamoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoRamoCreateForm.reset();
      this.ref.close();
    });
  }

  updateTipoRamo() {
    this.tipoRamoService.put(this.idTipoRamo, this.tipoRamoCreateForm.value).subscribe(result => {
      this.tipoRamoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoRamoCreateForm.reset();
      this.ref.close();
    });
  }

  dismiss() {
    this.ref.close();
  }

  getTipoRamoById() {
    this.tipoRamoService.getTipoRamoById(this.idTipoRamo).subscribe(data => {
      this.tipoRamo = data;
      this.tipoRamoCreateForm.controls['tipo'].setValue(this.tipoRamo.tipo);
    });
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTipoRamoById();
    }
    this.tipoRamoCreateForm = this.fb.group({
      'tipo': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'activo': 1,
    });
  }
}
