import {Component, Input, OnInit} from '@angular/core';
import {PreempleadosData} from '../../../../@core/data/interfaces/rrhh/preempleados';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {PreEmpleadosComponent} from '../../pre-empleados/pre-empleados.component';
import * as moment from 'moment';

@Component({
  selector: 'ngx-datos-preemplado',
  templateUrl: './datos-preemplado.component.html',
  styleUrls: ['./datos-preemplado.component.scss'],
})
export class DatosPreempladoComponent implements OnInit {
  @Input() idPreempleado: number;
  nombre: string;
  calificacionExamen: number;
  calificacionPsicometrico: string;
  comentarios: string;
  fechaIngreso;
  fechaNacimiento;
  email: string;
  genero: string;
  estadoCivil: string;
  escolaridad: string;
  estadoEscolar: string;
  cp: string;
  colonia: string;
  calle: string;
  numeroExterior: number;
  numeroInterior: number;
  telefonoFijo: string;
  telefonoMovil: string;
  fechaCreacion;
  fechaBaja;
  tiempoTraslado: string;
  medio: string;
  estacion: string;
  lineas: string;
  estadoRH: string;
  curp: string;
  detalleBaja: string;
  fechaRegistroBaja;
  nombreEtapa: string;
  puesto: string;
  puestoTipo: string;
  tipoUsuario: string;
  sede: string;
  empresa: string;
  subarea: string;
  grupo: string;
  recontratable: number;


  constructor(protected preempleadoService: PreempleadosData, protected ref: NbDialogRef<PreEmpleadosComponent>) { }
  getPreempleadoById() {
    this.preempleadoService.getPreEmpleadoById(this.idPreempleado).subscribe(data => {
      this.nombre = data.preEmpleado;
      this.fechaNacimiento = data.fechaNacimiento;
      this.curp = data.curp;
      if(data.genero === 'F'){this.genero = 'Mujer'}else{this.genero = 'Hombre'};
      this.estadoCivil = data.estadoCivil;
      this.escolaridad = data.escolaridad;
      this.estadoEscolar = data.estadoEscolar;
      this.email = data.email;
      this.telefonoMovil = data.telefonoMovil;
      this.telefonoFijo = data.telefonoFijo;
      this.calle = data.calle;
      this.numeroInterior = data.numeroInterior;
      this.numeroExterior = data.numeroExterior;
      this.cp = data.cp;
      this.colonia = data.colonia;
      this.tiempoTraslado = data.tiempoTraslado + ' minutos';
      this.medio = data.medio;
      this.lineas = data.lineas;
      this.estacion = data.estacion;
      this.fechaIngreso = moment(data.fechaIngreso).format('YYYY-MM-DD');
      if ( data.calificacionPsicometrico === 1) { this.calificacionPsicometrico = 'Aprobado';
      } else { this.calificacionPsicometrico = 'No Aprobado'; }
      this.calificacionExamen = data.calificacionExamen;
      this.comentarios = data.comentarios;
      this.grupo = data.grupo;
      this.empresa = data.empresa;
      this.sede = data.sede;
      this.subarea = data.subarea;
      this.puesto = data.puesto;
      this.puestoTipo = data.puestoTipo;
      this.tipoUsuario = data.tipoUsuario;
      this.estadoRH = data.estadoRH;
      this.nombreEtapa = data.nombreEtapa;
      if(data.fechaBaja){this.fechaBaja = moment(data.fechaBaja).format('YYYY-MM-DD')};
      if(data.detalleBaja){this.detalleBaja = moment(data.detalleBaja).format('YYYY-MM-DD')};
      this.recontratable = data.recontratable;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getPreempleadoById();
  }

}
