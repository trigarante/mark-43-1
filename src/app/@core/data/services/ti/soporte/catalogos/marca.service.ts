import {Injectable} from '@angular/core';
import {Marca, MarcaSoporteData} from '../../../../interfaces/ti/catalogos/marca';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class MarcaService extends MarcaSoporteData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Marca[]> {
    return this.http.get<Marca[]>(this.baseURL + 'v1/marca');
  }

  post(marca: Marca): Observable<Marca> {
    return this.http.post<Marca>(this.baseURL + 'v1/marca', marca);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'soportemarcas/saveSoporteMarcas', json);
  }

  getMarcasSoporte(idMarca): Observable<Marca> {
    return this.http.get<Marca>(this.baseURL + 'v1/marca/' + idMarca);
  }

  put(idMarca, marca: Marca): Observable<Marca> {
    return this.http.put<Marca>(this.baseURL + 'v1/marca/' + idMarca + '/'
      + sessionStorage.getItem('Empleado') , marca);
  }
}
