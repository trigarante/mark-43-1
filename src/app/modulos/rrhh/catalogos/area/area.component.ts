import {Component, OnInit, ViewChild} from '@angular/core';
import {Area, AreaData} from '../../../../@core/data/interfaces/catalogos/area';
import {NbDialogService} from '@nebular/theme';
import {AreaCreateComponent} from '../modals/area-create/area-create.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss'],
})
export class AreaComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  area: Area[];
  areas: Area;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * En este caso: Servicio de Area.
   * @param {AreaData} areasService Servicio del componente area para poder invocar los métodos. */
  constructor(private areasService: AreaData, private dialogoService: NbDialogService) {
  }
  connect() {
    const socket = new SockJS('http://localhost:8085/areas');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelAreas', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getArea();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getArea() {
    this.areasService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.area = [];
      this.area = data;
      this.dataSource = new MatTableDataSource(this.area); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearAreas() {
    this.dialogoService.open(AreaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getArea();
    });
  }

  updateArea(idArea) {
    this.dialogoService.open(AreaCreateComponent, {
      context: {
        idTipo: 2,
        idArea: idArea,
      },
    }).onClose.subscribe(x => {
      this.getArea();
    });
  }

  bajaArea(idArea) {
    {
      this.getAreasById(idArea);
      swal({
        title: '¿Deseas dar de baja esta Area?',
        text: 'Una vez dado de baja, no podrás verlo de nuevo.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          confirm: {
            text: 'Dar de baja',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.areas['activo'] = 0;
          this.areasService.put(idArea, this.areas).subscribe( result => {
            this.stompClient.send('/tasks/saveArea', {}, JSON.stringify({idArea: idArea, areas: this.areas}));
          });
          swal('¡La area se ha dado de baja exitosamente!', {
            icon: 'success',
          }).then((resultado => {
            this.getArea();
          }));
        }
      });
    }
  }

  getAreasById(idArea) {
    this.areasService.getAreaById(idArea).subscribe(data => {
      this.areas = data;
    });
  }

  /** Función que inicializa el componente, mandando a llamar a getArea para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.getArea();
    this.cols = ['sede', 'nombre', 'descripcion', 'acciones'];
    this.getPermisos();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.areas.escritura;
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

