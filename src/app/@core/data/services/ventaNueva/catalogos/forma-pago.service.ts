import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormaPago, FormaPagoData} from '../../../interfaces/ventaNueva/catalogos/forma-pago';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable()
export class FormaPagoService extends FormaPagoData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<FormaPago[]> {
    return this.http.get<FormaPago[]>(this.baseURL + 'v1/forma-pago');
  }
  post(formaPago): Observable<FormaPago> {
    return this.http.post<FormaPago>(this.baseURL + 'v1/forma-pago', formaPago );
  }
  getFormaPagoById(idFormaPago): Observable<FormaPago> {
    return this.http.get<FormaPago>(this.baseURL + 'v1/forma-pago' + idFormaPago);
  }
  put(idformaPago, formaPago): Observable<FormaPago> {
    return this.http.put<FormaPago>(this.baseURL + 'v1/forma-pago' + idformaPago, formaPago);
  }
}
