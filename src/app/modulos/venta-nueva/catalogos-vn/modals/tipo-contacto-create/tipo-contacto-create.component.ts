import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import { TipoContactoVnComponent } from '../../tipo-contacto-vn/tipo-contacto-vn.component';
import {Router} from '@angular/router';
import {TipoContactoVn, TipoContactoVnData} from '../../../../../@core/data/interfaces/catalogos/tipo-contacto-vn';


@Component({
  selector: 'ngx-tipo-contacto-create',
  templateUrl: './tipo-contacto-create.component.html',
  styleUrls: ['./tipo-contacto-create.component.scss'],
})
export class TipoContactoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idContacto obtenido. */
  @Input() idContacto: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  contactoCreateform: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  contacto: TipoContactoVn;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;


  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BancoComponent>} ref Referencia al diálogo (mensaje).
   * @param {TipoContactoVnData} contactoService Servicio del componente banco para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<TipoContactoVnComponent>, private contactoService: TipoContactoVnData,
              private router: Router) { }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoContacto() {
    if (this.contactoCreateform.invalid) {
      return;
    }
    this.submitted = true;
    this.contactoService.post(this.contactoCreateform.value).subscribe((result) => {
      this.contactoService.postSocket(this.contactoCreateform.value).subscribe(x => {});
      this.contactoCreateform.reset();
      this.router.navigate(['/modulos/venta-nueva/catalogos/tipocontacto']);
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un Tipo contacto. */
  updateTipoContacto() {
    this.contactoService.put(this.idContacto, this.contactoCreateform.value).subscribe( result => {
      this.contactoService.postSocket(this.contactoCreateform.value).subscribe(x => {});
      this.contactoCreateform.reset();
      this.ref.close();
      // window.location.reload();
    });
  }


  /** Función que obtiene la información de un contacto dependiendo de su ID. */
  getContactoById() {
    this.contactoService.getContactoById(this.idContacto).subscribe(data => {
      this.contacto = data;
      this.contactoCreateform.controls['descripcion'].setValue(this.contacto.descripcion);
      this.contactoCreateform.controls['activo'].setValue(this.contacto.activo);
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getContactoById();
    }
    this.contactoCreateform = this.fb.group( {
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
