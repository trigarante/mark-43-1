/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'https://www.mark-43.net/mark43-service/',
  GLOBAL_SOCKET: 'https://www.mark-43.net:445/',
  // apiUrl: 'http://localhost:8090/mark43-service/',
  CATALOGO_AUTOS: 'https://ahorraseguros.mx/ws-autos/servicios',
  // GLOBAL_SERVICIOS: 'https://www.mark-43.net/mark43-service/',
  GLOBAL_SERVICIOS:  'http://localhost:8090/mark43-service/',
     // 'http://192.168.32.237:8090/mark43-service/',
  // juan: 'http://localhost:8090/mark43-service/',
  // GLOBAL_SERVICIOS_AWS: 'https://trigarante2020BL-388916397.us-east-2.elb.amazonaws.com/mark43-service/v1/',
  // GLOBAL_SERVICIOS_LOCALHOST_LOGIN_GOGLE: 'http://localhost:8090/mark43-service/',
  // GLOBAL_SERVICIOS_AWS_LOGIN_GOGLE: 'https://trigarante2020BL-388916397.us-east-2.elb.amazonaws.com/mark43-service/',
  // GLOBAL_SERVICIOS: 'http://localhost:8090/mark43-service/',
};

