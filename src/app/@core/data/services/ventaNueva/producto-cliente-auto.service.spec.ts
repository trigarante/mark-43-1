import { TestBed } from '@angular/core/testing';

import { ProductoClienteAutoService } from './producto-cliente-auto.service';

describe('ProductoClienteAutoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductoClienteAutoService = TestBed.get(ProductoClienteAutoService);
    expect(service).toBeTruthy();
  });
});
