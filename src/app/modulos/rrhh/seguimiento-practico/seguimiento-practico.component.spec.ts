import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {SeguimientoPracticoComponent} from './seguimiento-practico.component';
import {
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {SeguimientoData} from '../../../@core/data/interfaces/rrhh/seguimiento';
import {NbDateService, NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';


describe('SeguimientoPracticoComponent', () => {
  let component: SeguimientoPracticoComponent;
  let fixture: ComponentFixture<SeguimientoPracticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoPracticoComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
      ],
      providers: [
        {
          provide: NbDialogService,
          useValue: Document,
        },
        NbDateService,
        SeguimientoData,
        EstadoRrhhData,
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoPracticoComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
