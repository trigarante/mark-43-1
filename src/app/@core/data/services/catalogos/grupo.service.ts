import { Injectable } from '@angular/core';
import {Grupo, GrupoData} from '../../interfaces/catalogos/grupo';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class GrupoService extends GrupoData {
  private baseURL;

  constructor( private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Grupo[]> {
    return this.http.get<Grupo[]>( this.baseURL + 'v1/grupo');
  }

  post(grupo: Grupo): Observable<Grupo> {
    return this.http.post<Grupo>(this.baseURL + 'v1/grupo', grupo);
  }

  put(idGrupo, grupo: Grupo): Observable<Grupo> {
    return this.http.put<Grupo>(this.baseURL + 'v1/grupo/' + idGrupo, grupo);
  }

  getGrupoById(idGrupo): Observable<Grupo> {
    return this.http.get<Grupo>(this.baseURL + 'v1/grupo/' + idGrupo);
  }
}
