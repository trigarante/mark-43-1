import {Component, Input, OnInit} from '@angular/core';
import {PuestoTipo, PuestoTipoData} from '../../../../../@core/data/interfaces/catalogos/puesto-tipo';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {TipoPuestoComponent} from '../../tipo-puesto/tipo-puesto.component';
import {Router} from '@angular/router';
/** Compoenente del modal del catálogo para "crear tipo de puesto". */
@Component({
  selector: 'ngx-tipo-puesto-create',
  templateUrl: './tipo-puesto-create.component.html',
  styleUrls: ['./tipo-puesto-create.component.scss'],
})
export class TipoPuestoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idPuestoTipo obtenido. */
  @Input() idPuestoTipo: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un tipo de puesto. */
  puestoTipoCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  tipoPuesto: PuestoTipo;
  tipopuestoEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {PuestoTipoData} puesTipoService Servicio del componente puesto-tipo para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private puesTipoService: PuestoTipoData, private fb: FormBuilder, protected ref: NbDialogRef<TipoPuestoComponent>,
              private router: Router) {
  }
  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoPuesto() {
    if (this.puestoTipoCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.puesTipoService.post(this.puestoTipoCreateForm.value).subscribe((result) => {
      this.puesTipoService.postSocket(this.puestoTipoCreateForm.value).subscribe(x => {});
      this.puestoTipoCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que actualiza la información de un tipo de puesto. */
  updateTipoPuesto() {
    this.puesTipoService.getPuestoTipoById(this.idPuestoTipo).subscribe(data => {
      if(data.activo === 1){
        this.puesTipoService.put(this.idPuestoTipo, this.puestoTipoCreateForm.value).subscribe(result => {
          this.puesTipoService.postSocket(this.puestoTipoCreateForm.value).subscribe(x => {});
          this.puestoTipoCreateForm.reset();
          this.ref.close();
        });
      }else{
        this.tipopuestoEliminado =true;
      }
    });

  }

  /** Función que da de baja un tipo de puesto, cambiando su atributo activo a 0. */

  /*bajaTipoPuesto() {
    this.puestoTipoCreateForm.controls['activo'].setValue(0);
    this.puesTipoService.put(this.idPuestoTipo, this.puestoTipoCreateForm.value).subscribe( result => {
      this.puestoTipoCreateForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que obtiene toda la información del tipo de puesto dependiendo de su ID. */
  getTipoPuestoById() {
    this.puesTipoService.getPuestoTipoById(this.idPuestoTipo).subscribe(data => {
      this.tipoPuesto = data;
      this.puestoTipoCreateForm.controls['nombre'].setValue(this.tipoPuesto.nombre);
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getTipoPuestoById();
    }
    this.puestoTipoCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
