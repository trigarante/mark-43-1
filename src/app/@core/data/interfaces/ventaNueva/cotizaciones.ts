import {Observable} from 'rxjs';

export interface Cotizaciones {
  id: number;
  idProducto: number;
  idPagina: number;
  idMedioDifusion: number;
  idTipoContacto: number;
  idEstadoCotizacion: number;
  fechaCreacion: string;
  url: string;
  idTipoPagina: number;
  idCampana: number;
  idProveedor: number;
  regla: number;
  presupuesto: number;
  descripcion: string;
  estado: string;
  idProspecto: number;
  idTipoCategoria: number;
}

export abstract class CotizacionesData {
  abstract get(): Observable<Cotizaciones[]>;

  abstract post(cotizaciones): Observable<Cotizaciones>;

  // abstract getProspectoById(idCotizacion): Observable<Cotizaciones>;

  abstract put(idCotizacion, cotizaciones): Observable<Cotizaciones>;

  abstract createEmisionParam(aseguradora);

  abstract returnEmision(): Observable<string>;
}
