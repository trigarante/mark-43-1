import { Injectable } from '@angular/core';
import {EstadoInspeccion, EstadoInspeccionData} from '../../../interfaces/ventaNueva/catalogos/estado-inspeccion';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {Observable} from 'rxjs';

@Injectable()
export class EstadoInspeccionService extends EstadoInspeccionData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = Global.url;
  }
  get(): Observable<EstadoInspeccion[]> {
    return this.http.get<EstadoInspeccion[]>(this.baseURL + 'estado-inspeccion');
  }

  post(estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.post<EstadoInspeccion>(this.baseURL + 'estado-inspeccion', estadoInspeccion);
  }

  getEstadoInsoeccionById(idEstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.get<EstadoInspeccion>(this.baseURL + 'estado-inspeccion/' + idEstadoInspeccion);
  }

  put(idEstadoInspeccion, estadoInspeccion: EstadoInspeccion): Observable<EstadoInspeccion> {
    return this.http.put<EstadoInspeccion>(this.baseURL + 'estado-inspeccion/' + idEstadoInspeccion, estadoInspeccion);
    // + sessionStorage.getItem('Empleado')
  }
}
