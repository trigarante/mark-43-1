import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {CompetenciasService} from './competencias.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('Competencias service', () => {
  let injector: TestBed;
  let service: CompetenciasService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 19,
    escala: 0,
    estado: 'HABILIDOSO',
    descripcion: 'TEST UNICO',
    activo: 0,
  },
    {
      id: 'competencias',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [CompetenciasService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(CompetenciasService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/competencias');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/competencias', '/19');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/competencias');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/competencias', '/19/74');
      });
    });
  }
})
