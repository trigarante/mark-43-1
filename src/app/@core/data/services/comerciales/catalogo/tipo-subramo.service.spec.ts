import { TestBed } from '@angular/core/testing';

import { TipoSubramoService } from './tipo-subramo.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';

describe('TipoSubramoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TipoSubramoService, HttpClient],
  }));

  it('should be created', () => {
    const service: TipoSubramoService = TestBed.get(TipoSubramoService);
    expect(service).toBeTruthy();
  });
});
