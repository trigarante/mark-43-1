import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {Procesador, ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {Switchs, SwitchsData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-switchs-create',
  templateUrl: './switchs-create.component.html',
  styleUrls: ['./switchs-create.component.scss'],
})
export class SwitchsCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idSwitch: number;
  switch: Switchs;
  switchCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  /**Areglo que inicia los valores del Microservicio*/
  procesadores: Procesador[];
  /**Arreglo auxiliar para  marcas*/
  procesador: any;

  origenEquipos: SelectItem[];

  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SwitchsCreateComponent>, private switchService: SwitchsData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData,
              private procesadorService: ProcesadorSoporteData, private router: Router, private tipoSedeService: TipoSedeData) { }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  getProcesador() {
    this.procesadorService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.procesador = [];
      this.procesadores = data;
      for (let i = 0; i < this.procesadores.length; i++) {
        this.procesador.push({'label': this.procesadores[i].nombreProcesador, 'value': this.procesadores[i].id});
      }
    });
  }

  guardarCreateSwitch() {
    if (this.switchCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.switchService.post(this.switchCreateForm.value).subscribe((result) => {
      this.switchService.postSocket({status: 'ok'}).subscribe(x => {});
      this.switchCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioRedes/', 'switch']);
    });
  }
  /** modificar despues*/
  updateSwitch() {
    this.switchService.put(this.idSwitch, this.switchCreateForm.value).subscribe(result => {
      this.switchService.postSocket({status: 'ok'}).subscribe(x => {});
      this.switchCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioRedes/', 'switch']);
    });
  }
  getSwitchById() {
    this.switchService.getSwitchsById(this.idSwitch).subscribe(data => {
      this.switch = data;
      this.switchCreateForm.controls['folio'].setValue(this.switch.folio);
      this.switchCreateForm.controls['idMarca'].setValue(this.switch.idMarca);
      this.switchCreateForm.controls['idEstadoInventario'].setValue(this.switch.idEstadoInventario);
      this.switchCreateForm.controls['idObservacionesInventario'].setValue(this.switch.idObservacionesInventario);
      // this.switchCreateForm.controls['comentarios'].setValue(this.switch.comentarios);
      this.switchCreateForm.controls['idEmpleado'].setValue(this.switch.idEmpleado);
      this.switchCreateForm.controls['idTipoSede'].setValue(this.switch.idTipoSede);
      this.switchCreateForm.controls['origenRecurso'].setValue(this.switch.origenRecurso);

    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSwitchById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getProcesador();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.switchCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
