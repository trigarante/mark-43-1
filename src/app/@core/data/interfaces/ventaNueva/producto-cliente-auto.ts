import {Observable} from 'rxjs';

export interface ProductoClienteAuto {
  id: number;
  tipo: string;
  datos: string;
  nombre: string;
  correo: string;
  numero: string;
  genero: string;
  edad: string;
  cp: string;
  clave: string;
  marca: string;
  modelo: string;
  paquete: string;
  servicio: string;
  descuento: number;
  movimiento: string;
  aseguradora: string;
  idProspecto: number;
  descripcion: string;
  numeroPlaca: number;
  numeroMotor: number;
  fechaNaimiento: string;
  idTipoCategoria: number;
}

export abstract class ProductoClienteAutoData {

  abstract get(): Observable <ProductoClienteAuto[]>;

  abstract getById(idProductoCliente): Observable <ProductoClienteAuto>;

}
