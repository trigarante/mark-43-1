import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoContactoVnData, TipoContactoVn} from '../../../../@core/data/interfaces/catalogos/tipo-contacto-vn';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TipoContactoCreateComponent} from '../modals/tipo-contacto-create/tipo-contacto-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {environment} from "../../../../../environments/environment";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-contacto-vn',
  templateUrl: './tipo-contacto-vn.component.html',
  styleUrls: ['./tipo-contacto-vn.component.scss'],
})
export class TipoContactoVnComponent implements OnInit {

  cols: any[];


  descripciones: TipoContactoVn[];

  // baja descripcion
  contacto: TipoContactoVn;

  dataSource: any;

  // sockets
  private stompClient = null;
  constructor(private descService: TipoContactoVnData,
    private dialogoService: NbDialogService,
              private toastrService: NbToastrService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vntipocontacto');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelVnTipoContacto', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoContacto(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  ngOnInit() {
    this.connect();
    this.getTipoContacto(0);
    this.cols = ['nombre', 'acciones'];
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  crearContacto() {
    this.dialogoService.open(TipoContactoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => {
      this.getTipoContacto(1);
    });
  }



  showToast() {
    this.toastrService.success('DATOS ACTUALIZADOS!', `Bancos`);
  }

  // Obtener contactos
  getTipoContacto(mensajeAct) {
    this.descService.get().pipe(map(resultado => {
      return resultado.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.descripciones = [];
      this.descripciones = data;
      this.dataSource = new MatTableDataSource(this.descripciones); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }


  // Obtener ID de contacto
  getTipoContactoById(idContacto) {
    this.descService.getContactoById(idContacto).subscribe(data => {
      this.contacto = data;
    });
  }

  // Actualizar información del contacto
  updateTipoContacto(idContacto) {
    this.dialogoService.open(TipoContactoCreateComponent, {
      context: {
        idTipo: 2,
        idContacto: idContacto,
      },
    }).onClose.subscribe(x => {
      this.getTipoContacto(1);
    });
  }

  // Dar de baja contacto
  bajaContacto(idContacto) {
    this.getTipoContactoById(idContacto);
    swal({
      title: '¿Deseas dar de baja este banco?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.contacto['activo'] = 0;
        this.descService.put(idContacto, this.contacto).subscribe(result => {
          this.descService.postSocket({idContacto: idContacto}).subscribe(x => {});
        });
        swal('¡El banco se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoContacto(0);
        }));
      }
    });

  }


}
