import { Injectable } from '@angular/core';
import {LicenciasDisponibles, LicenciasDisponiblesData} from '../../interfaces/ti/licencias-disponibles';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class LicenciasDisponiblesService extends LicenciasDisponiblesData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<LicenciasDisponibles[]> {
    return this.http.get<LicenciasDisponibles[]>(this.baseURL + 'v1/licencias-disponibles/' );
  }
  put(id, cantidad): Observable<LicenciasDisponibles> {
    return this.http.put<LicenciasDisponibles>(this.baseURL + 'v1/licencias-disponibles/'
       + id + '/' + cantidad, cantidad );
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'tiusuarioslicencias/saveTiUsuariosLicencias', json);
  }
  getById(id ): Observable<LicenciasDisponibles> {
    return this.http.get<LicenciasDisponibles>(this.baseURL + 'v1/licencias-disponibles/' + id );
  }
}
