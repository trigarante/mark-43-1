import {Component, Input, OnInit} from '@angular/core';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {CampanaMarketing, CampanaMarketingData} from '../../../../@core/data/interfaces/marketing/campana';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {CampanaComponent} from '../../campana/campana.component';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-campana-marketing-create',
  templateUrl: './campana-marketing-create.component.html',
  styleUrls: ['./campana-marketing-create.component.scss'],
})
export class CampanaMarketingCreateComponent implements OnInit {
  @Input() idCampanaMarketing: number;
  @Input() idTipo: number;
  campanaMarketingForm: FormGroup;
  submitted: Boolean;
  subarea: Subarea[];
  subAreas: any[];
  camapanaMarketing: CampanaMarketing[];

  constructor(protected subAreaService: SubareaData, protected camapanaMarketingService: CampanaMarketingData,
              protected fb: FormBuilder, protected ref: NbDialogRef<CampanaComponent>, private router: Router) {
  }

  updateCampanaMarketing() {
    this.camapanaMarketingService.put(this.idCampanaMarketing, this.campanaMarketingForm.value).subscribe(result => {
      this.camapanaMarketingService.postSocket({status: 'ok'}).subscribe(x => {});
      this.campanaMarketingForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }

  guardarCampanaMarketing() {
    this.submitted = true;
    if (this.campanaMarketingForm.invalid) {
      return;
    }
    this.camapanaMarketingService.post(this.campanaMarketingForm.value).subscribe((result) => {
      this.camapanaMarketingService.postSocket({status: 'ok'}).subscribe(x => {});
      this.campanaMarketingForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/marketing/campana']);
    });
  }
  getCampana() {
    this.camapanaMarketingService.get().subscribe(data => {
      this.camapanaMarketing = data;
    });
  }
  getSubArea() {
    this.subAreaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.subarea = result;
      this.subAreas = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.subAreas.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getSubArea();
    this.campanaMarketingForm = this.fb.group({
      'idSubarea': new FormControl('', Validators.required),
      'nombre': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

}
