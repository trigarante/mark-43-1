import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoSubramo, TipoSubramoData} from '../../../interfaces/comerciales/catalogo/tipo-subramo';

@Injectable({
  providedIn: 'root',
})
export class TipoSubramoService extends TipoSubramoData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoSubramo[]> {
    return this.http.get<TipoSubramo[]>(this.baseURL + 'v1/tipo-subramo');
  }

  post(tipoSubramo: TipoSubramo): Observable<TipoSubramo> {
    return this.http.post<TipoSubramo>(this.baseURL + 'v1/tipo-subramo', tipoSubramo);
  }

  getTipoSubramoById(idTipoSubramo): Observable<TipoSubramo> {
    return this.http.get<TipoSubramo>(this.baseURL + 'v1/tipo-subramo/' + idTipoSubramo);
  }

  put(idTipoSubramo, tipoSubramo: TipoSubramo): Observable<TipoSubramo> {
    return this.http.put<TipoSubramo>(this.baseURL + 'v1/tipo-subramo/' + idTipoSubramo+'/'
      + sessionStorage.getItem('Empleado'), tipoSubramo);
  }
}
