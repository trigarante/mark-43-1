import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EstadoInventario, EstadoInventarioData} from '../../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {NbDialogRef} from '@nebular/theme';
import {EstadoInventarioComponent} from '../../estado-inventario/estado-inventario.component';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'ngx-create-estado-inventario',
  templateUrl: './create-estado-inventario.component.html',
  styleUrls: ['./create-estado-inventario.component.scss'],
})
export class CreateEstadoInventarioComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idEstadoInventario: number;
  estadoInventarioCreateForm: FormGroup;
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  submitted: boolean;
  estadoInventario: EstadoInventario;
  tiposidEstado: SelectItem[];
  constructor( private estadoInventarioService: EstadoInventarioData, private fb: FormBuilder,
               protected ref: NbDialogRef<EstadoInventarioComponent>,
              private router: Router) { }
  updateEstadoInventario() {
    this.estadoInventarioService.put(this.idEstadoInventario, this.estadoInventarioCreateForm.value).subscribe( result => {
      this.estadoInventarioService.postSocket({status: 'ok'}).subscribe(x => {});
      this.estadoInventarioCreateForm.reset();
      this.ref.close();
      // window.location.reload();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/estado-inventario']);
    });
  }
  guardarEstadoInventario() {
    if (this.estadoInventarioCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.estadoInventarioService.post(this.estadoInventarioCreateForm.value).subscribe( (result) => {
      this.estadoInventarioService.postSocket({status: 'ok'}).subscribe(x => {});
      this.estadoInventarioCreateForm.reset();
      this.ref.close();
      // window.location.reload();
      this.router.navigate(['/modulos/ti/soporte/catalogos-soporte/estado-inventario']);
    });
  }
  getEstadoInventarioById() {
this.estadoInventarioService.getEstadoInventarioById(this.idEstadoInventario).subscribe( data => {
  this.estadoInventario = data ;
  this.estadoInventarioCreateForm.controls['estado'].setValue(this.estadoInventario.estado);
  this.estadoInventarioCreateForm.controls['idEstado'].setValue(this.estadoInventario.idEstado);
});
  }

  getTipo() {
    this.tiposidEstado = [];
    this.tiposidEstado.push({label: 'BUENO', value: 1});
    this.tiposidEstado.push({label: 'DESCOMPUESTO', value: 0});


  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getTipo();
    if (this.idTipo === 2) {

      this.getEstadoInventarioById();
    }
    this.estadoInventarioCreateForm = this.fb.group({
      'estado': new FormControl('', Validators.compose([Validators.required])),
      'idEstado': new FormControl('', Validators.compose([Validators.required])),

    });
  }

}
