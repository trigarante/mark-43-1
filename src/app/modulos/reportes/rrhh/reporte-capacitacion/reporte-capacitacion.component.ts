import {Component, ViewEncapsulation, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from  'd3-format';
import * as d3Inter from 'd3-interpolate';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import 'd3-transition';
import * as moment from 'moment';
import {CapacitacionService} from '../../../../@core/data/services/rrhh/capacitacion.service';
import {Capacitacion} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'ngx-reporte-capacitacion',
  encapsulation: ViewEncapsulation.None,    // Necesaria para que se vea chidito. :v
  templateUrl: './reporte-capacitacion.component.html',
  styleUrls: ['./reporte-capacitacion.component.scss'],
  providers: [CapacitacionService],
})
export class ReporteCapacitacionComponent implements OnInit {
  title = 'Prueba';
  capacitacion = [];
  flares: any;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;
  private maxRadius: number;
  x;
  y;
  partition;
  formatNumber;
  root;
  private arc: any;
  private color: any;
  private svg: any;
  datas: any;
  cols: any[];
  etapas: any[];
  dates: any;
  label = 'TOTAL DE CAPACITACION:';
  rangeDates: Date[];
  dataSource: any;

  constructor(private capacitacionService: CapacitacionService) {
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    // this.radius = Math.min(this.width, this.height) / 2;
    this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
    // this.cols = [
    //  {field: 'nombre', header: 'Nombre'},
    //  {field: 'competenciaCandidato', header: 'Candidato'},
    //  {field: 'competencias', header: 'Competencias'},
    //  {field: 'calificacionExamen', header: 'Calificacion Examen'},
    // ];
    this.cols = ['nombre', 'competenciaCandidato', 'competencia', 'calificacionExamen']
    this.etapas = ['Precandidato', 'Candidato', 'Empleado', 'Capacitación', 'Seguimiento 1', 'Seguimiento 2'];
    this.rangeDates = [
      new Date(2018, 11, 1, 0, 0),
      new Date(),
    ];
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.initSvg();
    this.getCapacitacion(null);
  }
  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  private initSvg() {
    const colors = ['#009975', '#1687a7', '#e36488'];
    this.formatNumber = d3Format.format(',d');
    // this.color = d3Scale.scaleOrdinal(colors);
    this.color = d3Scale.scaleOrdinal(colors);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition = d3Partition.partition();

    this.arc = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }

  getCapacitacion( rangeData: Date[]) {
    this.capacitacionService.get()
      .subscribe(data => {
        if (rangeData == null) {
          this.capacitacion = data;
          this.dates = this.capacitacion.length;
          this.drawPie(this.capacitacion);
        } else {
          this.capacitacion = [];

          // console.log(' FORMATO rangeData:: ' + moment(rangeData[1]).valueOf());
          // console.log('Fecha de servicio:: ' + moment(data[0].fechaCreacion).valueOf());

          for (let i = 0; i < data.length; i++) {
            // this.dates = data[i].fechaCreacion;
            if ( moment(data[i].fechaIngreso).valueOf() >= moment(rangeData[0]).valueOf()
              &&  moment(data[i].fechaIngreso).valueOf() <= moment(rangeData[1]).valueOf()) {
              this.capacitacion.push(data[i]);
            }
          }

          this.removeSvg();
          this.drawPie(this.capacitacion);
        }
      });
  }

  private drawPie(data) {
    this.root = d3Partition.hierarchy(this.dataTransform(data));
    this.root.sum(d => d.size);
    this.root.each(d => d.current = d);
    this.svg = d3.select('#graph')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn());
    const slice = this.svg.selectAll('g.slice')
      .data(this.partition(this.root).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn(d);
      });
    this.datas = this.dataFilter({data: this.flares});
    this.dataSource = new MatTableDataSource(this.dataFilter({data: this.flares}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.9 : 0.7)
      .attr('d', this.arc);

    newSlice.append('path')
      .attr('class', 'hidden-arc')
      .attr('id', (_, i) => 'hiddenArc' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', '#ffffff');

    return this.dataSource;
  }

  private dataTransform(data: Capacitacion[]) {
    const totalCandidatos = [];

    const calificacionApto = [];
    const calificacionRemedial = [];
    const calificacionNoApto = [];
    const calfs = [[], [], [], [], [], [], [], [], [], [], []];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idEtapa > 1) {
        if (data[i].idEstado === 1) {
          totalCandidatos.push(data[i]);
          if (data[i].calificacionExamen > 7) {
            calificacionApto.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            calificacionRemedial.push(data[i]);
          } else if (data[i].calificacionExamen <= 5) {
            calificacionNoApto.push(data[i]);
          }
          calfs[data[i].calificacionExamen].push(data[i]);
        }
      }
    }

    this.flares = {
      name: 'Total de Capacitaciones: ' + totalCandidatos.length,
      id: 'capacitacion',
      children: [],
    };

    const apto = {
      name: 'Total de Apto: ' + calificacionApto.length,
      id: 'apto',
      children: [
        {
          name: '10',
          id: '10',
          size: calfs[10].length,
        },
        {
          name: '9',
          id: '9',
          size: calfs[9].length,
        },
        {
          name: '8',
          id: '8',
          size: calfs[8].length,
        },
      ],
    };

    const remedial = {
      name: 'Total de Remedial: ' + calificacionRemedial.length,
      id: 'remedial',
      children: [
        {
          name: '7',
          id: '7',
          size: calfs[7].length,
        },
        {
          name: '6',
          id: '6',
          size: calfs[6].length,
        },
      ],
    };

    const noapto = {
      name: 'Total de No Apto: ' + calificacionNoApto.length,
      id: 'noapto',
      children: [
        {
          name: '5',
          id: '5',
          size: calfs[5].length,
        },
        {
          name: '4',
          id: '4',
          size: calfs[4].length,
        },
        {
          name: '3',
          id: '3',
          size: calfs[3].length,
        },
        {
          name: '2',
          id: '2',
          size: calfs[2].length,
        },
        {
          name: '1',
          id: '1',
          size: calfs[1].length,
        },
        {
          name: '0',
          id: '0',
          size: calfs[0].length,
        },
      ],
    };

    this.flares.children.push(apto);
    this.flares.children.push(remedial);
    this.flares.children.push(noapto);
    return this.flares;
  }

  private focusOn(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg.transition()
      .duration(750)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc')
      .attrTween('d', d => () => this.arc(d));

    transition.selectAll('path.hidden-arc')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private dataFilter(d: any) {
    const data = this.capacitacion;

    const totalCandidatos = [];

    const calificacionApto = [];
    const calificacionRemedial = [];
    const calificacionNoApto = [];
    const calfs = [[], [], [], [], [], [], [], [], [], [], []];

    for (let i = 0; i < data.length; i++) {
      if (data[i].idEtapa > 1) {
        if (data[i].idEstado === 1) {
          totalCandidatos.push(data[i]);
          if (data[i].calificacionExamen > 7) {
            calificacionApto.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            calificacionRemedial.push(data[i]);
          } else if (data[i].calificacionExamen <= 5) {
            calificacionNoApto.push(data[i]);
          }
          calfs[data[i].calificacionExamen].push(data[i]);
        }
      }
    }

    if (d.data.id === 'capacitacion') {
      this.label = 'TOTAL EN CAPACITACION:';
      this.dates = totalCandidatos.length;
      return totalCandidatos;
    } else if (d.data.id === 'apto') {
      this.label = 'TOTAL DE APTOS:';
      this.dates = calificacionApto.length;
      return calificacionApto;
    } else if (d.data.id === 'remedial') {
      this.label = 'TOTAL DE REMEDIAL:';
      this.dates = calificacionRemedial.length;
      return calificacionRemedial;
    } else if (d.data.id === 'noapto') {
      this.label = 'TOTAL DE NO APTOS:';
      this.dates = calificacionNoApto.length;
      return calificacionNoApto;
    } else if (d.data.id === '10') {
      this.label = 'TOTAL EN 10:';
      this.dates = calfs[10].length;
      return calfs[10];
    } else if (d.data.id === '9') {
      this.label = 'TOTAL EN 9:';
      this.dates = calfs[9].length;
      return calfs[9];
    } else if (d.data.id === '8') {
      this.label = 'TOTAL EN 8:';
      this.dates = calfs[8].length;
      return calfs[8];
    } else if (d.data.id === '7') {
      this.label = 'TOTAL EN 7:';
      this.dates = calfs[7].length;
      return calfs[7];
    } else if (d.data.id === '6') {
      this.label = 'TOTAL EN 6:';
      this.dates = calfs[6].length;
      return calfs[6];
    } else if (d.data.id === '5') {
      this.label = 'TOTAL EN 5:';
      this.dates = calfs[5].length;
      return calfs[5];
    } else if (d.data.id === '4') {
      this.label = 'TOTAL EN 4:';
      this.dates = calfs[4].length;
      return calfs[4];
    } else if (d.data.id === '3') {
      this.label = 'TOTAL EN 3:';
      this.dates = calfs[3].length;
      return calfs[3];
    } else if (d.data.id === '2') {
      this.label = 'TOTAL EN 2:';
      this.dates = calfs[2].length;
      return calfs[2];
    } else if (d.data.id === '1') {
      this.label = 'TOTAL EN 1:';
      this.dates = calfs[1].length;
      return calfs[1];
    } else if (d.data.id === '0') {
      this.label = 'TOTAL EN 0:';
      this.dates = calfs[0].length;
      return calfs[0];
    }
  }

  private middleArcLine(d) {
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw

    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path.path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);

    return path.toString();
  }

  private textFits(d) {
    const CHAR_SPACE = 8;
    const deltaAngle = this.x(d.x1) - this.x(d.x0);
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const perimeter = r * deltaAngle;

    return d.data.name.length * CHAR_SPACE < perimeter;
  }

  private removeSvg() {
    this.svg.remove();
  }
}
