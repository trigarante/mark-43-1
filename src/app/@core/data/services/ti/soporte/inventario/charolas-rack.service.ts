import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {CharolasRack, CharolasRackData} from '../../../../interfaces/ti/soporte/inventario/charolas-rack';


@Injectable()
export class CharolasRackService extends CharolasRackData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<CharolasRack[]> {
    return this.http.get<CharolasRack[]>( this.baseURL + 'v1/charolasRack');
  }

  post(charolasRack: CharolasRack): Observable<CharolasRack> {
    return this.http.post<CharolasRack>(this.baseURL + 'v1/charolasRack', charolasRack);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'charolasrack/saveCharolasRack', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajacharolasrack/saveBajaCharolasRack', json);
  }
  getCharolasRackById(idCharolasRack): Observable<CharolasRack> {
    return this.http.get<CharolasRack>(this.baseURL + 'v1/charolasRack/' + idCharolasRack);
  }
  put(idCharolasRack, charolasRack: CharolasRack): Observable<CharolasRack> {
    return this.http.put<CharolasRack>(this.baseURL + 'v1/charolasRack/' + idCharolasRack + '/'
      + sessionStorage.getItem('Empleado') , charolasRack);
  }
}
