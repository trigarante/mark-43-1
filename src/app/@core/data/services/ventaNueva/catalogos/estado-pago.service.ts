import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {EstadoPago} from '../../../interfaces/ventaNueva/catalogos/estado-pago';


@Injectable()
export class EstadoPagoService {
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<EstadoPago[]> {
    return this.http.get<EstadoPago[]>(this.baseURL + 'v1/estado-pago');
  }

  post(estadoPago): Observable<EstadoPago> {
    return this.http.post<EstadoPago>(this.baseURL + 'v1/estado-pago', estadoPago);
  }

  getEstadoPagoById(idEstadoPago): Observable<EstadoPago> {
    return this.http.get<EstadoPago>(this.baseURL + 'v1/estado-pago/' + idEstadoPago);
  }

  put(idEstadoPago, estadoPago): Observable<EstadoPago> {
    return this.http.put<EstadoPago>(this.baseURL + 'v1/estado-pago/' + idEstadoPago, estadoPago);
  }
}
