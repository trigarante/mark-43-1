import {Observable} from 'rxjs';

export interface Seguimiento {
  id: number;
  fechaIngreso: Date;
  calificacionRollplay: number;
  idCapacitacion: number;
  idCompetencia: number;
  calificacion: number;
  idCoach: number;
  idSubarea: number;
  nombreCampana: string;
  asistencia: string;
  registro: number;
  idCliente: number;
  prima: number;
  comentarios: string;
  idEtapa: number;
  empleado: string;
  nombreSubarea: string;
  comentariosFaseDos: number;
  idEstadoRRHH: number;
  sede: number;
  nombre: string;

}

export abstract class SeguimientoData {
  abstract get(): Observable<Seguimiento[]>;

  abstract post(seguimiento, idPrecandidato, idEtapa, fechaIngreso): Observable<Seguimiento>;
  abstract postSocketRolplay(json): Observable<JSON>;
  abstract postSocketAignaciones(json): Observable<JSON>;

  abstract update(idSeguimiento, seguimiento, fechaIngreso): Observable<Seguimiento>;

  abstract fasePractica(idSeguimiento, calificacionRollPlay, comentariosFaseDos, idEtapa, idPrecandidato): Observable<Seguimiento>;

  abstract getSeguimientoById(idSeguimiento): Observable<Seguimiento>;
}
