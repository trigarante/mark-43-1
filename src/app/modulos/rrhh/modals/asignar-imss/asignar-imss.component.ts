import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import * as moment from 'moment';
import {RazonSocial, RazonSociosData} from '../../../../@core/data/interfaces/catalogos/razon-social';
import {map} from 'rxjs/operators';
import {MatTableDataSource} from '@angular/material';

/** Componente del modak "asignar IMSS". */
@Component({
  selector: 'ngx-asignar-imss',
  templateUrl: './asignar-imss.component.html',
  styleUrls: ['./asignar-imss.component.scss'],
})
export class AsignarImssComponent implements OnInit {

  /** Variable que almacena el idEmpleado obtenido. */
  @Input() idEmpleado: number;
  /** Variable que almacena el fechaIngreso obtenido. */
  @Input() fechaIngreso: any;
  /**Nombre del empleado*/
  @Input() nombre: string;
  /** Variable que permite la inicialización del formulario para asignar el IMSS a un empleado. */
  formAsignarImss: FormGroup;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empleado: Empleados;
  /** Arreglo auxiliar para los documentos. */
  documentos: any;
  /** Arreglo auxiliar para la Razon Social. */
  razon: any[];
  /**Variable que inicia los valores de microservicio*/
  razonSocial: RazonSocial[];
  // Web Sockets
  // private stompClientEmpleado = null;
  /**
 * El método constructor invoca a Servicios y Formularios (con control de validaciones).
 *
 * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
 * @param {NbDialogRef<AsignarImssComponent>} ref Referencia al diálogo (mensaje).
 * @param {EmpleadosData} empleadosService Servicio del componente de empleados para poder acceder a sus métodos. */
  constructor(protected ref: NbDialogRef<AsignarImssComponent>, private fb: FormBuilder, protected empleadosService: EmpleadosData,
              protected razonSocialService: RazonSociosData) {
  }
  // Web socket connection
  // connect() {
  //   /** ------------Empleados--------*/
  //     // Web Sockets
  //   const socket2 = new SockJS('https://www.mark-43.net:445/empleados');
  //   this.stompClientEmpleado = Stomp.over(socket2);
  //   this.stompClientEmpleado.connect({}, frame =>  {
  //     console.log(frame);
  //   });
  //   /** ------------Empleados-Imss--------*/
  // }
  /** Función que permite dar de alta IMSS al empleado especificado. */
  asignarImss() {
    this.empleadosService.getEmpleadoById(this.idEmpleado).subscribe(data => {
      this.empleado = data;
      let feAlta: any;
      feAlta = Date.parse(this.formAsignarImss.controls['fechaAltaIMSS'].value);
      let formAlta: any;
      formAlta = {
        'imss': this.formAsignarImss.controls['imss'].value,
        'comentarios': this.formAsignarImss.controls['comentarios'].value,
        'razonSocial': this.formAsignarImss.controls['razonSocial'].value,
      };
      this.fechaIngreso = Date.parse(this.fechaIngreso);
      this.empleadosService.altaImss(this.idEmpleado, this.fechaIngreso, feAlta, formAlta).subscribe(result => {
        const obj = {
          idEmpleado: this.idEmpleado,
          fechaIngreso: this.fechaIngreso,
          feAlta: feAlta, formAlta: formAlta,
        };
        // this.stompClientEmpleadoImss.send('/tasks/saveEmpleadoImss', {}, JSON.stringify(obj));
        this.empleadosService.postSocketImss(obj).subscribe(x => {});
        // this.stompClientEmpleado.send('/tasks/saveEmpleado', {}, JSON.stringify(obj));
        this.empleadosService.postSocket(obj).subscribe(x => {});
        this.formAsignarImss.reset();
        this.ref.close();
        /**window.location.reload();*/
      });
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  getRazonSocial() {
    this.razonSocialService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.razonSocial = [];
      this.razonSocial = data;
      this.razon = [];
      for (let i = 0; i < this.razonSocial.length; i++) {
        this.razon.push({'label': this.razonSocial[i].alias, 'value': this.razonSocial[i].id});
      }
    });
  }

  // getRazon() {
  //   this.razon.push({'label': 'MORAL', 'value': 'MORAL'});
  // }

  /** Función que inicializa el componente. */
  ngOnInit() {
    // this.connect();
    this.getRazonSocial();
    this.formAsignarImss = this.fb.group({
      'fechaAltaIMSS': ['', Validators.required],
      'imss': ['', Validators.required],
      'comentarios': ['', Validators.required],
      'razonSocial': ['', Validators.required],
    });
  }

}
