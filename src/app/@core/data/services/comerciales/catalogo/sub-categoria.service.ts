import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {SubCategoria, SubCategoriaData} from '../../../interfaces/comerciales/catalogo/sub-categoria';

@Injectable()
export class SubCategoriaService extends SubCategoriaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super ();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }
  get(): Observable<SubCategoria[]> {
    return this.http.get<SubCategoria[]>(this.baseURL + 'v1/sub-categoria');
  }

  post(subCategoria: SubCategoria): Observable<SubCategoria> {
    return this.http.post<SubCategoria>(this.baseURL + 'v1/sub-categoria', subCategoria);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'subcategoria/saveSubCategoria', json);
  }

  getSubCategoriaById(idSubCategoria): Observable<SubCategoria> {
    return this.http.get<SubCategoria>(this.baseURL + 'v1/sub-categoria/' + idSubCategoria);
  }

  put(idSubCategoria, subCategoria: SubCategoria): Observable<SubCategoria> {
    return this.http.put<SubCategoria>(this.baseURL + 'v1/sub-categoria/' + idSubCategoria + '/'
      + sessionStorage.getItem('Empleado'), subCategoria);
  }

}
