import {Observable} from 'rxjs';
import {Domiciliacion} from './domiciliacion';
import {Tarjeta} from './tarjeta';
import {Transferencia} from './transferencia';

export interface Pagos {
  idRecibo?: number;
  idFormaPago: number;
  deposito: string;
  domiciliacion: Domiciliacion;
  tarjetaCreditoAhorra: Tarjeta;
  tarjetaCreditoAseguradora: Tarjeta;
  tarjetaDebito: Tarjeta;
  transferencia: Transferencia;
  idestadoPago: number;
  fechaPago: Date;
  cantidad: number;
  // archivo: any;
  // datos: any;
}
export abstract class PagosData {
  abstract get(): Observable<Pagos[]>;

  abstract getPagoById(id): Observable<Pagos>;

  abstract post(pago): Observable<Pagos>;

  abstract put(id, pago): Observable<Pagos>;


}
