import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BancoComponent} from '../../../rrhh/catalogos/banco/banco.component';
import {Empresa, EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-create-empresa',
  templateUrl: './create-empresa.component.html',
  styleUrls: ['./create-empresa.component.scss'],
})
export class CreateEmpresaComponent implements OnInit {
  @Input() idGrupo: number;
  @Input() idTipo: number;
  @Input() idEmpresa: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una empresa. */
  empresaForm: FormGroup;
  /** Bandera para verifica que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa;
  constructor(
    private fb: FormBuilder, protected ref: NbDialogRef<BancoComponent>, private empresaService: EmpresaData,
    private router: Router,
  ) {
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  guardarEmpresa() {
    if (this.empresaForm.invalid) {
      return;
    }
    this.submitted = true;
    this.empresaService.post(this.empresaForm.value).subscribe((result) => {
      this.empresaForm.reset();
      this.router.navigate(['/modulos/finanzas/empresas']);
      this.empresaForm.reset();
      this.ref.close();
    });
  }
  getEmpresaById() {
    this.empresaService.getEmpresasById(this.idGrupo).subscribe( data => {
      this.empresas = data;
      this.empresaForm.controls['nombre'].setValue(this.empresas.nombre);
      this.empresaForm.controls['descripcion'].setValue(this.empresas.descripcion);
      this.empresaForm.controls['activo'].setValue(this.empresas.activo);
      this.empresaForm.controls['codigo'].setValue(this.empresas.codigo);
    });
  }
  /** Función que actualiza los datos de una empresa. */
  updateEmprea() {
    this.empresaService.put(this.idGrupo, this.empresaForm.value).subscribe( result => {
      this.empresaForm.reset();
      this.ref.close();
    });
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEmpresaById();
    }
    this.empresaForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
      'idGrupo': this.idGrupo,
      'codigo': new FormControl('', Validators.compose([Validators.required])),
    });
  }

}
