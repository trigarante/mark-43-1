import {Observable} from 'rxjs';


export interface Grupo {
  id: number;
  descripcion: string;
  idPais: number;
  nombre: string;
  activo: number;
  codigo: string;
}

export abstract class GrupoData {
  abstract get(): Observable<Grupo[]>;

  abstract post(grupo): Observable<Grupo>;

  abstract getGrupoById(idGrupo): Observable<Grupo>;

  abstract put(idGrupo, grupo): Observable<Grupo>;
}
