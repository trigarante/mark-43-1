import {Observable} from 'rxjs';


export interface EstadoPago {
  id: number;
  description: string;
  activo: number;
}

export abstract class EstadoPagoData {
  abstract get(): Observable<EstadoPago[]>;
  abstract post(estadoPago): Observable<EstadoPago>;
  abstract ftygetEstadoPagoById(idEstadoPago): Observable<EstadoPago>;
  abstract put(idEstadoPago, estadoPago): Observable<EstadoPago>;
}
