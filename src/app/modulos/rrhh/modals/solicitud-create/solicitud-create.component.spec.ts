import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {SolicitudCreateComponent} from './solicitud-create.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbDialogRef, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {BolsaTrabajoData} from '../../../../@core/data/interfaces/catalogos/bolsa-trabajo';
import {VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';


describe('SolicitudCreateComponent', () => {
  let component: SolicitudCreateComponent;
  let fixture: ComponentFixture<SolicitudCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
      ],
      providers: [
        BolsaTrabajoData,
        VacanteData,
        SolicitudesData,
        SedeData,
        EmpleadosData,
        NbToastrService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
