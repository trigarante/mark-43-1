import { TestBed } from '@angular/core/testing';

import { EstadoCampanaService } from './estado-campana.service';

describe('EstadoCampanaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoCampanaService = TestBed.get(EstadoCampanaService);
    expect(service).toBeTruthy();
  });
});
