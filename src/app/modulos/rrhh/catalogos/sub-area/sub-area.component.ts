import {Component, OnInit, ViewChild} from '@angular/core';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {NbDialogService} from '@nebular/theme';
import {SubAreaCreateComponent} from '../modals/sub-area-create/sub-area-create.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'ngx-sub-area',
  templateUrl: './sub-area.component.html',
  styleUrls: ['./sub-area.component.scss'],
})
export class SubAreaComponent implements OnInit {
  subArea: Subarea[];
  cols: any[];
  subAreas: Subarea;
  dataSource: any;
  permisos: any;
  escritura: boolean = false;
  constructor(protected subAreaService: SubareaData, private dialogoService: NbDialogService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getSubArea() {
    this.subAreaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.subArea = [];
      this.subArea = data;
      this.dataSource = new MatTableDataSource(this.subArea); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      });
  }

  getSubAreasById(idSubarea) {
    this.subAreaService.getById(idSubarea).subscribe(data => {
      this.subAreas = data;
    });
  }

  crearSubArea() {
    this.dialogoService.open(SubAreaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getSubArea();
    });
  }

  updateSubArea(idSubArea) {
    this.dialogoService.open(SubAreaCreateComponent, {
      context: {
        idTipo: 2,
        idSubArea: idSubArea,
      },
    }).onClose.subscribe(x => {
      this.getSubArea();
    });
  }

  bajaSubArea(idSubarea) {
this.getSubAreasById(idSubarea);
    swal({
      title: '¿Deseas dar de baja esta subarea?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.subAreas['activo'] = 0;
        this.subAreaService.put(idSubarea, this.subAreas).subscribe( result => {
        });
        swal('¡El banco se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          //window.location.reload();
          this.getSubArea();
        }));
      }
    });
  }

  ngOnInit() {
    this.getSubArea();
    this.cols = ['idArea', 'subarea', 'descripcion', 'acciones'];
    this.getPermisos();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.solicitudes.escritura;
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
