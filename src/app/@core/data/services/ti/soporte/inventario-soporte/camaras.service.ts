import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Camaras, CamarasData} from '../../../../interfaces/ti/soporte/inventario-soporte/camaras';
import {Observable} from 'rxjs';

@Injectable()
export class CamarasService extends CamarasData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Camaras[]> {
    return this.http.get<Camaras[]>(this.baseURL + 'v1/camaras');
  }

  post(camaras: Camaras): Observable<Camaras> {
    return this.http.post<Camaras>(this.baseURL + 'v1/camaras', camaras);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'camara/saveCamara', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajacamara/saveBajaCamara', json);
  }

  getCamarasById(idCamara): Observable<Camaras> {
    return this.http.get<Camaras>(this.baseURL + 'v1/camaras/' + idCamara);
  }

  put(idCamara, camaras: Camaras): Observable<Camaras> {
    return this.http.put<Camaras>(this.baseURL + 'v1/camaras/' + idCamara  + '/'
      + sessionStorage.getItem('Empleado'), camaras);
  }
}
