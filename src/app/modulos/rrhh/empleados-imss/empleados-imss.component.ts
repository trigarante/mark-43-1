import {Component, OnInit, ViewChild} from '@angular/core';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {map} from 'rxjs/operators';
import * as moment from 'moment';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarImssComponent} from '../modals/asignar-imss/asignar-imss.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {environment} from '../../../../environments/environment';

/** Componente para dar de alta IMSS de un empleado. */
@Component({
  selector: 'ngx-empleados-imss',
  templateUrl: './empleados-imss.component.html',
  styleUrls: ['./empleados-imss.component.scss'],
})
export class EmpleadosImssComponent implements OnInit {

  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio */
  empleado: Empleados[];
  a: any;
  b: any;
  diaIngreso: any;
  element: any;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   *
   * En este caso: Servicio de Empleados.
   * @param {EmpleadosData} empleadoService Servicio del componente Empleados para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio de mensajes (diálogos). */
  constructor(protected empleadoService: EmpleadosData, private dialogService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'empleadosimss');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelEmpleadosImss', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEmpleado(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getEmpleado(mensajeAct) {
    const fechaSemana: any = moment(Date.now()).toISOString();
    this.empleadoService.get().pipe(map(result => {
      return result.filter(data => data.idEstadoRH === 1 && data.fechaAltaIMSS === null &&
        Math.abs(moment(data.fechaIngreso).diff(fechaSemana, 'days')) + 1 > 4 ||
        data.idEstadoRH === 16 && data.fechaAltaIMSS === null &&
        Math.abs(moment(data.fechaIngreso).diff(fechaSemana, 'days')) + 1 > 4);
    })).subscribe(data => {
      this.empleado = [];
      this.empleado = data;
      this.dataSource = new MatTableDataSource(this.empleado); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
      // for (let i = 0; i < this.empleado.length; i++) {
      //   this.empleado[i].fechaIngreso = moment(this.empleado[i].fechaIngreso).format('DD-MM-YYYY');
      // }
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Empleado-IMSS`);
  }
  /** Esta función despliega un modal que permite establecer información para dar de alta el IMSS de un empleado. */
  altaImss(idEmpleado: number, fechaIngreso, nombre, apaterno, amaterno) {
    this.dialogService.open(AsignarImssComponent, {
      context: {
        idEmpleado: idEmpleado,
        fechaIngreso: fechaIngreso,
        nombre: nombre + ' ' + apaterno + ' ' + amaterno,
      },
    }).onClose.subscribe(x => {
      this.getEmpleado(1);
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.empleados.escritura;
    console.log(this.escritura);
  }
  estaActivo() {
    this.activo = this.permisos.modulos.empleados.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  ngOnInit() {
    this.connect();
    this.getEmpleado(0);
    this.cols = ['nombre', 'telefono', 'email', 'fechaIngreso', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getEmpleado(0);
  }
}
