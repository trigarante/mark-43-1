import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  Subarea,
  SubareaData,
} from '../../../@core/data/interfaces/catalogos/subarea';
import { map } from 'rxjs/operators';
import {
  Puesto,
  PuestoData,
} from '../../../@core/data/interfaces/catalogos/puesto';
import {
  GrupoUsuarios,
  GrupoUsuariosData,
} from '../../../@core/data/interfaces/ti/grupoUsuarios';
import { Router } from '@angular/router';
import { Sede, SedeData } from '../../../@core/data/interfaces/catalogos/sede';
import {
  Grupo,
  GrupoData,
} from '../../../@core/data/interfaces/catalogos/grupo';
import {
  Empresa,
  EmpresaData,
} from '../../../@core/data/interfaces/catalogos/empresa';
import { Area, AreaData } from '../../../@core/data/interfaces/catalogos/area';
import { Permiso } from './permisos';
import * as _ from 'lodash';
import {
  MarcaEmpresarial,
} from '../../../@core/data/interfaces/catalogos/marca-empresarial';
import { PermisosVisualizacion } from '../../../@core/data/interfaces/ti/catalogos/permisos-visualizacion';
import { PermisosVisualizacionService } from '../../../@core/data/services/ti/permisos-visualizacion.service';
import {falseIfMissing} from "protractor/built/util";

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-crear-permisos',
  templateUrl: './crear-permisos.component.html',
  styleUrls: ['./crear-permisos.component.scss'],
})
export class CrearPermisosComponent implements OnInit {
  permisosForm: FormGroup;
  sub: any[];
  seleccion2: any[];
  permisos: string[];
  permiso: object;
  grupoUsuario: GrupoUsuarios;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarPuesto: boolean;
  mostrarPermisoVisualizacion: boolean;
  activoPuesto: boolean;

  Sedes: Sede[];
  Sede: any[];
  Grupos: Grupo[];
  grupo: any[];
  empresas: Empresa[];
  empresa: any[];
  areas: Area[];
  area: any[];
  subareas: Subarea[];
  subarea: any[];
  puestos: Puesto[];
  puesto: any[];
  /** Arreglo auxiliar para permiso de visualización. */
  permisosVisualizacion: any[];
  marcaEmpresarial: MarcaEmpresarial[];
  marcasEmpresariales: any[];
  mostrarMarcasEmpresariales: boolean;
  // permisos
  permisos_rh: string[]; // rh
  camposRH = [];
  permisosRH: any;
  permisos_ti: string[]; // ti
  camposTI = [];
  permisosTI: any;
  permisos_c: string[]; // comercial
  camposC = [];
  permisosC: any;
  permisos_m: string[]; // marketing
  camposM = [];
  permisosM: any;
  // socket
  disabled = true;
  private stompClient = null;
  dataSource: any;
  datos: any[] = [];
  /**
   * El método constructor manda a llamar Servicios y Formularios .
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {SubareaData} subareaService Servicio del componente subarea para poder invocar los métodos.
   * @param {PuestoData} puestoService Servicio del componente puestos para poder invocar los métodos.
   * */
  constructor(
    private fb: FormBuilder,
    protected subareaService: SubareaData,
    protected puestoService: PuestoData,
    private grupoUsuariosService: GrupoUsuariosData,
    private router: Router,
    private sedesService: SedeData,
    private grupoService: GrupoData,
    private empresaService: EmpresaData,
    private areaService: AreaData,
    private permisosVisualizacionService: PermisosVisualizacionService,
   // private marcaEmpresarialService: MarcaEmpresarialData,
  ) {
    this.initForma(); // se inicializa el formulario
  } // constructor

  ngOnInit() {
    this.initPermisos();
    this.getGrupo();
    this.seleccion();
  }

  initPermisos() {
    this.permisos_rh = Permiso.permisosRH;
    this.permisos_rh.sort();
    this.permisos_ti = Permiso.permisosTI;
    this.permisos_ti.sort();
    this.permisos_m = Permiso.permisosMARKETING;
    this.permisos_m.sort();
    this.permisos_c = Permiso.permisosCOMERCIAL;
    this.permisos_c.sort();
  }

  seleccion() {
    this.seleccion2 = [];
    this.seleccion2.push({ label: 'true', value: true });
    this.seleccion2.push({ label: 'false', value: false });
  }

  initForma() {
    this.permisosForm = this.fb.group({
      nombre: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[A-Za-z].*$'),
        ]),
      ),
      descripcion: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[A-Za-z].*$'),
        ]),
      ),
      idGrupo: new FormControl('', Validators.compose([Validators.required])),
      idEmpresa: new FormControl('', Validators.compose([Validators.required])),
      idSede: new FormControl('', Validators.compose([Validators.required])),
      idArea: new FormControl('', Validators.compose([Validators.required])),
      idSubarea: new FormControl('', Validators.compose([Validators.required])),
      idPuesto: new FormControl('', Validators.compose([Validators.required])),
      idPermisosVisualizacion: new FormControl('', Validators.compose([Validators.required])),

      subareaSelected: {},
      puestoSelected: {},
      descargarPNC: false,
      descargarPNE: false,
      // rh - activo
      ACTIVORECLUTAMIENTO: false,
      ACTIVOSOLICITUDES: false,
      ACTIVOPRECANDIDATOS: false,
      ACTIVOCANDIDATO: false,
      ACTIVOEMPLEADOS: false,
      ACTIVOTEORICO: false,
      ACTIVOROLEPLAY: false,
      ACTIVOVENTANUEVA: false,
      ACTIVOPREEMPLEADOS: false,
      ACTIVOVACANTES: false,
      ACTIVOSEGUIMIENTO: false,
      ACTIVOBAJAS: false,
      ACTIVOCAPITALHUMANO: false,
      ACTIVOBAJAEMPLEADOS: false,
      ACTIVOALTADEIMSS: false,
      ACTIVOCATALOGOS: false,
      ACTIVOAREAS: false,
      ACTIVOBANCO: false,
      ACTIVOBOLSADETRABAJO: false,
      ACTIVOCOMPETENCIAS: false,
      ACTIVOCOMPETENCIASCANDIDATO: false,
      ACTIVOEMPRESA: false,
      ACTIVOESCOLARIDAD: false,
      ACTIVOESTADODEESCOLARIDAD: false,
      ACTIVOESTADOCIVIL: false,
      ACTIVOMEDIODETRANSPORTE: false,
      ACTIVOMOTIVOSDEBAJA: false,
      ACTIVOPAISES: false,
      ACTIVOPUESTO: false,
      ACTIVOTIPODEPUESTO: false,
      ACTIVORAZONSOCIAL: false,
      ACTIVOTURNO: false,
      ACTIVORECURSOSHUMANOS: false,
      ACTIVOREPORTESRH: false,
      ACTIVOREPORTEPRECANDIDATO: false,
      ACTIVOREPORTECANDIDATO: false,
      ACTIVOREPORTECAPACITACION: false,
      ACTIVOREPORTETEORICO: false,
      ACTIVOREPORTEEMPLEADO: false,
      ACTIVOREPORTECAPITALHUMANO: false,
      ACTIVOPREGUNTAS: false,
      ACTIVOGENERARPREGUNTAS: false,
      ACTIVOGENERARRESPUESTAS: false,
      ACTIVORESULTADOS: false,
      // ti - activo
      ACTIVOTI: false,
      ACTIVOADMINISTRACIONTI: false,
      ACTIVOGRUPOPERMISOS: false,
      ACTIVOREACTIVARPERMISOS: false,
      ACTIVOUSUARIOSTI: false,
      ACTIVOEXTENSIONES: false,
      ACTIVOASIGNACIONES: false,
      ACTIVOUSUARIOASIGNACIONES: false,
      ACTIVOSUPERVISOR: false,
      ACTIVOSOPORTE: false,
      ACTIVOINVENTARIOUSUARIO: false,
      ACTIVOINVENTARIOREDES: false,
      ACTIVOINVENTARIOSOPORTE: false,
      ACTIVOBAJASINVENTARIOUSUARIO: false,
      ACTIVOBAJASINVENTARIOREDES: false,
      ACTIVOBAJASINVENTARIOSOPORTE: false,
      ACTIVOCATALOGOSSOPORTE: false,
      ACTIVOESTADODEINVENTARIO: false,
      ACTIVOOBSERVACIONESDEINVENTARIO: false,
      ACTIVOMARCAS: false,
      ACTIVOPROCESADORES: false,
      ACTIVOREPORTESTI: false,
      ACTIVOREPORTEGRUPOPERMISO: false,
      ACTIVOREPORTEUSUARIOS: false,
      ACTIVOREPORTEEXTENSIONES: false,
      ACTIVOREPORTESOPORTETI: false,
      ACTIVOREPORTEINVENTARIOUSUARIO: false,
      ACTIVOREPORTEINVENTARIOREDES: false,
      ACTIVOREPORTEINVENTARIOSOPORTE: false,
      ACTIVOCONTROLLLAMADAS: false,
      // marketing - activo
      ACTIVOMARKETING: false,
      ACTIVOCAMPAÑA: false,
      ACTIVOVERCAMPAÑA: false,
      ACTIVOMEDIODIFUSION: false,
      ACTIVOCAMPAÑACATEGORIA: false,
      ACTIVOPAGINAS: false,
      ACTIVOCATALOGOSMARKETING: false,
      ACTIVOTIPOPAGINAS: false,
      ACTIVOPROVEDORLEADS: false,
      // comercial - activo
      ACTIVOCOMERCIAL: false,
      ACTIVOSOCIOS: false,
      ACTIVOVERSOCIOS: false,
      ACTIVOCONTACTO: false,
      ACTIVOURL: false,
      ACTIVOOBJETIVODEVENTAS: false,
      ACTIVOCONTRATO: false,
      ACTIVORAMO: false,
      ACTIVOSUBRAMO: false,
      ACTIVOVERSUBRAMO: false,
      ACTIVOCONVENIOS: false,
      ACTIVOPRODUCTO: false,
      ACTIVOVERPRODUCTO: false,
      ACTIVOCOMPETENCIAPRODUCTO: false,
      ACTIVOANALISISCOMPETENCIA: false,
      ACTIVOCOBERTURA: false,
      ACTIVOCATALOGOSCOMERCIAL: false,
      ACTIVOTIPOGIRO: false,
      ACTIVOTIPOCATEGORIA: false,
      ACTIVOTIPOCOMPETENCIA: false,
      ACTIVOTIPOPRODUCTO: false,
      ACTIVODIVISAS: false,
      ACTIVOCATEGORIA: false,
      ACTIVOSUBCATEGORIA: false,
      ACTIVOTIPOCOBERTURA: false,
      ACTIVOBAJASSOCIOS: false,
      ACTIVOREPORTESCOMERCIAL: false,
      ACTIVOREPORTESOCIOS: false,
      // rh -escritura
      ESCRITURARECLUTAMIENTO: false,
      ESCRITURASOLICITUDES: false,
      ESCRITURAPRECANDIDATOS: false,
      ESCRITURACANDIDATO: false,
      ESCRITURAEMPLEADOS: false,
      ESCRITURATEORICO: false,
      ESCRITURAROLEPLAY: false,
      ESCRITURAVENTANUEVA: false,
      ESCRITURAPREEMPLEADOS: false,
      ESCRITURAVACANTES: false,
      ESCRITURASEGUIMIENTO: false,
      ESCRITURABAJAS: false,
      ESCRITURACAPITALHUMANO: false,
      ESCRITURABAJAEMPLEADOS: false,
      ESCRITURAALTADEIMSS: false,
      ESCRITURACATALOGOS: false,
      ESCRITURAAREAS: false,
      ESCRITURABANCO: false,
      ESCRITURABOLSADETRABAJO: false,
      ESCRITURACOMPETENCIAS: false,
      ESCRITURACOMPETENCIASCANDIDATO: false,
      ESCRITURAEMPRESA: false,
      ESCRITURAESCOLARIDAD: false,
      ESCRITURAESTADODEESCOLARIDAD: false,
      ESCRITURAESTADOCIVIL: false,
      ESCRITURAMEDIODETRANSPORTE: false,
      ESCRITURAMOTIVOSDEBAJA: false,
      ESCRITURAPAISES: false,
      ESCRITURAPUESTO: false,
      ESCRITURATIPODEPUESTO: false,
      ESCRITURARAZONSOCIAL: false,
      ESCRITURATURNO: false,
      ESCRITURARECURSOSHUMANOS: false,
      ESCRITURAREPORTESRH: false,
      ESCRITURAREPORTEPRECANDIDATO: false,
      ESCRITURAREPORTECANDIDATO: false,
      ESCRITURAREPORTECAPACITACION: false,
      ESCRITURAREPORTETEORICO: false,
      ESCRITURAREPORTEEMPLEADO: false,
      ESCRITURAREPORTECAPITALHUMANO: false,
      ESCRITURAPREGUNTAS: false,
      ESCRITURAGENERARPREGUNTAS: false,
      ESCRITURAGENERARRESPUESTAS: false,
      ESCRITURARESULTADOS: false,
      // ti - escritura
      ESCRITURATI: false,
      ESCRITURAADMINISTRACIONTI: false,
      ESCRITURAGRUPOPERMISOS: false,
      ESCRITURAREACTIVARPERMISOS: false,
      ESCRITURAUSUARIOSTI: false,
      ESCRITURAEXTENSIONES: false,
      ESCRITURAASIGNACIONES: false,
      ESCRITURAUSUARIOASIGNACIONES: false,
      ESCRITURASUPERVISOR: false,
      ESCRITURASOPORTE: false,
      ESCRITURAINVENTARIOUSUARIO: false,
      ESCRITURAINVENTARIOREDES: false,
      ESCRITURAINVENTARIOSOPORTE: false,
      ESCRITURABAJASINVENTARIOUSUARIO: false,
      ESCRITURABAJASINVENTARIOREDES: false,
      ESCRITURABAJASINVENTARIOSOPORTE: false,
      ESCRITURACATALOGOSSOPORTE: false,
      ESCRITURAESTADODEINVENTARIO: false,
      ESCRITURAOBSERVACIONESDEINVENTARIO: false,
      ESCRITURAMARCAS: false,
      ESCRITURAPROCESADORES: false,
      ESCRITURAREPORTESTI: false,
      ESCRITURAREPORTEGRUPOPERMISO: false,
      ESCRITURAREPORTEUSUARIOS: false,
      ESCRITURAREPORTEEXTENSIONES: false,
      ESCRITURAREPORTESOPORTETI: false,
      ESCRITURAREPORTEINVENTARIOUSUARIO: false,
      ESCRITURAREPORTEINVENTARIOREDES: false,
      ESCRITURAREPORTEINVENTARIOSOPORTE: false,
      ESCRITURACONTROLLLAMADAS: false,
      // marketing - escritura
      ESCRITURAMARKETING: false,
      ESCRITURACAMPAÑA: false,
      ESCRITURAVERCAMPAÑA: false,
      ESCRITURAMEDIODIFUSION: false,
      ESCRITURACAMPAÑACATEGORIA: false,
      ESCRITURAPAGINAS: false,
      ESCRITURACATALOGOSMARKETING: false,
      ESCRITURATIPOPAGINAS: false,
      ESCRITURAPROVEDORLEADS: false,
      // comercial - escritura
      ESCRITURACOMERCIAL: false,
      ESCRITURASOCIOS: false,
      ESCRITURAVERSOCIOS: false,
      ESCRITURACONTACTO: false,
      ESCRITURAURL: false,
      ESCRITURAOBJETIVODEVENTAS: false,
      ESCRITURACONTRATO: false,
      ESCRITURARAMO: false,
      ESCRITURASUBRAMO: false,
      ESCRITURAVERSUBRAMO: false,
      ESCRITURACONVENIOS: false,
      ESCRITURAPRODUCTO: false,
      ESCRITURAVERPRODUCTO: false,
      ESCRITURACOMPETENCIAPRODUCTO: false,
      ESCRITURAANALISISCOMPETENCIA: false,
      ESCRITURACOBERTURA: false,
      ESCRITURACATALOGOSCOMERCIAL: false,
      ESCRITURATIPOGIRO: false,
      ESCRITURATIPOCATEGORIA: false,
      ESCRITURATIPOCOMPETENCIA: false,
      ESCRITURATIPOPRODUCTO: false,
      ESCRITURADIVISAS: false,
      ESCRITURACATEGORIA: false,
      ESCRITURASUBCATEGORIA: false,
      ESCRITURATIPOCOBERTURA: false,
      ESCRITURABAJASSOCIOS: false,
      ESCRITURAREPORTESCOMERCIAL: false,
      ESCRITURAREPORTESOCIOS: false,
    }); // formGroup
  }

  getGrupo() {
    this.grupoService
      .get()
      .pipe(
        map(result => {
          return result.filter(valor => valor.activo === 1);
        }),
      )
      .subscribe(data => {
        this.Grupos = data;
        if (this.Grupos.length === 0) {
          this.mostrarGrupo = true;
        } else {
          this.mostrarGrupo = false;
        }
        this.grupo = [];
        for (let i = 0; i < this.Grupos.length; i++) {
          this.grupo.push({
            label: this.Grupos[i].nombre,
            value: this.Grupos[i].id,
          });
        }
      });
  }

  getEmpresa(idGrupo) {
    this.empresaService
      .get()
      .pipe(
        map(result => {
          return result.filter(
            valor => valor.activo === 1 && valor.idGrupo === idGrupo,
          );
        }),
      )
      .subscribe(data => {
        this.empresas = data;
        if (this.empresas.length === 0) {
          this.mostrarEmpresa = true;
        } else {
          this.mostrarEmpresa = false;
        }
        this.empresa = [];
        for (let i = 0; i < this.empresas.length; i++) {
          this.empresa.push({
            label: this.empresas[i].nombre,
            value: this.empresas[i].id,
          });
        }
      });
  }

  getSedes(idEmpresa) {
    this.sedesService
      .get()
      .pipe(
        map(result => {
          return result.filter(valor => valor.idEmpresa === idEmpresa);
        }),
      )
      .subscribe(data => {
        this.Sedes = data;
        if (this.Sedes.length === 0) {
          this.mostrarSede = true;
        } else {
          this.mostrarEmpresa = false;
        }
        this.Sede = [];
        for (let i = 0; i < this.Sedes.length; i++) {
          this.Sede.push({
            label: this.Sedes[i].nombre,
            value: this.Sedes[i].id,
          });
        }
      });
  }

  getArea(idSede) {
    this.areaService
      .get()
      .pipe(
        map(result => {
          return result.filter(
            valor =>
                valor.activo === 1 &&
                valor.idSede === idSede,
            );
          }),
      )
      .subscribe(data => {
        this.areas = data;
        if (this.areas.length === 0) {
          this.mostrarArea = true;
        } else {
          this.mostrarArea = false;
        }
        this.area = [];
        for (let i = 0; i < this.areas.length; i++) {
          this.area.push({
            label: this.areas[i].nombre,
            value: this.areas[i].id,
          });
        }
      });
  }

  getSubarea(idArea) {
    this.subareaService
      .get()
      .pipe(
        map(result => {
          return result.filter(
            valor => valor.activo === 1 && valor.idArea === idArea,
          );
        }),
      )
      .subscribe(data => {
        this.subareas = data;
        if (this.subareas.length === 0) {
          this.mostrarSubarea = true;
        } else {
          this.mostrarSubarea = false;
          this.activoPuesto = false;
        }
        this.subarea = [];
        for (let i = 0; i < this.subareas.length; i++) {
          this.subarea.push({
            label: this.subareas[i].subarea,
            value: this.subareas[i].id,
          });
        }
      });
  }

  getPuesto() {
    this.puestoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.puestos = data;
      if (this.puestos.length === 0) {
        this.mostrarPuesto = true;
      }else {
        this.mostrarPuesto = false;
      }
      this.puesto = [];
      for (let i = 0; i < this.puestos.length; i++) {
        this.puesto.push({'label': this.puestos[i].nombre, 'value': this.puestos[i].id});
      }
    });
  }

  getPermisosVisualizacion() {
    this.permisosVisualizacionService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
        this.permisosVisualizacion = data;
        if (this.permisosVisualizacion.length === 0) {
          this.mostrarPermisoVisualizacion = true;
        } else {
          this.mostrarPermisoVisualizacion = false;
        }
    });
  }

  guardarGrupoUsuario() {
    /** contante que guarda los valores de permisos de los modulos */
    this.permiso = {
      botones: {
        descargaPNC: this.permisosForm.value.descargarPNC,
        descargaPNE: this.permisosForm.value.descargarPNE,
      },
      modulos: {
        // rh
        reclutamiento: {
          activo: this.permisosForm.value.ACTIVORECLUTAMIENTO,
          escritura: this.permisosForm.value.ESCRITURARECLUTAMIENTO,
        },
        solicitudes: {
          activo: this.permisosForm.value.ACTIVOSOLICITUDES,
          escritura: this.permisosForm.value.ESCRITURASOLICITUDES,
        },
        precandidatos: {
          activo: this.permisosForm.value.ACTIVOPRECANDIDATOS,
          escritura: this.permisosForm.value.ESCRITURAPRECANDIDATOS,
        },
        candidato: {
          activo: this.permisosForm.value.ACTIVOCANDIDATO,
          escritura: this.permisosForm.value.ESCRITURACANDIDATO,
        },
        empleados: {
          activo: this.permisosForm.value.ACTIVOEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURAEMPLEADOS,
        },
        teorico: {
          activo: this.permisosForm.value.ACTIVOTEORICO,
          escritura: this.permisosForm.value.ESCRITURATEORICO,
        },
        roleplay: {
          activo: this.permisosForm.value.ACTIVOROLEPLAY,
          escritura: this.permisosForm.value.ESCRITURAROLEPLAY,
        },
        ventaNueva: {
          activo: this.permisosForm.value.ACTIVOVENTANUEVA,
          escritura: this.permisosForm.value.ESCRITURAVENTANUEVA,
        },
        preEmpleado: {
          activo: this.permisosForm.value.ACTIVOPREEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURAPREEMPLEADOS,
        },
        vacantes: {
          activo: this.permisosForm.value.ACTIVOVACANTES,
          escritura: this.permisosForm.value.ESCRITURAVACANTES,
        },
        seguimiento: {
          activo: this.permisosForm.value.ACTIVOSEGUIMIENTO,
          escritura: this.permisosForm.value.ESCRITURASEGUIMIENTO,
        },
        bajas: {
          activo: this.permisosForm.value.ACTIVOBAJAS,
          escritura: this.permisosForm.value.ESCRITURABAJAS,
        },
        capitalHumano: {
          activo: this.permisosForm.value.ACTIVOCAPITALHUMANO,
          escritura: this.permisosForm.value.ESCRITURACAPITALHUMANO,
        },
        bajaEmpleados: {
          activo: this.permisosForm.value.ACTIVOBAJAEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURABAJAEMPLEADOS,
        },
        altaDeIMSS: {
          activo: this.permisosForm.value.ACTIVOALTADEIMSS,
          escritura: this.permisosForm.value.ESCRITURAALTADEIMSS,
        },
        catalogos: {
          activo: this.permisosForm.value.ACTIVOCATALOGOS,
          escritura: this.permisosForm.value.ESCRITURACATALOGOS,
        },
        areas: {
          activo: this.permisosForm.value.ACTIVOAREAS,
          escritura: this.permisosForm.value.ESCRITURAAREAS,
        },
        banco: {
          activo: this.permisosForm.value.ACTIVOBANCO,
          escritura: this.permisosForm.value.ESCRITURABANCO,
        },
        bolsaDeTrabajo: {
          activo: this.permisosForm.value.ACTIVOBOLSADETRABAJO,
          escritura: this.permisosForm.value.ESCRITURABOLSADETRABAJO,
        },
        competencias: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIAS,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIAS,
        },
        competenciasCandidato: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIASCANDIDATO,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIASCANDIDATO,
        },
        empresa: {
          activo: this.permisosForm.value.ACTIVOEMPRESA,
          escritura: this.permisosForm.value.ESCRITURAEMPRESA,
        },
        escolaridad: {
          activo: this.permisosForm.value.ACTIVOESCOLARIDAD,
          escritura: this.permisosForm.value.ESCRITURAESCOLARIDAD,
        },
        estadoDeEscolaridad: {
          activo: this.permisosForm.value.ACTIVOESTADODEESCOLARIDAD,
          escritura: this.permisosForm.value.ESCRITURAESTADODEESCOLARIDAD,
        },
        estadoCivil: {
          activo: this.permisosForm.value.ACTIVOESTADOCIVIL,
          escritura: this.permisosForm.value.ESCRITURAESTADOCIVIL,
        },
        medioDeTransporte: {
          activo: this.permisosForm.value.ACTIVOMEDIODETRANSPORTE,
          escritura: this.permisosForm.value.ESCRITURAMEDIODETRANSPORTE,
        },
        motivosDeBaja: {
          activo: this.permisosForm.value.ACTIVOMOTIVOSDEBAJA,
          escritura: this.permisosForm.value.ESCRITURAMOTIVOSDEBAJA,
        },
        paises: {
          activo: this.permisosForm.value.ACTIVOPAISES,
          escritura: this.permisosForm.value.ESCRITURAPAISES,
        },
        puesto: {
          activo: this.permisosForm.value.ACTIVOPUESTO,
          escritura: this.permisosForm.value.ESCRITURAPUESTO,
        },
        tipoDePuesto: {
          activo: this.permisosForm.value.ACTIVOTIPODEPUESTO,
          escritura: this.permisosForm.value.ESCRITURATIPODEPUESTO,
        },
        razonSocial: {
          activo: this.permisosForm.value.ACTIVORAZONSOCIAL,
          escritura: this.permisosForm.value.ESCRITURARAZONSOCIAL,
        },
        turno: {
          activo: this.permisosForm.value.ACTIVOTURNO,
          escritura: this.permisosForm.value.ESCRITURATURNO,
        },
        recursosHumanos: {
          activo: this.permisosForm.value.ACTIVORECURSOSHUMANOS,
          escritura: this.permisosForm.value.ESCRITURARECURSOSHUMANOS,
        },
        reportesRH: {
          activo: this.permisosForm.value.ACTIVOREPORTESRH,
          escritura: this.permisosForm.value.ESCRITURAREPORTESRH,
        },
        reportePrecandidato: {
          activo: this.permisosForm.value.ACTIVOREPORTEPRECANDIDATO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEPRECANDIDATO,
        },
        reporteCandidato: {
          activo: this.permisosForm.value.ACTIVOREPORTECANDIDATO,
          escritura: this.permisosForm.value.ESCRITURAREPORTECANDIDATO,
        },
        reporteCapacitacion: {
          activo: this.permisosForm.value.ACTIVOREPORTECAPACITACION,
          escritura: this.permisosForm.value.ESCRITURAREPORTECAPACITACION,
        },
        reporteTeorico: {
          activo: this.permisosForm.value.ACTIVOREPORTETEORICO,
          escritura: this.permisosForm.value.ESCRITURAREPORTETEORICO,
        },
        reporteEmpleado: {
          activo: this.permisosForm.value.ACTIVOREPORTEEMPLEADO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEEMPLEADO,
        },
        reporteCapitalHumano: {
          activo: this.permisosForm.value.ACTIVOREPORTECAPITALHUMANO,
          escritura: this.permisosForm.value.ESCRITURAREPORTECAPITALHUMANO,
        },
        preguntas: {
          activo: this.permisosForm.value.ACTIVOPREGUNTAS,
          escritura: this.permisosForm.value.ESCRITURAPREGUNTAS,
        },
        generarPreguntas: {
          activo: this.permisosForm.value.ACTIVOGENERARPREGUNTAS,
          escritura: this.permisosForm.value.ESCRITURAGENERARPREGUNTAS,
        },
        generarRespuestas: {
          activo: this.permisosForm.value.ACTIVOGENERARRESPUESTAS,
          escritura: this.permisosForm.value.ESCRITURAGENERARRESPUESTAS,
        },
        resultados: {
          activo: this.permisosForm.value.ACTIVORESULTADOS,
          escritura: this.permisosForm.value.ESCRITURARESULTADOS,
        },
        // ti
        ti: {
          activo: this.permisosForm.value.ACTIVOTI,
          escritura: this.permisosForm.value.ESCRITURATI,
        },
        administracionTi: {
          activo: this.permisosForm.value.ACTIVOADMINISTRACIONTI,
          escritura: this.permisosForm.value.ESCRITURAADMINISTRACIONTI,
        },
        grupoPermisos: {
          activo: this.permisosForm.value.ACTIVOGRUPOPERMISOS,
          escritura: this.permisosForm.value.ESCRITURAGRUPOPERMISOS,
        },
        reactivarPermisos: {
          activo: this.permisosForm.value.ACTIVOREACTIVARPERMISOS,
          escritura: this.permisosForm.value.ESCRITURAREACTIVARPERMISOS,
        },
        usuariosTi: {
          activo: this.permisosForm.value.ACTIVOUSUARIOSTI,
          escritura: this.permisosForm.value.ESCRITURAUSUARIOSTI,
        },
        extensiones: {
          activo: this.permisosForm.value.ACTIVOEXTENSIONES,
          escritura: this.permisosForm.value.ESCRITURAEXTENSIONES,
        },
        asignaciones: {
          activo: this.permisosForm.value.ACTIVOASIGNACIONES,
          escritura: this.permisosForm.value.ESCRITURAASIGNACIONES,
        },
        usuarioAsignaciones: {
          activo: this.permisosForm.value.ACTIVOUSUARIOASIGNACIONES,
          escritura: this.permisosForm.value.ESCRITURAUSUARIOASIGNACIONES,
        },
        supervisor: {
          activo: this.permisosForm.value.ACTIVOSUPERVISOR,
          escritura: this.permisosForm.value.ESCRITURASUPERVISOR,
        },
        soporte: {
          activo: this.permisosForm.value.ACTIVOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURASOPORTE,
        },
        inventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOUSUARIO,
        },
        inventarioRedes: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOREDES,
        },
        inventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOSOPORTE,
        },
        bajasInventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOUSUARIO,
        },
        bajasInventarioRedes: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOREDES,
        },
        bajasInventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOSOPORTE,
        },
        catalogosSoporte: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSSOPORTE,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSSOPORTE,
        },
        estadoInventario: {
          activo: this.permisosForm.value.ACTIVOESTADODEINVENTARIO,
          escritura: this.permisosForm.value.ESCRITURAESTADODEINVENTARIO,
        },
        observacionesInventario: {
          activo: this.permisosForm.value.ACTIVOOBSERVACIONESDEINVENTARIO,
          escritura: this.permisosForm.value.ESCRITURAOBSERVACIONESDEINVENTARIO,
        },
        marcas: {
          activo: this.permisosForm.value.ACTIVOMARCAS,
          escritura: this.permisosForm.value.ESCRITURAMARCAS,
        },
        procesadores: {
          activo: this.permisosForm.value.ACTIVOPROCESADORES,
          escritura: this.permisosForm.value.ESCRITURAPROCESADORES,
        },
        reportesTI: {
          activo: this.permisosForm.value.ACTIVOREPORTESTI,
          escritura: this.permisosForm.value.ESCRITURAREPORTESTI,
        },
        reporteGrupoPermiso: {
          activo: this.permisosForm.value.ACTIVOREPORTEGRUPOPERMISO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEGRUPOPERMISO,
        },
        reporteUsuarios: {
          activo: this.permisosForm.value.ACTIVOREPORTEUSUARIOS,
          escritura: this.permisosForm.value.ESCRITURAREPORTEUSUARIOS,
        },
        reporteExtensiones: {
          activo: this.permisosForm.value.ACTIVOREPORTEEXTENSIONES,
          escritura: this.permisosForm.value.ESCRITURAREPORTEEXTENSIONES,
        },
        reporteSoporteTi: {
          activo: this.permisosForm.value.ACTIVOREPORTESOPORTETI,
          escritura: this.permisosForm.value.ESCRITURAREPORTESOPORTETI,
        },
        reporteInventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOUSUARIO,
        },
        reporteInventarioRedes: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOREDES,
        },
        reporteInventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOSOPORTE,
        },
        controlLlamadas: {
          activo: this.permisosForm.value.ACTIVOCONTROLLLAMADAS,
          escritura: this.permisosForm.value.ESCRITURACONTROLLLAMADAS,
        },
        // comercial
        comercial: {
          activo: this.permisosForm.value.ACTIVOCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURACOMERCIAL,
        },
        socios: {
          activo: this.permisosForm.value.ACTIVOSOCIOS,
          escritura: this.permisosForm.value.ESCRITURASOCIOS,
        },
        verSocios: {
          activo: this.permisosForm.value.ACTIVOVERSOCIOS,
          escritura: this.permisosForm.value.ESCRITURAVERSOCIOS,
        },
        contacto: {
          activo: this.permisosForm.value.ACTIVOCONTACTO,
          escritura: this.permisosForm.value.ESCRITURACONTACTO,
        },
        url: {
          activo: this.permisosForm.value.ACTIVOURL,
          escritura: this.permisosForm.value.ESCRITURAURL,
        },
        objetivoDeVentas: {
          activo: this.permisosForm.value.ACTIVOOBJETIVODEVENTAS,
          escritura: this.permisosForm.value.ESCRITURAOBJETIVODEVENTAS,
        },
        contrato: {
          activo: this.permisosForm.value.ACTIVOCONTRATO,
          escritura: this.permisosForm.value.ESCRITURACONTRATO,
        },
        ramo: {
          activo: this.permisosForm.value.ACTIVORAMO,
          escritura: this.permisosForm.value.ESCRITURARAMO,
        },
        subRamo: {
          activo: this.permisosForm.value.ACTIVOSUBRAMO,
          escritura: this.permisosForm.value.ESCRITURASUBRAMO,
        },
        verSubRamo: {
          activo: this.permisosForm.value.ACTIVOVERSUBRAMO,
          escritura: this.permisosForm.value.ESCRITURAVERSUBRAMO,
        },
        convenios: {
          activo: this.permisosForm.value.ACTIVOCONVENIOS,
          escritura: this.permisosForm.value.ESCRITURACONVENIOS,
        },
        producto: {
          activo: this.permisosForm.value.ACTIVOPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURAPRODUCTO,
        },
        verProducto: {
          activo: this.permisosForm.value.ACTIVOVERPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURAVERPRODUCTO,
        },
        competenciaProducto: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIAPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIAPRODUCTO,
        },
        analisisCompetencia: {
          activo: this.permisosForm.value.ACTIVOANALISISCOMPETENCIA,
          escritura: this.permisosForm.value.ESCRITURAANALISISCOMPETENCIA,
        },
        cobertura: {
          activo: this.permisosForm.value.ACTIVOCOBERTURA,
          escritura: this.permisosForm.value.ESCRITURACOBERTURA,
        },
        catalogosComercial: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSCOMERCIAL,
        },
        tipoGiro: {
          activo: this.permisosForm.value.ACTIVOTIPOGIRO,
          escritura: this.permisosForm.value.ESCRITURATIPOGIRO,
        },
        tipoCategoria: {
          activo: this.permisosForm.value.ACTIVOTIPOCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURATIPOCATEGORIA,
        },
        tipoCompetencia: {
          activo: this.permisosForm.value.ACTIVOTIPOCOMPETENCIA,
          escritura: this.permisosForm.value.ESCRITURATIPOCOMPETENCIA,
        },
        tipoProducto: {
          activo: this.permisosForm.value.ACTIVOTIPOPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURATIPOPRODUCTO,
        },
        divisas: {
          activo: this.permisosForm.value.ACTIVODIVISAS,
          escritura: this.permisosForm.value.ESCRITURADIVISAS,
        },
        categoria: {
          activo: this.permisosForm.value.ACTIVOCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURACATEGORIA,
        },
        subCategoria: {
          activo: this.permisosForm.value.ACTIVOSUBCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURASUBCATEGORIA,
        },
        tipoCobertura: {
          activo: this.permisosForm.value.ACTIVOTIPOCOBERTURA,
          escritura: this.permisosForm.value.ESCRITURATIPOCOBERTURA,
        },
        bajasSocios: {
          activo: this.permisosForm.value.ACTIVOBAJASSOCIOS,
          escritura: this.permisosForm.value.ESCRITURABAJASSOCIOS,
        },
        reportesComercial: {
          activo: this.permisosForm.value.ACTIVOREPORTESCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURAREPORTESCOMERCIAL,
        },
        reporteSocios: {
          activo: this.permisosForm.value.ACTIVOREPORTESOCIOS,
          escritura: this.permisosForm.value.ESCRITURAREPORTESOCIOS,
        },
        // marketing
        marketing: {
          activo: this.permisosForm.value.ACTIVOMARKETING,
          escritura: this.permisosForm.value.ESCRITURAMARKETING,
        },
        campaña: {
          activo: this.permisosForm.value.ACTIVOCAMPAÑA,
          escritura: this.permisosForm.value.ESCRITURACAMPAÑA,
        },
        verCampaña: {
          activo: this.permisosForm.value.ACTIVOVERCAMPAÑA,
          escritura: this.permisosForm.value.ESCRITURAVERCAMPAÑA,
        },
        medioDifusion: {
          activo: this.permisosForm.value.ACTIVOMEDIODIFUSION,
          escritura: this.permisosForm.value.ESCRITURAMEDIODIFUSION,
        },
        campañaCategoria: {
          activo: this.permisosForm.value.ACTIVOCAMPAÑACATEGORIA,
          escritura: this.permisosForm.value.ESCRITURACAMPAÑACATEGORIA,
        },
        paginas: {
          activo: this.permisosForm.value.ACTIVOPAGINAS,
          escritura: this.permisosForm.value.ESCRITURAPAGINAS,
        },
        catalogosMarketing: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSMARKETING,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSMARKETING,
        },
        tipoPaginas: {
          activo: this.permisosForm.value.ACTIVOTIPOPAGINAS,
          escritura: this.permisosForm.value.ESCRITURATIPOPAGINAS,
        },
        provedorLeads: {
          activo: this.permisosForm.value.ACTIVOPROVEDORLEADS,
          escritura: this.permisosForm.value.ESCRITURAPROVEDORLEADS,
        },
      },
    };

    console.table(this.permisosForm.value);
    const grupo = {
      idSubarea: this.permisosForm.value.idSubarea,
      idPuesto: this.permisosForm.value.idPuesto,
      idPermisosVisualizacion: this.permisosForm.value.idPermisosVisualizacion,
      nombre: this.permisosForm.value.nombre,
      descripcion: this.permisosForm.value.descripcion,
      permisos: JSON.stringify(this.permiso),
      activo: 1,
    };
    // console.log('e');
    //  console.log(grupo);
    this.grupoUsuariosService.post(grupo)
    .subscribe(data => {
      this.grupoUsuario = data ;
      this.grupoUsuariosService.postSocket({grupo: 'hehe'}).subscribe(x => {});
    });
    this.router.navigate(['/modulos/ti/permisos']);

  }
}
