import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {map} from 'rxjs/operators';
import {SaveComponent} from '../save/save.component';
import {Correo, CorreoData} from '../../../../@core/data/interfaces/ti/correo';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {
  LicenciasDisponibles,
  LicenciasDisponiblesData
} from "../../../../@core/data/interfaces/ti/licencias-disponibles";


@Component({
  selector: 'ngx-crear-coordinador',
  templateUrl: './crear-coordinador.component.html',
  styleUrls: ['./crear-coordinador.component.scss'],
})
export class CrearCoordinadorComponent implements OnInit {
  @Input() idUsuario: number;
  @Input() idSubarea: number;
  @Input() id: number;
  coordinadorForm: FormGroup;
  empleado: Empleados;
  usuario: Usuario;
  nombresupervisor: string;
  textoaviso: string;
  bandera: boolean;
  grupoUsuario: GrupoUsuarios[];
  grupUsuario: any[];
  extensiones: any[];
  extensionput: Extension;
  extensionput1: Extension;
  extension: Extension[];
  usuarios: Usuario[];
  usuario2: Usuario;
  usuario3: Usuario;
  correosdisponibles: any[];
  coordinad: RegExp = /^coordinador/;
  idCorreo: any[];
  idCorreoslista: any[];
  correoprimario: string;
  idCorreo2: string;
  datosUsuarios: object;
  extensioncoordinador: string;
  usuarioput: Usuario;
  usuarioput1: Usuario;
  datosUsuario: any;
  idUsuariox: number;
  banderaUsuarios: boolean;
  banderaUsuarios1: boolean;
  emailcoordinador: string;
  estadoCorreo2: boolean = false;
  estadoCorreo3: boolean;
  estadofinal: string;
  correo: Correo[];
  idCorreosupervisor: string;
  NuevoNombreCoordinador: string ;
  tipo: string = 'ejecutivo';
  licencias: LicenciasDisponibles[];
  correoAdministrativos: Correo[];
  banderalicencias: boolean;

  constructor(private fb: FormBuilder, private empleadosService: EmpleadosData,  private correoService: CorreoData,
              private usuarioService: UsuarioData, protected ref: NbDialogRef<CrearCoordinadorComponent>,
              private extensionesService: ExtensionData, private grupoUsuariosService: GrupoUsuariosData,
              public dialogService: NbDialogService,
              private licenciasService: LicenciasDisponiblesData, private toastrService: NbToastrService) {
    this.coordinadorForm = this.fb.group({
    'extensioncontrol': new FormControl(),
    'privilegiocontrol': new FormControl(),
    'correosdisponibleSubareas': new FormControl(),
    'emailmodificado': new FormControl(),
  });
  }
  getEmple() {
    this.empleadosService.getEmpleadoById(this.id).subscribe(data => {
      this.empleado = data;

    });
  }
  getUsuarios() {
    this.usuarioService.getUsuarioById(this.idUsuario).subscribe(data => {
      this.usuario = data;
      this.nombresupervisor = this.usuario.usuario.substring(0, 11) ;
      if (this.nombresupervisor === 'coordinador') {
        this.textoaviso = 'Ya es coordinador';
      }else if (this.nombresupervisor !== 'coordinador') {
        this.bandera = true; }
    });

  }
  getExtensiones() {/** en un futuro definir el subarea de las extensiones de los supervisores*/
  this.extensionesService.getExtensionByIdSubarea(10, 0).pipe(map(data => this.extension = data)).subscribe(data => {
    this.extensiones = [];
    for (let i = 0; i < this.extension.length; i++) {
      this.extensiones.push({'label': this.extension[i].id, 'value': this.extension[i].id});
    }
  });
  }

  getextension(event) {
    if (event) {
      this.extensioncoordinador = event;
    }
  }
  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(data => this.grupoUsuario = data)).subscribe(data => {
      this.grupUsuario = [];
      for (let i = 0; i < this.grupoUsuario.length; i++) {
        this.grupUsuario.push({'label': this.grupoUsuario[i].nombre, 'value': this.grupoUsuario[i].id});
      }
    });
  }
  /** metodo que obtiene todos los usuarios que comience sus correos disponibles*/
  getcorreosdisponiblesarea() {
    this.banderaUsuarios = false;
    this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
      return result.filter(data => data.usuario.match(/ahorraseguros.mx/) || data.usuario.match(/ahorracredit.com/)
        || data.usuario.match(/mejorsegurodeauto.mx/) || data.usuario.match(/mejorsegurodevida.mx/)
        || data.usuario.match(/qualitassegurosautos.com/) || data.usuario.match(/inprove.mx/)
        || data.usuario.match(/segurosmotos.com.mx/) || data.usuario.match(/seguroselpotosi.mx/)
        || data.usuario.match(/segurosdetaxi.com.mx/) || data.usuario.match(/multivaseguros.com/)
        || data.usuario.match(/lalatinoseguros./) || data.usuario.match(/gnpsegurosautos.com/)
        || data.usuario.match(/comparaya.mx/));
    })).subscribe(data => {
      this.correosdisponibles = [];
      this.idCorreoslista = [];
      this.idCorreo = [];
      this.usuarios = data;
      for (let i = 0; i < this.usuarios.length; i++) {
        this.correosdisponibles.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].usuario});
        this.idCorreoslista.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].idCorreo});
        this.idCorreo.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].id});

      }
      /** si no hay usuarios cambia la banderaUsuarios a true para que no muestre el p-dropdown donde se muestra los correos
       * disponibles
       */
      if (this.usuarios.length === 0) {
        this.banderaUsuarios = true;
        this.licenciasService.get().subscribe(data2 => {
          this.licencias = data2;
          this.correoService.get('ejecutivo').subscribe(data3 => {
            this.correoAdministrativos = data3;
            if (this.correoAdministrativos.length === this.licencias[0].cantidad ||
              this.correoAdministrativos.length >= this.licencias[0].cantidad) {
              // checa licencias
              this.banderalicencias = true;
              console.log(this.banderalicencias);
            } else if (this.correoAdministrativos.length <= this.licencias[0].cantidad) {
              // licencias disponibles
              this.banderalicencias = false;
              console.log(this.banderalicencias);
            }
          });
        });
        this.getnuevocorreoCoordinador();
      }
    });
  }

  getnuevocorreoCoordinador() {
    this.emailcoordinador =  'coordinadordeventanueva@ahorraseguros.mx';
    this.correoService.validarCorreo(this.tipo, this.emailcoordinador).subscribe(response => {
      this.estadoCorreo2 = response;
      if (this.estadoCorreo2 === true) {
        this.coordinadorForm.controls['emailmodificado'].setValue(this.emailcoordinador);
      }
    });

  }
  /** obtiene el nombre del correo por medio del input y ve si esta disponible o no*/
  getcorreofinal(event) {
    if (event) {
      this.estadoCorreo3 = false;

      this.correoService.validarCorreo(this.tipo, event).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.estadofinal = 'correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.emailcoordinador = event;
          this.estadofinal = 'correo disponible';
          if (this.banderaUsuarios1 === false) {
            this.correoprimario = event;
          }
          if (this.banderaUsuarios1 === true) {
            this.NuevoNombreCoordinador = event;
          }
        }
      });
    }
  }
  /** selecciona el correo principal, si el correo principal empieza con supervisor lo da a correo primario de lo
   * contrario manda a crear nuevo supervisor*/
  getcorreoprincialSubareas(event) {
    if (event) {
      this.correoprimario = event;
      if (this.correoprimario.match(this.coordinad)) {
        this.estadoCorreo3 = false;
        this.banderaUsuarios1 = false;
        this.correoprimario = event;
      }else {
        this.banderaUsuarios = true;
        this.banderaUsuarios1 = true;
        this.getnuevocorreoCoordinador();
      }
    }
  }
  guardarUsuario() {
    /**Obtiene el idCorreo del correo principal del api*/
    for (let i = 0; i < this.idCorreoslista.length; i++) {
      console.table(this.idCorreoslista);
      if (this.correoprimario === this.idCorreoslista[i].label) {
        this.idCorreo2 = this.idCorreoslista[i].value;
      }
    }
    /** saca el id del usuario del correo primario*/
    for (let i = 0; i < this.idCorreo.length; i++) {
      if (this.correoprimario === this.idCorreo[i].label) {
        this.idUsuariox = this.idCorreo[i].value;
      }
    }
    if (this.banderaUsuarios1 === false) {
      const objeto = {
        'id': this.idCorreo2, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.correoprimario, // nombre del correo
      };

      /*this.correoService.enviarByAlias(objeto).subscribe(response => {   /// crear ejecutivo supervisor pendiente
        this.estadofindalias = response;
      });*/

      /** busca el usuario por el id y remplaza los datos con el nuevo supervisor - obtiene la misma extension, cambian
       * los datos del id empleado y extension de datosUsuarios*/
      this.usuarioService.getUsuarioById(this.idUsuariox).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
        this.datosUsuarios = {
          'idCampania': this.idSubarea, 'idEmpleado': this.empleado.id, 'idExtension': this.datosUsuario.idExtension,
          'idProtocolo': 1, 'idPerfilMarcacion': 1,
        };
        this.usuarioput1 = {
          'usuario': this.usuarioput.usuario,
          'password': this.usuarioput.password,
          'idCorreo': this.usuarioput.idCorreo,
          'idTipo': this.usuarioput.idTipo, 'idSubarea': this.empleado.idSubarea,
          'datosUsuario': JSON.stringify(this.datosUsuarios),
          'estado': 1,
        };
        /*this.usuarioService.put(this.IdUsuariox, this.usuarioput1).subscribe(result => {});*/
        /** extension de usuario empleado cambia a ocupado */
        this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
          this.extensionput = dataextensiones;
          /*this.extensionput1 = {
            'idSubarea' : this.extensionput.idSubarea,
            'descrip' : this.extensionput.descrip,
            'idEstado' : 1,
          };*/
          /* this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {});*/
        });
      });
    }
    if (this.banderaUsuarios1 === true) {
      const objeto = {
        'id': this.idCorreo2, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.NuevoNombreCoordinador, // nombre del correo
      };

      /*this.correoService.enviarByAlias(objeto).subscribe(response => {   /// crear ejecutivo supervisor pendiente
        this.estadofindalias = response;
      });*/
      /** busca el usuario por el id y remplaza los datos con el nuevo supervisor - obtiene la misma extension, cambian
       * los datos del id empleado y extension de datosUsuarios*/
      this.usuarioService.getUsuarioById(this.idUsuariox).subscribe(datausuarios => {
        this.usuarioput = datausuarios;
        this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
        this.datosUsuarios = {
          'idCampania': this.idSubarea, 'idEmpleado': this.empleado.id, 'idExtension': this.coordinadorForm.value.extensioncontrol,
          'idProtocolo': 1, 'idPerfilMarcacion': 1,
        };
        this.usuarioput1 = {
          'usuario': this.NuevoNombreCoordinador,
          'password': this.usuarioput.password,
          'idCorreo': this.usuarioput.idCorreo,
          'idTipo': this.usuarioput.idTipo, 'idSubarea': this.empleado.idSubarea,
          'datosUsuario': JSON.stringify(this.datosUsuarios),
          'estado': 1,

        };
        /*this.usuarioService.put(this.IdUsuariox, this.usuarioput1).subscribe(result => {});*/
        /** extension de usuario empleado cambia a ocupado */
        this.extensionesService.get(this.coordinadorForm.value.extensioncontrol).subscribe(dataextensiones => {
          this.extensionput = dataextensiones;
          /*this.extensionput1 = {
            'idSubarea' : this.extensionput.idSubarea,
            'descrip' : this.extensionput.descrip,
            'idEstado' : 1,
          };*/
          /* this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {});*/
        });
      });
    }


    // })
  }
  guardarNuevoCoordinador() {
    if (this.banderalicencias === true) {
      console.log('entro');
      this.showToast2('danger');
    }else if (this.banderalicencias === false) {
      const objeto = {
        // 'id': this.idCorreo2, // id del correoprimario
        'givenName': this.empleado.nombre,
        'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
        'email': this.emailcoordinador, // nombre del correo ya disponible
      };

      /*this.correoService.post(objeto).subscribe(response => {   /// crear ejecutivo supervisor pendiente
        this.estadofindalias = response;
      });*/

      // una ves guardado el correo en el api, busca su id y lo manda a la tabla usuarios
      this.correoService.get('ejecutivo').subscribe(data => {
        this.correo = data;
        this.idCorreoslista = [];
        for (let i = 0; i < this.correo.length; i++) {
          this.idCorreoslista.push({'label': this.correo[i].email, 'value': this.correo[i].id});
        }
      });
      // obtiene el id del correo
      for (let i = 0; i < this.idCorreoslista.length; i++) {

        if (this.emailcoordinador === this.idCorreoslista[i].label) {
          this.idCorreosupervisor = this.idCorreoslista[i].value;
        }
      }
      this.datosUsuarios = {
        'idCampania': this.idSubarea, 'idEmpleado': this.empleado.id, 'idExtension': this.extensioncoordinador,
        'idProtocolo': 1, 'idPerfilMarcacion': 1,
      };
      this.usuario2 = {
        'usuario': this.emailcoordinador, 'password': 'mark-43',
        'idCorreo': this.idCorreosupervisor, 'idTipo': 3, 'idSubarea': this.idSubarea,
        'datosUsuario': JSON.stringify(this.datosUsuarios), 'estado': 1,

      };
      // guarda en la tabla usuario y modifica el idUsuario de la tabla empleado
      this.usuarioService.postUsuariosidEmpleados(this.usuario2, this.empleado.id).subscribe(data => {
        this.usuario3 = data;
        this.coordinadorForm.reset();
        // si guardo bien modifica al extension a ocupado
        if (this.usuario3 !== null) {
          // cambia el estado a 1 a la extencion dada
          this.extensionesService.get(this.extensioncoordinador).subscribe(dataextensiones => {
            this.extensionput = dataextensiones;
            this.extensionput1 = {
              'idSubarea': this.extensionput.idSubarea,
              'descrip': this.extensionput.descrip,
              'idEstado': 1,
            };
            this.extensionesService.put(this.extensioncoordinador, this.extensionput1).subscribe(result => {
            });
          });

          this.dialogService.open(SaveComponent, {});
        }
      });
    }
  }
  guardar() {
    if (this.correosdisponibles.length !== 0) {
      this.guardarUsuario();
    }
    if (this.correosdisponibles.length === 0) {
      this.guardarNuevoCoordinador();

    }
  }
  showToast2(status) {
    this.toastrService.danger('Atencion', `Licencias de correos ya no disponibles`);
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getEmple();
    this.getcorreosdisponiblesarea();
    this.getExtensiones();
    this.getUsuarios();
    this.getGrupoUsuarios();
  }

}
