import {Observable} from 'rxjs';

export interface CompetenciasCandidato {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class CompetenciaCandidatoData {
  abstract get(): Observable<CompetenciasCandidato[]>;
  abstract post(competenciasCandidato: CompetenciasCandidato): Observable<CompetenciasCandidato>;
  abstract postSocket(json): Observable<JSON>;
  abstract getCompetenciaCandidatoById(idCompetenciasCandidato): Observable<CompetenciasCandidato>;
  abstract put(idCompetenciasCandidato, competenciaCandidato: CompetenciasCandidato): Observable<CompetenciasCandidato>;
}
