import {Observable} from 'rxjs';
export interface Switchs {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class SwitchsData {
  abstract get(): Observable<Switchs[]>;
  abstract post(cpu): Observable<Switchs>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getSwitchsById(idCpu): Observable<Switchs>;
  abstract put(idCpu, cpu): Observable<Switchs>;
}
