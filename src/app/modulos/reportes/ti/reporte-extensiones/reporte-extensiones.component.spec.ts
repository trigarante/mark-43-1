import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteExtensionesComponent } from './reporte-extensiones.component';

describe('ReporteExtensionesComponent', () => {
  let component: ReporteExtensionesComponent;
  let fixture: ComponentFixture<ReporteExtensionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteExtensionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteExtensionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
