import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {
  EstadoPolizaVn,
  EstadoPolizaVnData,
} from '../../../../../@core/data/interfaces/ventaNueva/catalogos/estado-poliza-vn';
import {EstadoPolizaComponent} from '../../estado-poliza/estado-poliza.component';

@Component({
  selector: 'ngx-estado-poliza-create',
  templateUrl: './estado-poliza-create.component.html',
  styleUrls: ['./estado-poliza-create.component.scss'],
})
export class EstadoPolizaCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idEstado obtenido. */
  @Input() idEstado: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  EstadoCreateform: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  Estado: EstadoPolizaVn;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;


  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BancoComponent>} ref Referencia al diálogo (mensaje).
   * @param {TipoEstadoVnData} EstadoService Servicio del componente banco para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<EstadoPolizaComponent>, private EstadoService: EstadoPolizaVnData,
              private router: Router) { }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoEstado() {
    if (this.EstadoCreateform.invalid) {
      return;
    }
    this.submitted = true;
    this.EstadoService.post(this.EstadoCreateform.value).subscribe((result) => {
      this.EstadoService.postSocket(this.EstadoCreateform.value).subscribe(x => {});
      this.EstadoCreateform.reset();
      this.router.navigate(['/modulos/venta-nueva/catalogos/estadopoliza']);
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un Tipo Estado. */
  updateTipoEstado() {
    this.EstadoService.put(this.idEstado, this.EstadoCreateform.value).subscribe( result => {
      this.EstadoService.postSocket(this.EstadoCreateform.value).subscribe(x => {});
      this.EstadoCreateform.reset();
      this.ref.close();
      // window.location.reload();
    });
  }


  /** Función que obtiene la información de un Estado dependiendo de su ID. */
  getEstadoById() {
    this.EstadoService.getEstadoPolizaById(this.idEstado).subscribe(data => {
      this.Estado = data;
      this.EstadoCreateform.controls['estado'].setValue(this.Estado.estado);
      this.EstadoCreateform.controls['activo'].setValue(this.Estado.activo);
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getEstadoById();
    }
    this.EstadoCreateform = this.fb.group( {
      'estado': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }
}
