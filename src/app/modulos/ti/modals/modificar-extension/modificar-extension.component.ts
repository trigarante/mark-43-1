import {Component, Input, OnChanges, OnInit} from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {map} from 'rxjs/operators';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {CorreoData} from '../../../../@core/data/interfaces/ti/correo';

@Component({
  selector: 'ngx-modificar-extension',
  templateUrl: './modificar-extension.component.html',
  styleUrls: ['./modificar-extension.component.scss'],
})

export class ModificarExtensionComponent implements OnInit {
  @Input() idUsuario: string;
  usuario: Usuario = null;
  tipoUsuario: string;
  datosUsuario: any;
  extensionActual: number = null;
  extensiones: any[] = [];
  datosExtensionActual: Extension;
  datosExtensionAGuardar: Extension;
  extensionAGuardar: number;
  activarInputCorreo: boolean = false;
  desactivarDropDown: boolean = true;
  activarBoton: boolean = false;
  ocultarDesvincularExtension: boolean = true;
  dominiosDisponibles: any[] = [];
  correoActual: string;
  dominioSeleccionado: string;
  nuevoCorreo: string;
  correoExistente: boolean;
  /*usuario: usuario;
  idCorreo: idCorreo;*/

  constructor(private dialogoService: NbDialogService, protected ref: NbDialogRef<ModificarExtensionComponent>,
              private usuarioService: UsuarioData, private extensionesService: ExtensionData, private correoService: CorreoData) { }

  getUsuario(id) {
    this.usuarioService.getUsuarioById(id).subscribe(
      data => {
        this.usuario = data;
        this.datosUsuario = JSON.parse(data.datosUsuario);
        this.tipoUsuario = data.tipoUsuario;
        if (this.tipoUsuario !== 'ejecutivo') {
          this.ocultarDesvincularExtension = false;
        }
        this.extensionActual = this.datosUsuario.idExtension;
      },
      error => {},
      () => {
        this.getExtensionesDisponibles();
        if (this.extensionActual !== undefined) {
            this.getDatosExtension(this.extensionActual, 'actual');
        } else {
          this.ocultarDesvincularExtension = false;
        }
        if (this.tipoUsuario === 'ejecutivo') {
          this.getCorreo();
          this.getDominiosDisponibles();
        }
      },
    );
  }

  /**  Función para obtener el las extensiones disponibles, dichos valores se almacenan en el arreglo "extensiones".*/
  getExtensionesDisponibles() {
    let extension: Extension[];

    this.extensionesService.getExtensionByIdSubarea(this.usuario.idSubarea, 0).pipe(map(data => extension = data)).subscribe( data => {
      if (extension.length > 0) {
        for (let i = 0; i < extension.length; i++) {
          this.extensiones.push({'label': extension[i].id, 'value': extension[i].id});
        }
        this.desactivarDropDown = false;
      }
    });
  }

  getDatosExtension(idExtension: number, tipo: string) {

   this.extensionesService.get(idExtension).subscribe( data => {
     if (tipo === 'actual')
       this.datosExtensionActual = data;
     else
       this.datosExtensionAGuardar = data;
    });
  }

  getDominiosDisponibles() {
    this.correoService.getDominios('ejecutivo').subscribe(data => {
      for (const dominio of data) {
        this.dominiosDisponibles.push({'value': dominio.domainName});
      }
    }, error => {
      this.getDominiosDisponibles();
    });
  }

  getCorreo() {
    this.correoService.getById('ejecutivo', this.usuario.idCorreo).subscribe(data => {
      this.correoActual = data.primaryEmail;
    }, error => {
      this.getCorreo();
    });
  }

  ngOnInit() {
    this.getUsuario(this.idUsuario);
  }

  extensionSeleccionada(extension: number) {
    this.activarBoton = true;
    this.extensionAGuardar = extension;
    this.getDatosExtension(extension, 'guardar');
  }

  cambiaEstadoBotonGuardar() {
    if (this.nuevoCorreo === undefined) {
      this.activarBoton = !this.activarBoton;
    } else {
      this.nuevoCorreo = undefined;
      this.correoExistente = undefined;
    }
  }

  verificaExistenciaCorreo(dominio?: string) {
    let respuesta: boolean;

    if (dominio !== undefined) {
      this.dominioSeleccionado = dominio;
    }
    if (this.dominioSeleccionado !== undefined && this.nuevoCorreo !== undefined && this.nuevoCorreo !== '') {
      this.correoService.validarCorreo('ejecutivo', this.nuevoCorreo + '@' + this.dominioSeleccionado).subscribe( data => {
        respuesta = data;
      }, error => {},
        () => {
          this.correoExistente = respuesta;
          if (respuesta === false) {
            this.activarBoton = true;
          } else {
            this.activarBoton = false;
          }
        });
    }
  }

  modificarExtension() {
    const extension = {
      idEstado: 1,
      idSubarea: this.datosExtensionAGuardar.idSubarea,
      descrip: this.datosExtensionAGuardar.descrip,
    };

    // Si el correo se tiene que modificar entra en este IF y cambia su alias en Google
    if (this.activarInputCorreo) {
      const correo = {
        id: this.usuario.idCorreo,
        email: this.nuevoCorreo + '@' + this.dominioSeleccionado,
      };
      this.correoService.enviarByAlias('ejecutivo', correo).subscribe();
    }

    // Identifica si el usuario ya tiene extension asignada y la desasigna para que esté disponible
    if (this.extensionActual !== undefined) {
      const extensionVieja = {
        idEstado: 0,
        idSubarea: this.datosExtensionActual.idSubarea,
        descrip: this.datosExtensionActual.descrip,
      };

      this.extensionesService.put(this.extensionActual, extensionVieja).subscribe(result => {});
    }

    // Asigna la nueva extensión al usuario
    this.extensionesService.put(this.extensionAGuardar, extension).subscribe(
      result => {},
      error => {
        swal('La extensión no se ha guardado, contacte al departamento de TI', {
          icon: 'error',
        });
      },
      () => {
        const nuevosDatosUsuario = {
          idCampania: this.usuario.idSubarea,
          idExtension: this.extensionAGuardar,
          idProtocolo: 1,
          idPerfilMarcacion: 1,
        };
        this.usuario.datosUsuario = JSON.stringify(nuevosDatosUsuario);
        // Cambia el correo del usuario en la base de datos de mark44 si es necesario
        if (this.activarInputCorreo) {
          this.usuario.usuario = this.nuevoCorreo + '@' + this.dominioSeleccionado;
        }
        this.usuarioService.putUsuario(this.idUsuario, this.usuario).subscribe(
          result => {},
          error => {
            swal('La extensión no se ha guardado, contacte al departamento de TI', {
              icon: 'error',
            });
          },
          () => {
            swal('¡La extension se ha guardado exitosamente!', {
              icon: 'success',
            }).then((resultado => {
              this.ref.close();
            }));
          },
        );
      });
  }

  desvincularExtension() {
    swal({
      title: '¿Realmente deseas desvincular la extension ' + this.extensionActual + '?',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        const extension = {
          idEstado: 0,
          idSubarea: this.datosExtensionActual.idSubarea,
          descrip: this.datosExtensionActual.descrip,
        };
        this.extensionesService.put(this.extensionActual, extension).subscribe(
          result => {},
          error => {
            swal('La extensión no se ha desvinculado, contacte al departamento de TI', {
              icon: 'error',
            });
          },
          () => {
            const nuevosDatosUsuario = {
              idCampania: this.datosUsuario.idCampania,
            };
            this.usuario.datosUsuario = JSON.stringify(nuevosDatosUsuario);
            this.usuarioService.putUsuario(this.idUsuario, this.usuario).subscribe(
              result => {},
              error => {
                swal('La extensión no se ha desvinculado del usuario, contacte al departamento de TI', {
                  icon: 'error',
                });
              },
                () => {
                  swal('¡La extension se ha desvinculado exitosamente!', {
                    icon: 'success',
                  }).then((resultado => {
                    this.ref.close();
                  }));
                },
            );
          });
      }
    });
  }

  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
}
