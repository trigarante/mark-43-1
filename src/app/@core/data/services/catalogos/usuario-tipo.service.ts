import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TipoUsuario, TipoUsuarioData} from '../../interfaces/catalogos/tipo-usuario';

@Injectable()
export class UsuarioTipoService extends TipoUsuarioData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<TipoUsuario[]> {
    return this.http.get<TipoUsuario[]>(this.baseURL + 'v1/tipo-usuarios');
  }

  post(usuarioTipo: TipoUsuario): Observable<TipoUsuario> {
    return this.http.post<TipoUsuario>(this.baseURL + 'v1/tipo-usuarios', usuarioTipo);
  }

  getPuestoTipoById(idUsuarioTipo): Observable<TipoUsuario> {
    return this.http.get<TipoUsuario>(this.baseURL + 'v1/tipo-usuarios/' + idUsuarioTipo);
  }

  put(idUsuarioTipo, usuarioTipo: TipoUsuario): Observable<TipoUsuario> {
    return this.http.put< TipoUsuario>(this.baseURL + 'v1/tipo-usuarios/' + idUsuarioTipo, usuarioTipo);
  }
}
