/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  apiUrl: 'https://www.pruebas.mark-43.net/mark43-service/',
  CATALOGO_AUTOS: 'https://ahorraseguros.mx/ws-autos/servicios',
  GLOBAL_SERVICIOS: 'https://www.pruebas.mark-43.net/mark43-service/',
  GLOBAL_SOCKET: 'https://www.pruebas.mark-43.net:444/',
};
