import {Component, OnInit, ViewChild} from '@angular/core';
import {Vacante, VacanteData} from '../../../@core/data/interfaces/catalogos/vacante';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {VacantesCreateComponent} from '../modals/vacantes-create/vacantes-create.component';
import {map} from 'rxjs/operators';
import {GenerarUrlComponent} from '../modals/generar-url/generar-url.component';
import swal from 'sweetalert';
import {Sede, SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {SolicitudCreateComponent} from '../modals/solicitud-create/solicitud-create.component';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTableDataSource} from '@angular/material';
import {DatosVacantesComponent} from '../modals/datos-vacantes/datos-vacantes.component';
import {FormControl} from '@angular/forms';
import {TooltipPosition} from '@angular/material';
import {ViewEncapsulation} from '@angular/core';
import {environment} from '../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-vacantes',
  templateUrl: './vacantes.component.html',
  styleUrls: ['./vacantes.component.scss'],
  encapsulation: ViewEncapsulation.None,
})


export class VacantesComponent extends MatPaginatorIntl implements OnInit  {
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  vacantes: Vacante[];
  vacante: any;
  vacanted: any[];
  dataSource: any;
  permisos: any;
  activo: any;
  loading: boolean;
  escritura: boolean;
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {VacanteData} vacantesService Servicio del componente vacante para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes (diálogos). */
  constructor(
    private vacantesService: VacanteData,
    private dialogoService: NbDialogService,
    private sedesService: SedeData,
    private toastrService: NbToastrService,
    ) {
    super();
    this.loading = true;
    this.getAndInitTranslations();
  }

  ngOnInit() {
    this.connect();
    this.getVacante(0);
    this.cols = [
      'detalle',
      'nombre',
      'subarea',
      'sede',
      'puesto',
      'acciones',
    ];
    this.getPermisos();
  }

  // WEB SOCKET
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vacantes');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame){
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panel/1', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getVacante(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getAndInitTranslations() {
    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }
  getRangeLabel = (page: number, pageSize: number, length: number) =>  {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }



  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Función que obtiene las vacantes disponibles y las asigna al arreglo vacantes. */
  getVacante(mensajeAct) {
    this.vacantesService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(valor => {
      this.vacantes = [];
      this.vacantes = valor;
      this.dataSource = new MatTableDataSource(this.vacantes); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    }, () => {
        swal({
          icon: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
    },
    () => {
      if (mensajeAct === 1) {
        this.toastrService.success('!!Datos Actualizados¡¡', `Vacantes`);
      }
    });
  }

  /** Función que permite visualizar un modal para poder generar una URL con base en la selección de la bolsa de trabajo y la vacante. */
  generarUrl(idVacante) {
    this.dialogoService.open(GenerarUrlComponent, {
      context: {
        idVacante: idVacante,
      },
    });
  }

  crearSolicitud(idVacante) {
    this.dialogoService.open(SolicitudCreateComponent, {
      context: {
        idTipo: 2,
        idVacante: idVacante,
      },
    });
  }

  verDatos(idVacante) {
    this.dialogoService.open(DatosVacantesComponent, {
      context: {
        idVacante: idVacante,
      },
    });
  }
  /** Función que despliega un modal del componente "VacantesCreateComponent" para poder "crear una vacante". */
  crearVacante() {
    this.dialogoService.open(VacantesCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(y => {
      this.getVacante(0);
    });
  }
  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updateVacante(idVacante) {
    this.dialogoService.open(VacantesCreateComponent, {
      context: {
        idTipo: 2,
        idVacante: idVacante,
      },
    }).onClose.subscribe(x => {
      this.getVacante(1);
    });
  }

  getVacanteById(idVacante) {
    this.vacantesService.getVacantesById(idVacante).subscribe(data => {
      this.vacante = data;
    });
  }
  /** Función que despliega un modal para dar de baja una vacante. */
  bajaVacante(idVacante) {

    this.getVacanteById(idVacante);

    swal({
      title: '¿Deseas dar de baja esta Vacante?',
      text: 'Una vez dado de baja, no podrás ver la vacante de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.vacante['activo'] = 0;
        // console.log(this.vacante['id']);
        this.vacantesService.putBaja(idVacante).subscribe(result => {
        });
        swal('¡La Vacante se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.vacantesService.postSocket({idVacante: idVacante, vacante: this.vacante})
          .subscribe(data => {});
          this.getVacante(0);
        }));
      }
    });

  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.vacantes.escritura;
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
