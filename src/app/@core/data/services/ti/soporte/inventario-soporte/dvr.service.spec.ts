import { TestBed } from '@angular/core/testing';

import { DvrService } from './dvr.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

describe('DvrService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'BUENO',
    idEstadoInventario: 1,
    idObservacionesInventario: 1,
    folio: '2223',
    idMarca: 2,
    fechaRecepcion: '2019-07-26T21:51:18.000+0000',
    fechaSalida: null,
    activo: 1,
    usuario: null,
    nombreMarca: 'HP',
    origenRecurso: 'renta',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'Dvr',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [DvrService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/Dvr');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/Dvr', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/Dvr');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/Dvr', '/1/74');
      });
    });
  }
});
