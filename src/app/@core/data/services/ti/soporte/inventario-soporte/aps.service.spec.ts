import { TestBed } from '@angular/core/testing';

import { ApsService } from './aps.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('ApsService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 3,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'DESCOMPUESTO',
    idEstadoInventario: 3,
    idObservacionesInventario: 1,
    folio: 'APS---',
    idMarca: 1,
    fechaRecepcion: '2019-09-12T16:17:31.000+0000',
    fechaSalida: null,
    activo: 0,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    origenRecurso: 'propio',
    idSede: 1,
  },
    {
      id: 'aps',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ApsService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/aps');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/aps', '/3');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/aps');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/aps', '/3/74');
      });
    });
  }
});
