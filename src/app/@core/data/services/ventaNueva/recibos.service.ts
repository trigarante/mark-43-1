import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Recibos, RecibosData} from '../../interfaces/ventaNueva/recibos';

@Injectable()
export class RecibosService extends RecibosData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Recibos[]> {
    return this.http.get<Recibos[]>(this.baseURL + 'v1/recibovn');
  }

  post(recibo: Recibos): Observable<Recibos> {
    return this.http.post<Recibos>(this.baseURL + 'v1/recibovn/', recibo);
  }

  getReciboById(idRecibo): Observable<Recibos> {
    return this.http.get<Recibos>(this.baseURL + 'v1/recibovn/' + idRecibo);
  }

  put(idRecibo, recibo: Recibos): Observable<Recibos> {
    return this.http.put<Recibos>(this.baseURL + 'v1/recibovn/' + idRecibo, recibo);
  }
}
