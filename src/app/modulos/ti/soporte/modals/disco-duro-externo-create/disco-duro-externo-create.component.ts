import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {
  DiscoDuroExterno,
  DiscoDuroExternoData,
} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';


@Component({
  selector: 'ngx-disco-duro-externo-create',
  templateUrl: './disco-duro-externo-create.component.html',
  styleUrls: ['./disco-duro-externo-create.component.scss'],
})
export class DiscoDuroExternoCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idhdde: number;
  discoduroexterno: DiscoDuroExterno;
  discoduroexternoCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<DiscoDuroExternoCreateComponent>,
              private discoDuroExternoService: DiscoDuroExternoData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private router: Router, private sedesService: TipoSedeData) { }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }


  guardarDiscoDuroExterno() {
    if (this.discoduroexternoCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.discoDuroExternoService.post(this.discoduroexternoCreateForm.value).subscribe((result) => {
      this.discoDuroExternoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.discoduroexternoCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'discoduroexterno']);
    });
  }

  updateDiscoDuroExterno() {
    this.discoDuroExternoService.put(this.idhdde, this.discoduroexternoCreateForm.value).subscribe(result => {
      this.discoDuroExternoService.postSocket({status: 'ok'}).subscribe(x => {});
      this.discoduroexternoCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'discoduroexterno']);
    });
  }
  getDiscoDuroExternoById() {
    this.discoDuroExternoService.getDiscoExternoById(this.idhdde).subscribe(data => {
      this.discoduroexterno = data;
      this.discoduroexternoCreateForm.controls['folio'].setValue(this.discoduroexterno.folio);
      this.discoduroexternoCreateForm.controls['idMarca'].setValue(this.discoduroexterno.idMarca);
      this.discoduroexternoCreateForm.controls['idEstadoInventario'].setValue(this.discoduroexterno.idEstadoInventario);
      this.discoduroexternoCreateForm.controls['idObservacionesInventario'].setValue(this.discoduroexterno.idObservacionesInventario);
      this.discoduroexternoCreateForm.controls['idEmpleado'].setValue(this.discoduroexterno.idEmpleado);
      this.discoduroexternoCreateForm.controls['idTipoSede'].setValue(this.discoduroexterno.idTipoSede);
      this.discoduroexternoCreateForm.controls['origenRecurso'].setValue(this.discoduroexterno.origenRecurso);

    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getDiscoDuroExternoById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.discoduroexternoCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
