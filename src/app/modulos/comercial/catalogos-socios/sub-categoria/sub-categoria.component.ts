import {Component, OnInit, ViewChild} from '@angular/core';
import {SubCategoria, SubCategoriaData} from '../../../../@core/data/interfaces/comerciales/catalogo/sub-categoria';
import {map} from 'rxjs/operators';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {SubcategoriaCreateComponent} from '../modals/subcategoria-create/subcategoria-create.component';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-sub-categoria',
  templateUrl: './sub-categoria.component.html',
  styleUrls: ['./sub-categoria.component.scss'],
})
export class SubCategoriaComponent implements OnInit {
subCategioria: SubCategoria[];
  cols: any[];
  dataSource: any;
  subCategoria: SubCategoria;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // socket
  private stompClient = null;
  constructor(private subCategoriaServive: SubCategoriaData, private dialogService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) { }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'subcategoria');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelSubCategoria', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this. getSubCategoria(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Sub Categorías`);
  }
  getSubCategoria(mensajeAct) {
    this.subCategoriaServive.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      // console.log(data);
      this.subCategioria = data;
      this.dataSource = new MatTableDataSource(this.subCategioria);
      this.dataSource.paginator = this.paginator;
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  getSubCategoriaById(idSubCategoria) {
    this.subCategoriaServive.getSubCategoriaById(idSubCategoria).subscribe(data => {
      this.subCategoria = data;
    });
  }
  crearSubCategoria() {
    this.dialogService.open(SubcategoriaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getSubCategoria(0));
  }
  updateSubCategoria(idSubCategoria) {
    this.dialogService.open(SubcategoriaCreateComponent, {
      context: {
        idTipo: 2,
        idSubCategoria: idSubCategoria,
      },
    }).onClose.subscribe( x => this.getSubCategoria(1));
  }
  bajaSubCategoria(idSubCategoria) {
    this.getSubCategoriaById(idSubCategoria);
    swal({
      title: '¿Deseas dar de baja esta Sub Categoría?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.subCategoria['activo'] = 0;
        this.subCategoriaServive.put(idSubCategoria, this.subCategoria).subscribe( result => {
          this.subCategoriaServive.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡La sub categoría se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getSubCategoria(0);
        }));
      }
    });
  }
  ngOnInit() {
    this.connect();
    this.getSubCategoria(0);
    this.cols = ['detalle', 'regla', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.subCategoria.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.subCategoria.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
