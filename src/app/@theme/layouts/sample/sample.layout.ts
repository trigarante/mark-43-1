import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {delay, withLatestFrom, takeWhile} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';


import {StateService} from '../../../@core/data/state.service';
import {TokenStorageService} from '../../../@core/data/services/login/token-storage.service';
import {JwtResponseData} from '../../../@core/data/services/login/jwt-response';
// import {EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {User} from '../../../@core/data/interfaces/ti/user';
import {UserService} from '../../../@core/data/users.service';
import * as $ from 'jquery';

// TODO: move layouts into the framework
@Component({
  selector: 'ngx-sample-layout',
  templateUrl: './sample.layout.html',
  styleUrls: ['./sample.layout.scss'],
})
export class SampleLayoutComponent implements OnDestroy, OnInit {

  subMenu: NbMenuItem[] = [];
  layout: any = {};
  sidebar: any = {};
  rol: any;
  permisos: any;
  loader: any;

  private alive = true;

  currentTheme: string;

  constructor(protected stateService: StateService,
              protected menuService: NbMenuService,
              protected themeService: NbThemeService,
              protected bpService: NbMediaBreakpointsService,
              protected sidebarService: NbSidebarService, private token: TokenStorageService, protected loginService: JwtResponseData,
              private userService: UserService) {

    setTimeout(function () {
      $('.letter').animate({
        opacity: 1,
      }, 3000);
    }, 200);

    setTimeout(function () {
      $('#text').animate({
        opacity: 1,
        top: '40%',
      }, 2000);
    }, 1500);

    setTimeout(function () {
      // $('.loader').css('display', 'none');
      // $('.loader').fadeOut(1000);

      $('.loader').animate({
        top: '-110%',
      }, 1600);

    }, 4500);

    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    // console.log !(!!this.permisos.modulos.catalogos.escritura);
    this.subMenu.push(
      {
        title: 'Recursos Humanos',
         hidden: !(!!this.permisos.modulos.recursosHumanos.activo),
        icon: 'nb-person',
        children: [
          {
            title: 'Reclutamiento',
            hidden: !(!!this.permisos.modulos.reclutamiento.activo),
            children: [
              {
                title: 'Vacantes',
                link: '/modulos/rrhh/vacantes',
                hidden: !(!!this.permisos.modulos.vacantes.activo),
              },
              {
                title: 'Solicitudes',
                link: '/modulos/rrhh/solicitudes',
                hidden: !(!!this.permisos.modulos.solicitudes.activo),
              },
              {
                title: 'Pre candidatos',
                link: '/modulos/rrhh/precandidatos',
                hidden: !(!!this.permisos.modulos.precandidatos.activo),
              },
              {
                title: 'Candidatos',
                link: '/modulos/rrhh/candidatos',
                hidden: !(!!this.permisos.modulos.candidato.activo),
              },
              {
                title: 'Couching',
                link: '/modulos/rrhh/capacitacion',
                hidden: !(!!this.permisos.modulos.teorico.activo),
              },
              {
                title: 'Seguimiento',
                  link: '/modulos/rrhh/seguimiento',
                hidden: !(!!this.permisos.modulos.seguimiento.activo),
                children: [
                  {
                    title: 'Roll-Play',
                    link: '/modulos/rrhh/seguimiento',
                    hidden: !(!!this.permisos.modulos.roleplay.activo),
                  },
                  {
                    title: 'Asignaciones',
                    link: '/modulos/rrhh/seguimiento-practico',
                    hidden: !(!!this.permisos.modulos.ventaNueva.activo),
                  },
                ],
              },
              {
                title: 'Bajas',
                link: '/modulos/rrhh/bajas',
                hidden: !(!!this.permisos.modulos.bajas.activo),
              },
              {
                title: 'Reportes',
                 hidden: !(!!this.permisos.modulos.reportesRH.activo),
                children: [
                  {
                    title: 'Precandidato',
                    link: '/modulos/reportes/reporte-precandidato',
                    hidden: !(!!this.permisos.modulos.reportePrecandidato.activo),
                  },
                  {
                    title: 'Candidato',
                    link: '/modulos/reportes/reporte-candidato',
                    hidden: !(!!this.permisos.modulos.reporteCandidato.activo),
                  },
                  {
                    title: 'Capacitacion',
                    link: '/modulos/reportes/reporte-capacitacion',
                    hidden: !(!!this.permisos.modulos.reporteCapacitacion.activo),
                  },
                  {
                    title: 'Teorico',
                    link: '/modulos/reportes/reporte-seguimiento',
                    hidden: !(!!this.permisos.modulos.reporteTeorico.activo),
                  },
                ],
              },
            ],
          },
          {
            title: 'Capital Humano',
            hidden: !(!!this.permisos.modulos.capitalHumano.activo),
            children: [
              {
                title: 'Pre empleados',
                link: '/modulos/rrhh/pre-empleados',
                hidden: !(!!this.permisos.modulos.preEmpleado.activo),
              },
              {
                title: 'Empleados',
                hidden: !(!!this.permisos.modulos.empleados.activo),
                children: [
                  {
                    title: 'Ver empleados',
                    link: '/modulos/rrhh/empleados',
                    hidden: !(!!this.permisos.modulos.empleados.activo),
                  },
                  {
                    title: 'Baja de empleados',
                    link: '/modulos/rrhh/baja-empelados',
                    hidden: !(!!this.permisos.modulos.bajaEmpleados.activo),
                  },
                  {
                    title: 'Alta de IMSS',
                    link: '/modulos/rrhh/empleados/altaImss',
                    hidden: !(!!this.permisos.modulos.altaDeIMSS.activo),
                  },
                ],
              },
              {
                title: 'Reportes',
                hidden: !(!!this.permisos.modulos.reporteCapitalHumano.activo),
                children: [
                  {
                    title: 'Empleado',
                    link: '/modulos/reportes/reporte-empleado',
                    hidden: !(!!this.permisos.modulos.reporteEmpleado.activo),
                  },
                ],
              },
            ],
          },
          {
            title: 'Catálogos',
            hidden: !(!!this.permisos.modulos.catalogos.activo),
            children: [
              {
                title: 'Banco',
                link: '/modulos/rrhh/catalogos/banco',
                hidden: !(!!this.permisos.modulos.banco.activo),
              },
              {
                title: 'Bolsa de trabajo',
                link: '/modulos/rrhh/catalogos/bolsaTrabajo',
                hidden: !(!!this.permisos.modulos.bolsaDeTrabajo.activo),
              },
              {
                title: 'Competencias',
                link: '/modulos/rrhh/catalogos/competencias',
                hidden: !(!!this.permisos.modulos.competencias.activo),
              },
              {
                title: 'Competencias de candidato',
                link: '/modulos/rrhh/catalogos/competenciasCandidato',
                hidden: !(!!this.permisos.modulos.competenciasCandidato.activo),
              },
              // {
              //   title: 'Empresa',
              //   link: '/modulos/rrhh/catalogos/empresa',
              // },
              {
                title: 'Escolaridad',
                link: '/modulos/rrhh/catalogos/escolaridad',
                hidden: !(!!this.permisos.modulos.escolaridad.activo),
              },
              {
                title: 'Estado de escolaridad',
                link: '/modulos/rrhh/catalogos/estadoEscolaridad',
                hidden: !(!!this.permisos.modulos.estadoDeEscolaridad.activo),
              },
              {
                title: 'Estado civil',
                link: '/modulos/rrhh/catalogos/estadoCivil',
                hidden: !(!!this.permisos.modulos.estadoCivil.activo),
              },
              // {
              //   title: 'Grupo',
              //   link: '/modulos/rrhh/catalogos/grupo',
              // },
              {
                title: 'Medio de transporte',
                link: '/modulos/rrhh/catalogos/medioTransporte',
                hidden: !(!!this.permisos.modulos.medioDeTransporte.activo),
              },
              {
                title: 'Motivos de Baja',
                link: '/modulos/rrhh/catalogos/motivosBaja',
                hidden: !(!!this.permisos.modulos.motivosDeBaja.activo),
              },
              {
                title: 'Paises',
                link: '/modulos/rrhh/catalogos/paises',
                hidden: !(!!this.permisos.modulos.paises.activo),
              },
              {
                title: 'Puesto',
                link: '/modulos/rrhh/catalogos/puesto',
                hidden: !(!!this.permisos.modulos.puesto.activo),
              },
              // {
              //   title: 'Sede',
              //   link: '/modulos/rrhh/catalogos/sede',
              // },
              {
                title: 'Tipo de puesto',
                link: '/modulos/rrhh/catalogos/tipoPuesto',
                hidden: !(!!this.permisos.modulos.tipoDePuesto.activo),
              },
              {
                title: 'Turno',
                link: '/modulos/rrhh/catalogos/turno',
                hidden: !(!!this.permisos.modulos.turno.activo),
              },
              // {
              //   title: 'Sub Area',
              //   link: '/modulos/rrhh/catalogos/sub-area',
              // },
              {
                title: 'Razon Social',
                link: '/modulos/rrhh/catalogos/razon-social',
                hidden: !(!!this.permisos.modulos.razonSocial.activo),
              },
            ],
          },
          {
            /* MODULO PREGUNTAS CULTURA Y BIENESTAR DE RH */
            title: 'Preguntas',
            hidden: !(!!this.permisos.modulos.preguntas.activo),
            children: [
              {
                title: 'Generar Preguntas',
                link: '/modulos/rrhh/generar-preguntas',
                hidden: !(!!this.permisos.modulos.generarPreguntas.activo),
              },
              {
                title: 'Generar Respuestas',
                link: '/modulos/rrhh/generar-respuestas',
                hidden: !(!!this.permisos.modulos.generarRespuestas.activo),
              },
              {
                title: 'Resultados',
                link: '/modulos/rrhh/resultados',
                hidden: !(!!this.permisos.modulos.resultados.activo),
              },
            ],
          },
          // {
          //   title: 'Reportes',
          //   children: [
          //     {
          //       title: 'Pre-candidato',
          //       link: '/modulos/reportes/reporte-precandidato',
          //     },
          //     {
          //       title: 'Candidato',
          //       link: '/modulos/reportes/reporte-candidato',
          //     },
          //     {
          //       title: 'Capacitación',
          //       link: '/modulos/reportes/reporte-capacitacion',
          //     },
          //     {
          //       title: 'Seguimiento',
          //       link: '/modulos/reportes/reporte-seguimiento',
          //     },
          //     {
          //       title: 'Empleado',
          //       link: '/modulos/reportes/reporte-empleado',
          //     },
          //   ],
          // },
        ],
      },
      {
        title: 'TI',
        icon: 'nb-gear',
         hidden: !(!!this.permisos.modulos.ti.activo),
        children: [

          {
            title: 'Administración TI',
             hidden: !(!!this.permisos.modulos.administracionTi.activo),
            children: [
              {
                title: 'Grupo Permisos',
                link: '/modulos/ti/permisos',
                 hidden: !(!!this.permisos.modulos.grupoPermisos.activo),
              },
              {
                title: 'Reactivar Permisos',
                link: '/modulos/ti/eliminar',
                 hidden: !(!!this.permisos.modulos.reactivarPermisos.activo),
              },
              {
                title: 'Usuarios',
                link: '/modulos/ti/usuarios',
                  hidden: !(!!this.permisos.modulos.usuariosTi.activo),
              },
              {
                title: 'Extensiones',
                link: '/modulos/ti/extensiones',
                 hidden: !(!!this.permisos.modulos.extensiones.activo),
              },
              {
                title: 'Reportes',
                 hidden: !(!!this.permisos.modulos.reportesTI.activo),
                children: [
                  {
                    title: 'Grupo Permiso',
                    link: '/modulos/reportes/reportes-grupo-permisos',
                     hidden: !(!!this.permisos.modulos.reporteGrupoPermiso.activo),
                  },
                  {
                    title: 'Usuarios',
                    link: '/modulos/reportes/reportes-usuarios',
                     hidden: !(!!this.permisos.modulos.reporteUsuarios.activo),
                  },
                  {
                    title: 'Extensiones',
                    link: '/modulos/reportes/reportes-extensiones',
                     hidden: !(!!this.permisos.modulos.reporteExtensiones.activo),
                  },
                ],
              },
            ],
          },
          {
            title: 'Asignaciones',
            hidden: !(!!this.permisos.modulos.asignaciones.activo),
            children: [

              {
                title: 'Usuario',
                link: '/modulos/ti/ejecutivo',
                hidden: !(!!this.permisos.modulos.usuarioAsignaciones.activo),
              },
            ],
          },
          {
            title: 'Soporte',
            hidden: !(!!this.permisos.modulos.soporte.activo),
            children: [
              {
                title: 'Inventario Usuario',
                link: '/modulos/ti/soporte/inventario/cpu',
                hidden: !(!!this.permisos.modulos.inventarioUsuario.activo),
              },
              {
                title: 'Inventario Redes',
                link: '/modulos/ti/soporte/inventarioRedes/switch',
                hidden: !(!!this.permisos.modulos.inventarioRedes.activo),
              },
              {
                title: 'Inventario Soporte',
                link: '/modulos/ti/soporte/inventarioSoporte/discoduroexterno',
                hidden: !(!!this.permisos.modulos.inventarioSoporte.activo),
              },
              {
                title: 'Baja-Inventario Usuario',
                link: '/modulos/ti/soporte/baja-inventario',
                hidden: !(!!this.permisos.modulos.bajasInventarioUsuario.activo),
              },
              {
                title: 'Baja Inventario Redes',
                link: '/modulos/ti/soporte/baja-inventario-redes',
                hidden: !(!!this.permisos.modulos.bajasInventarioRedes.activo),
              },
              {
                title: 'Baja Inventario Soporte',
                link: '/modulos/ti/soporte/baja-inventario-soporte',
                hidden: !(!!this.permisos.modulos.bajasInventarioSoporte.activo),
              },
              {
                title: 'Reportes',
                hidden: !(!!this.permisos.modulos.reporteSoporteTi.activo),
                children: [
                  {
                    title: 'Inventario Usuario',
                    link: '/modulos/reportes/reportes-inventario-usuario',
                     hidden: !(!!this.permisos.modulos.reporteInventarioUsuario.activo),
                  },
                  {
                    title: 'Inventario Redes',
                    link: '/modulos/reportes/reportes-inventario-redes',
                    hidden: !(!!this.permisos.modulos.reporteInventarioRedes.activo),
                  },
                  {
                    title: 'Inventario Soporte',
                    link: '/modulos/reportes/reportes-inventario-soporte',
                    hidden: !(!!this.permisos.modulos.reporteInventarioSoporte.activo),
                  },
                ],
              },
              {
                title: 'Catalogos',
                hidden: !(!!this.permisos.modulos.catalogosSoporte.activo),
                children: [
                  {
                    title: 'Estado de Inventario',
                    link: '/modulos/ti/soporte/catalogos-soporte/estado-inventario',
                    hidden: !(!!this.permisos.modulos.estadoInventario.activo),
                  },
                  {
                    title: 'Observaciones de Inventario',
                    link: '/modulos/ti/soporte/catalogos-soporte/observaciones-inventario',
                    hidden: !(!!this.permisos.modulos.observacionesInventario.activo),
                  },
                  {
                    title: 'Marcas',
                    link: '/modulos/ti/soporte/catalogos-soporte/marca',
                    hidden: !(!!this.permisos.modulos.marcas.activo),
                  },
                  {
                    title: 'Procesadores',
                    link: '/modulos/ti/soporte/catalogos-soporte/procesadores',
                    hidden: !(!!this.permisos.modulos.procesadores.activo),
                  },
                ],
              },
            ],
          },
          {
            title: 'Control de llamadas',
            link: '/modulos/ti/control-llamadas',
            hidden: !(!!this.permisos.modulos.controlLlamadas.activo),
          },
        ],
      },
      {
        title: 'Comercial',
          hidden: !(!!this.permisos.modulos.comercial.activo),
        icon: 'nb-person',
        children: [
          {
            title: 'Socios',
            hidden: !(!!this.permisos.modulos.socios.activo),
            children: [
              {
                title: 'Ver Socios',
                link: '/modulos/comercial/socios',
                hidden: !(!!this.permisos.modulos.verSocios.activo),
              },
              {
                title: 'Contacto',
                link: '/modulos/comercial/contacto',
                hidden: !(!!this.permisos.modulos.contacto.activo),
              },
              {
                title: 'Url',
                link: '/modulos/comercial/asignar-url',
                hidden: !(!!this.permisos.modulos.url.activo),
              },
              {
                title: 'Objetivo de Ventas',
                link: '/modulos/comercial/objetivo-de-ventas',
                hidden: !(!!this.permisos.modulos.objetivoDeVentas.activo),
              },
              {
                title: 'Contratos',
                link: '/modulos/comercial/contratos',
                hidden: !(!!this.permisos.modulos.contrato.activo),
              },
            ],
          },
          {
            title: 'Ramo',
            link: '/modulos/comercial/ramo',
            hidden: !(!!this.permisos.modulos.ramo.activo),
          },
          {
            title: 'Sub Ramo',
             hidden: !(!!this.permisos.modulos.subRamo.activo),
            children: [
              {
                title: 'Ver Sub Ramo',
                link: '/modulos/comercial/sub-ramo',
                hidden: !(!!this.permisos.modulos.verSubRamo.activo),
              },
              {
                title: 'Convenios',
                link: '/modulos/comercial/convenios',
                hidden: !(!!this.permisos.modulos.convenios.activo),
              },
            ],
          },
          {
            title: 'Producto',
            hidden: !(!!this.permisos.modulos.producto.activo),
            children: [
              {
                title: 'Ver Producto',
                link: '/modulos/comercial/producto',
                hidden: !(!!this.permisos.modulos.verProducto.activo),
              },
              {
                title: 'Competencia',
                link: '/modulos/comercial/competencia',
                hidden: !(!!this.permisos.modulos.competenciaProducto.activo),
              },
              {
                title: 'Analisis-Competencia',
                link: '/modulos/comercial/analisis-competencia',
                hidden: !(!!this.permisos.modulos.analisisCompetencia.activo),
              },
            ],
          },
          {
            title: 'Cobertura',
            link: '/modulos/comercial/cobertura',
            hidden: !(!!this.permisos.modulos.cobertura.activo),
          },
          {
            title: 'Catalogos',
            hidden: !(!!this.permisos.modulos.catalogosComercial.activo),
            children: [
              {
                title: 'Tipo Ramo',
                link: '/modulos/comercial/catalogos-socios/tipo-ramo',
                hidden: !(!!this.permisos.modulos.tipoGiro.activo),
              },
              {
                title: 'Tipo Sub Ramo',
                link: '/modulos/comercial/catalogos-socios/tipo-sub-ramo',
                // hidden: !(!!this.permisos.modulos.tipoGiro.activo),
              },
              {
                title: 'Tipo Competencia',
                link: '/modulos/comercial/catalogos-socios/tipo-competencia',
                hidden: !(!!this.permisos.modulos.tipoCompetencia.activo),
              },
              {
                title: 'Tipo Producto',
                link: '/modulos/comercial/catalogos-socios/tipo-producto',
                hidden: !(!!this.permisos.modulos.tipoProducto.activo),
              },
              {
                title: 'Divisas',
                link: '/modulos/comercial/catalogos-socios/divisas',
                hidden: !(!!this.permisos.modulos.divisas.activo),
              },
              {
                title: 'Categoria',
                link: '/modulos/comercial/catalogos-socios/categoria',
                hidden: !(!!this.permisos.modulos.categoria.activo),
              },
              {
                title: 'Sub Categoria',
                link: '/modulos/comercial/catalogos-socios/sub-categoria',
                hidden: !(!!this.permisos.modulos.subCategoria.activo),
              },
              {
                title: 'Tipo Cobertura',
                link: '/modulos/comercial/catalogos-socios/tipo-cobertura',
                hidden: !(!!this.permisos.modulos.tipoCobertura.activo),
              },
              {
                title: 'Bajas Socios',
                link: '/modulos/comercial/catalogos-socios/bajas-socios',
                hidden: !(!!this.permisos.modulos.bajasSocios.activo),

              },
            ],
          },
          {
            title: 'Reportes',
            hidden: !(!!this.permisos.modulos.reportesComercial.activo),
            children: [
              {
                title: 'Socios',
                link: '/modulos/reportes/reporte-socios',
                hidden: !(!!this.permisos.modulos.reporteSocios.activo),
              },
            ],
          },
        ],
      },
      {
        title: 'Marketing',
        hidden: !(!!this.permisos.modulos.marketing.activo),
        icon: 'nb-email',
        children: [
          {
            title: 'Campaña',
            hidden: !(!!this.permisos.modulos.campaña.activo),
            children: [
              {
                title: 'Ver campaña',
                link: '/modulos/marketing/campana',
                hidden: !(!!this.permisos.modulos.verCampaña.activo),
              },
              {
                title: 'Medio difusión',
                link: '/modulos/marketing/medios-difusion',
                hidden: !(!!this.permisos.modulos.medioDifusion.activo),
              },
              {
                title: 'Campaña categoria',
                link: '/modulos/marketing/camapana-categoria',
                hidden: !(!!this.permisos.modulos.campañaCategoria.activo),
              },
            ],
          },
          {
            title: 'Paginas',
            link: '/modulos/marketing/pagina',
            hidden: !(!!this.permisos.modulos.paginas.activo),
          },
          {
            title: 'Catalogos',
            hidden: !(!!this.permisos.modulos.catalogosMarketing.activo),
            children: [
              {
                title: 'Tipo Paginas',
                link: '/modulos/marketing/catalogos-marketing/tipo-pagina',
                hidden: !(!!this.permisos.modulos.tipoPaginas.activo),
              },
              {
                title: 'Proveedor Leads',
                link: '/modulos/marketing/catalogos-marketing/provedor-lead',
                hidden: !(!!this.permisos.modulos.provedorLeads.activo),

              },
            ],
          },
          {
            title: 'Reportes',
            children: [
              {
                title: 'Campaña',
                link: '/modulos/reportes/reporte-campana',
              },
            ],
          },
        ],
      },
      // {
      //   title: 'Venta Nueva',
      //
      //   icon: 'nb-phone',
      //   children: [
      //     {
      //       title: 'Solicitudes',
      //       link: '/modulos/venta-nueva/solicitudes-vn',
      //     },
      //     // {
      //     //   title: 'Cotizador',
      //     //   link: '/modulos/venta-nueva/cotizador-vn',
      //     // },
      //     {
      //       title: 'Catalogos',
      //       children: [
      //         {
      //           title: 'Tipo Contacto',
      //           link: '/modulos/venta-nueva/catalogos/tipocontacto',
      //         },
      //         {
      //           title: 'Tipo Pago',
      //           link: '/modulos/venta-nueva/catalogos/tipopago',
      //         },
      //         {
      //           title: 'Estado Pago',
      //           link: '/modulos/venta-nueva/catalogos/estadopago',
      //         },
      //         {
      //           title: 'Estado Póliza',
      //           link: '/modulos/venta-nueva/catalogos/estadopoliza',
      //         },
      //         {
      //           title: 'Flujo Póliza',
      //           link: '/modulos/venta-nueva/catalogos/flujopoliza',
      //         },
      //       ],
      //     },
      //     {
      //       title: 'Reportes',
      //       children: [
      //         {
      //           title: 'Solicitudes',
      //           link: '/modulos/reportes/reporte-solicitudes',
      //         },
      //       ],
      //     },
      //   ],
      // },
      // {
      //   title: 'Mesa de Control',
      //   icon: 'nb-keypad',
      //   children: [
      //     {
      //       title: 'Ejemplo',
      //       link: '/modulos/venta-nueva/solicitudes-vn',
      //     },
      //   ],
      // },
      // {
      //   title: 'Autorizaciones',
      //   icon: 'nb-checkmark',
      //   children: [
      //     {
      //       title: 'Ejemplo',
      //       link: '/modulos/venta-nueva/solicitudes-vn',
      //     },
      //   ],
      // },
      // {
      //   title: 'Endosos y Cancelaciones',
      //   icon: 'nb-close',
      //   children: [
      //     {
      //       title: 'Solicitudes Endosos',
      //       link: '/modulos/endosos-cancelaciones/solicitudes-endosos-cancelaciones',
      //     },
      //     {
      //       title: 'Ver Endoso y cancelaciones',
      //       link: '/modulos/endosos-cancelaciones/endosos-cancelaciones',
      //     },
      //   ],
      // },
      // {
      //   title: 'Finanzas',
      //   icon: 'nb-list',
      //   children: [
      //     {
      //       title: 'Centros de Costos',
      //       children: [
      //         {
      //           title: 'Grupos',
      //           link: '/modulos/finanzas/grupo',
      //         },
      //         {
      //           title: 'Empresas',
      //           link: '/modulos/finanzas/empresas',
      //         },
      //         {
      //           title: 'Sede',
      //           link: '/modulos/finanzas/sede',
      //         },
      //         {
      //           title: 'Area',
      //           link: '/modulos/finanzas/area',
      //         },
      //         {
      //           title: 'Sub Area',
      //           link: '/modulos/finanzas/sub-area',
      //         },
      //         {
      //           title: 'Centro Costo',
      //           link: '/modulos/finanzas/centro-costo',
      //         },
      //       ],
      //     },
      //   ],
      // },
      // {
      //   title: 'Cotizaciones',
      //   icon: 'nb-shuffle',
      //   children: [
      //     {
      //       title: 'Ejemplo',
      //       link: '/modulos/cotizaciones/ejemplo1',
      //     },
      //   ],
      // },
      // {
      //   title: 'Actualizaciones',
      //   icon: 'nb-loop',
      //   link: '/modulos/actualizaciones/actualizaciones',
      // },

    );
    this.stateService.onLayoutState()
      .pipe(takeWhile(() => this.alive))
      .subscribe((layout: string) => this.layout = layout);

    this.stateService.onSidebarState()
      .pipe(takeWhile(() => this.alive))
      .subscribe((sidebar: string) => {
        this.sidebar = sidebar;
      });

    const isBp = this.bpService.getByName('is');
    this.menuService.onItemSelect()
      .pipe(
        takeWhile(() => this.alive),
        withLatestFrom(this.themeService.onMediaQueryChange()),
        delay(20),
      )
      .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

        if (bpTo.width <= isBp.width) {
          this.sidebarService.collapse('menu-sidebar');
        }
      });

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
      });

    setTimeout(function () {
      $('#text').animate({
        opacity: 1,
        top: '40%',
      }, 2000);
    }, 1500);

    setTimeout(function () {
      $('.line').animate({
        opacity: 0.8,
        width: '20%',
      }, 3500);
    }, 3000);

    setTimeout(function () {
      // $('.loader').css('display', 'none');
      // $('.loader').fadeOut(1000);

      $('.loader').animate({
        top: "-110%",
      }, 1600);

    }, 4500);
  }

  getPermisos() {
    this.userService.getCurrentUser(window.localStorage.getItem('token'))
      .subscribe((users: User) => {
        this.permisos = users.permisos;
        // console.log(this.permisos);
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit() {
    // window.console.log = function() {};
    // window.console.error = function() {};
    // window.console.info = function() {};
    // window.console.debug = function() {};
    // window.console.warn = function() {};
    // window.console.trace = function() {};
    // window.console.dir = function() {};
    // window.console.dirxml = function() {};
    // window.console.group = function() {};
    // window.console.groupEnd = function() {};
    // window.console.time = function() {};
    // window.console.timeEnd = function() {};
    // window.console.assert = function() {};
    // window.console.profile = function() {};
    // window.console.exception = function() {};
    // window.onerror = null;
  }
  /*
  texto = 'Lo sentimos, pero no esta permitido copiar este contenido :(';
  @HostListener('window:keydown', ['$event'])
  onKeyPress($event: KeyboardEvent) {
    if (($event.ctrlKey || $event.metaKey) && $event.keyCode === 67) {
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = this.texto;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }
    if (($event.ctrlKey || $event.metaKey) && $event.keyCode === 86) {
      return  null;
    }
  }*/
}
