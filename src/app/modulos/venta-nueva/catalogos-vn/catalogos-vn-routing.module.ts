import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  MatAutocompleteModule,
  MatBottomSheetModule, MatButtonModule,
  MatCardModule, MatChipsModule, MatDialogModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule,
  MatTableModule, MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import {TipoContactoVnComponent} from './tipo-contacto-vn/tipo-contacto-vn.component';
import {CatalogosVnComponent} from './catalogos-vn.component';
import {CommonModule} from '@angular/common';
import {TipoPagoComponent} from './tipo-pago/tipo-pago.component';
import {EstadoPolizaComponent} from './estado-poliza/estado-poliza.component';
import {FlujoPolizaComponent} from './flujo-poliza/flujo-poliza.component';
import {EstadoPagoComponent} from './estado-pago/estado-pago.component';

const routes: Routes = [{
  path: '',
  component: CatalogosVnComponent,
  children: [
    {
      path: 'tipocontacto',
      component: TipoContactoVnComponent,
    },
    {
      path: 'tipopago',
      component: TipoPagoComponent,
    },
    {
      path: 'estadopoliza',
      component: EstadoPolizaComponent,
    },
    {
      path: 'estadopago',
      component: EstadoPagoComponent,
    },
    {
      path: 'flujopoliza',
      component: FlujoPolizaComponent,
    },
  ],
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatAutocompleteModule,
    MatBottomSheetModule, MatButtonModule,
    MatCardModule, MatChipsModule, MatDialogModule, MatFormFieldModule,
    MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatOptionModule,
    MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
    MatSortModule,
    MatTableModule, MatToolbarModule, MatTooltipModule,
  ],
  exports: [
    RouterModule,
  ],
})
export class CatalogosVnRoutingModule { }
