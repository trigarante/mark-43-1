import {getTestBed, TestBed} from '@angular/core/testing';
import {VacantesService} from './vacantes.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CustomMatchers, Conexion} from '../matchers';
import {environment} from '../../../../../environments/environment';

fdescribe('Vacante Servicios', () => {
  const url = environment.GLOBAL_SERVICIOS;
  let injector: TestBed;
  let service: VacantesService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = [{
    id: 158,
    idPuesto: 16,
    idTipoPuesto: 1,
    idSubarea: 2,
    nombre: 'VACANTE TEST UNICO',
    activo: 0,
  },
    {
      id: 'vacantes',
    }];
  Conexion(url, body[1].id, true);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [VacantesService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(VacantesService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/vacantes');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/vacantes', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/vacantes');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/vacantes', '/158/74');
      });
    });
  }
});
