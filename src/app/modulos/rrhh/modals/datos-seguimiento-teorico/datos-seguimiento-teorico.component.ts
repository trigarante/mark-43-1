import {Component, Input, OnInit} from '@angular/core';
import {Seguimiento, SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import {NbDialogRef} from '@nebular/theme';
import {SeguimientoComponent} from '../../seguimiento/seguimiento.component';
import {discardPeriodicTasks} from '@angular/core/testing';

@Component({
  selector: 'ngx-datos-seguimiento-teorico',
  templateUrl: './datos-seguimiento-teorico.component.html',
  styleUrls: ['./datos-seguimiento-teorico.component.scss'],
})
export class DatosSeguimientoTeoricoComponent implements OnInit {
  @Input() idSeguimiento: number;
  seguimiento: Seguimiento;
  fechaIngreso: Date;
  calificacionRollplay: number;
  idCapacitacion: number;
  idCompetencia: number;
  calificacion: number;
  idCoach: number;
  idSubarea: number;
  nombreCampana: string;
  asistencia: string;
  registro: number;
  idCliente: number;
  prima: number;
  comentarios: string;
  idEtapa: number;
  empleado: string;
  nombreSubarea: string;
  comentariosFaseDos: number;
  idEstadoRRHH: number;
  sede: number;
  nombre: string;

  constructor(protected seguimientoService: SeguimientoData, protected ref: NbDialogRef<SeguimientoComponent>) {
  }

  getSeguimientoById() {
    this.seguimientoService.getSeguimientoById(this.idSeguimiento).subscribe(data => {
      this.seguimiento = data;
      this.nombre = data.nombre;
      this.calificacionRollplay = data.calificacionRollplay;
      this.calificacion = data.calificacion;
      this.comentarios = data.comentarios;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getSeguimientoById();
  }

}
