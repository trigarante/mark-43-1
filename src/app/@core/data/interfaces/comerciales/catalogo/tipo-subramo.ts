import {Observable} from 'rxjs';
export interface TipoSubramo {
  id: number;
  tipo: string;
  activo: number;
}
export abstract class TipoSubramoData {
  abstract get(): Observable<TipoSubramo[]>;

  abstract post(tipoSubramo): Observable<TipoSubramo>;

  abstract getTipoSubramoById(idTipoSubramo): Observable<TipoSubramo>;

  abstract put(idTipoSubramo, tipoSubramo): Observable<TipoSubramo>;
}
