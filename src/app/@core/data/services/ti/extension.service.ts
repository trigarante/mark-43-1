import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Extension, ExtensionData} from '../../interfaces/ti/extension';
import {Observable} from 'rxjs';


@Injectable()
export class ExtensionService extends ExtensionData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  getExtensionByIdSubarea(idSubarea, IdEstado): Observable<Extension[]> {
    return this.http.get<Extension[]>(this.baseURL + 'v1/extensiones/idSubarea/' + idSubarea + '/idEstado/' + IdEstado);
  }

  put(idExtension, extension: Extension): Observable<Extension> {
    return this.http.put<Extension>(this.baseURL + 'v1/extensiones/' + idExtension + '/'
      + sessionStorage.getItem('Empleado'), extension);
  }

  get(id): Observable<Extension> {
    return this.http.get<Extension>(this.baseURL + 'v1/extensiones/' + id);
  }

  getExtension(): Observable<Extension[]> {
    return this.http.get<Extension[]>(this.baseURL + 'v1/extensiones');
  }

  post(jsonExtension, min, max) {
    return this.http.post(this.baseURL + 'v1/extensiones/rango/' + min + '/' + max, jsonExtension);
  }

  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'extensiones/saveExtensiones', json); //
  }
}
