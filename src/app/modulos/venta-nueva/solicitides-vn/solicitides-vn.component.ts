import {Component, OnInit, ViewChild} from '@angular/core';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginatorIntl, TooltipPosition} from '@angular/material';
import {FormControl} from '@angular/forms';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {Solicitudes, SolicitudesVNData} from '../../../@core/data/interfaces/ventaNueva/solicitudes';
import {ProspectoCreateComponent} from '../modals/prospecto-create/prospecto-create.component';
import {DatosSolicitudComponent} from '../modals/datos-solicitud/datos-solicitud.component';
import {LlamadasMyccComponent} from '../modals/llamadas-mycc/llamadas-mycc.component';
import {EstadoSolicitudComponent} from '../modals/estado-solicitud/estado-solicitud.component';
import {environment} from '../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-solicitides-vn',
  templateUrl: './solicitides-vn.component.html',
  styleUrls: ['./solicitides-vn.component.scss'],
})
export class SolicitidesVnComponent extends MatPaginatorIntl implements OnInit {
  dataSource: any;
  loading: boolean;
  solicitudesVN: Solicitudes[];
  /**Tooltip**/
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  position = new FormControl(this.positionOptions[0]);
  // sockets
  private stompClient = null;
  constructor(protected solicitudVentaService: SolicitudesVNData, private dialogoService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router) {
    super();
    this.loading = true;

    this.getAndInitTranslations();
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'vnsolicitudes');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      _this.stompClient.subscribe('/task/panelVnSolicitudes', (content) => {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getSolicitudVN(0);
          }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getAndInitTranslations() {
    this.itemsPerPageLabel = 'DATOS POR PÁGINA';
    this.nextPageLabel = 'DATOS POR PÁGINA';
    this.previousPageLabel = 'DATOS POR PÁGINA';
    this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} DE ${length}`;
  }

  crearSolicitudVN() {
    this.dialogoService.open(ProspectoCreateComponent);
  }

  getSolicitudVN(mensajeAct) {
    this.solicitudVentaService.get().pipe(map(result => {
      return result.filter(data => data.idEstadoSolicitud === 1);
    })).subscribe(valor => {
      this.solicitudesVN = [];
      this.solicitudesVN = valor;
      this.dataSource = new MatTableDataSource(this.solicitudesVN); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Vacantes`);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.connect();
    this.cols = ['detalle', 'fechaSolicitud', 'empleado', 'prima', 'estadoSolicitud', 'acciones'];
    this.getSolicitudVN(0);
  }

  estadoSolicitud(idSolicitud, idCotizacionAli, idEmpleado) {
    this.dialogoService.open(EstadoSolicitudComponent, {
      context: {
        idSolicitud: idSolicitud,
        idCotizacionAli: idCotizacionAli,
        idEmpleado: idEmpleado,
      },
    });
  }

  verDatos(idProspecto, idProducto) {
    this.dialogoService.open(DatosSolicitudComponent, {
      context: {
        idProspecto: idProspecto,
        idProducto: idProducto,
      },
    });
  }

  programarLlamada(idSolicitud) {
    this.dialogoService.open(LlamadasMyccComponent, {
      context: {
        idSolicitud: idSolicitud,
      },
    });
  }

  generarEmision(resp, idProducto, idSolicitud, idSubRamo) {
    let aseguradora: any;
    aseguradora = JSON.parse(resp);
    this.router.navigate(['modulos/venta-nueva/step-cotizador'], {
      state: {
        aseguradora: aseguradora.Aseguradora,
        idProductoSolicitud: idProducto,
        idSolicitud: idSolicitud,
        idSubRamo: idSubRamo,
      },
    });
  }

}
