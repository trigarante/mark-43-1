import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { FasesTareasData, FasesTareas } from '../../interfaces/tareas/fasesTareas';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';



@Injectable(
  {providedIn: 'root'},
)
export class TareasService extends FasesTareasData {
  private baseUrl;
  constructor(
    private http: HttpClient,
  ) {
    super();
    this.baseUrl = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<FasesTareas[]> {
    return this.http.get<FasesTareas[]>(this.baseUrl + 'v1/fasesTareas');
  }

}

