import {Component, OnInit, ViewChild} from '@angular/core';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {NbDialogService} from '@nebular/theme';
import {CreateEstadoInventarioComponent} from '../modals/create-estado-inventario/create-estado-inventario.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-estado-inventario',
  templateUrl: './estado-inventario.component.html',
  styleUrls: ['./estado-inventario.component.scss'],
})
export class EstadoInventarioComponent implements OnInit {
  cols: any[];
  estadoInventario: EstadoInventario[];
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private estadoInventarioServive: EstadoInventarioData, private dialogService: NbDialogService,
              private router: Router) { }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    /**---------------------------------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'estadoinventario');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      _this.stompClient.subscribe('/task/panelEstadoInventario', (content) => {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEstadoInventario();
          }, 500);

      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getEstadoInventario() {
    this.estadoInventarioServive.get().subscribe( data => {
      this.estadoInventario = data;
      this.dataSource = new MatTableDataSource(this.estadoInventario);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
/**crea un estado inventario*/
  creearEstadoInventario() {
    this.dialogService.open(CreateEstadoInventarioComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEstadoInventario();


    });
  }
  /** modifica un estado Inventario*/
  updateEstadoInventario(idEstadoInventario) {
    this.dialogService.open(CreateEstadoInventarioComponent, {
      context: {
        idTipo: 2,
        idEstadoInventario: idEstadoInventario,
      },
    }).onClose.subscribe(x => {
      this.getEstadoInventario();


    });
  }
  ngOnInit() {
    this.connect();
    this.getEstadoInventario();
    this.cols = [ 'estado', 'acciones' ];
    // this.cols = [
    //   {field: 'estado', header: 'Estado'},
    //   {field: 'acciones', header: 'Acciones'},
    // ];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.estadoInventario.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.estadoInventario.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
