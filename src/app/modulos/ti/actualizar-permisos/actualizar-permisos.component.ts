import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subarea, SubareaData } from '../../../@core/data/interfaces/catalogos/subarea';
import { Puesto, PuestoData } from '../../../@core/data/interfaces/catalogos/puesto';
import { GrupoUsuarios, GrupoUsuariosData} from '../../../@core/data/interfaces/ti/grupoUsuarios';
import { Sede, SedeData } from '../../../@core/data/interfaces/catalogos/sede';
import { Grupo, GrupoData } from '../../../@core/data/interfaces/catalogos/grupo';
import { Empresa, EmpresaData } from '../../../@core/data/interfaces/catalogos/empresa';
import { Area, AreaData } from '../../../@core/data/interfaces/catalogos/area';
import { Permiso } from './permisos'; // permisos
import * as _ from 'lodash';
import {PermisosVisualizacionService} from '../../../@core/data/services/ti/permisos-visualizacion.service';
import {PermisosVisualizacion} from '../../../@core/data/interfaces/ti/catalogos/permisos-visualizacion';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-actualizar-permisos',
  templateUrl: './actualizar-permisos.component.html',
  styleUrls: ['./actualizar-permisos.component.scss'],
  providers: [DialogService],
})
export class ActualizarPermisosComponent implements OnInit {
  /** Variable que permite la inicialización del formulario para crear un permiso. */
  permisosForm: FormGroup;
  permisos: string[];
  /** Variable que almacena el id de la solicitud que fue seleccionada. */
  idgrupo: number = this.rutaActiva.snapshot.params.idGrupo;
  idPuesto: number = this.rutaActiva.snapshot.params.idPuesto;
  idSubarea: number = this.rutaActiva.snapshot.params.idSubarea;
  /**  permite tener los valores del selec*/
  seleccion2: any[];
  /** inicialia variable sub */
  sub: any[];
  grupoUsuario: GrupoUsuarios;
  permiso: object;
  permisosdat: any;
  botones: any;
  botones2: any;
  botonesultimo: any;
  modulos: any;
  modulospar: any;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarPuesto: boolean;
  mostrarPermisoVisualizacion: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  puestos: Puesto[];
  /** Arreglo auxiliar para permiso de visualización. */
  permisosVisualizacion: PermisosVisualizacion[];

  //
  areaget: Subarea;
  sedeget: any;
  empresaget: any;
  grupoempresarialget: any;
  grupoget: any;

  // permisos
  permisos_rh: string[]; // rh
  camposRH = [];
  permisosRH: any;
  permisos_ti: string[]; // ti
  camposTI = [];
  permisosTI: any;
  permisos_c: string[]; // comercial
  camposC = [];
  permisosC: any;
  permisos_m: string[]; // marketing
  camposM = [];
  permisosM: any;

  disabled = true;
  private stompClient = null;
  datos: any[] = [];

  constructor(
    private fb: FormBuilder,
    private rutaActiva: ActivatedRoute,
    protected subareaService: SubareaData,
    protected puestoService: PuestoData,
    private router: Router,
    private grupoUsuariosService: GrupoUsuariosData,
    private sedesService: SedeData,
    private grupoService: GrupoData,
    private empresaService: EmpresaData,
    private areaService: AreaData,
    private permisosVisualizacionService: PermisosVisualizacionService,
  ) {
    // se inicializa el formulario,
    this.initForma();
  } // constructor

  ngOnInit() {
    this.initPermisos();
    this.getGrupo();
    this.getGrupoById();
    this.seleccion();
    // this.connect();
  }

  initPermisos() {
    this.permisos_rh = Permiso.permisosRH;
    this.permisos_rh.sort();
    this.permisos_ti = Permiso.permisosTI;
    this.permisos_ti.sort();
    this.permisos_m = Permiso.permisosMARKETING;
    this.permisos_m.sort();
    this.permisos_c = Permiso.permisosCOMERCIAL;
    this.permisos_c.sort();
  }

  /** Funcion para obtener los valores de true y false */
  seleccion() {
    this.seleccion2 = [];
    this.seleccion2.push({ label: 'true', value: true });
    this.seleccion2.push({ label: 'false', value: false });
  }

  initForma() {
    this.permisosForm = this.fb.group({
      nombre: new FormControl('', Validators.compose([Validators.required, Validators.pattern("^[A-Za-z].*$")])),
      descripcion: new FormControl('', Validators.compose([Validators.required, Validators.pattern("^[A-Za-z].*$")])),
      idGrupo: new FormControl('', Validators.compose([Validators.required])),
      idEmpresa: new FormControl('', Validators.compose([Validators.required])),
      idSede: new FormControl('', Validators.compose([Validators.required])),
      idArea: new FormControl('', Validators.compose([Validators.required])),
      idSubarea: new FormControl('', Validators.compose([Validators.required])),
      idPuesto: new FormControl('', Validators.compose([Validators.required])),
      idPermisosVisualizacion: new FormControl('', Validators.compose([Validators.required])),
      subareaSelected: {},
      puestoSelected: {},
      descargarPNC: false,
      descargarPNE: false,
      // rh - activo
      ACTIVORECLUTAMIENTO: false,
      ACTIVOSOLICITUDES: false,
      ACTIVOPRECANDIDATOS: false,
      ACTIVOCANDIDATO: false,
      ACTIVOEMPLEADOS: false,
      ACTIVOTEORICO: false,
      ACTIVOROLEPLAY: false,
      ACTIVOASIGNACIONESRH: false,
      ACTIVOVENTANUEVA: false,
      ACTIVOPREEMPLEADOS: false,
      ACTIVOVACANTES: false,
      ACTIVOSEGUIMIENTO: false,
      ACTIVOBAJAS: false,
      ACTIVOCAPITALHUMANO: false,
      ACTIVOBAJAEMPLEADOS: false,
      ACTIVOALTADEIMSS: false,
      ACTIVOCATALOGOS: false,
      ACTIVOAREAS: false,
      ACTIVOBANCO: false,
      ACTIVOBOLSADETRABAJO: false,
      ACTIVOCOMPETENCIAS: false,
      ACTIVOCOMPETENCIASCANDIDATO: false,
      ACTIVOEMPRESA: false,
      ACTIVOESCOLARIDAD: false,
      ACTIVOESTADODEESCOLARIDAD: false,
      ACTIVOESTADOCIVIL: false,
      ACTIVOMEDIODETRANSPORTE: false,
      ACTIVOMOTIVOSDEBAJA: false,
      ACTIVOPAISES: false,
      ACTIVOPUESTO: false,
      ACTIVOTIPODEPUESTO: false,
      ACTIVORAZONSOCIAL: false,
      ACTIVOTURNO: false,
      ACTIVORECURSOSHUMANOS: false,
      ACTIVOREPORTESRH: false,
      ACTIVOREPORTEPRECANDIDATO: false,
      ACTIVOREPORTECANDIDATO: false,
      ACTIVOREPORTECAPACITACION: false,
      ACTIVOREPORTETEORICO: false,
      ACTIVOREPORTEEMPLEADO: false,
      ACTIVOREPORTECAPITALHUMANO: false,
      ACTIVOPREGUNTAS: false,
      ACTIVOGENERARPREGUNTAS: false,
      ACTIVOGENERARRESPUESTAS: false,
      ACTIVORESULTADOS: false,
      // ti - activo
      ACTIVOTI: false,
      ACTIVOADMINISTRACIONTI: false,
      ACTIVOGRUPOPERMISOS: false,
      ACTIVOREACTIVARPERMISOS: false,
      ACTIVOUSUARIOSTI: false,
      ACTIVOEXTENSIONES: false,
      ACTIVOASIGNACIONES: false,
      ACTIVOUSUARIOASIGNACIONES: false,
      ACTIVOSUPERVISOR: false,
      ACTIVOSOPORTE: false,
      ACTIVOINVENTARIOUSUARIO: false,
      ACTIVOINVENTARIOREDES: false,
      ACTIVOINVENTARIOSOPORTE: false,
      ACTIVOBAJASINVENTARIOUSUARIO: false,
      ACTIVOBAJASINVENTARIOREDES: false,
      ACTIVOBAJASINVENTARIOSOPORTE: false,
      ACTIVOCATALOGOSSOPORTE: false,
      ACTIVOESTADODEINVENTARIO: false,
      ACTIVOOBSERVACIONESDEINVENTARIO: false,
      ACTIVOMARCAS: false,
      ACTIVOPROCESADORES: false,
      ACTIVOREPORTESTI: false,
      ACTIVOREPORTEGRUPOPERMISO: false,
      ACTIVOREPORTEUSUARIOS: false,
      ACTIVOREPORTEEXTENSIONES: false,
      ACTIVOREPORTESOPORTETI: false,
      ACTIVOREPORTEINVENTARIOUSUARIO: false,
      ACTIVOREPORTEINVENTARIOREDES: false,
      ACTIVOREPORTEINVENTARIOSOPORTE: false,
      ACTIVOCONTROLLLAMADAS: false,
      // comercial - activo
      ACTIVOCOMERCIAL: false,
      ACTIVOSOCIOS: false,
      ACTIVOVERSOCIOS: false,
      ACTIVOCONTACTO: false,
      ACTIVOURL: false,
      ACTIVOOBJETIVODEVENTAS: false,
      ACTIVOCONTRATO: false,
      ACTIVORAMO: false,
      ACTIVOSUBRAMO: false,
      ACTIVOVERSUBRAMO: false,
      ACTIVOCONVENIOS: false,
      ACTIVOPRODUCTO: false,
      ACTIVOVERPRODUCTO: false,
      ACTIVOCOMPETENCIAPRODUCTO: false,
      ACTIVOANALISISCOMPETENCIA: false,
      ACTIVOCOBERTURA: false,
      ACTIVOCATALOGOSCOMERCIAL: false,
      ACTIVOTIPOGIRO: false,
      ACTIVOTIPOCATEGORIA: false,
      ACTIVOTIPOCOMPETENCIA: false,
      ACTIVOTIPOPRODUCTO: false,
      ACTIVODIVISAS: false,
      ACTIVOCATEGORIA: false,
      ACTIVOSUBCATEGORIA: false,
      ACTIVOTIPOCOBERTURA: false,
      ACTIVOBAJASSOCIOS: false,
      ACTIVOREPORTESCOMERCIAL: false,
      ACTIVOREPORTESOCIOS: false,
      // marketing - activo
      ACTIVOMARKETING: false,
      ACTIVOCAMPAÑA: false,
      ACTIVOVERCAMPAÑA: false,
      ACTIVOMEDIODIFUSION: false,
      ACTIVOCAMPAÑACATEGORIA: false,
      ACTIVOPAGINAS: false,
      ACTIVOCATALOGOSMARKETING: false,
      ACTIVOTIPOPAGINAS: false,
      ACTIVOPROVEDORLEADS: false,

      // rh -escritura
      ESCRITURARECLUTAMIENTO: false,
      ESCRITURASOLICITUDES: false,
      ESCRITURAPRECANDIDATOS: false,
      ESCRITURACANDIDATO: false,
      ESCRITURAEMPLEADOS: false,
      ESCRITURATEORICO: false,
      ESCRITURAROLEPLAY: false,
      ESCRITURAASIGNACIONESRH: false,
      ESCRITURAVENTANUEVA: false,
      ESCRITURAPREEMPLEADOS: false,
      ESCRITURAVACANTES: false,
      ESCRITURASEGUIMIENTO: false,
      ESCRITURABAJAS: false,
      ESCRITURACAPITALHUMANO: false,
      ESCRITURABAJAEMPLEADOS: false,
      ESCRITURAALTADEIMSS: false,
      ESCRITURACATALOGOS: false,
      ESCRITURAAREAS: false,
      ESCRITURABANCO: false,
      ESCRITURABOLSADETRABAJO: false,
      ESCRITURACOMPETENCIAS: false,
      ESCRITURACOMPETENCIASCANDIDATO: false,
      ESCRITURAEMPRESA: false,
      ESCRITURAESCOLARIDAD: false,
      ESCRITURAESTADODEESCOLARIDAD: false,
      ESCRITURAESTADOCIVIL: false,
      ESCRITURAMEDIODETRANSPORTE: false,
      ESCRITURAMOTIVOSDEBAJA: false,
      ESCRITURAPAISES: false,
      ESCRITURAPUESTO: false,
      ESCRITURATIPODEPUESTO: false,
      ESCRITURARAZONSOCIAL: false,
      ESCRITURATURNO: false,
      ESCRITURARECURSOSHUMANOS: false,
      ESCRITURAREPORTESRH: false,
      ESCRITURAREPORTEPRECANDIDATO: false,
      ESCRITURAREPORTECANDIDATO: false,
      ESCRITURAREPORTECAPACITACION: false,
      ESCRITURAREPORTETEORICO: false,
      ESCRITURAREPORTEEMPLEADO: false,
      ESCRITURAREPORTECAPITALHUMANO: false,
      ESCRITURAPREGUNTAS: false,
      ESCRITURAGENERARPREGUNTAS: false,
      ESCRITURAGENERARRESPUESTAS: false,
      ESCRITURARESULTADOS: false,
      // ti - escritura
      ESCRITURATI: false,
      ESCRITURAADMINISTRACIONTI: false,
      ESCRITURAGRUPOPERMISOS: false,
      ESCRITURAREACTIVARPERMISOS: false,
      ESCRITURAUSUARIOSTI: false,
      ESCRITURAEXTENSIONES: false,
      ESCRITURAASIGNACIONES: false,
      ESCRITURAUSUARIOASIGNACIONES: false,
      ESCRITURASUPERVISOR: false,
      ESCRITURASOPORTE: false,
      ESCRITURAINVENTARIOUSUARIO: false,
      ESCRITURAINVENTARIOREDES: false,
      ESCRITURAINVENTARIOSOPORTE: false,
      ESCRITURABAJASINVENTARIOUSUARIO: false,
      ESCRITURABAJASINVENTARIOREDES: false,
      ESCRITURABAJASINVENTARIOSOPORTE: false,
      ESCRITURACATALOGOSSOPORTE: false,
      ESCRITURAESTADODEINVENTARIO: false,
      ESCRITURAOBSERVACIONESDEINVENTARIO: false,
      ESCRITURAMARCAS: false,
      ESCRITURAPROCESADORES: false,
      ESCRITURAREPORTESTI: false,
      ESCRITURAREPORTEGRUPOPERMISO: false,
      ESCRITURAREPORTEUSUARIOS: false,
      ESCRITURAREPORTEEXTENSIONES: false,
      ESCRITURAREPORTESOPORTETI: false,
      ESCRITURAREPORTEINVENTARIOUSUARIO: false,
      ESCRITURAREPORTEINVENTARIOREDES: false,
      ESCRITURAREPORTEINVENTARIOSOPORTE: false,
      ESCRITURACONTROLLLAMADAS: false,
      // comercial - escritura
      ESCRITURACOMERCIAL: false,
      ESCRITURASOCIOS: false,
      ESCRITURAVERSOCIOS: false,
      ESCRITURACONTACTO: false,
      ESCRITURAURL: false,
      ESCRITURAOBJETIVODEVENTAS: false,
      ESCRITURACONTRATO: false,
      ESCRITURARAMO: false,
      ESCRITURASUBRAMO: false,
      ESCRITURAVERSUBRAMO: false,
      ESCRITURACONVENIOS: false,
      ESCRITURAPRODUCTO: false,
      ESCRITURAVERPRODUCTO: false,
      ESCRITURACOMPETENCIAPRODUCTO: false,
      ESCRITURAANALISISCOMPETENCIA: false,
      ESCRITURACOBERTURA: false,
      ESCRITURACATALOGOSCOMERCIAL: false,
      ESCRITURATIPOGIRO: false,
      ESCRITURATIPOCATEGORIA: false,
      ESCRITURATIPOCOMPETENCIA: false,
      ESCRITURATIPOPRODUCTO: false,
      ESCRITURADIVISAS: false,
      ESCRITURACATEGORIA: false,
      ESCRITURASUBCATEGORIA: false,
      ESCRITURATIPOCOBERTURA: false,
      ESCRITURABAJASSOCIOS: false,
      ESCRITURAREPORTESCOMERCIAL: false,
      ESCRITURAREPORTESOCIOS: false,
      // marketing - escritura
      ESCRITURAMARKETING: false,
      ESCRITURACAMPAÑA: false,
      ESCRITURAVERCAMPAÑA: false,
      ESCRITURAMEDIODIFUSION: false,
      ESCRITURACAMPAÑACATEGORIA: false,
      ESCRITURAPAGINAS: false,
      ESCRITURACATALOGOSMARKETING: false,
      ESCRITURATIPOPAGINAS: false,
      ESCRITURAPROVEDORLEADS: false,
    }); // formGroup
  }

  getGrupo() {
    this.grupoService.get().pipe(map(result => {
          return result.filter(valor => valor.activo === 1);
        })).subscribe(data => {
          this.Grupos = data;
          if (this.Grupos.length === 0) {
            this.mostrarGrupo = true;
          } else {
            this.mostrarGrupo = false;
          }
     });
  }

  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
          return result.filter(
            valor => valor.activo === 1 && valor.idGrupo === idGrupo,
          );
      })).subscribe(data => {
        this.empresas = data;
        if (this.empresas.length === 0) {
          this.mostrarEmpresa = true;
        } else {
          this.mostrarEmpresa = false;
        }
      });
  }

  getSedes(idEmpresa) {
    this.sedesService
      .get()
      .pipe(
        map(result => {
          return result.filter(valor => valor.idEmpresa === idEmpresa);
        })).subscribe(data => {
          this.Sedes = data;
          if (this.Sedes.length === 0) {
            this.mostrarSede = true;
          } else {
            this.mostrarEmpresa = false;
          }
      });
  }
  getArea(idSede) {
    this.areaService.get().pipe(
        map(result => {
          return result.filter(
            valor =>
            valor.activo === 1 &&
            valor.idSede === idSede,
          );
        })).subscribe(data => {
        this.areas = data;
        if (this.areas.length === 0) {
          this.mostrarArea = true;
        } else {
          this.mostrarArea = false;
        }
      });
  }

  getSubarea(idArea) {
    this.subareaService.get().pipe(
        map(result => {
          return result.filter(
            valor => valor.activo === 1 && valor.idArea === idArea,
          );
        })).subscribe(data => {
        this.subareas = data;
        if (this.subareas.length === 0) {
          this.mostrarSubarea = true;
        } else {
          this.mostrarSubarea = false;
        }
      });
  }

  getPuesto() {
    this.puestoService.get().pipe(map(result => {
          return result.filter(
            valor => valor.activo === 1,
          );
        }),
      ).subscribe(data => {
        this.puestos = data;

        if (this.puestos.length === 0) {
          this.mostrarPuesto = true;
        } else {
          this.mostrarPuesto = false;
        }
      });
  }

  getPermisosVisualizacion() {
    this.permisosVisualizacionService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.permisosVisualizacion = data;
      if (this.permisosVisualizacion.length === 0) {
        this.mostrarPermisoVisualizacion = true;
      } else {
        this.mostrarPermisoVisualizacion = false;
      }
    });
  }

  /** obtiene la informacion de un grupo por medio de su id*/
  getGrupoById() {
    this.idgrupo = Number(this.idgrupo);
    this.grupoUsuariosService.getByID(this.idgrupo).subscribe(data => {
      this.grupoUsuario = data;
      this.idPuesto = Number(this.idPuesto);
      this.idSubarea = Number(this.idSubarea);
      this.permisosdat = JSON.parse(this.grupoUsuario.permisos);
      this.botones2 = JSON.stringify(this.permisosdat.botones);
      this.botonesultimo = JSON.parse(this.botones2);
      this.modulos = JSON.stringify(this.permisosdat.modulos);
      this.modulospar = JSON.parse(this.modulos); // todos los modulos

      // permisos rh
      this.camposRH = Permiso.permisosRH_camel;
      this.permisosRH = _.pick(this.modulospar, this.camposRH);


      // permisos ti
      this.camposTI = Permiso.permisosTI_camel;
      this.permisosTI = _.pick(this.modulospar, this.camposTI);

      // permisos marketing
      this.camposM = Permiso.permisosM_camel;
      this.permisosM = _.pick(this.modulospar, this.camposM);

      // permisos comercial
      this.camposC = Permiso.permisosCOMERCIAL_camel;
      this.permisosC = _.pick(this.modulospar, this.camposC);

      this.getPuesto();
      this.getPermisosVisualizacion();
      this.permisosForm.controls['idPuesto'].setValue(this.idPuesto);
      this.permisosForm.controls['idPermisosVisualizacion'].setValue(data.idPermisosVisualizacion);

      /**toma los valores del json de permisos y los pone en el formulario */
      this.permisosForm.controls['nombre'].setValue(
        this.grupoUsuario.nombre);
      this.permisosForm.controls['descripcion'].setValue(
        this.grupoUsuario.descripcion,
      );
      this.permisosForm.controls['idGrupo'].setValue(
        this.grupoUsuario.idGrupo,
      );
      this.permisosForm.controls['idSubarea'].setValue(
        this.idSubarea,
      );
      this.permisosForm.controls['idPuesto'].setValue(
        this.idPuesto,
      );
      this.permisosForm.controls['descargarPNC'].setValue(
        this.botonesultimo.descargaPNC,
      );
      this.permisosForm.controls['descargarPNE'].setValue(
        this.botonesultimo.descargaPNE,
      );

      // RH
      this.permisosForm.controls['ACTIVORECLUTAMIENTO'].setValue(
        this.permisosRH.reclutamiento.activo,
      );
      this.permisosForm.controls['ESCRITURARECLUTAMIENTO'].setValue(
        this.permisosRH.reclutamiento.escritura,
      ),
      this.permisosForm.controls['ACTIVOSOLICITUDES'].setValue(
        this.permisosRH.solicitudes.activo,
      );
      this.permisosForm.controls['ESCRITURASOLICITUDES'].setValue(
        this.permisosRH.solicitudes.escritura,
      );
      this.permisosForm.controls['ACTIVOPRECANDIDATOS'].setValue(
        this.permisosRH.precandidatos.activo,
      );
      this.permisosForm.controls['ESCRITURAPRECANDIDATOS'].setValue(
        this.permisosRH.precandidatos.escritura,
      );
      this.permisosForm.controls['ACTIVOCANDIDATO'].setValue(
        this.permisosRH.candidato.activo,
      );
      this.permisosForm.controls['ESCRITURACANDIDATO'].setValue(
        this.permisosRH.candidato.escritura,
      );
      this.permisosForm.controls['ACTIVOEMPLEADOS'].setValue(
        this.permisosRH.empleados.activo,
      );
      this.permisosForm.controls['ESCRITURAEMPLEADOS'].setValue(
        this.permisosRH.empleados.escritura,
      );
      this.permisosForm.controls['ACTIVOTEORICO'].setValue(
        this.permisosRH.teorico.activo,
      );
      this.permisosForm.controls['ESCRITURATEORICO'].setValue(
        this.permisosRH.teorico.escritura,
      );
      this.permisosForm.controls['ACTIVOROLEPLAY'].setValue(
        this.permisosRH.roleplay.activo,
      );
      this.permisosForm.controls['ESCRITURAROLEPLAY'].setValue(
        this.permisosRH.roleplay.escritura,
      );
      this.permisosForm.controls['ACTIVOVENTANUEVA'].setValue(
        this.permisosRH.ventaNueva.activo,
      );
      this.permisosForm.controls['ESCRITURAVENTANUEVA'].setValue(
        this.permisosRH.ventaNueva.escritura,
      );
      this.permisosForm.controls['ACTIVOPREEMPLEADOS'].setValue(
        this.permisosRH.preEmpleado.activo,
      );
      this.permisosForm.controls['ESCRITURAPREEMPLEADOS'].setValue(
        this.permisosRH.preEmpleado.escritura,
      );
      this.permisosForm.controls['ACTIVOVACANTES'].setValue(
        this.permisosRH.vacantes.activo,
      );
      this.permisosForm.controls['ESCRITURAVACANTES'].setValue(
        this.permisosRH.vacantes.escritura,
      );
      this.permisosForm.controls['ACTIVOSEGUIMIENTO'].setValue(
        this.permisosRH.seguimiento.activo,
      );
      this.permisosForm.controls['ESCRITURASEGUIMIENTO'].setValue(
        this.permisosRH.seguimiento.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJAS'].setValue(
        this.permisosRH.bajas.activo,
      );
      this.permisosForm.controls['ESCRITURABAJAS'].setValue(
        this.permisosRH.bajas.escritura,
      );
      this.permisosForm.controls['ACTIVOCAPITALHUMANO'].setValue(
        this.permisosRH.capitalHumano.activo,
      );
      this.permisosForm.controls['ESCRITURACAPITALHUMANO'].setValue(
        this.permisosRH.capitalHumano.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJAEMPLEADOS'].setValue(
        this.permisosRH.bajaEmpleados.activo,
      );
      this.permisosForm.controls['ESCRITURABAJAEMPLEADOS'].setValue(
        this.permisosRH.bajaEmpleados.escritura,
      );
      this.permisosForm.controls['ACTIVOALTADEIMSS'].setValue(
        this.permisosRH.altaDeIMSS.activo,
      );
      this.permisosForm.controls['ESCRITURAALTADEIMSS'].setValue(
        this.permisosRH.altaDeIMSS.escritura,
      );
      this.permisosForm.controls['ACTIVOCATALOGOS'].setValue(
        this.permisosRH.catalogos.activo,
      );
      this.permisosForm.controls['ESCRITURACATALOGOS'].setValue(
        this.permisosRH.catalogos.escritura,
      );
      this.permisosForm.controls['ACTIVOAREAS'].setValue(
        this.permisosRH.areas.activo,
      );
      this.permisosForm.controls['ESCRITURAAREAS'].setValue(
        this.permisosRH.areas.escritura,
      );
      this.permisosForm.controls['ACTIVOBANCO'].setValue(
        this.permisosRH.banco.activo,
      );
      this.permisosForm.controls['ESCRITURABANCO'].setValue(
        this.permisosRH.banco.escritura,
      );
      this.permisosForm.controls['ACTIVOBOLSADETRABAJO'].setValue(
        this.permisosRH.bolsaDeTrabajo.activo,
      );
      this.permisosForm.controls['ESCRITURABOLSADETRABAJO'].setValue(
        this.permisosRH.bolsaDeTrabajo.escritura,
      );
      this.permisosForm.controls['ACTIVOCOMPETENCIAS'].setValue(
        this.permisosRH.competencias.activo,
      );
      this.permisosForm.controls['ESCRITURACOMPETENCIAS'].setValue(
        this.permisosRH.competencias.escritura,
      );
      this.permisosForm.controls['ACTIVOCOMPETENCIASCANDIDATO'].setValue(
        this.permisosRH.competenciasCandidato.activo,
      );
      this.permisosForm.controls['ESCRITURACOMPETENCIASCANDIDATO'].setValue(
        this.permisosRH.competenciasCandidato.escritura,
      );
      this.permisosForm.controls['ACTIVOEMPRESA'].setValue(
        this.permisosRH.empresa.activo,
      );
      this.permisosForm.controls['ESCRITURAEMPRESA'].setValue(
        this.permisosRH.empresa.escritura,
      );
      this.permisosForm.controls['ACTIVOESCOLARIDAD'].setValue(
        this.permisosRH.escolaridad.activo,
      );
      this.permisosForm.controls['ESCRITURAESCOLARIDAD'].setValue(
        this.permisosRH.escolaridad.escritura,
      );
      this.permisosForm.controls['ACTIVOESTADODEESCOLARIDAD'].setValue(
        this.permisosRH.estadoDeEscolaridad.activo,
      );
      this.permisosForm.controls['ESCRITURAESTADODEESCOLARIDAD'].setValue(
        this.permisosRH.estadoDeEscolaridad.escritura,
      );
      this.permisosForm.controls['ACTIVOESTADOCIVIL'].setValue(
        this.permisosRH.estadoCivil.activo,
      );
      this.permisosForm.controls['ESCRITURAESTADOCIVIL'].setValue(
        this.permisosRH.estadoCivil.escritura,
      );
      this.permisosForm.controls['ACTIVOMEDIODETRANSPORTE'].setValue(
        this.permisosRH.medioDeTransporte.activo,
      );
      this.permisosForm.controls['ESCRITURAMEDIODETRANSPORTE'].setValue(
        this.permisosRH.medioDeTransporte.escritura,
      );
      this.permisosForm.controls['ACTIVOMOTIVOSDEBAJA'].setValue(
        this.permisosRH.motivosDeBaja.activo,
      );
      this.permisosForm.controls['ESCRITURAMOTIVOSDEBAJA'].setValue(
        this.permisosRH.motivosDeBaja.escritura,
      );
      this.permisosForm.controls['ACTIVOPAISES'].setValue(
        this.permisosRH.paises.activo,
      );
      this.permisosForm.controls['ESCRITURAPAISES'].setValue(
        this.permisosRH.paises.escritura,
      );
      this.permisosForm.controls['ACTIVOPUESTO'].setValue(
        this.permisosRH.puesto.activo,
      );
      this.permisosForm.controls['ESCRITURAPUESTO'].setValue(
        this.permisosRH.puesto.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPODEPUESTO'].setValue(
        this.permisosRH.tipoDePuesto.activo,
      );
      this.permisosForm.controls['ESCRITURATIPODEPUESTO'].setValue(
        this.permisosRH.tipoDePuesto.escritura,
      );
      this.permisosForm.controls['ACTIVORAZONSOCIAL'].setValue(
        this.permisosRH.razonSocial.activo,
      );
      this.permisosForm.controls['ESCRITURARAZONSOCIAL'].setValue(
        this.permisosRH.razonSocial.escritura,
      );
      this.permisosForm.controls['ACTIVOTURNO'].setValue(
        this.permisosRH.turno.activo,
      );
      this.permisosForm.controls['ESCRITURATURNO'].setValue(
        this.permisosRH.turno.escritura,
      );
      this.permisosForm.controls['ACTIVORECURSOSHUMANOS'].setValue(
        this.permisosRH.recursosHumanos.activo,
      );
      this.permisosForm.controls['ESCRITURARECURSOSHUMANOS'].setValue(
        this.permisosRH.recursosHumanos.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTESRH'].setValue(
        this.permisosRH.reportesRH.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTESRH'].setValue(
        this.permisosRH.reportesRH.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEPRECANDIDATO'].setValue(
        this.permisosRH.reportePrecandidato.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEPRECANDIDATO'].setValue(
        this.permisosRH.reportePrecandidato.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTECANDIDATO'].setValue(
        this.permisosRH.reporteCandidato.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTECANDIDATO'].setValue(
        this.permisosRH.reporteCandidato.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTECAPACITACION'].setValue(
        this.permisosRH.reporteCapacitacion.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTECAPACITACION'].setValue(
        this.permisosRH.reporteCapacitacion.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTETEORICO'].setValue(
        this.permisosRH.reporteTeorico.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTETEORICO'].setValue(
        this.permisosRH.reporteTeorico.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEEMPLEADO'].setValue(
        this.permisosRH.reporteEmpleado.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEEMPLEADO'].setValue(
        this.permisosRH.reporteEmpleado.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTECAPITALHUMANO'].setValue(
        this.permisosRH.reporteCapitalHumano.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTECAPITALHUMANO'].setValue(
        this.permisosRH.reporteCapitalHumano.escritura,
      );
      this.permisosForm.controls['ACTIVOPREGUNTAS'].setValue(
        this.permisosRH.preguntas.activo,
      );
      this.permisosForm.controls['ESCRITURAPREGUNTAS'].setValue(
        this.permisosRH.preguntas.escritura,
      );
      this.permisosForm.controls['ACTIVOGENERARPREGUNTAS'].setValue(
        this.permisosRH.generarPreguntas.activo,
      );
      this.permisosForm.controls['ESCRITURAGENERARPREGUNTAS'].setValue(
        this.permisosRH.generarPreguntas.escritura,
      );
      this.permisosForm.controls['ACTIVOGENERARRESPUESTAS'].setValue(
        this.permisosRH.generarRespuestas.activo,
      );
      this.permisosForm.controls['ESCRITURAGENERARRESPUESTAS'].setValue(
        this.permisosRH.generarRespuestas.escritura,
      );
      this.permisosForm.controls['ACTIVORESULTADOS'].setValue(
        this.permisosRH.generarRespuestas.activo,
      );
      this.permisosForm.controls['ESCRITURARESULTADOS'].setValue(
        this.permisosRH.generarRespuestas.escritura,
      );

      // TI
      this.permisosForm.controls['ACTIVOTI'].setValue(
        this.permisosTI.ti.activo,
      );
      this.permisosForm.controls['ESCRITURATI'].setValue(
        this.permisosTI.ti.escritura,
      );
      this.permisosForm.controls['ACTIVOADMINISTRACIONTI'].setValue(
        this.permisosTI.administracionTi.activo,
      );
      this.permisosForm.controls['ESCRITURAADMINISTRACIONTI'].setValue(
        this.permisosTI.administracionTi.escritura,
      );
      this.permisosForm.controls['ACTIVOGRUPOPERMISOS'].setValue(
        this.permisosTI.grupoPermisos.activo,
      );
      this.permisosForm.controls['ESCRITURAGRUPOPERMISOS'].setValue(
        this.permisosTI.grupoPermisos.escritura,
      );
      this.permisosForm.controls['ACTIVOREACTIVARPERMISOS'].setValue(
        this.permisosTI.reactivarPermisos.activo,
      );
      this.permisosForm.controls['ESCRITURAREACTIVARPERMISOS'].setValue(
        this.permisosTI.reactivarPermisos.escritura,
      );
      this.permisosForm.controls['ACTIVOUSUARIOSTI'].setValue(
        this.permisosTI.usuariosTi.activo,
      );
      this.permisosForm.controls['ESCRITURAUSUARIOSTI'].setValue(
        this.permisosTI.usuariosTi.escritura,
      );
      this.permisosForm.controls['ACTIVOEXTENSIONES'].setValue(
        this.permisosTI.extensiones.activo,
      );
      this.permisosForm.controls['ESCRITURAEXTENSIONES'].setValue(
        this.permisosTI.extensiones.escritura,
      );
      this.permisosForm.controls['ACTIVOASIGNACIONES'].setValue(
        this.permisosTI.asignaciones.activo,
      );
      this.permisosForm.controls['ESCRITURAASIGNACIONES'].setValue(
        this.permisosTI.asignaciones.escritura,
      );
      this.permisosForm.controls['ACTIVOUSUARIOASIGNACIONES'].setValue(
        this.permisosTI.usuarioAsignaciones.activo,
      );
      this.permisosForm.controls['ESCRITURAUSUARIOASIGNACIONES'].setValue(
        this.permisosTI.usuarioAsignaciones.escritura,
      );
      this.permisosForm.controls['ACTIVOSUPERVISOR'].setValue(
        this.permisosTI.supervisor.activo,
      );
      this.permisosForm.controls['ESCRITURASUPERVISOR'].setValue(
        this.permisosTI.supervisor.escritura,
      );
      this.permisosForm.controls['ACTIVOSOPORTE'].setValue(
        this.permisosTI.soporte.activo,
      );
      this.permisosForm.controls['ESCRITURASOPORTE'].setValue(
        this.permisosTI.soporte.escritura,
      );
      this.permisosForm.controls['ACTIVOINVENTARIOUSUARIO'].setValue(
        this.permisosTI.inventarioUsuario.activo,
      );
      this.permisosForm.controls['ESCRITURAINVENTARIOUSUARIO'].setValue(
        this.permisosTI.inventarioUsuario.escritura,
      );
      this.permisosForm.controls['ACTIVOINVENTARIOREDES'].setValue(
        this.permisosTI.inventarioRedes.activo,
      );
      this.permisosForm.controls['ESCRITURAINVENTARIOREDES'].setValue(
        this.permisosTI.inventarioRedes.escritura,
      );
      this.permisosForm.controls['ACTIVOINVENTARIOSOPORTE'].setValue(
        this.permisosTI.inventarioSoporte.activo,
      );
      this.permisosForm.controls['ESCRITURAINVENTARIOSOPORTE'].setValue(
        this.permisosTI.inventarioSoporte.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJASINVENTARIOUSUARIO'].setValue(
        this.permisosTI.bajasInventarioUsuario.activo,
      );
      this.permisosForm.controls['ESCRITURABAJASINVENTARIOUSUARIO'].setValue(
        this.permisosTI.bajasInventarioUsuario.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJASINVENTARIOREDES'].setValue(
        this.permisosTI.bajasInventarioRedes.activo,
      );
      this.permisosForm.controls['ESCRITURABAJASINVENTARIOREDES'].setValue(
        this.permisosTI.bajasInventarioRedes.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJASINVENTARIOSOPORTE'].setValue(
        this.permisosTI.bajasInventarioSoporte.activo,
      );
      this.permisosForm.controls['ESCRITURABAJASINVENTARIOSOPORTE'].setValue(
        this.permisosTI.bajasInventarioSoporte.escritura,
      );
      this.permisosForm.controls['ACTIVOCATALOGOSSOPORTE'].setValue(
        this.permisosTI.catalogosSoporte.activo,
      );
      this.permisosForm.controls['ESCRITURACATALOGOSSOPORTE'].setValue(
        this.permisosTI.catalogosSoporte.escritura,
      );
      this.permisosForm.controls['ACTIVOESTADODEINVENTARIO'].setValue(
        this.permisosTI.estadoInventario.activo,
      );
      this.permisosForm.controls['ESCRITURAESTADODEINVENTARIO'].setValue(
        this.permisosTI.estadoInventario.escritura,
      );
      this.permisosForm.controls['ACTIVOOBSERVACIONESDEINVENTARIO'].setValue(
        this.permisosTI.observacionesInventario.activo,
      );
      this.permisosForm.controls['ESCRITURAOBSERVACIONESDEINVENTARIO'].setValue(
        this.permisosTI.observacionesInventario.escritura,
      );
      this.permisosForm.controls['ACTIVOMARCAS'].setValue(
        this.permisosTI.marcas.activo,
      );
      this.permisosForm.controls['ESCRITURAMARCAS'].setValue(
        this.permisosTI.marcas.escritura,
      );
      this.permisosForm.controls['ACTIVOPROCESADORES'].setValue(
        this.permisosTI.procesadores.activo,
      );
      this.permisosForm.controls['ESCRITURAPROCESADORES'].setValue(
        this.permisosTI.procesadores.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTESTI'].setValue(
        this.permisosTI.reportesTI.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTESTI'].setValue(
        this.permisosTI.reportesTI.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEGRUPOPERMISO'].setValue(
        this.permisosTI.reporteGrupoPermiso.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEGRUPOPERMISO'].setValue(
        this.permisosTI.reporteGrupoPermiso.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEUSUARIOS'].setValue(
        this.permisosTI.reporteUsuarios.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEUSUARIOS'].setValue(
        this.permisosTI.reporteUsuarios.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEEXTENSIONES'].setValue(
        this.permisosTI.reporteExtensiones.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEEXTENSIONES'].setValue(
        this.permisosTI.reporteExtensiones.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTESOPORTETI'].setValue(
        this.permisosTI.reporteSoporteTi.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTESOPORTETI'].setValue(
        this.permisosTI.reporteSoporteTi.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEINVENTARIOUSUARIO'].setValue(
        this.permisosTI.reporteInventarioUsuario.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEINVENTARIOUSUARIO'].setValue(
        this.permisosTI.reporteInventarioUsuario.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEINVENTARIOREDES'].setValue(
        this.permisosTI.reporteInventarioRedes.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEINVENTARIOREDES'].setValue(
        this.permisosTI.reporteInventarioRedes.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTEINVENTARIOSOPORTE'].setValue(
        this.permisosTI.reporteInventarioSoporte.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTEINVENTARIOSOPORTE'].setValue(
        this.permisosTI.reporteInventarioSoporte.escritura,
      );
      this.permisosForm.controls['ACTIVOCONTROLLLAMADAS'].setValue(
        this.permisosTI.controlLlamadas.activo,
      );
      this.permisosForm.controls['ESCRITURACONTROLLLAMADAS'].setValue(
        this.permisosTI.controlLlamadas.escritura,
      );

      // MARKETING
      this.permisosForm.controls['ACTIVOMARKETING'].setValue(
        this.permisosM.marketing.activo,
      );
      this.permisosForm.controls['ESCRITURAMARKETING'].setValue(
        this.permisosM.marketing.escritura,
      );
      this.permisosForm.controls['ACTIVOCAMPAÑA'].setValue(
        this.permisosM.campaña.activo,
      );
      this.permisosForm.controls['ESCRITURACAMPAÑA'].setValue(
        this.permisosM.campaña.escritura,
      );
      this.permisosForm.controls['ACTIVOVERCAMPAÑA'].setValue(
        this.permisosM.verCampaña.activo,
      );
      this.permisosForm.controls['ESCRITURAVERCAMPAÑA'].setValue(
        this.permisosM.verCampaña.escritura,
      );
      this.permisosForm.controls['ACTIVOMEDIODIFUSION'].setValue(
        this.permisosM.medioDifusion.activo,
      );
      this.permisosForm.controls['ESCRITURAMEDIODIFUSION'].setValue(
        this.permisosM.medioDifusion.escritura,
      );
      this.permisosForm.controls['ACTIVOCAMPAÑACATEGORIA'].setValue(
        this.permisosM.campañaCategoria.activo,
      );
      this.permisosForm.controls['ESCRITURACAMPAÑACATEGORIA'].setValue(
        this.permisosM.campañaCategoria.escritura,
      );
      this.permisosForm.controls['ACTIVOPAGINAS'].setValue(
        this.permisosM.paginas.activo,
      );
      this.permisosForm.controls['ESCRITURAPAGINAS'].setValue(
        this.permisosM.paginas.escritura,
      );
      this.permisosForm.controls['ACTIVOCATALOGOSMARKETING'].setValue(
        this.permisosM.catalogosMarketing.activo,
      );
      this.permisosForm.controls['ESCRITURACATALOGOSMARKETING'].setValue(
        this.permisosM.catalogosMarketing.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOPAGINAS'].setValue(
        this.permisosM.tipoPaginas.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOPAGINAS'].setValue(
        this.permisosM.tipoPaginas.escritura,
      );
      this.permisosForm.controls['ACTIVOPROVEDORLEADS'].setValue(
        this.permisosM.provedorLeads.activo,
      );
      this.permisosForm.controls['ESCRITURAPROVEDORLEADS'].setValue(
        this.permisosM.provedorLeads.escritura,
      );
      // COMERCIAL
      this.permisosForm.controls['ACTIVOCOMERCIAL'].setValue(
        this.modulospar.comercial.activo,
      );
      this.permisosForm.controls['ESCRITURACOMERCIAL'].setValue(
        this.modulospar.comercial.escritura,
      );
      this.permisosForm.controls['ACTIVOSOCIOS'].setValue(
        this.modulospar.socios.activo,
      );
      this.permisosForm.controls['ESCRITURASOCIOS'].setValue(
        this.modulospar.socios.escritura,
      );
      this.permisosForm.controls['ACTIVOVERSOCIOS'].setValue(
        this.modulospar.verSocios.activo,
      );
      this.permisosForm.controls['ESCRITURAVERSOCIOS'].setValue(
        this.modulospar.verSocios.escritura,
      );
      this.permisosForm.controls['ACTIVOCONTACTO'].setValue(
        this.modulospar.contacto.activo,
      );
      this.permisosForm.controls['ESCRITURACONTACTO'].setValue(
        this.modulospar.contacto.escritura,
      );
      this.permisosForm.controls['ACTIVOURL'].setValue(
        this.modulospar.url.activo,
      );
      this.permisosForm.controls['ESCRITURAURL'].setValue(
        this.modulospar.url.escritura,
      );
      this.permisosForm.controls['ACTIVOOBJETIVODEVENTAS'].setValue(
        this.modulospar.objetivoDeVentas.activo,
      );
      this.permisosForm.controls['ESCRITURAOBJETIVODEVENTAS'].setValue(
        this.modulospar.objetivoDeVentas.escritura,
      );
      this.permisosForm.controls['ACTIVOCONTRATO'].setValue(
        this.modulospar.contrato.activo,
      );
      this.permisosForm.controls['ESCRITURACONTRATO'].setValue(
        this.modulospar.contrato.escritura,
      );
      this.permisosForm.controls['ACTIVORAMO'].setValue(
        this.modulospar.ramo.activo,
      );
      this.permisosForm.controls['ESCRITURARAMO'].setValue(
        this.modulospar.ramo.escritura,
      );
      this.permisosForm.controls['ACTIVOSUBRAMO'].setValue(
        this.modulospar.subRamo.activo,
      );
      this.permisosForm.controls['ESCRITURASUBRAMO'].setValue(
        this.modulospar.subRamo.escritura,
      );
      this.permisosForm.controls['ACTIVOVERSUBRAMO'].setValue(
        this.modulospar.verSubRamo.activo,
      );
      this.permisosForm.controls['ESCRITURAVERSUBRAMO'].setValue(
        this.modulospar.verSubRamo.escritura,
      );
      this.permisosForm.controls['ACTIVOCONVENIOS'].setValue(
        this.modulospar.convenios.activo,
      );
      this.permisosForm.controls['ESCRITURACONVENIOS'].setValue(
        this.modulospar.convenios.escritura,
      );
      this.permisosForm.controls['ACTIVOPRODUCTO'].setValue(
        this.modulospar.producto.activo,
      );
      this.permisosForm.controls['ESCRITURAPRODUCTO'].setValue(
        this.modulospar.producto.escritura,
      );
      this.permisosForm.controls['ACTIVOVERPRODUCTO'].setValue(
        this.modulospar.verProducto.activo,
      );
      this.permisosForm.controls['ESCRITURAVERPRODUCTO'].setValue(
        this.modulospar.verProducto.escritura,
      );
      this.permisosForm.controls['ACTIVOCOMPETENCIAPRODUCTO'].setValue(
        this.modulospar.competenciaProducto.activo,
      );
      this.permisosForm.controls['ESCRITURACOMPETENCIAPRODUCTO'].setValue(
        this.modulospar.competenciaProducto.escritura,
      );
      this.permisosForm.controls['ACTIVOANALISISCOMPETENCIA'].setValue(
        this.modulospar.analisisCompetencia.activo,
      );
      this.permisosForm.controls['ESCRITURAANALISISCOMPETENCIA'].setValue(
        this.modulospar.analisisCompetencia.escritura,
      );
      this.permisosForm.controls['ACTIVOCOBERTURA'].setValue(
        this.modulospar.cobertura.activo,
      );
      this.permisosForm.controls['ESCRITURACOBERTURA'].setValue(
        this.modulospar.cobertura.escritura,
      );
      this.permisosForm.controls['ACTIVOCATALOGOSCOMERCIAL'].setValue(
        this.modulospar.catalogosComercial.activo,
      );
      this.permisosForm.controls['ESCRITURACATALOGOSCOMERCIAL'].setValue(
        this.modulospar.catalogosComercial.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOGIRO'].setValue(
        this.modulospar.tipoGiro.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOGIRO'].setValue(
        this.modulospar.tipoGiro.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOCATEGORIA'].setValue(
        this.modulospar.tipoCategoria.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOCATEGORIA'].setValue(
        this.modulospar.tipoCategoria.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOCOMPETENCIA'].setValue(
        this.modulospar.tipoCompetencia.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOCOMPETENCIA'].setValue(
        this.modulospar.tipoCompetencia.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOPRODUCTO'].setValue(
        this.modulospar.tipoProducto.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOPRODUCTO'].setValue(
        this.modulospar.tipoProducto.escritura,
      );
      this.permisosForm.controls['ACTIVODIVISAS'].setValue(
        this.modulospar.divisas.activo,
      );
      this.permisosForm.controls['ESCRITURADIVISAS'].setValue(
        this.modulospar.divisas.escritura,
      );
      this.permisosForm.controls['ACTIVOCATEGORIA'].setValue(
        this.modulospar.categoria.activo,
      );
      this.permisosForm.controls['ESCRITURACATEGORIA'].setValue(
        this.modulospar.categoria.escritura,
      );
      this.permisosForm.controls['ACTIVOSUBCATEGORIA'].setValue(
        this.modulospar.subCategoria.activo,
      );
      this.permisosForm.controls['ESCRITURASUBCATEGORIA'].setValue(
        this.modulospar.subCategoria.escritura,
      );
      this.permisosForm.controls['ACTIVOTIPOCOBERTURA'].setValue(
        this.modulospar.tipoCobertura.activo,
      );
      this.permisosForm.controls['ESCRITURATIPOCOBERTURA'].setValue(
        this.modulospar.tipoCobertura.escritura,
      );
      this.permisosForm.controls['ACTIVOBAJASSOCIOS'].setValue(
        this.modulospar.bajasSocios.activo,
      );
      this.permisosForm.controls['ESCRITURABAJASSOCIOS'].setValue(
        this.modulospar.bajasSocios.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTESCOMERCIAL'].setValue(
        this.modulospar.reportesComercial.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTESCOMERCIAL'].setValue(
        this.modulospar.reportesComercial.escritura,
      );
      this.permisosForm.controls['ACTIVOREPORTESCOMERCIAL'].setValue(
        this.modulospar.reporteSocios.activo,
      );
      this.permisosForm.controls['ESCRITURAREPORTESOCIOS'].setValue(
        this.modulospar.reporteSocios.escritura,
      );
      this.permisosForm.controls['idSubarea'].setValue(this.idSubarea);
      this.subareaService.getById(this.idSubarea).subscribe(data2 => {
        this.areaget = data2;

        this.getSubarea(this.areaget.idArea);

        this.areaService.getAreaById(this.areaget.idArea).subscribe(data3 => {
          this.sedeget = data3;
          this.getArea(this.sedeget.idSede);
          this.permisosForm.controls['idArea'].setValue(this.sedeget.id);

          this.sedesService
            .getSedeById(this.sedeget.idSede)
            .subscribe(data4 => {
              this.empresaget = data4;
              this.getSedes(this.empresaget.idEmpresa);
              this.permisosForm.controls['idSede'].setValue(
                this.empresaget.id,
              );

              this.empresaService
                .getEmpresasById(this.empresaget.idEmpresa)
                .subscribe(data5 => {
                  this.grupoempresarialget = data5;
                  this.getEmpresa(this.grupoempresarialget.idGrupo);
                  this.permisosForm.controls['idEmpresa'].setValue(
                    this.grupoempresarialget.id,
                  );

                  this.grupoService.getGrupoById(this.grupoempresarialget.idGrupo).subscribe(data6 => {
                      this.grupoget = data6;
                      this.getGrupo();
                      this.permisosForm.controls['idGrupo'].setValue(
                        this.grupoget.id,
                      );
                    });
                });
            });
        });
      });
    });
  } // getGrupoById()

  modificarGrupoUsuario() {
    /** constante que guarda los valores de permisos de los modulos */
    this.permiso = {
      botones: {
        descargaPNC: true,
        descargaPNE: true,
      },
      modulos: {
        // rh
        reclutamiento: {
          activo: this.permisosForm.value.ACTIVORECLUTAMIENTO,
          escritura: this.permisosForm.value.ESCRITURARECLUTAMIENTO,
        },
        solicitudes: {
          activo: this.permisosForm.value.ACTIVOSOLICITUDES,
          escritura: this.permisosForm.value.ESCRITURASOLICITUDES,
        },
        precandidatos: {
          activo: this.permisosForm.value.ACTIVOPRECANDIDATOS,
          escritura: this.permisosForm.value.ESCRITURAPRECANDIDATOS,
        },
        candidato: {
          activo: this.permisosForm.value.ACTIVOCANDIDATO,
          escritura: this.permisosForm.value.ESCRITURACANDIDATO,
        },
        empleados: {
          activo: this.permisosForm.value.ACTIVOEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURAEMPLEADOS,
        },
        teorico: {
          activo: this.permisosForm.value.ACTIVOTEORICO,
          escritura: this.permisosForm.value.ESCRITURATEORICO,
        },
        roleplay: {
          activo: this.permisosForm.value.ACTIVOROLEPLAY,
          escritura: this.permisosForm.value.ESCRITURAROLEPLAY,
        },
        ventaNueva: {
          activo: this.permisosForm.value.ACTIVOVENTANUEVA,
          escritura: this.permisosForm.value.ESCRITURAVENTANUEVA,
        },
        preEmpleado: {
          activo: this.permisosForm.value.ACTIVOPREEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURAPREEMPLEADOS,
        },
        vacantes: {
          activo: this.permisosForm.value.ACTIVOVACANTES,
          escritura: this.permisosForm.value.ESCRITURAVACANTES,
        },
        seguimiento: {
          activo: this.permisosForm.value.ACTIVOSEGUIMIENTO,
          escritura: this.permisosForm.value.ESCRITURASEGUIMIENTO,
        },
        bajas: {
          activo: this.permisosForm.value.ACTIVOBAJAS,
          escritura: this.permisosForm.value.ESCRITURABAJAS,
        },
        capitalHumano: {
          activo: this.permisosForm.value.ACTIVOCAPITALHUMANO,
          escritura: this.permisosForm.value.ESCRITURACAPITALHUMANO,
        },
        bajaEmpleados: {
          activo: this.permisosForm.value.ACTIVOBAJAEMPLEADOS,
          escritura: this.permisosForm.value.ESCRITURABAJAEMPLEADOS,
        },
        altaDeIMSS: {
          activo: this.permisosForm.value.ACTIVOALTADEIMSS,
          escritura: this.permisosForm.value.ESCRITURAALTADEIMSS,
        },
        catalogos: {
          activo: this.permisosForm.value.ACTIVOCATALOGOS,
          escritura: this.permisosForm.value.ESCRITURACATALOGOS,
        },
        areas: {
          activo: this.permisosForm.value.ACTIVOAREAS,
          escritura: this.permisosForm.value.ESCRITURAAREAS,
        },
        banco: {
          activo: this.permisosForm.value.ACTIVOBANCO,
          escritura: this.permisosForm.value.ESCRITURABANCO,
        },
        bolsaDeTrabajo: {
          activo: this.permisosForm.value.ACTIVOBOLSADETRABAJO,
          escritura: this.permisosForm.value.ESCRITURABOLSADETRABAJO,
        },
        competencias: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIAS,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIAS,
        },
        competenciasCandidato: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIASCANDIDATO,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIASCANDIDATO,
        },
        empresa: {
          activo: this.permisosForm.value.ACTIVOEMPRESA,
          escritura: this.permisosForm.value.ESCRITURAEMPRESA,
        },
        escolaridad: {
          activo: this.permisosForm.value.ACTIVOESCOLARIDAD,
          escritura: this.permisosForm.value.ESCRITURAESCOLARIDAD,
        },
        estadoDeEscolaridad: {
          activo: this.permisosForm.value.ACTIVOESTADODEESCOLARIDAD,
          escritura: this.permisosForm.value.ESCRITURAESTADODEESCOLARIDAD,
        },
        estadoCivil: {
          activo: this.permisosForm.value.ACTIVOESTADOCIVIL,
          escritura: this.permisosForm.value.ESCRITURAESTADOCIVIL,
        },
        medioDeTransporte: {
          activo: this.permisosForm.value.ACTIVOMEDIODETRANSPORTE,
          escritura: this.permisosForm.value.ESCRITURAMEDIODETRANSPORTE,
        },
        motivosDeBaja: {
          activo: this.permisosForm.value.ACTIVOMOTIVOSDEBAJA,
          escritura: this.permisosForm.value.ESCRITURAMOTIVOSDEBAJA,
        },
        paises: {
          activo: this.permisosForm.value.ACTIVOPAISES,
          escritura: this.permisosForm.value.ESCRITURAPAISES,
        },
        puesto: {
          activo: this.permisosForm.value.ACTIVOPUESTO,
          escritura: this.permisosForm.value.ESCRITURAPUESTO,
        },
        tipoDePuesto: {
          activo: this.permisosForm.value.ACTIVOTIPODEPUESTO,
          escritura: this.permisosForm.value.ESCRITURATIPODEPUESTO,
        },
        razonSocial: {
          activo: this.permisosForm.value.ACTIVORAZONSOCIAL,
          escritura: this.permisosForm.value.ESCRITURARAZONSOCIAL,
        },
        turno: {
          activo: this.permisosForm.value.ACTIVOTURNO,
          escritura: this.permisosForm.value.ESCRITURATURNO,
        },
        recursosHumanos: {
          activo: this.permisosForm.value.ACTIVORECURSOSHUMANOS,
          escritura: this.permisosForm.value.ESCRITURARECURSOSHUMANOS,
        },
        reportesRH: {
          activo: this.permisosForm.value.ACTIVOREPORTESRH,
          escritura: this.permisosForm.value.ESCRITURAREPORTESRH,
        },
        reportePrecandidato: {
          activo: this.permisosForm.value.ACTIVOREPORTEPRECANDIDATO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEPRECANDIDATO,
        },
        reporteCandidato: {
          activo: this.permisosForm.value.ACTIVOREPORTECANDIDATO,
          escritura: this.permisosForm.value.ESCRITURAREPORTECANDIDATO,
        },
        reporteCapacitacion: {
          activo: this.permisosForm.value.ACTIVOREPORTECAPACITACION,
          escritura: this.permisosForm.value.ESCRITURAREPORTECAPACITACION,
        },
        reporteTeorico: {
          activo: this.permisosForm.value.ACTIVOREPORTETEORICO,
          escritura: this.permisosForm.value.ESCRITURAREPORTETEORICO,
        },
        reporteEmpleado: {
          activo: this.permisosForm.value.ACTIVOREPORTEEMPLEADO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEEMPLEADO,
        },
        reporteCapitalHumano: {
          activo: this.permisosForm.value.ACTIVOREPORTECAPITALHUMANO,
          escritura: this.permisosForm.value.ESCRITURAREPORTECAPITALHUMANO,
        },
        preguntas: {
          activo: this.permisosForm.value.ACTIVOPREGUNTAS,
          escritura: this.permisosForm.value.ESCRITURAPREGUNTAS,
        },
        generarPreguntas: {
          activo: this.permisosForm.value.ACTIVOGENERARPREGUNTAS,
          escritura: this.permisosForm.value.ESCRITURAGENERARPREGUNTAS,
        },
        generarRespuestas: {
          activo: this.permisosForm.value.ACTIVOGENERARRESPUESTAS,
          escritura: this.permisosForm.value.ESCRITURAGENERARRESPUESTAS,
        },
        resultados: {
          activo: this.permisosForm.value.ACTIVORESULTADOS,
          escritura: this.permisosForm.value.ESCRITURARESULTADOS,
        },
        // ti
        ti: {
          activo: this.permisosForm.value.ACTIVOTI,
          escritura: this.permisosForm.value.ESCRITURATI,
        },
        administracionTi: {
          activo: this.permisosForm.value.ACTIVOADMINISTRACIONTI,
          escritura: this.permisosForm.value.ESCRITURAADMINISTRACIONTI,
        },
        grupoPermisos: {
          activo: this.permisosForm.value.ACTIVOGRUPOPERMISOS,
          escritura: this.permisosForm.value.ESCRITURAGRUPOPERMISOS,
        },
        reactivarPermisos: {
          activo: this.permisosForm.value.ACTIVOREACTIVARPERMISOS,
          escritura: this.permisosForm.value.ESCRITURAREACTIVARPERMISOS,
        },
        usuariosTi: {
          activo: this.permisosForm.value.ACTIVOUSUARIOSTI,
          escritura: this.permisosForm.value.ESCRITURAUSUARIOSTI,
        },
        extensiones: {
          activo: this.permisosForm.value.ACTIVOEXTENSIONES,
          escritura: this.permisosForm.value.ESCRITURAEXTENSIONES,
        },
        asignaciones: {
          activo: this.permisosForm.value.ACTIVOASIGNACIONES,
          escritura: this.permisosForm.value.ESCRITURAASIGNACIONES,
        },
        usuarioAsignaciones: {
          activo: this.permisosForm.value.ACTIVOUSUARIOASIGNACIONES,
          escritura: this.permisosForm.value.ESCRITURAUSUARIOASIGNACIONES,
        },
        supervisor: {
          activo: this.permisosForm.value.ACTIVOSUPERVISOR,
          escritura: this.permisosForm.value.ESCRITURASUPERVISOR,
        },
        soporte: {
          activo: this.permisosForm.value.ACTIVOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURASOPORTE,
        },
        inventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOUSUARIO,
        },
        inventarioRedes: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOREDES,
        },
        inventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURAINVENTARIOSOPORTE,
        },
        bajasInventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOUSUARIO,
        },
        bajasInventarioRedes: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOREDES,
        },
        bajasInventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOBAJASINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURABAJASINVENTARIOSOPORTE,
        },
        catalogosSoporte: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSSOPORTE,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSSOPORTE,
        },
        estadoInventario: {
          activo: this.permisosForm.value.ACTIVOESTADODEINVENTARIO,
          escritura: this.permisosForm.value.ESCRITURAESTADODEINVENTARIO,
        },
        observacionesInventario: {
          activo: this.permisosForm.value.ACTIVOOBSERVACIONESDEINVENTARIO,
          escritura: this.permisosForm.value.ESCRITURAOBSERVACIONESDEINVENTARIO,
        },
        marcas: {
          activo: this.permisosForm.value.ACTIVOMARCAS,
          escritura: this.permisosForm.value.ESCRITURAMARCAS,
        },
        procesadores: {
          activo: this.permisosForm.value.ACTIVOPROCESADORES,
          escritura: this.permisosForm.value.ESCRITURAPROCESADORES,
        },
        reportesTI: {
          activo: this.permisosForm.value.ACTIVOREPORTESTI,
          escritura: this.permisosForm.value.ESCRITURAREPORTESTI,
        },
        reporteGrupoPermiso: {
          activo: this.permisosForm.value.ACTIVOREPORTEGRUPOPERMISO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEGRUPOPERMISO,
        },
        reporteUsuarios: {
          activo: this.permisosForm.value.ACTIVOREPORTEUSUARIOS,
          escritura: this.permisosForm.value.ESCRITURAREPORTEUSUARIOS,
        },
        reporteExtensiones: {
          activo: this.permisosForm.value.ACTIVOREPORTEEXTENSIONES,
          escritura: this.permisosForm.value.ESCRITURAREPORTEEXTENSIONES,
        },
        reporteSoporteTi: {
          activo: this.permisosForm.value.ACTIVOREPORTESOPORTETI,
          escritura: this.permisosForm.value.ESCRITURAREPORTESOPORTETI,
        },
        reporteInventarioUsuario: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOUSUARIO,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOUSUARIO,
        },
        reporteInventarioRedes: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOREDES,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOREDES,
        },
        reporteInventarioSoporte: {
          activo: this.permisosForm.value.ACTIVOREPORTEINVENTARIOSOPORTE,
          escritura: this.permisosForm.value.ESCRITURAREPORTEINVENTARIOSOPORTE,
        },
        controlLlamadas: {
          activo: this.permisosForm.value.ACTIVOCONTROLLLAMADAS,
          escritura: this.permisosForm.value.ESCRITURACONTROLLLAMADAS,
        },
        // comercial
        comercial: {
          activo: this.permisosForm.value.ACTIVOCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURACOMERCIAL,
        },
        socios: {
          activo: this.permisosForm.value.ACTIVOSOCIOS,
          escritura: this.permisosForm.value.ESCRITURASOCIOS,
        },
        verSocios: {
          activo: this.permisosForm.value.ACTIVOVERSOCIOS,
          escritura: this.permisosForm.value.ESCRITURAVERSOCIOS,
        },
        contacto: {
          activo: this.permisosForm.value.ACTIVOCONTACTO,
          escritura: this.permisosForm.value.ESCRITURACONTACTO,
        },
        url: {
          activo: this.permisosForm.value.ACTIVOURL,
          escritura: this.permisosForm.value.ESCRITURAURL,
        },
        objetivoDeVentas: {
          activo: this.permisosForm.value.ACTIVOOBJETIVODEVENTAS,
          escritura: this.permisosForm.value.ESCRITURAOBJETIVODEVENTAS,
        },
        contrato: {
          activo: this.permisosForm.value.ACTIVOCONTRATO ,
          escritura: this.permisosForm.value.ESCRITURACONTRATO,
        },
        ramo: {
          activo: this.permisosForm.value.ACTIVORAMO,
          escritura: this.permisosForm.value.ESCRITURARAMO,
        },
        subRamo: {
          activo: this.permisosForm.value.ACTIVOSUBRAMO,
          escritura: this.permisosForm.value.ESCRITURASUBRAMO,
        },
        verSubRamo: {
          activo: this.permisosForm.value.ACTIVOVERSUBRAMO,
          escritura: this.permisosForm.value.ESCRITURAVERSUBRAMO,
        },
        convenios: {
          activo: this.permisosForm.value.ACTIVOCONVENIOS,
          escritura: this.permisosForm.value.ESCRITURACONVENIOS,
        },
        producto: {
          activo: this.permisosForm.value.ACTIVOPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURAPRODUCTO,
        },
        verProducto: {
          activo: this.permisosForm.value.ACTIVOVERPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURAVERPRODUCTO,
        },
        competenciaProducto: {
          activo: this.permisosForm.value.ACTIVOCOMPETENCIAPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURACOMPETENCIAPRODUCTO,
        },
        analisisCompetencia: {
          activo: this.permisosForm.value.ACTIVOANALISISCOMPETENCIA,
          escritura: this.permisosForm.value.ESCRITURAANALISISCOMPETENCIA,
        },
        cobertura: {
          activo: this.permisosForm.value.ACTIVOCOBERTURA,
          escritura: this.permisosForm.value.ESCRITURACOBERTURA,
        },
        catalogosComercial: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSCOMERCIAL,
        },
        tipoGiro: {
          activo: this.permisosForm.value.ACTIVOTIPOGIRO,
          escritura: this.permisosForm.value.ESCRITURATIPOGIRO,
        },
        tipoCategoria: {
          activo: this.permisosForm.value.ACTIVOTIPOCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURATIPOCATEGORIA,
        },
        tipoCompetencia: {
          activo: this.permisosForm.value.ACTIVOTIPOCOMPETENCIA,
          escritura: this.permisosForm.value.ESCRITURATIPOCOMPETENCIA,
        },
        tipoProducto: {
          activo: this.permisosForm.value.ACTIVOTIPOPRODUCTO,
          escritura: this.permisosForm.value.ESCRITURATIPOPRODUCTO,
        },
        divisas: {
          activo: this.permisosForm.value.ACTIVODIVISAS,
          escritura: this.permisosForm.value.ESCRITURADIVISAS,
        },
        categoria: {
          activo: this.permisosForm.value.ACTIVOCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURACATEGORIA,
        },
        subCategoria: {
          activo: this.permisosForm.value.ACTIVOSUBCATEGORIA,
          escritura: this.permisosForm.value.ESCRITURASUBCATEGORIA,
        },
        tipoCobertura: {
          activo: this.permisosForm.value.ACTIVOTIPOCOBERTURA,
          escritura: this.permisosForm.value.ESCRITURATIPOCOBERTURA,
        },
        bajasSocios: {
          activo: this.permisosForm.value.ACTIVOBAJASSOCIOS,
          escritura: this.permisosForm.value.ESCRITURABAJASSOCIOS,
        },
        reportesComercial: {
          activo: this.permisosForm.value.ACTIVOREPORTESCOMERCIAL,
          escritura: this.permisosForm.value.ESCRITURAREPORTESCOMERCIAL,
        },
        reporteSocios: {
          activo: this.permisosForm.value.ACTIVOREPORTESOCIOS,
          escritura: this.permisosForm.value.ESCRITURAREPORTESOCIOS,
        },
        // marketing
        marketing: {
          activo: this.permisosForm.value.ACTIVOMARKETING,
          escritura: this.permisosForm.value.ESCRITURAMARKETING,
        },
        campaña: {
          activo: this.permisosForm.value.ACTIVOCAMPAÑA,
          escritura: this.permisosForm.value.ESCRITURACAMPAÑA,
        },
        verCampaña: {
          activo: this.permisosForm.value.ACTIVOVERCAMPAÑA,
          escritura: this.permisosForm.value.ESCRITURAVERCAMPAÑA,
        },
        medioDifusion: {
          activo: this.permisosForm.value.ACTIVOMEDIODIFUSION,
          escritura: this.permisosForm.value.ESCRITURAMEDIODIFUSION,
        },
        campañaCategoria: {
          activo: this.permisosForm.value.ACTIVOCAMPAÑACATEGORIA,
          escritura: this.permisosForm.value.ESCRITURACAMPAÑACATEGORIA,
        },
        paginas: {
          activo: this.permisosForm.value.ACTIVOPAGINAS,
          escritura: this.permisosForm.value.ESCRITURAPAGINAS,
        },
        catalogosMarketing: {
          activo: this.permisosForm.value.ACTIVOCATALOGOSMARKETING,
          escritura: this.permisosForm.value.ESCRITURACATALOGOSMARKETING,
        },
        tipoPaginas: {
          activo: this.permisosForm.value.ACTIVOTIPOPAGINAS,
          escritura: this.permisosForm.value.ESCRITURATIPOPAGINAS,
        },
        provedorLeads: {
          activo: this.permisosForm.value.ACTIVOPROVEDORLEADS,
          escritura: this.permisosForm.value.ESCRITURAPROVEDORLEADS,
        },
      },
    };
    const grupo  = {
      idSubarea: this.permisosForm.value.idSubarea,
      idPuesto: this.permisosForm.value.idPuesto,
      idPermisosVisualizacion: this.permisosForm.value.idPermisosVisualizacion,
      nombre: this.permisosForm.value.nombre,
      descripcion: this.permisosForm.value.descripcion,
      permisos: JSON.stringify(this.permiso),
      activo: 1,
    };
    this.grupoUsuariosService.put(this.idgrupo, grupo).subscribe(result => {
      this.permisosForm.reset();
    });
    this.router.navigate(['/modulos/ti/permisos']);
  }

}
