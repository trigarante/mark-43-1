import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {JwtResponse, JwtResponseData} from './jwt-response';
import {AuthLoginInfo} from './login-info';
import 'rxjs-compat/add/operator/do';
import 'rxjs-compat/add/operator/shareReplay';
import {Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'}),
};

@Injectable({
  providedIn: 'root',
})
export class LoginService extends JwtResponseData {
  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  enviarCredenciales(credentials: AuthLoginInfo): Observable<JwtResponse> {
    // return this.http.post<JwtResponse>('http://192.168.10.16:8096/mark43-service/' + 'login', credentials, httpOptions);
    return this.http.post<JwtResponse>('https://pruebas.ahorraseguros.mx/mark43-service/' + 'login', credentials, httpOptions);
    // return this.http.post<JwtResponse>('http://192.168.10.3:8090/mark43-service/' + 'login', credentials, httpOptions);
  }

  getPermisos(token): Observable<any> {
    return this.http.get<any>(this.baseURL + 'grupos/' + token);
  }

}
