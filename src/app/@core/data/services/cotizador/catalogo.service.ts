import {Injectable} from '@angular/core';
import {Catalogo, CatalogoData, CatalogoPorAseguradora} from '../../interfaces/cotizador/catalogo';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class CatalogoService extends CatalogoData {

  constructor(private http: HttpClient) {
    super();

  }

  getDescripcion(aseguradora: string, marca: string, modelo: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(
      `${environment.CATALOGO_AUTOS}/descripciones?aseguradora=${aseguradora}&marca=${marca}&modelo=${modelo}`);
  }

  getDetalles(aseguradora: string, marca: string, modelo: string, descripcion: string, subdescripcion: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(
      `${environment.CATALOGO_AUTOS}/detallesAseguradora?aseguradora=${aseguradora}&marca=${marca}
      &modelo=${modelo}&descripcion=${descripcion}&subdescripcion=${subdescripcion}`);
  }

  getMarcas(aseguradora?: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(`${environment.CATALOGO_AUTOS}/marcas?aseguradora=${aseguradora}`);
  }

  getModelos(aseguradora: string, marca: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(`${environment.CATALOGO_AUTOS}/modelos?aseguradora=${aseguradora}&marca=${marca}`);
  }

  getSubdescripcion(aseguradora: string, marca: string, modelo: string, descripcion: string): Observable<Catalogo[]> {

    if (aseguradora.length === 0) {
      return this.http.get<Catalogo[]>(
        `${environment.CATALOGO_AUTOS}/subdescripciones?aseguradora=&marca=${marca}&modelo=${modelo}&descripcion=${descripcion}`);
    }
    return this.http.get<Catalogo[]>(
      `${environment.CATALOGO_AUTOS}/subdescripciones?aseguradora=${aseguradora}
      &marca=${marca}&modelo=${modelo}&descripcion=${descripcion}`);

  }

  getDetallesHomologado(marca: string, modelo: string, descripcion: string, subdescripcion: string): Observable<CatalogoPorAseguradora[]> {
    return this.http.get<CatalogoPorAseguradora[]>(
      `${environment.CATALOGO_AUTOS}/detallesAseguradora?marca=${marca}
      &modelo=${modelo}&descripcion=${descripcion}&subdescripcion=${subdescripcion}`);
  }

  limpiar() {
    // switch () {
    //
    // }
  }
}
