import {Global} from '../global';
import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {UsuarioTipoService} from './usuario-tipo.service';

describe('TipoPagina service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: UsuarioTipoService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = {
    id: 4,
    tipo: 'TEST',
    descripcion: 'TEST UNICO',
    activo: 0,
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [UsuarioTipoService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(UsuarioTipoService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  describe('funsiona servicio correctamente', () => {
    describe('get', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.get.toString());
        chai.request(url)
          .get(go)
          .end((err, res) => {
            console.log('UsuarioTipoService get GET 200 status ---> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion get debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.get);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('getPuestoTipoById', () => {
      it('status 200', (done) => {
        const url = validarFuncion();
        const go = validarFuncion(service.getPuestoTipoById.toString());
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        chai.use(chaiHttp);
        chai.request(url)
          .get(go + 1)
          .end((err, res) => {
            console.log('UsuarioTipoService getPuestoTipoById 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getPuestoTipoById debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getPuestoTipoById);
              }
            done();
          });
        done();
        setTimeout(done, 500);
      });
    });
    describe('post', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.post.toString());
        chai.request(url)
          .post(go)
          .send(body)
          .end( (err, res) => {
            console.log('UsuarioTipoService post POST 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion post debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.post.toString());
              }
            /*expect(res.status).toBe(200);*/
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('put', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.put.toString());
        chai.request(url)
          .put(go + 4)
          .send(body)
          .end( (err, res) => {console.log('UsuarioTipoService PUT 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion put debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.put);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
})
