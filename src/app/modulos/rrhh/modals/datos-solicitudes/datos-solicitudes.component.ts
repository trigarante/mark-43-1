import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';

@Component({
  selector: 'ngx-datos-solicitudes',
  templateUrl: './datos-solicitudes.component.html',
  styleUrls: ['./datos-solicitudes.component.scss'],
})
export class DatosSolicitudesComponent implements OnInit {

  @Input() idSolicitud: number;

  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  telefono: number;
  correo: string;
  cp: number;
  edad: number;
  usuario: string;
  estado: boolean;
  fechaCita: Date;
  bolsaTrabajo: string;
  idVacante: number;
  vacante: string;
  documentos: string;
  sede: string;
  idReclutador: number;
  activoFecha: boolean;


  constructor(private solicitudService: SolicitudesData, protected ref: NbDialogRef<DatosSolicitudesComponent>) { }

  ngOnInit() {
    this.getSolicitud();
    this.activoFecha = true;
  }

  /** Función que obtiene la solicitud  en el servidor. */
  getSolicitud() {
    this.solicitudService.getSolicitudById(this.idSolicitud).subscribe(data => {
        this.nombre = data.nombre;
        this.apellidoPaterno = data.apellidoPaterno;
        this.apellidoMaterno = data.apellidoMaterno;
        this.edad = data.edad;
        this.telefono = data.telefono;
        this.vacante = data.vacante;
        this.correo = data.correo;
        this.cp = data.cp;
        this.sede = data.sede;
        this.idReclutador = data.idReclutador;
        if (data.fechaCita === null) {
          this.activoFecha = false;
        } else {
          this.fechaCita = data.fechaCita;
        }
      });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

}
