import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../global';
import {FlujoPolizaData, FlujoPolizaVn} from '../../../interfaces/ventaNueva/catalogos/flujo-poliza-vn';
import {Observable} from 'rxjs';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class FlujoPolizaVnService extends FlujoPolizaData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = Global.url;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<FlujoPolizaVn[]> {
    return this.http.get<FlujoPolizaVn[]>(this.baseURL + 'flujo-poliza');
  }

  post(flujoPoliza: FlujoPolizaVn): Observable<FlujoPolizaVn> {
    return this.http.post<FlujoPolizaVn>(this.baseURL + 'flujo-poliza', flujoPoliza);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'vnflujopoliza/saveVnFlujoPoliza', json);
  }

  getFlujoPolizaById(idFlujoPoliza): Observable<FlujoPolizaVn> {
    return this.http.get<FlujoPolizaVn>(this.baseURL + 'flujo-poliza/' + idFlujoPoliza);
  }

  put(idFlujoPoliza, flujoPoliza: FlujoPolizaVn): Observable<FlujoPolizaVn> {
    return this.http.put<FlujoPolizaVn>(this.baseURL + 'flujo-poliza/' + idFlujoPoliza, flujoPoliza);
    // + sessionStorage.getItem('Empleado'),
  }
}
