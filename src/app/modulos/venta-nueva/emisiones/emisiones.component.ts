import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-emisiones',
  templateUrl: './emisiones.component.html',
  styleUrls: ['./emisiones.component.scss'],
})
export class EmisionesComponent implements OnInit {

  @Input() idSolicitud: number = this.route.snapshot.params.idSolicitud;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
  }

}
