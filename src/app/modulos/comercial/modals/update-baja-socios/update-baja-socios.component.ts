import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-update-baja-socios',
  templateUrl: './update-baja-socios.component.html',
  styleUrls: ['./update-baja-socios.component.scss'],
})
export class UpdateBajaSociosComponent implements OnInit {
  @Input() idTipo: number;

  constructor() { }

  ngOnInit() {
  }

}
