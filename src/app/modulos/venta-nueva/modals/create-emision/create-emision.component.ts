import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientesData, ClienteVn} from '../../../../@core/data/interfaces/ventaNueva/cliente-vn';
import {map} from 'rxjs/operators';
import {Sepomex, SepomexData} from '../../../../@core/data/interfaces/catalogos/sepomex';
import {SelectItem} from 'primeng/api';
import {Paises, PaisesData} from '../../../../@core/data/interfaces/catalogos/paises';
import {SolicitudesVNData} from '../../../../@core/data/interfaces/ventaNueva/solicitudes';

@Component({
  selector: 'ngx-create-emision',
  templateUrl: './create-emision.component.html',
  styleUrls: ['./create-emision.component.scss'],
})
export class CreateEmisionComponent implements OnInit {
  @Input() idProductoSolicitud: number;
  @Input() idSolicitud: number;
  @Input() idSubRamo: number;

  @Output() _idProductoSolicitud = new EventEmitter<number>();
  @Output() _idSolicitud = new EventEmitter<number>();
  @Output() _idSubRamo = new EventEmitter<number>();
  @Output() _idCliente = new EventEmitter();

  @Output() _datosProducto = new EventEmitter<boolean>();

  emisionFisicaForm: FormGroup;
  emisionMoralForm: FormGroup;
  cliente: ClienteVn;
  /** Arreglo auxiliar para coloniasSepomex */
  colonias: any[];
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  /**Variable que muestra mensaje si el CP no es valido*/
  mostrar: boolean;
  /**Variable que activa opcion de actualizar o crear si existe curp o rfc*/
  idTipo: boolean;
  /**Variable que cacha el tipo de tab seleccionado*/
  razonSocial: string = 'FISICO';
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9]+/;
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders: SelectItem[];
  /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para paisOrigen. */
  pais: any[];
  /**Variable que almacena el idCliente al validar si existe el curp o rfc*/
  idCliente: number;

  constructor(private fb: FormBuilder, protected clienteService: ClientesData,
              protected sepomexService: SepomexData, protected paisesService: PaisesData,
              private solicitudesService: SolicitudesVNData,
  ) {
  }

  guardarEmsion() {
    if (this.razonSocial === 'MORAL') {
      this.clienteService.post(this.emisionMoralForm.value).subscribe();
    } else if (this.razonSocial === 'FISICO') {
      this.clienteService.post(this.emisionFisicaForm.value).subscribe();
    }
  }

  actualizarEmision() {
    if (this.razonSocial === 'MORAL') {
      this.clienteService.put(this.cliente.id, this.emisionMoralForm.value).subscribe();
    } else if (this.razonSocial === 'FISICO') {
      this.clienteService.put(this.cliente.id, this.emisionFisicaForm.value).subscribe();
    }
  }

  /** Función que inicializa el arreglo "genero" para usarlo en el Select. */
  genero() {
    this.genders = [];
    this.genders.push({label: 'MASCULINO', value: 'M'});
    this.genders.push({label: 'FEMENINO', value: 'F'});
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }

  valueTab(label) {
    this.razonSocial = label;
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    this.sepomexService.getColoniaByCp(cp)
      .pipe(map(data => this.coloniasSepomex = data)).subscribe(data => {
      this.colonias = [];
      if (this.coloniasSepomex.length === 0) {
        this.mostrar = true;
      } else {
        this.mostrar = false;
      }
      for (let i = 0; i < this.coloniasSepomex.length; i++) {
        this.colonias.push({'label': this.coloniasSepomex[i].asenta, 'value': this.coloniasSepomex[i].idCp});
      }
    });

    const cpParse = parseInt(cp);
    this.sepomexService.getColoniaByCp(cpParse).subscribe(res => {
      if (res.length !== 0) {
        this.emisionMoralForm.controls['cp'].setErrors(null);
      } else {
        this.emisionMoralForm.controls['cp'].setErrors({'incorrect': true});
      }
    }, error => error);

  }

  validarCp(value) {

  }

  validarCurp(curp) {
    this.clienteService.curpExist(curp).subscribe(result => {
      if (result !== null) {
        this.idTipo = false;
        this.cliente = result;

        this.emisionFisicaForm.controls['nombre'].setValue(this.cliente.nombre);
        this.emisionFisicaForm.controls['paterno'].setValue(this.cliente.paterno);
        this.emisionFisicaForm.controls['materno'].setValue(this.cliente.materno);
        this.emisionFisicaForm.controls['correo'].setValue(this.cliente.correo);
        this.emisionFisicaForm.controls['cp'].setValue(this.cliente.cp);
        this.getColonias(this.cliente.cp);
        this.emisionFisicaForm.controls['idColonia'].setValue(this.cliente.idColonia);
        this.emisionFisicaForm.controls['calle'].setValue(this.cliente.calle);
        this.emisionFisicaForm.controls['numInt'].setValue(this.cliente.numInt);
        this.emisionFisicaForm.controls['numExt'].setValue(this.cliente.numExt);
        this.emisionFisicaForm.controls['genero'].setValue(this.cliente.genero);
        this.emisionFisicaForm.controls['telefonoFijo'].setValue(this.cliente.telefonoFijo);
        this.emisionFisicaForm.controls['telefonoMovil'].setValue(this.cliente.telefonoMovil);
        this.emisionFisicaForm.controls['fechaNacimiento'].setValue(this.cliente.fechaNacimiento);
        this.emisionFisicaForm.controls['rfc'].setValue(this.cliente.rfc);
        this.emisionFisicaForm.controls['idPais'].setValue(1);
      }
    });
  }

  validarRFC(rfc) {
    this.clienteService.rfcExist(rfc).subscribe(result => {
      if (result !== null) {
        this.idTipo = false;
        this.cliente = result;
        this.idCliente = result.id;
        this.emisionMoralForm.controls['nombre'].setValue(this.cliente.nombre);
        // this.emisionMoralForm.controls['materno'].setValue(this.cliente.materno);
        // this.emisionMoralForm.controls['paterno'].setValue(this.cliente.paterno);
        this.emisionMoralForm.controls['correo'].setValue(this.cliente.correo);
        this.emisionMoralForm.controls['cp'].setValue(this.cliente.cp);
        this.getColonias(this.cliente.cp);
        this.emisionMoralForm.controls['idColonia'].setValue(this.cliente.idColonia);
        this.emisionMoralForm.controls['calle'].setValue(this.cliente.calle);
        this.emisionMoralForm.controls['numInt'].setValue(this.cliente.numInt);
        this.emisionMoralForm.controls['numExt'].setValue(this.cliente.numExt);
        // this.emisionMoralForm.controls['genero'].setValue(this.cliente.genero);
        this.emisionMoralForm.controls['telefonoFijo'].setValue(this.cliente.telefonoFijo);
        this.emisionMoralForm.controls['telefonoMovil'].setValue(this.cliente.telefonoMovil);
        // this.emisionMoralForm.controls['fechaNacimiento'].setValue(this.cliente.fechaNacimiento);
        // this.emisionMoralForm.controls['curp'].setValue(this.cliente.curp);
        this.emisionMoralForm.controls['idPais'].setValue(this.cliente.idPais);
      }
    });
  }

  ngOnInit() {
    this.idTipo = true;
    this.genero();
    this.getPaises();
    this.emisionFisicaForm = this.fb.group({
      'curp': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]'),
        Validators.maxLength(18), Validators.minLength(18)])),
      'nombre': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(15)])),
      'paterno': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(15)])),
      'materno': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(15)])),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5),
        Validators.maxLength(5)])),
      'idColonia': new FormControl('', Validators.compose([Validators.required])),
      'calle': new FormControl('', Validators.compose([Validators.required])),
      'numInt': new FormControl(''),
      'numExt': new FormControl('', Validators.compose([Validators.required])),
      'genero': new FormControl('', Validators.compose([Validators.required])),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.minLength(10),
        Validators.maxLength(10)])),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.required, Validators.minLength(10),
        Validators.maxLength(10)])),
      'fechaNacimiento': new FormControl('', Validators.compose([Validators.required])),
      'razonSocial': 1,
      'idPais': new FormControl(),
    });
    this.emisionMoralForm = this.fb.group({
      'rfc': new FormControl('', Validators.compose([
        Validators.pattern('[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][1-9][A-Z-0-9][A-Z-0-9][A-Z-0-9]'),
        Validators.maxLength(13), Validators.minLength(13)])),
      // 'curp': new FormControl('', Validators.compose([Validators.required,
      //   Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]'),
      //   Validators.maxLength(18), Validators.minLength(18)])),
      'nombre': new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(3), Validators.maxLength(15)])),
      'correo': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'cp': new FormControl('', Validators.compose([Validators.required, Validators.minLength(5),
        Validators.maxLength(5)])),
      'idColonia': new FormControl('', Validators.compose([Validators.required])),
      'calle': new FormControl('', Validators.compose([Validators.required])),
      'numInt': new FormControl(''),
      'numExt': new FormControl('', Validators.compose([Validators.required])),
      // 'genero': new FormControl('', Validators.compose([Validators.required])),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.maxLength(10),
        Validators.minLength(10), Validators.required])),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.maxLength(10),
        Validators.minLength(10), Validators.required])),
      // 'fechaNacimiento': new FormControl('', Validators.compose([Validators.required])),
      'razonSocial': 2,
      'idPais': new FormControl('', Validators.required),
    });

    if (this.emisionMoralForm.controls['telefonoFijo'].valid) {
      // this.emisionMoralForm.controls['telefonoMovil'].valid;
      console.log('TelefonoFijo validado');
    }
    // } else if (this.emisionMoralForm.controls['telefonoFijo'].value === null) {
    //   this.emisionMoralForm.controls['telefonoMovil'].setValidators(Validators.required);
    // }
  }

  validarNumero(value) {
    // if (this.emisionMoralForm.controls['telefonoFijo'].valid) {
    //   // this.emisionMoralForm.controls['telefonoMovil'].valid;
    //   console.log('TelefonoFijo validado');
    // }
    console.log(value)
  }

  // dismiss() {
  //   this.ref.close();
  // }


  datosProducto() {
    // this._idCliente.emit(this.cliente.id);
    this.solicitudesService.entryIdCliente(this.idCliente);
    this._idProductoSolicitud.emit(this.idProductoSolicitud);
    this._idSubRamo.emit(this.idSubRamo);
    this._idSolicitud.emit(this.idSolicitud);
    this._datosProducto.emit(true);
    // this.dialogoService.open(DatosProductoComponent, {
    //   context: {
    //     idCliente: this.cliente.id,
    //     idProductoSolicitud: this.idProductoSolicitud,
    //     idSolicitud: this.idSolicitud,
    //     idSubRamo: this.idSubRamo,
    //   },
    // });
  }

}
