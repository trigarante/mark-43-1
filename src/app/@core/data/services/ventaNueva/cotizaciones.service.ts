import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Cotizaciones, CotizacionesData} from '../../interfaces/ventaNueva/cotizaciones';
// import {stream} from "xlsx";

@Injectable()
export class CotizacionesService extends CotizacionesData {

  returnEmisionParam: any;

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Cotizaciones[]> {
    return this.http.get<Cotizaciones[]>(this.baseURL + 'v1/cotizaciones');
  }

  post(cotizacion: Cotizaciones): Observable<Cotizaciones> {
    return this.http.post<Cotizaciones>(this.baseURL + 'v1/cotizaciones', cotizacion);
  }

  getCotizacionById(id): Observable<Cotizaciones> {
    return this.http.get<Cotizaciones>(this.baseURL + 'v1/cotizaciones/' + id);
  }

  put(idCotizacion, cotizacion: Cotizaciones): Observable<Cotizaciones> {
    return this.http.put<Cotizaciones>(this.baseURL + 'v1/cotizaciones/' + idCotizacion, cotizacion);
  }

  createEmisionParam(aseguradora) {
    this.returnEmisionParam = aseguradora;
  }
  returnEmision(): Observable<string> {
    return this.returnEmisionParam;
  }
}
