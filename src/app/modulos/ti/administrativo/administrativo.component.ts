import { Component, OnInit } from '@angular/core';
import {EmpleadosService} from '../../../@core/data/services/rrhh/empleados.service';
import {DialogService} from 'primeng/api';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {NbDialogService} from '@nebular/theme';
import {CrearCorreoComponent} from '../modals/crear-correo/crear-correo.component';
import {CrearExtensionComponent} from '../modals/crear-extension/crear-extension.component';
import {CrearAdministrativoComponent} from '../modals/crear-administrativo/crear-administrativo.component';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {SubareaService} from '../../../@core/data/services/catalogos/subarea.service';
import {map} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {CrearAuditoriaComponent} from '../modals/crear-auditoria/crear-auditoria.component';

@Component({
  selector: 'ngx-administrativo',
  templateUrl: './administrativo.component.html',
  styleUrls: ['./administrativo.component.scss'],
  providers: [EmpleadosService, DialogService, SubareaService],
})
export class AdministrativoComponent implements OnInit {
  cols: any[];
  empleado: Empleados[];
  subareaSelected: number;
  area: any[];
  subarea: Subarea[];
  subareass: any;
  userForm: FormGroup;
  idsubarea: any;
  /**se obtienen arreglos de las areas*/
  areas: any[];
  subareas: Area[];
  tipoarea: number;

  constructor(private empleadosService: EmpleadosData,  public dialogService: NbDialogService,
              private subareaService: SubareaData, private fb: FormBuilder,
              private areaService: AreaData) {

    this.userForm = this.fb.group({
      'subareacontrol': {'id': this.subareaSelected},
      'areacontrol': new FormControl(),
    });
  }
  getEmpleados(id) {
    this.empleadosService.getBySubarea(id).pipe(map( result => {
      return result.filter( data => data.idUsuario === null );
    })).subscribe(data => this.empleado = data);

    this.idsubarea = id;
  }

  asignarAdministrativo(id, subareax ) {
    this.dialogService.open(CrearAdministrativoComponent, {
      context: {
        id: id,
        subareax: subareax,
      },
    });
  }
  asignarAuditoria(id, subareax ) {
    this.dialogService.open(CrearAuditoriaComponent, {
      context: {
        id: id,
        subareax: subareax,
      },
    });
  }
  /** consigue todas las areas de administrativo*/
  getArea() {
    this.areaService.get().pipe(map(result => {
      return result.filter(data =>  data.id === 13 || data.id === 14 || data.id === 15 || data.id === 16
        || data.id === 17 || data.id === 18 || data.id === 19);
    })).subscribe(data => {
      this.subareas = data;
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }
  getsubareas(event) {
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.subareass = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.subareass.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});

        this.empleado = null;
      }
    });
    this.tipoarea = event;
  }

  ngOnInit() {
    this.getArea();
    this.cols = [
      {field: 'nombre', header: 'Nombre'},
      {field: 'telefono', header: 'Telefono'},
      {field: 'crearcorreo', header: 'Crear Correo'},
    ];

  }

}
