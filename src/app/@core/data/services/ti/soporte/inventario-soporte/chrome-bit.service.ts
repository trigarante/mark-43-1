import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {ChromeBit, ChromeBitData} from '../../../../interfaces/ti/soporte/inventario-soporte/chrome-bit';

@Injectable()
export class ChromeBitService extends ChromeBitData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<ChromeBit[]> {
    return this.http.get<ChromeBit[]>(this.baseURL + 'v1/chromeBit');
  }

  post(chromeBit: ChromeBit): Observable<ChromeBit> {
    return this.http.post<ChromeBit>(this.baseURL + 'v1/chromeBit', chromeBit);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'chromebit/saveChromeBit', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajachromebit/saveBajaChromeBit', json);
  }

  getChromeBit(idChromeBit): Observable<ChromeBit> {
    return this.http.get<ChromeBit>(this.baseURL + 'v1/chromeBit/' + idChromeBit);
  }

  put(idChromeBit, chromeBit: ChromeBit): Observable<ChromeBit> {
    return this.http.put<ChromeBit>(this.baseURL + 'v1/chromeBit/' + idChromeBit + '/'
      + sessionStorage.getItem('Empleado'), chromeBit);
  }
}
