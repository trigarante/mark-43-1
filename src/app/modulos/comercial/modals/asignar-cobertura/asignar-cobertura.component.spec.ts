import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCoberturaComponent } from './asignar-cobertura.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {CoberturaData} from '../../../../@core/data/interfaces/comerciales/cobertura';
import {TipoCoberturaData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-cobertura';

describe('AsignarCoberturaComponent', () => {
  let component: AsignarCoberturaComponent;
  let fixture: ComponentFixture<AsignarCoberturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCoberturaComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
      ],
      providers: [
        CoberturaData,
        TipoCoberturaData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCoberturaComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
