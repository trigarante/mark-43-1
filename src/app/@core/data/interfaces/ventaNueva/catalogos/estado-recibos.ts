import {Observable} from 'rxjs';

export interface EstadoRecibos {
  id: number;
  estdo: string;
  activo: number;
}

export abstract class EstadoRecibosData {

  abstract get(): Observable<EstadoRecibos[]>;

  abstract post(estadoRecibos): Observable<EstadoRecibos>;

  abstract getEstadoReciboById(idEstadoRecibos): Observable<EstadoRecibos>;

  abstract put(idEstadoRecibos, estadoRecibos): Observable<EstadoRecibos>;

}
