import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteSocioComponent } from './reporte-socio.component';

describe('ReporteSocioComponent', () => {
  let component: ReporteSocioComponent;
  let fixture: ComponentFixture<ReporteSocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteSocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteSocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
