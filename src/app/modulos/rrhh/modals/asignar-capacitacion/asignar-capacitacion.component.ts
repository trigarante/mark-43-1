import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDateService, NbDialogRef} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {Competencia, CompetenciaData} from '../../../../@core/data/interfaces/catalogos/competencia';
import {
  CompetenciaCandidatoData,
  CompetenciasCandidato,
} from '../../../../@core/data/interfaces/catalogos/competencias-candidato';
import {EmpleadosData, Empleados} from '../../../../@core/data/interfaces/rrhh/empleados';
import {CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import {Router} from '@angular/router';
/** import * as moment from "../agendar-cita/agendar-cita.component";*/
import * as moment from 'moment';
import {CandidatoData} from '../../../../@core/data/interfaces/rrhh/candidatos';
/** Componente del modal "asignar capacitación". */
@Component({
  selector: 'ngx-asignar-capacitacion',
  templateUrl: './asignar-capacitacion.component.html',
  styleUrls: ['./asignar-capacitacion.component.scss'],
})
export class AsignarCapacitacionComponent implements OnInit {

  /**
   * El método constructor invoca a Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<AsignarCapacitacionComponent>} ref Referencia al diálogo (mensaje).
   * @param {CompetenciaData} competenciaService Servicio del componente competencia para poder invocar los métodos.
   * @param {CompetenciaCandidatoData} competenciaCandidatoService Servicio del componente competencias-candidato
   * para poder invocar los métodos.
   * @param {CapacitacionData} capacitacionService Servicio del componente capacitacion para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<AsignarCapacitacionComponent>,
              protected competenciaService: CompetenciaData, private candidatoService: CandidatoData,
              protected competenciaCandidatoService: CompetenciaCandidatoData,
              protected capacitacionService: CapacitacionData, private router: Router,
              protected dateService: NbDateService<Date>, protected empleadoService: EmpleadosData) {

    // this.min = this.dateService.addDay(this.dateService.today(), 0);
    // this.max = this.dateService.addMonth(this.dateService.today(), 2);
  }
  /** Variable que almacena el idPrecandidato obtenido. */
  @Input() idPrecandidato: number;
  /** Variable que almacena el idCandidato obtenido. */
  @Input() idCandidato: number;
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  //
  @Input() idEtapa: number;
  /**Variable que almacena el Nombre*/
  @Input() nombre: string;
  /**Variable que almacena el apellidoPaterno*/
  @Input() apellidoPaterno: string;
  /**Variable que almacena el apellidoMaterno*/
  @Input() apellidoMaterno: string;
  /**Variable que almacena el nombreCompleto*/
  nombreCompleto: string;
  teoricoEliminado: boolean;
//
  @Input() idCapacitacion: number;
  /** Variable que permite la inicialización del formulario para asignar el seguimiento a un precandidato. */
  formAsiganrCapacitacion: FormGroup;
  /** Expresión regular para números y punto. */
  numeroPunto: RegExp = /[0-9.]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los datos del microservicio. */
  competencia: Competencia[];
  /** Arreglo auxiliar para competencia. */
  competenciass: any[];
  /** Arreglo que se inicializa con los datos del microservicio. */
  candidato: CompetenciasCandidato[];
  /** Arreglo auxiliar para candidato. */
  candidatos: any[];
  /** Arreglo que se inicializa con los datos del microservicio. */
  empleados: Empleados[];
  /** Arreglo auxiliar para empleados. */
  empleado: any[];
  /** Bandera para verificar que el formulario se ha enviado. */
  submitted: boolean;
  /** Arreglo que se inicializa con los valores del campo sede desde el Microservicio.  */
  reclutadores: Empleados[];
  /** Arreglo auxiliar para reclutadores. */
  reclutador: any[];
  //
  capcitacion: any;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**Variable que almacena la fecha ingreso para comparar si es modificada*/
  feIngreso: any;
  /** Esta función cierra el modal. */
  ocultar() {
    this.ref.close();
  }
  getReclutador() {
    this.empleadoService.get().pipe(map(result => {
      return result.filter(data => data.idPuesto === 10);
    })).subscribe(data => {
      this.reclutadores = data;
      this.reclutador = [];
      for (let i = 0; i < this.reclutadores.length; i++) {
        this.reclutador.push({
          'label': this.reclutadores[i].nombre + ' ' + this.reclutadores[i].apellidoPaterno
            + ' ' + this.reclutadores[i].apellidoMaterno + ' - ' + this.reclutadores[i].id,
          'value': this.reclutadores[i].id,
        });
      }
    });
  }

  /** Función que obtiene los datos del campo competencia del microservicio y los almacena en el arreglo "comptenciass". */
  getCompetencia() {
    this.competenciaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.competencia = result;
      this.competenciass = [];
      for (let i = 0; i < this.competencia.length; i++) {
        this.competenciass.push({'label': this.competencia[i].descripcion, 'value': this.competencia[i].id});
      }
    });
  }

  /** Función que obtiene los datos del campo candidato del microservicio y los almacena en el arreglo "candidatos". */
  getCompetenciaCandidato() {
    this.competenciaCandidatoService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.candidato = result;
      this.candidatos = [];
      for (let i = 0; i < this.candidato.length; i++) {
        this.candidatos.push({'label': this.candidato[i].descripcion, 'value': this.candidato[i].id});
      }
    });
  }

  /** Con esta función se obtienen todos los empleados existentes en el servidor. */
  getEmpleado() {
    this.empleadoService.get().pipe(map(result => {
      return result.filter(data => data.idSubarea === 8);
    })).subscribe(data => {
      this.empleados = data;
    });
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario y redirecciona a la página. */
  asignarCapacitacion() {
    this.inactivo = true;
    this.submitted = true;
    if (this.formAsiganrCapacitacion.invalid) {
      return;
    }
    let calificacion;
    calificacion = this.formAsiganrCapacitacion.value.calificacion;
    calificacion = Number(calificacion);
    this.formAsiganrCapacitacion.value.calificacion = calificacion;
    this.idEtapa = 4;
    let capacitacionUpd: any;
    capacitacionUpd = {
      'calificacion': this.formAsiganrCapacitacion.value.calificacion,
      'idCalificacionCompetencia': this.formAsiganrCapacitacion.value.idCalificacionCompetencia,
      'idCompetenciaCandidato': this.formAsiganrCapacitacion.value.idCompetenciaCandidato,
      'comentarios': this.formAsiganrCapacitacion.value.comentarios,
      'idCapacitador': this.formAsiganrCapacitacion.value.idCapacitador,
      'idCandidato': this.formAsiganrCapacitacion.value.idCandidato,
      // 'id': this.formAsiganrCapacitacion.value.id,
      'asistencia': this.formAsiganrCapacitacion.value.asistencia,
    };
    // let fecha: any;
    // fecha = moment(this.formAsiganrCapacitacion.controls['fechaIngreso'].value).format('YYYY-MM-DD');
    let fechaIngreso: any;
    fechaIngreso = Date.parse(this.formAsiganrCapacitacion.controls['fechaIngreso'].value);
    this.capacitacionService.post(capacitacionUpd, this.idPrecandidato, this.idEtapa, fechaIngreso).subscribe(
        (result) => {
        },
        error => { },
        () => {
          this.capacitacionService.postSocket(capacitacionUpd).subscribe(d => {});
          this.candidatoService.postSocket(capacitacionUpd).subscribe(d => {});
          this.router.navigate(['/modulos/rrhh/capacitacion/']);
          this.ref.close();
          this.formAsiganrCapacitacion.reset();
        },

    );
  }
  /** Validar que no se ingrese manualmente una fecha anterior a la actual*/
  validarFechaEscritaManualmente(date: Date) {
    const hoy = moment(new Date()).format('YYYYMMDD');
    const fecha = moment(date).format('YYYYMMDD');
    if (fecha < hoy) {
      this.inactivo = true;
    }else if (fecha >= hoy) {
      this.inactivo = false;
    }
  }
  getCapacitacionById(idCapacitacion) {
    this.capacitacionService.getCapacitacionById(idCapacitacion).subscribe(result => {
      this.capcitacion = result;
      this.formAsiganrCapacitacion.controls['calificacion'].setValue(this.capcitacion.calificacion);
      this.formAsiganrCapacitacion.controls['idCalificacionCompetencia'].setValue(this.capcitacion.idCalificacionCompetencia);
      this.formAsiganrCapacitacion.controls['idCompetenciaCandidato'].setValue(result.idCompetenciaCandidato);
      this.formAsiganrCapacitacion.controls['comentarios'].setValue(this.capcitacion.comentarios);
      this.formAsiganrCapacitacion.controls['idCapacitador'].setValue(this.capcitacion.idCapacitador);
      this.formAsiganrCapacitacion.controls['idCandidato'].setValue(this.capcitacion.idCandidato);
      this.formAsiganrCapacitacion.controls['fechaIngreso'].setValue(new Date(result.fechaIngreso.replace('-', '/')));
      this.formAsiganrCapacitacion.controls['asistencia'].setValue(this.capcitacion.asistencias);
      this.nombre = result.nombre;
      this.feIngreso = this.capcitacion.fechaIngreso;
    });
  }


  updateCapacitacion() {
    this.inactivo = true;
    let capacitacionUpd: any;
    capacitacionUpd = {
      'calificacion': this.formAsiganrCapacitacion.value.calificacion,
      'idCalificacionCompetencia': this.formAsiganrCapacitacion.value.idCalificacionCompetencia,
      'idCompetenciaCandidato': this.formAsiganrCapacitacion.value.idCompetenciaCandidato,
      'comentarios': this.formAsiganrCapacitacion.value.comentarios,
      'idCapacitador': this.formAsiganrCapacitacion.value.idCapacitador,
      'idCandidato': this.formAsiganrCapacitacion.value.idCandidato,
      'id': this.formAsiganrCapacitacion.value.id,
      'asistencia': this.formAsiganrCapacitacion.value.asistencia,
    };
    if (this.feIngreso !== moment(this.formAsiganrCapacitacion.controls['fechaIngreso'].value).format('YYYY-MM-DD')) {
      capacitacionUpd.asistencia = null;
    }
    /**this.formAsiganrCapacitacion.controls['fechaIngreso'].setValue(Date.parse(this.formAsiganrCapacitacion.controls['fechaIngreso'].
     * value));*/
    let fecha: any;
    fecha = moment(this.formAsiganrCapacitacion.controls['fechaIngreso'].value).format('YYYY-MM-DD');
    let fechaIngreso: any;
    fechaIngreso = Date.parse(fecha + 'T01:00');
    // fechaIngreso = Date.parse(this.formAsiganrCapacitacion.controls['fechaIngreso'].value);
    // console.log(fechaIngreso);
    this.capacitacionService.getCapacitacionById(this.idCapacitacion).subscribe(data => {
      if (data.idEtapa === 4 && data.idEstado === 1 || data.idEtapa === 4 && data.idEstado === 16) {
        console.log(capacitacionUpd);
        this.capacitacionService.updateCapacitacion(capacitacionUpd, fechaIngreso).subscribe(
          result => {
          },
          error => { },
          () => {
            this.capacitacionService.postSocket(capacitacionUpd).subscribe(d => {});
            this.formAsiganrCapacitacion.reset();
            this.ref.close();
          },
        );
      }else {
        this.teoricoEliminado = true;
      }
    });
  }

  blockearDate() {
    const date = new Date().toISOString().split('T')[0];
    document.getElementsByName('somedate')[0].setAttribute('min', date);
  }

  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    this.nombreCompleto = this.nombre + ' ' + this.apellidoPaterno + ' ' + this.apellidoMaterno;
    this.blockearDate();
    this.getCompetencia();
    this.getCompetenciaCandidato();
    this.getEmpleado();
    this.getReclutador();
    // this.getNombres();
    if (this.idTipo === 2) {
      this.getCapacitacionById(this.idCapacitacion);
    }
    this.formAsiganrCapacitacion = this.fb.group({
      'calificacion': new FormControl('', Validators.compose([Validators.required, Validators.max(10)])),
      'idCalificacionCompetencia': new FormControl('', Validators.required),
      'idCompetenciaCandidato': new FormControl('', Validators.required),
      'comentarios': new FormControl('', Validators.required),
      'idCapacitador': new FormControl('', Validators.required),
      'idCandidato': this.idCandidato,
      'id': this.idCapacitacion,
      'asistencia': null,
      'fechaIngreso': new FormControl('', Validators.compose([Validators.required])),
    });
  }
}
