import { Injectable } from '@angular/core';
import {environment} from '../../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Mouse, MouseData} from '../../../../interfaces/ti/soporte/inventario/mouse';
import {Observable} from 'rxjs';

@Injectable()
export class MouseService extends MouseData {
  private baseURL;
  private socketURl;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Mouse[]> {
    return this.http.get<Mouse[]>( this.baseURL + 'v1/mouse');
  }
  post(mouse: Mouse): Observable<Mouse> {
    return this.http.post<Mouse>(this.baseURL + 'v1/mouse', mouse);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'mouse/saveMouse', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajamouse/saveBajaMouse', json);
  }
  getMouseById(idMouse): Observable<Mouse> {
    return this.http.get<Mouse>(this.baseURL + 'v1/mouse/' + idMouse);
  }
  put(idMouse, mouse: Mouse): Observable<Mouse> {
    return this.http.put<Mouse>(this.baseURL + 'v1/mouse/' + idMouse + '/'
      + sessionStorage.getItem('Empleado'), mouse);
  }
}
