import { TestBed } from '@angular/core/testing';

import { ChromeBitService } from './chrome-bit.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('ChromeBitService', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 7,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    folio: '0001C',
    idMarca: 1,
    fechaRecepcion: '2019-09-24T18:33:38.000+0000',
    fechaSalida: null,
    activo: 0,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    origenRecurso: 'propio',
    idSede: 1,
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'chromeBit',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ChromeBitService, HttpClient],
  }));

  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/chromeBit');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/chromeBit', '/7');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/chromeBit');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/chromeBit', '/7/74');
      });
    });
  }
});
