import {Observable} from 'rxjs';

export interface Servidores {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class ServidoresData {
  abstract get(): Observable<Servidores[]>;
  abstract post(servidores): Observable<Servidores>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getServidoresById(idServidores): Observable<Servidores>;
  abstract put(idServidores, servidores): Observable<Servidores>;
}
