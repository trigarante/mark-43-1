import { Component, OnInit} from '@angular/core';
import {MENU_ITEMS} from './modulos-menu';


@Component({
  selector: 'ngx-modulos',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class ModulosComponent {
  menu = MENU_ITEMS;
}


