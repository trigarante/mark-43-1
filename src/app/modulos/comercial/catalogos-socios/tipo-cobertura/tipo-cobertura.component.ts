import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoCobertura, TipoCoberturaData} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-cobertura';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {TipoCoberturaCreateComponent} from '../modals/tipo-cobertura-create/tipo-cobertura-create.component';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-cobertura',
  templateUrl: './tipo-cobertura.component.html',
  styleUrls: ['./tipo-cobertura.component.scss'],
})
export class TipoCoberturaComponent implements OnInit {
  tipoCobertura: TipoCobertura[];
  cols: any[];
  tipoCoberturas: TipoCobertura;
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(protected tipoCoberturaService: TipoCoberturaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tipocobertura');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoCobertura', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this. getTipoCobertura(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getTipoCobertura(mensajeAct) {
    this.tipoCoberturaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.tipoCobertura = data;
      this.dataSource = new MatTableDataSource(this.tipoCobertura); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Cobertura`);
  }

  crearCobertura() {
    this.dialogService.open(TipoCoberturaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoCobertura(0));
  }

  updateTipoCobertura(idTipoCobertura) {
    this.dialogService.open(TipoCoberturaCreateComponent, {
      context: {
        idTipo: 2,
        idTipoCobertura: idTipoCobertura,
      },
    }).onClose.subscribe( x => this.getTipoCobertura(1));
  }

  bajaTipoCobertura(idTipoCobertura) {
    this.getTipoCoberturasById(idTipoCobertura);
    swal({
      title: '¿Deseas dar de baja este Tipo Cobertura?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoCoberturas['activo'] = 0;
        this.tipoCoberturaService.put(idTipoCobertura, this.tipoCoberturas).subscribe(result => {
          this.tipoCoberturaService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El Tipo Cobertura se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoCobertura(0);
        }));
      }
    });
  }

  getTipoCoberturasById(idTipoCobertura) {
    this.tipoCoberturaService.getTipoCoberturaById(idTipoCobertura).subscribe(data => {
      this.tipoCoberturas = data;
    });
  }

  ngOnInit() {
    this.connect();
    this.getTipoCobertura(0);
    this.cols = [ 'cobertura', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoCobertura.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoCobertura.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
