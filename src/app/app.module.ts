/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {APP_BASE_HREF, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, LOCALE_ID, APP_INITIALIZER} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CoreModule} from './@core/core.module';
import {registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es-MX';

registerLocaleData(localeEs);
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './@core/data/services/login/auth.guard';
import {NbAlertModule, NbLayoutModule, NbThemeModule} from '@nebular/theme';
import {AuthComponent} from './auth/auth.component';
import {GapiSession} from './@core/data/services/sessions/gapi.session';
import {AppContext} from './@core/data/services/app.context';
import {AppSession} from './@core/data/services/sessions/app.session';
import {FileSession} from './@core/data/services/sessions/file.session';
import {UserSession} from './@core/data/services/sessions/user.session';
import {AppService} from './@core/data/services/drive/app.service';
import {BreadCrumbSession} from './@core/data/services/sessions/breadcrumb.session';
import {FilesService} from './@core/data/services/drive/files.service';
import {UserdService} from './@core/data/services/drive/userd.service';
import {LoggedInGuard} from './@core/data/services/sessions/loggedInGuard';
import {MatFormFieldModule, MatInputModule, MatAutocompleteModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormaPagoService} from "./@core/data/services/ventaNueva/catalogos/forma-pago.service";
import {EstadoPagoService} from "./@core/data/services/ventaNueva/catalogos/estado-pago.service";
import {SolicitudesvnService} from "./@core/data/services/ventaNueva/solicitudes.service";

export function initGapi(gapiSession: GapiSession) {
  return () => gapiSession.initClient();
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbAlertModule,
    NbThemeModule.forRoot(),
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: APP_INITIALIZER, useFactory: initGapi, deps: [GapiSession], multi: true},
    // {provide: APP_BASE_HREF, useValue: '/mark43/'},
    // {provide: LocationStrategy, useClass: PathLocationStrategy},
    {
      provide: LOCALE_ID,
      useValue: 'es-MX',
    },
    AuthGuard,
    AppContext,
    AppSession,
    FileSession,
    GapiSession,
    UserSession,
    AppService,
    BreadCrumbSession,
    FilesService,
    UserdService,
    LoggedInGuard,
    FormaPagoService,
    EstadoPagoService,
    SolicitudesvnService,
  ],
})
export class AppModule {
}
