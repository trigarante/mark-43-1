import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.scss'],
})
export class SaveComponent implements OnInit {
  @Input() modo: number;

  constructor(protected ref: NbDialogRef<SaveComponent>, private router: Router) { }
  dismiss() {
    if (this.modo === 1 ) {
      this.ref.close();
      this.router.navigate(['/modulos/ti/ejecutivo/']);
    }
    this.ref.close();
    if ( this.modo === 2) {
      this.router.navigate(['/modulos/ti/supervisor/']);
    }
  }
  ngOnInit() {
  }

}
