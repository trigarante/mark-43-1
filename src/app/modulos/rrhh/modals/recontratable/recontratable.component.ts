import {Component, Input, OnInit} from '@angular/core';
import {Precandidatos, PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import {SelectItem} from 'primeng/api';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BajasrhComponent} from '../../bajasrh/bajasrh.component';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {map} from "rxjs/operators";
@Component({
  selector: 'ngx-recontratable',
  templateUrl: './recontratable.component.html',
  styleUrls: ['./recontratable.component.scss'],
})
export class RecontratableComponent implements OnInit {
  @Input() idPrecandidato: number;
  @Input() idEmpleado: number;
  @Input() idTipo: number;
  @Input() nombre: string;
  /**VAriable que alamcenara el value del Recontratable*/
  recontratable: SelectItem[];
  /**variable que inicializa el formuralio*/
  recontratableForm: FormGroup;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**Variable que obtiene el tipado de la interface*/
  precandidato: Precandidatos;
  selected: any;
  precandidatoReactivado: boolean;
  // Web Sockets
  // private stompClient = null;
  // private stompClientEmpleado = null;
  constructor(protected precandidatosService: PrecandidatosData, protected fb: FormBuilder, protected ref: NbDialogRef<BajasrhComponent>,
  protected empleadoService: EmpleadosData) {
  }
  // Web socket connection

  dismiss() {
    this.ref.close();
  }

  getRecontratable() {
    this.precandidatosService.getPrecandidatoById(this.idPrecandidato).subscribe(result => {
      this.precandidato = result;
      this.recontratableForm.controls['recontratable'].setValue(this.precandidato.recontratable);
      this.selected = result.recontratable.toString();
    });
  }

  updateRecontratable() {
    this.inactivo = true;
        this.precandidatosService.editarRecontratable(this.idPrecandidato, this.selected).subscribe(
          result => {
            this.inactivo = true;
          },
          error => { },
          () => {
            const object = {
              'idPrecandidato': this.idPrecandidato,
            };
            // this.stompClient.send('/tasks/saveBajasRH', {}, JSON.stringify(object));
            this.precandidatosService.postSocketBajaRH({estado: 'ok'}).subscribe(x => {});
            // this.stompClientEmpleado.send('/tasks/saveEmpleadoBaja', {}, JSON.stringify(object));
            this.empleadoService.postSocketBaja(object).subscribe(x => {});
            this.recontratableForm.reset();
            this.ref.close();
            /**window.location.reload();*/
          },
        );
  }

  recontratableDrow() {
    this.recontratable = [];
    this.recontratable.push({label: 'SI', value: '1'});
    this.recontratable.push({label: 'NO', value: '0'});
  }

  ngOnInit() {
    this.getRecontratable();
    this.recontratableDrow();
    this.recontratableForm = this.fb.group({
      'idPrecandidato': this.idPrecandidato,
      'recontratable': new FormControl('', Validators.required),
    });
  }

}
