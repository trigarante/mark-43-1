import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecontratableComponent } from './recontratable.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbAlertModule, NbCardModule, NbDateService, NbDialogRef, NbRadioModule, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';

describe('RecontratableComponent', () => {
  let component: RecontratableComponent;
  let fixture: ComponentFixture<RecontratableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecontratableComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        NbCardModule,
        NbAlertModule,
        NbRadioModule,
      ],
      providers: [
        PrecandidatosData,
        EmpleadosData,
        NbToastrService,
        NbDateService,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecontratableComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
