import {Observable} from 'rxjs';
export interface SubCategoria {
  id: number;
  detalle: string;
  regla: string;
  idDivisas: number;
  activo: number;
  idCategoria: number;
}
export abstract class SubCategoriaData {
  abstract get(): Observable<SubCategoria[]>;
  abstract post(SubCategoria): Observable<SubCategoria>;
  abstract postSocket(json): Observable<JSON>;
  abstract getSubCategoriaById(idSubCategoria): Observable<SubCategoria>;
  abstract put(idSubCategoria, SubCategoria): Observable<SubCategoria>;
}
