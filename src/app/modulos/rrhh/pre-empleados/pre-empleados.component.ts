import {Component, OnInit, ViewChild} from '@angular/core';
import {Preempleados, PreempleadosData} from '../../../@core/data/interfaces/rrhh/preempleados';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {DatosPreempladoComponent} from '../modals/datos-preemplado/datos-preemplado.component';
import {environment} from '../../../../environments/environment';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

/**
 * Componente de pre-empleados.
 */
@Component({
  selector: 'ngx-pre-empleados',
  templateUrl: './pre-empleados.component.html',
  styleUrls: ['./pre-empleados.component.scss'],
})
export class PreEmpleadosComponent implements OnInit {
  preEmpleado: Preempleados[];
  cols: any[];
  categoria: any[];
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {SeguimientoData} seguimientoService Servicio del componente precandidatos para poder invocar los métodos. */
  constructor(protected preEmpleadoService: PreempleadosData, private rutaActiva: ActivatedRoute, private dialogService: NbDialogService,
              private toastrService: NbToastrService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'preempleados');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelPreempleados', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPreEmpleado(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /** Esta función obtiene toda la información asociada al seguimiento que se le ha dado a cada candidato. */
  getPreEmpleado(mensajeAct) {
    this.preEmpleadoService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 7 && data.idEstadoRh === 1 || data.idEtapa === 7 && data.idEstadoRh === 16);
    })).subscribe(data => {
      this.preEmpleado = data;
      this.dataSource = new MatTableDataSource(this.preEmpleado);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (mensajeAct === 1) {
        this.showToast();
      }
    });
  }
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Pre-Empleado`);
  }

  /** Mostrar detalle del preempleado*/
  verDatos(idCandidato) {
    this.dialogService.open(DatosPreempladoComponent, {
      context: {
        idPreempleado: idCandidato,
      },
    });
  }

  ngOnInit() {
    this.connect();
    this.cols = [ 'detalle', 'preEmpleado', 'empresa', 'sede', 'subarea', 'puesto', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/pre-empleados') {
    //     this.getPreEmpleado(0);
    //   }
    // }, 1000);
    this.getPreEmpleado(0);
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.preEmpleado.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.empleados.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
