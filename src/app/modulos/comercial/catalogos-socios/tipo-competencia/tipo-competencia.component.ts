import {Component, OnInit, ViewChild} from '@angular/core';
import {
  TipoCometencia,
  TipoCompetenciaData,
} from '../../../../@core/data/interfaces/comerciales/catalogo/tipo-cometencia';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {TipoCompetenciaCreateComponent} from '../modals/tipo-competencia-create/tipo-competencia-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-tipo-competencia',
  templateUrl: './tipo-competencia.component.html',
  styleUrls: ['./tipo-competencia.component.scss'],
})
export class TipoCompetenciaComponent implements OnInit {
  cols: any[];
  tipoCompetencia: TipoCometencia[];
  tipoCompetencias: TipoCometencia;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // socket
  private stompClient = null;
  constructor(private tipoCompetenciaService: TipoCompetenciaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tipocompetencia');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoCompetencia', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoCompetencia(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getTipoCompetencia(mensajeAct) {
    this.tipoCompetenciaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.tipoCompetencia = data;
      this.dataSource = new MatTableDataSource(this.tipoCompetencia); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Tipo Competencia`);
  }

  crearTipoCategoria() {
    this.dialogService.open(TipoCompetenciaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getTipoCompetencia(0));
  }

  updateTipoCompetencia(idTipoCompetencia) {
    this.dialogService.open(TipoCompetenciaCreateComponent, {
      context: {
        idTipo: 2,
        idTipoCompetencia: idTipoCompetencia,
      },
    }).onClose.subscribe( x => this.getTipoCompetencia(1));
  }

  bajaTipoCompetencia(idTipoCompetencia) {
    this.getTipoCompetenciasById(idTipoCompetencia);
    swal({
      title: '¿Deseas dar de baja este Tipo Competencia?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.tipoCompetencias['activo'] = 0;
        this.tipoCompetenciaService.put(idTipoCompetencia, this.tipoCompetencias).subscribe( result => {
          this.tipoCompetenciaService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El Tipo Competencia se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getTipoCompetencia(0);
        }));
      }
    });
  }

  getTipoCompetenciasById(idTipoCompetencia) {
    this.tipoCompetenciaService.getTipoCompetenciaById(idTipoCompetencia).subscribe(data => {
      this.tipoCompetencias = data;
    });
  }

  ngOnInit() {
    this.connect();
    this.getTipoCompetencia(0);
    this.cols = ['tipo', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.tipoCompetencia.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.tipoCompetencia.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }


}
