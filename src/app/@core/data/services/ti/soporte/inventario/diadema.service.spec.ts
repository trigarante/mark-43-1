import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Diadema Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 11,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    numFolio: '0001DD',
    idMarca: 1,
    fechaRecepcion: '2019-09-24T16:38:35.000+0000',
    fechaSalida: '2019-09-24T16:38:32.000+0000',
    comentarios: 'EXCELENTE',
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    idSede: 1,
    origenRecurso: 'propio',
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'diadema',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/diadema');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/diadema', '/11');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/diadema');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/diadema', '/11/74');
      });
    });
  }
});
