import { Component, OnInit, ViewChild } from '@angular/core';
import { CpuData, Cpu } from '../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import { Monitor, MonitorDate } from '../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import { Laptop, LaptopData } from '../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import { Diadema, DiademaData } from '../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import { Tablet, TabletData } from '../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import { Teclado, TecladoData } from '../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import { Mouse, MouseData } from '../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import * as d3Select from 'd3-selection';
import * as d3Shape from 'd3-shape';
import * as d3Inter from 'd3-interpolate';

@Component({
  selector: 'ngx-reporte-inventario-usuario',
  templateUrl: './reporte-inventario-usuario.component.html',
  styleUrls: ['./reporte-inventario-usuario.component.scss']
})

export class ReporteInventarioUsuarioComponent implements OnInit {
  private margin = {top: 20, right: 20, bottom: 20, left: 20};
  private width: number;
  private height: number;
  cpu: Cpu[];
  monitor: Monitor[];
  laptop: Laptop[];
  diadema: Diadema[];
  tablet: Tablet[];
  teclado: Teclado[];
  mouse: Mouse[];
  dataSource: any;
  rangeDates: Date[];
  cols: any[];
  tipoEquipos: string = 'cpu';
  contadorPermisos: number;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  private svg: any;
  private arc: any;
  private circle: any;
  private circle2: any;
  private rect1: any;
  private rect2: any;
  private rect3: any;
  private start1: any;
  private start2: any;
  private datosMostrar: any;
  private text1: any;
  private text2: any;
  private text3: any;
  private percent1: any;
  private percent2: any;

  constructor(
    private cpuService: CpuData,
    private monitorService: MonitorDate,
    private laptopService: LaptopData,
    private diademaService: DiademaData,
    private tabletService: TabletData,
    private tecladoService: TecladoData,
    private mouseService: MouseData,
  ) {
    this.width = 500 - this.margin.left - this.margin.right;
    this.height = 480 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this.default();
    this.getEquipo();
    this.drawSVG();
    this.rangeDates = [
      new Date(2019, 0, 1),
      new Date(),
    ];
    this.cols = [
      'nombre',
      'usuario',
      'nombreMarca',
    ];
  }

  getEquipo() { // monitor, laptop, tablet, teclado, mouse, diadema
    switch (this.tipoEquipos) {
      case 'cpu':
        this.cpuService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.cpu = data;
          this.dataSource = new MatTableDataSource(this.cpu);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'monitor':
        this.monitorService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.monitor = data;
          this.dataSource = new MatTableDataSource(this.monitor);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'laptop':
        this.laptopService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.laptop = data;
          this.dataSource = new MatTableDataSource(this.laptop);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'tablet':
        this.tabletService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.tablet = data;
          this.dataSource = new MatTableDataSource(this.tablet);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'teclado':
        this.tecladoService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.teclado = data;
          this.dataSource = new MatTableDataSource(this.teclado);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'mouse':
        this.mouseService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.mouse = data;
          this.dataSource = new MatTableDataSource(this.mouse);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'diadema':
        this.diademaService.get().pipe(map(result => {
          return result.filter(data => data);
        })).subscribe(data => {
          this.diadema = data;
          this.dataSource = new MatTableDataSource(this.diadema);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      default: break;
    }
  }

  getEquipos(tipoEquipo) {
    this.tipoEquipos = tipoEquipo.toLowerCase();
    
    if (this.tipoEquipos === 'cpu') {
      this.cpuService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.cpu = data;
        console.log(data);
        this.contadorPermisos = this.cpu.length;
        this.dataSource = new MatTableDataSource(this.cpu);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'cpu';
    } else
    if (this.tipoEquipos === 'monitor') {
      this.monitorService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.monitor = data;
        this.contadorPermisos = this.monitor.length;
        this.dataSource = new MatTableDataSource(this.monitor);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'monitor';
    } else
    if (this.tipoEquipos === 'laptop') {
      this.laptopService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.laptop = data;
        this.contadorPermisos = this.laptop.length;
        this.dataSource = new MatTableDataSource(this.laptop);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'laptop';
    } else
    if (this.tipoEquipos === 'diadema') {
      this.diademaService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.diadema = data;
        this.contadorPermisos = this.diadema.length;
        this.dataSource = new MatTableDataSource(this.diadema);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'diadema';
    } else if (this.tipoEquipos === 'tablet') {
      this.tabletService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.tablet = data;
        this.contadorPermisos = this.tablet.length;
        this.dataSource = new MatTableDataSource(this.tablet);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'tablet';
    } else if (this.tipoEquipos === 'teclado') {
      this.tecladoService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.teclado = data;
        this.contadorPermisos = this.teclado.length;
        this.dataSource = new MatTableDataSource(this.teclado);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'teclado';

    } else if (this.tipoEquipos === 'mouse') {
      this.mouseService.get().pipe(map(result => {
        return result.filter(data => data);
      })).subscribe(data => {
        this.mouse = data;
        this.contadorPermisos = this.mouse.length;
        this.dataSource = new MatTableDataSource(this.mouse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.removedata();
        this.drawdash(data);
      });
      this.tipoEquipos = 'mouse';
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  drawdash(data) {
    this.datosMostrar = this.dataCompose(data);

    const ringAsignado = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[1].length})
      .attr('fill', '#808080')
      .attr('transform', 'translate(115,150)')
      .attr('d', this.arc);

    const ringNoAsignado = this.svg
      .append('path')
      .datum({endAngle: (6.24 / this.datosMostrar[0]) * this.datosMostrar[2].length})
      .attr('fill', '#808080')
      .attr('transform', 'translate(345,150)')
      .attr('d', this.arc);

    ringAsignado.transition()
      .duration(1500)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));

    ringNoAsignado.transition()
      .duration(1500)
      .attrTween('d', (d) => (t) => this.arc(d3Inter.interpolate({startAngle: 0, endAngle: 0}, d)(t)));

    const perc1 = Math.trunc((this.datosMostrar[1].length / this.datosMostrar[0]) * 100 );
    const perc2 = Math.trunc((this.datosMostrar[2].length / this.datosMostrar[0]) * 100 );
    var percActivo = this.svg.append('text').attr('x', 150).attr('y', 60).attr('font-size', 35).attr('id', 'txtValue').text(perc1).attr('fill', '#15aa96');
    const percNactivo = this.svg.append('text').attr('x', 380).attr('y', 60).attr('font-size', 35).attr('id', 'txtValue').text(perc2).attr('fill', '#247eba');


    const txtTotal = this.svg.append('text').attr('x', 70).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[0]).attr('fill','#a4a4a4');
    const txtActivo = this.svg.append('text').attr('x', 220).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[1].length).attr('fill','#a4a4a4');
    const txtNoactivo = this.svg.append('text').attr('x', 355).attr('y', 310).attr('font-size', 35).attr('id', 'txtValue').text(this.datosMostrar[2].length).attr('fill','#a4a4a4');

  }

  drawSVG() {
    this.svg = d3Select.select('#graph').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('viewBox', `${0} ${0} ${this.width} ${this.height}`);

    this.arc = d3Shape.arc()
      .innerRadius(76)
      .outerRadius(80)
      .startAngle(0);

    this.circle = this.svg.append('circle')
      .attr('cx', 115)
      .attr('cy', 150)
      .attr('r', 60)
      .style('fill', '#15aa96')
      .attr('stroke', '#15aa96').style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => this.activos())
      .attr('cursor', 'pointer')
      .on('mouseover', 'fill: #000000');

    this.circle2 = this.svg.append('circle')
      .attr('cx', 345)
      .attr('cy', 150)
      .attr('r', 60)
      .style('fill', '#247eba')
      .attr('stroke', '#247eba').style('stroke-width', 20).style('stroke-opacity', 0.7)
      .on('click', () => this.inactivos())
      .attr('cursor', 'pointer');

    const textA = this.svg.append('text').attr('x', 74).attr('y', 160).text('ACTIVOS').attr('fill', 'white').attr('font-size', 20)
      .on('click', () => this.activos())
      .attr('cursor', 'pointer');
    const textNa = this.svg.append('text').attr('x', 295).attr('y', 160).text('INACTIVOS').attr('fill', 'white').attr('font-size', 20)
      .on('click', () => this.inactivos())
      .attr('cursor', 'pointer');
    this.rect1 = this.svg.append('rect').attr('y', 280).attr('x', 10).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.rect2 = this.svg.append('rect').attr('y', 280).attr('x', 156).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.rect3 = this.svg.append('rect').attr('y', 280).attr('x', 302).attr('height', 50).attr('width', 140).attr('fill', '#e2e2e2').attr('rx', '10');
    this.start1 = this.svg.append('rect').attr('x', 115).attr('y', 20).attr('height', 50).attr('width', 4).attr('fill', '#808080');
    this.start2 = this.svg.append('rect').attr('x', 345).attr('y', 20).attr('height', 50).attr('width', 4).attr('fill', '#808080');
    this.text1 = this.svg.append('text').attr('x', 65).attr('y', 327).attr('font-size', 13).text('Total');
    this.text2 = this.svg.append('text').attr('x', 208).attr('y', 327).attr('font-size', 13).text('Activos');
    this.text3 = this.svg.append('text').attr('x', 340).attr('y', 327).attr('font-size', 13).text('No Activos');
    this.percent1 = this.svg.append('text').attr('x', 125).attr('y', 60).attr('font-size', 30).text('%').attr('fill', '#15aa96');
    this.percent2 = this.svg.append('text').attr('x', 355).attr('y', 60).attr('font-size', 30).text('%').attr('fill', '#247eba');
  }

  dataCompose(d) {
    const tamaño = d.length;
    const noActivos = [];
    const activos = [];

    for (let i = 0; i < tamaño; i++) {
      if (d[i].activo === 0) {
        noActivos.push(d[i]);
      } else if (d[i].activo === 1) {
        activos.push(d[i]);
      }
    }
    return [tamaño, activos, noActivos];

  }

  removedata() {
    this.svg.selectAll('path').remove();
    this.svg.selectAll('#txtValue').remove();
  }

  activos() {
    const data = this.datosMostrar[1];
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  inactivos() {
    const data = this.datosMostrar[2];
    this.contadorPermisos = data.length;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  default () {
    this.cpuService.get().pipe(map(result => {
      return result.filter(data => data);
    })).subscribe(data => {
      this.cpu = data;
      this.contadorPermisos = this.cpu.length;
      this.dataSource = new MatTableDataSource(this.cpu);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.removedata();
      this.drawdash(data);
    });
  }
}
