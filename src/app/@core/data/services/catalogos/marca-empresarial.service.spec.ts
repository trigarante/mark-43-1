import {getTestBed, TestBed} from '@angular/core/testing';
import { MarcaEmpresarialService } from './marca-empresarial.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Global} from '../global';

describe('MarcaEmpresarial service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: MarcaEmpresarialService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = {
    id: 3,
    idSede: 1,
    marca: 'GRUPO TEST',
    detalle: 11,
    codigo: 11,
    activo: 0,
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [MarcaEmpresarialService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(MarcaEmpresarialService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  describe('funsiona servicio correctamente', () => {
    describe('get', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.get.toString());
        chai.request(url)
          .get(go)
          .end((err, res) => {
            console.log('MarcaEmpresarialService get GET 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion get debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.get);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('getMarcaEmpresarialById', () => {
      it('status 200', (done) => {
        const url = validarFuncion();
        const go = validarFuncion(service.getMarcaEmpresarialById.toString());
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        chai.use(chaiHttp);
        chai.request(url)
          .get(go + 1)
          .end((err, res) => {
            console.log('MarcaEmpresarialService getMarcaEmpresarialById 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getMarcaEmpresarialById debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getMarcaEmpresarialById);
              }
            done();
          });
        done();
        setTimeout(done, 500);
      });
    });
    describe('post', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.post.toString());
        chai.request(url)
          .post(go)
          .send(body)
          .end( (err, res) => {
            console.log('MarcaEmpresarialService post POST 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion post debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.post.toString());
              }
            /*expect(res.status).toBe(200);*/
            done();
          });
        setTimeout(done, 500);
      });
    });
    describe('put', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.put.toString());
        chai.request(url)
          .put(go + 3 + '/' + 74)
          .send(body)
          .end( (err, res) => {console.log('MarcaEmpresarialService PUT 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion put debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.put);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
})
