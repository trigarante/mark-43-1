import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaInventarioRedesComponent } from './baja-inventario-redes.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {SwitchsData} from '../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {ServidoresData} from '../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {TarjetaRedData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIpData} from '../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {RacksData} from '../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanelData} from '../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRackData} from '../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';

describe('BajaInventarioRedesComponent', () => {
  let component: BajaInventarioRedesComponent;
  let fixture: ComponentFixture<BajaInventarioRedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaInventarioRedesComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        EstadoInventarioData,
        SwitchsData,
        ServidoresData,
        TarjetaRedData,
        TelefonoIpData,
        RacksData,
        PatchPanelData,
        CharolasRackData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(BajaInventarioRedesComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    /*expect(component).toBeTruthy();*/
  });
});
