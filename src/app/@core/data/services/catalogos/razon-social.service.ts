import {Injectable} from '@angular/core';
import {RazonSocial, RazonSociosData} from '../../interfaces/catalogos/razon-social';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class RazonSocialService extends RazonSociosData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<RazonSocial[]> {
    return this.http.get<RazonSocial[]>(this.baseURL + 'v1/razon-social');
  }

  post(razonSocial: RazonSocial): Observable<RazonSocial> {
    return this.http.post<RazonSocial>(this.baseURL + 'v1/razon-social', razonSocial);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'razonsocial/saveRazonSocial', json);
  }

  getAreaById(idRazonSocial): Observable<RazonSocial> {
    return this.http.get<RazonSocial>(this.baseURL + 'v1/razon-social/' + idRazonSocial);
  }

  put(idRazonSocial, razonSocial: RazonSocial) {
    return this.http.put<RazonSocial>(this.baseURL + 'v1/razon-social/' + idRazonSocial + '/'
      + sessionStorage.getItem('Empleado'), razonSocial);
  }
}
