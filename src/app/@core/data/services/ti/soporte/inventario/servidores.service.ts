import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Servidores, ServidoresData} from '../../../../interfaces/ti/soporte/inventario/servidores';

@Injectable()
export class ServidoresService extends ServidoresData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Servidores[]> {
    return this.http.get<Servidores[]>( this.baseURL + 'v1/servidores');
  }

  post(servidores: Servidores): Observable<Servidores> {
    return this.http.post<Servidores>(this.baseURL + 'v1/servidores', servidores);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'servidores/saveServidores', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajaservidores/saveBajaServidores', json);
  }
  getServidoresById(idServidores): Observable<Servidores> {
    return this.http.get<Servidores>(this.baseURL + 'v1/servidores/' + idServidores);
  }
  put(idServidores, servidores: Servidores): Observable<Servidores> {
    return this.http.put<Servidores>(this.baseURL + 'v1/servidores/' + idServidores + '/'
      + sessionStorage.getItem('Empleado') , servidores);
  }

}


