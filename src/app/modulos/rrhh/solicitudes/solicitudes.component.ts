import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Solicitudes, SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {NbDateService, NbDialogService, NbToastrService} from '@nebular/theme';
import {AgendarCitaComponent} from '../modals/agendar-cita/agendar-cita.component';
import {CheckDocumentosComponent} from '../modals/check-documentos/check-documentos.component';
import {SolicitudCreateComponent} from '../modals/solicitud-create/solicitud-create.component';
import {map} from 'rxjs/operators';
import {TokenStorageService} from '../../../@core/data/services/login/token-storage.service';
import swal from 'sweetalert2';
import {Message} from '../../../@core/data/interfaces/sockets/message';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {Sede, SedeData} from '../../../@core/data/interfaces/catalogos/sede';
import {Vacante, VacanteData} from '../../../@core/data/interfaces/catalogos/vacante';
import {DatosSolicitudesComponent} from '../modals/datos-solicitudes/datos-solicitudes.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TooltipPosition} from '@angular/material';
import {ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
 import * as SockJS from 'sockjs-client';
 import * as Stomp from 'stompjs';
 import {environment} from '../../../../environments/environment';

/**
 * Componente de solicitudes.
 * */
@Component({
  selector: 'ngx-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss'],
})

export class SolicitudesComponent implements OnInit {
  /**Tooltip**/
  solicitudEliminada: boolean;
  positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  solicitudes: Solicitudes[];
  bajaSoli: any;
  cols: any[];
  doc: any[];
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  sedes: Sede[];
  sede: any[];
  vacante: Vacante[];
  vacantes: any[];
  dataSource: any;
  isLoaded: boolean = false;
  isCustomSocketOpened = false;
  messages: Message[] = [];
  permisos: any;
  escritura: boolean;
  private stompClient = null;
  /** El método constructor únicamente manda a llamar Servicios.
   * @param {SolicitudesData} solicitudesService Servicio del componente Solicitudes para poder invocar los métodos.
   * @param token
   * @param dateService
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes).
   * @param {EstadoRrhhData} motivosService Servicio del componente EstadoRrhh para poder invocar los métodos.
   * @param {SedeData} sedeService Servicio del componente sede para poder invocar los métodos. */
  constructor(private solicitudesService: SolicitudesData, private dialogService: NbDialogService, private token: TokenStorageService,
              protected dateService: NbDateService<Date>, private motivosService: EstadoRrhhData,
              protected sedeService: SedeData, private vacanteService: VacanteData,
              private toastrService: NbToastrService) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  /** Web socket connection **/
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'solicitudes');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame){
      _this.stompClient.subscribe('/task/panel/1', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getSolicitudes(0);
        }, 400);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  /** Función que obtiene las sedes disponibles y las asigna al arreglo sedes. */
  getSedes() {
    this.sedeService.get().subscribe(result => {
      this.sedes = result;
      this.sede = [];
      for (let i = 0; i < this.sedes.length; i++) {
        this.sede.push({'label': this.sedes[i].nombre, 'value': this.sedes[i].id});
      }
      this.sede.push({'label': 'todos', 'value': '0'});
    });
  }

  // /** Función que obtiene las vacantes con base en la sede y las asigna al arreglo vacantes. */
  // getVacanteBySede(value) {
  //   this.vacantesService.get().pipe(map(result => {
  //     return result.filter( data => data.activo === 1 && data.idSede === value);
  //   })).subscribe( valor => {
  //     this.vacantes = valor;
  //   });
  // }


  /** Con esta función se obtienen las solicitudes existentes en el servidor. */
  getSolicitudes(mensajeAct) {
    this.solicitudesService.get().pipe(map(result => {
      return result.filter(data => data.estado === '1' && data.idEtapa === 0);
    })).subscribe(data => {
      this.solicitudes = data;
      this.dataSource = new MatTableDataSource(this.solicitudes);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      // dataSource = new MatTableDataSource(solicitudes);
      this.doc = [];

      if (this.solicitudes.length === 0) {
        swal.fire({
          type: 'error',
          title: 'Oops',
          text: 'No tienes solicitudes por mostrar',
        });
      } else {
        for (let i = 0; i < this.solicitudes.length; i++) {
          this.doc.push(JSON.parse(this.solicitudes[i].documentos));
          for (let j = 0; j < this.doc.length; j++) {
            if (this.doc[j] != null) {
              if (this.doc[j].rfc === true && this.doc[j].curp === true && this.doc[j].imss === true &&
                this.doc[j].actaNacimiento === true && this.doc[j].comprobanteEstudios === true &&
                this.doc[j].comprobanteDomicilio === true && this.doc[j].identificacionOficial === true) {
                this.solicitudes[i].documentos = 'completo';
              } else {
                this.solicitudes[i].documentos = 'incompleto';
              }
            } else {
              this.solicitudes[i].documentos = 'incompleto';
            }
          }
        }
      }
    }, () => {
        swal.fire({
          type: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      },
      () => {
        if (mensajeAct === 1 && this.solicitudes.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Solicitudes`);
        }
      });
  }
  /** Función que obtiene los motivos de baja. */
  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }
  /** Esta función muestra un modal el cual permite establecer la fecha y hora de la cita.
   * @param {number} idSolicitud ID de la solicitud seleccionada en la tabla.
   * */
  agendarCita(idSolicitud: number) {
    this.dialogService.open(AgendarCitaComponent, {
      context: {
        idSolicitud: idSolicitud,
      },
    }).onClose.subscribe(e => {
      this.getSolicitudes(1);
    });
  }
  /** Función que desprende un modal para dar de baja una solicitud. */
  async bajaSolicitud(idSolicitud) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }

    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal.fire({
      title: '¿Deseas dar de baja esta solicitud?',
      text: 'Una vez dada de baja la solicitud, no podrás verla de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada una opción, debe agregar un comentario
      const {value: comentarios} = await swal.fire({
        title: 'Comentarios',
        text: 'Añade un comentario para esta solicitud',
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Dar de baja',
        cancelButtonText: 'Cancelar',
        inputValidator: (value) => {
          if (!value) {
            return 'Por favor, escribe un comentario.';
          }
        },
      });
      if (comentarios) {
        this.bajaSoli = {'estado': Number(motivo), 'comentarios': comentarios.toString().toUpperCase()};
        this.solicitudesService.getSolicitudById(idSolicitud).subscribe( data => {
          if (data.estado === '1' && data.idEtapa === 0) {
            this.solicitudesService.bajaSolicitud(idSolicitud, this.bajaSoli).subscribe(result => {
              this.solicitudesService.postSocket({idSolicitud: idSolicitud, bajaSoli: this.bajaSoli}).subscribe(d => {});
            });
            swal.fire({
              type: 'success',
              title: '¡La solicitud se ha dado de baja exitosamente!',
            }).then((resultado => {
              // window.location.reload();
              this.getSolicitudes(0);
            }));
          } else {
            this.solicitudEliminada = true;
            swal.fire({
              type: 'warning',
              title: '¡Esta solicitud ya ha sido eliminada!',
            }).then((resultado => {
              // window.location.reload();
              this.getSolicitudes(0);
            }));
          }
        });
      }
    }
  }
  /** Función que permite visualizar un modal con todos los documentos (listados con checkbox) y guardar aquellos que han sido entregados.
   * @param {number} idSolicitud ID de la solicitud que ha sido seleccionada en la tabla.
   * */
  checkDocumentos(idSolicitud: number) {
    this.dialogService.open(CheckDocumentosComponent, {
      context: {
        idSolicitud: idSolicitud,
      },
    }).onClose.subscribe(yy => {
      this.getSolicitudes(1);
    });
  }
  /** Función que despliega un modal para poder actualizar la solicitud. */
  actualizarSolicitud(idSolicitud: number, idVacante) {
    this.dialogService.open(SolicitudCreateComponent, {
      context: {
        idTipo: 1,
        idSolicitud: idSolicitud,
        idVacante: idVacante,
      },
    }).onClose.subscribe(x => {
      this.getSolicitudes(1);
    });
  }
  /** Función que actualiza la información del candidato dependiendo de su ID. */
  verDatos(idSolicitud: number) {
    this.dialogService.open(DatosSolicitudesComponent, {
      context: {
        idSolicitud: idSolicitud,
      }
      ,
    });
  }
  /** Función que inicializa el componente, mandando a llamar a getSolicitudes para llenar la tabla. */
  ngOnInit() {
    this.connect();
    // this.getMotivosBaja();
    // this.getSolicitudes(0);
    // this.getSedes();
    // this.cols = ['id', 'nombre', 'vacante', 'empleado', 'sede', 'fechaCita', 'acciones'];
    // this.getPermisos();
    this.getMotivosBaja();
    this.getSedes();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/solicitudes') {
    this.getSolicitudes(0);
    //   }
    // }, 1000);
    this.cols = ['id', 'nombre', 'vacante', 'empleado', 'sede', 'fechaCita', 'acciones'];
    this.getPermisos();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.solicitudes.escritura;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
