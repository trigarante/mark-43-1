import {Component, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NbDialogService} from '@nebular/theme';
import {ActivatedRoute, Router} from '@angular/router';
import {DiscoDuroExterno, DiscoDuroExternoData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {
  DiscoDuroInterno,
  DiscoDuroInternoData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBit, ChromeBitData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {Aps, ApsData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {
  Biometricos,
  BiometricosData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {Dvr, DvrData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {Camaras, CamarasData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {
  BarrasMulticontacto,
  BarrasMulticontactoData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {
  FuenteCamaras,
  FuenteCamarasData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import {DiscoDuroExternoCreateComponent} from '../modals/disco-duro-externo-create/disco-duro-externo-create.component';
import {ChromeBitCreateComponent} from '../modals/chrome-bit-create/chrome-bit-create.component';
import {ApsCreateComponent} from '../modals/aps-create/aps-create.component';
import {BiometricosCreateComponent} from '../modals/biometricos-create/biometricos-create.component';
import {DvrCreateComponent} from '../modals/dvr-create/dvr-create.component';
import {CamaraCreateComponent} from '../modals/camara-create/camara-create.component';
import {BarraMulticontactoCreateComponent} from '../modals/barra-multicontacto-create/barra-multicontacto-create.component';
import {CorrienteCamaraCreateComponent} from '../modals/corriente-camara-create/corriente-camara-create.component';
import {DiscoDuroInternoCreateComponent} from '../modals/disco-duro-interno-create/disco-duro-interno-create.component';
import {AsignarEquipoComponent} from '../modals/asignar-equipo/asignar-equipo.component';
import swal from 'sweetalert';
import swal2 from 'sweetalert2';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-inventario-soporte',
  templateUrl: './inventario-soporte.component.html',
  styleUrls: ['./inventario-soporte.component.scss'],
})
export class InventarioSoporteComponent implements OnInit {
  /** variable que toma la informacion del url donde indica que equipo es  */
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;
  /** Variable que inicializa las columnas*/
  cols: any[];
  dataSource: any;
  /** Variable para el arreglo discoduroExterno*/
  ddexterno: DiscoDuroExterno[];
  /** Variable para el arreglo discoduroInterno*/
  ddinterno: DiscoDuroInterno[];
  chromeBit: ChromeBit[];
  aps: Aps[];
  biometricos: Biometricos[];
  dvr: Dvr[];
  camara: Camaras[];
  barraMulti: BarrasMulticontacto[];
  corrienteCam: FuenteCamaras[];
  /** Variable para desactivar el arreglo discoduroExterno*/
  ddexternodesactivar: DiscoDuroExterno;
  ddinternodesactivar: DiscoDuroInterno;
  chromeBitdesactivar: ChromeBit;
  apsdesactivar: Aps;
  biometricosdesactivar: Biometricos;
  dvrdesactivar: Dvr;
  camaradesactivar: Camaras;
  barraMultidesactivar: BarrasMulticontacto;
  corrienteCamdesactivar: FuenteCamaras;
  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  tipoEquipos: string;
  // sockets
  actualizar: string;
  private stompClient1 = null;
  private stompClient2 = null;
  private stompClient3 = null;
  private stompClient4 = null;
  private stompClient5 = null;
  private stompClient6 = null;
  private stompClient7 = null;
  private stompClient8 = null;
  private stompClient9 = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  constructor(
    private fb: FormBuilder,
    public dialogService: NbDialogService,
    public ddexternoService: DiscoDuroExternoData,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    public ddinternoService: DiscoDuroInternoData,
    private chromeBitServices: ChromeBitData,
    private apsService: ApsData,
    private biometricosService: BiometricosData,
    private dvrService: DvrData,
    private camaraService: CamarasData,
    private barraMultiService: BarrasMulticontactoData,
    private fuenteCorrienteService: FuenteCamarasData,
    private estadoInventarioServive: EstadoInventarioData,
  ) {}
  connect() {
    const _this = this;
    /**----------------------discoduroexterno-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'discoduroexterno');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, (frame) => {
      _this.stompClient1.subscribe('/task/panelDiscoDuroExterno', (content) => {
        if (this.actualizar === 'discoduroexterno' || this.tipoEquipo === 'discoduroexterno') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('discoduroexterno');
            _this.getEquipo();
          }, 500);
        }
      });
    });
    /**----------------------disco duro interno-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'discodurointerno');
    this.stompClient2 = Stomp.over(socket1);
    this.stompClient2.connect({}, (frame) => {
      _this.stompClient2.subscribe('/task/panelDiscoDuroInterno', (content) => {
        if (this.actualizar === 'disco duro interno') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('disco duro interno');
          }, 500);
        }
      });
    });
    /**----------------------chrome bit-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'chromebit');
    this.stompClient3 = Stomp.over(socket2);
    this.stompClient3.connect({}, (frame) => {
      _this.stompClient3.subscribe('/task/panelChromeBit', (content) => {
        if (this.actualizar === 'chrome bit') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('chrome bit');
          }, 500);
        }
      });
    });
    /**----------------------aps-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'aps');
    this.stompClient4 = Stomp.over(socket3);
    this.stompClient4.connect({}, (frame) => {
      _this.stompClient4.subscribe('/task/panelAps', (content) => {
        if (this.actualizar === 'aps') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('aps');
          }, 500);
        }
      });
    });
    /**----------------------biometricos-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'biometricos');
    this.stompClient5 = Stomp.over(socket4);
    this.stompClient5.connect({}, (frame) => {
      _this.stompClient5.subscribe('/task/panelBiometricos', (content) => {
        if (this.actualizar === 'biometricos') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('biometricos');
          }, 500);
        }
      });
    });
    /**----------------dvr-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'dvr');
    this.stompClient6 = Stomp.over(socket5);
    this.stompClient6.connect({}, (frame) => {
      _this.stompClient6.subscribe('/task/panelDvr', (content) => {
        if (this.actualizar === 'dvr') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('dvr');
          }, 500);
        }
      });
    });
    /**----------------camara-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'camara');
    this.stompClient7 = Stomp.over(socket6);
    this.stompClient7.connect({}, (frame) => {
      _this.stompClient7.subscribe('/task/panelCamara', (content) => {
        if (this.actualizar === 'camara') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('camara');
          }, 500);
        }
      });
    });
    /**----------------barra-multicontacto-------------------**/
    const socket7 = new SockJS(environment.GLOBAL_SOCKET + 'barramulticontacto');
    this.stompClient8 = Stomp.over(socket7);
    this.stompClient8.connect({}, (frame) => {
      _this.stompClient8.subscribe('/task/panelBarraMultiContacto', (content) => {
        if (this.actualizar === 'barra-multicontacto') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('barra-multicontacto');
          }, 500);
        }
      });
    });
    /**----------------corriente-camara-------------------**/
    const socket8 = new SockJS(environment.GLOBAL_SOCKET + 'corrientecamara');
    this.stompClient9 = Stomp.over(socket8);
    this.stompClient9.connect({}, (frame) => {
      _this.stompClient9.subscribe('/task/panelCorrienteCamara', (content) => {
        if (this.actualizar === 'corriente-camara') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('corriente-camara');
          }, 500);
        }
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  ngOnInit() {
    this.connect();
    this.getEstado();
    this.cols = ['detalle', 'id', 'nombre', 'usuario', 'folio', 'marca', 'activo', 'acciones'];
    this.getEquipo();
    this.getPermisos();
    this.estaActivo();
  }

  /** Funcion que elige que tabla a mostrar en el Tap deacuerdo con la variable TipoEquipo recogida de la Url*/
  getEquipo() {

    switch (this.tipoEquipo) {
      /**Mostrara la tabla de discos duros externos*/
      case 'discoduroexterno':
        this.ddexternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.ddexterno = data;
          this.dataSource = new MatTableDataSource(this.ddexterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'disco duro externo':
        this.ddexternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.ddexterno = data;
          this.dataSource = new MatTableDataSource(this.ddexterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      /**Mostrara la tabla de discos duros internos*/
      case 'discoduroInterno':
        this.ddinternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.ddinterno = data;
          this.dataSource = new MatTableDataSource(this.ddinterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        break;
      case 'disco duro interno':
        this.ddinternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.ddinterno = data;
          this.dataSource = new MatTableDataSource(this.ddinterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });

        break;
      /**Mostrara la tabla de chrome bit*/
      case 'chromeBit':
        this.chromeBitServices.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.chromeBit = data;
          this.dataSource = new MatTableDataSource(this.chromeBit);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'chrome bit':
        this.chromeBitServices.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.chromeBit = data;
          this.dataSource = new MatTableDataSource(this.chromeBit);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      /**Mostrara la tabla de aps*/
      case 'aps':
        this.apsService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.aps = data;
          this.dataSource = new MatTableDataSource(this.aps);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      /**Mostrara la tabla biometricos*/
      case 'biometricos':
        this.biometricosService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.biometricos = data;
          this.dataSource = new MatTableDataSource(this.biometricos);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      /**Mostrara la tabla de dvrs*/
      case 'dvr':
        this.dvrService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.dvr = data;
          this.dataSource = new MatTableDataSource(this.dvr);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      /**Mostrara la tabla de camaras*/
      case 'camara':
        this.camaraService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.camara = data;
          this.dataSource = new MatTableDataSource(this.camara);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      /**Mostrara la tabla de barra de multicontactos*/
      case 'barramulticontacto':
        this.barraMultiService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.barraMulti = data;
          this.dataSource = new MatTableDataSource(this.barraMulti);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      case 'barra-multicontacto':
        this.barraMultiService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.barraMulti = data;
          this.dataSource = new MatTableDataSource(this.barraMulti);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      /**Mostrara la tabla de fuente corriente camaras */
      case 'corrientecamara':
        this.fuenteCorrienteService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.corrienteCam = data;
          this.dataSource = new MatTableDataSource(this.corrienteCam);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
      break;
      case 'corriente-camara':
        this.fuenteCorrienteService.get().pipe(map(result => {
          return result.filter(data => data.activo === 1);
        })).subscribe(data => {
          this.corrienteCam = data;
          this.dataSource = new MatTableDataSource(this.corrienteCam);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        break;
      default: break;
    }
    this.tipoEquipo = this.tipoEquipo;
  }

  /** Funcion que elige que tabla mostrar en el Tap deacuerdo seleccionando la pestaña correspondiente*/
  getEquipos(tipoEquipo) {
    tipoEquipo = tipoEquipo.toLowerCase();
    this.actualizar = tipoEquipo;
    this.tipoEquipo = tipoEquipo;
    /**Obtiene los datos del Disco duro externo*/
    if (tipoEquipo === 'disco duro externo') {
      this.ddexternoService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.ddexterno = data;
        this.dataSource = new MatTableDataSource(this.ddexterno);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'disco duro externo';
      /**Obtiene los datos de disco duro interno*/
    } else if (tipoEquipo === 'disco duro interno') {
      this.ddinternoService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.ddinterno = data;
        this.dataSource = new MatTableDataSource(this.ddinterno);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'disco duro interno';

    } else if (tipoEquipo === 'chrome bit') {
      this.chromeBitServices.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.chromeBit = data;
        this.dataSource = new MatTableDataSource(this.chromeBit);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'chrome bit';

    } else if (tipoEquipo === 'aps') {
      this.apsService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.aps = data;
        this.dataSource = new MatTableDataSource(this.aps);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'aps';

    } else if (tipoEquipo === 'biometricos') {
      this.biometricosService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.biometricos = data;
        this.dataSource = new MatTableDataSource(this.biometricos);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'biometricos';

    } else if (tipoEquipo === 'dvr') {
      this.dvrService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.dvr = data;
        this.dataSource = new MatTableDataSource(this.dvr);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'dvr';

    } else if (tipoEquipo === 'camara') {
      this.camaraService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.camara = data;
        this.dataSource = new MatTableDataSource(this.camara);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'camara';

    } else if (tipoEquipo === 'barra-multicontacto') {
      this.barraMultiService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.barraMulti = data;
        this.dataSource = new MatTableDataSource(this.barraMulti);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'barra-multicontacto';

    } else if (tipoEquipo === 'corriente-camara') {
      this.fuenteCorrienteService.get().pipe(map(result => {
        return result.filter(data => data.activo === 1);
      })).subscribe(data => {
        this.corrienteCam = data;
        this.dataSource = new MatTableDataSource(this.corrienteCam);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      this.tipoEquipo = 'corriente-camara';
    }
  }

  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }

  /** funcion que envia a un modal para crear un HDD Interno*/
  crearHDDI() {
    this.dialogService.open(DiscoDuroInternoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar un HDD Interno*/
  modificarHDDI(idhddi) {
    this.dialogService.open(DiscoDuroInternoCreateComponent, {
      context: {
        idTipo: 2,
        idhddi: idhddi,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  // /** funcion que envia a un modal para crear un HDD Externo*/
  crearHDDE() {
    this.dialogService.open(DiscoDuroExternoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  // /** funcion que envia a un modal para modificar un HDD Externo*/
  modificarHDDE(idhdde) {
    this.dialogService.open(DiscoDuroExternoCreateComponent, {
      context: {
        idTipo: 2,
        idhdde: idhdde,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para crear un Chrome Bit*/
  crearChrBt() {
    this.dialogService.open(ChromeBitCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar un Chrome Bit*/
  modificarChrBt(idChrBt) {
    this.dialogService.open(ChromeBitCreateComponent, {
      context: {
        idTipo: 2,
        idChrBt: idChrBt,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para crear un APS*/
  crearAPS() {
    this.dialogService.open(ApsCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar un APS*/
  modificarAPS(idAps) {
    this.dialogService.open(ApsCreateComponent, {
      context: {
        idTipo: 2,
        idAps: idAps,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para crear un BIOMETRICOS*/
  crearBiometricos() {
    this.dialogService.open(BiometricosCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar un BIOMETRICOS*/
  modificarBiometricos(idBiometricos) {
    this.dialogService.open(BiometricosCreateComponent, {
      context: {
        idTipo: 2,
        idBiometricos: idBiometricos,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para crear un Dvr*/
  crearDvr() {
    this.dialogService.open(DvrCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar un Dvr*/
  modificarDvr(idDvr) {
    this.dialogService.open(DvrCreateComponent, {
      context: {
        idTipo: 2,
        idDvr: idDvr,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  /** funcion que envia a un modal para crear una camara*/
  crearCamaras() {
    this.dialogService.open(CamaraCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar una camara*/
  modificarCamaras(idCamaras) {
    this.dialogService.open(CamaraCreateComponent, {
      context: {
        idTipo: 2,
        idCamaras: idCamaras,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

   /** funcion que envia a un modal para crear una barra multicontacto*/
  crearBarraMulti() {
    this.dialogService.open(BarraMulticontactoCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar una barra multicontacto*/
  modificarBarraMulti(idBarraMulticontactos) {
    this.dialogService.open(BarraMulticontactoCreateComponent, {
      context: {
        idTipo: 2,
        idBarraMulticontactos: idBarraMulticontactos,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

   /** funcion que envia a un modal para crear una Fuente de Corriente*/
  crearFuenteCorriente() {
    this.dialogService.open(CorrienteCamaraCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** funcion que envia a un modal para modificar una Fuente de Corriente*/
  modificarFuenteCorriente(idFuenteCorriente) {
    this.dialogService.open(CorrienteCamaraCreateComponent, {
      context: {
        idTipo: 2,
        idFuenteCorriente: idFuenteCorriente,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }
  /** Funcion que manda un modal para asignar un equipo a un empleado*/
  asignarEquipo(id, tipoEquipos) {
    this.dialogService.open(AsignarEquipoComponent, {
      context: {
        id: id,
        tipoEquipos: tipoEquipos,
      },
    }).onClose.subscribe(x => {
      this.getEquipo();
    });
  }

  getDiscoDuroExternoById(id) {
    this.ddexternoService.getDiscoExternoById(id).subscribe(data => {
      this.ddexternodesactivar = data;
    });
  }
  getDiscoDuroInternoById(id) {
    this.ddinternoService.getDiscoInternoById(id).subscribe(data => {
      this.ddinternodesactivar = data;
    });
  }
  getChromeBitById(id) {
    this.chromeBitServices.getChromeBit(id).subscribe(data => {
      this.chromeBitdesactivar = data;
    });
  }
  getApsIpById(id) {
    this.apsService.getApsById(id).subscribe(data => {
      this.apsdesactivar = data;
    });
  }
  getBiometricById(id) {
    this.biometricosService.getBiometricosById(id).subscribe(data => {
      this.biometricosdesactivar = data;
    });
  }
  getDvrById(id) {
    this.dvrService.getDvrById(id).subscribe(data => {
      this.dvrdesactivar = data;
    });
  }
  getCamarasById(id) {
    this.camaraService.getCamarasById(id).subscribe(data => {
      this.camaradesactivar = data;
    });
  }
  getBarrasMulticontactoById(id) {
    this.barraMultiService.getBarrasMulticontactoById(id).subscribe(data => {
      this.barraMultidesactivar = data;
    });
  }
  getCorrienteCamaraById(id) {
    this.fuenteCorrienteService.getFuenteCamaraById(id).subscribe(data => {
      this.corrienteCamdesactivar = data;
    });
  }

  /**funcion para desasingnar el equipo de un empleado*/
  desasignarEquipo(idEquipo) {

    switch (this.tipoEquipo) {
      case 'discoduroexterno':
        this.getDiscoDuroExternoById(idEquipo);
        break;
      case 'disco duro interno':
        this.getDiscoDuroInternoById(idEquipo);
        break;
      case 'chrome bit':
        this.getChromeBitById(idEquipo);
        break;
      case 'aps':
        this.getApsIpById(idEquipo);
        break;
      case 'biometricos':
        this.getBiometricById(idEquipo);
        break;
      case 'dvr':
        this.getDvrById(idEquipo);
        break;
      case 'camara':
        this.getCamarasById(idEquipo);
        break;
      case 'barra-multicontacto':
        this.getBarrasMulticontactoById(idEquipo);
        break;
      case 'corriente-camara':
        this.getCorrienteCamaraById(idEquipo);
        break;
      default:
        break;
    }
    swal({
      title: '¿Deseas desvincular este equipo?',
      text: '.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Desvincular',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        switch (this.tipoEquipo) {
          case 'discoduroexterno':
            this.ddexternodesactivar['idEmpleado'] = null;
            this.ddexternoService.put(idEquipo, this.ddexternodesactivar).subscribe(result => {
              this.ddexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'disco duro externo':
            this.ddexternodesactivar['idEmpleado'] = null;
            this.ddexternoService.put(idEquipo, this.ddexternodesactivar).subscribe(result => {
              this.ddexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'disco duro interno':
            this.ddinternodesactivar['idEmpleado'] = null;
            this.ddinternoService.put(idEquipo, this.ddinternodesactivar).subscribe(result => {
              this.ddinternoService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'chrome bit':
            this.chromeBitdesactivar['idEmpleado'] = null;
            this.chromeBitServices.put(idEquipo, this.chromeBitdesactivar).subscribe(result => {
              this.chromeBitServices.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'aps':
            this.apsdesactivar['idEmpleado'] = null;
            this.apsService.put(idEquipo, this.apsdesactivar).subscribe(result => {
              this.apsService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'biometricos':
            this.biometricosdesactivar['idEmpleado'] = null;
            this.biometricosService.put(idEquipo, this.biometricosdesactivar).subscribe(result => {
              this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'dvr':
            this.dvrdesactivar['idEmpleado'] = null;
            this.dvrService.put(idEquipo, this.dvrdesactivar).subscribe(result => {
              this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'camara':
            this.camaradesactivar['idEmpleado'] = null;
            this.camaraService.put(idEquipo, this.camaradesactivar).subscribe(result => {
              this.camaraService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'barra-multicontacto':
            this.barraMultidesactivar['idEmpleado'] = null;
            this.barraMultiService.put(idEquipo, this.barraMultidesactivar).subscribe(result => {
              this.barraMultiService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          case 'corriente-camara':
            this.corrienteCamdesactivar['idEmpleado'] = null;
            this.fuenteCorrienteService.put(idEquipo, this.corrienteCamdesactivar).subscribe(result => {
              this.fuenteCorrienteService.postSocket({status: 'ok'}).subscribe(x => {});
            });
            break;
          default:
            break;
        }
        swal('¡El equipo se ha desvinculado exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getEquipo();
        }));
      }
    });
  }

  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 0);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({
          'id': this.estadoInventariodata[i].id,
          'value': this.estadoInventariodata[i].estado,
        });

      }
    });
  }
  /** Función que desprende un modal para dar de baja un Equipo. */
  async eliminarEquipo(idEquipo, tipoEquipo) {

    /** switch de seleccion dependiendo el tipo de Equipo que de el usuario hara la busqueda por idEquipo*/

    switch (this.tipoEquipo) {
      case 'discoduroexterno':
        this.getDiscoDuroExternoById(idEquipo);
        break;
      case 'disco duro externo':
        this.getDiscoDuroExternoById(idEquipo);
        break;
      case 'disco duro interno':
        this.getDiscoDuroInternoById(idEquipo);
        break;
      case 'chrome bit':
        this.getChromeBitById(idEquipo);
        break;
      case 'aps':
        this.getApsIpById(idEquipo);
        break;
      case 'biometricos':
        this.getBiometricById(idEquipo);
        break;
      case 'dvr':
        this.getDvrById(idEquipo);
        break;
      case 'camara':
        this.getCamarasById(idEquipo);
        break;
      case 'barra-multicontacto':
        this.getBarrasMulticontactoById(idEquipo);
        break;
      case 'corriente-camara':
        this.getCorrienteCamaraById(idEquipo);
        break;
      default:
        break;
    }

    /** Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,*/
    /** correspondiendo con los obtenidos desde el microservicio*/
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de baja este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { /** Si ya ha sido seleccionada una opción, debe de desactivar*/

      /**buscara por el tipo de Equipo y modificara el equipo*/
      switch (this.tipoEquipo) {
        case 'discoduroexterno':
          this.ddexternodesactivar['idEstadoInventario'] = Number(motivo);
          this.ddexternodesactivar['activo'] = 0;
          this.ddexternoService.put(idEquipo, this.ddexternodesactivar).subscribe(result => {
            this.ddexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.ddexternoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'disco duro externo':
          this.ddexternodesactivar['idEstadoInventario'] = Number(motivo);
          this.ddexternodesactivar['activo'] = 0;
          this.ddexternoService.put(idEquipo, this.ddexternodesactivar).subscribe(result => {
            this.ddexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.ddexternoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'disco duro interno':
          this.ddinternodesactivar['idEstadoInventario'] = Number(motivo);
          this.ddinternodesactivar['activo'] = 0;
          this.ddinternoService.put(idEquipo, this.ddinternodesactivar).subscribe(result => {
            this.ddinternoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.ddinternoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'chrome bit':
          this.chromeBitdesactivar['idEstadoInventario'] = Number(motivo);
          this.chromeBitdesactivar['activo'] = 0;
          this.chromeBitServices.put(idEquipo, this.chromeBitdesactivar).subscribe(result => {
            this.chromeBitServices.postSocket({status: 'ok'}).subscribe(x => {});
            this.chromeBitServices.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'aps':
          this.apsdesactivar['idEstadoInventario'] = Number(motivo);
          this.apsdesactivar['activo'] = 0;
          this.apsService.put(idEquipo, this.apsdesactivar).subscribe(result => {
            this.apsService.postSocket({status: 'ok'}).subscribe(x => {});
            this.apsService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'biometricos':
          this.biometricosdesactivar['idEstadoInventario'] = Number(motivo);
          this.biometricosdesactivar['activo'] = 0;
          this.biometricosService.put(idEquipo, this.biometricosdesactivar).subscribe(result => {
            this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
            this.biometricosService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'dvr':
          this.dvrdesactivar['idEstadoInventario'] = Number(motivo);
          this.dvrdesactivar['activo'] = 0;
          this.dvrService.put(idEquipo, this.dvrdesactivar).subscribe(result => {
            this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
            this.dvrService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'camara':
          this.camaradesactivar['idEstadoInventario'] = Number(motivo);
          this.camaradesactivar['activo'] = 0;
          this.camaraService.put(idEquipo, this.camaradesactivar).subscribe(result => {
            this.camaraService.postSocket({status: 'ok'}).subscribe(x => {});
            this.camaraService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'barra-multicontacto':
          this.barraMultidesactivar['idEstadoInventario'] = Number(motivo);
          this.barraMultidesactivar['activo'] = 0;
          this.barraMultiService.put(idEquipo, this.barraMultidesactivar).subscribe(result => {
            this.barraMultiService.postSocket({status: 'ok'}).subscribe(x => {});
            this.barraMultiService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'corriente-camara':
          this.corrienteCamdesactivar['idEstadoInventario'] = Number(motivo);
          this.corrienteCamdesactivar['activo'] = 0;
          this.fuenteCorrienteService.put(idEquipo, this.corrienteCamdesactivar).subscribe(result => {
            this.fuenteCorrienteService.postSocket({status: 'ok'}).subscribe(x => {});
            this.fuenteCorrienteService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de baja exitosamente!',
      }).then((resultado => {

        this.getEquipo();
      }));

    }
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.inventarioSoporte.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.inventarioSoporte.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
