import {Component, Input, OnInit} from '@angular/core';
import {EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {NbDialogRef} from '@nebular/theme';
import {CapacitacionComponent} from '../../capacitacion/capacitacion.component';
import * as moment from 'moment';

@Component({
  selector: 'ngx-datos-empleado',
  templateUrl: './datos-empleado.component.html',
  styleUrls: ['./datos-empleado.component.scss'],
})
export class DatosEmpleadoComponent implements OnInit {
  @Input() idEmpleado: number;
  empleado: any;
  nombre: any;
  email: any;
  movil: any;
  fijo: any;
  fechaIngreso: any;
  subarea: any;
  puesto: any;
  sede: any;
  empresa: any;
  grupo: any;

  constructor(
    protected empleadosService: EmpleadosData,
    protected ref: NbDialogRef<CapacitacionComponent>,
  ) { }

  getEmpleadoPersonalesById() {
    this.empleadosService.getEmpleadoById(this.idEmpleado).subscribe(data => {
      this.empleado = data;
      this.nombre = this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno;
      this.email = this.empleado.email;
      this.movil = this.empleado.telefonoMovil;
      this.fijo = this.empleado.telefonoFijo;
      this.fechaIngreso = moment(this.empleado.fechaIngreso).format('DD/MM/YYYY');
      this.subarea = this.empleado.subarea;
      this.puesto = this.empleado.puesto;
      this.sede = this.empleado.sede;
      this.empresa = this.empleado.empresa;
      this.grupo = this.empleado.grupo;
    });
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getEmpleadoPersonalesById();
  }

}
