import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {PatchPanel, PatchPanelData} from '../../../../interfaces/ti/soporte/inventario/patch-panel';


@Injectable()
export class PatchPanelService extends PatchPanelData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<PatchPanel[]> {
    return this.http.get<PatchPanel[]>( this.baseURL + 'v1/patchPanel');
  }

  post(patchPanel: PatchPanel): Observable<PatchPanel> {
    return this.http.post<PatchPanel>(this.baseURL + 'v1/patchPanel', patchPanel);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'patchpanel/savePatchPanel', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajapatchpanel/saveBajaPatchPanel', json);
  }
  getPatchPanelById(idPatchPanel): Observable<PatchPanel> {
    return this.http.get<PatchPanel>(this.baseURL + 'v1/patchPanel/' + idPatchPanel);
  }
  put(idPatchPanel, patchPanel: PatchPanel): Observable<PatchPanel> {
    return this.http.put<PatchPanel>(this.baseURL + 'v1/patchPanel/' + idPatchPanel + '/'
      + sessionStorage.getItem('Empleado') , patchPanel);
  }
}
