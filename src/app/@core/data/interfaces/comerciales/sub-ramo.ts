import {Observable} from 'rxjs';
export interface SubRamo {
  id: number;
  idRamo: number;
  idTipoSubRamo: number;
  prioridad: number;
  descripcion: string;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  tipoSubRamo: any;
  tipoRamo: any;
}
export abstract class SubramoData {
  abstract get(): Observable<SubRamo[]>;

  abstract post(subramo): Observable<SubRamo>;
  abstract postSocket(json): Observable<JSON>;

  abstract getSubramoById(idSubramo): Observable<SubRamo>;

  abstract put(idSubramo, subramo): Observable<SubRamo>;
}
