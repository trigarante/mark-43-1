import { Component, OnInit, ViewChild } from '@angular/core';
import { TipoPagina, TipoPaginaData } from '../../../../@core/data/interfaces/catalogos/tipo-pagina';
import {NbDialogService} from '@nebular/theme';
import swal from 'sweetalert';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TipoPaginaCreateComponent} from '../modals/tipo-pagina-create/tipo-pagina-create.component';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-tipo-pagina',
  templateUrl: './tipo-pagina.component.html',
  styleUrls: ['./tipo-pagina.component.scss'],
})
export class TipoPaginaComponent implements OnInit {
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  tipoPagina: TipoPagina[];
  tipoPaginas: TipoPagina;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  // sockets
  private stompClient = null;
  constructor(private tipoPaginaService: TipoPaginaData, private dialogoService: NbDialogService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'tipopaginas');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelTipoPaginas', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getTipoPagina();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getTipoPagina() {
    this.tipoPaginaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);

    })).subscribe(data => {
      this.tipoPagina = [];
      this.tipoPagina = data;
      this.dataSource = new MatTableDataSource(this.tipoPagina); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  crearTipoPaginas() {
    this.dialogoService.open(TipoPaginaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getTipoPagina();
    });
  }

  updateTipoPaginas(idtipoPagina) {
    this.dialogoService.open(TipoPaginaCreateComponent, {
      context: {
        idTipo: 2,
        idTipoPagina: idtipoPagina,
      },
    }).onClose.subscribe(x => {
      this.getTipoPagina();
    });
  }

  bajaTipoPagina(idTipoPagina) {
    {
      this.getTipoPaginaById(idTipoPagina);
      swal({
        title: '¿Deseas dar de baja esta Página?',
        text: 'Una vez dado de baja, no podrás verlo de nuevo.',
        icon: 'warning',
        dangerMode: true,
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: false,
            visible: true,
            className: '',
            closeModal: true,
          },
          confirm: {
            text: 'Dar de baja',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        if (valor) {
          this.tipoPaginas['activo'] = 0;
          this.tipoPaginaService.put(idTipoPagina, this.tipoPaginas).subscribe( result => {
            this.tipoPaginaService.postSocket({status: 'ok'}).subscribe(x => {});
          });
          swal('¡La area se ha dado de baja exitosamente!', {
            icon: 'success',
          }).then((result => {
            this.getTipoPagina();
          }));
        }
      });
    }
  }

  getTipoPaginaById(idTipoPagina) {
    this.tipoPaginaService.getTipoPaginaById(idTipoPagina).subscribe(data => {
      this.tipoPaginas = data;
    });
  }


  ngOnInit() {
    this.connect();
    this.getTipoPagina();
    this.cols = ['tipo', 'objetivo', 'acciones'];
    // this.getPermisos();
  }

  // getPermisos() {
  //   this.permisos = JSON.parse(window.localStorage.getItem('User'));
  //   this.escritura = this.permisos.modulos.areas.escritura;
  // }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
