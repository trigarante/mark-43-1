import { Injectable } from '@angular/core';
import {Laptop, LaptopData} from '../../../../interfaces/ti/soporte/inventario/laptop';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class LaptopService extends LaptopData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<Laptop[]> {
    return this.http.get<Laptop[]>(this.baseURL + 'v1/laptop');
  }
  post(laptop: Laptop): Observable<Laptop> {
    return this.http.post<Laptop>(this.baseURL + 'v1/laptop/', laptop);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'laptop/saveLaptop', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajalaptop/saveBajaLaptop', json);
  }
  getLaptopById(idLaptop): Observable<Laptop> {
    return this.http.get<Laptop>(this.baseURL + 'v1/laptop/' + idLaptop);
  }
  put(idLaptop, laptop: Laptop): Observable<Laptop> {
    return this.http.put<Laptop>(this.baseURL + 'v1/laptop/' + idLaptop + '/'
      + sessionStorage.getItem('Empleado'), laptop);
  }
}
