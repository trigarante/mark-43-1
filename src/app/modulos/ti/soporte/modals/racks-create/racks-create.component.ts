import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Racks, RacksData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-racks-create',
  templateUrl: './racks-create.component.html',
  styleUrls: ['./racks-create.component.scss'],
})
export class RacksCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idRacks: number;
  racks: Racks;
  racksCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;


  constructor(private fb: FormBuilder, protected ref: NbDialogRef<RacksCreateComponent>, private racksService: RacksData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private router: Router, private sedesService: TipoSedeData) { }

  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }


  guardarCreateRacks() {
    if (this.racksCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.racksService.post(this.racksCreateForm.value).subscribe((result) => {
      this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
      this.racksCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioRedes/', 'racks']);
    });
  }

  updateRacks() {
    this.racksService.put(this.idRacks, this.racksCreateForm.value).subscribe(result => {
      this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
      this.racksCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioRedes/', 'racks']);
    });
  }
  getRacksById() {
    this.racksService.getRacksById(this.idRacks).subscribe(data => {
      this.racks = data;
      this.racksCreateForm.controls['folio'].setValue(this.racks.folio);
      this.racksCreateForm.controls['idMarca'].setValue(this.racks.idMarca);
      this.racksCreateForm.controls['idEstadoInventario'].setValue(this.racks.idEstadoInventario);
      this.racksCreateForm.controls['idObservacionesInventario'].setValue(this.racks.idObservacionesInventario);
      this.racksCreateForm.controls['idEmpleado'].setValue(this.racks.idEmpleado);
      this.racksCreateForm.controls['idTipoSede'].setValue(this.racks.idTipoSede);
      this.racksCreateForm.controls['origenRecurso'].setValue(this.racks.origenRecurso);

    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getRacksById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.racksCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
