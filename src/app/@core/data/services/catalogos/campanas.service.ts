import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Campana, CampanaData} from '../../interfaces/catalogos/campana';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class CampanasService extends CampanaData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Campana[]> {
    return this.http.get<Campana[]>(this.baseURL + 'v1/campanas');
  }
}
