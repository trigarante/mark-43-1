import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {ChromeBit, ChromeBitData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-chrome-bit-create',
  templateUrl: './chrome-bit-create.component.html',
  styleUrls: ['./chrome-bit-create.component.scss'],
})
export class ChromeBitCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idChrBt: number;
  chromeBit: ChromeBit;
  chromeBitCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<ChromeBitCreateComponent>, private chromeBitService: ChromeBitData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private router: Router, private sedesService: TipoSedeData) { }

  /**selec que muestra las opciones de estado de inventario*/
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  /**selec que muestra las opciones de observaciones del inventario*/
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }
  /**selec que muestra las opciones de marcas*/
  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  /**Funcion que permite crear una nuevo ChromeBit*/
  guardarChromeBit() {
    if (this.chromeBitCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.chromeBitService.post(this.chromeBitCreateForm.value).subscribe((result) => {
      this.chromeBitService.postSocket({status: 'ok'}).subscribe(x => {});
      this.chromeBitCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'chromeBit']);
    });
  }
  /**Funcion que permite modificar ChromeBit*/
  updateChromeBit() {
    this.chromeBitService.put(this.idChrBt, this.chromeBitCreateForm.value).subscribe(result => {
      this.chromeBitService.postSocket({status: 'ok'}).subscribe(x => {});
      this.chromeBitCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'chromeBit']);
    });
  }
  /**Funcion que permite obtener un chromeBit por su id*/
  getChromeBitById() {
    this.chromeBitService.getChromeBit(this.idChrBt).subscribe(data => {
      this.chromeBit = data;
      this.chromeBitCreateForm.controls['folio'].setValue(this.chromeBit.folio);
      this.chromeBitCreateForm.controls['idMarca'].setValue(this.chromeBit.idMarca);
      this.chromeBitCreateForm.controls['idEstadoInventario'].setValue(this.chromeBit.idEstadoInventario);
      this.chromeBitCreateForm.controls['idObservacionesInventario'].setValue(this.chromeBit.idObservacionesInventario);
      this.chromeBitCreateForm.controls['idEmpleado'].setValue(this.chromeBit.idEmpleado);
      this.chromeBitCreateForm.controls['idTipoSede'].setValue(this.chromeBit.idTipoSede);
      this.chromeBitCreateForm.controls['origenRecurso'].setValue(this.chromeBit.origenRecurso);

    });
  }
  /**selec que muestra si el equipo es propio o rentado*/
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  /**selec que muestra de donde es el equipo*/
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getChromeBitById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.chromeBitCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
