import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EstadoCivil, EstadoCivilData} from '../../../@core/data/interfaces/catalogos/estado-civil';
import {Escolaridad, EscolaridadData} from '../../../@core/data/interfaces/catalogos/escolaridad';
import {EstadoEscolaridad, EstadoEscolaridadData} from '../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {Sepomex, SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';
import {SelectItem} from 'primeng/api';
import {MedioTransporte, MedioTransporteData} from '../../../@core/data/interfaces/catalogos/medio-transporte';
import {EstacionesLineas, EstacionesLineasData} from '../../../@core/data/interfaces/catalogos/estaciones-lineas';
import {Solicitudes, SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {Dropdown} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {Precandidatos, PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {NbToastrService} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {Paises, PaisesData} from '../../../@core/data/interfaces/catalogos/paises';
// import {MatTableDataSource} from '@angular/material';

import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
/**
 * Componente para actualizar los datos de un precandidato.
 */
@Component({
  selector: 'ngx-precandidato-update',
  templateUrl: './precandidato-update.component.html',
  styleUrls: ['./precandidato-update.component.scss'],
})
export class PrecandidatoUpdateComponent implements OnInit {
  precandidatoEliminado: boolean;
  /** Variable que permite la inicialización del formulario para crear un precandidato. */
  precandidatoForm: FormGroup;
  /** Variable para establecer el estado. */
  idEstadoRh = 1;
  /** Variable para establecer la etapa en la que se encuentra el precandidato. */
  idEtapa = 1;
  /** Variable que almacena el id de la solicitud que fue seleccionada. */
  idPrecandidato: number = this.rutaActiva.snapshot.params.idPrecandidato;
  /** Arreglo que se inicializa con los valores del campo estado-civil desde el Microservicio. */
  estadoCivil: EstadoCivil[];
  /** Arreglo que se inicializa con los valores del campo escolaridad desde el Microservicio. */
  escolaridades: Escolaridad[];
  /** Arreglo auxiliar para escolaridades. */
  gradoEscolar: any[];
  /** Arreglo que se inicializa con los valores del campo estado-escolaridad desde el Microservicio. */
  estadoEscolaridades: EstadoEscolaridad[];
  /** Arreglo auxiliar para estadoEscolaridades. */
  estadoEscoloaridad: any[];
  /** Arreglo auxiliar para coloniasSepomex */
  colonias: any[];
  /** Arreglo que se inicializa con los valores del campo sepomex desde el Microservicio. */
  coloniasSepomex: Sepomex[];
  /** Bandera para conocer si el campo CURP contiene caracteres. */
  estadoCurp: boolean;
  /** Arreglo auxiliar para estadoCivil. */
  civil: any[];
  /** Expresión regular para sólo letras. */
  soloLetras: RegExp = /[\\A-Za-zñÑ ]+/;
  /** Expresión regular para letras y puntos. */
  puntoLetras: RegExp = /[\\A-Za-zñÑ. ]+/;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Expresión regular para sólo números. */
  numero: RegExp = /[0-9]+/;
  /** Variable de tipo "item seleccionado" para almacenar el género (masculino y femenino). */
  genders: SelectItem[];
  /** Variable de tipo "item seleccionado" para almacenar los tiempos de llegada. */
  tiempoLlegada: SelectItem[];
  /** Arreglo que se inicializa con los valores del campo medio-transporte desde el Microservicio. */
  medioTransporte: MedioTransporte[];
  /** Arreglo auxiliar para medioTransporte. */
  medioTransporteSelect: any[];
  /** Arreglo que se inicializa con los valores del campo estaciones-linead desde el Microservicio. */
  estacionesLineas: EstacionesLineas[];
  /** Arreglo auxiliar para las lineas del metro. */
  lineas = [{'label': '', 'value': ''}];
  /** Arreglo auxiliar para las estaciones de cada linea. */
  estaciones = [{'label': '', 'value': 0}];
  /** Variable que almacena la informaciónd de la solicitud seleccionada. */
  solicitud: Solicitudes;
  /** Variable que se inicializa con los datos del microservicio. */
  precandidato: Precandidatos;
  /** Variable que almacena el nombre del precandidato que se va a actualizar. */
  nombre: String;
  /** Variable que almacena el apellido paterno del precandidato que se va a actualizar. */
  apellidoPaterno: String;
  /** Variable que almacena el apellido materno del precandidato que se va a actualizar. */
  apellidoMaterno: String;
  /** Arreglo que se inicializa con los valores del campo paises desde el Microservicio. */
  paisOrigen: Paises[];
  /** Arreglo auxiliar para paisOrigen. */
  pais: any[];
  /***/
  formact: any;
  /**Variable que muestra mensaje si el CP no es valido*/
  mostrar: boolean;

  /** Variable para referenciar al dropdown de las lineas del metro. */
  @ViewChild('lineaDropdown')
  lineaDropdown: Dropdown;

  /** Variable para referenciar al dropdown de las estaciones de la linea del metro seleccionada. */
  @ViewChild('estacionesDropdown')
  estacionesDropdown: Dropdown;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  precandidatos: any;

  /**
   * El método constructor manda a llamar Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {ActivatedRoute} rutaActiva Establece la ruta que se encuentra activa actualmente.
   * @param {EstadoCivilData} estadoCivilService Servicio del componente estado-civil para poder invocar los métodos.
   * @param {EscolaridadData} escolaridadesService Servicio del componente escolaridad para poder invocar los métodos.
   * @param {EstadoEscolaridadData} estadoEscolaridadService Servicio del componente estado-escolaridad para poder invocar los métodos.
   * @param {SepomexData} sepomexService Servicio del componente sepomex para poder invocar los métodos.
   * @param {PrecandidatosData} precandidatoService Servicio del componente precandidatos para poder invocar los métodos.
   * @param {MedioTransporteData} medioTransporteService Servicio del componente medio-transporte para poder invocar los métodos.
   * @param {EstacionesLineasData} estacionesLineasService Servicio del componente estaciones-lineas para poder invocar los métodos.
   * @param {Router} router Variable para navegar entre componentes.
   * @param {SolicitudesData} solicitudService Servicio del componente solicitudes para poder invocar los métodos.
   * @param {NbToastrService} toastrService Servicio por parte de la librería @nebular/theme para mostrar mensajes.  */
  constructor(private fb: FormBuilder, private rutaActiva: ActivatedRoute, protected estadoCivilService: EstadoCivilData,
              protected  escolaridadesService: EscolaridadData, protected estadoEscolaridadService: EstadoEscolaridadData,
              protected sepomexService: SepomexData, protected precandidatoService: PrecandidatosData,
              protected medioTransporteService: MedioTransporteData,
              protected estacionesLineasService: EstacionesLineasData, private router: Router,
              protected solicitudService: SolicitudesData, private toastrService: NbToastrService,
              protected paisesService: PaisesData,
              private precandidatosService: PrecandidatosData) {

    this.precandidatoForm = this.fb.group({
      'curp': new FormControl('', Validators.compose([Validators.required,
        Validators.pattern('^[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0,1][0-9][0-3][0-9][H,M][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]'),
        Validators.maxLength(18), Validators.minLength(18)])),
      'nombre': new FormControl('', Validators.required), // Validators.pattern('/^[\\pL\\s\\-]+$/u\n'
      'apellidoPaterno': new FormControl('', Validators.required),
      'apellidoMaterno': new FormControl('', Validators.required),
      'fechaNacimiento': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'genero': new FormControl('', Validators.required),
      'idEstadoCivil': new FormControl('', Validators.required),
      'idPais': new FormControl('', Validators.required),
      'idEscolaridad': new FormControl('', Validators.required),
      'idEstadoEscolaridad': new FormControl('', Validators.required),
      'cp':  new FormControl('', Validators.compose([Validators.required, Validators.maxLength(5), Validators.minLength(5)])),
      'colonia': new FormControl('', Validators.required),
      'calle': new FormControl('', Validators.required),
      'numeroInterior': new FormControl(''),
      'numeroExterior': new FormControl('', Validators.required),
      'telefonoMovil': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])),
      'telefonoFijo': new FormControl('', Validators.compose([Validators.maxLength(10), Validators.minLength(10)])),
      'tiempoTraslado': new FormControl('', Validators.required),
      'idMedioTraslado': new FormControl('', Validators.required),
      'lineaMetro': new FormControl(''),
      'idEstacion': new FormControl(''),
      'idEstadoRH': this.idEstadoRh,
      'idEtapa': this.idEtapa,
      'idSolicitudRRHH': 0,
    });
  }



  getPrecandidatos(mensajeAct) {
    this.precandidatosService.get().pipe(map(result => {
      return result.filter(data => data.idEtapa === 1 && data.idEstadoRRHH === 1 || data.idEtapa === 1 && data.idEstadoRRHH === 16);
    })).subscribe(data => {
      this.precandidatos = data;
    });
  }

  /** Función que obtiene toda la información del precandidato y la asigna a los campos del formulario. */
  getPrecandidato() {
    this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      this.precandidato = data;
      this.precandidatoForm.controls['idSolicitudRRHH'].setValue(this.precandidato.idSolicitudRrhh);
      this.precandidatoForm.controls['curp'].setValue(this.precandidato.curp);
      this.nombre = this.precandidato.nombre;
      this.precandidatoForm.controls['nombre'].setValue(this.nombre);
      this.apellidoPaterno = this.precandidato.apellidoPaterno;
      this.precandidatoForm.controls['apellidoPaterno'].setValue(this.apellidoPaterno);
      this.apellidoMaterno = this.precandidato.apellidoMaterno;
      this.precandidatoForm.controls['apellidoMaterno'].setValue(this.apellidoMaterno);
      this.precandidatoForm.controls['fechaNacimiento'].setValue(new Date(data.fechaNacimiento + 'T15:00'));
      this.precandidatoForm.controls['email'].setValue(this.precandidato.email);
      this.precandidatoForm.controls['genero'].setValue(this.precandidato.genero);
      this.precandidatoForm.controls['idEstadoCivil'].setValue(this.precandidato.idEstadoCivil);
      this.precandidatoForm.controls['idPais'].setValue(this.precandidato.idPais);
      this.precandidatoForm.controls['idEscolaridad'].setValue(this.precandidato.idEscolaridad);
      this.precandidatoForm.controls['idEstadoEscolaridad'].setValue(this.precandidato.idEstadoEscolaridad);
      this.precandidatoForm.controls['cp'].setValue(this.precandidato.cp);
      this.getColonias(this.precandidato.cp);
      this.precandidatoForm.controls['colonia'].setValue(this.precandidato.idColonia);
      this.precandidatoForm.controls['calle'].setValue(this.precandidato.calle);
      this.precandidatoForm.controls['numeroInterior'].setValue(this.precandidato.numeroInterior);
      this.precandidatoForm.controls['numeroExterior'].setValue(this.precandidato.numeroExterior);
      this.precandidatoForm.controls['telefonoMovil'].setValue(this.precandidato.telefonoMovil);
      this.precandidatoForm.controls['telefonoFijo'].setValue(this.precandidato.telefonoFijo);
      this.precandidatoForm.controls['tiempoTraslado'].setValue(this.precandidato.tiempoTraslado);
      this.precandidatoForm.controls['idMedioTraslado'].setValue(this.precandidato.idMedioTraslado);
      this.getLineas(this.precandidato.idMedioTraslado);
      this.estacionesLineasService.getEstacionById(this.precandidato.idEstacion).subscribe(response => {
        this.precandidatoForm.controls['lineaMetro'].setValue(response.lineas);
        this.getEstaciones(response.lineas);
        this.precandidatoForm.controls['idEstacion'].setValue(this.precandidato.idEstacion);
      });
    });
  }

  /** Función para obtener el estado civil del precandidato y lo almacena en el arreglo "civil". */
  getEstadoCivil() {
    this.estadoCivilService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoCivil = result;
      this.civil = [];
      for (let i = 0; i < this.estadoCivil.length; i++) {
        this.civil.push({'label': this.estadoCivil[i].descripcion, 'value': this.estadoCivil[i].id});
      }
    });
  }

  /** Función que obtiene el grado escolar del precandidato y lo almacena en el arreglo "gradoEscolar". */
  getEscolaridades() {
    this.escolaridadesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.escolaridades = result;
      this.gradoEscolar = [];
      for (let i = 0; i < this.escolaridades.length; i++) {
        this.gradoEscolar.push({'label': this.escolaridades[i].nivel, 'value': this.escolaridades[i].id});
      }
    });
  }

  /** Función para obtener el país de origen del precandidato y lo almacena en el arreglo "pais". */
  getPaises() {
    this.paisesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.paisOrigen = result;
      this.pais = [];
      for (let i = 0; i < this.paisOrigen.length; i++) {
        this.pais.push({'label': this.paisOrigen[i].nombre, 'value': this.paisOrigen[i].id});
      }
    });
  }

  /** Función para obtener el estado de escolaridad del precandidato y lo almacena en el arreglo "estadoEscolaridad". */
  getEstadoEscolaridad() {
    this.estadoEscolaridadService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.estadoEscolaridades = result;
      this.estadoEscoloaridad = [];
      for (let i = 0; i < this.estadoEscolaridades.length; i++) {
        this.estadoEscoloaridad.push({
          'label': this.estadoEscolaridades[i].estado,
          'value': this.estadoEscolaridades[i].id,
        });
      }
    });
  }

  /** Función que obtiene las colonias y las almacena en el arreglo "colonias" */
  getColonias(cp) {
    this.sepomexService.getColoniaByCp(cp)
      .pipe(map(data => this.coloniasSepomex = data)).subscribe(data => {
      this.colonias = [];
      if (this.coloniasSepomex.length === 0) {
        this.mostrar = true;
      }else {
        this.mostrar = false;
      }
      for (let i = 0; i < this.coloniasSepomex.length; i++) {
        this.colonias.push({'label': this.coloniasSepomex[i].asenta, 'value': this.coloniasSepomex[i].idCp});
      }
    });
  }

  /** Función que permite la validación del CURP introducido. */
  validarCurp(curp) {
    if (curp.length === 18) {
      this.estadoCurp = false;
      this.precandidatoService.validarCurp(curp).subscribe(response => {
        this.estadoCurp = response;
        if (this.estadoCurp === true) {
          this.showToast('danger');
        }
      });
    }
  }

  /** Función que inicializa el arreglo "genero" para usarlo en el Select. */
  genero() {
    this.genders = [];
    this.genders.push({label: 'Masculino', value: 'M'});
    this.genders.push({label: 'Femenino', value: 'F'});
  }

  /** Función que inicializa el arreglo "tiempoTraslado" para usarlo en el Select. */
  tiempoTraslado() {
    this.tiempoLlegada = [];
    this.tiempoLlegada.push({label: '0 - 15 min', value: 15});
    this.tiempoLlegada.push({label: '15 - 30 min', value: 30});
    this.tiempoLlegada.push({label: '30 - 45 min', value: 45});
    this.tiempoLlegada.push({label: '45 - 60 min', value: 60});
    this.tiempoLlegada.push({label: '60 - 1:15 hrs', value: 75});
    this.tiempoLlegada.push({label: '1:15 - 1:30 hrs', value: 90});
    this.tiempoLlegada.push({label: '1:30 - 1:45 hrs', value: 105});
    this.tiempoLlegada.push({label: '1:45- 2:00 hrs', value: 120});
  }

  /** Función para obtener la información acerca del medio de transporte que utiliza el empleado para llegar a la empresa
   * y lo almacena en el arreglo "medioTransporteSelect". */
  getMedioTransporte() {
    this.medioTransporteService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.medioTransporte = result;
      this.medioTransporteSelect = [];
      for (let i = 0; i < this.medioTransporte.length; i++) {
        this.medioTransporteSelect.push({'label': this.medioTransporte[i].medio, 'value': this.medioTransporte[i].id});
      }
    });
  }

  /** Función que obtiene la información acerca de las líneas del metro. */
  getLineas(idMedioTraslado) {
    this.estacionesLineasService.getByIdMedio(idMedioTraslado).subscribe(data => {
      this.lineas = [];
      this.estacionesLineas = data; /**
      if (idMedioTraslado === 1 || idMedioTraslado === 2) { // Si el medio seleccionado es metro o metrobus
        this.lineaDropdown.value = 'Selecciona una línea';  // Reseteando el dropdown de linea del metro
        this.estacionesDropdown.value = 'Seleccione una estación'; // Reseteando el dropdown de estaciones
        this.estaciones = [];
      }*/
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.lineas.push({'label': this.estacionesLineas[i].lineas, 'value': this.estacionesLineas[i].lineas});
      }
    });
    this.precandidatoForm.controls['lineaMetro'].setValue('');
    this.precandidatoForm.controls['idEstacion'].setValue('');
    this.estaciones = [];
    this.verificaFormulario();
  }

  /** Función que obtiene las estaciones de la línea del metro seleccionada. */
  getEstaciones(lineaMetro) {
    this.estacionesLineasService.getEstacionByIdLinea(this.precandidatoForm.value.idMedioTraslado, lineaMetro).subscribe(data => {
      this.estaciones = [];
      this.estacionesLineas = data;
      for (let i = 0; i < this.estacionesLineas.length; i++) {
        this.estaciones.push({'label': this.estacionesLineas[i].estacion, 'value': this.estacionesLineas[i].id});
      }
    });
  }

  /** Función que verifica que todos los campos del formulario estén completos. */
  verificaFormulario() {
    if (this.precandidatoForm.valid === true) {
      // if (this.lineaDropdown !== undefined && this.estacionesDropdown !== undefined) {
        if (this.precandidatoForm.controls['idMedioTraslado'].value === 1 ||
            this.precandidatoForm.controls['idMedioTraslado'].value === 2) {
          if (this.precandidatoForm.controls['lineaMetro'].value !== '' &&
            this.precandidatoForm.controls['idEstacion'].value !== '') {
            return false; // BOTÓN HABILITADO
          } else {
            return true;  // BOTÓN DESHABILITADO
          }
        } else {
          return false;   // BOTÓN HABILITADO
        }
      // }
    } else {
      return true;        // BOTÓN DESHABILITADO
    }
  }

  /** Esta función permite guardar toda la información vaciada en los campos de texto del formulario.
   * Toda esa información se envía mediante una petición PUT al Microservicio debido a que sólo es una actualización. */
  updatePrecandidato() {
    this.inactivo = true;
    if (this.precandidatoForm.value.idMedioTraslado > 2) {
      this.precandidatoForm.value.lineaMetro = null;
      this.precandidatoForm.value.idEstacion = null;
    }
    this.formact = {
      'apellidoMaterno': this.precandidatoForm.value.apellidoMaterno,
      'apellidoPaterno': this.precandidatoForm.value.apellidoPaterno,
      'calle': this.precandidatoForm.value.calle,
      'idColonia': this.precandidatoForm.value.colonia,
      'cp': this.precandidatoForm.value.cp,
      'curp': this.precandidatoForm.value.curp,
      'email': this.precandidatoForm.value.email,
      'fechaNacimiento': this.precandidatoForm.value.fechaNacimiento,
      'genero': this.precandidatoForm.value.genero,
      'idEscolaridad': this.precandidatoForm.value.idEscolaridad,
      'idEstacion': this.precandidatoForm.value.idEstacion,
      'idEstadoCivil': this.precandidatoForm.value.idEstadoCivil,
      'idPais': this.precandidatoForm.value.idPais,
      'idEstadoEscolaridad': this.precandidatoForm.value.idEstadoEscolaridad,
      'idEstadoRH': this.idEstadoRh,
      'idEtapa': this.idEtapa,
      'idMedioTraslado': this.precandidatoForm.value.idMedioTraslado,
      'idSolicitudRRHH': this.precandidatoForm.value.idSolicitudRRHH,
      'nombre': this.precandidatoForm.value.nombre,
      'numeroExterior': this.precandidatoForm.value.numeroExterior,
      'numeroInterior': this.precandidatoForm.value.numeroInterior,
      'telefonoFijo': this.precandidatoForm.value.telefonoFijo,
      'telefonoMovil': this.precandidatoForm.value.telefonoMovil,
      'tiempoTraslado': this.precandidatoForm.value.tiempoTraslado,
    };
     this.formact.fechaNacimiento = Date.parse(this.formact.fechaNacimiento);
   this.precandidatoService.getPrecandidatoById(this.idPrecandidato).subscribe(data => {
      if (data.idEtapa === 1 && data.idEstadoRH === 1 || data.idEtapa === 1 && data.idEstadoRH === 16) {
        this.precandidatoService.updatePrecandidato(this.idPrecandidato, this.formact, this.formact.fechaNacimiento)
          .subscribe(
            (result) => {
            },
            error => { },
            () => {
              this.precandidatoService.postSocket(this.precandidatoForm.value).subscribe(d => {});
              this.precandidatoForm.reset();
              this.router.navigate(['/modulos/rrhh/precandidatos']);
            },
          );
      } else {
        this.precandidatoEliminado = true;
      }
    });
  }

  /** Método para mostrar un mensaje en pantalla, en este caso, un mensaje de advertencia. */
  showToast(status) {
    this.toastrService.danger('Atencion', `Curp ya se encuentra registrado`);
  }

  /** Función que manda a llamar a todos los demás métodos al inicializar el componente. */
  ngOnInit() {
    this.inactivo = false;
    if (this.estadoCurp === false) {
      this.showToast('danger');
    }
    this.getPrecandidato();
    this.genero();
    this.getEstadoCivil();
    this.getEscolaridades();
    this.getEstadoEscolaridad();
    this.tiempoTraslado();
    this.getMedioTransporte();
    this.getPaises();
  }
}
