import {Injectable} from '@angular/core';
import {Biometricos, BiometricosData} from '../../../../interfaces/ti/soporte/inventario-soporte/biometricos';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {
  BarrasMulticontacto,
  BarrasMulticontactoData,
} from '../../../../interfaces/ti/soporte/inventario-soporte/barras-multicontacto';

@Injectable()
export class BarrasMulticontactoService extends BarrasMulticontactoData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<BarrasMulticontacto[]> {
    return this.http.get<BarrasMulticontacto[]>(this.baseURL + 'v1/barrasMulticontacto');
  }

  post(barrasMulticontacto: BarrasMulticontacto): Observable<BarrasMulticontacto> {
    return this.http.post<BarrasMulticontacto>(this.baseURL + 'v1/barrasMulticontacto', barrasMulticontacto);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'barramulticontacto/saveBarraMultiContacto', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajabarramulticontacto/saveBajaBarraMultiContacto', json);
  }

  getBarrasMulticontactoById(idBarrasMulticontacto): Observable<BarrasMulticontacto> {
    return this.http.get<BarrasMulticontacto>(this.baseURL + 'v1/barrasMulticontacto/' + idBarrasMulticontacto);
  }

  put(idBarraMulticontacto, barrasMulticontacto: BarrasMulticontacto): Observable<Biometricos> {
    return this.http.put<Biometricos>(this.baseURL + 'v1/barrasMulticontacto/' + idBarraMulticontacto + '/'
      + sessionStorage.getItem('Empleado') , barrasMulticontacto);
  }
}
