import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {Correo, CorreoData, Domains} from '../../../../@core/data/interfaces/ti/correo';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {map} from 'rxjs/operators';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {VariablesglobalService} from '../../../../@core/data/services/ti/variablesglobal.service';
import {SaveComponent} from '../save/save.component';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {
  LicenciasDisponibles, LicenciasDisponiblesData,
} from '../../../../@core/data/interfaces/ti/licencias-disponibles';

@Component({
  selector: 'ngx-crear-administrativo',
  templateUrl: './crear-administrativo.component.html',
  styleUrls: ['./crear-administrativo.component.scss'],
})
export class CrearAdministrativoComponent implements OnInit {
  @Input() id: number;
  @Input() subareax: number;
  empleado: Empleados;
  adminForm: FormGroup;
  alias: string;
  correocompleto: string;
  dominios: SelectItem[];
  dominio = new FormControl();
  correosdisponible = new FormControl();
  previosval: string;
  email: string;
  estadoCorreo: boolean;
  estadoCorreo2: boolean = false;
  estadoCorreo3: boolean = true;
  estadofinal: string;
  extensiones: any[];
  extension: Extension[];
  extensionput: Extension;
  correosdisponibles: any[];
  correosdisponibless: any[];
  correosSubareas: any[];
  usuario: Usuario[];
  usuario2: Usuario;
  usuario3: Usuario;
  usuarioput: Usuario;
  usuarioput1: Usuario;
  dominio2: string;
  correoprimario: string;
  idCorreo: any[];
  idCorreo2: string;
  variableglobal: any[];
  idCorreoslista: any[];
  estadofindalias: boolean;
  getidSubarea: number;
  datosUsuarios: object;
  extensionopcional: string;
  estado: number;
  email3: string;
  correo: Correo[];
  bandera: boolean;
  idUsuario: number;
  extensionidsubarea: number;
  extensionnuevo: Extension;
   extensionput1: Extension;
   banderatodosusuarios: boolean = false;
   grupoUsuario: GrupoUsuarios[];
   grupUsuario: any[];
   tipo: string = 'administrativo';
  trigarante: RegExp = /trigarante.com/;
  nexos: RegExp = /nexosmedia.com/;
  grupoahorra: RegExp = /grupoahorra.com/;
  licencias: LicenciasDisponibles[];
  correoAdministrativos: Correo[];
  banderalicencias: boolean;
  domainss: any[];
  domains: Domains[];


  constructor(private empleadosService: EmpleadosData, protected ref: NbDialogRef<CrearAdministrativoComponent>,
              private fb: FormBuilder, private correoService: CorreoData, private toastrService: NbToastrService,
              private extensionesService: ExtensionData, private usuarioService: UsuarioData,
              private variablesglobal: VariablesglobalService, public dialogService: NbDialogService,
              private grupoUsuariosService: GrupoUsuariosData, private licenciasService: LicenciasDisponiblesData) {

    this.adminForm = this.fb.group({
      'tiposdominio': new FormControl('', Validators.compose([Validators.required])),
      'extensioncontrol': new FormControl('', Validators.compose([Validators.required])),
      'correosdisponible': new FormControl(),
      'grupoUsuariosByIdGrupo': {},
      'emailmodificado': new FormControl(),
      'correosdisponibleSubareas': new FormControl(),
      'privilegiocontrol': new FormControl('', Validators.compose([Validators.required])),
    });
  }

  // obtiene el empleado por el id  y pone un alias  de correo deacuerdo a sus datos
  getEmpleados() {
    this.empleadosService.getEmpleadoById(this.id).subscribe(data => {
      this.empleado = data;
      this.alias = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoPaterno.substring(0);
      this.alias = this.alias.toLowerCase();
      this.getcompleto(this.alias);

    });
  }

  // muestra los dominios para seleccionarlos
  getDominios() {
    this.correoService.getDominios('administrativo').pipe(map(data => this.domains = data)).subscribe(data => {
      this.domainss = [];
      for (let i = 0; i < this.domains.length; i++) {
        this.domainss.push({'label': this.domains[i].domainName, 'value': this.domains[i].domainName});
      }
    });
  }

  getcompleto(alias) {

    this.correocompleto = alias;

  }

  // del dominio seleccionado lo completa con el correocompleto para tener un email completo y valida si ya existe en el api
  getadmin(event) {
    if (event) {
      this.dominio2 =  '@' + event;
      this.previosval = this.correocompleto + '@' + event;
      this.getcorreocompleto(this.previosval);
      this.validarEmail(this.previosval);

    }
  }

  getcorreocompleto(previosval) {
    this.email = previosval;

  }

  // verifica si existe el correo en la api, si existe cambia el email por apellidoMaterno manda true a estadoCorreo
  validarEmail(email) {
    this.tipo = 'administrativo';
    this.estadoCorreo = false;
    this.estadoCorreo3 = false;
    this.correoService.validarCorreo(this.tipo , email).subscribe(response => {
      this.estadoCorreo = response;
      if (this.estadoCorreo === true) {
        this.email = this.empleado.nombre.substring(0, 1) + this.empleado.apellidoMaterno.substring(0) + this.dominio2;
        this.email = this.email.toLowerCase();
        this.validar2email(this.email);
      }
    });
  }

  validar2email(email) {
    this.estadoCorreo2 = false;

    this.correoService.validarCorreo(this.tipo, email).subscribe(response => {
      this.estadoCorreo2 = response;
      if (this.estadoCorreo2 === true) {

        this.adminForm.controls['emailmodificado'].setValue(this.email);
        this.estadoCorreo3 = true;
      }
    });
  }

  getcorreofinal(event) {
    if (event) {
      this.estadoCorreo3 = false;

      this.correoService.validarCorreo(this.tipo, event).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.estadofinal = 'correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.email = event;
          this.estadofinal = 'correo disponible';
        }
      });
    }
  }

  getExtensiones() {
    this.extensionesService.getExtensionByIdSubarea(this.subareax, 0).pipe(map(data => this.extension = data)).subscribe(data => {
      this.extensiones = [];
      for (let i = 0; i < this.extension.length; i++) {
        this.extensiones.push({'label': this.extension[i].id, 'value': this.extension[i].id});
      }
    });
  }
  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.grupoUsuario = data;
      this.grupUsuario = [];
      for (let i = 0; i < this.grupoUsuario.length; i++) {
        this.grupUsuario.push({'label': this.grupoUsuario[i].nombre, 'value': this.grupoUsuario[i].id});
      }
    });
  }

  getcorreoprincial(event) {
    if (event) {
      this.correoprimario = event;

    }
  }
  getcorreoprincialSubareas(event) {
    if (event) {
      this.correoprimario = event;
    }
  }

  getcorreosdisponibles(subarea) {
    this.usuarioService.getUsuarioByIdSubarea(subarea, 0).pipe(map(data => this.usuario = data)).subscribe(data => {
      this.correosdisponibles = [];
      this.idCorreoslista = [];
      this.idCorreo = [];

      for (let i = 0; i < this.usuario.length; i++) {
         // saca los correos de los usaurios dados de baja
        this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
        // saca los id de los correos de los usuarios que esten dados de baja
        this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
        this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
      }
      if (this.usuario.length !== 0) {
        this.bandera = true;
      } else if (this.usuario.length === 0) {
        this.bandera = false;
        this.getcorreosdisponiblesSubareas();
      }
    });
  }

  getcorreosdisponiblesSubareas() {
    if (this.bandera === false) {

      this.usuarioService.getUsuarioByRango(0, 30, 63).pipe(map( result => {
       return result.filter(data => data.usuario.match(this.trigarante)
         || data.usuario.match(this.nexos) || data.usuario.match(this.grupoahorra)  );
      })).subscribe(data => {
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        this.usuario = data;
        console.table(this.usuario);

        for (let i = 0; i < this.usuario.length; i++) {
          this.correosdisponibles.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].usuario});
          this.idCorreoslista.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].idCorreo});
          this.idCorreo.push({'label': this.usuario[i].usuario, 'value': this.usuario[i].id});
        }
        if (this.usuario.length === 0) {

          this.licenciasService.get().subscribe(data2 => {
            this.licencias = data2;
            this.correoService.get('administrativo').subscribe(data3 => {
              this.correoAdministrativos = data3;
              if (this.correoAdministrativos.length === this.licencias[2].cantidad ||
                this.correoAdministrativos.length >= this.licencias[2].cantidad ) {
              // checar licencias
                 this.banderalicencias = true;
              }else if (this.correoAdministrativos.length <= this.licencias[2].cantidad) {
                // licencias disponibles
                this.banderalicencias = false;
              }
            });
          });

          this.banderatodosusuarios = true; // true si no hay correos disponibles hacer uno nuevo seleccionando las extensiones disponibles
        }
      });
    }
  }

  guardarUsuario() {
    // saca el id del usuario del correo primario
    for (let i = 0; i < this.idCorreo.length; i++) {
      if (this.correoprimario === this.idCorreo[i].label) {
        this.idUsuario = this.idCorreo[i].value;
      }
    }
    const objeto = {
      'id': this.idCorreo2, // id del correoprimario
      'givenName': this.empleado.nombre,
      'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
      'email': this.email, // nombre del correo para remplazar
    };
    console.table(objeto);
    this.correoService.enviarByAlias('administrativoCorreo', objeto).subscribe(response => {   /// checar
      this.estadofindalias = response;
    });

    this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.extensionopcional};

    console.table(this.datosUsuarios);


    /*const usuario = {
      'usuario': this.email, 'password': 'mark-43',
      'idCorreo': this.idCorreo2, 'idTipo': 3, 'idSubarea': this.subareax,
      'token': '1', 'datosUsuario': JSON.stringify(this.datosUsuarios), 'estado': 1,
      'grupoUsuariosByIdGrupo': {id: this.adminForm.value.privilegiocontrol},
    };
    console.table(usuario);*/


        this.usuarioService.getUsuarioById(this.idUsuario).subscribe( datausuarios => {
          this.usuarioput = datausuarios;
          this.usuarioput1 = {
            'id': this.usuarioput.id,
            'idCorreo': this.idCorreo2,
            'idGrupo': this.adminForm.value.privilegiocontrol,
            'idTipo': 1,
            'idSubarea': this.subareax,
            'usuario': this.email,
            'password': 'mark-43',
            'datosUsuario': JSON.stringify(this.datosUsuarios),
            'estado': 1,
            // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
          };
          this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(data => {
            this.usuario3 = data;
            this.adminForm.reset();
            if (this.usuario3 !== null) {

              this.extensionesService.get(this.extensionopcional).subscribe(dataextensiones => {
                this.extensionput = dataextensiones;
                this.extensionput1 = {
                  'idSubarea' : this.extensionput.idSubarea,
                  'descrip' : this.extensionput.descrip,
                  'idEstado' : 1,
                };
                this.extensionesService.put(this.extensionopcional, this.extensionput1).subscribe(result2 => {
                });
              });
        this.dialogService.open(SaveComponent, {});
        } // fin del if
        }); // fin del put
    });
  }

  guardarNuevos() {
if (this.banderalicencias === true) {
  this.showToast2('danger');
}else if (this.banderalicencias === false) {

  const objeto = {
    'givenName': this.empleado.nombre,
    'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
    'email': this.email,
  };

  this.correoService.post('administrativo', objeto).subscribe(response => {

  });
  // una ves guardado el correo en el api, busca su id y lo manda a la tabla usuarios
  this.correoService.get('administrativo').subscribe(data => {
    this.correo = data;
    this.idCorreoslista = [];
    for (let i = 0; i < this.usuario.length; i++) {
      this.idCorreoslista.push({'label': this.correo[i].emails, 'value': this.correo[i].id});
    }
    for (let i = 0; i < this.idCorreoslista.length; i++) {

      if (this.email === this.idCorreoslista[i].label) {
        this.idCorreo2 = this.idCorreoslista[i].value;
      }
    }
    this.datosUsuarios = {'idEmpleado': this.empleado.id, 'idExtension': this.extensionopcional};
    this.usuario2 = {
      'idCorreo': this.idCorreo2,
      'idGrupo': this.adminForm.value.privilegiocontrol,
      'idTipo': 1,
      'idSubarea': this.subareax,
      'usuario': this.email,
      'password': 'mark-43',
      'datosUsuario': JSON.stringify(this.datosUsuarios),
      'estado': 1,
      // 'grupoUsuariosByIdGrupo': { 'id': this.usuarioput.grupoUsuariosByIdGrupo},
    };
    // guarda en la tabla usuario y modifica el idUsuario de la tabla empleado
    this.usuarioService.postUsuariosidEmpleados(this.usuario2, this.empleado.id).subscribe(data2 => {
      this.usuario3 = data2;
      this.adminForm.reset();
      // si guardo bien modifica al extension a ocupado
      if (this.usuario3 !== null) {
        // cambia el estado a 1 a la extencion dada
        this.extensionesService.get(this.extensionopcional).subscribe(dataextensiones => {
          this.extensionput = dataextensiones;
          this.extensionput1 = {
            'idSubarea': this.extensionput.idSubarea,
            'descrip': this.extensionput.descrip,
            'idEstado': 1,
          };
        });

        this.dialogService.open(SaveComponent, {});
      }

    }); // fin de post usuario
  }); // fin del get
}
  }

  guardar() {
    // si hay correos disponibles entra a esta funcion de guardar correos utilizando un alias nuevo
    if (this.correosdisponibles.length !== 0) {
      // saca el id del correo que se va a ocupar
      for (let i = 0; i < this.idCorreoslista.length; i++) {
        console.table(this.idCorreoslista);
        if (this.correoprimario === this.idCorreoslista[i].label) {
          this.idCorreo2 = this.idCorreoslista[i].value;
        }
      }
      if (this.correoprimario ) {
        // si no hay correos iguales guarda usuario
        if (this.estadoCorreo2 === true) {
          this.guardarUsuario();
        } else if (this.estadoCorreo2 === false) {
          if (this.estadoCorreo === false) {
            this.guardarUsuario();
          } else if (this.estadoCorreo === true) {
            this.guardarUsuario();
          }
        }
      }else {
        this.showToast('danger');
    }
    }
    // si no hay correos disponibles hace correos nuevos
    if (this.correosdisponibles.length === 0) {

      if (this.estadoCorreo2 === true) {
        this.guardarNuevos();
      } else if (this.estadoCorreo2 === false) {
        if (this.estadoCorreo === false) {
          this.guardarNuevos();
        } else if (this.estadoCorreo === true) {
          this.guardarNuevos();
        }
      }
    }
  }

  getidSubareas(subareax) {
    this.getidSubarea = this.subareax;
  }

  getextension(event) {
    if (event) {
      this.extensionopcional = event;
    }
  }

  dismiss() {
    this.ref.close();
  }
  showToast(status) {
    this.toastrService.danger('Atencion', `Hay correos disponibles selecciona uno`);
  }
  showToast2(status) {
    this.toastrService.danger('Atencion', `No hay licencias disponibles`);
  }

  ngOnInit() {
    this.getcorreosdisponibles(this.subareax);
    this.getcorreosdisponiblesSubareas();
    this.getEmpleados();
    this.getDominios();
    this.validarEmail(this.email);
    this.getExtensiones();
    this.getidSubareas(this.subareax);
    this.getGrupoUsuarios();

  }
}
