import {Observable} from 'rxjs';

export interface Campana {
  id: number;
  nombre: string;
  activo: number;
}

export abstract class CampanaData {
  abstract get(): Observable<Campana[]>;
}
