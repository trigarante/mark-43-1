import {Component, Input, OnInit} from '@angular/core';
import {Dvr, DvrData} from '../../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  EstadoInventario,
  EstadoInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SelectItem} from 'primeng/api';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {NbDialogRef} from '@nebular/theme';
import {TarjetaRedData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-dvr-create',
  templateUrl: './dvr-create.component.html',
  styleUrls: ['./dvr-create.component.scss'],
})
export class DvrCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idDvr: number;
  dvr: Dvr;
  dvrCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<DvrCreateComponent>, private dvrService: DvrData,
              private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private router: Router, private sedesService: TipoSedeData) { }

  /**selec que muestra las opciones de estado de inventario*/
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  /**selec que muestra las opciones de observaciones del inventario*/
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }
  /**selec que muestra las opciones de marcas*/
  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(marcadata => {
      this.marca = [];
      this.marcas = marcadata;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  /**Funcion que permite crear un Dvr*/
  guardarDvr() {
    if (this.dvrCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.dvrService.post(this.dvrCreateForm.value).subscribe((result) => {
      this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
      this.dvrCreateForm.reset();
      // this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'dvr']);
    });
  }
  /**Funcion que permite modificar Dvr*/
  updatedvr() {
    this.dvrService.put(this.idDvr, this.dvrCreateForm.value).subscribe(result => {
      this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
      this.dvrCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventarioSoporte/', 'drv']);
    });
  }
  /**Funcion que permite obtener un dvr por su id*/
  getdvrById() {
    this.dvrService.getDvrById(this.idDvr).subscribe(data => {
      this.dvr = data;
      this.dvrCreateForm.controls['folio'].setValue(this.dvr.folio);
      this.dvrCreateForm.controls['idMarca'].setValue(this.dvr.idMarca);
      this.dvrCreateForm.controls['idEstadoInventario'].setValue(this.dvr.idEstadoInventario);
      this.dvrCreateForm.controls['idObservacionesInventario'].setValue(this.dvr.idObservacionesInventario);
      this.dvrCreateForm.controls['idEmpleado'].setValue(this.dvr.idEmpleado);
      this.dvrCreateForm.controls['idTipoSede'].setValue(this.dvr.idTipoSede);
      this.dvrCreateForm.controls['origenRecurso'].setValue(this.dvr.origenRecurso);

    });
  }
  /**selec que muestra si el equipo es propio o rentado*/
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  /**selec que muestra de donde es el equipo*/
  getSedes() {
    this.sedesService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  /** funcion que permite cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getdvrById();
    }
    this.getEstadoInventario();
    this.getObservacionInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.dvrCreateForm = this.fb.group({
      'folio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,

    });
  }

}
