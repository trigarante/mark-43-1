import { Component, OnInit } from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {LicenciasDisponibles, LicenciasDisponiblesData,
} from '../../../../@core/data/interfaces/ti/licencias-disponibles';


@Component({
  selector: 'ngx-modificar-total-licencias',
  templateUrl: './modificar-total-licencias.component.html',
  styleUrls: ['./modificar-total-licencias.component.scss'],
})
export class ModificarTotalLicenciasComponent implements OnInit {
  licenciasForm: FormGroup;
  licencias: any[];
  licenciasdisponibles: LicenciasDisponibles[];
  licenciasdis: LicenciasDisponibles;
  licenciasdi: LicenciasDisponibles;
  submitted: boolean;

  constructor(protected ref: NbDialogRef<ModificarTotalLicenciasComponent>, private fb: FormBuilder,
              private licenciasService: LicenciasDisponiblesData ) {
    this.licenciasForm = this.fb.group({
      'total': new FormControl('', Validators.compose([Validators.required])),
      'licenciacontrol': new FormControl('', Validators.compose([Validators.required])),
    });
  }
  getlicencias() {
    this.licenciasService.get().pipe(map(data => this.licenciasdisponibles = data)).subscribe(data => {
      this.licencias = [];
      for (let i = 0; i < this.licenciasdisponibles.length; i++) {
        this.licencias.push({'label': this.licenciasdisponibles[i].nombre, 'value': this.licenciasdisponibles[i].id});
      }
    });
  }
  getIdLicencia(event) {
    this.licenciasService.getById(event).subscribe(data => {
      this.licenciasdis = data;
      this.licenciasForm.controls['licenciacontrol'].setValue(this.licenciasdis.id);
      this.licenciasForm.controls['total'].setValue(this.licenciasdis.cantidad);
    });
  }
  guardar() {
    if (this.licenciasForm.invalid) {
      return;
    }
    this.submitted = true;
    this.licenciasService.put(this.licenciasdis.id, this.licenciasForm.value.total).subscribe(data => {
      this.licenciasService.postSocket({status: 'ok'}).subscribe(x => {});
      this.licenciasdi = data;
      this.ref.close();
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getlicencias();
  }

}
