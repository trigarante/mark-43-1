import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {Area, AreaData} from '../../../../@core/data/interfaces/catalogos/area';
import {NbDialogRef} from '@nebular/theme';
import {SubAreaComponent} from '../../../rrhh/catalogos/sub-area/sub-area.component';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'ngx-create-subarea',
  templateUrl: './create-subarea.component.html',
  styleUrls: ['./create-subarea.component.scss'],
})
export class CreateSubareaComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idArea: number;
  subAreaCreateForm: FormGroup;
  submitted: boolean;
  subArea: Subarea;
  area: any[];
  Areas: Area[];

  constructor(
    protected subAreaService: SubareaData, protected areaSeervice: AreaData,
    private fb: FormBuilder, protected ref: NbDialogRef<SubAreaComponent>, private router: Router,
  ) {
  }
  guardarSubArea() {
    if (this.subAreaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.subAreaService.posting(this.subAreaCreateForm.value).subscribe((result) => {
      this.subAreaCreateForm.reset();
      this.ref.close();
    });
  }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  updateSubArea() {
    this.subAreaService.put(this.idArea, this.subAreaCreateForm.value).subscribe(result => {
      this.subAreaCreateForm.reset();
      this.ref.close();
    });
  }

  getSubAreaById() {
    this.subAreaService.getById(this.idArea).subscribe(data => {
      this.subArea = data;
      this.subAreaCreateForm.controls['idArea'].setValue(this.subArea.idArea);
      this.subAreaCreateForm.controls['subarea'].setValue(this.subArea.subarea);
      this.subAreaCreateForm.controls['descripcion'].setValue(this.subArea.descripcion);
    });
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getSubAreaById();
    }
    this.subAreaCreateForm = this.fb.group({
      'idArea': this.idArea,
      'subarea': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
      'codigo': new FormControl('', Validators.compose([Validators.required])),
    });
  }

}
