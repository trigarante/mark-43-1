import {Component, ViewEncapsulation, OnInit, ViewChild} from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from 'd3-format';
import * as d3Inter from 'd3-interpolate';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import 'd3-transition';
import * as moment from 'moment';
import {EmpleadosService} from '../../../../@core/data/services/rrhh/empleados.service';
import {Empleados} from '../../../../@core/data/interfaces/rrhh/empleados';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'ngx-reporte-empleado',
  encapsulation: ViewEncapsulation.None,    // Necesaria para que se vea chidito. :v
  templateUrl: './reporte-empleado.component.html',
  styleUrls: ['./reporte-empleado.component.scss'],
  providers: [EmpleadosService],
})
export class ReporteEmpleadoComponent implements OnInit {
  title = 'Prueba';
  candidatos = [];
  flares: any;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;
  private maxRadius: number;
  x;
  y;
  partition;
  formatNumber;
  root;
  private arc: any;
  private color: any;
  private svg: any;
  datas: any;
  datasTable: any;
  cols: any[];
  etapas: any[];
  edoCivil: any[];
  dates: any;
  dates2: any;
  dates3: any;
  rangeDates: Date[];
  candidatos2 = [];
  flares2: any;
  partition2;
  formatNumber2;
  root2;
  private arc2: any;
  private color2: any;
  private svg2: any;
  datas2: any;
  candidatos3 = [];
  flares3: any;
  partition3;
  formatNumber3;
  root3;
  private arc3: any;
  private color3: any;
  private svg3: any;
  datas3: any;
  label = 'TOTAL DE EMPLEADOS:';
  label2 = 'TOTAL DE EMPLEADOS2:';
  label3 = 'TOTAL DE EMPLEADOS3:';
  dataSource: any;
  generalData: any;
  dataSource3: any;

  constructor(private empleadoService: EmpleadosService) {
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    // this.radius = Math.min(this.width, this.height) / 2;
    this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
    // this.cols = [
    //  {field: 'nombre', header: 'Nombre'},
    //  {field: 'edoCivil[idEstadoCivil]', header: 'Estado Civil'},
    //  {field: 'cp', header: 'Código Postal'},
    //  {field: 'puesto', header: 'Puesto'},
    //  {field: 'puestoDetalle', header: 'Puesto actual'},
    //  {field: 'fechaIngreso', header: 'Fecha de Ingreso'},
    // ];
    this.cols = ['nombre', 'idestadoCivil', 'cp', 'puesto', 'puestoDetalle', 'fechaIngreso'];
    this.etapas = ['Precandidato', 'Candidato', 'Empleado', 'Capacitación', 'Seguimiento 1', 'Seguimiento 2'];
    this.edoCivil = ['Soltero', 'Casado', 'Union Libre', 'Viuda(o)'];
    this.rangeDates = [
      new Date(2018, 11, 1, 0, 0),
      new Date(),
    ];
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.initSvg();
    this.initSvg2();
    this.initSvg3();
    this.getEmpleados(null);
  }
  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private initSvg() {
    const colors = ['#00adb5', '#309286', '#009975'];
    this.formatNumber = d3Format.format(',d');
    this.color = d3Scale.scaleOrdinal(colors);
    // d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition = d3Partition.partition();

    this.arc = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }
  private initSvg2() {
    const colors = ['#e2598b', '#866ec7', '#fdd043'];
    this.formatNumber2 = d3Format.format(',d');
    this.color2 = d3Scale.scaleOrdinal(colors);
    // this.color2 = d3Scale.scaleOrdinal(d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition2 = d3Partition.partition();

    this.arc2 = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }
  private initSvg3() {
    const colors = ['#eb5f5d', '#facd49', '#f7931e'];
    this.formatNumber3 = d3Format.format(',d');
    this.color3 = d3Scale.scaleOrdinal(colors);
    // this.color3 = d3Scale.scaleOrdinal(d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition3 = d3Partition.partition();

    this.arc3 = d3Shape.arc()
      .startAngle(d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }

  getEmpleados(rangeData: Date[]) {
    this.empleadoService.get()
      .subscribe(data => {
        if (rangeData == null) {
          this.datasTable = data;
          this.candidatos = data;
          this.dates = this.candidatos.length;
          this.candidatos2 = data;
          this.dates2 = this.candidatos2.length;
          this.candidatos3 = data;
          this.dates3 = this.candidatos3.length;
          this.drawPie(this.candidatos);
          this.drawPie2(this.candidatos2);
          this.drawPie3(this.candidatos3);
          this.generalData = data;
        } else {
          this.candidatos = [];
          this.candidatos2 = [];
          this.candidatos3 = [];
          this.generalData = [];

          // console.log(' FORMATO rangeData:: ' + moment(rangeData[1]).valueOf());
          // console.log('Fecha de servicio:: ' + moment(data[0].fechaCreacion).valueOf());

          for (let i = 0; i < data.length; i++) {
            this.dates = data[i].fechaCreacion;
            if (moment(data[i].fechaCreacion).valueOf() >= moment(rangeData[0]).valueOf()
              && moment(data[i].fechaCreacion).valueOf() <= moment(rangeData[1]).valueOf()) {
              this.candidatos.push(data[i]);
              this.candidatos2.push(data[i]);
              this.candidatos3.push(data[i]);
              this.generalData.push(data[i]);
            }
          }

          this.removeSvg();
          this.removeSvg2();
          this.removeSvg3();
          this.drawPie(this.candidatos);
          this.drawPie2(this.candidatos2);
          this.drawPie3(this.candidatos3);
        }
      });
  }

  private drawPie(data) {
    // console.log('1 = ',data.length);
    this.root = d3Partition.hierarchy(this.dataTransform(data));
    this.root.sum(d => d.size);
    this.root.each(d => d.current = d);
    this.svg = d3.select('#graph1')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn());
    const slice = this.svg.selectAll('g.slice')
      .data(this.partition(this.root).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn(d);
      });
    this.datas = this.dataFilter({data: this.flares});
    this.dataSource = new MatTableDataSource(this.dataFilter({data: this.flares}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.9 : 0.7)
      .attr('d', this.arc);

    newSlice.append('path')
      .attr('class', 'hidden-arc')
      .attr('id', (_, i) => 'hiddenArc' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', '#ffffff');
    return this.dataSource;
  }
  private drawPie2(data) {
    // console.log('2 = ',data.length);
    this.root2 = d3Partition.hierarchy(this.dataTransform2(data));
    this.root2.sum(d => d.size);
    this.root2.each(d => d.current = d);
    this.svg2 = d3.select('#graph2')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn2());
    const slice = this.svg2.selectAll('g.slice2')
      .data(this.partition2(this.root2).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice2')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn2(d);
      });
    this.datas2 = this.dataFilter2({data: this.flares2});
    this.dataSource = new MatTableDataSource(this.dataFilter2({data: this.flares2}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas2.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc2')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color2(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.9 : 0.7)
      .attr('d', this.arc2);

    newSlice.append('path')
      .attr('class', 'hidden-arc2')
      .attr('id', (_, i) => 'hiddenArcDos' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArcDos' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color2(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArcDos' + i)
      .text(d => d.data.name)
      .style('fill', '#313131');

    return this.dataSource;
  }
  private drawPie3(data) {
    // console.log('3 = ',data.length);
    this.root3 = d3Partition.hierarchy(this.dataTransform3(data));
    this.root3.sum(d => d.size);
    this.root3.each(d => d.current = d);
    this.svg3 = d3.select('#graph3')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn3());
    const slice = this.svg3.selectAll('g.slice3')
      .data(this.partition3(this.root3).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice3')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn3(d);
      });
    this.datas3 = this.dataFilter3({data: this.flares3});
    this.dataSource = new MatTableDataSource(this.dataFilter3({data: this.flares}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas3.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc3')
      .attr('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color3(d.data.name);
      })
      .attr('fill-opacity', d => d.children ? 0.9 : 0.7)
      .attr('d', this.arc3);

    newSlice.append('path')
      .attr('class', 'hidden-arc3')
      .attr('id', (_, i) => 'hiddenArcTres' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArcTres' + i)
      .text(d => d.data.name)
      .style('fill', d => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return this.color3(d.data.name);
      })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArcTres' + i)
      .text(d => d.data.name)
      .style('fill', '#032d3c');

    return this.dataSource;
  }

  private dataTransform(data: Empleados[]) {
    const activos = [];
    const acRecontratable = [];
    const acNoRecontratable = [];

    const bajas = [];
    const baRecontratable = [];
    const baNoRecontratable = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].estadoRH === 'ACTIVOS') {
        activos.push(data[i]);
        if (!data[i].recontratable) {
          acRecontratable.push(data[i]);
        } else {
          acNoRecontratable.push(data[i]);
        }
      } else if (data[i].estadoRH === 'BAJA') {
        bajas.push(data[i]);
        if (!data[i].recontratable) {
          baRecontratable.push(data[i]);
        } else {
          baNoRecontratable.push(data[i]);
        }
      }
    }

    this.flares = {
      name: 'Total de Empleados: ' + data.length,
      id: 'empleados',
      children: [],
    };

    const flaresActivos = {
      name: 'Total de Activos: ' + activos.length,
      id: 'activo',
      children: [
        {
          name: 'Total de Recontratables' + acRecontratable.length,
          id: 'activo.recontratable',
          size: acRecontratable.length,
        },
        {
          name: 'Total de No Recontratables' + acNoRecontratable.length,
          id: 'activo.norecontratable',
          size: acNoRecontratable.length,
        },
      ],
    };

    const flaresBajas = {
      name: 'Total de Bajas: ' + bajas.length,
      id: 'baja',
      children: [
        {
          name: 'Total de Recontratables: ' + baRecontratable.length,
          id: 'baja.recontratable',
          size: baRecontratable.length,
        },
        {
          name: 'Total de No Recontratables: ' + baNoRecontratable.length,
          id: 'baja.norecontratable',
          size: baNoRecontratable.length,
        },
      ],
    };

    this.flares.children.push(flaresActivos);
    this.flares.children.push(flaresBajas);
    return this.flares;
  }
  private dataTransform2(data: Empleados[]) {
    const Are1 = [];
    const Are1Sub1 = [];
    const Are1Sub2 = [];
    const Are1Sub3 = [];
    const Are1Sub4 = [];
    const Are1Sub5 = [];
    const Are1Sub6 = [];
    const Are1Sub7 = [];
    const Are2 = [];
    const Are2Sub1 = [];
    const Are2Sub2 = [];
    const Are2Sub3 = [];
    const Are2Sub4 = [];
    const Are2Sub5 = [];
    const Are2Sub6 = [];
    const Are2Sub7 = [];
    const Are3 = [];
    const Are3Sub1 = [];
    const Are3Sub2 = [];
    const Are3Sub3 = [];
    const Are3Sub4 = [];
    const Are3Sub5 = [];
    const Are3Sub6 = [];
    const Are3Sub7 = [];
    const Are4 = [];
    const Are4Sub1 = [];
    const Are4Sub2 = [];
    const Are4Sub3 = [];
    const Are4Sub4 = [];
    const Are4Sub5 = [];
    const Are4Sub6 = [];
    const Are4Sub7 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].area === 'MARKETING') {
        Are1.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are1Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are1Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are1Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are1Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are1Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are1Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are1Sub7.push(data[i]);
        }
      } else if (data[i].area === 'RRHH') {
        Are2.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are2Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are2Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are2Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are2Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are2Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are2Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are2Sub7.push(data[i]);
        }
      } else if (data[i].area === 'VENTA NUEVA') {
        Are3.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are3Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are3Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are3Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are3Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are3Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are3Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are3Sub7.push(data[i]);
        }
      } else if (data[i].area === 'FINANZAS') {
        Are4.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are4Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are4Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are4Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are4Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are4Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are4Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are4Sub7.push(data[i]);
        }
      }
    }

    this.flares2 = {
      name: 'Total de Empleados: ' + data.length,
      id: 'empleados',
      children: [],
    };

    const area1 = {
      name: 'Total de MARKETING: ' + Are1.length,
      id: 'area1',
      children: [
        {
          name: 'Total de Desarrollo: ' + Are1Sub1.length,
          id: 'area1.sub1',
          size: Are1Sub1.length,
        },
        {
          name: 'Total de DESARROLLO: ' + Are1Sub2.length,
          id: 'area1.sub2',
          size: Are1Sub2.length,
        },
        {
          name: 'Total de DESARROLLOPRUEBAS: ' + Are1Sub3.length,
          id: 'area1.sub3',
          size: Are1Sub3.length,
        },
        {
          name: 'Total de FINANZAS: ' + Are1Sub4.length,
          id: 'area1.sub4',
          size: Are1Sub4.length,
        },
        {
          name: 'Total de QUALITAS: ' + Are1Sub5.length,
          id: 'area1.sub5',
          size: Are1Sub5.length,
        },
        {
          name: 'Total de GNP: ' + Are1Sub6.length,
          id: 'area1.sub6',
          size: Are1Sub6.length,
        },
        {
          name: 'Total de INBURSA: ' + Are1Sub7.length,
          id: 'area1.sub7',
          size: Are1Sub7.length,
        },
      ],
    };

    const area2 = {
      name: 'Total de RRHH: ' + Are2.length,
      id: 'area2',
      children: [
        {
          name: 'Total de Desarrollo: ' + Are2Sub1.length,
          id: 'area2.sub1',
          size: Are2Sub1.length,
        },
        {
          name: 'Total de DESARROLLO: ' + Are2Sub2.length,
          id: 'area2.sub2',
          size: Are2Sub2.length,
        },
        {
          name: 'Total de DESARROLLOPRUEBAS: ' + Are2Sub3.length,
          id: 'area2.sub3',
          size: Are2Sub3.length,
        },
        {
          name: 'Total de FINANZAS: ' + Are2Sub4.length,
          id: 'area2.sub4',
          size: Are2Sub4.length,
        },
        {
          name: 'Total de QUALITAS: ' + Are2Sub5.length,
          id: 'area2.sub5',
          size: Are2Sub5.length,
        },
        {
          name: 'Total de GNP: ' + Are2Sub6.length,
          id: 'area2.sub6',
          size: Are2Sub6.length,
        },
        {
          name: 'Total de INBURSA: ' + Are2Sub7.length,
          id: 'area2.sub7',
          size: Are2Sub7.length,
        },
      ],
    };

    const area3 = {
      name: 'Total de VENTA NUEVA: ' + Are3.length,
      id: 'area3',
      children: [
        {
          name: 'Total de Desarrollo: ' + Are3Sub1.length,
          id: 'area3.sub1',
          size: Are3Sub1.length,
        },
        {
          name: 'Total de DESARROLLO: ' + Are3Sub2.length,
          id: 'area3.sub2',
          size: Are3Sub2.length,
        },
        {
          name: 'Total de DESARROLLOPRUEBAS: ' + Are3Sub3.length,
          id: 'area3.sub3',
          size: Are3Sub3.length,
        },
        {
          name: 'Total de FINANZAS: ' + Are3Sub4.length,
          id: 'area3.sub4',
          size: Are3Sub4.length,
        },
        {
          name: 'Total de QUALITAS: ' + Are3Sub5.length,
          id: 'area3.sub5',
          size: Are3Sub5.length,
        },
        {
          name: 'Total de GNP: ' + Are3Sub6.length,
          id: 'area3.sub6',
          size: Are3Sub6.length,
        },
        {
          name: 'Total de INBURSA: ' + Are3Sub7.length,
          id: 'area3.sub7',
          size: Are3Sub7.length,
        },
      ],
    };

    const area4 = {
      name: 'Total de FINANZAS: ' + Are4.length,
      id: 'area4',
      children: [
        {
          name: 'Total de Desarrollo: ' + Are4Sub1.length,
          id: 'area4.sub1',
          size: Are4Sub1.length,
        },
        {
          name: 'Total de DESARROLLO: ' + Are4Sub2.length,
          id: 'area4.sub2',
          size: Are4Sub2.length,
        },
        {
          name: 'Total de DESARROLLOPRUEBAS: ' + Are4Sub3.length,
          id: 'area4.sub3',
          size: Are4Sub3.length,
        },
        {
          name: 'Total de FINANZAS: ' + Are4Sub4.length,
          id: 'area4.sub4',
          size: Are4Sub4.length,
        },
        {
          name: 'Total de QUALITAS: ' + Are4Sub5.length,
          id: 'area4.sub5',
          size: Are4Sub5.length,
        },
        {
          name: 'Total de GNP: ' + Are4Sub6.length,
          id: 'area4.sub6',
          size: Are4Sub6.length,
        },
        {
          name: 'Total de INBURSA: ' + Are4Sub7.length,
          id: 'area4.sub7',
          size: Are4Sub7.length,
        },
      ],
    };

    this.flares2.children.push(area1);
    this.flares2.children.push(area2);
    this.flares2.children.push(area3);
    this.flares2.children.push(area4);
    return this.flares2;
  }
  private dataTransform3(data: Empleados[]) {
    const genero = [[], []];
    const edades = [
      [],   // < 20
      [],   // 21 - 30
      [],   // 31 - 40
      [],   // 41 - 50
      [],   // 51 - 60
    ];
    const fprimaria = [[], [], [], []];
    const fsecundaria = [[], [], [], []];
    const fpreparatoria = [[], [], [], []];
    const flicenciatura = [[], [], [], []];
    const fmaestria = [[], [], [], []];
    const fdoctorado = [[], [], [], []];
    const mprimaria = [[], [], [], []];
    const msecundaria = [[], [], [], []];
    const mpreparatoria = [[], [], [], []];
    const mlicenciatura = [[], [], [], []];
    const mmaestria = [[], [], [], []];
    const mdoctorado = [[], [], [], []];

    for (let i = 0; i < data.length; i++) {
      if (data[i].genero === 'F') {
        genero[0].push(data[i]);
        if (true) {
          if (data[i].idEscolaridad === 1) {    // Primaria
            fprimaria[0].push(data[i]);
            fprimaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 2) {    // Secundaria en curso
            fsecundaria[0].push(data[i]);
            fsecundaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 3) {    // Secundaria trunca
            fsecundaria[0].push(data[i]);
            fsecundaria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 4) {    // Secundaria terminada
            fsecundaria[0].push(data[i]);
            fsecundaria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 5) {    // Preparatoria en curso
            fpreparatoria[0].push(data[i]);
            fpreparatoria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 6) {    // Preparatoria trunca
            fpreparatoria[0].push(data[i]);
            fpreparatoria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 7) {    // Preparatoria terminada
            fpreparatoria[0].push(data[i]);
            fpreparatoria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 8) {    // Licenciatura en curso
            flicenciatura[0].push(data[i]);
            flicenciatura[1].push(data[i]);
          } else if (data[i].idEscolaridad === 9) {    // Licenciatura trunca
            flicenciatura[0].push(data[i]);
            flicenciatura[2].push(data[i]);
          } else if (data[i].idEscolaridad === 10) {    // Licenciatura Terminada
            flicenciatura[0].push(data[i]);
            flicenciatura[3].push(data[i]);
          } else if (data[i].idEscolaridad === 11) {    // Maestria
            fmaestria[0].push(data[i]);
            fmaestria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 12) {    // Doctorado
            fdoctorado[0].push(data[i]);
            fdoctorado[1].push(data[i]);
          }
        }
      } else if (data[i].genero === 'M') {
        genero[1].push(data[i]);
        if (true) {
          if (data[i].idEscolaridad === 1) {    // Primaria
            mprimaria[0].push(data[i]);
            mprimaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 2) {    // Secundaria en curso
            msecundaria[0].push(data[i]);
            msecundaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 3) {    // Secundaria trunca
            msecundaria[0].push(data[i]);
            msecundaria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 4) {    // Secundaria terminada
            msecundaria[0].push(data[i]);
            msecundaria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 5) {    // Preparatoria en curso
            mpreparatoria[0].push(data[i]);
            mpreparatoria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 6) {    // Preparatoria trunca
            mpreparatoria[0].push(data[i]);
            mpreparatoria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 7) {    // Preparatoria terminada
            mpreparatoria[0].push(data[i]);
            mpreparatoria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 8) {    // Licenciatura en curso
            mlicenciatura[0].push(data[i]);
            mlicenciatura[1].push(data[i]);
          } else if (data[i].idEscolaridad === 9) {    // Licenciatura trunca
            mlicenciatura[0].push(data[i]);
            mlicenciatura[2].push(data[i]);
          } else if (data[i].idEscolaridad === 10) {    // Licenciatura Terminada
            mlicenciatura[0].push(data[i]);
            mlicenciatura[3].push(data[i]);
          } else if (data[i].idEscolaridad === 11) {    // Maestria
            mmaestria[0].push(data[i]);
            mmaestria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 12) {    // Doctorado
            mdoctorado[0].push(data[i]);
            mdoctorado[1].push(data[i]);
          }
        }
      }
    }

    this.flares3 = {
      name: 'Total de Empleados: ' + data.length,
      id: 'empleados',
      children: [],
    };

    const femenino = {
      name: 'Total de mujeres: ' + genero[0].length,
      id: 'feme',
      children: [],
    };

    const masculino = {
      name: 'Total de hombres: ' + genero[1].length,
      id: 'masc',
      children: [],
    };

    const femPrimaria = {
      name: 'Total de con primaria: ' + fprimaria[0].length,
      id: 'feme.primaria',
      children: [
        {
          name: 'Total de Terminada: ' + fprimaria[3].length,
          id: 'feme.primaria.terminada',
          size: fprimaria[3].length,
        },
        {
          name: 'Total de Trunca: ' + fprimaria[2].length,
          id: 'feme.primaria.trunca',
          size: fprimaria[2].length,
        },
        {
          name: 'Total de En Curso: ' + fprimaria[1].length,
          id: 'feme.primaria.encurso',
          size: fprimaria[1].length,
        },
      ],
    };

    const femSecundaria = {
      name: 'Total de con secundaria: ' + fsecundaria[0].length,
      id: 'feme.secundaria',
      children: [
        {
          name: 'Total de Terminada: ' + fsecundaria[3].length,
          id: 'feme.secundaria.terminada',
          size: fsecundaria[3].length,
        },
        {
          name: 'Total de Trunca: ' + fsecundaria[2].length,
          id: 'feme.secundaria.trunca',
          size: fsecundaria[2].length,
        },
        {
          name: 'Total de En Curso: ' + fsecundaria[1].length,
          id: 'feme.secundaria.encurso',
          size: fsecundaria[1].length,
        },
      ],
    };

    const femPreparatoria = {
      name: 'Total de con preparatoria: ' + fpreparatoria[0].length,
      id: 'feme.preparatoria',
      children: [
        {
          name: 'Total de Terminada: ' + fpreparatoria[3].length,
          id: 'feme.preparatoria.terminada',
          size: fpreparatoria[3].length,
        },
        {
          name: 'Total de Trunca: ' + fpreparatoria[2].length,
          id: 'feme.preparatoria.trunca',
          size: fpreparatoria[2].length,
        },
        {
          name: 'Total de En Curso: ' + fpreparatoria[1].length,
          id: 'feme.preparatoria.encurso',
          size: fpreparatoria[1].length,
        },
      ],
    };

    const femLicenciatura = {
      name: flicenciatura[0].length + 'licenciatura',
      children: [
        {
          name: 'Total de Terminada: ' + flicenciatura[3].length,
          id: 'feme.licenciatura.terminada',
          size: flicenciatura[3].length,
        },
        {
          name: 'Total de Trunca: ' + flicenciatura[2].length,
          id: 'feme.licenciatura.trunca',
          size: flicenciatura[2].length,
        },
        {
          name: 'Total de En Curso: ' + flicenciatura[1].length,
          id: 'feme.licenciatura.encurso',
          size: flicenciatura[1].length,
        },
      ],
    };

    const femMaestria = {
      name: 'Total de con maestria: ' + fmaestria[0].length,
      id: 'feme.maestria',
      children: [
        {
          name: 'Total de Terminada: ' + fmaestria[3].length,
          id: 'feme.maestria.terminada',
          size: fmaestria[3].length,
        },
        {
          name: 'Total de Trunca: ' + fmaestria[2].length,
          id: 'feme.maestria.trunca',
          size: fmaestria[2].length,
        },
        {
          name: 'Total de En Curso: ' + fmaestria[1].length,
          id: 'feme.maestria.encurso',
          size: fmaestria[1].length,
        },
      ],
    };

    const femDoctorado = {
      name: 'Total de con doctorado: ' + fdoctorado[0].length,
      id: 'feme.doctorado',
      children: [
        {
          name: 'Total de Terminada: ' + fdoctorado[3].length,
          id: 'feme.doctorado.terminada',
          size: fdoctorado[3].length,
        },
        {
          name: 'Total de Trunca: ' + fdoctorado[2].length,
          id: 'feme.doctorado.trunca',
          size: fdoctorado[2].length,
        },
        {
          name: 'Total de En Curso: ' + fdoctorado[1].length,
          id: 'feme.doctorado.encurso',
          size: fdoctorado[1].length,
        },
      ],
    };

    const masPrimaria = {
      name: 'Total de con primaria: ' + mprimaria[0].length,
      id: 'masc.primaria',
      children: [
        {
          name: 'Total de Terminada: ' + mprimaria[3].length,
          id: 'masc.primaria.terminada',
          size: mprimaria[3].length,
        },
        {
          name: 'Total de Trunca: ' + mprimaria[2].length,
          id: 'masc.primaria.trunca',
          size: mprimaria[2].length,
        },
        {
          name: 'Total de En Curso: ' + mprimaria[1].length,
          id: 'masc.primaria.encurso',
          size: mprimaria[1].length,
        },
      ],
    };

    const masSecundaria = {
      name: 'Total de con secundaria: ' + msecundaria[0].length,
      id: 'masc.secundaria',
      children: [
        {
          name: 'Total de Terminada: ' + msecundaria[3].length,
          id: 'masc.secundaria.terminada',
          size: msecundaria[3].length,
        },
        {
          name: 'Total de Trunca: ' + msecundaria[2].length,
          id: 'masc.secundaria.trunca',
          size: msecundaria[2].length,
        },
        {
          name: 'Total de En Curso: ' + msecundaria[1].length,
          id: 'masc.secundaria.encurso',
          size: msecundaria[1].length,
        },
      ],
    };

    const masPreparatoria = {
      name: 'Total de con preparatoria: ' + mpreparatoria[0].length,
      id: 'masc.preparatoria',
      children: [
        {
          name: 'Total de Terminada: ' + mpreparatoria[3].length,
          id: 'masc.preparatoria.terminada',
          size: mpreparatoria[3].length,
        },
        {
          name: 'Total de Trunca: ' + mpreparatoria[2].length,
          id: 'masc.preparatoria.trunca',
          size: mpreparatoria[2].length,
        },
        {
          name: 'Total de En Curso: ' + mpreparatoria[1].length,
          id: 'masc.preparatoria.encurso',
          size: mpreparatoria[1].length,
        },
      ],
    };

    const masLicenciatura = {
      name: 'Total de con licenciatura: ' + mlicenciatura[0].length,
      id: 'masc.licenciatura',
      children: [
        {
          name: 'Total de Terminada: ' + mlicenciatura[3].length,
          id: 'masc.licenciatura.terminada',
          size: mlicenciatura[3].length,
        },
        {
          name: 'Total de Trunca: ' + mlicenciatura[2].length,
          id: 'masc.licenciatura.trunca',
          size: mlicenciatura[2].length,
        },
        {
          name: 'Total de En Curso: ' + mlicenciatura[1].length,
          id: 'masc.licenciatura.encurso',
          size: mlicenciatura[1].length,
        },
      ],
    };

    const masMaestria = {
      name: 'Total de con maestria: ' + mmaestria[0].length,
      id: 'masc.maestria',
      children: [
        {
          name: 'Total de Terminada: ' + mmaestria[3].length,
          id: 'masc.maestria.terminada',
          size: mmaestria[3].length,
        },
        {
          name: 'Total de Trunca: ' + mmaestria[2].length,
          id: 'masc.maestria.trunca',
          size: mmaestria[2].length,
        },
        {
          name: 'Total de En Curso: ' + mmaestria[1].length,
          id: 'masc.maestria.encurso',
          size: mmaestria[1].length,
        },
      ],
    };

    const masDoctorado = {
      name: 'Total de con doctorado: ' + mdoctorado[0].length,
      id: 'masc.doctorado',
      children: [
        {
          name: 'Total de Terminada: ' + mdoctorado[3].length,
          id: 'masc.doctorado.terminada',
          size: mdoctorado[3].length,
        },
        {
          name: 'Total de Trunca: ' + mdoctorado[2].length,
          id: 'masc.doctorado.trunca',
          size: mdoctorado[2].length,
        },
        {
          name: 'Total de En Curso: ' + mdoctorado[1].length,
          id: 'masc.doctorado.encurso',
          size: mdoctorado[1].length,
        },
      ],
    };

    femenino.children.push(femPrimaria);
    femenino.children.push(femSecundaria);
    femenino.children.push(femPreparatoria);
    femenino.children.push(femLicenciatura);
    femenino.children.push(femMaestria);
    femenino.children.push(femDoctorado);
    masculino.children.push(masPrimaria);
    masculino.children.push(masSecundaria);
    masculino.children.push(masPreparatoria);
    masculino.children.push(masLicenciatura);
    masculino.children.push(masMaestria);
    masculino.children.push(masDoctorado);

    this.flares3.children.push(femenino);
    this.flares3.children.push(masculino);
    return this.flares3;
  }

  private focusOn(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg.transition()
      .duration(500)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc')
      .attrTween('d', d => () => this.arc(d));

    transition.selectAll('path.hidden-arc')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  private focusOn2(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg2.transition()
      .duration(500)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc2')
      .attrTween('d', d => () => this.arc2(d));

    transition.selectAll('path.hidden-arc2')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter2(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  private focusOn3(d = {x0: 0, x1: 1, y0: 0, y1: 1}) {
    const transition = this.svg3.transition()
      .duration(500)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => {
          this.x.domain(xd(t));
          this.y.domain(yd(t));
        };
      });

    transition.selectAll('path.main-arc3')
      .attrTween('d', d => () => this.arc3(d));

    transition.selectAll('path.hidden-arc3')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter3(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // Adecuar el nombre de  filtros a la base en los dataFilter
  private dataFilter(d: any) {
    const data = this.candidatos;

    const activos = [];
    const acRecontratable = [];
    const acNoRecontratable = [];

    const bajas = [];
    const baRecontratable = [];
    const baNoRecontratable = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].estadoRH === 'ACTIVOS') {
        activos.push(data[i]);
        if (!data[i].recontratable) {
          acRecontratable.push(data[i]);
        } else {
          acNoRecontratable.push(data[i]);
        }
      } else if (data[i].estadoRH === 'BAJA') {
        bajas.push(data[i]);
        if (!data[i].recontratable) {
          baRecontratable.push(data[i]);
        } else {
          baNoRecontratable.push(data[i]);
        }
      }
    }

    if (d.data.id === 'empleados') {
      this.label = 'TOTAL DE EMPLEADOS:';
      this.dates = data.length;
      this.datasTable = data;
      // this.candidatos2 = this.generalData;
      // this.candidatos3 = this.generalData;
      // this.candidatos2 = data;
      // this.candidatos3 = data;
      return data;
    } else if (d.data.id === 'activo') {
      this.label = 'TOTAL DE ACTIVOS:';
      this.dates = activos.length;
      this.candidatos2 = activos;
      this.candidatos3 = activos;
      this.removeSvg2();
      this.printSvg2(activos);
      this.removeSvg3();
      this.printSvg3(activos);
      this.datasTable = activos;
      return activos;
    } else if (d.data.id === 'baja') {
      this.label = 'TOTAL DE BAJAS:';
      this.dates = bajas.length;
      this.candidatos2 = bajas;
      this.candidatos3 = bajas;
      this.removeSvg2();
      this.printSvg2(bajas);
      this.removeSvg3();
      this.printSvg3(bajas);
      this.datasTable = bajas;
      return bajas;
    } else if (d.data.id === 'activo.recontratable') {
      this.candidatos2 = acRecontratable;
      this.candidatos3 = acRecontratable;
      this.removeSvg2();
      this.printSvg2(acRecontratable);
      this.removeSvg3();
      this.printSvg3(acRecontratable);
      this.datasTable = acRecontratable;
      return acRecontratable;
    } else if (d.data.id === 'activo.norecontratable') {
      this.candidatos2 = acNoRecontratable;
      this.candidatos3 = acNoRecontratable;
      this.removeSvg2();
      this.printSvg2(acNoRecontratable);
      this.removeSvg3();
      this.printSvg3(acNoRecontratable);
      this.datasTable = acNoRecontratable;
      return acNoRecontratable;
    } else if (d.data.id === 'baja.recontratable') {
      this.candidatos2 = baRecontratable;
      this.candidatos3 = baRecontratable;
      this.removeSvg2();
      this.printSvg2(baRecontratable);
      this.removeSvg3();
      this.printSvg3(baRecontratable);
      this.datasTable = baRecontratable;
      return baRecontratable;
    } else if (d.data.id === 'baja.norecontratable') {
      this.candidatos2 = baNoRecontratable;
      this.candidatos3 = baNoRecontratable;
      this.removeSvg2();
      this.printSvg2(baNoRecontratable);
      this.removeSvg3();
      this.printSvg3(baNoRecontratable);
      this.datasTable = baNoRecontratable;
      return baNoRecontratable;
    }
  }
  private dataFilter2(d: any) {
    const data = this.candidatos2;

    const Are1 = [];
    const Are1Sub1 = [];
    const Are1Sub2 = [];
    const Are1Sub3 = [];
    const Are1Sub4 = [];
    const Are1Sub5 = [];
    const Are1Sub6 = [];
    const Are1Sub7 = [];
    const Are2 = [];
    const Are2Sub1 = [];
    const Are2Sub2 = [];
    const Are2Sub3 = [];
    const Are2Sub4 = [];
    const Are2Sub5 = [];
    const Are2Sub6 = [];
    const Are2Sub7 = [];
    const Are3 = [];
    const Are3Sub1 = [];
    const Are3Sub2 = [];
    const Are3Sub3 = [];
    const Are3Sub4 = [];
    const Are3Sub5 = [];
    const Are3Sub6 = [];
    const Are3Sub7 = [];
    const Are4 = [];
    const Are4Sub1 = [];
    const Are4Sub2 = [];
    const Are4Sub3 = [];
    const Are4Sub4 = [];
    const Are4Sub5 = [];
    const Are4Sub6 = [];
    const Are4Sub7 = [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].area === 'MARKETING') {
        Are1.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are1Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are1Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are1Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are1Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are1Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are1Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are1Sub7.push(data[i]);
        }
      } else if (data[i].area === 'RRHH') {
        Are2.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are2Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are2Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are2Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are2Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are2Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are2Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are2Sub7.push(data[i]);
        }
      } else if (data[i].area === 'VENTA NUEVA') {
        Are3.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are3Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are3Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are3Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are3Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are3Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are3Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are3Sub7.push(data[i]);
        }
      } else if (data[i].area === 'FINANZAS') {
        Are4.push(data[i]);
        if (data[i].subarea === 'Desarrollo') {
          Are4Sub1.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLO') {
          Are4Sub2.push(data[i]);
        } else if (data[i].subarea === 'DESARROLLOPRUEBAS') {
          Are4Sub3.push(data[i]);
        } else if (data[i].subarea === 'FINANZAS') {
          Are4Sub4.push(data[i]);
        } else if (data[i].subarea === 'QUALITAS') {
          Are4Sub5.push(data[i]);
        } else if (data[i].subarea === 'GNP') {
          Are4Sub6.push(data[i]);
        } else if (data[i].subarea === 'INBURSA') {
          Are4Sub7.push(data[i]);
        }
      }
    }

    if (d.data.id === 'empleados') {
      this.label2 = 'TOTAL DE EMPLEADOS:';
      this.dates2 = data.length;
      // this.candidatos = this.generalData;
      // this.candidatos3 = this.generalData;
      // this.candidatos = data;
      // this.candidatos3 = data;
      return data;
    } else if (d.data.id === 'area1') {
      this.label = 'TOAL EN MARKETING:';
      this.dates2 = Are1.length;
      this.datasTable = Are1;
      this.removeSvg();
      this.printSvg(Are1);
      this.removeSvg3();
      this.printSvg3(Are1);
      this.candidatos = Are1;
      this.candidatos3 = Are1;
      return Are1;
    } else if (d.data.id === 'area1.sub1') {
      this.label2 = 'TOTAL DE DESARROLLO:';
      this.dates2 = Are1Sub1.length;
      this.datasTable = Are1Sub1;
      this.removeSvg();
      this.printSvg(Are1Sub1);
      this.removeSvg3();
      this.printSvg3(Are1Sub1);
      this.candidatos = Are1Sub1;
      this.candidatos3 = Are1Sub1;
      return Are1Sub1;
    } else if (d.data.id === 'area1.sub2') {
      this.label = 'TOTAL EN DESARROLLO WEB:';
      this.dates2 = Are1Sub2.length;
      this.datasTable = Are1Sub2;
      this.removeSvg();
      this.printSvg(Are1Sub2);
      this.removeSvg3();
      this.printSvg3(Are1Sub2);
      this.candidatos = Are1Sub2;
      this.candidatos3 = Are1Sub2;
      return Are1Sub2;
    } else if (d.data.id === 'area1.sub3') {
      this.label2 = 'TOTAL EN DESARROLLO PRUEBAS:';
      this.dates2 = Are1Sub3.length;
      this.datasTable = Are1Sub3;
      this.removeSvg();
      this.printSvg(Are1Sub3);
      this.removeSvg3();
      this.printSvg3(Are1Sub3);
      this.candidatos = Are1Sub3;
      this.candidatos3 = Are1Sub3;
      return Are1Sub3;
    } else if (d.data.id === 'area1.sub4') {
      this.label2 = 'TOTAL EN TAL X CUAL:';
      this.dates2 = Are1Sub4.length;
      this.datasTable = Are1Sub4;
      this.removeSvg();
      this.printSvg(Are1Sub4);
      this.removeSvg3();
      this.printSvg3(Are1Sub4);
      this.candidatos = Are1Sub4;
      this.candidatos3 = Are1Sub4;
      return Are1Sub4;
    } else if (d.data.id === 'area1.sub5') {
      this.datasTable = Are1Sub5;
      this.removeSvg();
      this.printSvg(Are1Sub5);
      this.removeSvg3();
      this.printSvg3(Are1Sub5);
      this.candidatos = Are1Sub5;
      this.candidatos3 = Are1Sub5;
      return Are1Sub5;
    } else if (d.data.id === 'area1.sub6') {
      this.datasTable = Are1Sub6;
      this.removeSvg();
      this.printSvg(Are1Sub6);
      this.removeSvg3();
      this.printSvg3(Are1Sub6);
      this.candidatos = Are1Sub6;
      this.candidatos3 = Are1Sub6;
      return Are1Sub6;
    } else if (d.data.id === 'area1.sub7') {
      this.datasTable = Are1Sub7;
      this.removeSvg();
      this.printSvg(Are1Sub7);
      this.removeSvg3();
      this.printSvg3(Are1Sub7);
      this.candidatos = Are1Sub7;
      this.candidatos3 = Are1Sub7;
      return Are1Sub7;
    } else if (d.data.id === 'area2') {
      this.datasTable = Are2;
      this.removeSvg();
      this.printSvg(Are2);
      this.removeSvg3();
      this.printSvg3(Are2);
      this.candidatos = Are2;
      this.candidatos3 = Are2;
      return Are2;
    } else if (d.data.id === 'area2.sub1') {
      this.datasTable = Are2Sub1;
      this.removeSvg();
      this.printSvg(Are2Sub1);
      this.removeSvg3();
      this.printSvg3(Are2Sub1);
      this.candidatos = Are2Sub1;
      this.candidatos3 = Are2Sub1;
      return Are2Sub1;
    } else if (d.data.id === 'area2.sub2') {
      this.datasTable = Are2Sub2;
      this.removeSvg();
      this.printSvg(Are2Sub2);
      this.removeSvg3();
      this.printSvg3(Are2Sub2);
      this.candidatos = Are2Sub2;
      this.candidatos3 = Are2Sub2;
      return Are2Sub2;
    } else if (d.data.id === 'area2.sub3') {
      this.datasTable = Are2Sub3;
      this.removeSvg();
      this.printSvg(Are2Sub3);
      this.removeSvg3();
      this.printSvg3(Are2Sub3);
      this.candidatos = Are2Sub3;
      this.candidatos3 = Are2Sub3;
      return Are2Sub3;
    } else if (d.data.id === 'area2.sub4') {
      this.datasTable = Are2Sub4;
      this.removeSvg();
      this.printSvg(Are2Sub4);
      this.removeSvg3();
      this.printSvg3(Are2Sub4);
      this.candidatos = Are2Sub4;
      this.candidatos3 = Are2Sub4;
      return Are2Sub4;
    } else if (d.data.id === 'area2.sub5') {
      this.datasTable = Are2Sub5;
      this.removeSvg();
      this.printSvg(Are2Sub5);
      this.removeSvg3();
      this.printSvg3(Are2Sub5);
      this.candidatos = Are2Sub5;
      this.candidatos3 = Are2Sub5;
      return Are2Sub5;
    } else if (d.data.id === 'area2.sub6') {
      this.datasTable = Are2Sub6;
      this.removeSvg();
      this.printSvg(Are2Sub6);
      this.removeSvg3();
      this.printSvg3(Are2Sub6);
      this.candidatos = Are2Sub6;
      this.candidatos3 = Are2Sub6;
      return Are2Sub6;
    } else if (d.data.id === 'area2.sub7') {
      this.datasTable = Are2Sub7;
      this.removeSvg();
      this.printSvg(Are2Sub7);
      this.removeSvg3();
      this.printSvg3(Are2Sub7);
      this.candidatos = Are2Sub7;
      this.candidatos3 = Are2Sub7;
      return Are2Sub7;
    } else if (d.data.id === 'area3') {
      this.label2 = 'TOTAL EN VENTA NUEVA:';
      this.dates2 = Are3.length;
      this.datasTable = Are3;
      this.removeSvg();
      this.printSvg(Are3);
      this.removeSvg3();
      this.printSvg3(Are3);
      this.candidatos = Are3;
      this.candidatos3 = Are3;
      return Are3;
    } else if (d.data.id === 'area3.sub1') {
      this.label2 = 'TOTAL EN PRUBAS SUB AREA1:';
      this.dates2 = Are3Sub1.length;
      this.datasTable = Are3Sub1;
      this.removeSvg();
      this.printSvg(Are3Sub1);
      this.removeSvg3();
      this.printSvg3(Are3Sub1);
      this.candidatos = Are3Sub1;
      this.candidatos3 = Are3Sub1;
      return Are3Sub1;
    } else if (d.data.id === 'area3.sub2') {
      this.datasTable = Are3Sub2;
      this.removeSvg();
      this.printSvg(Are3Sub2);
      this.removeSvg3();
      this.printSvg3(Are3Sub2);
      this.candidatos = Are3Sub2;
      this.candidatos3 = Are3Sub2;
      return Are3Sub2;
    } else if (d.data.id === 'area3.sub3') {
      this.datasTable = Are3Sub3;
      this.removeSvg();
      this.printSvg(Are3Sub3);
      this.removeSvg3();
      this.printSvg3(Are3Sub3);
      this.candidatos = Are3Sub3;
      this.candidatos3 = Are3Sub3;
      return Are3Sub3;
    } else if (d.data.id === 'area3.sub4') {
      this.label2 = 'TOTAL EN  GNP:';
      this.dates2 = Are3Sub4.length;
      this.datasTable = Are3Sub4;
      this.removeSvg();
      this.printSvg(Are3Sub4);
      this.removeSvg3();
      this.printSvg3(Are3Sub4);
      this.candidatos = Are3Sub4;
      this.candidatos3 = Are3Sub4;
      return Are3Sub4;
    } else if (d.data.id === 'area3.sub5') {
      this.label2 = 'TOTAL EN QUALITAS:';
      this.dates2 = Are3Sub5.length;
      this.datasTable = Are3Sub5;
      this.removeSvg();
      this.printSvg(Are3Sub5);
      this.removeSvg3();
      this.printSvg3(Are3Sub5);
      this.candidatos = Are3Sub5;
      this.candidatos3 = Are3Sub5;
      return Are3Sub5;
    } else if (d.data.id === 'area3.sub6') {
      this.datasTable = Are3Sub6;
      this.removeSvg();
      this.printSvg(Are3Sub6);
      this.removeSvg3();
      this.printSvg3(Are3Sub6);
      this.candidatos = Are3Sub6;
      this.candidatos3 = Are3Sub6;
      return Are3Sub6;
    } else if (d.data.id === 'area3.sub7') {
      this.datasTable = Are3Sub7;
      this.removeSvg();
      this.printSvg(Are3Sub7);
      this.removeSvg3();
      this.printSvg3(Are3Sub7);
      this.candidatos = Are3Sub7;
      this.candidatos3 = Are3Sub7;
      return Are3Sub7;
    } else if (d.data.id === 'area4') {
      this.datasTable = Are4;
      this.removeSvg();
      this.printSvg(Are4);
      this.removeSvg3();
      this.printSvg3(Are4);
      this.candidatos = Are4;
      this.candidatos3 = Are4;
      return Are4;
    } else if (d.data.id === 'area4.sub1') {
      this.datasTable = Are4Sub1;
      this.removeSvg();
      this.printSvg(Are4Sub1);
      this.removeSvg3();
      this.printSvg3(Are4Sub1);
      this.candidatos = Are4Sub1;
      this.candidatos3 = Are4Sub1;
      return Are4Sub1;
    } else if (d.data.id === 'area4.sub2') {
      this.datasTable = Are4Sub2;
      this.removeSvg();
      this.printSvg(Are4Sub2);
      this.removeSvg3();
      this.printSvg3(Are4Sub2);
      this.candidatos = Are4Sub2;
      this.candidatos3 = Are4Sub2;
      return Are4Sub2;
    } else if (d.data.id === 'area4.sub3') {
      this.datasTable = Are4Sub3;
      this.removeSvg();
      this.printSvg(Are4Sub3);
      this.removeSvg3();
      this.printSvg3(Are4Sub3);
      this.candidatos = Are4Sub3;
      this.candidatos3 = Are4Sub3;
      return Are4Sub3;
    } else if (d.data.id === 'area4.sub4') {
      this.datasTable = Are4Sub4;
      this.removeSvg();
      this.printSvg(Are4Sub4);
      this.removeSvg3();
      this.printSvg3(Are4Sub4);
      this.candidatos = Are4Sub4;
      this.candidatos3 = Are4Sub4;
      return Are4Sub4;
    } else if (d.data.id === 'area4.sub5') {
      this.datasTable = Are4Sub5;
      this.removeSvg();
      this.printSvg(Are4Sub5);
      this.removeSvg3();
      this.printSvg3(Are4Sub5);
      this.candidatos = Are4Sub5;
      this.candidatos3 = Are4Sub5;
      return Are4Sub5;
    } else if (d.data.id === 'area4.sub6') {
      this.datasTable = Are4Sub6;
      this.removeSvg();
      this.printSvg(Are4Sub6);
      this.removeSvg3();
      this.printSvg3(Are4Sub6);
      this.candidatos = Are4Sub6;
      this.candidatos3 = Are4Sub6;
      return Are4Sub6;
    } else if (d.data.id === 'area4.sub7') {
      this.datasTable = Are4Sub7;
      this.removeSvg();
      this.printSvg(Are4Sub7);
      this.removeSvg3();
      this.printSvg3(Are4Sub7);
      this.candidatos = Are4Sub7;
      this.candidatos3 = Are4Sub7;
      return Are4Sub7;
    }
  }
  private dataFilter3(d: any) {
    const data = this.candidatos3;
    const genero = [[], []];
    const edades = [
      [],   // < 20
      [],   // 21 - 30
      [],   // 31 - 40
      [],   // 41 - 50
      [],   // 51 - 60
    ];
    const fprimaria = [[], [], [], []];
    const fsecundaria = [[], [], [], []];
    const fpreparatoria = [[], [], [], []];
    const flicenciatura = [[], [], [], []];
    const fmaestria = [[], [], [], []];
    const fdoctorado = [[], [], [], []];
    const mprimaria = [[], [], [], []];
    const msecundaria = [[], [], [], []];
    const mpreparatoria = [[], [], [], []];
    const mlicenciatura = [[], [], [], []];
    const mmaestria = [[], [], [], []];
    const mdoctorado = [[], [], [], []];

    for (let i = 0; i < data.length; i++) {
      if (data[i].genero === 'F') {
        genero[0].push(data[i]);
        if (true) {
          if (data[i].idEscolaridad === 1) {    // Primaria
            fprimaria[0].push(data[i]);
            fprimaria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 2) {    // Secundaria en curso
            fsecundaria[0].push(data[i]);
            fsecundaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 3) {    // Secundaria trunca
            fsecundaria[0].push(data[i]);
            fsecundaria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 4) {    // Secundaria terminada
            fsecundaria[0].push(data[i]);
            fsecundaria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 5) {    // Preparatoria en curso
            fpreparatoria[0].push(data[i]);
            fpreparatoria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 6) {    // Preparatoria trunca
            fpreparatoria[0].push(data[i]);
            fpreparatoria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 7) {    // Preparatoria terminada
            fpreparatoria[0].push(data[i]);
            fpreparatoria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 8) {    // Licenciatura en curso
            flicenciatura[0].push(data[i]);
            flicenciatura[1].push(data[i]);
          } else if (data[i].idEscolaridad === 9) {    // Licenciatura trunca
            flicenciatura[0].push(data[i]);
            flicenciatura[2].push(data[i]);
          } else if (data[i].idEscolaridad === 10) {    // Licenciatura Terminada
            flicenciatura[0].push(data[i]);
            flicenciatura[3].push(data[i]);
          } else if (data[i].idEscolaridad === 11) {    // Maestria
            fmaestria[0].push(data[i]);
            fmaestria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 12) {    // Doctorado
            fdoctorado[0].push(data[i]);
            fdoctorado[1].push(data[i]);
          }
        }
      } else if (data[i].genero === 'M') {
        genero[1].push(data[i]);
        if (true) {
          if (data[i].idEscolaridad === 1) {    // Primaria
            mprimaria[0].push(data[i]);
            mprimaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 2) {    // Secundaria en curso
            msecundaria[0].push(data[i]);
            msecundaria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 3) {    // Secundaria trunca
            msecundaria[0].push(data[i]);
            msecundaria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 4) {    // Secundaria terminada
            msecundaria[0].push(data[i]);
            msecundaria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 5) {    // Preparatoria en curso
            mpreparatoria[0].push(data[i]);
            mpreparatoria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 6) {    // Preparatoria trunca
            mpreparatoria[0].push(data[i]);
            mpreparatoria[2].push(data[i]);
          } else if (data[i].idEscolaridad === 7) {    // Preparatoria terminada
            mpreparatoria[0].push(data[i]);
            mpreparatoria[3].push(data[i]);
          } else if (data[i].idEscolaridad === 8) {    // Licenciatura en curso
            mlicenciatura[0].push(data[i]);
            mlicenciatura[1].push(data[i]);
          } else if (data[i].idEscolaridad === 9) {    // Licenciatura trunca
            mlicenciatura[0].push(data[i]);
            mlicenciatura[2].push(data[i]);
          } else if (data[i].idEscolaridad === 10) {    // Licenciatura Terminada
            mlicenciatura[0].push(data[i]);
            mlicenciatura[3].push(data[i]);
          } else if (data[i].idEscolaridad === 11) {    // Maestria
            mmaestria[0].push(data[i]);
            mmaestria[1].push(data[i]);
          } else if (data[i].idEscolaridad === 12) {    // Doctorado
            mdoctorado[0].push(data[i]);
            mdoctorado[1].push(data[i]);
          }
        }
      }
    }

    if (d.data.id === 'empleados') {
      this.label3 = 'TOTAL DE EMPLEADOS:';
      this.dates3 = data.length;
      // this.candidatos = this.generalData;
      // this.candidatos2 = this.generalData;
      // this.candidatos = data;
      // this.candidatos2 = data;
      return data;
    } else if (d.data.id === 'feme') {      /************************* Femenino *************************/
    this.label3 = 'TOTAL DE MUJERES:';
      this.dates3 = genero[0].length;
      this.datasTable = genero[0];
      this.removeSvg();
      this.printSvg(genero[0]);
      this.removeSvg2();
      this.printSvg2(genero[0]);
      this.candidatos = genero[0];
      this.candidatos2 = genero[0];
      return genero[0];
    } else if (d.data.id === 'feme.primaria') {
      this.label3 = 'TOTAL CON PRIMARIA:';
      this.dates3 = fprimaria[0].length;
      this.datasTable = fprimaria[0];
      this.removeSvg();
      this.printSvg(fprimaria[0]);
      this.removeSvg2();
      this.printSvg2(fprimaria[0]);
      this.candidatos = fprimaria[0];
      this.candidatos2 = fprimaria[0];
      return fprimaria[0];
    } else if (d.data.id === 'feme.primaria.encurso') {
      this.label3 = 'TOTAL EN CURSO:';
      this.dates3 = fprimaria[1].length;
      this.datasTable = fprimaria[1];
      this.removeSvg();
      this.printSvg(fprimaria[1]);
      this.removeSvg2();
      this.printSvg2(fprimaria[1]);
      this.candidatos = fprimaria[1];
      this.candidatos2 = fprimaria[1];
      return fprimaria[1];
    } else if (d.data.id === 'feme.primaria.trunca') {
      this.datasTable = fprimaria[2];
      this.removeSvg();
      this.printSvg(fprimaria[2]);
      this.removeSvg2();
      this.printSvg2(fprimaria[2]);
      this.candidatos = fprimaria[2];
      this.candidatos2 = fprimaria[2];
      return fprimaria[2];
    } else if (d.data.id === 'feme.primaria.terminada') {
      this.datasTable = fprimaria[3];
      this.removeSvg();
      this.printSvg(fprimaria[3]);
      this.removeSvg2();
      this.printSvg2(fprimaria[3]);
      this.candidatos = fprimaria[3];
      this.candidatos2 = fprimaria[3];
      return fprimaria[3];
    } else if (d.data.id === 'feme.secundaria') {
      this.datasTable = fsecundaria[0];
      this.removeSvg();
      this.printSvg(fsecundaria[0]);
      this.removeSvg2();
      this.printSvg2(fsecundaria[0]);
      this.candidatos = fsecundaria[0];
      this.candidatos2 = fsecundaria[0];
      return fsecundaria[0];
    } else if (d.data.id === 'feme.secundaria.encurso') {
      this.datasTable = fsecundaria[1];
      this.removeSvg();
      this.printSvg(fsecundaria[1]);
      this.removeSvg2();
      this.printSvg2(fsecundaria[1]);
      this.candidatos = fsecundaria[1];
      this.candidatos2 = fsecundaria[1];
      return fsecundaria[1];
    } else if (d.data.id === 'feme.secundaria.trunca') {
      this.datasTable = fsecundaria[2];
      this.removeSvg();
      this.printSvg(fsecundaria[2]);
      this.removeSvg2();
      this.printSvg2(fsecundaria[2]);
      this.candidatos = fsecundaria[2];
      this.candidatos2 = fsecundaria[2];
      return fsecundaria[2];
    } else if (d.data.id === 'feme.secundaria.terminada') {
      this.datasTable = fsecundaria[3];
      this.removeSvg();
      this.printSvg(fsecundaria[3]);
      this.removeSvg2();
      this.printSvg2(fsecundaria[3]);
      this.candidatos = fsecundaria[3];
      this.candidatos2 = fsecundaria[3];
      return fsecundaria[3];
    } else if (d.data.id === 'feme.preparatoria') {
      this.removeSvg();
      this.printSvg(fpreparatoria[0]);
      this.removeSvg2();
      this.printSvg2(fpreparatoria[0]);
      this.datasTable = fpreparatoria[0];
      this.candidatos = fpreparatoria[0];
      this.candidatos2 = fpreparatoria[0];
      return fpreparatoria[0];
    } else if (d.data.id === 'feme.preparatoria.encurso') {
      this.datasTable = fpreparatoria[1];
      this.removeSvg();
      this.printSvg(fpreparatoria[1]);
      this.removeSvg2();
      this.printSvg2(fpreparatoria[1]);
      this.candidatos = fpreparatoria[1];
      this.candidatos2 = fpreparatoria[1];
      return fpreparatoria[1];
    } else if (d.data.id === 'feme.preparatoria.trunca') {
      this.datasTable = fpreparatoria[2];
      this.removeSvg();
      this.printSvg(fpreparatoria[2]);
      this.removeSvg2();
      this.printSvg2(fpreparatoria[2]);
      this.candidatos = fpreparatoria[2];
      this.candidatos2 = fpreparatoria[2];
      return fpreparatoria[2];
    } else if (d.data.id === 'feme.preparatoria.terminada') {
      this.datasTable = fpreparatoria[3];
      this.removeSvg();
      this.printSvg(fpreparatoria[3]);
      this.removeSvg2();
      this.printSvg2(fpreparatoria[3]);
      this.candidatos = fpreparatoria[3];
      this.candidatos2 = fpreparatoria[3];
      return fpreparatoria[3];
    } else if (d.data.id === 'feme.licenciatura') {
      this.datasTable = flicenciatura[0];
      this.removeSvg();
      this.printSvg(flicenciatura[0]);
      this.removeSvg2();
      this.printSvg2(flicenciatura[0]);
      this.candidatos = flicenciatura[0];
      this.candidatos2 = flicenciatura[0];
      return flicenciatura[0];
    } else if (d.data.id === 'feme.licenciatura.encurso') {
      this.datasTable = flicenciatura[1];
      this.removeSvg();
      this.printSvg(flicenciatura[1]);
      this.removeSvg2();
      this.printSvg2(flicenciatura[1]);
      this.candidatos = flicenciatura[1];
      this.candidatos2 = flicenciatura[1];
      return flicenciatura[1];
    } else if (d.data.id === 'feme.licenciatura.trunca') {
      this.datasTable = flicenciatura[2];
      this.removeSvg();
      this.printSvg(flicenciatura[2]);
      this.removeSvg2();
      this.printSvg2(flicenciatura[2]);
      this.candidatos = flicenciatura[2];
      this.candidatos2 = flicenciatura[2];
      return flicenciatura[2];
    } else if (d.data.id === 'feme.licenciatura.terminada') {
      this.datasTable = flicenciatura[3];
      this.removeSvg();
      this.printSvg(flicenciatura[3]);
      this.removeSvg2();
      this.printSvg2(flicenciatura[3]);
      this.candidatos = flicenciatura[3];
      this.candidatos2 = flicenciatura[3];
      return flicenciatura[3];
    } else if (d.data.id === 'feme.maestria') {
      this.datasTable = fmaestria[0];
      this.removeSvg();
      this.printSvg(fmaestria[0]);
      this.removeSvg2();
      this.printSvg2(fmaestria[0]);
      this.candidatos = fmaestria[0];
      this.candidatos2 = fmaestria[0];
      return fmaestria[0];
    } else if (d.data.id === 'feme.maestria.encurso') {
      this.datasTable = fmaestria[1];
      this.removeSvg();
      this.printSvg(fmaestria[1]);
      this.removeSvg2();
      this.printSvg2(fmaestria[1]);
      this.candidatos = fmaestria[1];
      this.candidatos2 = fmaestria[1];
      return fmaestria[1];
    } else if (d.data.id === 'feme.maestria.trunca') {
      this.datasTable = fmaestria[2];
      this.removeSvg();
      this.printSvg(fmaestria[2]);
      this.removeSvg2();
      this.printSvg2(fmaestria[2]);
      this.candidatos = fmaestria[2];
      this.candidatos2 = fmaestria[2];
      return fmaestria[2];
    } else if (d.data.id === 'feme.maestria.terminada') {
      this.datasTable = fmaestria[3];
      this.removeSvg();
      this.printSvg(fmaestria[3]);
      this.removeSvg2();
      this.printSvg2(fmaestria[3]);
      this.candidatos = fmaestria[3];
      this.candidatos2 = fmaestria[3];
      return fmaestria[3];
    } else if (d.data.id === 'feme.doctorado') {
      this.datasTable = fdoctorado[0];
      this.removeSvg();
      this.printSvg(fdoctorado[0]);
      this.removeSvg2();
      this.printSvg2(fdoctorado[0]);
      this.candidatos = fdoctorado[0];
      this.candidatos2 = fdoctorado[0];
      return fdoctorado[0];
    } else if (d.data.id === 'feme.doctorado.encurso') {
      this.datasTable = fdoctorado[1]
      this.removeSvg();
      this.printSvg(fdoctorado[1]);
      this.removeSvg2();
      this.printSvg2(fdoctorado[1]);
      this.candidatos = fdoctorado[1];
      this.candidatos2 = fdoctorado[1];
      return fdoctorado[1];
    } else if (d.data.id === 'feme.doctorado.trunca') {
      this.datasTable = fdoctorado[2];
      this.removeSvg();
      this.printSvg(fdoctorado[2]);
      this.removeSvg2();
      this.printSvg2(fdoctorado[2]);
      this.candidatos = fdoctorado[2];
      this.candidatos2 = fdoctorado[2];
      return fdoctorado[2];
    } else if (d.data.id === 'feme.doctorado.terminada') {
      this.datasTable = fdoctorado[3];
      this.removeSvg();
      this.printSvg(fdoctorado[3]);
      this.removeSvg2();
      this.printSvg2(fdoctorado[3]);
      this.candidatos = fdoctorado[3];
      this.candidatos2 = fdoctorado[3];
      return fdoctorado[3];
    } else if (d.data.id === 'masc') {      /************************* Masculino *************************/
    this.label3 = 'TOTAL DE HOMBRES:';
      this.dates3 = genero[1].length;
      this.datasTable = genero[1];
      this.removeSvg();
      this.printSvg(genero[1]);
      this.removeSvg2();
      this.printSvg2(genero[1]);
      this.candidatos = genero[1];
      this.candidatos2 = genero[1];
      return genero[1];
    } else if (d.data.id === 'masc.primaria') {
      this.datasTable = mprimaria[0];
      this.removeSvg();
      this.printSvg(mprimaria[0]);
      this.removeSvg2();
      this.printSvg2(mprimaria[0]);
      this.candidatos = mprimaria[0];
      this.candidatos2 = mprimaria[0];
      return mprimaria[0];
    } else if (d.data.id === 'masc.primaria.encurso') {
      this.datasTable = mprimaria[1];
      this.removeSvg();
      this.printSvg(mprimaria[1]);
      this.removeSvg2();
      this.printSvg2(mprimaria[1]);
      this.candidatos = mprimaria[1];
      this.candidatos2 = mprimaria[1];
      return mprimaria[1];
    } else if (d.data.id === 'masc.primaria.trunca') {
      this.datasTable = mprimaria[2];
      this.removeSvg();
      this.printSvg(mprimaria[2]);
      this.removeSvg2();
      this.printSvg2(mprimaria[2]);
      this.candidatos = mprimaria[2];
      this.candidatos2 = mprimaria[2];
      return mprimaria[2];
    } else if (d.data.id === 'masc.primaria.terminada') {
      this.datasTable = mprimaria[3];
      this.removeSvg();
      this.printSvg(mprimaria[3]);
      this.removeSvg2();
      this.printSvg2(mprimaria[3]);
      this.candidatos = mprimaria[3];
      this.candidatos2 = mprimaria[3];
      return mprimaria[3];
    } else if (d.data.id === 'masc.secundaria') {
      this.label3 = 'TOTAL EN SECUNDARIA:'
      this.dates3 = msecundaria[0].length;
      this.datasTable = msecundaria[0];
      this.removeSvg();
      this.printSvg(msecundaria[0]);
      this.removeSvg2();
      this.printSvg2(msecundaria[0]);
      this.candidatos = msecundaria[0];
      this.candidatos2 = msecundaria[0];
      return msecundaria[0];
    } else if (d.data.id === 'masc.secundaria.encurso') {
      this.label3 = 'TOTAL EN CURSO:';
      this.dates3 = msecundaria[1].length;
      this.datasTable = msecundaria[1];
      this.removeSvg();
      this.printSvg(msecundaria[1]);
      this.removeSvg2();
      this.printSvg2(msecundaria[1])
      this.candidatos = msecundaria[1];
      this.candidatos2 = msecundaria[1];
      return msecundaria[1];
    } else if (d.data.id === 'masc.secundaria.trunca') {
      this.datasTable = msecundaria[2];
      this.removeSvg();
      this.printSvg(msecundaria[2]);
      this.removeSvg2();
      this.printSvg2(msecundaria[2]);
      this.candidatos = msecundaria[2];
      this.candidatos2 = msecundaria[2];
      return msecundaria[2];
    } else if (d.data.id === 'masc.secundaria.terminada') {
      this.datasTable = msecundaria[3];
      this.removeSvg();
      this.printSvg(msecundaria[3]);
      this.removeSvg2();
      this.printSvg2(msecundaria[3]);
      this.candidatos = msecundaria[3];
      this.candidatos2 = msecundaria[3];
      return msecundaria[3];
    } else if (d.data.id === 'masc.preparatoria') {
      this.datasTable = mpreparatoria[0];
      this.removeSvg();
      this.printSvg(mpreparatoria[0]);
      this.removeSvg2();
      this.printSvg2(mpreparatoria[0]);
      this.candidatos = mpreparatoria[0];
      this.candidatos2 = mpreparatoria[0];
      return mpreparatoria[0];
    } else if (d.data.id === 'masc.preparatoria.encurso') {
      this.datasTable = mpreparatoria[1];
      this.removeSvg();
      this.printSvg(mpreparatoria[1]);
      this.removeSvg2();
      this.printSvg2(mpreparatoria[1]);
      this.candidatos = mpreparatoria[1];
      this.candidatos2 = mpreparatoria[1];
      return mpreparatoria[1];
    } else if (d.data.id === 'masc.preparatoria.trunca') {
      this.datasTable = mpreparatoria[2];
      this.removeSvg();
      this.printSvg(mpreparatoria[2]);
      this.removeSvg2();
      this.printSvg2(mpreparatoria[2]);
      this.candidatos = mpreparatoria[2];
      this.candidatos2 = mpreparatoria[2];
      return mpreparatoria[2];
    } else if (d.data.id === 'masc.preparatoria.terminada') {
      this.datasTable = mpreparatoria[3];
      this.removeSvg();
      this.printSvg(mpreparatoria[3]);
      this.removeSvg2();
      this.printSvg2(mpreparatoria[3]);
      this.candidatos = mpreparatoria[3];
      this.candidatos2 = mpreparatoria[3];
      return mpreparatoria[3];
    } else if (d.data.id === 'masc.licenciatura') {
      this.datasTable = mlicenciatura[0];
      this.removeSvg();
      this.printSvg(mlicenciatura[0]);
      this.removeSvg2();
      this.printSvg2(mlicenciatura[0]);
      this.candidatos = mlicenciatura[0];
      this.candidatos2 = mlicenciatura[0];
      return mlicenciatura[0];
    } else if (d.data.id === 'masc.licenciatura.encurso') {
      this.datasTable = mlicenciatura[1];
      this.removeSvg();
      this.printSvg(mlicenciatura[1]);
      this.removeSvg2();
      this.printSvg2(mlicenciatura[1]);
      this.candidatos = mlicenciatura[1];
      this.candidatos2 = mlicenciatura[1];
      return mlicenciatura[1];
    } else if (d.data.id === 'masc.licenciatura.trunca') {
      this.datasTable = mlicenciatura[2];
      this.removeSvg();
      this.printSvg(mlicenciatura[2]);
      this.removeSvg2();
      this.printSvg2(mlicenciatura[2]);
      this.candidatos = mlicenciatura[2];
      this.candidatos2 = mlicenciatura[2];
      return mlicenciatura[2];
    } else if (d.data.id === 'masc.licenciatura.terminada') {
      this.datasTable = mlicenciatura[3];
      this.removeSvg();
      this.printSvg(mlicenciatura[3]);
      this.removeSvg2();
      this.printSvg2(mlicenciatura[3]);
      this.candidatos = mlicenciatura[3];
      this.candidatos2 = mlicenciatura[3];
      return mlicenciatura[3];
    } else if (d.data.id === 'masc.maestria') {
      this.datasTable = mmaestria[0];
      this.removeSvg();
      this.printSvg(mmaestria[0]);
      this.removeSvg2();
      this.printSvg2(mmaestria[0]);
      this.candidatos = mmaestria[0];
      this.candidatos2 = mmaestria[0];
      return mmaestria[0];
    } else if (d.data.id === 'masc.maestria.encurso') {
      this.datasTable = mmaestria[1];
      this.removeSvg();
      this.printSvg(mmaestria[1]);
      this.removeSvg2();
      this.printSvg2(mmaestria[1]);
      this.candidatos = mmaestria[1];
      this.candidatos2 = mmaestria[1];
      return mmaestria[1];
    } else if (d.data.id === 'masc.maestria.trunca') {
      this.datasTable = mmaestria[2];
      this.removeSvg();
      this.printSvg(mmaestria[2]);
      this.removeSvg2();
      this.printSvg2(mmaestria[2]);
      this.candidatos = mmaestria[2];
      this.candidatos2 = mmaestria[2];
      return mmaestria[2];
    } else if (d.data.id === 'masc.maestria.terminada') {
      this.datasTable = mmaestria[3];
      this.removeSvg();
      this.printSvg(mmaestria[3]);
      this.removeSvg2();
      this.printSvg2(mmaestria[3]);
      this.candidatos = mmaestria[3];
      this.candidatos2 = mmaestria[3];
      return mmaestria[3];
    } else if (d.data.id === 'masc.doctorado') {
      this.datasTable = mdoctorado[0];
      this.removeSvg();
      this.printSvg(mdoctorado[0]);
      this.removeSvg2();
      this.printSvg2(mdoctorado[0]);
      this.candidatos = mdoctorado[0];
      this.candidatos2 = mdoctorado[0];
      return mdoctorado[0];
    } else if (d.data.id === 'masc.doctorado.encurso') {
      this.datasTable = mdoctorado[1];
      this.removeSvg();
      this.printSvg(mdoctorado[1]);
      this.removeSvg2();
      this.printSvg2(mdoctorado[1]);
      this.candidatos = mdoctorado[1];
      this.candidatos2 = mdoctorado[1];
      return mdoctorado[1];
    } else if (d.data.id === 'masc.doctorado.trunca') {
      this.datasTable = mdoctorado[2];
      this.removeSvg();
      this.printSvg(mdoctorado[2]);
      this.removeSvg2();
      this.printSvg2(mdoctorado[2]);
      this.candidatos = mdoctorado[2];
      this.candidatos2 = mdoctorado[2];
      return mdoctorado[2];
    } else if (d.data.id === 'masc.doctorado.terminada') {
      this.datasTable = mdoctorado[3];
      this.removeSvg();
      this.printSvg(mdoctorado[3]);
      this.removeSvg2();
      this.printSvg2(mdoctorado[3]);
      this.candidatos = mdoctorado[3];
      this.candidatos2 = mdoctorado[3];
      return mdoctorado[3];
    }
  }

  private middleArcLine(d) {
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw

    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path.path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);

    return path.toString();
  }

  private textFits(d) {
    const CHAR_SPACE = 8;
    const deltaAngle = this.x(d.x1) - this.x(d.x0);
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const perimeter = r * deltaAngle;

    return d.data.name.length * CHAR_SPACE < perimeter;
  }

  private removeSvg() {
    this.svg.remove();
  }
  private removeSvg2() {
    this.svg2.remove();
  }
  private removeSvg3() {
    this.svg3.remove();
  }

  private printSvg(data) {
    this.drawPie(data);
  }
  private printSvg2(data) {
    this.drawPie2(data);
  }
  private printSvg3(data) {
    this.drawPie3(data);
  }

  private setReport () {
    // document.getElementById("recharge"). = true ;
    setTimeout('document.getElementById("recharge").disabled = false', 1250);
    this.svg.remove();
    this.svg2.remove();
    this.svg3.remove();
    this.initSvg();
    this.initSvg2();
    this.initSvg3();
    this.getEmpleados(null);
    this.rangeDates = [
      new Date(2018, 11, 1, 0, 0),
      new Date(),
    ];
  }

}
