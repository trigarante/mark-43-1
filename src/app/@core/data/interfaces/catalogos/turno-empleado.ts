import {Observable} from 'rxjs';

export interface TurnoEmpleado {
  id: number;
  turno: string;
  horario: string;
  activo: number;
}

export abstract class TurnoEmpleadoData {
  abstract get(): Observable<TurnoEmpleado[]>;

  abstract getTurnoEmpleadoById(id): Observable<TurnoEmpleado>;

  abstract post(turno): Observable<TurnoEmpleado>;

  abstract put(id, turno): Observable<TurnoEmpleado>;
}

