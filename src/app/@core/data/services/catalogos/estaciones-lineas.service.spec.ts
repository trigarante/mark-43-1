import {Global} from '../global';
import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {EstacionesLineasService} from './estaciones-lineas.service';

describe('EstacionesLineas service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: EstacionesLineasService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EstacionesLineasService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EstacionesLineasService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  describe('funsiona servicio correctamente', () => {
    describe('getEstacionByIdLinea', () => {
      it('status 200', (done) => {
        const url = validarFuncion();
        const go = validarFuncion(service.getEstacionByIdLinea.toString());
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        chai.use(chaiHttp);
        chai.request(url)
          .get(go + 7)
          .end((err, res) => {
            console.log('EstacionesLineasService getEstacionByIdLinea 200 status --> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getEstacionByIdLinea debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getEstacionByIdLinea);
              }
            done();
          });
        done();
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
})
