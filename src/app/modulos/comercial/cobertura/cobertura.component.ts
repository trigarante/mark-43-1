import { Component, OnInit, ViewChild } from '@angular/core';
import {
  Cobertura,
  CoberturaData,
} from '../../../@core/data/interfaces/comerciales/cobertura';
import { map } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DatosCoberturaComponent } from '../modals/datos-cobertura/datos-cobertura.component';
import { NbDialogService, NbToastrService } from '@nebular/theme';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-cobertura',
  templateUrl: './cobertura.component.html',
  styleUrls: ['./cobertura.component.scss'],
})
export class CoberturaComponent implements OnInit {
  cols: any[];
  cobertura: Cobertura[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(
    private coberturasService: CoberturaData,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.getCoberturas(0);
    this.cols = [
      'vista',
      'idProducto',
      'idTipoCobertura',
      'detalle',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }


  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getCoberturas(mensajeAct) {
    this.coberturasService
      .get()
      .pipe(
        map(result => {
          return result.filter(data => data.activo === 1);
        })
      )
      .subscribe(data => {
        this.cobertura = [];
        this.cobertura = data;
        this.dataSource = new MatTableDataSource(this.cobertura); //
        this.dataSource.sort = this.sort; //
        this.dataSource.paginator = this.paginator; //
      });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Cobertura`);
  }

  verDatos(idCobertura, idProducto) {
    this.dialogService
      .open(DatosCoberturaComponent, {
        context: {
          idCobertura: idCobertura,
          idProducto: idProducto
        }
      })
      .onClose.subscribe(x => {
        this.getCoberturas(1);
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'cobertura');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelCobertura', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCoberturas(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.cobertura.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.cobertura.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  updateCobertura() {}

  bajaCobertura() {}
}
