import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('CPU Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 43,
    nombre: null,
    idEmpleado: null,
    observaciones: 'nuevo',
    estadoInventario: 'medio uso',
    idEstadoInventario: 2,
    idObservacionesInventario: 1,
    numeroSerie: '0001C',
    idProcesador: 3,
    hdd: '500',
    ram: '4',
    idMarca: 4,
    modelo: '0001C',
    fechaRecepcion: '2019-09-24T16:07:49.000+0000',
    fechaSalida: null,
    comentarios: 'EXCELENTE',
    activo: 1,
    usuario: null,
    nombreProcesador: 'AMD',
    nombreMarca: 'LENOVO',
    origenRecurso: 'renta',
    idSede: 6,
    sedeNombre: 'NEW SEDE TEST',
  },
    {
      id: 'cpu',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/cpu');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/cpu', '/43');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/cpu');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/cpu', '/43/74');
      });
    });
  }
});
