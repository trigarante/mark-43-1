import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { TipoPagina, TipoPaginaData } from '../../interfaces/catalogos/tipo-pagina';


@Injectable()
export class TipoPaginaService extends TipoPaginaData {

  private baseURL;
  private socketURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<TipoPagina[]> {
    return this.http.get<TipoPagina[]>(this.baseURL + 'v1/tipo-pagina');
  }
  post(tipoPagina: TipoPagina): Observable<TipoPagina> {
    return this.http.post<TipoPagina>(this.baseURL + 'v1/tipo-pagina', tipoPagina);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'tipopaginas/saveTipoPaginas', json);
  }
  getTipoPaginaById(idTipoPagina): Observable<TipoPagina> {
    return this.http.get<TipoPagina>(this.baseURL + 'v1/tipo-pagina/' + idTipoPagina);
  }

  put(idTipoPagina, tipoPagina: TipoPagina) {
    return this.http.put<TipoPagina>(this.baseURL + 'v1/tipo-pagina/' + idTipoPagina  + '/'
      + sessionStorage.getItem('Empleado'), tipoPagina);
  }

}
