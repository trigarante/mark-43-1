import {Component, OnInit, ViewChild} from '@angular/core';
import {Contratos, ContratosData} from '../../../@core/data/interfaces/comerciales/contratos';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {AsignarContratosComponent} from '../modals/asignar-contratos/asignar-contratos.component';
import * as moment from 'moment';
import swal from 'sweetalert';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.scss'],
})
export class ContratosComponent implements OnInit {
  cols: any[];
  contratos: Contratos[];
  dataSource: any;
  /**Variable que contendra los dias restantes para el convenio*/
  diasRestantes;
  /**Variable que contendra el arreglo del contrato ByID*/
  contrato: Contratos;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private contratosService: ContratosData,
              private dialogoService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getContratos(0);
    this.cols = [
      'socio',
      'fechaInicio',
      'fechaFin',
      'diasRestantes',
      'referencia',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getContratos(mensajeAct) {
    this.contratosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.contratos = [];
      this.contratos = data;
      this.diasRest(this.contratos);
      this.dataSource = new MatTableDataSource(this.contratos); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Contratos`);
  }

  diasRest(contratos) {
    this.diasRestantes = [];
    for (let i = 0; i < contratos.length; i++) {
      this.diasRestantes = moment(this.contratos[i].fechaFin).diff(this.contratos[i].fechaInicio, 'days');
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updaContratos(idSocio) {
    this.dialogoService.open(AsignarContratosComponent, {
      context: {
        idTipo: 2,
        idSocio: idSocio,
      },
    }).onClose.subscribe(x => this.getContratos(1));
  }

  bajaContratos(idContrato) {
    this.contratosService.getContratoById(idContrato).subscribe(data => {
      this.contrato = data;
    });
    swal({
      title: '¿Deseas dar de baja este Contrato?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.contrato.activo = 0;
        this.contratosService.put(idContrato, this.contrato).subscribe( result => {
          this.contratosService.postSocket({contrato: this.contrato}).subscribe(x => {});
        });
        swal('¡El Contrato se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getContratos(0);
        }));
      }
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'contratos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelContratos', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getContratos(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.contrato.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.contrato.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
