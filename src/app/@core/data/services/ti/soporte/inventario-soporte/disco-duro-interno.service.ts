import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {
  DiscoDuroInterno,
  DiscoDuroInternoData,
} from '../../../../interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {Observable} from 'rxjs';

@Injectable()
export class DiscoDuroInternoService extends DiscoDuroInternoData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }
  get(): Observable<DiscoDuroInterno[]> {
    return this.http.get<DiscoDuroInterno[]>(this.baseURL + 'v1/discodInterno');
  }

  post(discoInterno: DiscoDuroInterno): Observable<DiscoDuroInterno> {
    return this.http.post<DiscoDuroInterno>(this.baseURL + 'v1/discodInterno', discoInterno);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'discodurointerno/saveDiscoDuroInterno', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajadiscodurointerno/saveBajaDiscoDuroInterno', json);
  }
  getDiscoInternoById(idDiscoInterno): Observable<DiscoDuroInterno> {
    return this.http.get<DiscoDuroInterno>(this.baseURL + 'v1/discodInterno/' + idDiscoInterno);
  }

  put(idDiscoInterno, discoInterno: DiscoDuroInterno): Observable<DiscoDuroInterno> {
    return this.http.put<DiscoDuroInterno>(this.baseURL + 'v1/discodInterno/' + idDiscoInterno + '/'
      + sessionStorage.getItem('Empleado'), discoInterno);
  }
}
