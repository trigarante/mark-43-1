import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Creado por equipo <b>Angular</b> 2019</span>
  `,
})
export class FooterComponent {
}
