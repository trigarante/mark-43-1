import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Presupuesto, PresupuestoData} from '../../../../@core/data/interfaces/comerciales/presupuesto';
import {NbDialogRef} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import {Router} from '@angular/router';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
 import * as _moment from 'moment';
 import { default as _rollupMoment, Moment} from 'moment';
import {MatDatepicker} from '@angular/material';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MMMM/YYYY',
  },
  display: {
    dateInput: 'MMMM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'ngx-asignar-presupuesto',
  templateUrl: './asignar-presupuesto.component.html',
  styleUrls: ['./asignar-presupuesto.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],

})
export class AsignarPresupuestoComponent implements OnInit {
  @ Input() idPresupuesto: number;
  @ Input() idSocio: number;
  @ Input() idTipo: number;
  /** Expresión regular para sólo números. */
  numeros: RegExp = /[0-9.,]+/;
  submitted: Boolean;
  asignarPresupuestoForm: FormGroup;
  presupuesto: Presupuesto;
  date;
  dataSource: any;
  // socket
  disabled = true;
  datos: any[] = [];
  newDatos: any;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<SociosComponent>,
              private router: Router, protected presupuestoService: PresupuestoData) {
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getPresupuestoById();
    }
    this.asignarPresupuestoForm = this.fb.group({
      'idSocio': this.idSocio,
      'presupuesto': new FormControl('', Validators.required),
      'activo': 1,
      'anio': new FormControl('', Validators.required),
    });
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  guardarPresupuesto() {
    this.submitted = true;
    if (this.asignarPresupuestoForm.invalid) {
      return;
    }
    this.presupuestoService.post(this.asignarPresupuestoForm.value)
    .subscribe((result) => {
      this.presupuestoService.postSocket(this.asignarPresupuestoForm.value).subscribe(x => {});
      this.asignarPresupuestoForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/comercial/objetivo-de-ventas']);
    });
  }

  dismiss() {
    this.ref.close();
  }

  updatePresupuesto() {
    this.presupuestoService.put(this.idPresupuesto, this.asignarPresupuestoForm.value)
      .subscribe(result => {
        this.presupuestoService.postSocket(this.asignarPresupuestoForm.value).subscribe(x => {});
      this.asignarPresupuestoForm.reset();
      this.ref.close();
    });
  }

  getPresupuestoById() {
    this.presupuestoService.getPresupuestoById(this.idPresupuesto).subscribe(data => {
      this.presupuesto = data;
      this.asignarPresupuestoForm.controls['presupuesto'].setValue(this.presupuesto.presupuesto);
      this.asignarPresupuestoForm.controls['anio'].setValue(this.presupuesto.anio);
    });
  }

  formatoNumerosConComas(objetivo) {
    /**La asignacion de comas llega hasta 100,000,000,000 para mayor cantidad, agregar maas validaciones o hacerlo dinamico*/
    let formatNumber: any = '';
    objetivo = objetivo.replace(/,/g, '');
    if (objetivo.length > 3 && objetivo.length < 7) {
      formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
      this.asignarPresupuestoForm.controls['presupuesto'].setValue(formatNumber);
    } else if (objetivo.length > 6 && objetivo.length < 10) {
      formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
      this.asignarPresupuestoForm.controls['presupuesto'].setValue(formatNumber);
    } else if (objetivo.length > 9 && objetivo.length < 13) {
      formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
        ',' + objetivo.slice(-3);
      this.asignarPresupuestoForm.controls['presupuesto'].setValue(formatNumber);
    }else {
      this.asignarPresupuestoForm.controls['presupuesto'].setValue(objetivo);
    }
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

}
