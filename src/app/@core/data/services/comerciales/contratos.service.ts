import {Injectable} from '@angular/core';
import {Contratos, ContratosData} from '../../interfaces/comerciales/contratos';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ContratosService extends ContratosData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Contratos[]> {
    return this.http.get<Contratos[]>(this.baseURL + 'v1/contratos-socio');
  }

  post(contratos: Contratos, fechaInicio, fechaFin): Observable<Contratos> {
    return this.http.post<Contratos>(this.baseURL + 'v1/contratos-socio' + '/' + fechaInicio + '/' +
      fechaFin, contratos);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'contratos/saveContrato/1', json);
  }

  getContratoById(idContrato): Observable<Contratos> {
    return this.http.get<Contratos>(this.baseURL + 'v1/contratos-socio/' + idContrato);
  }

  put(idContrato, contratos: Contratos): Observable<Contratos> {
    return this.http.put<Contratos>(this.baseURL + 'v1/contratos-socio/' + idContrato + '/'
      + sessionStorage.getItem('Empleado'), contratos);
  }
}
