import {Observable} from 'rxjs';

export interface Inspecciones {
  id: number;
  idRegistro: number;
  idEstadoInspeccion: number;
  cp: string;
  idColonia: number;
  fechaInspeccion: Date;
  kilometraje: number;
  placas: number;
  urlPreventa: number;
  urlPostventa: string;
}

export abstract class InspeccionesData {

  abstract get(): Observable<Inspecciones[]>;

  abstract post(Inspeccion): Observable<Inspecciones>;

  abstract getInspeccionById(idInspeccion): Observable<Inspecciones>;

  abstract put(idInspeccion, Inspeccion): Observable<Inspecciones>;

}
