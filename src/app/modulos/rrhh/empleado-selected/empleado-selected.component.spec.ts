import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmpleadoSelectedComponent} from './empleado-selected.component';

describe('EmpleadoSelectedComponent', () => {
  let component: EmpleadoSelectedComponent;
  let fixture: ComponentFixture<EmpleadoSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmpleadoSelectedComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
