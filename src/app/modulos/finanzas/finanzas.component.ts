import { Component} from '@angular/core';

@Component({
  selector: 'ngx-finanzas',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class FinanzasComponent {

}
