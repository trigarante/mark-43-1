import {Component, OnInit, ViewChild} from '@angular/core';
import {Pagina, PaginasData} from '../../../@core/data/interfaces/marketing/pagina';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NbDialogService} from '@nebular/theme';
import {ActualizarPaginaComponent} from '../modals/actualizar-pagina/actualizar-pagina.component';
import {VerConfiguracionComponent} from '../modals/ver-configuracion/ver-configuracion.component';
import {environment} from '../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.scss'],
})
export class PaginaComponent implements OnInit {
  cols: any[];
  pagina: Pagina[];
  dataSource: any;
  // sockets
  private stompClient = null;
  constructor(private paginaService: PaginasData, public dialogService: NbDialogService) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'paginas');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelPaginas', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getPagina();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
   /** metodo que muestra las paginas en una tabla*/
  getPagina() {
    this.paginaService.get().subscribe(data => {
      this.pagina = data;
      this.dataSource = new MatTableDataSource(this.pagina);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  /** modal que actualiza los datos de la pagina*/
  actualizarPagina(idPagina) {
    this.dialogService.open(ActualizarPaginaComponent, {
      context: {
        id: idPagina,

      },
    }).onClose.subscribe(x => {
      this.getPagina();
    });
  }
  /** modal que permite ver la configuracion json*/
  verconfiguracion(idPagina) {
    this.dialogService.open(VerConfiguracionComponent, {
      context: {
        id: idPagina,
        tipo: 1,

      },
    });

  }
  /** modal que  agrega el json en configuracion*/
  agregarConfiguracion(idPagina) {
    this.dialogService.open(VerConfiguracionComponent, {
      context: {
        id: idPagina,
        tipo: 2,

      },
    });

  }

  ngOnInit() {
    this.connect();
    this.cols = ['id', 'idCampana', 'url', 'descripcion' , 'fechaRegistro', 'fechaActualizacion', 'acciones'];
    this.getPagina();
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
