import { Injectable } from '@angular/core';
import {AppService} from '../drive/app.service';

const CLIENT_ID = '350349390391-gbs14s8sl28fes78hog8oqgmoldg8imc.apps.googleusercontent.com';
const API_KEY = 'AIzaSyAOoWCRaTpyoUfxHmb5UxOrqj7DqK6Qep8';
const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'];
const SCOPES = 'https://www.googleapis.com/auth/drive';

@Injectable()
export class GapiSession {
    googleAuth: gapi.auth2.GoogleAuth;

    constructor(private appService: AppService) {}

    initClient() {
        return new Promise((resolve, reject) => {
            gapi.load('client:auth2', () => {
                return gapi.client.init({
                    apiKey: API_KEY,
                    clientId: CLIENT_ID,
                    discoveryDocs: DISCOVERY_DOCS,
                    scope: SCOPES,
                }).then(() => {
                    this.googleAuth = gapi.auth2.getAuthInstance();
                    resolve();
                });
            });
        });
    }

    get isSignedIn(): boolean {
        return this.googleAuth.isSignedIn.get();
    }

    signIn() {
        this.googleAuth = gapi.auth2.getAuthInstance();
        return this.googleAuth.signIn({
            prompt: 'consent',
        }).then((googleUser: gapi.auth2.GoogleUser) => {
            this.appService.User.add(googleUser.getBasicProfile());
        });
    }

    signOut(): void {
        this.googleAuth.signOut();
    }
}
