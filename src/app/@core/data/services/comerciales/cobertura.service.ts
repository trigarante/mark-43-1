import {Injectable} from '@angular/core';
import {Cobertura, CoberturaData} from '../../interfaces/comerciales/cobertura';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CoberturaService extends CoberturaData {
  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Cobertura[]> {
    return this.http.get<Cobertura[]>(this.baseURL + 'v1/coberturas');
  }

  post(cobertura: Cobertura): Observable<Cobertura> {
    return this.http.post<Cobertura>(this.baseURL + 'v1/coberturas', cobertura);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURL + 'cobertura/saveCobertura/1', json);
  }

  getCoberturaById(idCobertura): Observable<Cobertura> {
    return this.http.get<Cobertura>(this.baseURL + 'v1/coberturas/' + idCobertura);
  }

  put(idCobertura, cobertura: Cobertura): Observable<Cobertura> {
    return this.http.put<Cobertura>(this.baseURL + 'v1/coberturas/' + idCobertura + '/'
      + sessionStorage.getItem('Empleado'), cobertura);
  }
}
