import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {Conexion, CustomMatchers} from '../matchers';
import {environment} from '../../../../../environments/environment';
import {MediosDifusionService} from './medios-difusion.service';
fdescribe('Medio difusion service', () => {
  let injector: TestBed;
  let service: MediosDifusionService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idProveedor: 3,
    idExterno: null,
    descripcion: 'SWDSD01',
    regla: 'SDSDSDW',
    presupuesto: '1,111',
    idCampana: 4,
    activo: 0,
  },
    {
      id: 'mediodifusion',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [MediosDifusionService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(MediosDifusionService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/medio-difusion');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/medio-difusion', '/1');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/medio-difusion');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/medio-difusion', '/1/74');
      });
    });
  }
})
