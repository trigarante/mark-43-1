import {Component, OnInit, ViewChild} from '@angular/core';
import {EjecutivoCuentaData} from '../../../@core/data/interfaces/comerciales/ejecutivo-cuenta';
import {EjecutivoCuenta} from '../../../@core/data/interfaces/comerciales/ejecutivo-cuenta';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {DatosEjecutivoCuentaComponent} from '../modals/datos-ejecutivo-cuenta/datos-ejecutivo-cuenta.component';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-ejecutivo-cuenta',
  templateUrl: './ejecutivo-cuenta.component.html',
  styleUrls: ['./ejecutivo-cuenta.component.scss'],
})
export class EjecutivoCuentaComponent implements OnInit {
  cols: any[];
  ejectutivoCuenta: EjecutivoCuenta[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private ejecutivoCuentaService: EjecutivoCuentaData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }

  @ ViewChild(MatPaginator) paginator: MatPaginator; //
  @ ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getEjecutivoCuenta(0);
    this.cols = [
      'detalles',
      'alias',
      'nombre',
      'favorito',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getEjecutivoCuenta(mensajeAct) {
    this.ejecutivoCuentaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.ejectutivoCuenta = [];
      this.ejectutivoCuenta = data;
      this.dataSource = new MatTableDataSource(this.ejectutivoCuenta); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Contacto`);
  }

  verDatos(idEjecutivoCuenta, idSocio) {
    this.dialogService.open(DatosEjecutivoCuentaComponent, {
      context: {
        idSocio: idSocio,
        idEjecutivoCuenta: idEjecutivoCuenta,
      },
    }).onClose.subscribe(x => {
      this.getEjecutivoCuenta(1);
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'contactos');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelContactos', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEjecutivoCuenta(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
    // this.router.navigate(['/modulos/comercial/socios']);
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.contacto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.contacto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
