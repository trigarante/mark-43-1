import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {Divisas, DivisasData} from '../../../../@core/data/interfaces/comerciales/catalogo/divisas';
import {DivisaCreateComponent} from '../modals/divisa-create/divisa-create.component';
import swal from 'sweetalert';
import {environment} from '../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-divisas',
  templateUrl: './divisas.component.html',
  styleUrls: ['./divisas.component.scss'],
})
export class DivisasComponent implements OnInit {
  cols: any[];
  divisa: Divisas[];
  divisas: Divisas;
  dataSource: any;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  constructor(private divisasService: DivisasData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'divisas');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelDivisas', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getDivisa(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getDivisa(mensajeAct) {
    this.divisasService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.divisa = data;
      this.dataSource = new MatTableDataSource(this.divisa); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }
  getDivisaById(idDivisa) {
    this.divisasService.getDivisaById(idDivisa).subscribe(data => {
      this.divisas = data;
    });
  }
  crearDivisa() {
    this.dialogService.open(DivisaCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe( x => this.getDivisa(0));

  }
  updateDivisa(idDivisa) {
    this.dialogService.open(DivisaCreateComponent, {
      context: {
        idTipo: 2,
        idDivisa: idDivisa,
      },
    }).onClose.subscribe( x => this.getDivisa(0));
  }
  bajaDivisa(idDivisa) {
    this.getDivisaById(idDivisa);
    swal({
      title: '¿Deseas dar de baja esta Divisa?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.divisas.activo = 0;
        this.divisasService.put(idDivisa, this.divisas).subscribe( result => {
          this.divisasService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡La divisa se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getDivisa(0);
        }));
      }
    });
  }
  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Divisas`);
  }
  ngOnInit() {
    this.connect();
    this.getDivisa(0);
    this.cols = ['divisa', 'descripcion', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.divisas.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.divisas.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
