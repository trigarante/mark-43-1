import { TestBed } from '@angular/core/testing';

import { TipoPolizaVnService } from './tipo-poliza-vn.service';

describe('TipoPolizaVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoPolizaVnService = TestBed.get(TipoPolizaVnService);
    expect(service).toBeTruthy();
  });
});
