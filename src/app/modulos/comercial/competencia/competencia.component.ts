import {Component, OnInit, ViewChild} from '@angular/core';
import {CompetenciaSocioData, CompetenciasSocios} from '../../../@core/data/interfaces/comerciales/competencias-socios';
import {AsignarAnalisisCompetenciaComponent} from '../modals/asignar-analisis-competencia/asignar-analisis-competencia.component';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {DatosCompetenciaComponent} from '../modals/datos-competencia/datos-competencia.component';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-competencia',
  templateUrl: './competencia.component.html',
  styleUrls: ['./competencia.component.scss'],
})
export class CompetenciaComponent implements OnInit {
  cols: any[];
  competenciaSocios: CompetenciasSocios[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(protected competenciaService: CompetenciaSocioData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {}

  ngOnInit() {
    this.getCompetencia(0);
    this.cols = [
      'detalle',
      'idProducto',
      'idTipoCompetencia',
      'competencia',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  getCompetencia(mensajeAct) {
    this.competenciaService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.competenciaSocios = [];
      this.competenciaSocios = data;
      this.dataSource = new MatTableDataSource(this.competenciaSocios); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Competencia`);
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  bajaCompretencia() {
  }

  verDatos(idCompetencia) {
    this.dialogService.open(DatosCompetenciaComponent, {
      context: {
        idCompetencia: idCompetencia,
      },
    }).onClose.subscribe(x => {
      this.getCompetencia(1);
    });
  }

  updateCompetencia() {
  }

  asignarAnalisisCompetencia(idCompetencia) {
    this.dialogService.open(AsignarAnalisisCompetenciaComponent, {
      context: {
        idCompetencia: idCompetencia,
      },
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'competencia');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelCompetencia', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getCompetencia(0);
        }, 500);
      });
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.competenciaProducto.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.competenciaProducto.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }

}
