import {Observable} from 'rxjs';
export interface Monitor {
  id: number;
  idEmpleado: number;
  numSerie: string;
  idMarca: number;
  dimensiones: string;
  idEstadoInventario: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  comentarios: string;
  activo: number;
  nombreMarca?: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;
}
export abstract class MonitorDate {
  abstract get(): Observable<Monitor[]>;
  abstract post(monitor): Observable<Monitor>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getMonitorById(idMonitor): Observable<Monitor>;
  abstract put(idMonitor, monitor): Observable<Monitor>;
}
