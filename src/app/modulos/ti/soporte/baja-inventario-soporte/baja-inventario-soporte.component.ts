import {Component, OnInit, ViewChild} from '@angular/core';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import swal2 from 'sweetalert2';
import {NbDialogService} from '@nebular/theme';
import {
  DiscoDuroExterno,
  DiscoDuroExternoData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {
  DiscoDuroInterno,
  DiscoDuroInternoData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBit, ChromeBitData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {Aps, ApsData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {
  Biometricos,
  BiometricosData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {Dvr, DvrData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {Camaras, CamarasData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {
  BarrasMulticontacto,
  BarrasMulticontactoData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {
  FuenteCamaras,
  FuenteCamarasData,
} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-baja-inventario-soporte',
  templateUrl: './baja-inventario-soporte.component.html',
  styleUrls: ['./baja-inventario-soporte.component.scss'],
})
export class BajaInventarioSoporteComponent implements OnInit {
  /** Variable que inicializa las columnas*/
  cols: any[];
  dataSource: any;
  /** Variable para el arreglo discoduroExterno*/
  ddexterno: DiscoDuroExterno[];
  /** Variable para el arreglo discoduroInterno*/
  ddinterno: DiscoDuroInterno[];
  chromeBit: ChromeBit[];
  aps: Aps[];
  biometricos: Biometricos[];
  dvr: Dvr[];
  camara: Camaras[];
  barraMulti: BarrasMulticontacto[];
  corrienteCam: FuenteCamaras[];
  /** Variable para desactivar el arreglo discoduroExterno*/
  ddexternodesactivar: DiscoDuroExterno;
  ddinternodesactivar: DiscoDuroInterno;
  chromeBitdesactivar: ChromeBit;
  apsdesactivar: Aps;
  biometricosdesactivar: Biometricos;
  dvrdesactivar: Dvr;
  camaradesactivar: Camaras;
  barraMultidesactivar: BarrasMulticontacto;
  corrienteCamdesactivar: FuenteCamaras;
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;
  tipoEquipos: string = 'disco duro externo';
  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  opcionequipo: string;
// sockets
  actualizar: string;
  private stompClient1 = null;
  private stompClient2 = null;
  private stompClient3 = null;
  private stompClient4 = null;
  private stompClient5 = null;
  private stompClient6 = null;
  private stompClient7 = null;
  private stompClient8 = null;
  private stompClient9 = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(public ddexternoService: DiscoDuroExternoData, private rutaActiva: ActivatedRoute,
              public ddinternoService: DiscoDuroInternoData, private chromeBitServices: ChromeBitData,
              private apsService: ApsData, private biometricosService: BiometricosData, private dvrService: DvrData,
              private camaraService: CamarasData, private barraMultiService: BarrasMulticontactoData,
              private fuenteCorrienteService: FuenteCamarasData,  public dialogService: NbDialogService,
              private fb: FormBuilder, private estadoInventarioServive: EstadoInventarioData,
              private router: Router) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const _this = this;
    /**----------------------discoduroexterno-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bajadiscoduroexterno');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, (frame) => {
      _this.stompClient1.subscribe('/task/panelBajaDiscoDuroExterno', (content) => {
        if (this.actualizar === 'DISCO DURO EXTERNO' || this.tipoEquipos === 'disco duro externo') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('DISCO DURO EXTERNO');
            _this.getEquipo();
          }, 500);
        }
      });
    });
    /**----------------------disco duro interno-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'bajadiscodurointerno');
    this.stompClient2 = Stomp.over(socket1);
    this.stompClient2.connect({}, (frame) => {
      _this.stompClient2.subscribe('/task/panelBajaDiscoDuroInterno', (content) => {
        if (this.actualizar === 'DISCO DURO INTERNO') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('DISCO DURO INTERNO');
          }, 500);
        }
      });
    });
    /**----------------------chrome bit-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'bajachromebit');
    this.stompClient3 = Stomp.over(socket2);
    this.stompClient3.connect({}, (frame) => {
      _this.stompClient3.subscribe('/task/panelBajaChromeBit', (content) => {
        if (this.actualizar === 'CHROME BIT') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('CHROME BIT');
          }, 500);
        }
      });
    });
    /**----------------------aps-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'bajaaps');
    this.stompClient4 = Stomp.over(socket3);
    this.stompClient4.connect({}, (frame) => {
      _this.stompClient4.subscribe('/task/panelBajaAps', (content) => {
        if (this.actualizar === 'APS') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('APS');
          }, 500);
        }
      });
    });
    /**----------------------biometricos-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'bajabiometricos');
    this.stompClient5 = Stomp.over(socket4);
    this.stompClient5.connect({}, (frame) => {
      _this.stompClient5.subscribe('/task/panelBajaBiometricos', (content) => {
        if (this.actualizar === 'BIOMETRICOS' || this.actualizar === 'biometricos') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('BIOMETRICOS');
          }, 500);
        }
      });
    });
    /**----------------dvr-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'bajadvr');
    this.stompClient6 = Stomp.over(socket5);
    this.stompClient6.connect({}, (frame) => {
      _this.stompClient6.subscribe('/task/panelBajaDvr', (content) => {
        if (this.actualizar === 'DVR') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('DVR');
          }, 500);
        }
      });
    });
    /**----------------camara-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'bajacamara');
    this.stompClient7 = Stomp.over(socket6);
    this.stompClient7.connect({}, (frame) => {
      _this.stompClient7.subscribe('/task/panelBajaCamara', (content) => {
        if (this.actualizar === 'CAMARA') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('CAMARA');
          }, 500);
        }
      });
    });
    /**----------------barra-multicontacto-------------------**/
    const socket7 = new SockJS(environment.GLOBAL_SOCKET + 'bajabarramulticontacto');
    this.stompClient8 = Stomp.over(socket7);
    this.stompClient8.connect({}, (frame) => {
      _this.stompClient8.subscribe('/task/panelBajaBarraMultiContacto', (content) => {
        if (this.actualizar === 'BARRA-MULTICONTACTO') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('BARRA-MULTICONTACTO');
          }, 500);
        }
      });
    });
    /**----------------corriente-camara-------------------**/
    const socket8 = new SockJS(environment.GLOBAL_SOCKET + 'bajacorrientecamara');
    this.stompClient9 = Stomp.over(socket8);
    this.stompClient9.connect({}, (frame) => {
      _this.stompClient9.subscribe('/task/panelBajaCorrienteCamara', (content) => {
        if (this.actualizar === 'CORRIENTE-CAMARA') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('CORRIENTE-CAMARA');
          }, 500);
        }
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getEquipo() {
    this.ddexternoService.get().pipe(map(result => {
      return result.filter(data => data.activo === 0);
    })).subscribe(data => {
      this.ddexterno = data;
      this.dataSource = new MatTableDataSource(this.ddexterno);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    });
    this.opcionequipo = this.tipoEquipos.toLowerCase();
  }

  getEquipos(tipoEquipo) {
    this.actualizar = tipoEquipo;
    tipoEquipo = tipoEquipo.toLowerCase();
    this.tipoEquipos = tipoEquipo;
    this.opcionequipo = this.tipoEquipos;
    switch (tipoEquipo) {
      /**Mostrara la tabla de discos duros externos*/
      case 'disco duro externo':
        this.ddexternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.ddexterno = data;
          this.dataSource = new MatTableDataSource(this.ddexterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        });
        this.tipoEquipo = 'disco duro externo';
        break;
      /**Mostrara la tabla de discos duros internos*/
      case 'disco duro interno':
        this.ddinternoService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.ddinterno = data;
          this.dataSource = new MatTableDataSource(this.ddinterno);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'disco duro interno';
        break;
      /**Mostrara la tabla de chrome bit*/
      case 'chrome bit':
        this.chromeBitServices.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.chromeBit = data;
          this.dataSource = new MatTableDataSource(this.chromeBit);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'chrome bit';
        break;
      /**Mostrara la tabla de aps*/
      case 'aps':
        this.apsService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.aps = data;
          this.dataSource = new MatTableDataSource(this.aps);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'aps';
        break;
      /**Mostrara la tabla biometricos*/
      case 'biometricos':
        this.biometricosService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.biometricos = data;
          this.dataSource = new MatTableDataSource(this.biometricos);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'biometricos';
        break;
      /**Mostrara la tabla de dvrs*/
      case 'dvr':
        this.dvrService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.dvr = data;
          this.dataSource = new MatTableDataSource(this.dvr);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'dvr';
        break;
      /**Mostrara la tabla de camaras*/
      case 'camara':
        this.camaraService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.camara = data;
          this.dataSource = new MatTableDataSource(this.camara);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'camara';
        break;
      /**Mostrara la tabla de barra de multicontactos*/
      case 'barra-multicontacto':
        this.barraMultiService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.barraMulti = data;
          this.dataSource = new MatTableDataSource(this.barraMulti);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'barra-multicontacto';
        break;
      /**Mostrara la tabla de fuente corriente camaras */
      case 'corriente-camara':
        this.fuenteCorrienteService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.corrienteCam = data;
          this.dataSource = new MatTableDataSource(this.corrienteCam);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'corriente-camara';
        break;
      default:

        break;
    }
  }
  getDiscoDuroExternoById(id) {
    this.ddexternoService.getDiscoExternoById(id).subscribe(data => {
      this.ddexternodesactivar = data;
    });
  }
  getDiscoDuroInternoById(id) {
    this.ddinternoService.getDiscoInternoById(id).subscribe(data => {
      this.ddinternodesactivar = data;
    });
  }
  getChromeBitById(id) {
    this.chromeBitServices.getChromeBit(id).subscribe(data => {
      this.chromeBitdesactivar = data;
    });
  }
  getApsIpById(id) {
    this.apsService.getApsById(id).subscribe(data => {
      this.apsdesactivar = data;
    });
  }
  getBiometricById(id) {
    this.biometricosService.getBiometricosById(id).subscribe(data => {
      this.biometricosdesactivar = data;
    });
  }
  getDvrById(id) {
    this.dvrService.getDvrById(id).subscribe(data => {
      this.dvrdesactivar = data;
    });
  }
  getCamarasById(id) {
    this.camaraService.getCamarasById(id).subscribe(data => {
      this.camaradesactivar = data;
    });
  }
  getBarrasMulticontactoById(id) {
    this.barraMultiService.getBarrasMulticontactoById(id).subscribe(data => {
      this.barraMultidesactivar = data;
    });
  }
  getCorrienteCamaraById(id) {
    this.fuenteCorrienteService.getFuenteCamaraById(id).subscribe(data => {
      this.corrienteCamdesactivar = data;
    });
  }
  /** funcion que activara de nuevo un equipo y se mostrara un dropdown de cual es el estado del equipo*/
  async ActivarEquipo(idEquipo, tiposEquipo) {
    switch (this.tipoEquipos) {
      case 'disco duro externo':
        this.getDiscoDuroExternoById(idEquipo);
        break;
      case 'disco duro interno':
        this.getDiscoDuroInternoById(idEquipo);
        break;
      case 'chrome bit':
        this.getChromeBitById(idEquipo);
        break;
      case 'aps':
        this.getApsIpById(idEquipo);
        break;
      case 'biometricos':
        this.getBiometricById(idEquipo);
        break;
      case 'dvr':
        this.getDvrById(idEquipo);
        break;
      case 'camara':
        this.getCamarasById(idEquipo);
        break;
      case 'barra-multicontacto':
        this.getBarrasMulticontactoById(idEquipo);
        break;
      case 'corriente-camara':
        this.getCorrienteCamaraById(idEquipo);
        break;
      default:
        break;
    }

    // Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de Alta este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de Alta',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada una opción, debe de desactivar
      switch (this.tipoEquipos) {
        case 'disco duro externo':
          this.ddexternodesactivar['idEstadoInventario'] = Number(motivo);
          this.ddexternodesactivar['activo'] = 1;
          this.ddexternoService.put(idEquipo, this.ddexternodesactivar).subscribe(result => {
            this.ddexternoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.ddexternoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'disco duro interno':
          this.ddinternodesactivar['idEstadoInventario'] = Number(motivo);
          this.ddinternodesactivar['activo'] = 1;
          this.ddinternoService.put(idEquipo, this.ddinternodesactivar).subscribe(result => {
            this.ddinternoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.ddinternoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'chrome bit':
          this.chromeBitdesactivar['idEstadoInventario'] = Number(motivo);
          this.chromeBitdesactivar['activo'] = 1;
          this.chromeBitServices.put(idEquipo, this.chromeBitdesactivar).subscribe(result => {
            this.chromeBitServices.postSocket({status: 'ok'}).subscribe(x => {});
            this.chromeBitServices.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'aps':
          this.apsdesactivar['idEstadoInventario'] = Number(motivo);
          this.apsdesactivar['activo'] = 1;
          this.apsService.put(idEquipo, this.apsdesactivar).subscribe(result => {
            this.apsService.postSocket({status: 'ok'}).subscribe(x => {});
            this.apsService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'biometricos':
          this.biometricosdesactivar['idEstadoInventario'] = Number(motivo);
          this.biometricosdesactivar['activo'] = 1;
          this.biometricosService.put(idEquipo, this.biometricosdesactivar).subscribe(result => {
            this.biometricosService.postSocket({status: 'ok'}).subscribe(x => {});
            this.biometricosService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'dvr':
          this.dvrdesactivar['idEstadoInventario'] = Number(motivo);
          this.dvrdesactivar['activo'] = 1;
          this.dvrService.put(idEquipo, this.dvrdesactivar).subscribe(result => {
            this.dvrService.postSocket({status: 'ok'}).subscribe(x => {});
            this.dvrService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'camara':
          this.camaradesactivar['idEstadoInventario'] = Number(motivo);
          this.camaradesactivar['activo'] = 1;
          this.camaraService.put(idEquipo, this.camaradesactivar).subscribe(result => {
            this.camaraService.postSocket({status: 'ok'}).subscribe(x => {});
            this.camaraService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'barra-multicontacto':
          this.barraMultidesactivar['idEstadoInventario'] = Number(motivo);
          this.barraMultidesactivar['activo'] = 1;
          this.barraMultiService.put(idEquipo, this.barraMultidesactivar).subscribe(result => {
            this.barraMultiService.postSocket({status: 'ok'}).subscribe(x => {});
            this.barraMultiService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'corriente-camara':
          this.corrienteCamdesactivar['idEstadoInventario'] = Number(motivo);
          this.corrienteCamdesactivar['activo'] = 1;
          this.fuenteCorrienteService.put(idEquipo, this.corrienteCamdesactivar).subscribe(result => {
            this.fuenteCorrienteService.postSocket({status: 'ok'}).subscribe(x => {});
            this.fuenteCorrienteService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de Alta exitosamente!',
      }).then((resultado => {
        // window.location.reload();
        this.getEquipos(this.tipoEquipos);
      }));

    }

  }
  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({'id': this.estadoInventariodata[i].id, 'value': this.estadoInventariodata[i].estado});

      }
    });
  }
  /** funcion que muestra los detalles del equipo*/
  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }
  ngOnInit() {
    this.connect();
    this.getEstado();
    this.getEquipo();
    this.getPermisos();
    this.estaActivo();
    /** Se inicializa los nombres de las columnas*/
    this.cols = [ 'detalle', 'id', 'folio', 'marca', 'activo', 'acciones' ];
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bajasInventarioSoporte.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bajasInventarioSoporte.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
