import {Observable} from 'rxjs';

export interface TipoCometencia {
  id: number;
  tipo: string;
  activo: number;
}

export abstract class TipoCompetenciaData {
  abstract get(): Observable<TipoCometencia[]>;

  abstract post(tipoCompetencia): Observable<TipoCometencia>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTipoCompetenciaById(idTipoCompetencia): Observable<TipoCometencia>;

  abstract put(idTipoCompetencia, tipoCompetencia): Observable<TipoCometencia>;
}
