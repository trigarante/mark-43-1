import {Observable} from 'rxjs';

export interface EstadoRrhh {
    id: number;
    estado: string;
    idEstadoRh: number;
    activo: number;
    idEtapa: number;
}

export abstract class EstadoRrhhData {
  abstract get(): Observable<EstadoRrhh[]>;
  abstract createEstadoRrhh(estadoRrhh): Observable<EstadoRrhh>;
  abstract postSocket(json): Observable<JSON>;
  abstract getEstadoRrhhById(idEstadoRh): Observable<EstadoRrhh>;
  abstract updateEstadoRrhh(idEstadoRh, estadoRrhh): Observable<EstadoRrhh>;
}
