import { Component, OnInit, ViewChild } from '@angular/core';
import { VerDatosComponent } from '../modals/ver-datos/ver-datos.component';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'ngx-actualizacionesti',
  templateUrl: './actualizacionesti.component.html',
  styleUrls: ['./actualizacionesti.component.scss'],
})
export class ActualizacionestiComponent implements OnInit {



  constructor(private dialogService: NbDialogService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
  }

  verDatos() {
  }



}
