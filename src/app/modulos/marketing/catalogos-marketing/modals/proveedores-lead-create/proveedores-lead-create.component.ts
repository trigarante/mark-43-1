import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ProveedorLeadsData, ProveedorLeads } from '../../../../../@core/data/interfaces/catalogos/proveedor-leads';
import {NbDialogRef} from '@nebular/theme';
import { ProveedorLeadComponent} from '../../provedor-lead/provedor-lead.component';
import {TipoPaginaComponent} from '../../tipo-pagina/tipo-pagina.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-proveedores-lead-create',
  templateUrl: './proveedores-lead-create.component.html',
  styleUrls: ['./proveedores-lead-create.component.scss'],
})
export class ProveedoresLeadCreateComponent implements OnInit {

  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idTipoProveedor obtenido. */
  @Input() idTipoProveedor: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un tipo . */
  ProveedorCreateForm: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  tipoProveedor: ProveedorLeads;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;


  constructor(private fb: FormBuilder, protected ref: NbDialogRef<TipoPaginaComponent>, private ProveedorService: ProveedorLeadsData,
              private router: Router) {
  }

  // guardarProveedoresLead() {
  //   this.submitted = true;
  //   if (this.ProveedorCreateForm.invalid) {
  //     return;
  //   }
  //   this.proovedoresLeadService.post(this.ProveedorCreateForm.value).subscribe((result) => {
  //     this.ProveedorCreateForm.reset();
  //     this.ref.close();
  //     window.location.reload();
  //   });
  // }
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoPagina() {
    if (this.ProveedorCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.ProveedorService.post(this.ProveedorCreateForm.value).subscribe((result) => {
      this.ProveedorService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ProveedorCreateForm.reset();
      this.router.navigate(['/modulos/marketing/catalogos-marketing/provedor-lead']);
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un proveedor. */
  updateTipoPagina() {
    this.ProveedorService.put(this.idTipoProveedor, this.ProveedorCreateForm.value).subscribe( result => {
      this.ProveedorService.postSocket({status: 'ok'}).subscribe(x => {});
      this.ProveedorCreateForm.reset();
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que obtiene la información de un tipo dependiendo de su ID. */
  getProveedorById() {
    this.ProveedorService.getProveedorLeadById(this.idTipoProveedor).subscribe(data => {
      this.tipoProveedor = data;
      this.ProveedorCreateForm.controls['nombre'].setValue(this.tipoProveedor.nombre);
      this.ProveedorCreateForm.controls['url'].setValue(this.tipoProveedor.url);
      // this.ProveedorCreateForm.controls['activo'].setValue(this.tipoProveedor.activo);
    });
  }

  ngOnInit() {
    this.getProveedorById();
    this.ProveedorCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'url': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

}
