import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PrecandidatoUpdateComponent} from './precandidato-update.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {Form, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {EstadoCivilData} from '../../../@core/data/interfaces/catalogos/estado-civil';
import {EscolaridadData} from '../../../@core/data/interfaces/catalogos/escolaridad';
import {EstadoEscolaridadData} from '../../../@core/data/interfaces/catalogos/estado-escolaridad';
import {SepomexData} from '../../../@core/data/interfaces/catalogos/sepomex';
import {MedioTransporteData} from '../../../@core/data/interfaces/catalogos/medio-transporte';
import {EstacionesLineasData} from '../../../@core/data/interfaces/catalogos/estaciones-lineas';
import {SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';
import {PaisesData} from '../../../@core/data/interfaces/catalogos/paises';

describe('PrecandidatoUpdateComponent', () => {
  let component: PrecandidatoUpdateComponent;
  let fixture: ComponentFixture<PrecandidatoUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrecandidatoUpdateComponent],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
      ],
      providers: [
        PrecandidatosData,
        EstadoRrhhData,
        EstadoCivilData,
        EscolaridadData,
        EstadoEscolaridadData,
        SepomexData,
        MedioTransporteData,
        EstacionesLineasData,
        SolicitudesData,
        PaisesData,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(PrecandidatoUpdateComponent);*/
    /*component = fixture.componentInstance;*/
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    /*expect(component).toBeTruthy();*/
  });
});
