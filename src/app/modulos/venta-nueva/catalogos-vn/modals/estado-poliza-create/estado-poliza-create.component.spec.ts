import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoPolizaCreateComponent } from './estado-poliza-create.component';

describe('EstadoPolizaCreateComponent', () => {
  let component: EstadoPolizaCreateComponent;
  let fixture: ComponentFixture<EstadoPolizaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoPolizaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoPolizaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
