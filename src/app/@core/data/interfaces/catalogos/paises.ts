import {Observable} from 'rxjs';

export interface Paises {
    id: number;
    nombre: string;
    activo: number;
}

export abstract class PaisesData {
  abstract get(): Observable<Paises[]>;
  abstract post(paises: Paises): Observable<Paises>;
  abstract postSocket(json): Observable<JSON>;
  abstract getPaisesById(idPaises): Observable<Paises>;
  abstract put(idPaises, paises): Observable<Paises>;
}
