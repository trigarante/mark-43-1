import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosRoutingModule } from './catalogos-routing.module';
import {CatalogosComponent} from './catalogos.component';
import {ThemeModule} from '../../../@theme/theme.module';
import { AreaComponent } from './area/area.component';
import { BancoComponent } from './banco/banco.component';
import { BolsaTrabajoComponent } from './bolsa-trabajo/bolsa-trabajo.component';
import { CompetenciasComponent } from './competencias/competencias.component';
import { CompetenciasCandidatoComponent } from './competencias-candidato/competencias-candidato.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { EscolaridadComponent } from './escolaridad/escolaridad.component';
import { EstadoEscolaridadComponent } from './estado-escolaridad/estado-escolaridad.component';
import { EstadoCivilComponent } from './estado-civil/estado-civil.component';
import { MedioTransporteComponent } from './medio-transporte/medio-transporte.component';
import { PaisesComponent } from './paises/paises.component';
import { PuestoComponent } from './puesto/puesto.component';
import { TipoPuestoComponent } from './tipo-puesto/tipo-puesto.component';
import {TableModule} from 'primeng/table';
import {BancoCreateComponent} from './modals/banco-create/banco-create.component';
import {BolsaTrabajoCreateComponent} from './modals/bolsa-trabajo-create/bolsa-trabajo-create.component';
import {CompetenciasCreateComponent} from './modals/competencias-create/competencias-create.component';
import {CompetenciasCandidatoCreateComponent} from './modals/competencias-candidato-create/competencias-candidato-create.component';
import {EmpresaCreateComponent} from './modals/empresa-create/empresa-create.component';
import {EscolaridadCreateComponent} from './modals/escolaridad-create/escolaridad-create.component';
import {EstadoEscolaridadCreateComponent} from './modals/estado-escolaridad-create/estado-escolaridad-create.component';
import {MedioTransporteCreateComponent} from './modals/medio-transporte-create/medio-transporte-create.component';
import {PaisesCreateComponent} from './modals/paises-create/paises-create.component';
import {PuestoCreateComponent} from './modals/puesto-create/puesto-create.component';
import {TipoPuestoCreateComponent} from './modals/tipo-puesto-create/tipo-puesto-create.component';
import {NbDialogModule} from '@nebular/theme';
import {DropdownModule, KeyFilterModule} from 'primeng/primeng';
import {EstadoCivilCreateComponent} from './modals/estado-civil-create/estado-civil-create.component';
import {SedeComponent} from './sede/sede.component';
import { SedeCreateComponent } from './modals/sede-create/sede-create.component';
import { MotivosBajaComponent } from './motivos-baja/motivos-baja.component';
import { MotivoBajaCreateComponent } from './modals/motivo-baja-create/motivo-baja-create.component';
import {TooltipModule} from 'primeng/tooltip';
import { GruposComponent } from './grupos/grupos.component';
import { GruposCreateComponent } from './modals/grupos-create/grupos-create.component';
import { SubAreaComponent } from './sub-area/sub-area.component';
import { SubAreaCreateComponent } from './modals/sub-area-create/sub-area-create.component';
import { AreaCreateComponent } from './modals/area-create/area-create.component';
import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';
import {RazonSocialComponent} from './razon-social/razon-social.component';
import { RazonSocialCreateComponent } from './modals/razon-social-create/razon-social-create.component';
import { TurnoComponent } from './turno/turno.component';
import { TurnoCreateComponent } from './modals/turno-create/turno-create.component';

@NgModule({
  declarations: [
    CatalogosComponent,
    AreaComponent,
    BancoComponent,
    BolsaTrabajoComponent,
    CompetenciasComponent,
    CompetenciasCandidatoComponent,
    EmpresaComponent,
    EscolaridadComponent,
    EstadoEscolaridadComponent,
    EstadoCivilComponent,
    MedioTransporteComponent,
    PaisesComponent,
    PuestoComponent,
    TipoPuestoComponent,
    BancoCreateComponent,
    BolsaTrabajoCreateComponent,
    CompetenciasCreateComponent,
    CompetenciasCandidatoCreateComponent,
    EmpresaCreateComponent,
    EscolaridadCreateComponent,
    EstadoEscolaridadCreateComponent,
    MedioTransporteCreateComponent,
    PaisesCreateComponent,
    PuestoCreateComponent,
    TipoPuestoCreateComponent,
    EstadoCivilCreateComponent,
    SedeComponent,
    SedeCreateComponent,
    MotivosBajaComponent,
    MotivoBajaCreateComponent,
    GruposCreateComponent,
    SubAreaCreateComponent,
    AreaCreateComponent,
    SubAreaComponent,
    GruposComponent,
    RazonSocialComponent,
    RazonSocialCreateComponent,
    TurnoComponent,
    TurnoCreateComponent,
  ],
  imports: [
    CommonModule,
    ThemeModule,
    KeyFilterModule,
    CatalogosRoutingModule,
    TableModule,
    NbDialogModule.forChild(),
    DropdownModule,
    TooltipModule,
    MatTableModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
  ],
  entryComponents: [
    BancoCreateComponent,
    BolsaTrabajoCreateComponent,
    CompetenciasCreateComponent,
    CompetenciasCandidatoCreateComponent,
    EmpresaCreateComponent,
    EscolaridadCreateComponent,
    EstadoEscolaridadCreateComponent,
    MedioTransporteCreateComponent,
    PaisesCreateComponent,
    PuestoCreateComponent,
    TipoPuestoCreateComponent,
    EstadoCivilCreateComponent,
    SedeCreateComponent,
    MotivoBajaCreateComponent,
    GruposCreateComponent,
    SubAreaCreateComponent,
    AreaCreateComponent,
    RazonSocialCreateComponent,
    TurnoCreateComponent,
  ],
})
export class CatalogosModule { }
