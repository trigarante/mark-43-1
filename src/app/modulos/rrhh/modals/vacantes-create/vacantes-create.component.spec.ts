import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {VacantesCreateComponent} from './vacantes-create.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Router, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {PuestoData} from '../../../../@core/data/interfaces/catalogos/puesto';
import {PuestoTipoData} from '../../../../@core/data/interfaces/catalogos/puesto-tipo';
import {TipoUsuarioData} from '../../../../@core/data/interfaces/catalogos/tipo-usuario';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData} from '../../../../@core/data/interfaces/catalogos/area';
import {NbDialogRef, NbToastrService} from '@nebular/theme';
import {MarcaEmpresarialData} from '../../../../@core/data/interfaces/catalogos/marca-empresarial';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';


describe('VacantesCreateComponent', () => {
  let component: VacantesCreateComponent;
  let fixture: ComponentFixture<VacantesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacantesCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
      ],
      providers: [
        VacanteData,
        SubareaData,
        PuestoData,
        PuestoTipoData,
        TipoUsuarioData,
        SedeData,
        GrupoData,
        EmpresaData,
        AreaData,
        NbToastrService,
        MarcaEmpresarialData,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacantesCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
