import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {Switchs, SwitchsData} from '../../../../interfaces/ti/soporte/inventario/switchs';

@Injectable()
export class SwitchsService extends SwitchsData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Switchs[]> {
    return this.http.get<Switchs[]>( this.baseURL + 'v1/switch');
  }

  post(switchs: Switchs): Observable<Switchs> {
    return this.http.post<Switchs>(this.baseURL + 'v1/switch', switchs);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'switch/saveSwitch', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajaswitch/saveBajaSwitch', json);
  }
  getSwitchsById(idSwitchs): Observable<Switchs> {
    return this.http.get<Switchs>(this.baseURL + 'v1/switch/' + idSwitchs);
  }
  put(idSwitchs, switchs: Switchs): Observable<Switchs> {
    return this.http.put<Switchs>(this.baseURL + 'v1/switch/' + idSwitchs + '/'
      + sessionStorage.getItem('Empleado') , switchs );
  }

}
