import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Aps, ApsData} from '../../../../interfaces/ti/soporte/inventario-soporte/aps';
import {Observable} from 'rxjs';

@Injectable()
export class ApsService extends ApsData {

  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Aps[]> {
    return this.http.get<Aps[]>(this.baseURL + 'v1/aps');
  }

  post(aps: Aps): Observable<Aps> {
    return this.http.post<Aps>(this.baseURL + 'v1/aps', aps);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'aps/saveAps', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajaaps/saveBajaAps', json);
  }

  getApsById(idAps): Observable<Aps> {
    return this.http.get<Aps>(this.baseURL + 'v1/aps/' + idAps);
  }

  put(idAps, aps: Aps): Observable<Aps> {
    return this.http.put<Aps>(this.baseURL + 'v1/aps/' + idAps + '/'
      + sessionStorage.getItem('Empleado')  , aps);
  }
}
