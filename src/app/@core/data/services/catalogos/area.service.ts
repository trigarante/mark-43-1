import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Area, AreaData} from '../../interfaces/catalogos/area';
import {environment} from '../../../../../environments/environment';


@Injectable()
export class AreaService extends AreaData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Area[]> {
    return this.http.get<Area[]>(this.baseURL + 'v1/areas');
  }

  post(area: Area): Observable<Area> {
    return this.http.post<Area>(this.baseURL + 'v1/areas', area);
  }

  getAreaById(idArea): Observable<Area> {
    return this.http.get<Area>(this.baseURL + 'v1/areas/' + idArea);
  }

  put(idArea, area: Area) {
    return this.http.put<Area>(this.baseURL + 'v1/areas/' + idArea, area);
  }
}
