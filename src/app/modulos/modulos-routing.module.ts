import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ModulosComponent} from './modulos.component';
import {NotFoundComponent} from './miscellaneous/not-found/not-found.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from '../@core/data/services/login/auth.guard';


const routes: Routes = [{


  path: '',
  component: ModulosComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'actualizaciones',
      loadChildren: './actualizaciones/actualizaciones.module#ActualizacionesModule',

    },
    {
      path: 'rrhh',
      loadChildren: './rrhh/rrhh.module#RrhhModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'ti',
      loadChildren: './ti/ti.module#TiModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'reportes',
      loadChildren: './reportes/reportes.module#ReportesModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'markcalle',
      loadChildren: './markcalle/markcalle.module#MarkcalleModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'comercial',
      loadChildren: './comercial/comercial.module#ComercialModule',
      canActivate: [AuthGuard],

    },
    {
      path: 'marketing',
      loadChildren: './marketing/marketing.module#MarketingModule',
      canActivate: [AuthGuard],

    },
    {
      path: 'venta-nueva',
      loadChildren: './venta-nueva/venta-nueva.module#VentaNuevaModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'mesa-control',
      loadChildren: './mesa-control/mesa-control.module#MesaControlModule',
    },
    {
      path: 'autorizaciones',
      loadChildren: './autorizaciones/autorizaciones.module#AutorizacionesModule',
    },
    {
      path: 'endosos-cancelaciones',
      loadChildren: './endosos-cancelaciones/endosos-cancelaciones.module#EndososCancelacionesModule',
    },
    {
      path: 'cotizaciones',
      loadChildren: './cotizaciones/cotizaciones.module#CotizacionesModule',
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
      canActivate: [AuthGuard],
    },
    {
      path: 'finanzas',
      loadChildren: './finanzas/finanzas.module#FinanzasModule',
    },
    {
      path: 'pruebas-g',
      loadChildren: './pruebas/pruebas.module#PruebasModule',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModulosRoutingModule {
}
