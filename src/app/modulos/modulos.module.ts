import { NgModule } from '@angular/core';
import {ModulosComponent} from './modulos.component';
import {ModulosRoutingModule} from './modulos-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {MiscellaneousModule} from './miscellaneous/miscellaneous.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {TableModule} from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatRippleModule, MatInputModule, MatButtonModule, MatStepperModule, MatCheckboxModule,
  MatChipsModule, MatDatepickerModule, MatCardModule, MatBottomSheetModule, MatButtonToggleModule, MatBadgeModule,
  MatAutocompleteModule, MatGridListModule, MatListModule, MatDialogModule, MatDividerModule, MatIconModule, MatMenuModule,
  MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule,
  MatToolbarModule,
  MatTooltipModule, MatTreeModule } from '@angular/material';
import { DialogModule, TooltipModule, FileUploadModule } from 'primeng/primeng';
import { NbTooltipModule } from '@nebular/theme';

// import { PruebasComponent } from './pruebas/pruebas.component';

@NgModule({
  declarations: [
    ModulosComponent,
  ],
  imports: [
    ModulosRoutingModule,
    ThemeModule,
    TableModule,
    MiscellaneousModule,
    DashboardModule,
    CheckboxModule,
    CalendarModule,
    FullCalendarModule,
    DialogModule,
    NbTooltipModule,
    TooltipModule,
    FileUploadModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  exports: [
  ],
})
export class ModulosModule { }
