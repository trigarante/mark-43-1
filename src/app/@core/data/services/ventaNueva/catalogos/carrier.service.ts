import { Injectable } from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Carrier, CarrierData} from '../../../interfaces/ventaNueva/catalogos/carrier';

@Injectable()
export class CarrierService extends CarrierData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Carrier[]> {
    return this.http.get<Carrier[]>(this.baseURL + 'v1/carrier');
  }

  getCarrierById(id): Observable<Carrier> {
    return this.http.get<Carrier>(this.baseURL + 'v1/carrier/' + id);
  }

  post(carrier): Observable<Carrier> {
    return this.http.post<Carrier>(this.baseURL + 'v1/carrier', carrier);
  }

  put(id, carrier): Observable<Carrier> {
    return this.http.put<Carrier>(this.baseURL + 'v1/carrier' + id, carrier);
  }


}
