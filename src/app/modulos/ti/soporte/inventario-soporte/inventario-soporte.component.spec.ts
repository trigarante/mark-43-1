import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarioSoporteComponent } from './inventario-soporte.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {DiscoDuroExternoData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-externo';
import {DiscoDuroInternoData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/disco-duro-interno';
import {ChromeBitData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/chrome-bit';
import {ApsData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/aps';
import {BiometricosData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/biometricos';
import {DvrData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/dvr';
import {CamarasData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/camaras';
import {BarrasMulticontactoData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/barras-multicontacto';
import {FuenteCamarasData} from '../../../../@core/data/interfaces/ti/soporte/inventario-soporte/fuenta-camaras';

describe('InventarioSoporteComponent', () => {
  let component: InventarioSoporteComponent;
  let fixture: ComponentFixture<InventarioSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventarioSoporteComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        DiscoDuroExternoData,
        DiscoDuroInternoData,
        ChromeBitData,
        ApsData,
        BiometricosData,
        DvrData,
        CamarasData,
        BarrasMulticontactoData,
        FuenteCamarasData,
        EstadoInventarioData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    /*fixture = TestBed.createComponent(InventarioSoporteComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    /*expect(component).toBeTruthy();*/
  });
});
