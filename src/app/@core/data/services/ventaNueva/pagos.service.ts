import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pagos, PagosData} from '../../interfaces/ventaNueva/pagos';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PagosService extends PagosData {
  private baseURL;

  constructor(private http: HttpClient ) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  get(): Observable<Pagos[]> {
    return this.http.get<Pagos[]>(this.baseURL + 'v1/pagos');
  }
  getPagoById(id): Observable<Pagos> {
    return this.http.get<Pagos>(this.baseURL + 'v1/pagos/' + id);
  }
  post( pago ): Observable<Pagos> {
    return this.http.post<Pagos>(this.baseURL + 'v1/pagos', pago);
  }
  put( id, pago: Pagos ): Observable<Pagos> {
    return this.http.put<Pagos>(this.baseURL + 'v1/pagos' + id, pago);
  }
}
