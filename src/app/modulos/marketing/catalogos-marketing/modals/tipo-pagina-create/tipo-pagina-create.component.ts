import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import { TipoPagina, TipoPaginaData } from '../../../../../@core/data/interfaces/catalogos/tipo-pagina';
import { TipoPaginaComponent } from '../../tipo-pagina/tipo-pagina.component';



@Component({
  selector: 'ngx-tipo-pagina-create',
  templateUrl: './tipo-pagina-create.component.html',
  styleUrls: ['./tipo-pagina-create.component.scss'],
})
export class TipoPaginaCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idTipoPagina obtenido. */
  @Input() idTipoPagina: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un tipo . */
  tipoPaginaCreateForm: FormGroup;
  /** Variable que se inicializa con los valores del Microservicio. */
  tipoPagina: TipoPagina;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;


  /**El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {NbDialogRef<BancoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<TipoPaginaComponent>, private tipoPaginaService: TipoPaginaData,
              private router: Router) { }

  /** Función que cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarTipoPagina() {
    if (this.tipoPaginaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.tipoPaginaService.post(this.tipoPaginaCreateForm.value).subscribe((result) => {
      this.tipoPaginaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoPaginaCreateForm.reset();
      this.router.navigate(['/modulos/marketing/catalogos-marketing/tipo-pagina']);
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que actualiza la información de un Tipo pagina. */
  updateTipoPagina() {
    this.tipoPaginaService.put(this.idTipoPagina, this.tipoPaginaCreateForm.value).subscribe( result => {
      this.tipoPaginaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.tipoPaginaCreateForm.reset();
      this.ref.close();
      // window.location.reload();
    });
  }

  /** Función que obtiene la información de un tipo dependiendo de su ID. */
  getTipoPaginaById() {
    this.tipoPaginaService.getTipoPaginaById(this.idTipoPagina).subscribe(data => {
      this.tipoPagina = data;
      this.tipoPaginaCreateForm.controls['tipo'].setValue(this.tipoPagina.tipo);
      this.tipoPaginaCreateForm.controls['objetivo'].setValue(this.tipoPagina.objetivo);
      this.tipoPaginaCreateForm.controls['activo'].setValue(this.tipoPagina.activo);
    });
  }

  ngOnInit() {
    if (this.idTipo === 2 || this.idTipo === 3) {
      this.getTipoPaginaById();
    }
    this.tipoPaginaCreateForm = this.fb.group( {
      'tipo': new FormControl('', Validators.compose([Validators.required])),
      'objetivo': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
