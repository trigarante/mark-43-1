import {Observable} from 'rxjs';

export interface Marca {
  id: number;
  nombreMarca: string;
  activo: number;
}
export abstract class MarcaSoporteData {
  abstract get(): Observable<Marca[]>;
  abstract post(marca): Observable<Marca>;
  abstract postSocket(json): Observable<JSON>;
  abstract getMarcasSoporte(idMarca): Observable<Marca>;
  abstract put(idMarca, marca): Observable<Marca>;
}
