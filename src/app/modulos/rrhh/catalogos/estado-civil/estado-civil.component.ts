import {Component, OnInit, ViewChild} from '@angular/core';
import {EstadoCivil, EstadoCivilData} from '../../../../@core/data/interfaces/catalogos/estado-civil';
import {NbDialogService} from '@nebular/theme';
import {EstadoCivilCreateComponent} from '../modals/estado-civil-create/estado-civil-create.component';
import {environment} from '../../../../../environments/environment';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de estado civil (catálogo)
 */
@Component({
  selector: 'ngx-estado-civil',
  templateUrl: './estado-civil.component.html',
  styleUrls: ['./estado-civil.component.scss'],
})
export class EstadoCivilComponent implements OnInit {
  cols: any[];
  dataSource: any;
  estadoCivil: EstadoCivil[];
  estadoCiv: EstadoCivil;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {EstadoCivilData} estadoCicvilService Servicio del componente estado-civil para poder invocar los métodos.
   * @param {NbDialogService} dialogoService Servicio de mensajes. */
  constructor(private estadoCicvilService: EstadoCivilData, private dialogoService: NbDialogService, private router: Router) {
  }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'estadocivil');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      console.log(frame);
      _this.stompClient.subscribe('/task/panelEstadoCivil', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getEstadoCivil();
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Esta función permite obtener el estado civil y lo asigna al arreglo estadoCivil. */
  getEstadoCivil() {
    this.estadoCicvilService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.estadoCivil = [];
      this.estadoCivil = data;
      this.dataSource = new MatTableDataSource(this.estadoCivil); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  /** Función que obtiene la información del estado civil dependiendo de su ID. */
  getEstadoCivilById(idEstadoCivil) {
    this.estadoCicvilService.getEstadoCivilById(idEstadoCivil).subscribe(data => {
      this.estadoCiv = data;
    });
  }

  /** Función que despliega un modal del componente "EstadoCivilCreateComponent" para poder "crear estado civil". */
  crearEstadoCivil() {
    this.dialogoService.open(EstadoCivilCreateComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getEstadoCivil();
    });
  }

  /** Función que despliega un modal el cual permite actualizar la información del estado civil. */
  updateEstadoCivil(idEstadoCivil) {
    this.dialogoService.open(EstadoCivilCreateComponent, {
      context: {
        idTipo: 2,
        idEstadoCivil: idEstadoCivil,
      },
    }).onClose.subscribe(x => {
      this.getEstadoCivil();
    });
  }

  /** Función que despliega un modal para dar de baja un estado civil. */
  bajaEstadoCivil(idEstadoCivil) {
      /*this.dialogoService.open(EstadoCivilCreateComponent, {
        context: {
          idTipo: 3,
          idEstadoCivil: idEstadoCivil,
        },
      });*/
      this.getEstadoCivilById(idEstadoCivil);
    swal({
      title: '¿Deseas dar de baja este estado civil?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.estadoCiv['activo'] = 0;
        this.estadoCicvilService.put(idEstadoCivil, this.estadoCiv).subscribe( result => {
          this.estadoCicvilService.postSocket({idEstadoCivil: idEstadoCivil, estadoCiv: this.estadoCiv}).subscribe(x => {});
        });
        swal('¡El estado civil se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          //window.location.reload();
          this.getEstadoCivil();
        }));
      }
    });
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.estadoCivil.escritura;
    console.log(this.escritura);
  }

  estaActivo() {
    this.activo = this.permisos.modulos.estadoCivil.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  ngOnInit() {
    this.connect();
    this.cols = ['descripcion', 'acciones'];
    this.getPermisos();
    this.estaActivo();
    this.getEstadoCivil();
  }
}
