import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../environments/environment';
import {EstadoSolicitud, EstadoSolicitudData} from '../../../interfaces/ventaNueva/catalogos/estado-solicitud';
import {Observable} from 'rxjs';

@Injectable()
export class EstadoSolicitudService extends EstadoSolicitudData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<EstadoSolicitud[]> {
    return this.http.get<EstadoSolicitud[]>(this.baseURL + 'v1/estado-solicitud');
  }

  post(estadoSolicitud: EstadoSolicitud): Observable<EstadoSolicitud> {
    return this.http.post<EstadoSolicitud>(this.baseURL + 'v1/estado-solicitud', estadoSolicitud);
  }

  getAreaById(idEstadoSolicitud): Observable<EstadoSolicitud> {
    return this.http.get<EstadoSolicitud>(this.baseURL + 'v1/estado-solicitud/' + idEstadoSolicitud);
  }

  put(idEstadoSolicitud, estadoSolicitud: EstadoSolicitud) {
    return this.http.put<EstadoSolicitud>(this.baseURL + 'v1/estado-solicitud/' + idEstadoSolicitud, estadoSolicitud);
  }
}
