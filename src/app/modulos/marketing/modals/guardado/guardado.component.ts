import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-guardado',
  templateUrl: './guardado.component.html',
  styleUrls: ['./guardado.component.scss'],
})
export class GuardadoComponent implements OnInit {
  @Input() id: number;
  @Input() modo: number;
  constructor(protected ref: NbDialogRef<GuardadoComponent>, private router: Router) { }

  dismiss() {
    this.ref.close();
    if (this.modo === 1) {
      this.router.navigate(['/modulos/marketing/aseguradoras/', this.id]);
    }

    if (this.modo === 2) {
      this.router.navigate(['/modulos/marketing/pagina/']);
    }
    if (this.modo === 3) {
      this.router.navigate(['/modulos/marketing/campana/']);
    }

  }

  ngOnInit() {
  }

}
