import { TestBed } from '@angular/core/testing';

import { TipoSedeService } from './tipo-sede.service';

describe('TipoSedeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoSedeService = TestBed.get(TipoSedeService);
    expect(service).toBeTruthy();
  });
});
