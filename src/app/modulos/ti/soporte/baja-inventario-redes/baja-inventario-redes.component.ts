import {Component, OnInit, ViewChild} from '@angular/core';
import {Switchs, SwitchsData} from '../../../../@core/data/interfaces/ti/soporte/inventario/switchs';
import {Servidores, ServidoresData} from '../../../../@core/data/interfaces/ti/soporte/inventario/servidores';
import {TarjetaRed, TarjetaRedData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tarjeta-red';
import {TelefonoIp, TelefonoIpData} from '../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';
import {Racks, RacksData} from '../../../../@core/data/interfaces/ti/soporte/inventario/racks';
import {PatchPanel, PatchPanelData} from '../../../../@core/data/interfaces/ti/soporte/inventario/patch-panel';
import {CharolasRack, CharolasRackData} from '../../../../@core/data/interfaces/ti/soporte/inventario/charolas-rack';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import swal2 from 'sweetalert2';
import {NbDialogService} from '@nebular/theme';
import {environment} from '../../../../../environments/environment';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-baja-inventario-redes',
  templateUrl: './baja-inventario-redes.component.html',
  styleUrls: ['./baja-inventario-redes.component.scss'],
})
export class BajaInventarioRedesComponent implements OnInit {
  /** Variable que inicializa las columnas*/
  cols: any[];
  dataSource: any;
  switch: Switchs[];
  servidores: Servidores[];
  tarjetaRed: TarjetaRed[];
  telefonoIp: TelefonoIp[];
  racks: Racks[];
  patchPanel: PatchPanel[];
  charolaRack: CharolasRack[];
  switchdesactivar: Switchs;
  servidoresdesactivar: Servidores;
  tarjetaReddesactivar: TarjetaRed;
  telefonoIpdesactivar: TelefonoIp;
  racksdesactivar: Racks;
  patchPaneldesactivar: PatchPanel;
  charolaRackdesactivar: CharolasRack;
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;
  tipoEquipos: string;

  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  opcionequipo: string;
  // sockets
  actualizar: string;
  private stompClient1 = null;
  private stompClient2 = null;
  private stompClient3 = null;
  private stompClient4 = null;
  private stompClient5 = null;
  private stompClient6 = null;
  private stompClient7 = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private fb: FormBuilder, private estadoInventarioServive: EstadoInventarioData, private rutaActiva: ActivatedRoute,
              private switchsService: SwitchsData, private servidoresService: ServidoresData,
              private tarjetaRedService: TarjetaRedData, private telefonoIdService: TelefonoIpData, private racksService: RacksData,
              private patchPanelService: PatchPanelData, private charolasRackService: CharolasRackData,
              public dialogService: NbDialogService, private router: Router) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    /**----------------------switch-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bajaswitch');
    this.stompClient1 = Stomp.over(socket);
    this.stompClient1.connect({}, (frame) => {
      _this.stompClient1.subscribe('/task/panelBajaSwitch', (content) => {
        if (this.actualizar === 'SWITCH' || this.tipoEquipos === 'SWITCH') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('SWITCH');
            _this.getEquipo();
          }, 500);
        }
      });
    });
    /**----------------------servidores-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'bajaservidores');
    this.stompClient2 = Stomp.over(socket1);
    this.stompClient2.connect({}, (frame) => {
      _this.stompClient2.subscribe('/task/panelBajaServidores', (content) => {
        if (this.actualizar === 'SERVIDORES') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('SERVIDORES');
          }, 500);
        }
      });
    });
    /**----------------------tarjeta de red-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'bajatargetared');
    this.stompClient3 = Stomp.over(socket2);
    this.stompClient3.connect({}, (frame) => {
      _this.stompClient3.subscribe('/task/panelBajaTargetaRed', (content) => {
        if (this.actualizar === 'TARJETA DE RED') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('TARJETA DE RED');
          }, 500);
        }
      });
    });
    /**----------------------telefonos ip-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'bajatelefonoip');
    this.stompClient4 = Stomp.over(socket3);
    this.stompClient4.connect({}, (frame) => {
      _this.stompClient4.subscribe('/task/panelBajaTelefonoIP', (content) => {
        if (this.actualizar === 'TELEFONOS IP') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('TELEFONOS IP');
          }, 500);
        }
      });
    });
    /**----------------------rack-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'bajarack');
    this.stompClient5 = Stomp.over(socket4);
    this.stompClient5.connect({}, (frame) => {
      _this.stompClient5.subscribe('/task/panelBajaRack', (content) => {
        if (this.actualizar === 'RACK') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('RACK');
          }, 500);
        }
      });
    });
    /**----------------patch panel-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'bajapatchpanel');
    this.stompClient6 = Stomp.over(socket5);
    this.stompClient6.connect({}, (frame) => {
      _this.stompClient6.subscribe('/task/panelBajaPatchPanel', (content) => {
        if (this.actualizar === 'PATCH PANEL') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('PATCH PANEL');
          }, 500);
        }
      });
    });
    /**----------------charolas rack-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'bajacharolasrack');
    this.stompClient7 = Stomp.over(socket6);
    this.stompClient7.connect({}, (frame) => {
      _this.stompClient7.subscribe('/task/panelBajaCharolasRack', (content) => {
        if (this.actualizar === 'CHAROLAS RACK') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('CHAROLAS RACK');
          }, 500);
        }
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getEquipo() {
    this.switchsService.get().pipe(map(result => {
      return result.filter(data => data.activo === 0);
    })).subscribe(data => {
      this.switch = data;
      this.dataSource = new MatTableDataSource(this.switch);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    });
    this.tipoEquipos = 'switch';
    this.opcionequipo = this.tipoEquipos.toLowerCase();
  }
  /** Se muestra un menu con el componente TAB, con el selectedTabChange se selecciona que datos mostrara en la tabla*/
  getEquipos(tipoEquipo) {
    this.actualizar = tipoEquipo;
    tipoEquipo = tipoEquipo.toLowerCase();
    this.tipoEquipos = tipoEquipo;
    this.opcionequipo = this.tipoEquipos;

    switch (tipoEquipo) {
      case 'switch':
        this.switchsService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.switch = data;
          this.dataSource = new MatTableDataSource(this.switch);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;

        });
        this.tipoEquipo = 'switch';
        break;
      case 'servidores':
        this.servidoresService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.servidores = data;
          this.dataSource = new MatTableDataSource(this.servidores);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'servidores';
        break;
      case 'tarjeta de red':
        this.tarjetaRedService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.tarjetaRed = data;
          this.dataSource = new MatTableDataSource(this.tarjetaRed);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'tarjeta de red';
        break;
      case 'telefonos ip':
        this.telefonoIdService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.telefonoIp = data;
          this.dataSource = new MatTableDataSource(this.telefonoIp);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'telefonos ip';
        break;
      case 'rack':
        this.racksService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.racks = data;
          this.dataSource = new MatTableDataSource(this.racks);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'rack';
        break;
      case 'patch panel':
        this.patchPanelService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.patchPanel = data;
          this.dataSource = new MatTableDataSource(this.patchPanel);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'patch panel';
        break;
      case 'charolas rack':
        this.charolasRackService.get().pipe(map(result => {
          return result.filter(data => data.activo === 0);
        })).subscribe(data => {
          this.charolaRack = data;
          this.dataSource = new MatTableDataSource(this.charolaRack);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        });
        this.tipoEquipo = 'charolas rack';
        break;
      default:

        break;
    }
  }
  /** funcion que muestra los detalles del equipo*/
  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }
  getSwitchById(id) {
    this.switchsService.getSwitchsById(id).subscribe(data => {
      this.switchdesactivar = data;
    });
  }
  getServidoresById(id) {
    this.servidoresService.getServidoresById(id).subscribe(data => {
      this.servidoresdesactivar = data;
    });
  }
  getTarjetaRedById(id) {
    this.tarjetaRedService.getTarjetaRedById(id).subscribe(data => {
      this.tarjetaReddesactivar = data;
    });
  }
  getTelefonosIpById(id) {
    this.telefonoIdService.getTelefonoIpById(id).subscribe(data => {
      this.telefonoIpdesactivar = data;
    });
  }
  getRackById(id) {
    this.racksService.getRacksById(id).subscribe(data => {
      this.racksdesactivar = data;
    });
  }
  getPatchPanelById(id) {
    this.patchPanelService.getPatchPanelById(id).subscribe(data => {
      this.patchPaneldesactivar = data;
    });
  }
  getCharolasRackById(id) {
    this.charolasRackService.getCharolasRackById(id).subscribe(data => {
      this.charolaRackdesactivar  = data;
    });
  }
  /** funcion que activara de nuevo un equipo y se mostrara un dropdown de cual es el estado del equipo*/
  async ActivarEquipo(idEquipo, tiposEquipo) {
    switch (this.tipoEquipos) {
      case 'switch':
        this.getSwitchById(idEquipo);
        break;
      case 'servidores':
        this.getServidoresById(idEquipo);
        break;
      case 'tarjeta de red':
        this.getTarjetaRedById(idEquipo);
        break;
      case 'telefonos ip':
        this.getTelefonosIpById(idEquipo);
        break;
      case 'rack':
        this.getRackById(idEquipo);
        break;
      case 'patch panel':
        this.getPatchPanelById(idEquipo);
        break;
      case 'charolas rack':
        this.getCharolasRackById(idEquipo);
        break;
      default:
        break;
    }

    // Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de Alta este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de Alta',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada una opción, debe de desactivar
      switch (this.tipoEquipos) {
        case 'switch':
          this.switchdesactivar['idEstadoInventario'] = Number(motivo);
          this.switchdesactivar['activo'] = 1;
          this.switchsService.put(idEquipo, this.switchdesactivar).subscribe(result => {
            this.switchsService.postSocket({status: 'ok'}).subscribe(x => {});
            this.switchsService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'servidores':
          this.servidoresdesactivar['idEstadoInventario'] = Number(motivo);
          this.servidoresdesactivar['activo'] = 1;
          this.servidoresService.put(idEquipo, this.servidoresdesactivar).subscribe(result => {
            this.servidoresService.postSocket({status: 'ok'}).subscribe(x => {});
            this.servidoresService.postSocketBaja({status: 'ok'}).subscribe(x => {});

          });
          break;
        case 'tarjeta de red':
          this.tarjetaReddesactivar['idEstadoInventario'] = Number(motivo);
          this.tarjetaReddesactivar['activo'] = 1;
          this.tarjetaRedService.put(idEquipo, this.tarjetaReddesactivar).subscribe(result => {
            this.tarjetaRedService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tarjetaRedService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'telefonos ip':
          this.telefonoIpdesactivar['idEstadoInventario'] = Number(motivo);
          this.telefonoIpdesactivar['activo'] = 1;
          this.telefonoIdService.put(idEquipo, this.telefonoIpdesactivar).subscribe(result => {
            this.telefonoIdService.postSocket({status: 'ok'}).subscribe(x => {});
            this.telefonoIdService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'rack':
          this.racksdesactivar['idEstadoInventario'] = Number(motivo);
          this.racksdesactivar['activo'] = 1;
          this.racksService.put(idEquipo, this.racksdesactivar).subscribe(result => {
            this.racksService.postSocket({status: 'ok'}).subscribe(x => {});
            this.racksService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'patch panel':
          this.patchPaneldesactivar['idEstadoInventario'] = Number(motivo);
          this.patchPaneldesactivar['activo'] = 1;
          this.patchPanelService.put(idEquipo, this.patchPaneldesactivar).subscribe(result => {
            this.patchPanelService.postSocket({status: 'ok'}).subscribe(x => {});
            this.patchPanelService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'charolas rack':
          this.charolaRackdesactivar['idEstadoInventario'] = Number(motivo);
          this.charolaRackdesactivar['activo'] = 1;
          this.charolasRackService.put(idEquipo, this.charolaRackdesactivar).subscribe(result => {
            this.charolasRackService.postSocket({status: 'ok'}).subscribe(x => {});
            this.charolasRackService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de Alta exitosamente!',
      }).then((resultado => {
        // window.location.reload();
        this.getEquipos(this.tipoEquipos);
      }));

    }

  }
  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({'id': this.estadoInventariodata[i].id, 'value': this.estadoInventariodata[i].estado});

      }
    });
  }
  ngOnInit() {
    this.connect();
    this.getEstado();
    this.getEquipo();
    /** Se inicializa los nombres de las columnas*/
    this.cols = [ 'detalle', 'id', 'folio', 'marca', 'activo', 'acciones' ];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bajasInventarioRedes.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bajasInventarioRedes.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
