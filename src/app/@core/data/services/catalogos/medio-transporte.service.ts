import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MedioTransporte, MedioTransporteData} from '../../interfaces/catalogos/medio-transporte';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class MedioTransporteService extends MedioTransporteData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  get(): Observable<MedioTransporte[]> {
    return this.http.get<MedioTransporte[]>(this.baseURL + 'v1/medio-transporte');
  }

  post(medioTransporte: MedioTransporte): Observable<MedioTransporte> {
    return this.http.post<MedioTransporte>(this.baseURL + 'v1/medio-transporte', medioTransporte);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'mediotransporte/saveMedioTransporte', json);
  }

  getMedioTransporteById(idMedioTransporte): Observable<MedioTransporte> {
    return this.http.get<MedioTransporte>(this.baseURL + 'v1/medio-transporte/' + idMedioTransporte);
  }
  put(idMedioTransporte, medioTransporte: MedioTransporte): Observable<MedioTransporte> {
    return this.http.put<MedioTransporte>(this.baseURL + 'v1/medio-transporte/' + idMedioTransporte + '/'
      + sessionStorage.getItem('Empleado'), medioTransporte);
  }
}
