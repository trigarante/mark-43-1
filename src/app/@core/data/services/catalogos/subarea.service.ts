import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Subarea, SubareaData} from '../../interfaces/catalogos/subarea';
import {Observable} from 'rxjs';

@Injectable()
export class SubareaService extends SubareaData {

  private baseURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<Subarea[]> {
    return this.http.get<Subarea[]>(this.baseURL + 'v1/subarea');
  }

  post(subarea: Subarea) {
    return this.http.post(this.baseURL + 'v1/subarea', subarea);
  }

  posting(subarea: Subarea): Observable<Subarea> {
    return this.http.post<Subarea>(this.baseURL + 'v1/subarea', subarea);
  }

  getById(idSubarea): Observable<Subarea> {
    return this.http.get<Subarea>(this.baseURL + 'v1/subarea/' + idSubarea);
  }

  put(idSubArea, subarea: Subarea): Observable<Subarea> {
    return this.http.put<Subarea>(this.baseURL + 'v1/subarea/' + idSubArea, subarea);
  }

  findByIdArea(idArea): Observable<Subarea[]> {
    return this.http.get<Subarea[]>(this.baseURL + 'v1/subarea/find-by-id-area/' + idArea);
  }

}
