import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {PrecandidatosData} from '../../../../@core/data/interfaces/rrhh/precandidatos';
import swal from 'sweetalert';
import * as moment from 'moment';
import {CandidatoData} from '../../../../@core/data/interfaces/rrhh/candidatos';
import {CapacitacionData} from '../../../../@core/data/interfaces/rrhh/capacitacion';
import {SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-baja-precandidato',
  templateUrl: './baja-precandidato.component.html',
  styleUrls: ['./baja-precandidato.component.scss'],
})
export class BajaPrecandidatoComponent implements OnInit {

  /** Variable que almacena el id del precandidato obtenida. */
  @Input() idPrecandidato: number;
  /** Variable que almacena el id del motivo de baja obtenido. */
  @Input() idEstadoRh: number;

  @Input() idCapacitacion: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un banco. */
  bajaPreCandidatoForm: FormGroup;
  /** Variable que alamacena la fecha de baja. */
  fecha: any;
  /** Variable para conocer el número de día de la fecha de baja. */
  dia: number;
  //
  recontratable: any;
// Web Sockets
//   private stompClientBajaRH = null;
  /**El método constructor manda llamar a Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param  NbDialogRef<BajaPrecandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {PrecandidatosData} precandidatoService Servicio del componente precandidato para poder invocar los métodos.
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BajaPrecandidatoComponent>,
              protected capacitacionService: CapacitacionData, protected seguimientoService: SeguimientoData,
              protected precandidatoService: PrecandidatosData, protected candidatoService: CandidatoData) {
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  activoRecontratable() {
    if (this.bajaPreCandidatoForm.controls['recontratable'].value === 0 ||
      this.bajaPreCandidatoForm.controls['recontratable'].value === false) {
      this.recontratable = 1;
    } else {
      this.recontratable = 0;
    }
  }

  /** Función que permite verificar el formulario para asignar la fecha de baja, validando que sea un día laborable. */
  verificarFormulario() {
    // Creando variable de tipo Date para validar que el día introducido sea laborable
    if (this.bajaPreCandidatoForm.valid) {
      this.fecha = new Date(this.bajaPreCandidatoForm.value.fechaBaja);  // Obtiene la fecha del día anterior
      this.fecha.setDate(this.fecha.getDate()); // Le sumamos un día para que concuerde con la fecha ingresada
      this.dia = this.fecha.getDay();
      if (this.dia === 0 || this.dia === 6) { // Si el dia de baja es sábado o domingo, no debe permitir dar de baja
        return true;  // Botón deshabilitado
      } else {
        return false; // Botón habilitado
      }
    } else {
      return true;  // Botón deshabilitado
    }
  }

  /** Función que da de baja al precandidato. */
  bajaPrecandidato() {
    this.bajaPreCandidatoForm.value.idEstadoRH = this.idEstadoRh;
    this.bajaPreCandidatoForm.value.recontratable = this.recontratable;
    let fecha: any;
    fecha = moment(this.bajaPreCandidatoForm.value.fechaBaja).format('YYYY-MM-DD');
    let fechaBaja: any;
    fechaBaja = Date.parse(fecha + 'T01:00');
    this.precandidatoService.bajaPrecandidato(this.idPrecandidato, this.bajaPreCandidatoForm.value, fechaBaja).subscribe(result => {
      this.precandidatoService.postSocket(this.bajaPreCandidatoForm.value).subscribe(d => {});
      this.candidatoService.postSocket(this.bajaPreCandidatoForm.value).subscribe(d => {});
      this.capacitacionService.postSocket(this.bajaPreCandidatoForm.value).subscribe(x => {});
      this.seguimientoService.postSocketRolplay(this.bajaPreCandidatoForm.value).subscribe(x => {});
      this.seguimientoService.postSocketAignaciones(this.bajaPreCandidatoForm.value).subscribe(x => {});
      // this.stompClientBajaRH.send('/tasks/saveBajasRH', {}, JSON.stringify(this.bajaPreCandidatoForm.value));
      this.precandidatoService.postSocketBajaRH(this.bajaPreCandidatoForm.value).subscribe(x => {});
      swal({
        title: '¡Se ha dado de baja exitosamente!',
        icon: 'success',
        buttons: {
          confirm: {
            text: 'Aceptar',
            value: true,
            visible: true,
            className: '',
            closeModal: true,
          },
        },
      }).then((valor) => {
        this.ref.close();
      });
    });
  }

  ngOnInit() {
    // this.connect();
    this.bajaPreCandidatoForm = this.fb.group({
      'fechaBaja': new FormControl('', Validators.compose( [Validators.required])),
      'idEstadoRRHH': new FormControl(''),
      'recontratable': new FormControl(0, Validators.compose( [Validators.required])),
      'detalleBaja': new FormControl(''),
    });
  }

}
