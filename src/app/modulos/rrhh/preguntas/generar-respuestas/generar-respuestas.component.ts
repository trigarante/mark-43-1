import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AreasService} from '../../../../@core/data/services/rrhh/areas.service';
import {IconInterface} from '../../../../@core/data/interfaces/rrhh/icon-interface';
import {SedeData} from '../../../../@core/data/interfaces/catalogos/sede';
import {MatInput} from '@angular/material';

@Component({
  selector: 'ngx-generar-respuestas',
  templateUrl: './generar-respuestas.component.html',
  styleUrls: ['./generar-respuestas.component.scss'],
})

export class GenerarRespuestasComponent implements OnInit {
  @ViewChild('respuesta', {read: MatInput}) respuesta: MatInput;
  @ViewChild('sede', {read: MatInput}) sede: MatInput;
  preguntas: any;
  areas: any;
  respuestas: any;
  iconos: IconInterface[];
  icon: IconInterface;
  icon1: IconInterface;
  icon2: IconInterface;
  selectedIcon: boolean;
  sedes: any;
  respuestasForm: FormGroup;
  submitted = false;
  selectedSede: number;
  selectedArea: number;
  selectedPregunta: number;
  selectedColor: any;

  constructor(public areaService: AreasService, public areasServices: AreasService, public sedesService: SedeData,
              private formBuilder: FormBuilder) {
    this.iconos = this.areasServices.getIconos();
    this.sedesService.get().subscribe(s => {
      this.sedes = s;
    });
  }

  ngOnInit() {
    this.respuestasForm = this.formBuilder.group({
      sedeControl: ['', Validators.required],
      areaControl: ['', Validators.required],
      preguntaControl: ['', Validators.required],
      respuestaControl: ['', Validators.required],
      radioControl: ['', Validators.required],
      colorControl: ['', Validators.required],
    });
  }

  loadAreaBySede() {
    this.areaService.getAreasByIdSede(this.respuestasForm.controls['sedeControl'].value).subscribe(a => {
      this.areas = a;
    });
  }

  loadQuestionsByArea() {
    this.areaService.getPreguntasByArea(this.respuestasForm.controls['areaControl'].value).subscribe(p => {
      this.preguntas = p;
    });
  }

  agregarRes() {
    const respuestas = {
      id: 0,
      respuestas: this.respuestasForm.controls['respuestaControl'].value,
      icono: this.respuestasForm.controls['radioControl'].value,
      idPregunta: this.respuestasForm.controls['preguntaControl'].value,
      color: this.respuestasForm.controls['colorControl'].value,
      fechaCreacion: new Date(),

    };
    // ----------------------------filtro mayusculas implementar-------------------
    // this.apiService.addRespuestas(respuestas).subscribe(z => this.respuestas = z);
    this.areasServices.createRes(respuestas);
    this.reset();
  }
  loadRespuestasByPregunta() {
    // this.areasServices.getPreguntasByArea(this.respuestasForm.controls['preguntaControl'].value).subscribe(x => {
    //   this.preguntas = x;
    //
    // });
    /* carga respuestas */
    this.areaService.getRespuestaByPregunta(this.respuestasForm.controls['preguntaControl'].value).subscribe(r => {
      this.respuestas = r;
    });
  }

  reset() {
    this.respuesta.value = '';
    this.selectedSede = 0;
    this.selectedArea = 0;
    this.selectedPregunta = 0;
    this.selectedIcon = false;
    this.selectedColor = false;
    this.respuestasForm = this.formBuilder.group({
      sedeControl: ['', Validators.required],
      areaControl: ['', Validators.required],
      preguntaControl: ['', Validators.required],
      respuestaControl: ['', Validators.required],
      radioControl: ['', Validators.required],
      colorControl: ['', Validators.required],
    });
    this.respuestas = [];
  }

  onSubmit(agregarRes: void) {
    this.submitted = true;


    // stop here if form is invalid
    if (this.respuestasForm.invalid) {
      return;
    }
    this.sedes.reset();
    this.respuestasForm.controls['sedeControl'].setValue('mat-option-0');
    this.preguntas = null;
    this.areas = null;

    this.reset();
  }

  get f() {
    return this.respuestasForm.controls;
  }
}
