import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Partition from 'd3-hierarchy';
import * as d3Path from 'd3-path';
import * as d3Format from 'd3-format';
import * as d3Inter from 'd3-interpolate';
import 'd3-transition';
import * as moment from 'moment';
import {CandidatosService} from '../../../../@core/data/services/rrhh/candidatos.service';
import {Candidatos} from '../../../../@core/data/interfaces/rrhh/candidatos';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

@Component({
  selector: 'ngx-reporte-candidato',
  encapsulation: ViewEncapsulation.None,    // Necesaria para que se vea chidito. :v
  templateUrl: './reporte-candidato.component.html',
  styleUrls: ['./reporte-candidato.component.scss'],
  providers: [CandidatosService],
})
export class ReporteCandidatoComponent implements OnInit {
  title = 'Prueba';
  candidatos = [];
  flares: any;
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;
  private maxRadius: number;
  x;
  y;
  partition;
  formatNumber;
  root;
  private arc: any;
  private color: any;
  private svg: any;
  datas: any;
  cols: any[];
  etapas: any[];
  dates: any;
  label = 'TOTAL DE CANDIDATOS:'
  rangeDates: Date[];
  dataSource: any;

  constructor(private candidatosService: CandidatosService) {
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    // this.radius = Math.min(this.width, this.height) / 2;
    this.maxRadius = (Math.min(this.width, this.height) / 2) - 5;
    // this.cols = [
    //  {field: 'nombre', header: 'Nombre'},
    //  {field: 'telefonoMovil', header: 'Número de Teléfono'},
    //  {field: 'etapas[idEtapas]', header: 'Etapa'},
    //  {field: 'calificacionExamen', header: 'Calificacion Examen'},
    // ];
    this.cols = ['nombre', 'telefonoMovil', 'nombreEtapa', 'calificacionExamen'];
    this.etapas = ['Precandidato', 'Candidato', 'Empleado', 'Capacitación', 'Seguimiento 1', 'Seguimiento 2'];
    this.rangeDates = [
      new Date(2018, 11, 1, 0, 0),
      new Date(),
    ];
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.initSvg();
    this.getCandidatos(null);
  }
  applyFilterC(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  private initSvg() {
    const colors = ['#2d3a85', '#ff1456', '#006ebb'];
    this.formatNumber = d3Format.format(',d');
    this.color = d3Scale.scaleOrdinal(colors);
    // d3ScaleChromatic.schemeCategory10);
    this.x = d3Scale.scaleLinear()
      .range([0, 2 * Math.PI])
      .clamp(true);

    this.y = d3Scale.scaleSqrt()
      .range([this.maxRadius * .2, this.maxRadius]);

    this.partition = d3Partition.partition();

    this.arc = d3Shape.arc()
      .startAngle( d => this.x(d.x0))
      .endAngle(d => this.x(d.x1))
      .innerRadius(d => Math.max(0, this.y(d.y0)))
      .outerRadius(d => Math.max(0, this.y(d.y1)));
  }

  getCandidatos( rangeData: Date[] ) {
    this.candidatosService.get()
      .subscribe(data => {
        if ( rangeData == null ) {
          this.candidatos = data;
          this.dates = this.candidatos.length;
          this.drawPie(this.candidatos);
        } else {
          this.candidatos = [];

          // console.log(' FORMATO rangeData:: ' + moment(rangeData[1]).valueOf());
          // console.log('Fecha de servicio:: ' + moment(data[0].fechaCreacion).valueOf());

          for (let i = 0; i < data.length; i++) {
            // this.dates = data[i].fechaCreacion;
            if ( moment(data[i].fechaCreacion).valueOf() >= moment(rangeData[0]).valueOf()
                &&  moment(data[i].fechaCreacion).valueOf() <= moment(rangeData[1]).valueOf()) {
                this.candidatos.push(data[i]);
            }
          }

          this.removeSvg();
          this.drawPie(this.candidatos);
        }
      });
  }

  private drawPie(data) {
    this.root = d3Partition.hierarchy(this.dataTransform(data));
    this.root.sum(d => d.size);
    this.root.each(d => d.current = d);
    this.svg = d3.select('#graph')
      .append('svg')
      .style('width', '100%')
      .style('height', '100%')
      .attr('viewBox', `${-this.width / 2} ${-this.height / 2} ${this.width} ${this.height}`)
      .on('click', () => this.focusOn());
    const slice = this.svg.selectAll('g.slice')
      .data(this.partition(this.root).descendants());
    slice.exit().remove();
    const newSlice = slice.enter()
      .append('g').attr('class', 'slice')
      .on('click', d => {
        d3.event.stopPropagation();
        this.focusOn(d);
      });
    this.datas = this.dataFilter({data: this.flares});
    this.dataSource = new MatTableDataSource(this.dataFilter({data: this.flares}));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    newSlice.append('title')
      .text(d => d.data.name + '\n' + (d.value / this.datas.length) * 100 + '%');

    newSlice.append('path')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('class', 'main-arc')
      .attr('fill', d => { while (d.depth > 1) { d = d.parent; } return this.color(d.data.name); })
      .attr('fill-opacity', d => d.children ? 0.8 : 0.6 )
      .attr('d', this.arc);

    newSlice.append('path')
      .attr('class', 'hidden-arc')
      .attr('id', (_, i) => 'hiddenArc' + i)
      .attr('d', d => this.middleArcLine(d));

    const text = newSlice.append('text')
      .attr('display', d => this.textFits(d) ? null : 'none');

    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', d => { while (d.depth > 1) { d = d.parent; } return this.color(d.data.name); })
      .style('fill-opacity', 0.3);
    text.append('textPath')
      .attr('startOffset', '50%')
      .attr('xlink:href', (_, i) => '#hiddenArc' + i)
      .text(d => d.data.name)
      .style('fill', '#ffffff' );

    return this.dataSource;
  }

  private dataTransform(data: Candidatos[]) {
    this.flares = {
      name: 'Total de Candidatos: ' + data.length,
      id: 'candidatos',
      children: [],
    };
    const candidatosActivos = [];   // Candidatos Activos
    const caCandidato = [];
    const caEmpleado = [];
    const caCapacitacion = [];
    const caSeguimiento1 = [];
    const caSeguimiento2 = [];
    const caCandiCalificacionExcelente = [];
    const caCandiCalificacionBuena = [];
    const caCandiCalificacionMala = [];
    const caCandicalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caEmpleCalificacionExcelente = [];
    const caEmpleCalificacionBuena = [];
    const caEmpleCalificacionMala = [];
    const caEmplecalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caCapaCalificacionExcelente = [];
    const caCapaCalificacionBuena = [];
    const caCapaCalificacionMala = [];
    const caCapacalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caSegui1CalificacionExcelente = [];
    const caSegui1CalificacionBuena = [];
    const caSegui1CalificacionMala = [];
    const caSegui1calf = [[], [], [], [], [], [], [], [], [], [], []];
    const caSegui2CalificacionExcelente = [];
    const caSegui2CalificacionBuena = [];
    const caSegui2CalificacionMala = [];
    const caSegui2calf = [[], [], [], [], [], [], [], [], [], [], []];

    const candidatosBaja = [];    // Candidatos Baja
    const cbCandidato = [];
    const cbEmpleado = [];
    const cbCapacitacion = [];
    const cbSeguimiento1 = [];
    const cbSeguimiento2 = [];
    const cbCandiCalificacionExcelente = [];
    const cbCandiCalificacionBuena = [];
    const cbCandiCalificacionMala = [];
    const cbCandicalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbEmpleCalificacionExcelente = [];
    const cbEmpleCalificacionBuena = [];
    const cbEmpleCalificacionMala = [];
    const cbEmplecalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbCapaCalificacionExcelente = [];
    const cbCapaCalificacionBuena = [];
    const cbCapaCalificacionMala = [];
    const cbCapacalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbSegui1CalificacionExcelente = [];
    const cbSegui1CalificacionBuena = [];
    const cbSegui1CalificacionMala = [];
    const cbSegui1calf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbSegui2CalificacionExcelente = [];
    const cbSegui2CalificacionBuena = [];
    const cbSegui2CalificacionMala = [];
    const cbSegui2calf = [[], [], [], [], [], [], [], [], [], [], []];

    // const candidatos
    for (let i = 0; i < data.length ; i++) {
      if (data[i].idEstadoRH === 1) {   ///////////////////// Candidatos Activos
        candidatosActivos.push(data[i]);
        if (data[i].idEtapa === 2) {   // Candidato
          caCandidato.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caCandiCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caCandiCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caCandiCalificacionMala.push(data[i]);
          }
          caCandicalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 3) {   // Empleado
          caEmpleado.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caEmpleCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caEmpleCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caEmpleCalificacionMala.push(data[i]);
          }
          caEmplecalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 4) {   // Capacitacion
          caCapacitacion.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caCapaCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caCapaCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caCapaCalificacionMala.push(data[i]);
          }
          caCapacalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 5) {   // Seguimiento 1
          caSeguimiento1.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caSegui1CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caSegui1CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caSegui1CalificacionMala.push(data[i]);
          }
          caSegui1calf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 6) {   // Seguimiento 2
          caSeguimiento2.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caSegui2CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caSegui2CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caSegui2CalificacionMala.push(data[i]);
          }
          caSegui2calf[data[i].calificacionExamen].push(data[i]);
        }
      } else if (data[i].idEstadoRH === 2) {   ///////////////////// Candidatos Baja
        candidatosBaja.push(data[i]);
        if (data[i].idEtapa === 2) {   // Candidato
          cbCandidato.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbCandiCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbCandiCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbCandiCalificacionMala.push(data[i]);
          }
          cbCandicalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 3) {   // Empleado
          cbEmpleado.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbEmpleCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbEmpleCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbEmpleCalificacionMala.push(data[i]);
          }
          cbEmplecalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 4) {   // Cbpacitacion
          cbCapacitacion.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbCapaCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbCapaCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbCapaCalificacionMala.push(data[i]);
          }
          cbCapacalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 5) {   // Seguimiento 1
          cbSeguimiento1.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbSegui1CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbSegui1CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbSegui1CalificacionMala.push(data[i]);
          }
          cbSegui1calf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 6) {   // Seguimiento 2
          cbSeguimiento2.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbSegui2CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbSegui2CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbSegui2CalificacionMala.push(data[i]);
          }
          cbSegui2calf[data[i].calificacionExamen].push(data[i]);
        }
      }
    }

    const activos = {
      name: 'Total de Activos: ' + candidatosActivos.length,
      id: '1',
      children: [
        {
          name: 'Total de Candidatos: ' + caCandidato.length,
          id: '1.2',
          children: [
            {
              name: 'Total de Exclente: ' + caCapaCalificacionExcelente.length,
              id: '1.2.Exc',
              children: [
                {
                  name: '10',
                  id: '1.2.Exc.10',
                  size: caCandicalf[10].length,
                },
                {
                  name: '9',
                  id: '1.2.Exc.9',
                  size: caCandicalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + caCandiCalificacionBuena.length,
              id: '1.2.Bue',
              children: [
                {
                  name: '8',
                  id: '1.2.Bue.8',
                  size: caCandicalf[8].length,
                },
                {
                  name: '7',
                  id: '1.2.Bue.7',
                  size: caCandicalf[7].length,
                },
                {
                  name: '6',
                  id: '1.2.Bue.6',
                  size: caCandicalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + caCandiCalificacionMala.length,
              id: '1.2.Mal',
              children: [
                {
                  name: '5',
                  id: '1.2.Mal.5',
                  size: caCandicalf[5].length,
                },
                {
                  name: '4',
                  id: '1.2.Mal.4',
                  size: caCandicalf[4].length,
                },
                {
                  name: '3',
                  id: '1.2.Mal.3',
                  size: caCandicalf[3].length,
                },
                {
                  name: '2',
                  id: '1.2.Mal.2',
                  size: caCandicalf[2].length,
                },
                {
                  name: '1',
                  id: '1.2.Mal.1',
                  size: caCandicalf[1].length,
                },
                {
                  name: '0',
                  id: '1.2.Mal.0',
                  size: caCandicalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Empleados: ' + caEmpleado.length,
          id: '1.3',
          children: [
            {
              name: 'Total de Exclente: ' + caEmpleCalificacionExcelente.length,
              id: '1.3.Exc',
              children: [
                {
                  name: '10',
                  id: '1.3.Exc.10',
                  size: caEmplecalf[10].length,
                },
                {
                  name: '9',
                  id: '1.3.Exc.9',
                  size: caEmplecalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + caEmpleCalificacionBuena.length,
              id: '1.3.Bue',
              children: [
                {
                  name: '8',
                  id: '1.3.Bue.8',
                  size: caEmplecalf[8].length,
                },
                {
                  name: '7',
                  id: '1.3.Bue.7',
                  size: caEmplecalf[7].length,
                },
                {
                  name: '6',
                  id: '1.3.Bue.6',
                  size: caEmplecalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + caEmpleCalificacionMala.length,
              id: '1.3.Mal',
              children: [
                {
                  name: '5',
                  id: '1.3.Mal.5',
                  size: caEmplecalf[5].length,
                },
                {
                  name: '4',
                  id: '1.3.Mal.4',
                  size: caEmplecalf[4].length,
                },
                {
                  name: '3',
                  id: '1.3.Mal.3',
                  size: caEmplecalf[3].length,
                },
                {
                  name: '2',
                  id: '1.3.Mal.2',
                  size: caEmplecalf[2].length,
                },
                {
                  name: '1',
                  id: '1.3.Mal.1',
                  size: caEmplecalf[1].length,
                },
                {
                  name: '0',
                  id: '1.3.Mal.0',
                  size: caEmplecalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Capacitación: ' + caCapacitacion.length,
          id: '1.4',
          children: [
            {
              name: 'Total de Exclente: ' + caCapaCalificacionExcelente.length,
              id: '1.4.Exc',
              children: [
                {
                  name: '10',
                  id: '1.4.Exc.10',
                  size: caCapacalf[10].length,
                },
                {
                  name: '9',
                  id: '1.4.Exc.9',
                  size: caCapacalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + caCapaCalificacionBuena.length,
              id: '1.4.Bue',
              children: [
                {
                  name: '8',
                  id: '1.4.Bue.8',
                  size: caCapacalf[8].length,
                },
                {
                  name: '7',
                  id: '1.4.Bue.7',
                  size: caCapacalf[7].length,
                },
                {
                  name: '6',
                  id: '1.4.Bue.6',
                  size: caCapacalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + caCapaCalificacionMala.length,
              id: '1.4.Mal',
              children: [
                {
                  name: '5',
                  id: '1.4.Mal.5',
                  size: caCapacalf[5].length,
                },
                {
                  name: '4',
                  id: '1.4.Mal.4',
                  size: caCapacalf[4].length,
                },
                {
                  name: '3',
                  id: '1.4.Mal.3',
                  size: caCapacalf[3].length,
                },
                {
                  name: '2',
                  id: '1.4.Mal.2',
                  size: caCapacalf[2].length,
                },
                {
                  name: '1',
                  id: '1.4.Mal.1',
                  size: caCapacalf[1].length,
                },
                {
                  name: '0',
                  id: '1.4.Mal.0',
                  size: caCapacalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Seguimiento 1: ' + caSeguimiento1.length,
          id: '1.5',
          children: [
            {
              name: 'Total de Exclente: ' + caSegui1CalificacionExcelente.length,
              id: '1.5.Exc',
              children: [
                {
                  name: '10',
                  id: '1.5.Exc.10',
                  size: caSegui1calf[10].length,
                },
                {
                  name: '9',
                  id: '1.5.Exc.9',
                  size: caSegui1calf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + caSegui1CalificacionBuena.length,
              id: '1.5.Bue',
              children: [
                {
                  name: '8',
                  id: '1.5.Bue.8',
                  size: caSegui1calf[8].length,
                },
                {
                  name: '7',
                  id: '1.5.Bue.7',
                  size: caSegui1calf[7].length,
                },
                {
                  name: '6',
                  id: '1.5.Bue.6',
                  size: caSegui1calf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + caSegui1CalificacionMala.length,
              id: '1.5.Mal',
              children: [
                {
                  name: '5',
                  id: '1.5.Mal.5',
                  size: caSegui1calf[5].length,
                },
                {
                  name: '4',
                  id: '1.5.Mal.4',
                  size: caSegui1calf[4].length,
                },
                {
                  name: '3',
                  id: '1.5.Mal.3',
                  size: caSegui1calf[3].length,
                },
                {
                  name: '2',
                  id: '1.5.Mal.2',
                  size: caSegui1calf[2].length,
                },
                {
                  name: '1',
                  id: '1.5.Mal.1',
                  size: caSegui1calf[1].length,
                },
                {
                  name: '0',
                  id: '1.5.Mal.0',
                  size: caSegui1calf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Seguimiento 2: ' + caSeguimiento2.length,
          id: '1.6',
          children: [
            {
              name: 'Total de Exclente: ' + caSegui2CalificacionExcelente.length,
              id: '1.6.Exc',
              children: [
                {
                  name: '10',
                  id: '1.6.Exc.10',
                  size: caSegui2calf[10].length,
                },
                {
                  name: '9',
                  id: '1.6.Exc.9',
                  size: caSegui2calf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + caSegui2CalificacionExcelente.length,
              id: '1.6.Bue',
              children: [
                {
                  name: '8',
                  id: '1.6.Bue.8',
                  size: caSegui2calf[8].length,
                },
                {
                  name: '7',
                  id: '1.6.Bue.7',
                  size: caSegui2calf[7].length,
                },
                {
                  name: '6',
                  id: '1.6.Bue.6',
                  size: caSegui2calf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + caSegui2CalificacionMala.length,
              id: '1.6.Mal',
              children: [
                {
                  name: '5',
                  id: '1.6.Mal.5',
                  size: caSegui2calf[5].length,
                },
                {
                  name: '4',
                  id: '1.6.Mal.4',
                  size: caSegui2calf[4].length,
                },
                {
                  name: '3',
                  id: '1.6.Mal.3',
                  size: caSegui2calf[3].length,
                },
                {
                  name: '2',
                  id: '1.6.Mal.2',
                  size: caSegui2calf[2].length,
                },
                {
                  name: '1',
                  id: '1.6.Mal.1',
                  size: caSegui2calf[1].length,
                },
                {
                  name: '0',
                  id: '1.6.Mal.0',
                  size: caSegui2calf[0].length,
                },
              ],
            },
          ],
        },
      ],
    };

    const bajas = {
      name: 'Total de Bajas: ' + candidatosBaja.length,
      id: '2',
      children: [
        {
          name: 'Total de Candidatos: ' + cbCandidato.length,
          id: '2.2',
          children: [
            {
              name: 'Total de Excelente: ' + cbCandiCalificacionExcelente.length,
              id: '2.2.Exc',
              children: [
                {
                  name: '10',
                  id: '2.2.Exc.10',
                  size: cbCandicalf[10].length,
                },
                {
                  name: '9',
                  id: '2.2.Exc.9',
                  size: cbCandicalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + cbCandiCalificacionBuena.length,
              id: '2.2.Bue',
              children: [
                {
                  name: '8',
                  id: '2.2.Bue.8',
                  size: cbCandicalf[8].length,
                },
                {
                  name: '7',
                  id: '2.2.Bue.7',
                  size: cbCandicalf[7].length,
                },
                {
                  name: '6',
                  id: '2.2.Bue.6',
                  size: cbCandicalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + cbCandiCalificacionMala.length,
              id: '2.2.Mal',
              children: [
                {
                  name: '5',
                  id: '2.2.Mal.5',
                  size: cbCandicalf[5].length,
                },
                {
                  name: '4',
                  id: '2.2.Mal.4',
                  size: cbCandicalf[4].length,
                },
                {
                  name: '3',
                  id: '2.2.Mal.3',
                  size: cbCandicalf[3].length,
                },
                {
                  name: '2',
                  id: '2.2.Mal.2',
                  size: cbCandicalf[2].length,
                },
                {
                  name: '1',
                  id: '2.2.Mal.1',
                  size: cbCandicalf[1].length,
                },
                {
                  name: '0',
                  id: '2.2.Mal.0',
                  size: cbCandicalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Empleados: ' + cbEmpleado.length,
          id: '2.3',
          children: [
            {
              name: 'Total de Exclente: ' + cbEmpleCalificacionExcelente.length,
              id: '2.3.Exc',
              children: [
                {
                  name: '10',
                  id: '2.3.Exc.10',
                  size: cbEmplecalf[10].length,
                },
                {
                  name: '9',
                  id: '2.3.Exc.9',
                  size: cbEmplecalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + cbEmpleCalificacionBuena.length,
              id: '2.3.Bue',
              children: [
                {
                  name: '8',
                  id: '2.3.Bue.8',
                  size: cbEmplecalf[8].length,
                },
                {
                  name: '7',
                  id: '2.3.Bue.7',
                  size: cbEmplecalf[7].length,
                },
                {
                  name: '6',
                  id: '2.3.Bue.6',
                  size: cbEmplecalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + cbEmpleCalificacionMala.length,
              id: '2.3.Mal',
              children: [
                {
                  name: '5',
                  id: '2.3.Mal.5',
                  size: cbEmplecalf[5].length,
                },
                {
                  name: '4',
                  id: '2.3.Mal.4',
                  size: cbEmplecalf[4].length,
                },
                {
                  name: '3',
                  id: '2.3.Mal.3',
                  size: cbEmplecalf[3].length,
                },
                {
                  name: '2',
                  id: '2.3.Mal.2',
                  size: cbEmplecalf[2].length,
                },
                {
                  name: '1',
                  id: '2.3.Mal.1',
                  size: cbEmplecalf[1].length,
                },
                {
                  name: '0',
                  id: '2.3.Mal.0',
                  size: cbEmplecalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Capacitación: ' + cbCapacitacion.length,
          id: '2.4',
          children: [
            {
              name: 'Total de Exclente: ' + cbCapaCalificacionExcelente.length,
              id: '2.4.Exc',
              children: [
                {
                  name: '10',
                  id: '2.4.Exc.10',
                  size: cbCapacalf[10].length,
                },
                {
                  name: '9',
                  id: '2.4.Exc.9',
                  size: cbCapacalf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + cbCapaCalificacionBuena.length,
              id: '2.4.Bue',
              children: [
                {
                  name: '8',
                  id: '2.4.Bue.8',
                  size: cbCapacalf[8].length,
                },
                {
                  name: '7',
                  id: '2.4.Bue.7',
                  size: cbCapacalf[7].length,
                },
                {
                  name: '6',
                  id: '2.4.Bue.6',
                  size: cbCapacalf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + cbCapaCalificacionMala.length,
              id: '2.4.Mal',
              children: [
                {
                  name: '5',
                  id: '2.4.Mal.5',
                  size: cbCapacalf[5].length,
                },
                {
                  name: '4',
                  id: '2.4.Mal.4',
                  size: cbCapacalf[4].length,
                },
                {
                  name: '3',
                  id: '2.4.Mal.3',
                  size: cbCapacalf[3].length,
                },
                {
                  name: '2',
                  id: '2.4.Mal.2',
                  size: cbCapacalf[2].length,
                },
                {
                  name: '1',
                  id: '2.4.Mal.1',
                  size: cbCapacalf[1].length,
                },
                {
                  name: '0',
                  id: '2.4.Mal.0',
                  size: cbCapacalf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Seguimiento 1: ' + cbSeguimiento1.length,
          id: '2.5',
          children: [
            {
              name: 'Total de Exclente: ' + cbSegui1CalificacionExcelente.length,
              id: '2.5.Exc',
              children: [
                {
                  name: '10',
                  id: '2.5.Exc.10',
                  size: cbSegui1calf[10].length,
                },
                {
                  name: '9',
                  id: '2.5.Exc.9',
                  size: cbSegui1calf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + cbSegui1CalificacionBuena.length,
              id: '2.5.Bue',
              children: [
                {
                  name: '8',
                  id: '2.5.Bue.8',
                  size: cbSegui1calf[8].length,
                },
                {
                  name: '7',
                  id: '2.5.Bue.7',
                  size: cbSegui1calf[7].length,
                },
                {
                  name: '6',
                  id: '2.5.Bue.6',
                  size: cbSegui1calf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + cbSegui1CalificacionMala.length,
              id: '2.5.Mal',
              children: [
                {
                  name: '5',
                  id: '2.5.Mal.5',
                  size: cbSegui1calf[5].length,
                },
                {
                  name: '4',
                  id: '2.5.Mal.4',
                  size: cbSegui1calf[4].length,
                },
                {
                  name: '3',
                  id: '2.5.Mal.3',
                  size: cbSegui1calf[3].length,
                },
                {
                  name: '2',
                  id: '2.5.Mal.2',
                  size: cbSegui1calf[2].length,
                },
                {
                  name: '1',
                  id: '2.5.Mal.1',
                  size: cbSegui1calf[1].length,
                },
                {
                  name: '0',
                  id: '2.5.Mal.0',
                  size: cbSegui1calf[0].length,
                },
              ],
            },
          ],
        },
        {
          name: 'Total de Seguimiento 2: ' + cbSeguimiento2.length,
          id: '2.6',
          children: [
            {
              name: 'Total de Exclente: ' + cbSegui2CalificacionExcelente.length,
              id: '2.6.Exc',
              children: [
                {
                  name: '10',
                  id: '2.6.Exc.10',
                  size: cbSegui2calf[10].length,
                },
                {
                  name: '9',
                  id: '2.6.Exc.9',
                  size: cbSegui2calf[9].length,
                },
              ],
            },
            {
              name: 'Total de Buena: ' + cbSegui2CalificacionBuena.length,
              id: '2.6.Bue',
              children: [
                {
                  name: '8',
                  id: '2.6.Bue.8',
                  size: cbSegui2calf[8].length,
                },
                {
                  name: '7',
                  id: '2.6.Bue.7',
                  size: cbSegui2calf[7].length,
                },
                {
                  name: '6',
                  id: '2.6.Bue.6',
                  size: cbSegui2calf[6].length,
                },
              ],
            },
            {
              name: 'Total de Mala: ' + cbSegui2CalificacionMala.length,
              id: '2.6.Mal',
              children: [
                {
                  name: '5',
                  id: '2.6.Mal.5',
                  size: cbSegui2calf[5].length,
                },
                {
                  name: '4',
                  id: '2.6.Mal.4',
                  size: cbSegui2calf[4].length,
                },
                {
                  name: '3',
                  id: '2.6.Mal.3',
                  size: cbSegui2calf[3].length,
                },
                {
                  name: '2',
                  id: '2.6.Mal.2',
                  size: cbSegui2calf[2].length,
                },
                {
                  name: '1',
                  id: '2.6.Mal.1',
                  size: cbSegui2calf[1].length,
                },
                {
                  name: '0',
                  id: '2.6.Mal.0',
                  size: cbSegui2calf[0].length,
                },
              ],
            },
          ],
        },
      ],
    };

    this.flares.children.push(activos);
    this.flares.children.push(bajas);
    return this.flares;
  }

  private focusOn(d = { x0: 0, x1: 1, y0: 0, y1: 1 }) {
    const transition = this.svg.transition()
      .duration(750)
      .tween('scale', () => {
        const xd = d3Inter.interpolate(this.x.domain(), [d.x0, d.x1]),
          yd = d3Inter.interpolate(this.y.domain(), [d.y0, 1]);
        return t => { this.x.domain(xd(t)); this.y.domain(yd(t)); };
      });

    transition.selectAll('path.main-arc')
      .attrTween('d', d => () => this.arc(d));

    transition.selectAll('path.hidden-arc')
      .attrTween('d', d => () => this.middleArcLine(d));

    transition.selectAll('text')
      .attrTween('display', d => () => this.textFits(d) ? null : 'none');
    this.dataSource = new MatTableDataSource(this.dataFilter(d));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private dataFilter(d: any) {
    const data = this.candidatos;
    const candidatosActivos = [];   // Candidatos Activos
    const caCandidato = [];
    const caEmpleado = [];
    const caCapacitacion = [];
    const caSeguimiento1 = [];
    const caSeguimiento2 = [];
    const caCandiCalificacionExcelente = [];
    const caCandiCalificacionBuena = [];
    const caCandiCalificacionMala = [];
    const caCandicalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caEmpleCalificacionExcelente = [];
    const caEmpleCalificacionBuena = [];
    const caEmpleCalificacionMala = [];
    const caEmplecalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caCapaCalificacionExcelente = [];
    const caCapaCalificacionBuena = [];
    const caCapaCalificacionMala = [];
    const caCapacalf = [[], [], [], [], [], [], [], [], [], [], []];
    const caSegui1CalificacionExcelente = [];
    const caSegui1CalificacionBuena = [];
    const caSegui1CalificacionMala = [];
    const caSegui1calf = [[], [], [], [], [], [], [], [], [], [], []];
    const caSegui2CalificacionExcelente = [];
    const caSegui2CalificacionBuena = [];
    const caSegui2CalificacionMala = [];
    const caSegui2calf = [[], [], [], [], [], [], [], [], [], [], []];

    const candidatosBaja = [];    // Candidatos Baja
    const cbCandidato = [];
    const cbEmpleado = [];
    const cbCapacitacion = [];
    const cbSeguimiento1 = [];
    const cbSeguimiento2 = [];
    const cbCandiCalificacionExcelente = [];
    const cbCandiCalificacionBuena = [];
    const cbCandiCalificacionMala = [];
    const cbCandicalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbEmpleCalificacionExcelente = [];
    const cbEmpleCalificacionBuena = [];
    const cbEmpleCalificacionMala = [];
    const cbEmplecalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbCapaCalificacionExcelente = [];
    const cbCapaCalificacionBuena = [];
    const cbCapaCalificacionMala = [];
    const cbCapacalf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbSegui1CalificacionExcelente = [];
    const cbSegui1CalificacionBuena = [];
    const cbSegui1CalificacionMala = [];
    const cbSegui1calf = [[], [], [], [], [], [], [], [], [], [], []];
    const cbSegui2CalificacionExcelente = [];
    const cbSegui2CalificacionBuena = [];
    const cbSegui2CalificacionMala = [];
    const cbSegui2calf = [[], [], [], [], [], [], [], [], [], [], []];

    // const candidatos
    for (let i = 0; i < data.length ; i++) {
      if (data[i].idEstadoRH === 1) {   ///////////////////// Candidatos Activos
        candidatosActivos.push(data[i]);
        if (data[i].idEtapa === 2) {   // Candidato
          caCandidato.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caCandiCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caCandiCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caCandiCalificacionMala.push(data[i]);
          }
          caCandicalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 3) {   // Empleado
          caEmpleado.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caEmpleCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caEmpleCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caEmpleCalificacionMala.push(data[i]);
          }
          caEmplecalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 4) {   // Capacitacion
          caCapacitacion.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caCapaCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caCapaCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caCapaCalificacionMala.push(data[i]);
          }
          caCapacalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 5) {   // Seguimiento 1
          caSeguimiento1.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caSegui1CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caSegui1CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caSegui1CalificacionMala.push(data[i]);
          }
          caSegui1calf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 6) {   // Seguimiento 2
          caSeguimiento2.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            caSegui2CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            caSegui2CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            caSegui2CalificacionMala.push(data[i]);
          }
          caSegui2calf[data[i].calificacionExamen].push(data[i]);
        }
      } else if (data[i].idEstadoRH === 2) {   ///////////////////// Candidatos Baja
        candidatosBaja.push(data[i]);
        if (data[i].idEtapa === 2) {   // Candidato
          cbCandidato.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbCandiCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbCandiCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbCandiCalificacionMala.push(data[i]);
          }
          cbCandicalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 3) {   // Empleado
          cbEmpleado.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbEmpleCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbEmpleCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbEmpleCalificacionMala.push(data[i]);
          }
          cbEmplecalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 4) {   // Cbpacitacion
          cbCapacitacion.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbCapaCalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbCapaCalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbCapaCalificacionMala.push(data[i]);
          }
          cbCapacalf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 5) {   // Seguimiento 1
          cbSeguimiento1.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbSegui1CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbSegui1CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbSegui1CalificacionMala.push(data[i]);
          }
          cbSegui1calf[data[i].calificacionExamen].push(data[i]);
        } else if (data[i].idEtapa === 6) {   // Seguimiento 2
          cbSeguimiento2.push(data[i]);
          if (data[i].calificacionExamen > 8) {
            cbSegui2CalificacionExcelente.push(data[i]);
          } else if (data[i].calificacionExamen > 5) {
            cbSegui2CalificacionBuena.push(data[i]);
          } else if (data[i].calificacionExamen < 6) {
            cbSegui2CalificacionMala.push(data[i]);
          }
          cbSegui2calf[data[i].calificacionExamen].push(data[i]);
        }
      }
    }

    if (d.data.id === 'candidatos') {
      this.label = 'TOTAL DE CANDIDATOS:';
      this.dates = data.length;
      return data;
    } else if (d.data.id === '1') {   ///////////// Activos
      this.label = 'TOTAL DE ACTIVOS:';
      this.dates = candidatosActivos.length;
      return candidatosActivos;
    } else if (d.data.id === '1.2') {   // Candidatos
      this.label = 'TOTAL EN CANDIDATO:';
      this.dates = caCandidato.length;
      return caCandidato;
    } else if (d.data.id === '1.2.Exc') {
      this.label = 'TOTAL CALIFCACIÓN EXCELENTE:';
      this.dates = caCandiCalificacionExcelente.length;
      return caCandiCalificacionExcelente;
    } else if (d.data.id === '1.2.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = caCapacalf[10].length;
      return caCapacalf[10];
    } else if (d.data.id === '1.2.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = caCandicalf[9].length;
      return caCandicalf[9];
    } else if (d.data.id === '1.2.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = caCandiCalificacionBuena.length;
      return caCandiCalificacionBuena;
    } else if (d.data.id === '1.2.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = caCandicalf[8].length;
      return caCandicalf[8];
    } else if (d.data.id === '1.2.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = caCandicalf[7].length;
      return caCandicalf[7];
    } else if (d.data.id === '1.2.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = caCandicalf[6].length;
      return caCandicalf[6];
    } else if (d.data.id === '1.2.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = caCandiCalificacionMala.length;
      return caCandiCalificacionMala;
    } else if (d.data.id === '1.2.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = caCandicalf[5].length;
      return caCandicalf[5];
    } else if (d.data.id === '1.2.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = caCandicalf[4].length;
      return caCandicalf[4];
    } else if (d.data.id === '1.2.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = caCandicalf[3].length;
      return caCandicalf[3];
    } else if (d.data.id === '1.2.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = caCandicalf[2].length;
      return caCandicalf[2];
    } else if (d.data.id === '1.2.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = caCandicalf[1].length;
      return caCandicalf[1];
    } else if (d.data.id === '1.2.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = caCandicalf[0].length;
      return caCandicalf[0];
    } else if (d.data.id === '1.3') {    // Empleados
      this.label = 'TOTAL EN EMPLEADOS:';
      this.dates = caEmpleado.length;
      return caEmpleado;
    } else if (d.data.id === '1.3.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:'
      this.dates = caEmpleCalificacionExcelente.length;
      return caEmpleCalificacionExcelente;
    } else if (d.data.id === '1.3.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = caEmplecalf[10].length;
      return caEmplecalf[10];
    } else if (d.data.id === '1.3.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = caEmplecalf[9].length;
      return caEmplecalf[9];
    } else if (d.data.id === '1.3.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = caEmpleCalificacionBuena.length;
      return caEmpleCalificacionBuena;
    } else if (d.data.id === '1.3.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = caEmplecalf[8].length;
      return caEmplecalf[8];
    } else if (d.data.id === '1.3.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = caEmplecalf[7].length;
      return caEmplecalf[7];
    } else if (d.data.id === '1.3.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = caEmplecalf[6].length;
      return caEmplecalf[6];
    } else if (d.data.id === '1.3.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = caEmpleCalificacionMala.length;
      return caEmpleCalificacionMala;
    } else if (d.data.id === '1.3.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = caEmplecalf[5].length;
      return caEmplecalf[5];
    } else if (d.data.id === '1.3.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = caEmplecalf[4].length;
      return caEmplecalf[4];
    } else if (d.data.id === '1.3.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = caEmplecalf[3].length;
      return caEmplecalf[3];
    } else if (d.data.id === '1.3.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = caEmplecalf[2].length;
      return caEmplecalf[2];
    } else if (d.data.id === '1.3.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = caEmplecalf[1].length;
      return caEmplecalf[1];
    } else if (d.data.id === '1.3.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = caEmplecalf[0].length;
      return caEmplecalf[0];
    } else if (d.data.id === '1.4') {    // Capacitación
      this.label = 'TOTAL EN CAPACITACIÓN:';
      this.dates = caCapacitacion.length;
      return caCapacitacion;
    } else if (d.data.id === '1.4.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = caCapaCalificacionExcelente.length;
      return caCapaCalificacionExcelente;
    } else if (d.data.id === '1.4.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = caCapacalf[10].length;
      return caCapacalf[10];
    } else if (d.data.id === '1.4.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = caCapacalf[9].length;
      return caCapacalf[9];
    } else if (d.data.id === '1.4.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = caCapaCalificacionBuena.length;
      return caCapaCalificacionBuena;
    } else if (d.data.id === '1.4.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = caCapacalf[8].length;
      return caCapacalf[8];
    } else if (d.data.id === '1.4.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = caCapacalf[7].length;
      return caCapacalf[7];
    } else if (d.data.id === '1.4.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = caCapacalf[6].length;
      return caCapacalf[6];
    } else if (d.data.id === '1.4.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = caCapaCalificacionMala.length;
      return caCapaCalificacionMala;
    } else if (d.data.id === '1.4.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = caCapacalf[5].length;
      return caCapacalf[5];
    } else if (d.data.id === '1.4.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = caCapacalf[4].length;
      return caCapacalf[4];
    } else if (d.data.id === '1.4.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = caCapacalf[3].length;
      return caCapacalf[3];
    } else if (d.data.id === '1.4.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = caCapacalf[2].length;
      return caCapacalf[2];
    } else if (d.data.id === '1.4.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = caCapacalf[1].length;
      return caCapacalf[1];
    } else if (d.data.id === '1.4.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = caCapacalf[0].length;
      return caCapacalf[0];
    } else if (d.data.id === '1.5') {    // Seguimiento 1
      this.label = 'TOTAL EN SEGUIMIENTO 1:';
      this.dates = caSeguimiento1.length;
      return caSeguimiento1;
    } else if (d.data.id === '1.5.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = caSegui1CalificacionExcelente.length;
      return caSegui1CalificacionExcelente;
    } else if (d.data.id === '1.5.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = caSegui1calf[10].length;
      return caSegui1calf[10];
    } else if (d.data.id === '1.5.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = caSegui1calf[9].length;
      return caSegui1calf[9];
    } else if (d.data.id === '1.5.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = caSegui1CalificacionBuena.length;
      return caSegui1CalificacionBuena;
    } else if (d.data.id === '1.5.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = caSegui1calf[8].length;
      return caSegui1calf[8];
    } else if (d.data.id === '1.5.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = caSegui1calf[7].length;
      return caSegui1calf[7];
    } else if (d.data.id === '1.5.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = caSegui1calf[6].length;
      return caSegui1calf[6];
    } else if (d.data.id === '1.5.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = caSegui1CalificacionMala.length;
      return caSegui1CalificacionMala;
    } else if (d.data.id === '1.5.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = caSegui1calf[5];
      return caSegui1calf[5];
    } else if (d.data.id === '1.5.Mal.4') {
      this.label =  'TOTAl EN 4:';
      this.dates = caSegui1calf[4].length;
      return caSegui1calf[4];
    } else if (d.data.id === '1.5.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = caSegui1calf[3].length;
      return caSegui1calf[3];
    } else if (d.data.id === '1.5.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = caSegui1calf[2].length;
      return caSegui1calf[2];
    } else if (d.data.id === '1.5.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = caSegui1calf[1].length;
      return caSegui1calf[1];
    } else if (d.data.id === '1.5.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = caSegui1calf[0].length;
      return caSegui1calf[0];
    } else if (d.data.id === '1.6') {    // Seguimiento 2
      this.label = 'TOTAL EN SEGUIMIENTO 2:';
      this.dates = caSeguimiento2.length;
      return caSeguimiento2;
    } else if (d.data.id === '1.6.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = caSegui2CalificacionExcelente.length;
      return caSegui2CalificacionExcelente;
    } else if (d.data.id === '1.6.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = caSegui2calf[10].length;
      return caSegui2calf[10];
    } else if (d.data.id === '1.6.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = caSegui2calf[9].length;
      return caSegui2calf[9];
    } else if (d.data.id === '1.6.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = caSegui2CalificacionBuena.length;
      return caSegui2CalificacionBuena;
    } else if (d.data.id === '1.6.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = caSegui2calf[8].length;
      return caSegui2calf[8];
    } else if (d.data.id === '1.6.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = caSegui2calf[7].length;
      return caSegui2calf[7];
    } else if (d.data.id === '1.6.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = caSegui2calf[6].length;
      return caSegui2calf[6];
    } else if (d.data.id === '1.6.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = caSegui2CalificacionMala.length;
      return caSegui2CalificacionMala;
    } else if (d.data.id === '1.6.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = caSegui2calf[5].length;
      return caSegui2calf[5];
    } else if (d.data.id === '1.6.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = caSegui2calf[4].length;
      return caSegui2calf[4];
    } else if (d.data.id === '1.6.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = caSegui2calf[3].length;
      return caSegui2calf[3];
    } else if (d.data.id === '1.6.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = caSegui2calf[2].length;
      return caSegui2calf[2];
    } else if (d.data.id === '1.6.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = caSegui2calf[1].length;
      return caSegui2calf[1];
    } else if (d.data.id === '1.6.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = caSegui2calf[0].length;
      return caSegui2calf[0];
    } else if (d.data.id === '2') {   ///////////// Baja
      this.label = 'TOTAL DE BAJAS:';
      this.dates = candidatosBaja.length;
      return candidatosBaja;
    } else if (d.data.id === '2.2') {   // Candidatos
      this.label = 'TOTAN EN CANDIDATOS:';
      this.dates = cbCandidato.length;
      return cbCandidato;
    } else if (d.data.id === '2.2.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = cbCandiCalificacionExcelente.length;
      return cbCandiCalificacionExcelente;
    } else if (d.data.id === '2.2.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = cbCandicalf[10].length;
      return cbCandicalf[10];
    } else if (d.data.id === '2.2.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = cbCandicalf[9].length;
      return cbCandicalf[9];
    } else if (d.data.id === '2.2.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = cbCandiCalificacionBuena.length;
      return cbCandiCalificacionBuena;
    } else if (d.data.id === '2.2.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = cbCandicalf[8].length;
      return cbCandicalf[8];
    } else if (d.data.id === '2.2.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = cbCandicalf[7].length;
      return cbCandicalf[7];
    } else if (d.data.id === '2.2.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = cbCandicalf[6].length;
      return cbCandicalf[6];
    } else if (d.data.id === '2.2.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = cbCandiCalificacionMala.length;
      return cbCandiCalificacionMala;
    } else if (d.data.id === '2.2.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = cbCandicalf[5].length;
      return cbCandicalf[5];
    } else if (d.data.id === '2.2.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = cbCandicalf[4].length;
      return cbCandicalf[4];
    } else if (d.data.id === '2.2.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = cbCandicalf[3].length;
      return cbCandicalf[3];
    } else if (d.data.id === '2.2.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = cbCandicalf[2].length;
      return cbCandicalf[2];
    } else if (d.data.id === '2.2.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = cbCandicalf[1].length;
      return cbCandicalf[1];
    } else if (d.data.id === '2.2.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = cbCandicalf[0].length;
      return cbCandicalf[0];
    } else if (d.data.id === '2.3') {    // Empleados
      this.label = 'TOTAL EN EMPLEADOS:';
      this.dates = cbEmpleado.length;
      return cbEmpleado;
    } else if (d.data.id === '2.3.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = cbEmpleCalificacionExcelente.length;
      return cbEmpleCalificacionExcelente;
    } else if (d.data.id === '2.3.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = cbEmplecalf[10].length;
      return cbEmplecalf[10];
    } else if (d.data.id === '2.3.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = cbEmplecalf[9].length;
      return cbEmplecalf[9];
    } else if (d.data.id === '2.3.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = cbEmpleCalificacionBuena.length;
      return cbEmpleCalificacionBuena;
    } else if (d.data.id === '2.3.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = cbEmplecalf[8].length;
      return cbEmplecalf[8];
    } else if (d.data.id === '2.3.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = cbEmplecalf[7].length;
      return cbEmplecalf[7];
    } else if (d.data.id === '2.3.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = cbEmplecalf[6].length;
      return cbEmplecalf[6];
    } else if (d.data.id === '2.3.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = cbEmpleCalificacionMala.length;
      return cbEmpleCalificacionMala;
    } else if (d.data.id === '2.3.Mal.5') {
      this.label = 'TOAL EN 5:';
      this.dates = cbEmplecalf[5].length;
      return cbEmplecalf[5];
    } else if (d.data.id === '2.3.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = cbEmplecalf[4].length;
      return cbEmplecalf[4];
    } else if (d.data.id === '2.3.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = cbEmplecalf[3].length;
      return cbEmplecalf[3];
    } else if (d.data.id === '2.3.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = cbEmplecalf[2].length;
      return cbEmplecalf[2];
    } else if (d.data.id === '2.3.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = cbEmplecalf[1].length;
      return cbEmplecalf[1];
    } else if (d.data.id === '2.3.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = cbEmplecalf[0].length;
      return cbEmplecalf[0];
    } else if (d.data.id === '2.4') {    // Capacitación
      this.label = 'TOTAL EN CAPACITACIÓN:';
      this.dates = cbCapacitacion.length;
      return cbCapacitacion;
    } else if (d.data.id === '2.4.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = cbCapaCalificacionExcelente.length;
      return cbCapaCalificacionExcelente;
    } else if (d.data.id === '2.4.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = cbCapacalf[10].length;
      return cbCapacalf[10];
    } else if (d.data.id === '2.4.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = cbCapacalf[9].length;
      return cbCapacalf[9];
    } else if (d.data.id === '2.4.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = cbCapaCalificacionBuena.length;
      return cbCapaCalificacionBuena;
    } else if (d.data.id === '2.4.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = cbCapacalf[8].length;
      return cbCapacalf[8];
    } else if (d.data.id === '2.4.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = cbCapacalf[7].length;
      return cbCapacalf[7];
    } else if (d.data.id === '2.4.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = cbCapacalf[6].length;
      return cbCapacalf[6];
    } else if (d.data.id === '2.4.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = cbCapaCalificacionMala.length;
      return cbCapaCalificacionMala;
    } else if (d.data.id === '2.4.Mal.5') {
      this.label = 'TOTAL EN 5:';
      this.dates = cbCapacalf[5].length;
      return cbCapacalf[5];
    } else if (d.data.id === '2.4.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = cbCapacalf[4].length;
      return cbCapacalf[4];
    } else if (d.data.id === '2.4.Mal.3') {
      this.label = 'TOTAL EN 3:';
      this.dates = cbCapacalf[3].length;
      return cbCapacalf[3];
    } else if (d.data.id === '2.4.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = cbCapacalf[2].length;
      return cbCapacalf[2];
    } else if (d.data.id === '2.4.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = cbCapacalf[1].length;
      return cbCapacalf[1];
    } else if (d.data.id === '2.4.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates =cbCapacalf[0].length;
      return cbCapacalf[0];
    } else if (d.data.id === '2.5') {    // Seguimiento 1
      this.label = 'TOTAL EN SEGUIMIENTO 1:';
      this.dates = cbSeguimiento1.length;
      return cbSeguimiento1;
    } else if (d.data.id === '2.5.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = cbSegui1CalificacionExcelente.length;
      return cbSegui1CalificacionExcelente;
    } else if (d.data.id === '2.5.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = cbSegui1calf[10].length;
      return cbSegui1calf[10];
    } else if (d.data.id === '2.5.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = cbSegui1calf[9].length;
      return cbSegui1calf[9];
    } else if (d.data.id === '2.5.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = cbSegui1CalificacionBuena.length;
      return cbSegui1CalificacionBuena;
    } else if (d.data.id === '2.5.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = cbSegui1calf[8].length;
      return cbSegui1calf[8];
    } else if (d.data.id === '2.5.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = cbSegui1calf[7].length;
      return cbSegui1calf[7];
    } else if (d.data.id === '2.5.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = cbSegui1calf[6].length;
      return cbSegui1calf[6];
    } else if (d.data.id === '2.5.Mal') {
      this.label = 'TOTAl CALIFICACIÓN MALA:';
      this.dates = cbSegui1CalificacionMala.length;
      return cbSegui1CalificacionMala;
    } else if (d.data.id === '2.5.Mal.5') {
      this.label ='TOTAL EN 5:';
      this.dates = cbSegui1calf[5].length;
      return cbSegui1calf[5];
    } else if (d.data.id === '2.5.Mal.4') {
      this.label = 'TOTAL EN 4.';
      this.dates = cbSegui1calf[4].length;
      return cbSegui1calf[4];
    } else if (d.data.id === '2.5.Mal.3'){
      this.label = 'TOTAL EN 3:';
      this.dates = cbSegui1calf[3].length;
      return cbSegui1calf[3];
    } else if (d.data.id === '2.5.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = cbSegui1calf[2].length;
      return cbSegui1calf[2];
    } else if (d.data.id === '2.5.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = cbSegui1calf[1].length;
      return cbSegui1calf[1];
    } else if (d.data.id === '2.5.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = cbSegui1calf[0].length;
      return cbSegui1calf[0];
    } else if (d.data.id === '2.6') {    // Seguimiento 2
      this.label = 'TOTAL SEGUIMIENTO 2:';
      this.dates = cbSeguimiento2.length;
      return cbSeguimiento2;
    } else if (d.data.id === '2.6.Exc') {
      this.label = 'TOTAL CALIFICACIÓN EXCELENTE:';
      this.dates = cbSegui2CalificacionExcelente.length;
      return cbSegui2CalificacionExcelente;
    } else if (d.data.id === '2.6.Exc.10') {
      this.label = 'TOTAL EN 10:';
      this.dates = cbSegui2calf[10].length;
      return cbSegui2calf[10];
    } else if (d.data.id === '2.6.Exc.9') {
      this.label = 'TOTAL EN 9:';
      this.dates = cbSegui2calf[9].length;
      return cbSegui2calf[9];
    } else if (d.data.id === '2.6.Bue') {
      this.label = 'TOTAL CALIFICACIÓN BUENA:';
      this.dates = cbSegui2CalificacionBuena.length;
      return cbSegui2CalificacionBuena;
    } else if (d.data.id === '2.6.Bue.8') {
      this.label = 'TOTAL EN 8:';
      this.dates = cbSegui2calf[8].length;
      return cbSegui2calf[8];
    } else if (d.data.id === '2.6.Bue.7') {
      this.label = 'TOTAL EN 7:';
      this.dates = cbSegui2calf[7].length;
      return cbSegui2calf[7];
    } else if (d.data.id === '2.6.Bue.6') {
      this.label = 'TOTAL EN 6:';
      this.dates = cbSegui2calf[6].length;
      return cbSegui2calf[6];
    } else if (d.data.id === '2.6.Mal') {
      this.label = 'TOTAL CALIFICACIÓN MALA:';
      this.dates = cbSegui2CalificacionMala.length;
      return cbSegui2CalificacionMala;
    } else if (d.data.id === '2.6.Mal.5') {
      this.label = 'TOTAL EN 5.';
      this.dates = cbSegui2calf[5].length;
      return cbSegui2calf[5];
    } else if (d.data.id === '2.6.Mal.4') {
      this.label = 'TOTAL EN 4:';
      this.dates = cbSegui2calf[4].length;
      return cbSegui2calf[4];
    } else if (d.data.id === '2.6.Mal.3') {
      this.label = 'TOAL EN 3:';
      this.dates = cbSegui2calf[3].length;
      return cbSegui2calf[3];
    } else if (d.data.id === '2.6.Mal.2') {
      this.label = 'TOTAL EN 2:';
      this.dates = cbSegui2calf[2].length;
      return cbSegui2calf[2];
    } else if (d.data.id === '2.6.Mal.1') {
      this.label = 'TOTAL EN 1:';
      this.dates = cbSegui2calf[1].length;
      return cbSegui2calf[1];
    } else if (d.data.id === '2.6.Mal.0') {
      this.label = 'TOTAL EN 0:';
      this.dates = cbSegui2calf[0].length;
      return cbSegui2calf[0];
    }
  }

  private middleArcLine (d) {
    const halfPi = Math.PI / 2;
    const angles = [this.x(d.x0) - halfPi, this.x(d.x1) - halfPi];
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const middleAngle = (angles[1] + angles[0]) / 2;
    const invertDirection = middleAngle > 0 && middleAngle  <  Math.PI; // On lower quadrants write text ccw

    if (invertDirection) {
      angles.reverse();
    }

    const path = d3Path.path();
    path.arc(0, 0, r, angles[0], angles[1], invertDirection);

    return path.toString();
  }

  private textFits(d) {
    const CHAR_SPACE = 8;
    const deltaAngle = this.x(d.x1) - this.x(d.x0);
    const r = Math.max(0, (this.y(d.y0) + this.y(d.y1)) / 2);
    const perimeter = r * deltaAngle;

    return d.data.name.length * CHAR_SPACE  <  perimeter;
  }

  private removeSvg() {
    this.svg.remove();
  }
}
