import {getTestBed, TestBed} from '@angular/core/testing';
import {EmpleadosService} from './empleados.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('EmpleadosService', () => {
  let service: EmpleadosService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 104,
    idBanco: 3,
    idCandidato: 145,
    idPuesto: 7,
    idSubarea: 1,
    idTipoPuesto: 1,
    idTurnoEmpleado: 2,
    kpi: 0,
    kpiMensual: 0,
    kpiSemestral: 0,
    kpiTrimestral: 0,
    puestoDetalle: 'DESARROLLADOR',
    sueldoDiario: 300,
    sueldoMensual: 4000,
  },
    {
      id: 'empleados',
    }];

  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [EmpleadosService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(EmpleadosService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });

  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/empleados');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/empleados', '/97');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/empleados/267/2');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/empleados', '/97/1008136800000/74');
      });
    });
  }
});
