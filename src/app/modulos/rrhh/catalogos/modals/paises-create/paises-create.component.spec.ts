import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaisesCreateComponent} from './paises-create.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {Router} from '@angular/router';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {PaisesData} from '../../../../../@core/data/interfaces/catalogos/paises';

describe('PaisesCreateComponent', () => {
  let component: PaisesCreateComponent;
  let fixture: ComponentFixture<PaisesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaisesCreateComponent],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
      ],
      providers: [
        PaisesData,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaisesCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
