import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectoCreateComponent } from './prospecto-create.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {ProductoSolicitudData} from '../../../../@core/data/interfaces/ventaNueva/producto-solicitud';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {ProspectoData} from '../../../../@core/data/interfaces/ventaNueva/prospecto';

describe('ProspectoCreateComponent', () => {
  let component: ProspectoCreateComponent;
  let fixture: ComponentFixture<ProspectoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectoCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        ProspectoData,
        ProductoSolicitudData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectoCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
