import {Component, Input, OnInit} from '@angular/core';
import {CandidatoData, Candidatos} from '../../../../@core/data/interfaces/rrhh/candidatos';
import {NbDialogRef} from '@nebular/theme';
import {CandidatosComponent} from '../../candidatos/candidatos.component';
import * as moment from 'moment';


@Component({
  selector: 'ngx-datos-candidato',
  templateUrl: './datos-candidato.component.html',
  styleUrls: ['./datos-candidato.component.scss'],
})
export class DatosCandidatoComponent implements OnInit {
  @Input() idCandidato: number;
  candidato: Candidatos;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  calificacionPsicometrico: number;
  calificacionExamen: number;
  idPrecandidato: number;
  comentarios: string;
  fechaCreacion: string;
  fechaIngreso: string;
  fechaBaja: number;
  idEstadoRH: number;
  idEtapa: number;
  telefonoMovil: number;
  idTipoUsuario: number;
  sede: number;
  puesto: string;

  constructor(protected candidatosService: CandidatoData,  protected ref: NbDialogRef<CandidatosComponent>) {
  }

  getCandidatosById() {
    this.candidatosService.getCandidatoById(this.idCandidato).subscribe(data => {
      this.candidato = data;
      let calificaPsi: any;
       if (data.calificacionPsicometrico) { calificaPsi = 'SI'; }else { calificaPsi = 'NO'; }
      this.nombre = data.nombre;
      this.calificacionPsicometrico = calificaPsi;
      this.calificacionExamen = data.calificacionExamen;
      this.comentarios = data.comentarios;
      this.fechaIngreso = moment(data.fechaIngreso).format('DD-MM-YYYY');
      this.sede = data.sede;
      this.puesto = data.puesto
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getCandidatosById();
  }

}
