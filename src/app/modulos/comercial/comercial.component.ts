import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-comercial',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ComercialComponent implements OnInit  {
  // constructor(private router)
  ngOnInit() {
    
  }
  // getPermisos() {
  //   this.permisos = JSON.parse(window.localStorage.getItem('User'));
  //   this.escritura = this.permisos.modulos.solicitudes.escritura;
  // }
}
