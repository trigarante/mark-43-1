import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../@core/data/services/login/token-storage.service';
import {User} from '../../@core/data/interfaces/ti/user';
import {UserService} from '../../@core/data/users.service';
import {NbToastrService} from '@nebular/theme';
import {Usuario} from '../../@core/data/interfaces/ti/usuario';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})


export class DashboardComponent implements OnInit {
  name?: any;
  decode?: any;
  user?: User;
  private stompClient = null;

constructor(protected tokenStorage: TokenStorageService, private userService: UserService,
  private toastrService: NbToastrService) {
}


  ngOnInit() {
    this.userService.getCurrentUser(window.localStorage.getItem('token'))
      .subscribe((users: User) => this.user = users);
    this.showToastYes();
    // this.connect();
  }
  showToastYes() {
    this.toastrService.success('!Estas adentro 3:) ¡', `Logueo Exitoso`);
  }

  // connect() {
  //   const socket = new SockJS('http://192.168.7.186:8090/mark43-service/v1/dashboard');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({'Auth-token': localStorage.getItem('token')}, function(frame){
  //     console.log(frame);
  //   });
  //
  // }

}
