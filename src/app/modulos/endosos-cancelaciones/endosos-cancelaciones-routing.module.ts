import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EndososCancelacionesComponent} from './endosos-cancelaciones.component';
import {SolicitudEndososComponent} from './solicitud-endosos/solicitud-endosos.component';
import {VerEndososCancelacionesComponent} from './ver-endosos-cancelaciones/ver-endosos-cancelaciones.component';

const routes: Routes = [{
  path: '',
  component: EndososCancelacionesComponent,
  children: [
    {
      path: 'solicitudes-endosos-cancelaciones',
      component: SolicitudEndososComponent,
    },
    {
      path: 'endosos-cancelaciones',
      component: VerEndososCancelacionesComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndososCancelacionesRoutingModule {
}
