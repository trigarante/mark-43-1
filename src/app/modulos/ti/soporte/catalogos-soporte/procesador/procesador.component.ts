import {Component, OnInit, ViewChild} from '@angular/core';
import {Procesador, ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NbDialogService} from '@nebular/theme';
import {map} from 'rxjs/operators';
import {CrearProcesadorComponent} from '../modals/crear-procesador/crear-procesador.component';
import swal from 'sweetalert';
import {environment} from '../../../../../../environments/environment';
import {Router} from '@angular/router';
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-procesador',
  templateUrl: './procesador.component.html',
  styleUrls: ['./procesador.component.scss'],
})
export class ProcesadorComponent implements OnInit {
  cols: any[];
  procesadores: Procesador[];
  dataSource: any;
  procesador: Procesador;
  // socket
  private stompClient = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(protected procesadorService: ProcesadorSoporteData, private dialogoService: NbDialogService,
              private router: Router) {
  }
  connect() {
    const _this = this;
    /**---------------------------------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'procesadores');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      _this.stompClient.subscribe('/task/panelProcesadores', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getProcesadorSoporte();
        }, 500);

      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  crearProcesador() {
    this.dialogoService.open(CrearProcesadorComponent, {
      context: {
        idTipo: 1,
      },
    }).onClose.subscribe(x => {
      this.getProcesadorSoporte();


    });
  }
  getProcesadorById(idprocesador) {
    this.procesadorService.getProcesadorSoporte(idprocesador).subscribe(data => {
      this.procesador = data;
    });
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getProcesadorSoporte() {
    this.procesadorService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.procesadores = [];
      this.procesadores = data;
      this.dataSource = new MatTableDataSource(this.procesadores); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }

  updateProcesador(idProcesador) {
    this.dialogoService.open(CrearProcesadorComponent, {
      context: {
        idProcesador: idProcesador,
        idTipo: 2,
      },
    }).onClose.subscribe(x => {
      this.getProcesadorSoporte();
    });
  }

  bajaProcesador(idProcesador) {
    this.getProcesadorById(idProcesador);
    swal({
      title: '¿Deseas dar de baja este Procesador?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.procesador['activo'] = 0;
        this.procesadorService.put(idProcesador, this.procesador).subscribe( result => {
          this.procesadorService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡El Procesador se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getProcesadorSoporte();
        }));
      }
    });
  }

  ngOnInit() {
    this.connect();
    this.getProcesadorSoporte();
    this.cols = ['procesador', 'acciones'];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.procesadores.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.procesadores.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

}
