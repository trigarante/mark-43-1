import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {Router} from '@angular/router';
import {NbDialogRef} from '@nebular/theme';
import {SeguimientoData} from '../../../../@core/data/interfaces/rrhh/seguimiento';
import {map} from "rxjs/operators";
import {PreempleadosData} from "../../../../@core/data/interfaces/rrhh/preempleados";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/** Componente del modal "asignar etapa de práctica" */
@Component({
  selector: 'ngx-asignar-etapa-practica',
  templateUrl: './asignar-etapa-practica.component.html',
  styleUrls: ['./asignar-etapa-practica.component.scss'],
})
export class AsignarEtapaPracticaComponent implements OnInit {

  /** Variable que guarda el idSeguimiento obtenido. */
  @Input() idSeguimiento: number;
  /** Variable que guarda el idPrecandidato obtenido. */
  @Input() idPrecandidato: number;
  @Input() idTipo: number;
  seguimientoPracticoEliminado: boolean;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Variable que permite la inicialización del formulario para asignar la etapa de práctica a un precandidato. */
  etapaPracticaForm: FormGroup;
  /** Lista de elementos de rollplay. */
  rollPlay: SelectItem[];
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**
   * El método constructor invoca Servicios y Formularios (con control de validaciones).
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {Router} router Inicializa variable para poder navegar entre componentes.
   * @param {NbDialogRef<AsignarEtapaPracticaComponent>} ref Referencia al diálogo (mensaje).
   * @param seguimientoService Servicio del componente seguimiento para poder invocar los métodos. */
  constructor(private fb: FormBuilder, private router: Router, protected ref: NbDialogRef<AsignarEtapaPracticaComponent>,
              protected seguimientoService: SeguimientoData, protected servicePreempleado: PreempleadosData) {
  }
  /** Esta función cierra el modal. */
  ocultar() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario.
  * Además, reinicia el formulario y redirecciona a la página. */
  guardarEtapaPractica() {
    this.seguimientoService.getSeguimientoById(this.idSeguimiento).subscribe(data => {
      if (data.idEtapa === 6 && data.idEstadoRRHH === 1 || data.idEtapa === 6 && data.idEstadoRRHH === 16) {
        this.seguimientoService.fasePractica(this.idSeguimiento, this.etapaPracticaForm.value.calificacionRollPlay,
          this.etapaPracticaForm.value.comentariosFaseDos, this.etapaPracticaForm.value.idEtapa, this.idPrecandidato,
        ).subscribe(
          result => {
            this.inactivo = true;
          },
          error => { },
          () => {
            this.servicePreempleado.postSocket(this.etapaPracticaForm.value).subscribe(x => {});
            this.seguimientoService.postSocketAignaciones(this.etapaPracticaForm.value).subscribe(x => {});
            this.etapaPracticaForm.reset();
            this.ref.close();
            this.router.navigate(['/modulos/rrhh/pre-empleados']);
          },
        );
      }else {
        this.seguimientoPracticoEliminado = true;
      }
    });

  }

  /** Función que inicializa la lista "rollplay", agregándole los valores de "si" y "no". */
  selectRollplay() {
    this.rollPlay = [];
    this.rollPlay.push({label: 'Si', value: true});
    this.rollPlay.push({label: 'No', value: false});
  }

  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    this.selectRollplay();
    this.etapaPracticaForm = this.fb.group({
      'calificacionRollPlay': new FormControl(null, Validators.required),
      'comentariosFaseDos': new FormControl('', Validators.required),
      'idEtapa': 7,
    });
  }

}
