import { Observable } from 'rxjs';

export interface TipoPagina {
  id: number;
  tipo: number;
  objetivo: string;
  activo: number;
}

export abstract class TipoPaginaData {
  abstract get(): Observable<TipoPagina[]>;

  abstract post(tipoPagina): Observable<TipoPagina>;
  abstract postSocket(json): Observable<JSON>;

  abstract getTipoPaginaById(idTipoPagina): Observable<TipoPagina>;

  abstract put(idTipoPagina, tipoPagina): Observable<TipoPagina>;
}
