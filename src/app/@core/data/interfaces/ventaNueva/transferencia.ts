export interface Transferencia {
  banco: string;
  primaNeta: string;
  estatusFinanzas: string;
  estatusPago: string;
  noAutorizacion: string;
}
