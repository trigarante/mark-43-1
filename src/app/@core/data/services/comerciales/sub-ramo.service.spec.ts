import { TestBed, getTestBed } from '@angular/core/testing';
import { SubRamoService } from './sub-ramo.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';



fdescribe('SubRamoService', () => {
  let injector: TestBed;
  let service: SubRamoService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 1,
    idRamo: 30,
    idTipoSubRamo: 1,
    prioridad: 1,
    prioridades: 'ALTA',
    descripcion: 'TEST FINALL',
    activo: 0,
    tipoRamo: 'SEGUROS',
    nombreComercial: 'KK',
    alias: 'KK',
    idEstadoSocio: 2,
    estado: 'INACTIVO',
    idEstado: 2,
    tipoSubRamo: 'AUT',
  },
    {
      id: 'subramo',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SubRamoService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(SubRamoService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/subramo');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/subramo', '/80');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/subramo');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/subramo', '/80/74');
      });
    });
  }

});
