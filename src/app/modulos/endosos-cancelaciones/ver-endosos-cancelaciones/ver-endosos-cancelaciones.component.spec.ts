import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEndososCancelacionesComponent } from './ver-endosos-cancelaciones.component';

describe('VerEndososCancelacionesComponent', () => {
  let component: VerEndososCancelacionesComponent;
  let fixture: ComponentFixture<VerEndososCancelacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEndososCancelacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEndososCancelacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
