import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Empleados, EmpleadosData} from '../../../@core/data/interfaces/rrhh/empleados';
import {Subarea, SubareaData} from '../../../@core/data/interfaces/catalogos/subarea';
import {Area, AreaData} from '../../../@core/data/interfaces/catalogos/area';
import {map} from 'rxjs/operators';
import {NbDialogService} from '@nebular/theme';
import {CrearCoordinadorComponent} from '../modals/crear-coordinador/crear-coordinador.component';

@Component({
  selector: 'ngx-coordinador',
  templateUrl: './coordinador.component.html',
  styleUrls: ['./coordinador.component.scss'],
})
export class CoordinadorComponent implements OnInit {

  CoordinadorForm: FormGroup;
  cols: any[];
  empleado: Empleados[];
  area: any[];
  subarea: Subarea[];
  idSubarea: any;
  areas: any[];
  subareas: Area[];
  constructor(private fb: FormBuilder, private empleadosService: EmpleadosData,  private subareaService: SubareaData,
              private areaService: AreaData, public dialogService: NbDialogService) {
    this.CoordinadorForm = this.fb.group({
      'subareacontrol': new FormControl(),
      'areacontrol': new FormControl(),
    });
  }

  /** va a mostrar los empleados que ya tengan id de Usuario*/
  getEmpleados(id) {
    this.empleadosService.getBySubarea(id).pipe(map( result => {
      return result.filter( data => data.idUsuario !== null );
    })).subscribe(data => this.empleado = data);
    this.idSubarea = id;
  }
  /** consigue todas las areas de ejecutivos*/
  getAreas() {
    this.areaService.get().pipe(map(result => {
      return result.filter(data => data.id === 3 || data.id === 5 || data.id === 6 || data.id === 7 || data.id === 8);
    })).subscribe(data => {
      this.subareas = data;
      this.areas = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.areas.push({'label': this.subareas[i].nombre, 'value': this.subareas[i].id});

      }
    });
  }
  getSubarea(event) {
    this.subareaService.findByIdArea(event).pipe(map(data => this.subarea = data)).subscribe(data => {
      this.area = [];
      for (let i = 0; i < this.subarea.length; i++) {
        this.area.push({'label': this.subarea[i].subarea, 'value': this.subarea[i].id});
        this.empleado = null;
      }
    });
  }
  asignarCoordinador(idUsuario, idSubarea, id) {
    this.dialogService.open(CrearCoordinadorComponent, {
      context: {
        idUsuario: idUsuario,
        idSubarea: idSubarea,
        id: id,
      },
    });
  }
  ngOnInit() {
    this.getAreas(),
      this.cols = [
        {field: 'nombre', header: 'Nombre'},
        {field: 'telefono', header: 'Telefono'},
        {field: 'crearcorreo', header: 'Crear Coordinador'},

      ];
  }

}
