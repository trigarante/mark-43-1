import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';
import {TarjetaRed, TarjetaRedData} from '../../../../interfaces/ti/soporte/inventario/tarjeta-red';


@Injectable()
export class TarjetaRedService extends TarjetaRedData {
  private baseURL;
  private socketURl;
    constructor(private http: HttpClient) {
      super();
      this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
    }
  get(): Observable<TarjetaRed[]> {
    return this.http.get<TarjetaRed[]>( this.baseURL + 'v1/tarjetaRed');
  }

  post(tarjetaRed: TarjetaRed): Observable<TarjetaRed> {
    return this.http.post<TarjetaRed>(this.baseURL + 'v1/tarjetaRed', tarjetaRed);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'targetared/saveTargetaRed', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajatargetared/saveBajaTargetaRed', json);
  }
  getTarjetaRedById(idTarjetaRed): Observable<TarjetaRed> {
    return this.http.get<TarjetaRed>(this.baseURL + 'v1/tarjetaRed/' + idTarjetaRed);
  }
  put(idTarjetaRed, tarjetaRed: TarjetaRed): Observable<TarjetaRed> {
    return this.http.put<TarjetaRed>(this.baseURL + 'v1/tarjetaRed/' + idTarjetaRed + '/'
      + sessionStorage.getItem('Empleado') , tarjetaRed);
  }
}
