import {Component, Input, OnInit} from '@angular/core';
import {Usuario, UsuarioData} from '../../../../@core/data/interfaces/ti/usuario';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map} from 'rxjs/operators';
import {GrupoUsuarios, GrupoUsuariosData} from '../../../../@core/data/interfaces/ti/grupoUsuarios';
import {Extension, ExtensionData} from '../../../../@core/data/interfaces/ti/extension';
import {Empleados, EmpleadosData} from '../../../../@core/data/interfaces/rrhh/empleados';
import {Correo, CorreoData, Domains} from '../../../../@core/data/interfaces/ti/correo';
import {SaveComponent} from '../save/save.component';
import {
  LicenciasDisponibles,
  LicenciasDisponiblesData,
} from '../../../../@core/data/interfaces/ti/licencias-disponibles';
import {SelectItem} from 'primeng/api';
import {SaveModificadoComponent} from '../save-modificado/save-modificado.component';

@Component({
  selector: 'ngx-crear-supervisor',
  templateUrl: './crear-supervisor.component.html',
  styleUrls: ['./crear-supervisor.component.scss'],
})
export class CrearSupervisorComponent implements OnInit {
  @Input() idUsuario: number;
  @Input() idSubarea: number;
  @Input() id: number;
  @Input() areaprincipal: number;
  empleado: Empleados;
  usuario: Usuario;
  nombresupervisor: string;
  textoaviso: string;
  bandera: boolean;
  supervisorForm: FormGroup;
  grupoUsuario: GrupoUsuarios[];
  grupUsuario: any[];
  extensiones: any[];
  extensionput: Extension;
  extensionput1: Extension;
  extension: Extension[];
  usuarios: Usuario[];
  usuario2: Usuario;
  usuario3: Usuario;
  correosdisponibles: any[];
  superv: RegExp = /^supervisor/;
  idCorreo: any[];
  idCorreoslista: any[];
  correoprimario: string;
  idCorreo2: string;
  datosUsuarios: object;
  extensionsupervisor: string;
  usuarioput: Usuario;
  usuarioput1: Usuario;
  datosUsuario: any;
  idUsuariox: number;
  banderaUsuarios: boolean = false;
  banderaUsuarios1: boolean;
  emailsupervisor: string;
  estadoCorreo2: boolean = false;
  estadoCorreo3: boolean;
  estadofinal: string;
  /**  se obtenie un arreglo correo*/
  correo: Correo[];
  idCorreosupervisor: string;
  banderaUsuariosejecutivos: boolean;
  NuevoNombreSupervisor: string ;
  tipo: string = 'ejecutivo';
  licencias: LicenciasDisponibles[];
  correoAdministrativos: Correo[];
  banderalicencias: boolean;
  tipoLicencia: number;
  /** inicializa arrelo de dominios*/
  domainss: any[];
  domains: Domains[];
  estadofindalias: boolean;
  tiposdominios: SelectItem[];
  tipodominio: string;
  tipodominio2: string;
  nombreUnidadesOrganizativas: any[];

  constructor(private empleadosService: EmpleadosData, private correoService: CorreoData,
              private usuarioService: UsuarioData, protected ref: NbDialogRef<CrearSupervisorComponent>,
              private fb: FormBuilder, private extensionesService: ExtensionData,
              private grupoUsuariosService: GrupoUsuariosData,  public dialogService: NbDialogService,
              private licenciasService: LicenciasDisponiblesData, private toastrService: NbToastrService ) {
    this.supervisorForm = this.fb.group({
      'extensioncontrol': new FormControl(),
      'privilegiocontrol': new FormControl(),
      'correosdisponibleSubareas': new FormControl(),
       'emailmodificado': new FormControl(),
      'dominio': new FormControl(),
      'tiposdominios': new FormControl(),
      'unidadOrganizativa': new FormControl(),
    });
  }
  getEmple() {
    this.empleadosService.getEmpleadoById(this.id).subscribe(data => {
      this.empleado = data;

    });
  }
  getUsuarios() {
    /*this.usuarioService.getUsuarioById(this.idUsuario).subscribe(data => {
      this.usuario = data;

    });*/
    this.bandera = true;
}
  getExtensiones() {/** en un futuro definir el subarea de las extensiones de los supervisores*/
    this.extensionesService.getExtensionByIdSubarea(71, 0).pipe(map(data => this.extension = data)).subscribe(data => {
      this.extensiones = [];
      for (let i = 0; i < this.extension.length; i++) {
        this.extensiones.push({'label': this.extension[i].id, 'value': this.extension[i].id});
      }
    });
  }

  getextension(event) {
      if (event) {
        this.extensionsupervisor = event;
      }
    }


  getTipo() {
    this.tiposdominios = [];
    this.tiposdominios.push({label: 'ejecutivo', value: 'ejecutivo'});
    this.tiposdominios.push({label: 'asistencia', value: 'asistencia'});

  }
  /** obtiene los dominios disponibles por tipo de correo y obtiene el numero de licencias de google*/
  getdominios(event) {
  if (event) {
    this.correoService.getDominios(event).pipe(map(data => this.domains = data)).subscribe(data => {
      this.domainss = [];
      for (let i = 0; i < this.domains.length; i++) {
        this.domainss.push({'label': this.domains[i].domainName, 'value': this.domains[i].domainName});
      }
    });
    this.getcorreosdisponiblesarea();
    this.tipodominio = event;

    if (this.tipodominio === 'ejecutivo') {
        this.tipodominio2 = 'ejecutivo';
      this.tipoLicencia = 0;
    }
    if (this.tipodominio === 'asistencia') {
       this.tipodominio2 = 'asistencia';
      this.tipoLicencia = 1;
    }

    this.licenciasService.get().subscribe(data2 => {
      this.licencias = data2;
      this.correoService.get(event).subscribe(data3 => {
        this.correoAdministrativos = data3;
        if (this.correoAdministrativos.length === this.licencias[this.tipoLicencia].cantidad ||
          this.correoAdministrativos.length >= this.licencias[this.tipoLicencia].cantidad ) {
          // checar licencias
          this.banderalicencias = true;
        }else if (this.correoAdministrativos.length <= this.licencias[this.tipoLicencia].cantidad) {
          // licencias disponibles
          this.banderalicencias = false;
        }
      });
    });
  }
    this.getUnidadesOrganizativas(event);
  }

  getGrupoUsuarios() {
    this.grupoUsuariosService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1);
    })).subscribe(data => {
      this.grupoUsuario = data;
      this.grupUsuario = [];
      for (let i = 0; i < this.grupoUsuario.length; i++) {
        this.grupUsuario.push({'label': this.grupoUsuario[i].nombre, 'value': this.grupoUsuario[i].id});
      }
    });
  }

  /** metodo que obtiene todos los usuarios libres*/
  getcorreosdisponiblesarea() {
    this.banderaUsuarios = false;
    /** area de ahorraasintencia*/
    if ( this.supervisorForm.value.tiposdominios === 'asistencia') {
      this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
        return result.filter(data => data.usuario.match(/ahorraasistencia.com/));
      })).subscribe(data => {
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        this.usuarios = data;
        for (let i = 0; i < this.usuarios.length; i++) {
          this.correosdisponibles.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].usuario});
          this.idCorreoslista.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].idCorreo});
          this.idCorreo.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].id});

        }

        /** si no hay usuarios cambia la banderaUsuarios a true para que no muestre el p-dropdown donde se muestra los correos
         * disponibles
         */
        if (this.usuarios.length === 0) {
          this.banderaUsuarios = true;

        } else if (this.usuarios.length !== 0) {

        }
      });

    } else {
      this.usuarioService.getUsuarioByEstado(0).pipe(map(result => {
        return result.filter(data => data.usuario.match(/ahorraseguros.mx/) || data.usuario.match(/ahorracredit.com/)
          || data.usuario.match(/mejorsegurodeauto.mx/) || data.usuario.match(/mejorsegurodevida.mx/)
          || data.usuario.match(/qualitassegurosautos.com/) || data.usuario.match(/inprove.mx/)
          || data.usuario.match(/segurosmotos.com.mx/) || data.usuario.match(/seguroselpotosi.mx/)
          || data.usuario.match(/segurosdetaxi.com.mx/) || data.usuario.match(/multivaseguros.com/)
          || data.usuario.match(/lalatinoseguros.mx/)
          || data.usuario.match(/gnpsegurosautos.com/) || data.usuario.match(/comparaya.mx/));
      })).subscribe(data => {
        this.correosdisponibles = [];
        this.idCorreoslista = [];
        this.idCorreo = [];
        this.usuarios = data;
        for (let i = 0; i < this.usuarios.length; i++) {
          this.correosdisponibles.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].usuario});
          this.idCorreoslista.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].idCorreo});
          this.idCorreo.push({'label': this.usuarios[i].usuario, 'value': this.usuarios[i].id});

        }

        /** si no hay usuarios cambia la banderaUsuarios a true para que no muestre el p-dropdown donde se muestra los correos
         * disponibles
         */
        if (this.usuarios.length === 0) {
          this.banderaUsuarios = true;

        } else if (this.usuarios.length !== 0) {

        }
      });
    }
    }

    getnuevocorreoSupervisor() {


    }
    /** obtiene el nombre del correo por medio del input y ve si esta disponible o no*/
  getcorreofinal(event) {
    if (event) {
      this.estadoCorreo3 = false;

      this.correoService.validarCorreo(this.tipodominio2, event).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.estadofinal = 'correo ya en uso';
        } else if (this.estadoCorreo3 === false) {
          this.emailsupervisor = event;
          this.estadofinal = 'correo disponible';
          if (this.banderaUsuarios1 === false) {
            this.correoprimario = event;
          }
          if (this.banderaUsuarios1 === true) { // b
            this.NuevoNombreSupervisor = event;
          }
        }
      });
    }
  }
  /** selecciona el correo principal */
  getcorreoprincialSubareas(event) {
    if (event) {
      this.correoprimario = event;
      // this.banderaUsuarios = false;
      /** se obtiene si es tipo  ejecutivo o ahorraasistencia */
      if (this.correoprimario.match(/ahorraseguros.mx/) || this.correoprimario.match(/ahorracredit.com/)
        || this.correoprimario.match(/mejorsegurodeauto.mx/) || this.correoprimario.match(/mejorsegurodevida.mx/)
        || this.correoprimario.match(/qualitassegurosautos.com/)) {
        this.tipodominio = 'ejecutivo';
        this.tipodominio2 = 'ejecutivo';
      } else if (this.correoprimario.match(/ahorraasistencia.com/)) {
        this.tipodominio = 'asistencia';
        this.tipodominio2 = 'asistencia';
      }
    }
  }

  getdomainss(event) {
    if (event) {
      this.banderaUsuarios1 = true;
      this.emailsupervisor = 'supervisor1@' + this.supervisorForm.value.dominio;
      this.correoService.validarCorreo(this.tipodominio2, this.emailsupervisor).subscribe(response => {
        this.estadoCorreo3 = response;
        if (this.estadoCorreo3 === true) {
          this.supervisorForm.controls['emailmodificado'].setValue(this.emailsupervisor);
          this.estadofinal = 'correo ocupado';
        }else if (this.estadoCorreo3 === false) {
          this.supervisorForm.controls['emailmodificado'].setValue(this.emailsupervisor);
          this.estadofinal = 'correo disponible';
        }
      });
    }
  }

  getUnidadesOrganizativas(event) {
    this.correoService.getUnidadesOrganizativas(event).subscribe( data => {
      this.nombreUnidadesOrganizativas = [];
      for (const unidad of data){
        this.nombreUnidadesOrganizativas.push({'label': unidad.name, 'value': unidad.orgUnitPath});
      }
    });
  }
    guardarUsuario() {
      /**Obtiene el idCorreo del correo principal del api*/
      for (let i = 0; i < this.idCorreoslista.length; i++) {
        console.table(this.idCorreoslista);
        if (this.correoprimario === this.idCorreoslista[i].label) {
          this.idCorreo2 = this.idCorreoslista[i].value;
        }
      }
      /** saca el id del usuario del correo primario*/
      for (let i = 0; i < this.idCorreo.length; i++) {
        if (this.correoprimario === this.idCorreo[i].label) {
          this.idUsuariox = this.idCorreo[i].value;
        }
      }

        // toma el mismo nombre ,mal no funciona if
        if (this.banderaUsuarios1 === false) {
          // const objeto = {
          //   'id': this.idCorreo2, // id del correoprimario
          //   'givenName': this.empleado.nombre,
          //   'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
          //   'email': this.correoprimario, // nombre del correo
          //   'orgUnitPath': this.supervisorForm.value.unidadOrganizativa,
          // };

          // /*this.correoService.enviarByAlias(objeto).subscribe(response => {   /// crear ejecutivo supervisor pendiente
          //   this.estadofindalias = response;
          // });*/

          /** busca el usuario por el id y remplaza los datos con el nuevo supervisor - obtiene la misma extension, cambian
           * los datos del id empleado y extension de datosUsuarios*/
          this.usuarioService.getUsuarioById(this.idUsuariox).subscribe(datausuarios => {
            this.usuarioput = datausuarios;
            this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
            this.datosUsuarios = {
              'idCampania': this.idSubarea,
              'idEmpleado': this.empleado.id,
              'idExtension': this.datosUsuario.idExtension,
              'idProtocolo': 1,
              'idPerfilMarcacion': 1,
            };
            this.usuarioput1 = {
              'usuario': this.usuarioput.usuario,
              'password': this.usuarioput.password,
              'idGrupo' : this.usuarioput.idGrupo,
              'idCorreo': this.usuarioput.idCorreo,
              'idTipo': this.usuarioput.idTipo,
              'idSubarea': this.empleado.idSubarea,
              'datosUsuario': JSON.stringify(this.datosUsuarios),
              'estado': 1,
            };
            // this.usuarioService.put(this.idUsuariox, this.usuarioput1).subscribe(result => {});
            /** extension de usuario empleado cambia a ocupado */
            /*this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
              this.extensionput = dataextensiones;*/
              /*this.extensionput1 = {
                'idSubarea' : this.extensionput.idSubarea,
                'descrip' : this.extensionput.descrip,
                'idEstado' : 1,
              };*/
              /* this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result => {});*/
            /*});*/
          });
        }
        // toma el correo de otra persona y cambia el correo a supervisorx bien
        if (this.banderaUsuarios1 === true) {
          const objeto = {
            'id': this.idCorreo2, // id del correoprimario
            'givenName': this.empleado.nombre,
            'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
            'email': this.NuevoNombreSupervisor, // nombre del correo
            'orgUnitPath': this.supervisorForm.value.unidadOrganizativa,
          };
          this.correoService.enviarByAlias(this.tipodominio, objeto).subscribe(response => { // nuevo servicio cambia nombre
            this.estadofindalias = response;
          });
          /** busca el usuario por el id y remplaza los datos con el nuevo supervisor - obtiene la misma extension, cambian
           * los datos del id empleado y extension de datosUsuarios*/
          this.usuarioService.getUsuarioById(this.idUsuariox).subscribe(datausuarios => {
            this.usuarioput = datausuarios;
            this.datosUsuario = JSON.parse(this.usuarioput.datosUsuario);
            this.datosUsuarios = {
              'idCampania': this.idSubarea,
              'idEmpleado': this.empleado.id,
              'idExtension': this.supervisorForm.value.extensioncontrol,
              'idProtocolo': 1,
              'idPerfilMarcacion': 1,
            };
            if ( this.supervisorForm.value.tiposdominios === 'ejecutivo') {
              this.usuarioput1 = {
                'id': this.usuarioput.id,
                'idCorreo': this.idCorreo2,
                'idGrupo': this.supervisorForm.value.privilegiocontrol,
                'idTipo': 2,
                'idSubarea': this.idSubarea,
                'usuario': this.NuevoNombreSupervisor,
                'password': 'mark-43',
                'datosUsuario': JSON.stringify(this.datosUsuarios),
                'estado': 1,
                'nombre': this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
              };
            }
            if ( this.supervisorForm.value.tiposdominios === 'asistencia') {
              this.usuarioput1 = {
                'id': this.usuarioput.id,
                'idCorreo': this.idCorreo2,
                'idGrupo': this.supervisorForm.value.privilegiocontrol,
                'idTipo': 3,
                'idSubarea': this.idSubarea,
                'usuario': this.NuevoNombreSupervisor,
                'password': 'mark-43',
                'datosUsuario': JSON.stringify(this.datosUsuarios),
                'estado': 1,
                'nombre': this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
              };
            }
            this.usuarioService.put(this.empleado.id, this.usuarioput1).subscribe(result => {
              this.supervisorForm.reset();

              // desactiva la extension anterior
              this.extensionesService.get(this.datosUsuario.idExtension).subscribe(dataextensiones => {
                this.extensionput = dataextensiones;
                this.extensionput1 = {
                  'id': this.extensionput.id,
                  'idSubarea': this.extensionput.idSubarea,
                  'descrip': this.extensionput.descrip,
                  'idEstado': 0,
                };
                this.extensionesService.put(this.datosUsuario.idExtension, this.extensionput1).subscribe(result2 => {
                });
              });
              /** extension de usuario empleado cambia a ocupado */
              this.extensionesService.get(this.extensionsupervisor).subscribe(dataextensiones => {
                this.extensionput = dataextensiones;
                this.extensionput1 = {
                  'id': this.extensionput.id,
                  'idSubarea': this.extensionput.idSubarea,
                  'descrip': this.extensionput.descrip,
                  'idEstado': 1,
                };
                this.extensionesService.put(this.extensionsupervisor, this.extensionput1).subscribe(result2 => {
                });
              });
              this.ref.close();
              this.dialogService.open(SaveModificadoComponent, {
              }).onClose.subscribe(x => {
              });
            });
          });
        }
       // })
    }
    guardarNuevoSupervisor() {
      let idCorreo: string;
      if (this.banderalicencias === true) {
        this.showToast2('danger');
      }else if (this.banderalicencias === false) {

        const objeto = {
          'givenName': this.empleado.nombre,
          'familyName': this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,
          'email': this.emailsupervisor, // nombre del correo ya disponible
          'orgUnitPath': this.supervisorForm.value.unidadOrganizativa,
        };
       // crear ejecutivo
        this.correoService.post(this.supervisorForm.value.dominio, objeto).subscribe(
          result => {
            idCorreo = result;
        },
          error1 => {},
          () => {
            // una ves guardado el correo en el api, busca su id y lo manda a la tabla usuarios
            this.correoService.get(this.supervisorForm.value.tiposdominios).subscribe(data => {
              this.correo = data;
              this.datosUsuarios = {
                'idCampania': this.idSubarea, 'idEmpleado': this.empleado.id, 'idExtension': this.supervisorForm.value.extensioncontrol,
                'idProtocolo': 1, 'idPerfilMarcacion': 1,
              };
              if ( this.supervisorForm.value.tiposdominios === 'ejecutivo') {
                this.usuario2 = {
                  'idCorreo': idCorreo,
                  'idGrupo': this.supervisorForm.value.privilegiocontrol,
                  'idTipo': 2,
                  'idSubarea': this.idSubarea,
                  'usuario': this.emailsupervisor,
                  'password': 'mark-43',
                  'datosUsuario': JSON.stringify(this.datosUsuarios),
                  'estado': 1,
                  'nombre': this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,

                };
              }
              if ( this.supervisorForm.value.tiposdominios === 'asistencia') {
                this.usuario2 = {
                  'idCorreo': idCorreo,
                  'idGrupo': this.supervisorForm.value.privilegiocontrol,
                  'idTipo': 3,
                  'idSubarea': this.idSubarea,
                  'usuario': this.emailsupervisor,
                  'password': 'mark-43',
                  'datosUsuario': JSON.stringify(this.datosUsuarios),
                  'estado': 1,
                  'nombre': this.empleado.nombre + ' ' + this.empleado.apellidoPaterno + ' ' + this.empleado.apellidoMaterno,

                };
              }
              // guarda en la tabla usuario y modifica el idUsuario de la tabla empleado
              this.usuarioService.postUsuariosidEmpleados(this.usuario2, this.empleado.id).subscribe(data2 => {
                this.usuario3 = data2;
                this.supervisorForm.reset();
                // si guardo bien modifica al extension a ocupado
                if (this.usuario3 !== null) {
                  // cambia el estado a 1 a la extencion dada
                  this.extensionesService.get(this.extensionsupervisor).subscribe(dataextensiones => {
                    this.extensionput = dataextensiones;
                    this.extensionput1 = {
                      'id': this.extensionput.id,
                      'idSubarea': this.extensionput.idSubarea,
                      'descrip': this.extensionput.descrip,
                      'idEstado': 1,
                    };
                    this.extensionesService.put(this.extensionsupervisor, this.extensionput1).subscribe(result => {
                    });
                  });

                  this.dialogService.open(SaveComponent, {
                    context: {
                      modo: 2,
                    },
                  }).onClose.subscribe(x => {

                  });
                }
              });
            });
          }
          );
      }
    }
   guardar() {
     if (this.correosdisponibles.length !== 0) {
        this.guardarUsuario();
       this.tipodominio = this.tipodominio + 'Correo';
     }
     if (this.correosdisponibles.length === 0) {
        this.guardarNuevoSupervisor();
     }
   }
  dismiss() {
    this.ref.close();
  }
  showToast2(status) {
    this.toastrService.danger('Atencion', `Licencias de correos ya no disponibles`);
  }
  ngOnInit() {
    this.getEmple();
    this.getcorreosdisponiblesarea();
    this.getExtensiones();
    this.getUsuarios();
    this.getGrupoUsuarios();
    // this.getdominios();
    this.getTipo();
  }

}
