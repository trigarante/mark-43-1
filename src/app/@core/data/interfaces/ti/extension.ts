import {Observable} from 'rxjs';

export interface Extension {
  id?: number;
  idSubarea?: number;
  descrip?: string;
  idEstado: number;
  area?: string;
  subarea?: string;
  estado?: string;
  idArea?: number;
}
export abstract class ExtensionData {
  abstract post(jsonExtension, min, max);
  abstract get(id): Observable<Extension>;
  abstract getExtension(): Observable<Extension[]>;
  abstract getExtensionByIdSubarea(IdSubarea, IdEstado): Observable<Extension[]>;
  abstract put(idExtension, extension: Extension): Observable<Extension>;
  abstract postSocket(json): Observable<JSON>; //
}

