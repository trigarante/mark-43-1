import {Observable} from 'rxjs';

export interface EstadoPolizaVn {
  id: number;
  estado: string;
  activo: number;
}

export abstract class EstadoPolizaVnData {
  abstract get(): Observable<EstadoPolizaVn[]>;

  abstract post(estadoPoliza): Observable<EstadoPolizaVn>;

  abstract getEstadoPolizaById(idEstadoPoliza): Observable<EstadoPolizaVn>;

  abstract put(idEstadoPoliza, estadoPoliza): Observable<EstadoPolizaVn>;

  abstract postSocket(json): Observable<JSON>;

}
