import {Observable} from 'rxjs';

export interface EstadoInventario {
  id: number;
  estado: string;
  idEstado?: number;
  activo?: number;
}
export abstract class EstadoInventarioData {
  abstract get(): Observable<EstadoInventario[]>;
  abstract post(estadoInventario): Observable<EstadoInventario>;
  abstract postSocket(json): Observable<JSON>;
  abstract getEstadoInventarioById(idEstadoInventario): Observable<EstadoInventario>;
  abstract put(idEstadoInventario, estadoInventario): Observable<EstadoInventario>;
}

