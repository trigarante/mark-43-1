import {Component, Input, OnInit} from '@angular/core';
/**import {Categorias, CategoriasData} from '../../../../@core/data/interfaces/comerciales/categorias';*/
import {CampanaCategoriaData} from '../../../../@core/data/interfaces/marketing/campana-categoria';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {CampanaCategoriaComponent} from '../../campana-categoria/campana-categoria.component';
import {map} from 'rxjs/operators';
import {SubRamo, SubramoData} from '../../../../@core/data/interfaces/comerciales/sub-ramo';

@Component({
  selector: 'ngx-asignar-categoria-camapana',
  templateUrl: './asignar-categoria-camapana.component.html',
  styleUrls: ['./asignar-categoria-camapana.component.scss'],
})
export class AsignarCategoriaCamapanaComponent implements OnInit {
  @Input() idCampanaMarketing: number;
  @Input() idTipo: number;
  categoriaCampanaForm: FormGroup;
  categoria: any[];
  categorias: SubRamo[];

  constructor(protected categoriaService: SubramoData, protected categoriaCampanaService: CampanaCategoriaData,
              private fb: FormBuilder, protected ref: NbDialogRef<CampanaCategoriaComponent>,
              private router: Router) {
  }

  guradarCategoriaCampana() {
    console.log(this.categoriaCampanaForm.value);
    this.categoriaCampanaService.post(this.categoriaCampanaForm.value).subscribe((result) => {
      this.categoriaCampanaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.categoriaCampanaForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/marketing/camapana-categoria']);
    });
  }

  updateCategoriasCampana() {
  }

  getCategorias() {
    this.categoriaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.categorias = result;
      this.categoria = [];
      for (let i = 0; i < this.categorias.length; i++) {
        this.categoria.push({'label': this.categorias[i].descripcion, 'value': this.categorias[i].id});
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getCategorias();
    this.categoriaCampanaForm = this.fb.group({
      'idCampana': this.idCampanaMarketing,
      'idSubRamo': new FormControl('', Validators.required),
      'comentarios': new FormControl('', Validators.required),
      'activo': 1,
    });
  }

}
