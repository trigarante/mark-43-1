import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {Diadema, DiademaData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {Sede, SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {SelectItem} from 'primeng/api';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-diademe-create',
  templateUrl: './diademe-create.component.html',
  styleUrls: ['./diademe-create.component.scss'],
})
export class DiademeCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idDiadema: number;
  diadema: Diadema;
  diademaCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;
  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];


  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<DiademeCreateComponent>, private diademaService: DiademaData,
              private router: Router, private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private tipoSedeService: TipoSedeData ) { }
  guardarCreateDiadema() {
    if (this.diademaCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.diademaService.post(this.diademaCreateForm.value).subscribe((result) => {
      this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.diademaCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario' , 'diadema']);
    });
  }
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map( data => this.observaciones = data)).subscribe( data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({'label': this.observaciones[i].observaciones, 'value': this.observaciones[i].id});
      }
    });
  }
  dismiss() {
    this.ref.close();
  }
  updateDiadema() {
    this.diademaService.put(this.idDiadema, this.diademaCreateForm.value).subscribe( result => {
      this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
      this.diademaCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario' , 'diadema']);
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }

  getDiademaById() {
    this.diademaService.getDiademaById(this.idDiadema).subscribe( data => {
      this.diadema = data ;
      this.diademaCreateForm.controls['numFolio'].setValue(this.diadema.numFolio);
      this.diademaCreateForm.controls['idMarca'].setValue(this.diadema.idMarca);
      this.diademaCreateForm.controls['idEstadoInventario'].setValue(this.diadema.idEstadoInventario);
      this.diademaCreateForm.controls['idObservacionesInventario'].setValue(this.diadema.idObservacionesInventario);
      this.diademaCreateForm.controls['comentarios'].setValue(this.diadema.comentarios);
      this.diademaCreateForm.controls['idEmpleado'].setValue(this.diadema.idEmpleado);
      this.diademaCreateForm.controls['idTipoSede'].setValue(this.diadema.idTipoSede);
      this.diademaCreateForm.controls['origenRecurso'].setValue(this.diadema.origenRecurso);

    });
  }

  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getDiademaById();
    }
    this.getEstadoInventario();
    this.getMarca();
    this.getObservacionInventario();
    this.getOrigen();
    this.getSedes();
    this.diademaCreateForm = this.fb.group({
      'numFolio': new FormControl('', Validators.compose([Validators.required])),
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });
  }

}
