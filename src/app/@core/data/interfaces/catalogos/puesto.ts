import {Observable} from 'rxjs';

export interface Puesto {
  id: number;
  idSubarea: number;
  nombre: string;
  activo: number;
  idTipoUsuario: number;
}

export abstract class PuestoData {
  abstract get(): Observable<Puesto[]>;

  abstract post(puesto: Puesto): Observable<Puesto>;
  abstract postSocket(json): Observable<JSON>;

  abstract getById(idPuesto): Observable<Puesto>;

  abstract put(idPuesto, puesto): Observable<Puesto>;
}
