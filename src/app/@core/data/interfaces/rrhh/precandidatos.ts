import {Observable} from 'rxjs';

export interface Precandidatos {
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  curp: string;
  estadoRh: string;
  genero: string;
  fechaNacimiento: Date;
  idPais: number;
  idEstadoCivil: number;
  idEstadoRH: number;
  idEscolaridad: number;
  idEstadoEscolaridad: number;
  idSolicitudRrhh: number;
  email: string;
  cp: number;
  colonia: string;
  idColonia: number;
  calle: string;
  numeroInterior: number;
  numeroExterior: number;
  telefonoFijo: number;
  telefonoMovil: number;
  fechaCreacion: string;
  fechaBaja: string;
  tiempoTraslado: number;
  idMedioTraslado: number;
  idEstacion: number;
  idEstadoRRHH: number;
  idEtapa: number;
  detalleBaja: string;
  nombreEtapa: string;
  recontratable: number;
  recontratables: string;
  sede: string;
  idSede: number;
  idSubarea: number;
  subarea: string;
  // activo:number;
  //  estado:string;
  //  delMun:string;
  //  ciudad:string;
}

export abstract class PrecandidatosData {
  abstract get(): Observable<Precandidatos[]>;

  abstract validarCurp(curp);

  abstract post(precandidato, idRrhh, fechaNacimiento): Observable<Precandidatos[]>;

  abstract postSocket(json): Observable<JSON>;
  abstract postSocketBajaRH(json): Observable<JSON>;

  abstract getPrecandidatoById(idPrecandidato): Observable<Precandidatos>;

  abstract updatePrecandidato(idPrecandidato, precandidato, fechaNacimiento): Observable<Precandidatos>;

  abstract activaPrecandidato(idPrecandidato): Observable<Precandidatos>;

  abstract reactivarPrecandidato(idPrecandidato, date): Observable<Precandidatos>;

  abstract editarRecontratable(idPrecandidato, recontratable): Observable<Precandidatos>;

  abstract bajaPrecandidato(iPrecandidato, precandidato, fechaBaja): Observable<Precandidatos>;
}
