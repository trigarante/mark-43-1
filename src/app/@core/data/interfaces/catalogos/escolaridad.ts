import {Observable} from 'rxjs';

export interface Escolaridad {
  id: number;
  nivel: string;
  activo: number;
}

export abstract class EscolaridadData {
  abstract get(): Observable<Escolaridad[]>;
  abstract post(escolaridades: Escolaridad): Observable<Escolaridad>;
  abstract postSocket(json): Observable<JSON>;
  abstract getEscolaridadById(idEscolaridad): Observable<Escolaridad>;
  abstract put(idEscolaridad, escolaridad): Observable<Escolaridad>;
}
