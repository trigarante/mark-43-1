import {Component, OnInit, ViewChild} from '@angular/core';
import {Seguimiento, SeguimientoData} from '../../../@core/data/interfaces/rrhh/seguimiento';
import {map} from 'rxjs/operators';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import Swal from 'sweetalert2';
import {EstadoRrhh, EstadoRrhhData} from '../../../@core/data/interfaces/catalogos/estado-rrhh';
import {AsignarSeguimientoComponent} from '../modals/asignar-seguimiento/asignar-seguimiento.component';
import {BajaPrecandidatoComponent} from '../modals/baja-precandidato/baja-precandidato.component';
import {SeguimientoAsistenciaComponent} from '../modals/seguimiento-asistencia/seguimiento-asistencia.component';
import {Router} from '@angular/router';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DatosSeguimientoTeoricoComponent} from '../modals/datos-seguimiento-teorico/datos-seguimiento-teorico.component';
import {environment} from '../../../../environments/environment';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
/**
 * Componente de seguimiento.
 */
@Component({
  selector: 'ngx-seguimiento',
  templateUrl: './seguimiento.component.html',
  styleUrls: ['./seguimiento.component.scss'],
})
export class SeguimientoComponent implements OnInit {
  seguimiento: Seguimiento[];
  cols: any;
  motivosBaja: EstadoRrhh[];
  motivos: any[];
  jsonFecha: any;
  actAsignar: boolean;
  segmiento: any;
  dataSource: any;
  permisos: any;
  escritura: boolean;
  // Web Sockets
  private stompClient = null;
  /**
   * @param {SeguimientoData} seguimientoService Servicio del componente Seguimiento para poder invocar los métodos.
   * @param {NbDialogService} dialogService Servicio para diálogos (mensajes).*/
  constructor(protected seguimientoService: SeguimientoData,
              private dialogService: NbDialogService,
              protected motivosService: EstadoRrhhData,
              private router: Router,
              private toastrService: NbToastrService) {
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'rollplay');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, (frame) => {
      // _this.setConnected(true);
      _this.stompClient.subscribe('/task/panelRollplay', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getSeguimiento(0);
        }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  /** Esta función obtiene toda la información asociada al seguimiento que se le ha dado a cada candidato. */
  getSeguimiento(mensajeAct) {
       this.seguimientoService.get().pipe(map(result => {
         return result.filter(data => data.idEtapa === 5 && data.idEstadoRRHH === 1 || data.idEtapa === 5 &&
           data.idEstadoRRHH === 16);
       })).subscribe(data => {
         this.seguimiento = data;
         this.dataSource = new MatTableDataSource(this.seguimiento);
         this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      if (this.seguimiento.length === 0) {
        swal({
          icon: 'error',
          title: 'Oops',
          text: 'No tienes candidatos por capacitar',
        });
      }
    }, () => {
        swal({
          icon: 'error',
          title: 'Error al cargar los datos',
          text: 'Favor de comunicarse con el departamento de TI',
        });
      },
      () => {
        if (mensajeAct === 1 && this.seguimiento.length !== 0) {
          this.toastrService.success('!!Datos Actualizados¡¡', `Roll-Play`);
        }
      });
  }

  activarAsignar(asistencia) {
    if (asistencia !== null) {
      let asistencias: any;
      asistencias = JSON.parse(asistencia);
      if (Object.keys(asistencias).length > 1) {
        return true;
      }else {
        return false;
      }
    }else {
      return false;
    }
  }

  getMotivosBaja() {
    this.motivosService.get().pipe(map(data => {
      // Filtro para aquellas opciones que estén activas, sean de tipo baja y la etapa sea de solicitudes.
      data = data.filter(result => result.activo === 1 && result.idEstadoRh === 0 && result.idEtapa === 0);
      return data;
    })).subscribe(result => {
      this.motivosBaja = result;
      this.motivos = [];
      for (let i = 0; i < this.motivosBaja.length; i++) {
        this.motivos.push({'id': this.motivosBaja[i].id, 'value': this.motivosBaja[i].estado});
      }
    });
  }

  verDatos(idSeguimiento) {
    this.dialogService.open(DatosSeguimientoTeoricoComponent, {
      context: {
        idSeguimiento: idSeguimiento,
      },
    });
  }

  /** Función que despliega un modal para dar de baja un precandidato. */
  async bajaSeguimiento(idPrecandidato) {
    // Inicializando opciones de motivos de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.motivos.length; i++) {
      opciones[this.motivos[i]['id']] = this.motivos[i]['value'];
    }

    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await Swal.fire({
      title: '¿Deseas dar de baja de Roll-Play?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un motivo de baja',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada un motivo de baja
      this.dialogService.open(BajaPrecandidatoComponent, {
        context: {
          idPrecandidato: idPrecandidato,
          idEstadoRh: motivo,
        },
      }).onClose.subscribe(x => {
        this.getSeguimiento(0);
      });
    }
  }

  updateSeguimiento(idSeguimiento, idPrecandidato, nombre, apellidoPaterno, apellidoMaterno) {
    this.dialogService.open(AsignarSeguimientoComponent, {
      context: {
        idSeguimiento: idSeguimiento,
        idTipo: 2,
        idPrecandidato: idPrecandidato,
        nombre: nombre,
        apellidoPaterno: apellidoPaterno,
        apellidoMaterno: apellidoMaterno,

      },
    }).onClose.subscribe(x => {
      this.getSeguimiento(1);
    });
  }

  registroAsistencia(idSeguimiento) {
    this.dialogService.open(SeguimientoAsistenciaComponent, {
      context: {
        idSeguimiento: idSeguimiento,
      },
    }).onClose.subscribe(x => {
      this.getSeguimiento(0);
    });
  }

  updateAsistencia(idSeguimiento) {
    this.dialogService.open(SeguimientoAsistenciaComponent, {
      context: {
        idSeguimiento: idSeguimiento,
        idTipo: 2,
      },
    }).onClose.subscribe(x => {
      this.getSeguimiento(0);
    });
  }

  async asignarPractica(idSeguimiento) {
    swal({
      title: '¿Deseas enviar a asignaciones?',
      text: 'El candidato se mostrara en Asignaciones.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        succes: {
          text: 'Asignar',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      this.seguimientoService.getSeguimientoById(idSeguimiento).subscribe(data => {
        if (data.idEtapa === 5 && data.idEstadoRRHH === 1 || data.idEtapa === 5 && data.idEstadoRRHH === 16) {
          if (valor) {
            this.seguimientoService.getSeguimientoById(idSeguimiento).subscribe(result => {
              this.segmiento = result;
              this.segmiento.idEtapa = 6;
              this.segmiento.calificacionRollPlay = 0;
              this.seguimientoService.fasePractica(idSeguimiento, this.segmiento.calificacionRollPlay, this.segmiento.comentariosFaseDos,
                this.segmiento.idEtapa, this.segmiento.idPrecandidato).subscribe(() => {
                const object = {
                  'idSeguimiento': idSeguimiento,
                  'calificacionRollPlay': this.segmiento.calificacionRollPlay,
                  'idEtapa': this.segmiento.idEtapa,
                  'idPrecandidato': this.segmiento.idPrecandidato,
                };
                this.seguimientoService.postSocketRolplay(object).subscribe(x => {});
                this.seguimientoService.postSocketAignaciones(object).subscribe(x => {});
              });
            });
            swal('¡Se ha enviado exitosamente!, Redireccionando a Asignaciones...', {
              icon: 'success',
            }).then((resultado => {
              this.router.navigate(['modulos/rrhh/seguimiento-practico']);
            }));
          }
        }else {
          swal('¡Atención este roll play ha sido Eliminado!', {
            icon: 'warning',
          });
        }
      });
    });
  }

  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.seguimiento.escritura;
  }
  /** Función que inicializa el componente, mandando a llamar a getSeguimiento para llenar la tabla. */
  ngOnInit() {
    this.connect();
    this.getMotivosBaja();
    this.cols = [ 'detalle', 'nombre', 'calificacion', 'sede', 'subarea', 'acciones'];
    this.getPermisos();
    // setInterval(() => {
    //   if (location.pathname === '/mark43/modulos/rrhh/seguimiento') {
        this.getSeguimiento(0);
    //   }
    // }, 1000);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
