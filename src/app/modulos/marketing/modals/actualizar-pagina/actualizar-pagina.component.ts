import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subarea, SubareaData} from '../../../../@core/data/interfaces/catalogos/subarea';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {SedeData, Sede} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData, Grupo} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData, Empresa} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData, Area} from '../../../../@core/data/interfaces/catalogos/area';
import {map} from 'rxjs/operators';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Router} from '@angular/router';
import {GuardadoComponent} from '../guardado/guardado.component';
import {MarcaEmpresarial, MarcaEmpresarialData} from '../../../../@core/data/interfaces/catalogos/marca-empresarial';

@Component({
  selector: 'ngx-actualizar-pagina',
  templateUrl: './actualizar-pagina.component.html',
  styleUrls: ['./actualizar-pagina.component.scss'],
})
export class ActualizarPaginaComponent implements OnInit {
  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  actualizarPaginaForm: FormGroup;
  pagina: Pagina;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarcategorias: boolean;
  mostrarMarca: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para puesto. */
  empresa: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  marcas: MarcaEmpresarial[];

  areaget: Subarea;
  sedeget: any;
  empresaget: any;
  marcaget: any;
  grupoempresarialget: any;
  grupoget: any;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<ActualizarPaginaComponent>,  private router: Router,
              private paginaService: PaginasData, protected subareaService: SubareaData, private sedesService: SedeData,
              private grupoService: GrupoData, private empresaService: EmpresaData, private areaService: AreaData,
              private marcaEmpresarialService: MarcaEmpresarialData, public dialogService: NbDialogService) {
    this.actualizarPaginaForm = this.fb.group({
      'idCampana': new FormControl('', Validators.compose([Validators.required])),
      'url': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'idGrupo': new FormControl(),
      'idEmpresa': new FormControl(),
      'idMarcaEmpresarial': new FormControl(),
      'idSede': new FormControl(),
      'idArea': new FormControl(),
      // 'configuracion': this.pagina.configuracion,
      // 'fechaRegistro': this.pagina.fechaRegistro,
      // 'fechaActualizacion': this.pagina.fechaActualizacion,
    });
  }
  getPaginaById() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.actualizarPaginaForm.controls['idCampana'].setValue(this.pagina.idCampana);
      this.actualizarPaginaForm.controls['url'].setValue(this.pagina.url);
      this.actualizarPaginaForm.controls['descripcion'].setValue(this.pagina.descripcion);
      this.subareaService.getById(this.pagina.idCampana).subscribe(data2 => {
        this.areaget = data2;

        this.getSubarea(this.areaget.idArea);

        this.areaService.getAreaById(this.areaget.idArea).subscribe(data3 => {
          this.sedeget = data3;
          this.getArea(this.sedeget.idSede);
          this.actualizarPaginaForm.controls['idArea'].setValue(this.sedeget.id);

          // this.marcaEmpresarialService.getMarcaEmpresarialById(this.marcaget.idMarcaEmpresarial).subscribe( data4 => {
          //   this.sedeget = data4;
          //   this.getMarcaEmpresarial(this.sedeget.idSede);
          //   this.actualizarPaginaForm.controls['idMarcaEmpresarial'].setValue(this.sedeget.id);

            this.sedesService.getSedeById(this.sedeget.idSede).subscribe(data5 => {
              this.empresaget = data5;
              this.getSedes(this.empresaget.idEmpresa);
              this.actualizarPaginaForm.controls['idSede'].setValue(this.empresaget.id);


              this.empresaService.getEmpresasById(this.empresaget.idEmpresa).subscribe(data6 => {
                this.grupoempresarialget = data6;
                this.getEmpresa(this.grupoempresarialget.idGrupo);
                this.actualizarPaginaForm.controls['idEmpresa'].setValue(this.grupoempresarialget.id);

                this.grupoService.getGrupoById(this.grupoempresarialget.idGrupo).subscribe(data7 => {
                  this.grupoget = data7;
                  this.getGrupo();
                  this.actualizarPaginaForm.controls['idGrupo'].setValue(this.grupoget.id);
                });
              });

            });
          // });
        });

      });
    });
  }
  mostrarIdCampana() {


  }

  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }

  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  getSedes(idEmpresa) {
    this.sedesService.get().pipe(map(result => {
      return result.filter( valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }
  // getMarcaEmpresarial(idSede) {
  //   this.marcaEmpresarialService.get().pipe(map(result => {
  //     return result.filter( valor => valor.idSede === idSede);
  //   })).subscribe(data => {
  //     this.marcas = data;
  //     if (this.marcas.length === 0) {
  //       this.mostrarMarca = true;
  //     }else {
  //       this.mostrarMarca = false;
  //     }
  //
  //   });
  //
  // }
  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      }else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }
  guardar() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;

      this.pagina['idCampana'] = this.actualizarPaginaForm.value.idCampana;
      this.pagina['url'] = this.actualizarPaginaForm.value.url;
      this.pagina['descripcion'] = this.actualizarPaginaForm.value.descripcion;

      let fechaRegistro: any;
      fechaRegistro = Date.parse(this.pagina.fechaRegistro);
      let fechaActualizacion: any;
      fechaActualizacion = Date.parse(this.pagina.fechaActualizacion);

      this.paginaService.put(this.id, fechaRegistro, fechaActualizacion, this.pagina).subscribe(result => {
        this.paginaService.postSocket({status: 'ok'}).subscribe(x => {});
        this.ref.close();
        this.router.navigate(['/modulos/marketing/pagina/']);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 2,
        },
      });
      this.router.navigate(['/modulos/marketing/pagina/']);
    });

  }
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getPaginaById();
    this.getGrupo();
    this.mostrarIdCampana();
  }

}
