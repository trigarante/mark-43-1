import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelefonoIpCreateComponent } from './telefono-ip-create.component';
import {
  MatButtonToggleModule,
  MatCardModule, MatDatepickerModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatRadioModule, MatSelectModule,
  MatTableModule, MatTabsModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {CalendarModule, DropdownModule, InputTextModule, KeyFilterModule} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import {NbActionsModule, NbCardModule, NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {ObservacionesInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {ProcesadorSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/procesador';
import {SedeData} from '../../../../../@core/data/interfaces/catalogos/sede';
import {HttpClient} from '@angular/common/http';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {TelefonoIpData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/telefono-ip';

describe('TelefonoIpCreateComponent', () => {
  let component: TelefonoIpCreateComponent;
  let fixture: ComponentFixture<TelefonoIpCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelefonoIpCreateComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
        TableModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        HttpClientTestingModule,
        RouterModule,
        MatButtonToggleModule,
        MatRadioModule,
        MatDividerModule,
        NbCardModule,
        DropdownModule,
        NbActionsModule,
        MatTabsModule,
      ],
      providers: [
        TelefonoIpData,
        ObservacionesInventarioData,
        EstadoInventarioData,
        MarcaSoporteData,
        ProcesadorSoporteData,
        SedeData,
        HttpClient,
        {
          provide: ActivatedRoute, useValue: [],
        },
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelefonoIpCreateComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
