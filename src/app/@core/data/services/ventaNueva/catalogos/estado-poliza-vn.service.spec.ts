import { TestBed } from '@angular/core/testing';

import { EstadoPolizaVnService } from './estado-poliza-vn.service';

describe('EstadoPolizaVnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadoPolizaVnService = TestBed.get(EstadoPolizaVnService);
    expect(service).toBeTruthy();
  });
});
