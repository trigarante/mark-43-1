import {Observable} from 'rxjs';

export interface AnalisisCompetencia {
  idCompetencia: number;
  precios: number;
  participacion: number;
  activo: number;
  nombre?: string;
  idProducto?: number;
  idTipoCompetencia?: number;
  competencia?: string;
  idEstado?: number;
  idEstadoSocio?: number;
  alias?: any;
  descripcion?: any;
  estado?: any;
  nombreComercial?: any;
  tipoCategoria?: any;
  tipoCompetencia?: any;
  tipoGiro?: any;
  id: number;
  tipoRamo: string;
  tipoSubRamo: string;
}

export abstract class AnalisisCompetenciaData {
  abstract get(): Observable<AnalisisCompetencia[]>;

  abstract post(analisisCompetencia): Observable<AnalisisCompetencia>;
  abstract postSocket(json): Observable<JSON>;

  abstract getAnalisisCompetenciaById(idAnalisisCompetencia): Observable<AnalisisCompetencia>;

  abstract put(idAnalisisCompetencia, analisisCompetencia): Observable<AnalisisCompetencia>;
}
