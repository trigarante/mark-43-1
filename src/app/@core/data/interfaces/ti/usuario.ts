import {Observable} from 'rxjs';
import {DatosUsuarios} from './datosUsuarios';


export interface Usuario {
  id?: number;
  usuario?: string;
  password?: string;
  idCorreo?: string;
  idGrupo?: number;
  idTipo?: number;
  idSubarea?: number;
  fechaCreacion?: Date;
  datosUsuario?: string;
  estado: number;
  asignado?: number;
  imagenUrl?: string;
  nombre?: string;
  idEmpleado?: number;
  tipoUsuario?: string;
}
export abstract class UsuarioData {
  abstract get(): Observable<Usuario[]>;
  abstract getUsuarioByIdSubarea(IdSubarea, Estado): Observable<Usuario[]>;
  abstract postUsuarios(usuario: Usuario): Observable<Usuario>;
  abstract getUsuarioByEstado(Estado): Observable<Usuario[]>;
  abstract getUsuarioByRango(idEstado, menor, mayor): Observable<Usuario[]>;
  abstract postUsuariosidEmpleados(jsonUsuario, idempleado);
  abstract postSocket(json): Observable<JSON>;
  abstract postSocketTiUsuarios(json): Observable<JSON>;

  abstract put(idUsuario, usuario: Usuario): Observable<Usuario>;
  abstract putUsuario(idUsuario, usuario: Usuario): Observable<Usuario>;
  abstract getUsuarioById(idUsuario): Observable<Usuario>;

  abstract PutBajaGrupo(idgrupo): Observable <string>;
}
