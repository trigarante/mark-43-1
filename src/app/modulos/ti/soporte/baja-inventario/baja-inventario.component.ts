import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Cpu, CpuData} from '../../../../@core/data/interfaces/ti/soporte/inventario/cpu';
import {Monitor, MonitorDate} from '../../../../@core/data/interfaces/ti/soporte/inventario/monitor';
import {Laptop, LaptopData} from '../../../../@core/data/interfaces/ti/soporte/inventario/laptop';
import {Diadema, DiademaData} from '../../../../@core/data/interfaces/ti/soporte/inventario/diadema';
import {Tablet, TabletData} from '../../../../@core/data/interfaces/ti/soporte/inventario/tablet';
import {Teclado, TecladoData} from '../../../../@core/data/interfaces/ti/soporte/inventario/teclado';
import {Mouse, MouseData} from '../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {EstadoInventario, EstadoInventarioData} from '../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {NbDialogService} from '@nebular/theme';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {map} from 'rxjs/operators';
import {VerDetalleComponent} from '../modals/ver-detalle/ver-detalle.component';
import swal2 from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../../../environments/environment'; // LIBRERIA PARA LOS ALERT DE SWEET
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-baja-inventario',
  templateUrl: './baja-inventario.component.html',
  styleUrls: ['./baja-inventario.component.scss'],
})
export class BajaInventarioComponent implements OnInit {
  /** Variable que inicializa las columnas*/
  cols: any[];
  /** Variable para el arreglo cpu*/
  cpu: Cpu[];
  /** Variable para el arreglo monitor*/
  monitor: Monitor[];
  /** Variable para el arreglo laptop*/
  laptop: Laptop[];
  /** Variable para el arreglo diadema*/
  diadema: Diadema[];
  /** Variable para el arreglo tablet*/
  tablet: Tablet[];
  /** Variable para el arreglo teclado*/
  teclado: Teclado[];
  /** Variable para el arreglo mouse*/
  mouse: Mouse[];

  cpuactivar: Cpu;
  monitoractivar: Monitor;
  laptopactivar: Laptop;
  diademaactivar: Diadema;
  tabletactivar: Tablet;
  tecladoactivar: Teclado;
  mouseactivar: Mouse;
  tipoEquipo: string = this.rutaActiva.snapshot.params.tipo;

  dataSource: any;
  tipoEquipos: string;

  estadoInventariodata: EstadoInventario[];
  estadoInventario: any[];
  opcionequipo: string;
// sockets
  actualizar: string;
  private stompClientCPU = null;
  private stompClientMonitor = null;
  private stompLap = null;
  private stompTab = null;
  private stompTec = null;
  private stompM = null;
  private stompD = null;
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor( private fb: FormBuilder, private cpuService: CpuData, private monitorService: MonitorDate,
               private laptopService: LaptopData, private diademaService: DiademaData,
               private tabletService: TabletData, private tecladoService: TecladoData,
               private mouseService: MouseData, public dialogService: NbDialogService,
               private estadoInventarioServive: EstadoInventarioData, private rutaActiva: ActivatedRoute,
               private router: Router) { }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  connect() {
    const _this = this;
    /**----------------------CPU-------------**/
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'bajacpu');
    this.stompClientCPU = Stomp.over(socket);
    this.stompClientCPU.connect({}, (frame) => {
      _this.stompClientCPU.subscribe('/task/panelBajaCpu', (content) => {
        if (this.actualizar === 'CPU' || this.tipoEquipos === 'CPU') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('CPU');
            _this.getEquipo();
          }, 500);
        }
      });
    });
    /**----------------------MONITOR-------------**/
    const socket1 = new SockJS(environment.GLOBAL_SOCKET + 'bajamonitor');
    this.stompClientMonitor = Stomp.over(socket1);
    this.stompClientMonitor.connect({}, (frame) => {
      _this.stompClientMonitor.subscribe('/task/panelBajaMonitor', (content) => {
        if (this.actualizar === 'MONITOR') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('MONITOR');
          }, 500);
        }
      });
    });
    /**----------------------LAPTOP-------------**/
    const socket2 = new SockJS(environment.GLOBAL_SOCKET + 'bajalaptop');
    this.stompLap = Stomp.over(socket2);
    this.stompLap.connect({}, (frame) => {
      _this.stompLap.subscribe('/task/panelBajaLaptop', (content) => {
        if (this.actualizar === 'LAPTOP') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('LAPTOP');
          }, 500);
        }
      });
    });
    /**----------------------TABLET-------------**/
    const socket3 = new SockJS(environment.GLOBAL_SOCKET + 'bajatablet');
    this.stompTab = Stomp.over(socket3);
    this.stompTab.connect({}, (frame) => {
      _this.stompTab.subscribe('/task/panelBajaTablet', (content) => {
        if (this.actualizar === 'TABLET') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('TABLET');
          }, 500);
        }
      });
    });
    /**----------------------teclado-------------**/
    const socket4 = new SockJS(environment.GLOBAL_SOCKET + 'bajateclado');
    this.stompTec = Stomp.over(socket4);
    this.stompTec.connect({}, (frame) => {
      _this.stompTec.subscribe('/task/panelBajaTeclado', (content) => {
        if (this.actualizar === 'TECLADO') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('TECLADO');
          }, 500);
        }
      });
    });
    /**----------------mouse-------------------**/
    const socket5 = new SockJS(environment.GLOBAL_SOCKET + 'bajamouse');
    this.stompM = Stomp.over(socket5);
    this.stompM.connect({}, (frame) => {
      _this.stompM.subscribe('/task/panelBajaMouse', (content) => {
        if (this.actualizar === 'MOUSE') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('MOUSE');
          }, 500);
        }
      });
    });
    /**----------------diadema-------------------**/
    const socket6 = new SockJS(environment.GLOBAL_SOCKET + 'bajadiadema');
    this.stompD = Stomp.over(socket6);
    this.stompD.connect({}, (frame) => {
      _this.stompD.subscribe('/task/panelBajaDiadema', (content) => {
        if (this.actualizar === 'DIADEMA') {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getEquipos('DIADEMA');
          }, 500);
        }
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  /** muestra los valores de cpu*/
  getEquipo() {
    this.cpuService.get().pipe(map(result => {
      return result.filter(data => data.activo === 0);
    })).subscribe(data => {
      this.cpu = data;
      this.dataSource = new MatTableDataSource(this.cpu);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
    this.tipoEquipos = 'cpu';
    this.opcionequipo = this.tipoEquipos.toLowerCase();
  }
  /** Se muestra un menu con el componente TAB, con el selectedTabChange se selecciona que tabla selecionar*/
  getEquipos(tipoEquipo) {
    this.actualizar = tipoEquipo;
    this.tipoEquipos = tipoEquipo;
    this.opcionequipo = this.tipoEquipos.toLowerCase();
    // this.opcionequipo = this.tipoEquipos.toLowerCase();
    /**Obtiene los datos del cpu*/
    if (tipoEquipo === 'CPU') {
      this.cpuService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.cpu = data;
        this.dataSource = new MatTableDataSource(this.cpu);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
      /**Obtiene los datos de monitor*/
    } else if (tipoEquipo === 'MONITOR') {
      this.monitorService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.monitor = data;
        this.dataSource = new MatTableDataSource(this.monitor);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

    }else if (tipoEquipo === 'LAPTOP') {
      this.laptopService.get().pipe(map( result => {
        return result.filter(data => data.activo === 0);
      })).subscribe( data => {
        this.laptop = data;
        this.dataSource = new MatTableDataSource(this.laptop);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

    }else if (tipoEquipo === 'DIADEMA') {
      this.diademaService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.diadema = data;
        this.dataSource = new MatTableDataSource(this.diadema);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
    }else if (tipoEquipo === 'TABLET') {
      this.tabletService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.tablet = data;
        this.dataSource = new MatTableDataSource(this.tablet);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

    }else if (tipoEquipo === 'TECLADO') {
      this.tecladoService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.teclado = data;
        this.dataSource = new MatTableDataSource(this.teclado);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

    }else if (tipoEquipo === 'MOUSE') {
      this.mouseService.get().pipe(map(result => {
        return result.filter(data => data.activo === 0);
      })).subscribe(data => {
        this.mouse = data;
        this.dataSource = new MatTableDataSource(this.mouse);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });


    }

  }
  /** funcion que muestra los detalles del equipo*/
  verDetalles(id, opcion) {
    this.dialogService.open(VerDetalleComponent, {
      context: {
        id: id,
        opcion: opcion,
      },
    });
  }

  getCpuById(id) {
    this.cpuService.getCpuById(id).subscribe(data => {
      this.cpuactivar = data;
    });
  }
  getMonitorById(id) {
    this.monitorService.getMonitorById(id).subscribe(data => {
      this.monitoractivar = data;
    });
  }
  getLaptopById(id) {
    this.laptopService.getLaptopById(id).subscribe(data => {
      this.laptopactivar = data;
    });
  }
  getTabletById(id) {
    this.tabletService.getTabletById(id).subscribe(data => {
      this.tabletactivar = data;
    });
  }
  getTecladoById(id) {
    this.tecladoService.getTecladoById(id).subscribe(data => {
      this.tecladoactivar = data;
    });
  }
  getMouseById(id) {
    this.mouseService.getMouseById(id).subscribe(data => {
      this.mouseactivar = data;
    });
  }
  getDiademaById(id) {
    this.diademaService.getDiademaById(id).subscribe(data => {
      this.diademaactivar = data;
    });
  }
  getEstado() {
    this.estadoInventarioServive.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estadoInventariodata = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estadoInventariodata.length; i++) {
        this.estadoInventario.push({'id': this.estadoInventariodata[i].id, 'value': this.estadoInventariodata[i].estado});

      }
    });
  }
 /** funcion que activara de nuevo un equipo y se mostrara un dropdown de cual es el estado del equipo*/
  async ActivarEquipo(idEquipo, tiposEquipo) {
    switch (this.tipoEquipos) {
      case 'CPU':
        this.getCpuById(idEquipo);
        break;
      case 'MONITOR':
        this.getMonitorById(idEquipo);
        break;
      case 'LAPTOP':
        this.getLaptopById(idEquipo);
        break;
      case 'TABLET':
        this.getTabletById(idEquipo);
        break;
      case 'TECLADO':
        this.getTecladoById(idEquipo);
        break;
      case 'MOUSE':
        this.getMouseById(idEquipo);
        break;
      case 'DIADEMA':
        this.getDiademaById(idEquipo);
        break;
      default:
        break;
    }

    // Inicializando opciones de estadosInventarios de baja que tendrá el dropdown dentro del sweet alert,
    // correspondiendo con los obtenidos desde el microservicio
    const opciones = {};
    for (let i = 0; i < this.estadoInventario.length; i++) {
      opciones[this.estadoInventario[i]['id']] = this.estadoInventario[i]['value'];
    }
    // Modal Sweet Alert con Dropdown
    const {value: motivo} = await swal2.fire({
      title: '¿Deseas dar de Alta este Equipo?',
      text: '.',
      type: 'warning',
      input: 'select',
      inputOptions: opciones,
      inputPlaceholder: 'Selecciona un estado de Alta',
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value !== '') {
            resolve();
          } else {
            resolve('Selecciona una opción válida');
          }
        });
      },
    });
    if (motivo) { // Si ya ha sido seleccionada una opción, debe de desactivar
      switch (this.tipoEquipos) {
        case 'CPU':
          this.cpuactivar['idEstadoInventario'] = Number(motivo);
          this.cpuactivar['activo'] = 1;
          this.cpuService.put(idEquipo, this.cpuactivar).subscribe( result => {
            this.cpuService.postSocket({status: 'ok'}).subscribe(x => {});
            this.cpuService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'MONITOR':
          this.monitoractivar['idEstadoInventario'] = Number(motivo);
          this.monitoractivar['activo'] = 1;
          this.monitorService.put(idEquipo, this.monitoractivar).subscribe(result => {
            this.monitorService.postSocket({status: 'ok'}).subscribe(x => {});
            this.monitorService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });

          break;
        case 'LAPTOP':
          this.laptopactivar['idEstadoInventario'] = Number(motivo);
          this.laptopactivar['activo'] = 1;
          this.laptopService.put(idEquipo, this.laptopactivar).subscribe(result => {
            this.laptopService.postSocket({status: 'ok'}).subscribe(x => {});
            this.laptopService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'TABLET':
          this.tabletactivar['idEstadoInventario'] = Number(motivo);
          this.tabletactivar['activo'] = 1;
          this.tabletService.put(idEquipo, this.tabletactivar).subscribe(result => {
            this.tabletService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tabletService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'TECLADO':
          this.tecladoactivar['idEstadoInventario'] = Number(motivo);
          this.tecladoactivar['activo'] = 1;
          this.tecladoService.put(idEquipo, this.tecladoactivar).subscribe(result => {
            this.tecladoService.postSocket({status: 'ok'}).subscribe(x => {});
            this.tecladoService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'MOUSE':
          this.mouseactivar['idEstadoInventario'] = Number(motivo);
          this.mouseactivar['activo'] = 1;
          this.mouseService.put(idEquipo, this.mouseactivar).subscribe(result => {
            this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
            this.mouseService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        case 'DIADEMA':
          this.diademaactivar['idEstadoInventario'] = Number(motivo);
          this.diademaactivar['activo'] = 1;
          this.diademaService.put(idEquipo, this.diademaactivar).subscribe(result => {
            this.diademaService.postSocket({status: 'ok'}).subscribe(x => {});
            this.diademaService.postSocketBaja({status: 'ok'}).subscribe(x => {});
          });
          break;
        default:
          break;
      }
      swal2.fire({
        type: 'success',
        title: '¡El aquipo se ha dado de Alta exitosamente!',
      }).then((resultado => {
        // window.location.reload();
        this.getEquipos(this.tipoEquipos);
      }));

    }

  }

  ngOnInit() {
    this.connect();
    this.getEstado();
    this.getEquipo();
    /** Se inicializa los nombres de las columnas*/
    this.cols = [ 'detalle', 'id', 'numeroSerie', 'marca', 'activo', 'acciones' ];
    this.getPermisos();
    this.estaActivo();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.bajasInventarioUsuario.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.bajasInventarioUsuario.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
