import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef} from '@nebular/theme';
import {BancoComponent} from '../../banco/banco.component';
import {Empresa, EmpresaData} from '../../../../../@core/data/interfaces/catalogos/empresa';
import {Router} from '@angular/router';

/** Componente del modal del catálogo para "crear empresa". */
@Component({
  selector: 'ngx-empresa-create',
  templateUrl: './empresa-create.component.html',
  styleUrls: ['./empresa-create.component.scss'],
})
export class EmpresaCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idEmpresa obtenido. */
  @Input() idEmpresa: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de una empresa. */
  empresaForm: FormGroup;
  /** Bandera para verifica que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras y números. */
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {EmpresaData} empresaService Servicio del componente empresa para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private fb: FormBuilder, protected ref: NbDialogRef<BancoComponent>, private empresaService: EmpresaData,
              private router: Router) {
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarEmpresa() {
    if (this.empresaForm.invalid) {
      return;
    }
    this.submitted = true;
    this.empresaService.post(this.empresaForm.value).subscribe((result) => {
      this.empresaForm.reset();
      this.router.navigate(['/modulos/rrhh/catalogos/empresa']);
      this.empresaForm.reset();
      this.ref.close();
    });
  }

  /** Función que obtiene la información de la empresa dependiendo de su ID. */
  getEmpresaById() {
    this.empresaService.getEmpresasById(this.idEmpresa).subscribe( data => {
      this.empresas = data;
      this.empresaForm.controls['nombre'].setValue(this.empresas.nombre);
      this.empresaForm.controls['descripcion'].setValue(this.empresas.descripcion);
      this.empresaForm.controls['activo'].setValue(this.empresas.activo);

    });
  }

  /** Función que actualiza los datos de una empresa. */
  updateEmprea() {
    this.empresaService.put(this.idEmpresa, this.empresaForm.value).subscribe( result => {
      this.empresaForm.reset();
      this.ref.close();
      //window.location.reload();
    });
  }

  /** Función que da de baja una empresa, cambiando el atributo activo a 0. */
  /*bajaEmpresa() {
    this.empresaForm.controls['activo'].setValue(0);
    this.empresaService.put(this.idEmpresa, this.empresaForm.value).subscribe( result => {
      this.empresaForm.reset();
      this.ref.close();
      window.location.reload();
    });
  }*/

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    if (this.idTipo === 2) {
      this.getEmpresaById();
    } else if (this.idTipo === 3) {
      this.getEmpresaById();
    }
    this.empresaForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
      'idGrupo': 1,
    });
  }
}
