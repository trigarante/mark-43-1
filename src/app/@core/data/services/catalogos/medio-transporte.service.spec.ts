import {Global} from '../global';
import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {MedioTransporteService} from './medio-transporte.service';
import {environment} from '../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../matchers';

fdescribe('MedioTransporte service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: MedioTransporteService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 23,
    medio: 'TEST UNICO',
    activo: 0,
  },
    {
      id: 'mediotransporte',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [MedioTransporteService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(MedioTransporteService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/medio-transporte');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/medio-transporte', '/23');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/medio-transporte');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/medio-transporte', '/23/74');
      });
    });
  }
})
