import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Pagina, PaginasData} from '../../../../@core/data/interfaces/marketing/pagina';
import {AgregarCamposNodoComponent} from '../agregar-campos-nodo/agregar-campos-nodo.component';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {ActualizarCamposNodoComponent} from '../actualizar-campos-nodo/actualizar-campos-nodo.component';
import {environment} from "../../../../../environments/environment";
const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
@Component({
  selector: 'ngx-datos-nodos',
  templateUrl: './datos-nodos.component.html',
  styleUrls: ['./datos-nodos.component.scss'],
})
export class DatosNodosComponent implements OnInit {

  /** variable que se puede obtener el id de la pagina*/
  @Input() id: number;
  /** variable que se puede obtener el nombre de la aseguradora*/
  @Input() nombrenodo: string;
  /** posicion del array de la aseguradora*/
  @Input() nombre: number;
  @Input() tiponombre: string;
  cols: any[];
  pagina: Pagina;
  aseguradoras: any;
  dataSource: any;
  camposaseguradora: any;
  valoraseguradora: any;
  arregloconfiguracionjson: any[];
  // sockets
  private stompClient = null;

  constructor(private paginaService: PaginasData, public dialogService: NbDialogService ,
              protected ref: NbDialogRef<DatosNodosComponent>) { }
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'datosnodo');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log(frame);
      _this.stompClient.subscribe('/task/panelDatosNodo', (content) => {
          _this.showMessage(JSON.parse(content.body));
          setTimeout(() => {
            _this.getdatos();
          }, 500);
      });
    });
  }
  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getdatos() {
    this.paginaService.getPaginaById(this.id).subscribe(data => {
      this.pagina = data;
      this.aseguradoras = JSON.parse(this.pagina.configuracion);

      /** obtiene solo los campos name*/
      this.camposaseguradora = Object.keys(this.aseguradoras[this.tiponombre].aseguradoras[this.nombre][this.nombrenodo][0]);
      /** obtiene solo los campos value*/
      this.valoraseguradora = Object.values(this.aseguradoras[this.tiponombre].aseguradoras[this.nombre][this.nombrenodo][0]);

      this.arregloconfiguracionjson = [];
      for (let i = 0; i < this.camposaseguradora.length; i++) {
        this.arregloconfiguracionjson.push({'campo': this.camposaseguradora[i], 'value': this.valoraseguradora[i]});
      }

      this.dataSource = new MatTableDataSource(this.arregloconfiguracionjson);

    });
  }
  agregarCampos() {
    this.dialogService.open(AgregarCamposNodoComponent, {
      context: {
        id: this.id,
        nombrenodo: this.nombrenodo,
        posicionnodo: this.nombre,
        tiponombre: this.tiponombre,

      },
    }).onClose.subscribe( x => {
      this.getdatos();
    });
  }
  modificarDatos(campo, nombre) {

    this.dialogService.open(ActualizarCamposNodoComponent, {
      context: {
        id: this.id,
        nombrenodo: this.nombrenodo,
        posicionnodo: this.nombre,
        campo: campo,
        nombre: nombre,
        tiponombre: this.tiponombre,

      },
    }).onClose.subscribe( x => {
      this.getdatos();
    });

  }
  /** funcion para cerrar la ventana*/
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.connect();
    this.getdatos();
    this.cols = ['campo', 'valor', 'acciones'];
  }

}
