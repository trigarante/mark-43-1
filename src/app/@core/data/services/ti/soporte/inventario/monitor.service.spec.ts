import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Monitor Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 14,
    idEmpleado: null,
    nombre: null,
    observacionesInventario: 'medio uso',
    estadoInventario: 'A ESTADO',
    idEstadoInventario: 12,
    idObservacionesInventario: 2,
    numSerie: '0001M',
    idMarca: 1,
    dimensiones: '27X20',
    fechaRecepcion: '2019-09-24T16:11:42.000+0000',
    fechaSalida: null,
    comentarios: 'CLON',
    activo: 1,
    usuario: null,
    nombreMarca: 'ALLENWARE',
    idSede: 2,
    origenRecurso: 'renta',
    sedeNombre: 'QUERETARO',
  },
    {
      id: 'monitor',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/monitor');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/monitor', '/14');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/monitor');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/monitor', '/14/74');
      });
    });
  }
});
