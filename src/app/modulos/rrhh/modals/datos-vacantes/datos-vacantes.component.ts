import {Component, Input, OnInit} from '@angular/core';
import {Vacante, VacanteData} from '../../../../@core/data/interfaces/catalogos/vacante';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {VacantesComponent} from '../../vacantes/vacantes.component';

@Component({
  selector: 'ngx-datos-vacantes',
  templateUrl: './datos-vacantes.component.html',
  styleUrls: ['./datos-vacantes.component.scss'],
})
export class DatosVacantesComponent implements OnInit {
  @Input() idVacante: number;
  vacantes: Vacante;
  area: string;
  empresa: string;
  idGrupo: number;
  id: number;
  idArea: number;
  idEmpresa: number;
  idPuesto: number;
  idSede: number;
  idSubarea: number;
  idTipoPuesto: string;
  nombre: string;
  puesto: string;
  sede: string;
  subarea: string;
  tipoUsuario: string;
  activo: number;

  constructor(protected vacantesService: VacanteData, protected ref: NbDialogRef<VacantesComponent>,
              private dialogService: NbDialogService) {
  }

  getVacantesById() {
    this.vacantesService.getVacantesById(this.idVacante).subscribe(data => {
      this.vacantes = data;
      this.nombre = data.nombre;
      this.subarea = data.subarea;
      this.puesto = data.puesto;
      this.idTipoPuesto = data.puestoTipo;
      this.sede = data.sede;
    });
  }
  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getVacantesById();
  }

}
