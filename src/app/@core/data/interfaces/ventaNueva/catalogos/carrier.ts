import {Observable} from 'rxjs';

export interface Carrier {
  id: string;
  carrier: string;
  activo: string;
}

export abstract class CarrierData {
  abstract get(): Observable<Carrier[]>;
  abstract getCarrierById(id): Observable<Carrier>;
  abstract post(carrier): Observable<Carrier>;
  abstract put(id, carrier): Observable<Carrier>;

}
