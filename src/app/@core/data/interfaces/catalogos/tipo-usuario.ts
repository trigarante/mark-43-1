import {Observable} from 'rxjs';

export interface TipoUsuario {
  id: number;
  tipo: string;
  descripcion: string;
  activo: number;
}

export abstract class TipoUsuarioData {
  abstract get(): Observable<TipoUsuario[]>;

  abstract post(usuarioTipo): Observable<TipoUsuario>;

  abstract put(idUsuarioTipo, usuarioTipo): Observable<TipoUsuario>;

  abstract getPuestoTipoById(idUsuarioTipo): Observable<TipoUsuario>;
}
