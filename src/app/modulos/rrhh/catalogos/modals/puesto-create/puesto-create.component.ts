import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Puesto, PuestoData} from '../../../../../@core/data/interfaces/catalogos/puesto';
import {NbDialogRef} from '@nebular/theme';
import {PuestoComponent} from '../../puesto/puesto.component';
import {Router} from '@angular/router';
import {Subarea, SubareaData} from '../../../../../@core/data/interfaces/catalogos/subarea';
import {map} from 'rxjs/operators';
import {TipoUsuario, TipoUsuarioData} from '../../../../../@core/data/interfaces/catalogos/tipo-usuario';
/** Componente del modal del catálogo para "crear puesto". */
@Component({
  selector: 'ngx-puesto-create',
  templateUrl: './puesto-create.component.html',
  styleUrls: ['./puesto-create.component.scss'],
})
export class PuestoCreateComponent implements OnInit {
  /** Variable que almacena el idTipo obtenido. */
  @Input() idTipo: number;
  /** Variable que almacena el idPuesto obtenido. */
  @Input() idPuesto: number;
  /** Variable que permite la inicialización del formulario para realizar la adición de un puesto. */
  puestoCreateForm: FormGroup;
  /** Bandera para verificar que el formulario ha sido enviado. */
  submitted: boolean;
  /** Expresión regular para letras. */
  letras: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z]+/;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  puesto: Puesto;
  subArea: Subarea[];
  subAreas: any[];
  tipoUsuario: TipoUsuario[];
  tipoUsuarios: any[];
  puestoEliminado: boolean;
  /**
   * El método constructor invoca Servicios, Formularios (con control de validaciones) y Rutas.
   *
   * @param {FormBuilder} fb Inicializa el grupo de control de validaciones para el formulario.
   * @param {PuestoData} puestoService Servicio del componente puesto para poder invocar los métodos.
   * @param {NbDialogRef<CompetenciasCandidatoComponent>} ref Referencia al diálogo (mensaje).
   * @param {Router} router Inicializa variable para poder navegar entre componentes. */
  constructor(private puestoService: PuestoData, private fb: FormBuilder, protected ref: NbDialogRef<PuestoComponent>,
              private router: Router, protected subAreaService: SubareaData, protected tipoUsuarioService: TipoUsuarioData) {
  }
  /** Función que actualiza la información de un puesto. */
  updatePuesto() {
    this.puestoService.getById(this.idPuesto).subscribe(data => {
      if(data.activo === 1) {
        this.puestoService.put(this.idPuesto, this.puestoCreateForm.value).subscribe(result => {
          this.puestoService.postSocket(this.puestoCreateForm.value).subscribe(x => {});
          this.puestoCreateForm.reset();
          //window.location.reload();
          this.ref.close();
        });
      }else{
        this.puestoEliminado =true;
      }
    });

  }

  getSubArea() {
    this.subAreaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(result => {
      this.subArea = result;
      this.subAreas = [];
      for (let i = 0; i < this.subArea.length; i++) {
        this.subAreas.push({'label': this.subArea[i].subarea, 'value': this.subArea[i].id});
      }
    });
  }

  /** Función que guarda los datos contenidos en el formulario, realizando una petición POST al Microservicio.
   * Además, reinicia el formulario, redirecciona a la página y realiza un refresh de la ventana. */
  guardarPuesto() {
    if (this.puestoCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.puestoService.post(this.puestoCreateForm.value).subscribe((result) => {
      this.puestoService.postSocket(this.puestoCreateForm.value).subscribe(x => {});
      this.puestoCreateForm.reset();
      this.ref.close();
      //window.location.reload();
      //this.router.navigate(['/modulos/rrhh/catalogos/puesto']);
    });
  }

  /** Función que obtiene toda la información del puesto dependiendo de su ID. */
  getPuestoById() {
    this.puestoService.getById(this.idPuesto).subscribe(data => {
      this.puesto = data;
      this.puestoCreateForm.controls['nombre'].setValue(this.puesto.nombre);
    });
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  getTipoUsuario() {
    this.tipoUsuarioService.get().pipe(map(result => {
      return result.filter(data => data.id !== 1);
    })).subscribe(result => {
      this.tipoUsuario = result;
      this.tipoUsuarios = [];
      for (let i = 0; i < this.tipoUsuario.length; i++) {
        this.tipoUsuarios.push({'label': this.tipoUsuario[i].tipo, 'value': this.tipoUsuario[i].id});
      }
    });
  }

  /** Función que inicializa el componente y el formulario. */
  ngOnInit() {
    this.getTipoUsuario();
    this.getSubArea();
    if (this.idTipo === 2) {
      this.getPuestoById();
    }
    this.puestoCreateForm = this.fb.group({
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'activo': 1,
    });
  }

}
