import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef} from '@nebular/theme';
import {ProspectoData} from '../../../../@core/data/interfaces/ventaNueva/prospecto';
import {map} from 'rxjs/operators';
import {ProductoSolicitudData} from '../../../../@core/data/interfaces/ventaNueva/producto-solicitud';


@Component({
  selector: 'ngx-datos-solicitud',
  templateUrl: './datos-solicitud.component.html',
  styleUrls: ['./datos-solicitud.component.scss'],
})
export class DatosSolicitudComponent implements OnInit {
  @Input() idProspecto: number;
  @Input() idProducto: number;
  productoSolicitud: any;
  prospecto: any;
  nombre: any;
  correo: any;
  genero: any;
  numero: any;
  edad: any;
  /**Variables que almacenan los valores del Producto*/
  cp: any;
  clave: any;
  marca: any;
  modelo: any;
  paquete: any;
  servicio: any;
  solicitud: any;
  descuento: any;
  movimiento: any;
  aseguradora: any;
  descripcion: any;
  /**Bandera para mostrar detalles de solicitud*/
  mostrarDetalle: boolean;

  constructor(protected ref: NbDialogRef<DatosSolicitudComponent>, private prospectoService: ProspectoData,
              private productoSolicitudService: ProductoSolicitudData) { }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    this.getProductoSolicitud();
    this.getProspecto();
  }

  getProductoSolicitud() {
    this.productoSolicitudService.getProductoSolicitud(this.idProducto).subscribe( data => {
      if (data !== null) {
        this.solicitud = JSON.parse(data.datos);
        if (this.solicitud.aseguradora === 'TODAS') {
          this.mostrarDetalle = false;
        }else { this.mostrarDetalle = true; }
        this.cp = this.solicitud.cp;
        this.clave = this.solicitud.clave;
        this.marca = this.solicitud.marca;
        this.modelo = this.solicitud.modelo;
        this.paquete = this.solicitud.paquete;
        this.servicio = this.solicitud.servicio;
        this.descuento = this.solicitud.descuento;
        this.movimiento = this.solicitud.movimiento;
        this.aseguradora = this.solicitud.aseguradora;
        this.descripcion = this.solicitud.descripcion;
      }
    });
  }

  getProspecto() {
    this.prospectoService.getProspectoById(this.idProspecto).subscribe(result => {
      this.nombre = result.nombre;
      this.correo = result.correo;
      this.genero = result.sexo;
      this.edad = result.edad;
      this.numero = result.numero;
    });
  }

}
