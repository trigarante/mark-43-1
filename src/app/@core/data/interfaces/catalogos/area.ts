import {Observable} from 'rxjs';

export interface Area {
  id: number;
  nombre: string;
  descripcion: string;
  idSede: number;
  activo: number;
  codigo: string;
  idMarcaEmpresarial: number;
}

export abstract class AreaData {
  abstract get(): Observable<Area[]>;

  abstract post(area): Observable<Area>;

  abstract getAreaById(idArea): Observable<Area>;

  abstract put(idArea, area): Observable<Area>;
}
