import {Observable} from 'rxjs';

export interface TipoConvenio {
  id: number;
  tipo: string;
  regla: string;
  activo: number;
}

export abstract class TipoConvenioData {
  abstract get(): Observable<TipoConvenio[]>;

  abstract post(tipoConvenio): Observable<TipoConvenio>;

  abstract getTipoCoberturaById(idTipoConvenio): Observable<TipoConvenio>;

  abstract put(idTipoConvenio, tipoConvenio): Observable<TipoConvenio>;
}
