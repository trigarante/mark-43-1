import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarRespuestasComponent } from './generar-respuestas.component';

describe('GenerarRespuestasComponent', () => {
  let component: GenerarRespuestasComponent;
  let fixture: ComponentFixture<GenerarRespuestasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerarRespuestasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarRespuestasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
