import {Component, Input, OnInit} from '@angular/core';
import {AnalisisCompetencia, AnalisisCompetenciaData} from '../../../../@core/data/interfaces/comerciales/analisis-competencia';
import {AsignarAnalisisCompetenciaComponent} from '../asignar-analisis-competencia/asignar-analisis-competencia.component';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {SociosComponent} from '../../socios/socios.component';
import swal from 'sweetalert';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-datos-analisi-competencia',
  templateUrl: './datos-analisi-competencia.component.html',
  styleUrls: ['./datos-analisi-competencia.component.scss'],
})
export class DatosAnalisiCompetenciaComponent implements OnInit {
@Input() idCompetencia: number;
@Input() idAnalisisCompetencia: number;
  precios: number;
  participacion: number;
  activo: number;
  nombre: string;
  idProducto: number;
  idTipoCompetencia: number;
  competencia: string;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoCompetencia: any;
  tipoGiro: any;
  competencias: AnalisisCompetencia;
  permisos: any;
  escritura: boolean;
  constructor(protected ref: NbDialogRef<SociosComponent>, protected analisiCompetenciaService: AnalisisCompetenciaData,
              private dialogService: NbDialogService, private route: Router) { }

  ngOnInit() {
    this.getAnalaisisCompetencia();
    this.getPermisos();
  }

  getAnalaisisCompetencia() {
    this.analisiCompetenciaService.getAnalisisCompetenciaById(this.idAnalisisCompetencia).subscribe(result => {
        this.precios = result.precios;
        this.participacion = result.participacion;
        this.nombre = result.nombre;
        this.alias = result.alias;
        this.competencia = result.competencia;
        this.descripcion = result.descripcion;
        this.estado = result.estado;
        this.nombre = result.nombre;
        this.nombreComercial = result.nombreComercial;
        this.tipoCategoria = result.tipoSubRamo;
        this.tipoCompetencia = result.tipoCompetencia;
        this.tipoGiro = result.tipoRamo;
      },
    );
  }
  dismiss() {
    this.ref.close();
  }
  updateAnalisisCompetencia() {
    this.dialogService.open(AsignarAnalisisCompetenciaComponent, {
      context: {
        idTipo: 2,
        idAnalisisCompetencia: this.idAnalisisCompetencia,
      },
    });
  }

  bajaAnalisisComp() {
    this.analisiCompetenciaService.getAnalisisCompetenciaById(this.idAnalisisCompetencia).subscribe(data => {
      this.competencias = data; });
    swal({
      title: '¿Deseas dar de baja este Analisis?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.competencias.activo = 0;
        let competenciaForm: any;
        competenciaForm = {
          'activo': this.competencias.activo,
          'id': this.competencias.id,
          'idCompetencia': this.competencias.idCompetencia,
          'participacion': this.competencias.participacion,
          'precios': this.competencias.precios,
        };
        this.analisiCompetenciaService.put(this.idAnalisisCompetencia, competenciaForm).subscribe(result => {
          this.analisiCompetenciaService.postSocket({analisiscompetencia: competenciaForm}).subscribe(x => {});
        });
        swal('¡El Analisis se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.dismiss();
        }));
        this.route.navigate(['modulos/comercial/analisis-competencia']);
      }
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.analisisCompetencia.escritura;
  }

}
