import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPagoCreateComponent } from './tipo-pago-create.component';

describe('TipoPagoCreateComponent', () => {
  let component: TipoPagoCreateComponent;
  let fixture: ComponentFixture<TipoPagoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPagoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPagoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
