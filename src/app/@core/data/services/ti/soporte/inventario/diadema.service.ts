import {Injectable} from '@angular/core';
import {Diadema, DiademaData} from '../../../../interfaces/ti/soporte/inventario/diadema';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class DiademaService extends DiademaData {
  private baseURL;
  private socketURl;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  this.socketURl = environment.GLOBAL_SOCKET;
  }

  get(): Observable<Diadema[]> {
    return this.http.get<Diadema[]>( this.baseURL + 'v1/diadema');
  }

  post(diadema: Diadema): Observable<Diadema> {
    return this.http.post<Diadema>(this.baseURL + 'v1/diadema', diadema);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'diadema/saveDiadema', json);
  }
  postSocketBaja(json): Observable<JSON> {
    return this.http.post<JSON>(this.socketURl + 'bajadiadema/saveBajaDiadema', json);
  }
  getDiademaById(idDiadema): Observable<Diadema> {
    return this.http.get<Diadema>(this.baseURL + 'v1/diadema/' + idDiadema);
  }
  put(idDiadema, diadema: Diadema): Observable<Diadema> {
    return this.http.put<Diadema>(this.baseURL + 'v1/diadema/' + idDiadema + '/'
      + sessionStorage.getItem('Empleado') , diadema);
  }
}
