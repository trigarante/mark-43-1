import {Component, OnInit, ViewChild} from '@angular/core';
import {Convenios, ConveniosData} from '../../../@core/data/interfaces/comerciales/convenios';
import {map} from 'rxjs/operators';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {DatosConveniosComponent} from '../modals/datos-convenios/datos-convenios.component';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-convenios',
  templateUrl: './convenios.component.html',
  styleUrls: ['./convenios.component.scss'],
})
export class ConveniosComponent implements OnInit {
  cols: any[];
  /**Arreglos aux de los microservicios*/
  convenios: Convenios[];
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  permisos: any;
  escritura: boolean;
  activo: boolean;
  constructor(private conveniosService: ConveniosData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {}

  ngOnInit() {
    this.getConvenios(0);
    this.cols = [
      'detalle',
      'subcategoria',
      'subramo',
      'divisa',
      'cantidad',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  getConvenios(mensajeAct) {
    this.conveniosService.get().pipe(map(result => {
      return result.filter(data =>  data.activo === 1);
    })).subscribe(data => {
      this.convenios = [];
      this.convenios = data;
      for (let i = 0; i < this.convenios.length; i++) {
        let objetivo: any = this.convenios[i].cantidad.toString();
        let formatNumber: any = '';
        objetivo = objetivo.replace(/,/g, '');
        if (objetivo.length > 3 && objetivo.length < 7) {
          formatNumber = objetivo.slice(0, objetivo.length - 3) + ',' + objetivo.slice(-3);
          this.convenios[i].cantidad = formatNumber;
        } else if (objetivo.length > 6 && objetivo.length < 10) {
          formatNumber = objetivo.slice(0, objetivo.length - 6) + ',' + objetivo.slice(-6, -3) + ',' + objetivo.slice(-3);
          this.convenios[i].cantidad = formatNumber;
        } else if (objetivo.length > 9 && objetivo.length < 13) {
          formatNumber = objetivo.slice(0, objetivo.length - 9) + ',' + objetivo.slice(-9, -6) + ',' + objetivo.slice(-6, -3) +
            ',' + objetivo.slice(-3);
          this.convenios[i].cantidad = formatNumber;
        }
      }
      this.dataSource = new MatTableDataSource(this.convenios); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `Convenios`);
  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  verDatos(idConvenio) {
    this.dialogService.open(DatosConveniosComponent, {
      context: {
        idConvenio: idConvenio,
      },
    }).onClose.subscribe(x => {
      this.getConvenios(1);
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'convenios');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelConvenios', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getConvenios(0);
        }, 500);
      });
    });
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.convenios.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.convenios.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
}
