import {Observable} from 'rxjs';


export interface Racks {
  id: number;
  idEmpleado?: number;
  idObservacionesInventario: number;
  observacionesInventario?: string;
  idEstadoInventario: number;
  estadoInventario?: string;
  folio: number;
  idMarca: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  observaciones?: string;
  sedeNombre?: string;
}
export abstract class RacksData {
  abstract get(): Observable<Racks[]>;
  abstract post(racks): Observable<Racks>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getRacksById(idRacks): Observable<Racks>;
  abstract put(idRacks, racks): Observable<Racks>;
}
