import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './@core/data/services/login/auth.guard';
import {AuthComponent} from './auth/auth.component';

const routes: Routes = [
  { path: 'modulos', loadChildren: 'app/modulos/modulos.module#ModulosModule', canActivate: [AuthGuard]},
  { path: 'auth', component: AuthComponent},
  {
    path: 'login',
    component: LoginComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
    ],
  },
  { path: 'login/:log', component: LoginComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'modulos', redirectTo: 'modulos', pathMatch: 'full' , canActivate: [AuthGuard]},
  { path: '**', redirectTo: 'login' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
