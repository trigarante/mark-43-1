import {Component, Input, OnInit} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {SubareaData, Subarea} from '../../../../@core/data/interfaces/catalogos/subarea';
import {SedeData, Sede} from '../../../../@core/data/interfaces/catalogos/sede';
import {GrupoData, Grupo} from '../../../../@core/data/interfaces/catalogos/grupo';
import {EmpresaData, Empresa} from '../../../../@core/data/interfaces/catalogos/empresa';
import {AreaData, Area} from '../../../../@core/data/interfaces/catalogos/area';
import {MarcaEmpresarialData, MarcaEmpresarial} from '../../../../@core/data/interfaces/catalogos/marca-empresarial';
import {CampanaMarketing, CampanaMarketingData} from '../../../../@core/data/interfaces/marketing/campana';
import {GuardadoComponent} from '../guardado/guardado.component';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-actualizar-campana',
  templateUrl: './actualizar-campana.component.html',
  styleUrls: ['./actualizar-campana.component.scss'],
})
export class ActualizarCampanaComponent implements OnInit {
  @Input() id: number;
  actualizarCampanaForm: FormGroup;
  campanaMarketing: CampanaMarketing;
  /**Las siguientes variables son banderas para los campos vacios*/
  mostrarGrupo: boolean;
  mostrarEmpresa: boolean;
  mostrarSede: boolean;
  mostrarArea: boolean;
  mostrarSubarea: boolean;
  mostrarcategorias: boolean;
  mostrarMarca: boolean;
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: Sede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Grupos: Grupo[];
  /** Arreglo auxiliar para puesto. */
  grupo: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  empresas: Empresa[];
  /** Arreglo auxiliar para puesto. */
  empresa: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  areas: Area[];
  /** Arreglo auxiliar para puesto. */
  area: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  subareas: Subarea[];
  /** Arreglo auxiliar para subareas. */
  subarea: any[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  marcas: MarcaEmpresarial[];
  areaget: Subarea;
  sedeget: any;
  empresaget: any;
  marcaget: any;
  grupoempresarialget: any;
  grupoget: any;

  constructor(protected ref: NbDialogRef<ActualizarCampanaComponent>, private fb: FormBuilder,
              protected subareaService: SubareaData, private sedesService: SedeData,
              private grupoService: GrupoData, private empresaService: EmpresaData, private areaService: AreaData,
              private marcaEmpresarialService: MarcaEmpresarialData, protected campanaMarketingService: CampanaMarketingData,
              private router: Router, public dialogService: NbDialogService ) {
    this.actualizarCampanaForm = this.fb.group({
      'idSubarea': new FormControl('', Validators.compose([Validators.required])),
      'nombre': new FormControl('', Validators.compose([Validators.required])),
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'idGrupo': new FormControl(),
      'idEmpresa': new FormControl(),
      'idMarcaEmpresarial': new FormControl(),
      'idSede': new FormControl(),
      'idArea': new FormControl(),
    });
  }

  getCampanaById() {
    this.campanaMarketingService.getCampanasMarketingBiId(this.id).subscribe(data => {
      this.campanaMarketing = data;
      this.actualizarCampanaForm.controls['idSubarea'].setValue(this.campanaMarketing.idSubarea);
      this.actualizarCampanaForm.controls['nombre'].setValue(this.campanaMarketing.nombre);
      this.actualizarCampanaForm.controls['descripcion'].setValue(this.campanaMarketing.descripcion);
      this.subareaService.getById(this.campanaMarketing.idSubarea).subscribe(data2 => {
        this.areaget = data2;

        this.getSubarea(this.areaget.idArea);

        this.areaService.getAreaById(this.areaget.idArea).subscribe(data3 => {
          this.sedeget = data3;
          this.getArea(this.sedeget.idSede);
          this.actualizarCampanaForm.controls['idArea'].setValue(this.sedeget.id);

          // this.marcaEmpresarialService.getMarcaEmpresarialById(this.marcaget.idMarcaEmpresarial).subscribe( data4 => {
          //   this.sedeget = data4;
          //   this.getMarcaEmpresarial(this.sedeget.idSede);
          //   this.actualizarCampanaForm.controls['idMarcaEmpresarial'].setValue(this.sedeget.id);

            this.sedesService.getSedeById(this.sedeget.idSede).subscribe(data5 => {
              this.empresaget = data5;
              this.getSedes(this.empresaget.idEmpresa);
              this.actualizarCampanaForm.controls['idSede'].setValue(this.empresaget.id);


              this.empresaService.getEmpresasById(this.empresaget.idEmpresa).subscribe(data6 => {
                this.grupoempresarialget = data6;
                this.getEmpresa(this.grupoempresarialget.idGrupo);
                this.actualizarCampanaForm.controls['idEmpresa'].setValue(this.grupoempresarialget.id);

                this.grupoService.getGrupoById(this.grupoempresarialget.idGrupo).subscribe(data7 => {
                  this.grupoget = data7;
                  this.getGrupo();
                  this.actualizarCampanaForm.controls['idGrupo'].setValue(this.grupoget.id);
                });
              });

            });
        });

      });
    });
  }

  getGrupo() {
    this.grupoService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.Grupos = data;
      if (this.Grupos.length === 0) {
        this.mostrarGrupo = true;
      }else {
        this.mostrarGrupo = false;
      }
      this.grupo = [];
      for (let i = 0; i < this.Grupos.length; i++) {
        this.grupo.push({'label': this.Grupos[i].nombre, 'value': this.Grupos[i].id});
      }
    });
  }

  getEmpresa(idGrupo) {
    this.empresaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idGrupo === idGrupo);
    })).subscribe(data => {
      this.empresas = data;
      if (this.empresas.length === 0) {
        this.mostrarEmpresa = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.empresa = [];
      for (let i = 0; i < this.empresas.length; i++) {
        this.empresa.push({'label': this.empresas[i].nombre, 'value': this.empresas[i].id});
      }
    });
  }

  getSedes(idEmpresa) {
    this.sedesService.get().pipe(map(result => {
      return result.filter( valor => valor.idEmpresa === idEmpresa);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
        this.mostrarEmpresa = false;
      }
      this.Sede = [];
      for (let i = 0; i < this.Sedes.length; i++) {
        this.Sede.push({'label': this.Sedes[i].nombre, 'value': this.Sedes[i].id});
      }
    });
  }
  // getMarcaEmpresarial(idSede) {
  //   this.marcaEmpresarialService.get().pipe(map(result => {
  //     return result.filter( valor => valor.idSede === idSede);
  //   })).subscribe(data => {
  //     this.marcas = data;
  //     if (this.marcas.length === 0) {
  //       this.mostrarMarca = true;
  //     }else {
  //       this.mostrarMarca = false;
  //     }
  //
  //   });
  //
  // }
  getArea(idSede) {
    this.areaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idSede === idSede);
    })).subscribe(data => {
      this.areas = data;
      if (this.areas.length === 0) {
        this.mostrarArea = true;
      }else {
        this.mostrarArea = false;
      }
      this.area = [];
      for (let i = 0; i < this.areas.length; i++) {
        this.area.push({'label': this.areas[i].nombre, 'value': this.areas[i].id});
      }
    });
  }

  getSubarea(idArea) {
    this.subareaService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1 && valor.idArea === idArea);
    })).subscribe(data => {
      this.subareas = data;
      if (this.subareas.length === 0) {
        this.mostrarSubarea = true;
      }else {
        this.mostrarSubarea = false;
      }
      this.subarea = [];
      for (let i = 0; i < this.subareas.length; i++) {
        this.subarea.push({'label': this.subareas[i].subarea, 'value': this.subareas[i].id});
      }
    });
  }
  guardar() {
    this.campanaMarketingService.getCampanasMarketingBiId(this.id).subscribe(data => {
      this.campanaMarketing = data;

      this.campanaMarketing['idSubarea'] = this.actualizarCampanaForm.value.idSubarea;
      this.campanaMarketing['nombre'] = this.actualizarCampanaForm.value.nombre;
      this.campanaMarketing['descripcion'] = this.actualizarCampanaForm.value.descripcion;



      this.campanaMarketingService.put(this.id, this.campanaMarketing).subscribe(result => {
        this.campanaMarketingService.postSocket({status: 'ok'}).subscribe(x => {});
        this.ref.close();
        this.router.navigate(['/modulos/marketing/campana/']);
      });
      this.dialogService.open(GuardadoComponent, {
        context: {
          id: this.id,
          modo: 3,
        },
      });
      this.router.navigate(['/modulos/marketing/campana/']);
    });

  }


  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getCampanaById();
  }

}
