import {Observable} from 'rxjs';

export class JwtResponse {
    token: string;
    status: number;
}

export abstract class JwtResponseData {
  abstract enviarCredenciales(credentials): Observable<JwtResponse>;
  abstract getPermisos(token): Observable<any>;
}
