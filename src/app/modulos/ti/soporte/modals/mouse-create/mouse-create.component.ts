import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  ObservacionesInventario,
  ObservacionesInventarioData,
} from '../../../../../@core/data/interfaces/ti/catalogos/observaciones-inventario';
import {EstadoInventario, EstadoInventarioData} from '../../../../../@core/data/interfaces/ti/catalogos/estado-inventario';
import {Mouse, MouseData} from '../../../../../@core/data/interfaces/ti/soporte/inventario/mouse';
import {NbDialogRef} from '@nebular/theme';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Marca, MarcaSoporteData} from '../../../../../@core/data/interfaces/ti/catalogos/marca';
import {SedeData, Sede} from '../../../../../@core/data/interfaces/catalogos/sede';
import {SelectItem} from 'primeng/api';
import {TipoSede, TipoSedeData} from '../../../../../@core/data/interfaces/ti/catalogos/tipo-sede';

@Component({
  selector: 'ngx-mouse-create',
  templateUrl: './mouse-create.component.html',
  styleUrls: ['./mouse-create.component.scss'],
})
export class MouseCreateComponent implements OnInit {
  @Input() idTipo: number;
  @Input() idMouse: number;
  mouse: Mouse;
  mouseCreateForm: FormGroup;
  submitted: boolean;
  estadoInventario: any[];
  estado: EstadoInventario[];
  ObservacionesInventario: any[];
  observaciones: ObservacionesInventario[];
  letrasNumeros: RegExp = /[A-Z-Ñ-\u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1 a-z-0-9]+/;
  /**Areglo que inicia los valores del Microservicio*/
  marcas: Marca[];
  /**Arreglo auxiliar para  marcas*/
  marca: any;

  origenEquipos: SelectItem[];
  /** Arreglo que se inicializa con los valores del Microservicio. */
  Sedes: TipoSede[];
  /** Arreglo auxiliar para puesto. */
  Sede: any[];

  mostrarSede: boolean;

  constructor(private fb: FormBuilder, protected ref: NbDialogRef<MouseCreateComponent>, private mouseService: MouseData,
              private router: Router, private estadoInventarioService: EstadoInventarioData,
              private observacionesInventarioService: ObservacionesInventarioData,
              private marcaService: MarcaSoporteData, private tipoSedeService: TipoSedeData) { }

  guardarMouse() {
    if (this.mouseCreateForm.invalid) {
      return;
    }
    this.submitted = true;
    this.mouseService.post(this.mouseCreateForm.value).subscribe((result) => {
      this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
      this.mouseCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'mouse']);
    });
  }
  getObservacionInventario() {
    this.observacionesInventarioService.get().pipe(map(data => this.observaciones = data)).subscribe(data => {
      this.ObservacionesInventario = [];
      for (let i = 0; i < this.observaciones.length; i++) {
        this.ObservacionesInventario.push({
          'label': this.observaciones[i].observaciones,
          'value': this.observaciones[i].id,
        });
      }
    });
  }
  getEstadoInventario() {
    this.estadoInventarioService.get().pipe(map(result => {
      return result.filter(data => data.idEstado === 1);
    })).subscribe(data => {
      this.estado = data;
      this.estadoInventario = [];
      for (let i = 0; i < this.estado.length; i++) {
        this.estadoInventario.push({'label': this.estado[i].estado, 'value': this.estado[i].id});
      }
    });
  }

  getMarca() {
    this.marcaService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1); })).subscribe(data => {
      this.marca = [];
      this.marcas = data;
      for (let i = 0; i < this.marcas.length; i++) {
        this.marca.push({'label': this.marcas[i].nombreMarca, 'value': this.marcas[i].id});
      }
    });
  }


  updateMouse() {
    this.mouseService.put(this.idMouse, this.mouseCreateForm.value).subscribe( result => {
      this.mouseService.postSocket({status: 'ok'}).subscribe(x => {});
      this.mouseCreateForm.reset();
      this.ref.close();
      this.router.navigate(['/modulos/ti/soporte/inventario', 'mouse']);
    });
  }
  getMouseById() {
    this.mouseService.getMouseById(this.idMouse).subscribe(data => {
      this.mouse = data;
      this.mouseCreateForm.controls['idMarca'].setValue(this.mouse.idMarca);
      this.mouseCreateForm.controls['idEstadoInventario'].setValue(this.mouse.idEstadoInventario);
      this.mouseCreateForm.controls['idObservacionesInventario'].setValue(this.mouse.idObservacionesInventario);
      this.mouseCreateForm.controls['comentarios'].setValue(this.mouse.comentarios);
      this.mouseCreateForm.controls['idEmpleado'].setValue(this.mouse.idEmpleado);
      this.mouseCreateForm.controls['idTipoSede'].setValue(this.mouse.idTipoSede);
      this.mouseCreateForm.controls['origenRecurso'].setValue(this.mouse.origenRecurso);
    });
  }
  getOrigen() {
    this.origenEquipos = [];
    this.origenEquipos.push({label: 'Propio', value: 'propio'});
    this.origenEquipos.push({label: 'Renta', value: 'renta'});

  }
  getSedes() {
    this.tipoSedeService.get().pipe(map(data => {
      return data.filter(result => result.activo === 1);
    })).subscribe(data => {
      this.Sedes = data;
      if (this.Sedes.length === 0) {
        this.mostrarSede = true;
      }else {
      }
    });
  }
  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    if (this.idTipo === 2) {
      this.getMouseById();
    }
    this.getObservacionInventario();
    this.getEstadoInventario();
    this.getMarca();
    this.getOrigen();
    this.getSedes();
    this.mouseCreateForm = this.fb.group({
      'idMarca': new FormControl('', Validators.compose([Validators.required])),
      'idEstadoInventario': new FormControl('', Validators.compose([Validators.required])),
      'idObservacionesInventario': new FormControl('', Validators.compose([Validators.required])),
      'comentarios': new FormControl('', Validators.compose([Validators.required])),
      'origenRecurso': new FormControl('', Validators.compose([Validators.required])),
      'idTipoSede': new FormControl('', Validators.compose([Validators.required])),
      'idEmpleado': new FormControl(),
      'activo': 1,
    });
  }

}
