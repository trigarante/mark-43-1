import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';
import {BolsaTrabajo, BolsaTrabajoData} from '../../interfaces/catalogos/bolsa-trabajo';

@Injectable()
export class BolsaTrabajoService extends BolsaTrabajoData {

  private baseURL;
  private socketURL;

  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
    this.socketURL = environment.GLOBAL_SOCKET;
  }

  getBolsaTrabajo(): Observable<BolsaTrabajo[]> {
    return this.http.get<BolsaTrabajo[]>(this.baseURL + 'v1/bolsa-trabajo');
  }

  post(bolsaTrabajo: BolsaTrabajo): Observable<BolsaTrabajo> {
    return this.http.post<BolsaTrabajo>(this.baseURL + 'v1/bolsa-trabajo', bolsaTrabajo);
  }
  postSocket(json): Observable<JSON> {
    return this.http.post<JSON>(environment.GLOBAL_SOCKET + 'bolsatrabajo/saveBolsaTrabajo', json);
  }

  getBolsaTrabajoById(idBolsaTrabajo): Observable<BolsaTrabajo> {
    return this.http.get<BolsaTrabajo>(this.baseURL + 'v1/bolsa-trabajo/' + idBolsaTrabajo);
  }

  put(idBolsaTrabajo, bolsaTrabajo: BolsaTrabajo): Observable<BolsaTrabajo> {
    return this.http.put<BolsaTrabajo>(this.baseURL + 'v1/bolsa-trabajo/' + idBolsaTrabajo + '/'
      + sessionStorage.getItem('Empleado'), bolsaTrabajo);
  }
}
