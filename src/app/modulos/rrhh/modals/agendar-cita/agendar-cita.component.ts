import {Component, Input, OnInit} from '@angular/core';
import {NbDateService, NbDialogRef} from '@nebular/theme';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import * as moment from 'moment';
import {SolicitudesData} from '../../../../@core/data/interfaces/rrhh/solicitudes';

/**
 * Componente del modal "agendar cita".
 */
@Component({
  selector: 'ngx-agendar-cita',
  templateUrl: './agendar-cita.component.html',
  styleUrls: ['./agendar-cita.component.scss'],
})

export class AgendarCitaComponent implements OnInit {
  time = {hour: 13, minute: 30};
  /** Variable que recibe el id de la solicitud. */
  @Input() idSolicitud: number;
  /** Variable para la fecha mínima. */
  min: Date;
  /** Variable para la fecha máxima. */
  max: Date;
  /** Variable para inicializar el formulario. */
  formAgendarCita: FormGroup;
  /** Bandera para verificar que el formulario se ha enviado.*/
  submitted: boolean;
  /** Variable que alamacena la fecha de cita. */
  fecha: any;
  /** Variable para conocer el número de día de la fecha de cita. */
  dia: number;
  /**Variable que desactiva lso botons para las peticiones HTTP*/
  inactivo: boolean;
  /**Variable para establecer la fecha actual en el calendario*/
  minDate: Date;
  solicitudEliminada: boolean;
  /**
   * El método constructor manda a llamar Servicios, además de que inicializa las variables min y max, siendo éstas fechas.
   *
   * @param {NbDialogRef<AgendarCitaComponent>} ref Variable para mostrar el diálogo.
   * @param {FormBuilder} fb Variable que inicializa el formulario.
   * @param {NbDateService<Date>} dateService Variable para mostrar fechas.
   * @param {SolicitudesData} solicitudesService Servicio del componente solicitudes para poder invocar los métodos.
   */
  constructor(protected ref: NbDialogRef<AgendarCitaComponent>, private fb: FormBuilder, protected dateService: NbDateService<Date>,
              private solicitudesService: SolicitudesData) {
    this.min = this.dateService.today();
    this.max = this.dateService.addDay(this.dateService.today(), 5);
  }

  /** Esta función cierra el modal. */
  dismiss() {
    this.ref.close();
  }



  /** Esta función permite validar el formulario en caso de que los datos no estén completos.
   *
   * Además, es la función encargada de hacer el proceso de agendar la cita.
   * */
  agendarCita() {
    this.inactivo = true;
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe( data => {
      console.log(data);
      if (Number(data.estado) === 1 && data.idEtapa === 0) {
        this.submitted = true;
        if (this.formAgendarCita.invalid) {
          return;
        }
        let fechaCita: any;
        fechaCita = moment(this.formAgendarCita.value.fechaCita).format('YYYY-MM-DD') + 'T' + this.formAgendarCita.value.horaCita;
        fechaCita = Date.parse(fechaCita);
        this.solicitudesService.postSocket(this.formAgendarCita.value).subscribe(d => {});
        this.solicitudesService.agendarCita(this.formAgendarCita.value.id, fechaCita).subscribe(
          result => {
            this.inactivo = true;
          },
          error => { },
          () => {
            this.formAgendarCita.reset();
            this.ref.close();
          },
        );
      }else {
        this.solicitudEliminada = true;
      }
    });


  }
  /** Validar que no se ingrese manualmente una fecha anterior a la actual*/
  validarFechaEscritaManualmente(date: Date) {
    const hoy = moment(new Date()).format('YYYYMMDD');
    const fecha = moment(date).format('YYYYMMDD');
    if (fecha < hoy) {
      this.inactivo = true;
    }else if (fecha >= hoy) {
      this.inactivo = false;
    }
  }

  blockearDate() {
    const date = new Date().toISOString().split('T')[0];
    document.getElementsByName('somedate')[0].setAttribute('min', date);
  }

  /** Función que permite verificar el formulario para asignar la fecha de cita, validando que sea un día laborable. */
  verificarFormulario() {
    // Creando variable de tipo Date para validar que el día introducido sea laborable
    if (this.formAgendarCita.valid) {
      this.fecha = new Date(this.formAgendarCita.value.fechaCita);  // Obtiene la fecha del día anterior
//      this.fecha.setDate(this.fecha.getDate() + 1); // Le sumamos un día para que concuerde con la fecha ingresada
      this.dia = this.fecha.getDay();
      if (this.dia === 0 || this.dia === 6) { // Si el dia de cita es sábado o domingo, no debe permitir asignar la cita
        return true;  // Botón deshabilitado
      } else {
        return false; // Botón habilitado
      }
    } else {
      return true;  // Botón deshabilitado
    }
  }

  getcita () {
    this.solicitudesService.getSolicitudById(this.idSolicitud).subscribe( data => {
      if (data.fechaCita) {
      let fecha: any;
      let hora: any;
      hora = new Date(data.fechaCita);
      hora = moment(hora).format('HH:mm');
      fecha = moment(data.fechaCita).format('YYYY-MM-DD');
      this.formAgendarCita.controls['fechaCita'].setValue(new Date(data.fechaCita));
      this.formAgendarCita.controls['horaCita'].setValue(hora);
    }
    });
  }
  /** Función que inicializa el componente, así como también el formulario (con control de validaciones). */
  ngOnInit() {
    this.minDate = new Date();
    this.blockearDate();
    this.getcita();
    this.formAgendarCita = this.fb.group({
      'fechaCita': new FormControl('', Validators.compose( [Validators.required])),
      'horaCita': new FormControl('', Validators.compose([Validators.required, Validators.min(8.00), Validators.max(19.00)])),
      'id': this.idSolicitud,
    });
  }
}
