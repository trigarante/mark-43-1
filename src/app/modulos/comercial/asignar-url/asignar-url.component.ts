import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AsignarUrl, AsignarUrlData} from '../../../@core/data/interfaces/comerciales/asignar-url';
import {NbDialogService, NbToastrService} from '@nebular/theme';
import {UrlCreateComponent} from '../modals/url-create/url-create.component';
import {map} from 'rxjs/operators';
import swal from 'sweetalert';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

const SockJS = require('sockjs-client');
const Stomp = require('stompjs');
import {environment} from '../../../../environments/environment';
import {Router} from "@angular/router";
@Component({
  selector: 'ngx-asignar-url',
  templateUrl: './asignar-url.component.html',
  styleUrls: ['./asignar-url.component.scss'],
})
export class AsignarUrlComponent implements OnInit {
  cols: any[];
  asignarUrl: AsignarUrl[];
  url: AsignarUrl;
  dataSource: any;
  // socket
  disabled = true;
  private stompClient = null;
  datos: any[] = [];
  newDatos: any;
  permisos: any;
  escritura: boolean;
  activo: boolean;

  constructor(private asignarUrlService: AsignarUrlData,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private router: Router,
              ) {
  }
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //

  ngOnInit() {
    this.getAsignarUrl(0);
    this.cols = [
      'alias',
      'url',
      'descripcion',
      'acciones',
    ];
    this.connect();
    this.getPermisos();
    this.estaActivo();
  }

  getUrlById(idSocio) {
    this.asignarUrlService.getAsignarUrlById(idSocio).subscribe(data => {
      this.url = data;
    });
  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getAsignarUrl(mensajeAct) {
    this.asignarUrlService.get().pipe(map(result => {
      return result.filter(data => data.activo === 1 && data.idEstadoSocio === 1);
    })).subscribe(data => {
      this.asignarUrl = [];
      this.asignarUrl = data;
      this.dataSource = new MatTableDataSource(this.asignarUrl); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
    if (mensajeAct === 1) {
      this.showToast();
    }
  }

  /** Funcion que muestra mensaje al dar clic en el boton actualizar*/
  showToast() {
    this.toastrService.success('!!Datos Actualizados¡¡', `URL`);
  }

  updateUrl(idUrl, idSocio) {
    this.dialogService.open(UrlCreateComponent, {
      context: {
        idUrl: idUrl,
        idSocio: idSocio,
        idTipo: 2,
      },
    }).onClose.subscribe( x => this.getAsignarUrl(1));
  }

  bajaUrl(idSocio) {
  this.getUrlById(idSocio);
    swal({
      title: '¿Deseas dar de baja esta URL?',
      text: 'Una vez dado de baja, no podrás verlo de nuevo.',
      icon: 'warning',
      dangerMode: true,
      buttons: {
        cancel: {
          text: 'Cancelar',
          value: false,
          visible: true,
          className: '',
          closeModal: true,
        },
        confirm: {
          text: 'Dar de baja',
          value: true,
          visible: true,
          className: '',
          closeModal: true,
        },
      },
    }).then((valor) => {
      if (valor) {
        this.url['activo'] = 0;
        this.asignarUrlService.put(idSocio, this.url)
        .subscribe( result => {
          this.asignarUrlService.postSocket({status: 'ok'}).subscribe(x => {});
        });
        swal('¡La URL se ha dado de baja exitosamente!', {
          icon: 'success',
        }).then((resultado => {
          this.getAsignarUrl(0);
        }));
      }
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
    if (connected) { this.datos = []; }
  }

  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET + 'url');
    this.stompClient = Stomp.over(socket);
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);
      console.log(frame);

      _this.stompClient.subscribe('/task/panelUrl', (content) => {
        _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          _this.getAsignarUrl(0);
        }, 500);
      });
    });
  }

  showMessage(message) {
    this.dataSource.data.unshift(message);
    this.dataSource.data = this.dataSource.data.slice();
  }
  getPermisos() {
    this.permisos = JSON.parse(window.localStorage.getItem('User'));
    this.escritura = this.permisos.modulos.url.escritura;
  }

  estaActivo() {
    this.activo = this.permisos.modulos.url.activo;
    if (this.activo === false ) {
      this.router.navigate(['/modulos/dashboard']);
    }
  }
}
