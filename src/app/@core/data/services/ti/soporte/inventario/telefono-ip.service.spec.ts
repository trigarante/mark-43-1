import {environment} from '../../../../../../../environments/environment';
import {Conexion, CustomMatchers} from '../../../matchers';

fdescribe('Telefono ip Service', () => {
  const url = environment.GLOBAL_SERVICIOS;
  const body = [{
    id: 8,
    idEmpleado: null,
    nombre: null,
    estadoInventario: 'RAYADO',
    idObservacionesInventario: 1,
    idEstadoInventario: 13,
    folio: '0001T',
    idMarca: 1,
    idSede: 1,
    origenRecurso: 'propio',
    activo: 1,
    fechaRecepcion: '2019-09-24T16:58:16.000+0000',
    fechaSalida: null,
    usuario: null,
    observaciones: 'nuevo',
    nombreMarca: 'ALLENWARE',
    sedeNombre: 'VALLEJO',
  },
    {
      id: 'telefonoIp',
    }];
  Conexion(url, body[1].id);
  const status = localStorage.getItem('status' + body[1].id);
  beforeAll(() => {
    jasmine.addMatchers(CustomMatchers);
  });
  it('Conexión a servicio correcto', () => {
    expect(status).toBeConection(url);
  });
  if (status === 'OnLine') {
    describe('Service', () => {
      it('Status GET Correcto', () => {
        expect(body).toBeStatusGet(url, 'v1/telefonoIp');
      });
      it('Status GET by Id Correcto', () => {
        expect(body).toBeStatusGetById(url, 'v1/telefonoIp', '/8');
      });
      it('Status POST Correcto', () => {
        expect(body).toBeStatusPost(url, 'v1/telefonoIp');
      });
      it('Status PUT Correcto', () => {
        expect(body).toBeStatusPut(url, 'v1/telefonoIp', '/8/74');
      });
    });
  }
});
