import {Global} from '../global';
import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {SepomexService} from './sepomex.service';

describe('Puesto service', () => {
  const baseURL = Global.url;
  let injector: TestBed;
  let service: SepomexService;
  let httpMock: HttpTestingController;
  let httpclient: HttpClient;
  const body = {
    id: 32,
    idEmpresa: 1,
    idPais: 1,
    nombre: 'TEST UNICO',
    cp: '15300',
    colonia: '',
    calle: '',
    numero: '',
    codigo: 'SDSDSD',
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [SepomexService, HttpClient],
  }));
  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(SepomexService);
    httpMock = injector.get(HttpTestingController);
    httpclient = TestBed.get(HttpClient);
  });
  describe('funsiona servicio correctamente', () => {
    describe('get', () => {
      it('status 200', (done) => {
        const chai = require('chai');
        const chaiHttp = require('chai-http');
        const url = baseURL;
        chai.use(chaiHttp);
        const go = validarFuncion(service.getColoniaByCp.toString());
        chai.request(url)
          .get(go + '01220')
          .end((err, res) => {
            console.log('SepomexService getColoniaByCp GET 200 status ---> ', res.status);
            if (!err)
              if (res.status === 404) {
                fail(`La funcion getColoniaByCp debe retornar un status 200, Pero retorno un status ${res.status}\n
              Esto se debe a que el prefijo '${go}' es incorrecto\n` + service.getColoniaByCp);
              }
            done();
          });
        setTimeout(done, 500);
      });
    });
  });
  function validarFuncion (funcionString = null) {
    const url = baseURL;
    let value;
    if (funcionString === null) {
      value = url;
    } else {
      const apiget = funcionString;
      const apisubstring = apiget.split('baseURL + \'')[1];
      const go = apisubstring.split('\'')[0];
      value = go;
    }
    return value;
  }
})
