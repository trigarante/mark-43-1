import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProveedorLeads, ProveedorLeadsData} from '../../../../@core/data/interfaces/catalogos/proveedor-leads';
import {map} from 'rxjs/operators';
import {CampanaMarketing, CampanaMarketingData} from '../../../../@core/data/interfaces/marketing/campana';
import {MediosDifusion, MediosDifusionData} from '../../../../@core/data/interfaces/marketing/medios-difusion';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Router} from '@angular/router';
import {GuardadoComponent} from '../guardado/guardado.component';

@Component({
  selector: 'ngx-actualizar-medios-difusion',
  templateUrl: './actualizar-medios-difusion.component.html',
  styleUrls: ['./actualizar-medios-difusion.component.scss'],
})
export class ActualizarMediosDifusionComponent implements OnInit {
  @Input() id: number;
  proveedores: ProveedorLeads[];
  campana: CampanaMarketing[];
  mediosDifusion: MediosDifusion;
  mostrarProveedores: boolean;
  mostrarCampana: boolean;
  actualizarMediosDifusionForm: FormGroup;
  constructor( protected ref: NbDialogRef<ActualizarMediosDifusionComponent>, private fb: FormBuilder,
               protected proveedorLeadService: ProveedorLeadsData, private router: Router,
               protected campanaMarketingService: CampanaMarketingData, protected mediosDifusionService: MediosDifusionData,
               public dialogService: NbDialogService) {
    this.actualizarMediosDifusionForm = this.fb.group({
      'idProveedor': new FormControl('', Validators.compose([Validators.required])),
      'idExterno': 1,
      'descripcion': new FormControl('', Validators.compose([Validators.required])),
      'regla': new FormControl('', Validators.compose([Validators.required])),
      'presupuesto': new FormControl('', Validators.compose([Validators.required])),
      'idCampana': new FormControl('', Validators.compose([Validators.required])),
    });
  }
 getprovedorees() {
    this.proveedorLeadService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.proveedores = data;
      if (this.proveedores.length === 0) {
        this.mostrarProveedores = true;
      }else {
        this.mostrarProveedores = false;
      }

    });
 }
 getCampana() {
    this.campanaMarketingService.get().pipe(map(result => {
      return result.filter(valor => valor.activo === 1);
    })).subscribe(data => {
      this.campana = data;
      if (this.campana.length === 0) {
        this.mostrarCampana = true;
      }else {
        this.mostrarCampana = false;
      }

    });

 }
 getMediosDifusionById() {

   this.mediosDifusionService.getMediosDifusionById(this.id).subscribe(data => {
     this.mediosDifusion = data;
     this.actualizarMediosDifusionForm.controls['idProveedor'].setValue((this.mediosDifusion.idProveedor));
     this.actualizarMediosDifusionForm.controls['descripcion'].setValue((this.mediosDifusion.descripcion));
     this.actualizarMediosDifusionForm.controls['regla'].setValue((this.mediosDifusion.regla));
     this.actualizarMediosDifusionForm.controls['presupuesto'].setValue((this.mediosDifusion.presupuesto));
     this.actualizarMediosDifusionForm.controls['idCampana'].setValue((this.mediosDifusion.idCampana));
   });

 }
 guardar() {
   this.mediosDifusionService.getMediosDifusionById(this.id).subscribe(data => {
     this.mediosDifusion = data;
     this.mediosDifusion['idProveedor'] = this.actualizarMediosDifusionForm.value.idProveedor;
     this.mediosDifusion['descripcion'] = this.actualizarMediosDifusionForm.value.descripcion;
     this.mediosDifusion['regla'] = this.actualizarMediosDifusionForm.value.regla;
     this.mediosDifusion['presupesto'] = this.actualizarMediosDifusionForm.value.presupuesto;
     this.mediosDifusion['idCampana'] = this.actualizarMediosDifusionForm.value.idCampana;
     this.mediosDifusionService.put(this.id, this.mediosDifusion).subscribe(result => {
       this.mediosDifusionService.postSocket({status: 'ok'}).subscribe(x => {});
       this.ref.close();
       this.router.navigate(['/modulos/marketing/medios-difusion/']);
     });
     this.dialogService.open(GuardadoComponent, {
       context: {
         id: this.id,
         modo: 0,
       },
     });
   });
 }
  dismiss() {
    this.ref.close();
  }
  ngOnInit() {
    this.getprovedorees();
    this.getCampana();
    this.getMediosDifusionById();

  }

}
