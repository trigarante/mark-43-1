import { Injectable } from '@angular/core';
import {ProductoSolicitudData, ProductoSolicitudvn} from '../../interfaces/ventaNueva/producto-solicitud';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProductoSolicitudvnService implements ProductoSolicitudData {
  private baseURL;

  constructor(private http: HttpClient) {
    // super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }

  get(): Observable<ProductoSolicitudvn[]> {
    return this.http.get<ProductoSolicitudvn[]>(this.baseURL + 'v1/producto-solicitudvn');
  }

  getDatos(): Observable<ProductoSolicitudvn[]> {
    return this.http.get<ProductoSolicitudvn[]>(this.baseURL + 'v1/producto-cliente/datos');
  }

  post(nombre: ProductoSolicitudvn): Observable<ProductoSolicitudvn> {
    return this.http.post<ProductoSolicitudvn>(this.baseURL + 'v1/producto-solicitudvn', nombre);
  }

  getProductoSolicitud(id): Observable<ProductoSolicitudvn> {
    return this.http.get<ProductoSolicitudvn>(this.baseURL + 'v1/producto-solicitudvn/' + id);
  }

  getProductoSolicitudDatos(id): Observable<ProductoSolicitudvn> {
    return this.http.get<ProductoSolicitudvn>(this.baseURL + 'v1/producto-cliente/datos/' + id);
  }

  put(id, nombre: ProductoSolicitudvn): Observable<ProductoSolicitudvn> {
    return this.http.put<ProductoSolicitudvn>(this.baseURL + 'v1/producto-solicitudvn/' +
      id, nombre);
  }
}
