import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {BajasrhComponent} from './bajasrh.component';
import {
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatOptionModule,
  MatPaginatorModule, MatSelectModule,
  MatTableModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {KeyFilterModule} from 'primeng/primeng';
import {PrecandidatosData} from '../../../@core/data/interfaces/rrhh/precandidatos';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {Router} from '@angular/router';
import {NbToastrConfig} from '@nebular/theme/components/toastr/toastr-config';
import {EtapasModulosData} from '../../../@core/data/interfaces/catalogos/etapasModulos';
import {SolicitudesData} from '../../../@core/data/interfaces/rrhh/solicitudes';


describe('BajasrhComponent', () => {
  let component: BajasrhComponent;
  let fixture: ComponentFixture<BajasrhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajasrhComponent ],
      imports: [MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        KeyFilterModule,
        MatDatepickerModule,
        MatOptionModule,
        MatSelectModule,
      ],
      providers: [
        PrecandidatosData,
        EtapasModulosData,
        SolicitudesData,
        {
          provide: NbDialogRef, useValue: [],
        },
        {
          provide:  Router, useValue: [],
        },
        {
          provide: NbToastrService,
          useValue: NbToastrConfig,
        },
        {
          provide: NbDialogService,
          useValue: Document,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajasrhComponent);
    component = fixture.componentInstance;
    /*fixture.detectChanges();*/
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
